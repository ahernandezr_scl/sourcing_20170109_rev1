<?php

use App\Models\Contratistas;

class MySourcing
{


  static public function parserFthirty($pstrText){

		$larrData = array();
		$larrDataResult = array();
		$lstrTextProcess = "0";
		$lstrText = $pstrText;
		//Ejemplo: DIRECCIÓN DEL TRABAJO Nº: Codigo Oficina AÑO CERTIFICADO 2000 2015 3219003 CERTIFICADO DE CUMPLIMIENTO DE OBLIGACIONES LABORALES Y PREVISIONALES La Dirección del Trabajo, respecto de la empresa solicitante que se individualiza a continuación, en su calidad de CONTRATISTA y de conformidad con la información entregada en la Solicitud de Certificado, que es de su responsabilidad, certifica lo siguiente: 1.- INDIVIDUALIZACIÓN DEL SOLICITANTE RUT RAZÓN SOCIAL / NOMBRE 76355804-5 SOURCING SPA RUT REP. LEGAL REPRESENTANTE LEGAL 13028567-8 EUGENIO CEPEDA SÁNCHEZ DOMICILIO Av. Nueva Providencia 2155, of. 601, TC REGIÓN COMUNA TELÉFONO 13 PROVIDENCIA 76494634 CÓDIGO DE ACTIVIDAD ECONÓMICA (CAE) SERVICIOS DE INGENIERIA PRESTADOS POR EMPRESAS N.C.P. 2.- ANTECEDENTES DE LA OBRA, EMPRESA O FAENA OBJETO DEL CERTIFICADO NOMBRE DE LA OBRA, FAENA, PUESTO DE TRABAJO O SERVICIO SEGÚN CONTRATO CIVIL División Ministro Hales DOMICILIO DE LA OBRA Calama REGIÓN COMUNA LOCALIDAD (SI CORRESPONDE) 02 CALAMA 2.1.- SITUACIÓN DE LOS TRABAJADORES DECLARADOS A LA FECHA DE LA SOLICITUD DESVINCULADOS EN EL PERÍODO TOTAL TRABAJADORES VIGENTES 0 7 2.2.- ESTADO DE LAS COTIZACIONES PREVISIONALES PAGADAS NO PAGADAS SE ADJUNTA NÓMINA X No 2.3.- DETALLE DE REMUNERACIONES MES AÑO N° TRABAJADORES CON PAGO MONTO PAGADO ($) N° TRABAJADORES SIN PAGO 11 2015 7 11198938 0 2.4.- DETALLE DE INDEMNIZACIONES 2.4.1.- INDEMNIZACIÓN SUSTITUTIVA DEL AVISO PREVIO N° TRABAJADORES CON PAGO MONTO PAGADO ($) N° TRABAJADORES SIN PAGO - - - 2.4.2.- INDEMNIZACION POR AÑO(S) DE SERVICIO N° TRABAJADORES CON PAGO MONTO PAGADO ($) N° TRABAJADORES SIN PAGO - - - 3.- ANTECEDENTES DE LA EMPRESA PRINCIPAL RUT RAZÓN SOCIAL / NOMBRE 61704000-K Corporación Nacional del Cobre de Chile RUT REP. LEGAL REPRESENTANTE LEGAL 11820221-K Ignacio Tejeda Salazar DOMICILIO DE EMPRESA PRINCIPAL Huerfanos 1270 REGIÓN COMUNA TELÉFONO 13 SANTIAGO 66171344 4.- OBJETIVO DEL CERTIFICADO CURSAR ESTADOS DE PAGO DEVOLUCIÓN DE GARANTÍA CUMPLIMIENTO DE OBLIGACIONES X - 5.- PERÍODO CERTIFICADO Y ÁMBITO DE VALIDEZ El presente Certificado cubre exclusivamente la Obra, Empresa o Faena señalada en el punto 2 anterior y por el período comprendido entre 11/2015  y  11/2015 , siendo válido en todo el territorio nacional. 6.- REQUISITOS DE VALIDEZ Este Certificado tiene validez sin enmendaduras y con su respectivo CÓDIGO DE VERIFICACIÓN. 7.- OBSERVACIÓN FINAL La empresa principal deberá verificar que los datos consignados en el presente Certificado, entregados por el propio solicitante correspondan a la realidad de los servicios prestados en su calidad de contratista o subcontratista, según sea el caso, como por ejemplo “TOTAL TRABAJADORES VIGENTES”, del punto 2.1 del presente Certificado. GABRIEL ISMAEL RAMIREZ ZUÑIGA SUBJEFE DEPARTAMENTO DE INSPECCIÓN DIRECCION DEL TRABAJO - Fecha de emisión en linea 16-12-2015 12:59:33 Hrs. - Es de responsabilidad de la empresa principal o contratista, según corresponda, verificar la validez del certificado en el sitio web de la Dirección del Trabajo, <004B005700570053001D001200120057005500440050004C0057004800560011004700570011004A0052004500110046004F00120057005500440050004C00570048005600480051004F004C00510048 00440012003900480055004C0049004C004600440047005200550037005500440050004C0057004800560012003900480055004C0049004C004600440047005200550037005500440050004C00570048 00560011004400560053005B> (Ingresar el folio en el recuadro “Verificación de Trámites”, y seleccionar el trámite “Certificado Cumplimiento de Obligaciones Laborales”). - El certificado se podrá verificar hasta 60 días después de su emisión. - El presente Certificado incorpora Firma electrónica Avanzada. Ht5w8K3N Código de Verificación   CERTIFICADO 2000/2015/3219003 Detalle por mes, de los trabajadores declarados en la certificación Nómina de Trabajadores MES AÑO RUT NOMBRE TRABAJADOR 11 2015 12722349-1 EDUARDO ALEX MATURANA TEUSCHER 11 2015 13028567-8 EUGENIO ERNESTO CEPEDA SANCHEZ 11 2015 13065447-9 ABEL MAURICIO CARREÑO MIRANDA 11 2015 15525396-7 DANNY ALEJANDRO LETELIER CABEZAS 11 2015 8111914-7 JUAN PABLO CANCINO GONZALEZ 11 2015 8890969-0 GABRIEL ARTURO CARRASCO DONOSO 11 2015 9293085-8 JORGE LEANDRO ABARCA CORNEJO TOTAL DE TRABAJADORES: 7

		//Extraemos la empresa a la que se le está cargando el archivo
		$lstrTextProcess = strrpos($lstrText,"1.- INDIVIDUALIZACIÓN DEL SOLICITANTE RUT RAZÓN SOCIAL / NOMBRE"); //Usamos como etiqueta clave el encabezado de la linea

		if ($lstrTextProcess){ //Si el encabezado de la linea existe, entonces procedemos a revisar
			$lstrTextProcess = trim(substr($lstrText, $lstrTextProcess+65)); //Tomamos la información que se encuentra luego del encabezado.
			$lstrTextProcess = trim(substr($lstrTextProcess, 0, strpos($lstrTextProcess,"RUT REP. LEGAL"))); //Nos traemos hasta el final del nombre de la empresa que sería el comienzo del representante.
			$larrData['RUT'] = trim(substr($lstrTextProcess, 0, strpos($lstrTextProcess," ")));
			$larrData['NOMBRE'] = trim(substr($lstrTextProcess, strpos($lstrTextProcess," ")));

	        //Extraemos el detalle de los empleados
			$lstrTextProcess = strrpos($lstrText,"MES AÑO RUT NOMBRE TRABAJADOR"); //Usamos como etiqueta clave el encabezado de la linea
			if ($lstrTextProcess){ //Si el encabezado de la linea existe, entonces procedemos a revisar
			  $lstrTextProcess = trim(substr($lstrText, $lstrTextProcess+30)); //Tomamos la información que se encuentra luego del encabezado.
			  $lintCountEmployee = 0; //Inicializamos variables que vamos a utilizar
			  $larrDataTemp = array(); //Inicializamos variables que vamos a utilizar
			  $larrRUT = explode('-',$lstrTextProcess); //Tomamos un caracter que sea constante en la estructura, en este caso el guion, de tal manera sabemos que cada elemento del arreglo comienza por el ultimo digito del rut del elmento anterior. (a excepción del primer elemento)
			  $lintPre = 3; //Como estámos picando por el guion, sabemos que los ultimos 3 elementos de cada elemento del arreglo corresponden al mes, año y rut del empleado, sin su verificador (a excepción del ultimo)
			  foreach ($larrRUT as $key => $value) { //Comenzamos a recorrer el arreglo
			  	if (strrpos($value,"TOTAL DE TRABAJADORES:")){ //Usamos como etiqueta clave el total de los trabajadores, ya que marcará el final de la información de los empleados.
			  	  $lintCountEmployee = trim(substr($value,strrpos($value,"TOTAL DE TRABAJADORES:")+22));
			      $value = trim(substr($value,0,strrpos($value,"TOTAL DE TRABAJADORES:")));
			      $lintPre = 0;
			  	}
			  	if ($key==0){
			  	  $larrDataTemp = explode(" ",$value);
			  	  if (count($larrDataTemp) == 3){
			  	  	$larrDataTemp = array("month" =>$larrDataTemp[0],
			  	  					      "year" =>$larrDataTemp[1],
			  	  					      "rut" => $larrDataTemp[2],
			  	  					      "name"=> ""
			  	  		                  );
			  	  }
			  	}else{
			  	  $larrDataTemp = explode(" ",$value);
			  	  $lintCount = count($larrDataTemp);
			  	  if ($lintCount){
			  	  	$larrData['employeedetail'][$key-1]['rut'] .= '-'.$larrDataTemp[0];
			  	    $lstrName = "";
			  	    for ($i=1; $i < $lintCount-$lintPre; $i++) {
			  	    	$lstrName .= $larrDataTemp[$i]." ";
			  	    }
			  	    $larrData['employeedetail'][$key-1]['name'] = trim($lstrName);
			  	  	$larrDataTemp = array("month" =>$larrDataTemp[$lintCount-3],
			  	  					      "year" =>$larrDataTemp[$lintCount-2],
			  	  					      "rut" => $larrDataTemp[$lintCount-1],
			  	  					      "name"=> ""
			  	  		                  );
			  	  }
			  	}
			  	if ($lintPre){
			  	  $larrData['employeedetail'][] = $larrDataTemp;
			  	}
			  }
			  if (count($larrData['employeedetail']) and $lintCountEmployee){
				if (count($larrData['employeedetail']) != $lintCountEmployee){
				  $larrDataResult['code'] = "2";
				  $larrDataResult['message'] = "La cantidad de empleados no coincide con el total";
				  $larrDataResult['result'] = $larrData;
				}
			  }
			}else{ //Si el encabezado de la linea NO existe, entonces enviamos un mensaje de no encontrarse detalle de los empleados
				$larrDataResult['code'] = "3";
				$larrDataResult['message'] = "No se encontró detalle de los empleados (Falta encabezado)";
				$larrDataResult['result'] = "";
			}

		}else{
			$larrDataResult['code'] = "3";
			$larrDataResult['message'] = "No se encontró la información de la empresa (Falta encabezado)";
			$larrDataResult['result'] = "";
		}

		if (!isset($larrDataResult['message'])){
			$larrDataResult['code'] = "1";
			$larrDataResult['message'] = "Archivo procesado satisfactoriamente";
			$larrDataResult['result'] = $larrData;
		}

		return $larrDataResult;
	}


	static public function parserFthirtySave($lobjDocument, $lobjArchivo){

		$larrResult = array();
		$lobjContratistas = \DB::select('select * from tbl_contratistas where RUT = \''.$lobjDocument['RUT'].'\'');

		if ($lobjContratistas) {
			$lintIdContratista = $lobjContratistas[0]->IdContratista;
			$lstrRUT = $lobjDocument['RUT'];
			$lstrNombre = $lobjDocument['NOMBRE'];

			//Guardamos el archivo en la base de datos:
			//$lstrQuery = "INSERT INTO tbl_f30_1(IdContratista, RUT, RazonSocial, TrabajadoresVigentes, TrabajadoresDesvinculados, TotalCotizaciones) VALUES ";
			//$lstrQuery .= "(".$lintIdContratista.",'".$lstrRUT."', '".$lstrNombre."', 0, 0, 0)";

			//$lintIdF30 = \DB::select($lstrQuery);

			$lintIdF30 = DB::table('tbl_f30_1')-> insertGetId(array(
													        'IdContratista' => $lintIdContratista,
													        'RUT' => $lstrRUT,
													        'RazonSocial' => $lstrNombre
													));

			foreach ($lobjDocument['employeedetail'] as $rows) {

				//Buscamos cada uno de los empleados de la contratista
				$lobjPersonas = \DB::select('select * from tbl_personas where rut = \''.$rows['rut'].'\'');


				$ldatPeriodo = $rows['year']."-".$rows['month'].'-1';
				$lstrRUT = $rows['rut'];
				$lstrNombre = $rows['name'];

				$dataempleado = array(
										'IdF301' => $lintIdF30,
										'Periodo' => $ldatPeriodo,
										'RUT' => $lstrRUT,
										'Nombre' => $lstrNombre
									  );
				if ($lobjPersonas){
					$dataempleado['IdPersona'] = $lobjPersonas[0]->IdPersona;
				}
				$lintIdF30Detalle = DB::table('tbl_f30_1_empleados')-> insertGetId($dataempleado);

			}



			//Cargamos el archivo en el documento la empresa.s
			$lstrQuery = 'select tbl_documentos.*, tbl_tipos_documentos.Vigencia from tbl_documentos inner join tbl_tipos_documentos on tbl_documentos.IdTipoDocumento = tbl_tipos_documentos.IdTipoDocumento where tbl_documentos.Entidad = 1 and tbl_documentos.IdEntidad = '.$lintIdContratista.' and tbl_documentos.IdTipoDocumento = 1';
			$lobjDocumentos = \DB::select($lstrQuery);
			if ($lobjDocumentos){

			  $ldatVigencia = date_create($ldatPeriodo);
              date_add($ldatVigencia, date_interval_create_from_date_string($lobjDocumentos[0]->Vigencia.' days'));

			  $larrData = array(
										'Documento' => $lobjArchivo['Archivo'],
										'DocumentoURL' => $lobjArchivo['ArchivoUrl'],
										'IdEstatus' => 2, //Marcamos que está para aprobar
										'FechaVencimiento' => $ldatVigencia,
									  );
			  \DB::table('tbl_documentos')->where("IdDocumento",$lobjDocumentos[0]->IdDocumento)->update($larrData);
			}

			$larrResult["code"] = "1";
			$larrResult["message"] = "Archivo procesado satisfactoriamente";
			$larrResult["result"] = "";
		}else{
			$larrResult["code"] = "4";
			$larrResult["message"] = "El archivo F30-1 que intenta guardar no pertenece a ningún contratista registrado";
			$larrResult["result"] = "";
		}
		return $larrResult;

    }

  	static public function CargaDocumentoEstatus($pintIdEstatus){

  	  if ($pintIdEstatus==1){
		$lstrResult = "Cargado";
  	  }else if ($pintIdEstatus==2){
  	  	$lstrResult = "Procesado";
  	  }else if ($pintIdEstatus==3){
  	  	$lstrResult = "Estructura no correcta";
  	  }else if ($pintIdEstatus==4){
  	  	$lstrResult = "Error en datos";
  	  }else{
  	  	$lstrResult = "No especificado";
  	  }
  	  return $lstrResult;
  	}

    static public function LevelUser($pintIdUser) {
    	$lobjUserLevel = DB::table("tb_users")
				    	     ->select("tb_groups.level","tb_groups.group_id")
				    	     ->join("tb_groups","tb_users.group_id", "=", "tb_groups.group_id")
				    	     ->where("id","=",$pintIdUser)
				    	     ->get();
		// Almaceno el resulrado de la consulta en un vector
    	$larrUserLevel = array();
        foreach ($lobjUserLevel as $value){
            $larrUserLevel[] = $value->group_id;
        }
        if (count($larrUserLevel)) {
        	return $larrUserLevel[0];
        }else{
        	return 0;
        }
    }

  	static public function Status($pintIdEstatus){

  	  if ($pintIdEstatus==1){
		$lstrResult = "Activo";
  	  }else if ($pintIdEstatus==2){
  	  	$lstrResult = "Suspendido";
  	  }else{
  	  	$lstrResult = "No especificado";
  	  }
  	  return $lstrResult;
  	}
  	static public function ValorUno($pintIdSexo=""){

  	  if ($pintIdSexo==1){
		$lstrResult = "Hombre";
  	  }else if ($pintIdSexo==2){
  	  	$lstrResult = "Mujer";
  	  }else{
  	  	$lstrResult = "No especificado";
  	  }
  	  return $lstrResult;
  	}
  	static public function RecurrenciaDocumentos($pintVigencia=0){
      if ($pintVigencia==0){
		$lstrResult = "Una vez";
	  }else if ($pintVigencia==1){		
	  	$lstrResult = "Diario";
	  }else if ($pintVigencia==7){	
	  	$lstrResult = "Semanal";
  	  }else if ($pintVigencia==30){
  	  	$lstrResult = "Mensual";
  	  }else if ($pintVigencia==60){
  	  	$lstrResult = "Bimensual";
  	  }else if ($pintVigencia==90){
  	  	$lstrResult = "Trimensual";
  	  }else if ($pintVigencia==180){
  	  	$lstrResult = "Semestral";
  	  }else if ($pintVigencia==365){
  	  	$lstrResult = "Anual";
  	  }else if ($pintVigencia==730){
  	  	$lstrResult = "Bianual";
  	  }else if ($pintVigencia==1095){
  	  	$lstrResult = "Trianual";
  	  }else{
  	  	$lstrResult = "No especificado";
  	  }
  	  return $lstrResult;
  	}
  	static public function ValorDos($pintIdEstadoCivil=""){

  	  if ($pintIdEstadoCivil==1){
		$lstrResult = "Soltero";
  	  }else if ($pintIdEstadoCivil==2){
  	  	$lstrResult = "Casado";
  	  }else if ($pintIdEstadoCivil==3){
  	  	$lstrResult = "Divorciado";
  	  }else if ($pintIdEstadoCivil==4){
  	  	$lstrResult = "Viudo";
  	  }else{
  	  	$lstrResult = "No especificado";
  	  }
  	  return $lstrResult;
  	}
  	static public function AccessStatus($pintIdEstatus=""){

  	  if ($pintIdEstatus==1){
		$lstrResult = "Activo";
  	  }else if ($pintIdEstatus==2){
  	  	$lstrResult = "Suspendido";
  	  }else if ($pintIdEstatus==3){
  	  	$lstrResult = "No autorizado";
  	  }else if ($pintIdEstatus==4){
  	  	$lstrResult = "Temporal";
  	  }else if ($pintIdEstatus==5){
  	  	$lstrResult = "No permitido";
  	  }else{
  	  	$lstrResult = "No especificado";
  	  }
  	  return $lstrResult;
  	}
  	static public function AccessType($pintIdTipo=""){

  	  if ($pintIdTipo==1){
		$lstrResult = "Trabajador";
  	  }else if ($pintIdTipo==2){
  	  	$lstrResult = "Visitante";
  	  }else if ($pintIdTipo==3){
  	  	$lstrResult = "Temporal";
  	  }else{
  	  	$lstrResult = "No especificado";
  	  }
  	  return $lstrResult;
  	}
  	static public function DocumentsStatus($pintIdEstatus=""){

  	  if ($pintIdEstatus==1){
		$lstrResult = "Por cargar";
  	  }else if ($pintIdEstatus==2){
  	  	$lstrResult = "Por aprobar";
  	  }else if ($pintIdEstatus==3){
  	  	$lstrResult = "No aprobado";
  	  }else if ($pintIdEstatus==4){
  	  	$lstrResult = "Temporal";
  	  }else if ($pintIdEstatus==5){
  	  	$lstrResult = "Aprobado";
  	  }else{
  	  	$lstrResult = "No especificado";
  	  }
  	  return $lstrResult;
  	}
  	static public function DocumentsView($pstrName) {
  		if (strpos($pstrName,',')!=""){
	  		if (strpos($pstrName,',')>=0){
	  		  $larrArg = explode(",",$pstrName);
	  		  if ($larrArg[0]!=""){
	  			return "<a onClick=\"ViewPDF('".$larrArg[0]."',".$larrArg[1].");\" class=\"btn btn-xs btn-white tips\"><i class=\"\" ></i>Ver</a>";
	    	  }else{
	  			return "-";
	  		  }
	  		}else{
	  	  	  if ($pstrName!=''){
	  			return "<a onClick=\"ViewPDF('".$pstrName."','');\" class=\"btn btn-xs btn-white tips\"><i class=\"\" ></i>Ver</a>";
	    	  }else{
	  			return "-";
	  		  }
	  		}
	  	}else{
	  		if ($pstrName!=''){
	  			return "<a onClick=\"ViewPDF('".$pstrName."','');\" class=\"btn btn-xs btn-white tips\"><i class=\"\" ></i>Ver</a>";
	    	  }else{
	  			return "-";
	  		  }
	  	}

  	}
  	static public function DocumentsDetail($pintEntidad){
  		$larrArg = explode(",",$pintEntidad);
  		if (count($larrArg)>1){
  			if ($larrArg[1]==1){//Se trata de un contratista
  				$module = "contratistas";
  			}else if ($larrArg[1]==2){//Se trata de un contrato
  				$module = "contratos";
  			}else if ($larrArg[1]==3){//Se trata de una persona
				$module = "personas";
  			}else if ($larrArg[1]==6){//Se trata de un centro
  				$module = "centros";
  			}
  			$onclick = " onclick=\"SximoModal(this.href,'View Detail'); return false; \"" ;
			$html = '<a href="'.URL::to($module.'/show/'.$larrArg[0]).'" '.$onclick.' class="btn btn-xs btn-white tips" title="'.Lang::get('core.btn_view').'"><i class="fa fa-search"></i></a>';
  		}
  		return $html;
  	}
  	static public function DocumentsDetailTwo($pintEntidad){
  		$larrArg = explode(",",$pintEntidad);
  		if (count($larrArg)>1){
  			if ($larrArg[1]==1){//Se trata de un contratista
  				$module = "contratistas";
  			}else if ($larrArg[1]==2){//Se trata de un contrato
  				$module = "contratos";
  			}else if ($larrArg[1]==3){//Se trata de una persona
				$module = "personas";
  			}else if ($larrArg[1]==6){//Se trata de un centro
  				$module = "centros";
  			}
  			$onclick = " onclick=\"SximoModal(this.href,'View Detail'); return false; \"" ;
			$html = '<a href="'.URL::to($module.'/show/'.$larrArg[0]).'" '.$onclick.' class="btn btn-xs btn-white tips" title="'.Lang::get('core.btn_view').'">ver más</a>';
  		}
  		return $html;
  	}

         static public function tipoAcceso($IdTipoAcceso)
            {
               switch ($IdTipoAcceso) {
                        case 1:
                           return '<span > Trabajador </span>';
                            break;
                        case 2:
                            return '<span > Visitante </span>';
                            break;
                    }
            }
        static public function statusAcceso($IdEstatus)
            {
                if ($IdEstatus==1){
                    return '<span class="label label-primary"> Con Acceso </span>';
                }
                else if ($IdEstatus==2){
                    return '<span class="label label-danger"> Sin Acceso </span>';
                }
                else{
                         return '<span class="label label-success"> Acceso Temporal </span>';
                }


            }
}

?>
