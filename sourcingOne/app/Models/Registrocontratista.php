<?php namespace App\Models;

use Illuminate\Auth\Authenticatable;
use Illuminate\Database\Eloquent\Model;

class registrocontratista extends Sximo  {
	
	protected $table = 'sb_empresa';
	protected $primaryKey = 'id_empresa';

	public function __construct() {
		parent::__construct();
		
	}

	public static function querySelect(  ){
		
		return "  SELECT sb_empresa.* FROM sb_empresa  ";
	}	

	public static function queryWhere(  ){
		
		return "  WHERE sb_empresa.id_empresa IS NOT NULL ";
	}
	
	public static function queryGroup(){
		return "  ";
	}
	

}
