<?php namespace App\Models;

use Illuminate\Auth\Authenticatable;
use Illuminate\Database\Eloquent\Model;

class adjuntoscontratos extends Sximo  {
	
	protected $table = 'sb_contrato_adjuntos';
	protected $primaryKey = 'id_adjunto';

	public function __construct() {
		parent::__construct();
		
	}

	public static function querySelect(  ){
		
		return "  SELECT sb_contrato_adjuntos.* FROM sb_contrato_adjuntos  ";
	}	

	public static function queryWhere(  ){
		
		return "  WHERE sb_contrato_adjuntos.id_adjunto IS NOT NULL ";
	}
	
	public static function queryGroup(){
		return "  ";
	}
	

}
