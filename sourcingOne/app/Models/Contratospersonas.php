<?php namespace App\Models;

use Illuminate\Auth\Authenticatable;
use Illuminate\Database\Eloquent\Model;

class contratospersonas extends Sximo  {
	
	protected $table = 'tbl_contratos_personas';
	protected $primaryKey = 'IdContratosPersonas';

	public function __construct() {
		parent::__construct();
		
	}

	public static function querySelect(  ){
		
		return "  SELECT tbl_contratos_personas.*, tbl_personas.RUT, tbl_personas.Nombres, tbl_personas.Apellidos, tb_users.first_name, tb_users.last_name FROM tbl_contratos_personas INNER JOIN tbl_personas ON tbl_personas.IdPersona = tbl_contratos_personas.IdPersona INNER JOIN tb_users ON tbl_personas.entry_by_access = tb_users.id ";
	}	

	public static function queryWhere(  ){
		$lintLevelUser = \MySourcing::LevelUser(\Session::get('uid'));
	    $lintIdUser = \Session::get('uid');
	    if ($lintLevelUser==6){
		  return "  WHERE tbl_contratos_personas.IdContratosPersonas IS NOT NULL 
		                AND (tbl_contratos_personas.entry_by_access = ".$lintIdUser." 
		                 OR EXISTS (select tbl_contrato.contrato_id
					 	            from tbl_contrato
						            inner join tbl_contratos_personas on tbl_contrato.contrato_id = tbl_contratos_personas.contrato_id
						            inner join tbl_contratos_subcontratistas on tbl_contratos_subcontratistas.contrato_id = tbl_contratos_personas.contrato_id
						            inner join tbl_contratistas on tbl_contratos_subcontratistas.idsubcontratista = tbl_contratistas.idcontratista
						            where tbl_contratistas.entry_by_access = tbl_contratos_personas.entry_by_access
						            and tbl_contratos_personas.idpersona = tbl_contratos_personas.idpersona
						            and tbl_contrato.entry_by_access = ".$lintIdUser.") 
						     ) ";
		}else{
		  return "  WHERE tbl_contratos_personas.IdContratosPersonas IS NOT NULL ";
		}
	}
	
	public static function queryGroup(){
		return "  ";
	}
	

}
