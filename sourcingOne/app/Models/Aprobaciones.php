<?php namespace App\Models;

use Illuminate\Auth\Authenticatable;
use Illuminate\Database\Eloquent\Model;

class aprobaciones extends Sximo  {

	protected $table = 'tbl_documentos';
	protected $primaryKey = 'IdDocumento';

	public function __construct() {
		parent::__construct();

	}

	public static function querySelect(  ){

		return " SELECT `tbl_documentos`.*,
    case when `tbl_documentos`.`Entidad` = 1 then CONCAT(`tbl_contratistas`.`RUT`,' ',`tbl_contratistas`.`RazonSocial`)
       when `tbl_documentos`.`Entidad` = 2 then CONCAT(`tbl_contrato`.`cont_proveedor`,' ',`tbl_contrato`.`cont_numero`)
       when `tbl_documentos`.`Entidad` = 3 then CONCAT(`tbl_personas`.`RUT`,' ', `tbl_personas`.`Nombres`,' ', `tbl_personas`.`Apellidos`) else 'No especificado' end as Detalle,
       '' as OtroDetalle
FROM `tbl_documentos`
LEFT JOIN `tbl_contratistas` ON `tbl_documentos`.`Entidad` = 1 AND `tbl_documentos`.`IdEntidad` = `tbl_contratistas`.`IdContratista`
LEFT JOIN `tbl_contrato` ON `tbl_documentos`.`Entidad` = 2 AND `tbl_documentos`.`IdEntidad` = `tbl_contrato`.`contrato_id`
LEFT JOIN `tbl_personas` ON `tbl_documentos`.`Entidad` = 3 AND `tbl_documentos`.`IdEntidad` = `tbl_personas`.`IdPersona` ";
	}

	public static function queryWhere(  ){

		return "  WHERE tbl_documentos.IdDocumento IS NOT NULL AND tbl_documentos.entidad != 7 ";
	}

	public static function queryGroup(){
		return "  ";
	}


}
