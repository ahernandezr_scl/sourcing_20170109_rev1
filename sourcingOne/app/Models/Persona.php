<?php namespace App\Models;

use Illuminate\Auth\Authenticatable;
use Illuminate\Database\Eloquent\Model;

class persona extends Sximo  {
	
	protected $table = 'tb_personas';
	protected $primaryKey = 'IdPersona';

	public function __construct() {
		parent::__construct();
		
	}

	public static function querySelect(  ){
		
		return "  SELECT tb_personas.* FROM tb_personas  ";
	}	

	public static function queryWhere(  ){
		
		return "  WHERE tb_personas.IdPersona IS NOT NULL ";
	}
	
	public static function queryGroup(){
		return "  ";
	}
	

}
