<?php namespace App\Models;

use Illuminate\Auth\Authenticatable;
use Illuminate\Database\Eloquent\Model;

class personas extends Sximo  {
	
	protected $table = 'tbl_personas';
	protected $primaryKey = 'IdPersona';

	public function __construct() {
		parent::__construct();
		
	}

	public static function querySelect(  ){
		
		
		return "  SELECT tbl_personas.* FROM tbl_personas ";
	}	

	public static function queryWhere(  ){
		$lintLevelUser = \MySourcing::LevelUser(\Session::get('uid'));
	    $lintIdUser = \Session::get('uid');
	    if ($lintLevelUser==6){
		  return "  WHERE tbl_personas.IdPersona IS NOT NULL 
		                AND (tbl_personas.entry_by_access = ".$lintIdUser." 
		                 OR EXISTS (select tbl_contrato.contrato_id
					 	            from tbl_contrato
						            inner join tbl_contratos_personas on  tbl_contrato.contrato_id = tbl_contratos_personas.contrato_id
						            inner join tbl_contratos_subcontratistas on tbl_contratos_subcontratistas.contrato_id = tbl_contratos_personas.contrato_id
						            inner join tbl_contratistas on tbl_contratos_subcontratistas.idsubcontratista = tbl_contratistas.idcontratista
						            where tbl_contratistas.entry_by_access = tbl_personas.entry_by_access
						            and tbl_contratos_personas.idpersona = tbl_personas.idpersona
						            and tbl_contrato.entry_by_access = ".$lintIdUser.") 
						     ) ";
		}else{
			return "  WHERE tbl_personas.IdPersona IS NOT NULL ";
		}
	}
	
	public static function queryGroup(){
		return "  ";
	}
	

}
