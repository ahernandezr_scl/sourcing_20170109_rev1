<?php namespace App\Models;

use Illuminate\Auth\Authenticatable;
use Illuminate\Database\Eloquent\Model;

class ordenitem extends Sximo  {
	
	protected $table = 'sb_order_items';
	protected $primaryKey = 'id_item';

	public function __construct() {
		parent::__construct();
		
	}

	public static function querySelect(  ){
		
		return "  SELECT sb_order_items.* FROM sb_order_items  ";
	}	

	public static function queryWhere(  ){
		
		return "  WHERE sb_order_items.id_item IS NOT NULL ";
	}
	
	public static function queryGroup(){
		return "  ";
	}
	

}
