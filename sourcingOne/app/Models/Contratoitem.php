<?php namespace App\Models;

use Illuminate\Auth\Authenticatable;
use Illuminate\Database\Eloquent\Model;

class contratoitem extends Sximo  {
	
	protected $table = 'sb_contrato_items';
	protected $primaryKey = 'id_cont_items';

	public function __construct() {
		parent::__construct();
		
	}

	public static function querySelect(  ){
		
		return "  SELECT sb_contrato_items.* FROM sb_contrato_items  ";
	}	

	public static function queryWhere(  ){
		
		return "  WHERE sb_contrato_items.id_cont_items IS NOT NULL ";
	}
	
	public static function queryGroup(){
		return "  ";
	}
	

}
