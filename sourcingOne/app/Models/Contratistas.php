<?php namespace App\Models;

use Illuminate\Auth\Authenticatable;
use Illuminate\Database\Eloquent\Model;

class contratistas extends Sximo  {
	
	protected $table = 'tbl_contratistas';
	protected $primaryKey = 'IdContratista';

	public function __construct() {
		parent::__construct();
		
	}

	public static function querySelect(  ){
		
		return "  SELECT tbl_contratistas.* FROM tbl_contratistas  ";
	}	

	public static function queryWhere(  ){
		
		return "  WHERE tbl_contratistas.IdContratista IS NOT NULL ";
	}
	
	public static function queryGroup(){
		return "  ";
	}
	

}
