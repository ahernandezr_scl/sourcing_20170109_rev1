<?php namespace App\Models;

use Illuminate\Auth\Authenticatable;
use Illuminate\Database\Eloquent\Model;

class contratosmaquinas extends Sximo  {
	
	protected $table = 'tbl_contratos_maquinas';
	protected $primaryKey = 'id_cont_maq';

	public function __construct() {
		parent::__construct();
		
	}

	public static function querySelect(  ){
		
		return "  SELECT tbl_contratos_maquinas.* FROM tbl_contratos_maquinas  ";
	}	

	public static function queryWhere(  ){
		
		return "  WHERE tbl_contratos_maquinas.id_cont_maq IS NOT NULL ";
	}
	
	public static function queryGroup(){
		return "  ";
	}
	

}
