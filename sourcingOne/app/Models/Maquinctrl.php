<?php namespace App\Models;

use Illuminate\Auth\Authenticatable;
use Illuminate\Database\Eloquent\Model;

class maquinctrl extends Sximo  {
	
	protected $table = 'tbl_maquinarias';
	protected $primaryKey = 'id_maq';

	public function __construct() {
		parent::__construct();
		
	}

	public static function querySelect(  ){
		
		return "  SELECT tbl_maquinarias.* FROM tbl_maquinarias  ";
	}	

	public static function queryWhere(  ){
		
		return "  WHERE tbl_maquinarias.id_maq IS NOT NULL ";
	}
	
	public static function queryGroup(){
		return "  ";
	}
	

}
