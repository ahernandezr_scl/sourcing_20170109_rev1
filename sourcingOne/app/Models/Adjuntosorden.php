<?php namespace App\Models;

use Illuminate\Auth\Authenticatable;
use Illuminate\Database\Eloquent\Model;

class adjuntosorden extends Sximo  {
	
	protected $table = 'sb_order_adjuntos';
	protected $primaryKey = 'id_adjunto';

	public function __construct() {
		parent::__construct();
		
	}

	public static function querySelect(  ){
		
		return "  SELECT sb_order_adjuntos.* FROM sb_order_adjuntos  ";
	}	

	public static function queryWhere(  ){
		
		return "  WHERE sb_order_adjuntos.id_adjunto IS NOT NULL ";
	}
	
	public static function queryGroup(){
		return "  ";
	}
	

}
