<?php namespace App\Models;

use Illuminate\Auth\Authenticatable;
use Illuminate\Database\Eloquent\Model;

class servicios extends Sximo  {
	
	protected $table = 'sb_services';
	protected $primaryKey = 'id_service';

	public function __construct() {
		parent::__construct();
		
	}

	public static function querySelect(  ){
		
		return "  SELECT sb_services.* FROM sb_services  ";
	}	

	public static function queryWhere(  ){
		
		return " where id_service is not null ";
	}
	
	public static function queryGroup(){
		return "  ";
	}
	

}
