<?php namespace App\Models;

use Illuminate\Auth\Authenticatable;
use Illuminate\Database\Eloquent\Model;

class tblkpigral extends Sximo  {
	
	protected $table = 'tbl_kpigral';
	protected $primaryKey = 'id_kpi';

	public function __construct() {
		parent::__construct();
		
	}

	public static function querySelect(  ){
		
		return "  SELECT tbl_kpigral.* FROM tbl_kpigral  ";
	}	

	public static function queryWhere(  ){
		
		return "  WHERE tbl_kpigral.id_kpi IS NOT NULL ";
	}
	
	public static function queryGroup(){
		return "  ";
	}
	

}
