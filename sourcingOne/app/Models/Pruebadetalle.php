<?php namespace App\Models;

use Illuminate\Auth\Authenticatable;
use Illuminate\Database\Eloquent\Model;

class pruebadetalle extends Sximo  {
	
	protected $table = 'prueba_detalle';
	protected $primaryKey = 'IdPruebaDetalle';

	public function __construct() {
		parent::__construct();
		
	}

	public static function querySelect(  ){
		
		return "  SELECT prueba_detalle.* FROM prueba_detalle  ";
	}	

	public static function queryWhere(  ){
		
		return "  WHERE prueba_detalle.IdPruebaDetalle IS NOT NULL ";
	}
	
	public static function queryGroup(){
		return "  ";
	}
	

}
