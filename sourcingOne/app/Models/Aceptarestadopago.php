<?php namespace App\Models;

use Illuminate\Auth\Authenticatable;
use Illuminate\Database\Eloquent\Model;

class aceptarestadopago extends Sximo  {
	
	protected $table = 'sb_orders';
	protected $primaryKey = 'id_order';

	public function __construct() {
		parent::__construct();
		
	}

	public static function querySelect(  ){
		
		return "  SELECT sb_orders.* FROM sb_orders  ";
	}	

	public static function queryWhere(  ){
		
		return "  WHERE sb_orders.id_order IS NOT NULL and sb_orders.estado_id='Para Validacion' ";
	}
	
	public static function queryGroup(){
		return "  ";
	}
	

}
