<?php namespace App\Models;

use Illuminate\Auth\Authenticatable;
use Illuminate\Database\Eloquent\Model;

class maestrocontrato extends Sximo  {
	
	protected $table = 'sb_contratos';
	protected $primaryKey = 'id_contrato';

	public function __construct() {
		parent::__construct();
		
	}

	public static function querySelect(  ){
		
		return "  SELECT sb_contratos.* FROM sb_contratos  ";
	}	

	public static function queryWhere(  ){
		
		return "  WHERE sb_contratos.id_contrato IS NOT NULL ";
	}
	
	public static function queryGroup(){
		return "  ";
	}
	

}
