<?php namespace App\Http\Controllers;

use App\Http\Controllers\controller;
use App\Models\Porteria;
use Illuminate\Http\Request;
use Illuminate\Pagination\LengthAwarePaginator as Paginator;
use Validator, Input, Redirect ;


class PorteriaController extends Controller {

	protected $layout = "layouts.main";
	protected $data = array();
	public $module = 'porteria';
	static $per_page	= '10';

	public function __construct()
	{

		$this->beforeFilter('csrf', array('on'=>'post'));
		$this->model = new Porteria();

		$this->info = $this->model->makeInfo( $this->module);
		$this->access = $this->model->validAccess($this->info['id']);

		$this->data = array(
			'pageTitle'	=> 	$this->info['title'],
			'pageNote'	=>  $this->info['note'],
			'pageModule'=> 'porteria',
			'return'	=> self::returnUrl()

		);

		\App::setLocale(CNF_LANG);
		if (defined('CNF_MULTILANG') && CNF_MULTILANG == '1') {

		$lang = (\Session::get('lang') != "" ? \Session::get('lang') : CNF_LANG);
		\App::setLocale($lang);
		}



	}

	public function getIndex( Request $request )
	{

		if($this->access['is_view'] ==0)
			return Redirect::to('dashboard')
				->with('messagetext', \Lang::get('core.note_restric'))->with('msgstatus','error');

		$sort = (!is_null($request->input('sort')) ? $request->input('sort') : 'IdAcceso');
		$order = (!is_null($request->input('order')) ? $request->input('order') : 'asc');
		// End Filter sort and order for query
		// Filter Search for query
		$filter = '';
		if(!is_null($request->input('search')))
		{
			$search = 	$this->buildSearch('maps');
			$filter = $search['param'];
			$this->data['search_map'] = $search['maps'];
		}


		$page = $request->input('page', 1);
		$params = array(
			'page'		=> $page ,
			'limit'		=> (!is_null($request->input('rows')) ? filter_var($request->input('rows'),FILTER_VALIDATE_INT) : static::$per_page ) ,
			'sort'		=> $sort ,
			'order'		=> $order,
			'params'	=> $filter,
			'global'	=> (isset($this->access['is_global']) ? $this->access['is_global'] : 0 )
		);
		// Get Query
		$results = $this->model->getRows( $params );

		// Build pagination setting
		$page = $page >= 1 && filter_var($page, FILTER_VALIDATE_INT) !== false ? $page : 1;
		$pagination = new Paginator($results['rows'], $results['total'], $params['limit']);
		$pagination->setPath('porteria');

		$this->data['rowData']		= $results['rows'];
		// Build Pagination
		$this->data['pagination']	= $pagination;
		// Build pager number and append current param GET
		$this->data['pager'] 		= $this->injectPaginate();
		// Row grid Number
		$this->data['i']			= ($page * $params['limit'])- $params['limit'];
		// Grid Configuration
		$this->data['tableGrid'] 	= $this->info['config']['grid'];
		$this->data['tableForm'] 	= $this->info['config']['forms'];
		$this->data['colspan'] 		= \SiteHelpers::viewColSpan($this->info['config']['grid']);
		// Group users permission
		$this->data['access']		= $this->access;
		// Detail from master if any
		$this->data['fields'] =  \AjaxHelpers::fieldLang($this->info['config']['grid']);
		// Master detail link if any
		$this->data['subgrid']	= (isset($this->info['config']['subgrid']) ? $this->info['config']['subgrid'] : array());
		// Render into template
		//return view('porteria.index',$this->data);
		$this->data['row'] = $this->model->getColumnTable('tbl_accesos');

		$this->data['fields'] =  \AjaxHelpers::fieldLang($this->info['config']['forms']);

		return view('porteria.form',$this->data);
	}



	function getUpdate(Request $request, $id = null)
	{

		if($id =='')
		{
			if($this->access['is_add'] ==0 )
			return Redirect::to('dashboard')->with('messagetext',\Lang::get('core.note_restric'))->with('msgstatus','error');
		}

		if($id !='')
		{
			if($this->access['is_edit'] ==0 )
			return Redirect::to('dashboard')->with('messagetext',\Lang::get('core.note_restric'))->with('msgstatus','error');
		}

		$row = $this->model->find($id);
		if($row)
		{
			$this->data['row'] =  $row;
		} else {
			$this->data['row'] = $this->model->getColumnTable('tbl_accesos');
		}
		$this->data['fields'] =  \AjaxHelpers::fieldLang($this->info['config']['forms']);

		$this->data['id'] = $id;
		return view('porteria.form',$this->data);
	}

	public function getShow( Request $request, $id = null)
	{

		if($this->access['is_detail'] ==0)
		return Redirect::to('dashboard')
			->with('messagetext', \Lang::get('core.note_restric'))->with('msgstatus','error');

		$row = $this->model->getRow($id);
		if($row)
		{
			$this->data['row'] =  $row;
			$this->data['fields'] 		=  \SiteHelpers::fieldLang($this->info['config']['grid']);
			$this->data['id'] = $id;
			$this->data['access']		= $this->access;
			$this->data['subgrid']	= (isset($this->info['config']['subgrid']) ? $this->info['config']['subgrid'] : array());
			$this->data['fields'] =  \AjaxHelpers::fieldLang($this->info['config']['grid']);
			return view('porteria.view',$this->data);
		} else {
			return Redirect::to('porteria')->with('messagetext','Record Not Found !')->with('msgstatus','error');
		}
	}

	function postCopy( Request $request)
	{
	    foreach(\DB::select("SHOW COLUMNS FROM tbl_accesos ") as $column)
        {
			if( $column->Field != 'IdAcceso')
				$columns[] = $column->Field;
        }

		if(count($request->input('ids')) >=1)
		{
			$toCopy = implode(",",$request->input('ids'));
			$sql = "INSERT INTO tbl_accesos (".implode(",", $columns).") ";
			$sql .= " SELECT ".implode(",", $columns)." FROM tbl_accesos WHERE IdAcceso IN (".$toCopy.")";
			\DB::select($sql);
			return Redirect::to('porteria')->with('messagetext',\Lang::get('core.note_success'))->with('msgstatus','success');
		} else {

			return Redirect::to('porteria')->with('messagetext','Please select row to copy')->with('msgstatus','error');
		}

	}

	function postSave( Request $request)
	{

		$rules = $this->validateForm();
		$validator = Validator::make($request->all(), $rules);
		if ($validator->passes()) {
			$data = $this->validatePost('tb_porteria');

			$id = $this->model->insertRow($data , $request->input('IdAcceso'));

			if(!is_null($request->input('apply')))
			{
				$return = 'porteria/update/'.$id.'?return='.self::returnUrl();
			} else {
				$return = 'porteria?return='.self::returnUrl();
			}

			// Insert logs into database
			if($request->input('IdAcceso') =='')
			{
				\SiteHelpers::auditTrail( $request , 'New Data with ID '.$id.' Has been Inserted !');
			} else {
				\SiteHelpers::auditTrail($request ,'Data with ID '.$id.' Has been Updated !');
			}

			return Redirect::to($return)->with('messagetext',\Lang::get('core.note_success'))->with('msgstatus','success');

		} else {

			return Redirect::to('porteria/update/'. $request->input('IdAcceso'))->with('messagetext',\Lang::get('core.note_error'))->with('msgstatus','error')
			->withErrors($validator)->withInput();
		}

	}

	public function postDelete( Request $request)
	{

		if($this->access['is_remove'] ==0)
			return Redirect::to('dashboard')
				->with('messagetext', \Lang::get('core.note_restric'))->with('msgstatus','error');
		// delete multipe rows
		if(count($request->input('ids')) >=1)
		{
			$this->model->destroy($request->input('ids'));

			\SiteHelpers::auditTrail( $request , "ID : ".implode(",",$request->input('ids'))."  , Has Been Removed Successfull");
			// redirect
			return Redirect::to('porteria')
        		->with('messagetext', \Lang::get('core.note_success_delete'))->with('msgstatus','success');

		} else {
			return Redirect::to('porteria')
        		->with('messagetext','No Item Deleted')->with('msgstatus','error');
		}

	}

	public static function display( )
	{
		$mode  = isset($_GET['view']) ? 'view' : 'default' ;
		$model  = new Porteria();
		$info = $model::makeInfo('porteria');

		$data = array(
			'pageTitle'	=> 	$info['title'],
			'pageNote'	=>  $info['note']

		);

		if($mode == 'view')
		{
			$id = $_GET['view'];
			$row = $model::getRow($id);
			if($row)
			{
				$data['row'] =  $row;
				$data['fields'] 		=  \SiteHelpers::fieldLang($info['config']['grid']);
				$data['id'] = $id;
				return view('porteria.public.view',$data);
			}

		} else {

			$page = isset($_GET['page']) ? $_GET['page'] : 1;
			$params = array(
				'page'		=> $page ,
				'limit'		=>  (isset($_GET['rows']) ? filter_var($_GET['rows'],FILTER_VALIDATE_INT) : 10 ) ,
				'sort'		=> 'IdAcceso' ,
				'order'		=> 'asc',
				'params'	=> '',
				'global'	=> 1
			);

			$result = $model::getRows( $params );
			$data['tableGrid'] 	= $info['config']['grid'];
			$data['rowData'] 	= $result['rows'];

			$page = $page >= 1 && filter_var($page, FILTER_VALIDATE_INT) !== false ? $page : 1;
			$pagination = new Paginator($result['rows'], $result['total'], $params['limit']);
			$pagination->setPath('');
			$data['i']			= ($page * $params['limit'])- $params['limit'];
			$data['pagination'] = $pagination;
			return view('porteria.public.index',$data);
		}


	}

	function postSavepublic( Request $request)
	{

		$rules = $this->validateForm();
		$validator = Validator::make($request->all(), $rules);
		if ($validator->passes()) {
			$data = $this->validatePost('tbl_accesos');
			 $this->model->insertRow($data , $request->input('IdAcceso'));
			return  Redirect::back()->with('messagetext','<p class="alert alert-success">'.\Lang::get('core.note_success').'</p>')->with('msgstatus','success');
		} else {

			return  Redirect::back()->with('messagetext','<p class="alert alert-danger">'.\Lang::get('core.note_error').'</p>')->with('msgstatus','error')
			->withErrors($validator)->withInput();

		}

	}

		public  function postValidarrut(Request $request)
	{
		$rut = $request->rut;
		$area = $request->area;
		$actual = date("Y-m-d H:i:s");


		$users = \DB::table('tbl_personas')
			->select('tbl_personas.IdPersona','tbl_personas.Nombres','tbl_personas.Apellidos','tbl_personas.ArchivoFoto','tbl_accesos.IdEstatus as acceso','tbl_acceso_areas.IdEstatus as area','tbl_accesos.IdAcceso','tbl_accesos.contrato_id')
			->leftJoin('tbl_accesos', 'tbl_accesos.IdPersona', '=', 'tbl_personas.IdPersona')
        			->leftJoin('tbl_acceso_areas','tbl_accesos.IdAcceso', '=',  \DB::raw('tbl_acceso_areas.IdAcceso AND tbl_acceso_areas.IdAreaTrabajo = '.$area))
			->where('RUT', '=', $rut)
			->get();
			$acce = $users[0]->IdAcceso;
			$per = $users[0]->IdPersona;
			$cont = $users[0]->contrato_id;

			if ($users[0]->acceso!=1){
				$ban = 0;
				$ban1 = 0;
				$ban2 = 0;
				$ban3 = 0;
			$data = array();
				$docs = \DB::table('tbl_documentos')
				->join('tbl_tipos_documentos', 'tbl_documentos.IdTipoDocumento', '=', 'tbl_tipos_documentos.IdTipoDocumento')
				->leftJoin('tbl_accesos', 'tbl_documentos.contrato_id', '=', 'tbl_accesos.contrato_id')
				->select('tbl_tipos_documentos.Descripcion','tbl_documentos.FechaVencimiento','tbl_tipos_documentos.BloqueaAcceso','tbl_documentos.IdEstatus',
					\DB::raw('(CASE WHEN tbl_documentos.IdEstatus != "1" THEN "Estatus Incorrecto" ELSE 0 END) AS esta'),
					\DB::raw('(CASE WHEN tbl_documentos.FechaVencimiento <= NOW() THEN "Documento Vencido el : " ELSE 0 END) AS estaF'))
				->where('tbl_accesos.IdAcceso', '=', $acce)
				->where('tbl_documentos.IdEntidad', '=', $per)
				->get();
				if (sizeof($docs)>1){
				     for($i = 0; $i<count($docs); $i++){
				     	if ($docs[$i]->BloqueaAcceso=="SI"){
					     	if (($docs[$i]->IdEstatus!=1) || ($docs[$i]->FechaVencimiento<=$actual)){
							$time = strtotime($docs[$i]->FechaVencimiento);
							$docs[$i]->FechaVencimiento = date('d/m/Y',$time);
					     		array_push($data, $docs[$i]);
					     		$ban = 1;
					     	}
					    }
				     }
				}

				if ($ban==0){
					$docs = \DB::table('tbl_documentos')
					->join('tbl_tipos_documentos', 'tbl_documentos.IdTipoDocumento', '=', 'tbl_tipos_documentos.IdTipoDocumento')
					->select('tbl_tipos_documentos.Descripcion','tbl_documentos.FechaVencimiento','tbl_tipos_documentos.BloqueaAcceso', 'tbl_documentos.IdEstatus',
						\DB::raw('(CASE WHEN tbl_documentos.IdEstatus != "1" THEN "Estatus Incorrecto" ELSE 0 END) AS esta'),
						\DB::raw('(CASE WHEN tbl_documentos.FechaVencimiento <= NOW() THEN "Documento Vencido el : " ELSE 0 END) AS estaF'))
					->where('tbl_documentos.contrato_id', '=', $cont)
					->where('tbl_documentos.Entidad', '=', 2)
					->get();

					if (sizeof($docs)>1){
					     for($i = 0; $i<count($docs); $i++){
					     	if ($docs[$i]->BloqueaAcceso=="SI"){
						     	if (($docs[$i]->IdEstatus!=1) || ($docs[$i]->FechaVencimiento<=$actual)){
						     		$time = strtotime($docs[$i]->FechaVencimiento);
								$docs[$i]->FechaVencimiento = date('d/m/Y',$time);
						     		array_push($data, $docs[$i]);
						     		$ban1 = 1;
						     	}
						  }
					     }
					}

					if ($ban1==0){
						$docs = \DB::table('tbl_documentos')
						->join('tbl_tipos_documentos', 'tbl_documentos.IdTipoDocumento', '=', 'tbl_tipos_documentos.IdTipoDocumento')
						->select('tbl_tipos_documentos.Descripcion','tbl_documentos.FechaVencimiento','tbl_tipos_documentos.BloqueaAcceso','tbl_documentos.IdEstatus',
							\DB::raw('(CASE WHEN tbl_documentos.IdEstatus != "1" THEN "Estatus Incorrecto" ELSE 0 END) AS esta'),
							\DB::raw('(CASE WHEN tbl_documentos.FechaVencimiento <= NOW() THEN "Documento Vencido el : " ELSE 0 END) AS estaF'))
						->where('tbl_documentos.contrato_id', '=', $cont)
						->where('tbl_documentos.Entidad', '=', 1)
						->get();

						if (sizeof($docs)>1){
						     for($i = 0; $i<count($docs); $i++){
						     	if ($docs[$i]->BloqueaAcceso=="SI"){
							     	if (($docs[$i]->IdEstatus!=1) || ($docs[$i]->FechaVencimiento<=$actual)){
							     		$time = strtotime($docs[$i]->FechaVencimiento);
									$docs[$i]->FechaVencimiento = date('d/m/Y',$time);
							     		array_push($data, $docs[$i]);
							     		$ban2 = 1;
							     	}
							   }
						     }
						}

						if ($ban2==0){
							$docs = \DB::table('tbl_contrato')
							->select('contrato_id','cont_fechaFin','cont_estado',
								\DB::raw('(CASE WHEN cont_estado != "1" THEN "Estatus Incorrecto" ELSE 0 END) AS esta'),
								\DB::raw('(CASE WHEN cont_fechaFin <= NOW() THEN "Contrato Vencido el :" ELSE 0 END) AS estaF'))
							->where('contrato_id', '=', $cont)
							->get();

							if (sizeof($docs)>1){
							     for($i = 0; $i<count($docs); $i++){
							     	if (($docs[$i]->cont_estado!=1) || ($docs[$i]->cont_fechaFin<=$actual)){
							     		$time = strtotime($docs[$i]->cont_fechaFin);
									$docs[$i]->cont_fechaFin = date('d/m/Y',$time);
							     		array_push($data, $docs[$i]);
							     		$ban3 = 1;
							     	}
							     }
							}
							if ($ban3==0){
								$docs = \DB::table('tbl_contratistas')
								->join('tbl_contrato', 'tbl_contrato.IdContratista', '=', 'tbl_contratistas.IdContratista')
								->select('tbl_contratistas.IdContratista','tbl_contratistas.RazonSocial','tbl_contratistas.IdEstatus',
									\DB::raw('(CASE WHEN tbl_contratistas.IdEstatus != "1" THEN "Estatus Suspendido" ELSE 0 END) AS esta'))
								->where('tbl_contrato.contrato_id', '=', $cont)
								->get();
								if (sizeof($docs)>1){
								     for($i = 0; $i<count($docs); $i++){
								     	if ($docs[$i]->IdEstatus!=1 ){
								     		array_push($data, $docs[$i]);
								     	}
								     }
								}

							}


						}
					}

				}
			return response()->json(array(
			'status'=>'sucess',
			'valores'=>$users,
			'razon'=>$data,
			'message'=>\Lang::get('core.note_sucess')
			));

			}
			else{
				return response()->json(array(
				'status'=>'sucess',
				'valores'=>$users,
				'message'=>\Lang::get('core.note_sucess')
				));
			}

	}

		public  function postCompruebarut(Request $request)
	{
		$rut = $request->rut;

		$personas = \DB::table('tbl_personas')
		->select('Nombres','Apellidos','ArchivoFoto','RUT')
		->where('RUT', '=', $rut)
		->get();
		return response()->json(array(
			'status'=>'sucess',
			'valores'=>$personas,
			'message'=>\Lang::get('core.note_sucess')
			));
	}
}
