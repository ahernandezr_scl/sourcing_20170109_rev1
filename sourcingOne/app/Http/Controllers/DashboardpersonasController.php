<?php namespace App\Http\Controllers;

use App\Http\Controllers\controller;
use App\Models\Dashboardpersonas;
use Illuminate\Http\Request;
use Illuminate\Pagination\LengthAwarePaginator as Paginator;
use Validator, Input, Redirect ; 
use DB;


class DashboardpersonasController extends Controller {

	protected $layout = "layouts.main";
	protected $data = array();	
	public $module = 'dashboardpersonas';
	static $per_page	= '10';

	public function __construct()
	{
		
		$this->beforeFilter('csrf', array('on'=>'post'));
		$this->model = new Dashboardpersonas();
		$this->info = $this->model->makeInfo( $this->module);
		$this->access = $this->model->validAccess($this->info['id']);
	
		$this->data = array(
			'pageTitle'	=> 	$this->info['title'],
			'pageNote'	=>  $this->info['note'],
			'pageModule'=> 'dashboardpersonas',
			'return'	=> self::returnUrl()
			
		);
		\App::setLocale(CNF_LANG);
		if (defined('CNF_MULTILANG') && CNF_MULTILANG == '1') {

		$lang = (\Session::get('lang') != "" ? \Session::get('lang') : CNF_LANG);
		\App::setLocale($lang);
		}  
		
		
	}

	public function getIndex( Request $request )
	{

		if($this->access['is_view'] ==0) 
			return Redirect::to('dashboard')
				->with('messagetext', \Lang::get('core.note_restric'))->with('msgstatus','error');

			$data1 = DB::select(DB::raw('SELECT COUNT(DISTINCT tbl_tipos_documentos.IdTipoDocumento) total_enc,
					COUNT(DISTINCT tbl_tipo_documento_valor.IdTipoDocumentoValor) total_preg,
					COUNT(DISTINCT CASE WHEN tbl_documento_valor.entry_by_access IS NOT NULL THEN tbl_tipo_documento_valor.IdTipoDocumentoValor END) cont_preg
				FROM tbl_documentos
				LEFT JOIN tbl_tipos_documentos ON tbl_documentos.IdTipoDocumento LIKE tbl_tipos_documentos.IdTipoDocumento
				LEFT JOIN tbl_tipo_documento_valor ON tbl_tipos_documentos.IdTipoDocumento LIKE tbl_tipo_documento_valor.IdTipoDocumento
				LEFT JOIN tbl_documento_valor ON tbl_documento_valor.IdTipoDocumentoValor LIKE tbl_tipo_documento_valor.IdTipoDocumentoValor
				LEFT JOIN tb_users ON tb_users.id LIKE tbl_documento_valor.entry_by_access
				WHERE tbl_tipos_documentos.Descripcion LIKE "%encuesta%"'));

				$data2 = DB::select(DB::raw('SELECT Descripcion,
					COUNT(tbl_tipo_documento_valor.IdTipoDocumento) cant_enc,
					COUNT(DISTINCT CASE WHEN tbl_documento_valor.entry_by_access IS NOT NULL THEN tbl_tipo_documento_valor.IdTipoDocumentoValor END) enc_cont
				FROM tbl_documentos
				LEFT JOIN tbl_tipos_documentos ON tbl_documentos.IdTipoDocumento LIKE tbl_tipos_documentos.IdTipoDocumento
				LEFT JOIN tbl_tipo_documento_valor ON tbl_tipos_documentos.IdTipoDocumento LIKE tbl_tipo_documento_valor.IdTipoDocumento
				LEFT JOIN tbl_documento_valor ON tbl_documento_valor.IdTipoDocumentoValor LIKE tbl_tipo_documento_valor.IdTipoDocumentoValor
				LEFT JOIN tb_users ON tb_users.id LIKE tbl_documento_valor.entry_by_access
				WHERE tbl_tipos_documentos.Descripcion LIKE "%encuesta%"
				GROUP BY Descripcion, tbl_documento_valor.entry_by_access'));

				$personas = DB::select(DB::raw("SELECT COUNT(a.IdPersona) AS total,
																								COUNT(b.IdPersona) AS personasContrato,
																								COUNT(c.IdPersona) AS personasAcceso,
																								COUNT(d.IdPersona) AS personasAnotacion,
																								SUM(CASE WHEN a.Sexo = 1 THEN 1 ELSE 0 END) AS personasM,
																								SUM(CASE WHEN a.Sexo = 2 THEN 1 ELSE 0 END) AS personasF,
																								SUM(CASE WHEN DATE_FORMAT(NOW(), '%Y') - DATE_FORMAT(a.fechaNacimiento, '%Y') - (DATE_FORMAT(NOW(), '00-%m-%d') < DATE_FORMAT(a.fechaNacimiento, '00-%m-%d')) < 18 THEN 1 ELSE 0 END) AS edad0,
																								SUM(CASE WHEN DATE_FORMAT(NOW(), '%Y') - DATE_FORMAT(a.fechaNacimiento, '%Y') - (DATE_FORMAT(NOW(), '00-%m-%d') < DATE_FORMAT(a.fechaNacimiento, '00-%m-%d')) > 17 AND DATE_FORMAT(NOW(), '%Y') - DATE_FORMAT(fechaNacimiento, '%Y') - (DATE_FORMAT(NOW(), '00-%m-%d') < DATE_FORMAT(fechaNacimiento, '00-%m-%d')) < 30 THEN 1 ELSE 0 END) AS edad1,
																								SUM(CASE WHEN DATE_FORMAT(NOW(), '%Y') - DATE_FORMAT(a.fechaNacimiento, '%Y') - (DATE_FORMAT(NOW(), '00-%m-%d') < DATE_FORMAT(a.fechaNacimiento, '00-%m-%d')) > 29 AND DATE_FORMAT(NOW(), '%Y') - DATE_FORMAT(fechaNacimiento, '%Y') - (DATE_FORMAT(NOW(), '00-%m-%d') < DATE_FORMAT(fechaNacimiento, '00-%m-%d')) < 40 THEN 1 ELSE 0 END) AS edad2,
																								SUM(CASE WHEN DATE_FORMAT(NOW(), '%Y') - DATE_FORMAT(a.fechaNacimiento, '%Y') - (DATE_FORMAT(NOW(), '00-%m-%d') < DATE_FORMAT(a.fechaNacimiento, '00-%m-%d')) > 39 AND DATE_FORMAT(NOW(), '%Y') - DATE_FORMAT(fechaNacimiento, '%Y') - (DATE_FORMAT(NOW(), '00-%m-%d') < DATE_FORMAT(fechaNacimiento, '00-%m-%d')) < 50 THEN 1 ELSE 0 END) AS edad3,
																								SUM(CASE WHEN DATE_FORMAT(NOW(), '%Y') - DATE_FORMAT(a.fechaNacimiento, '%Y') - (DATE_FORMAT(NOW(), '00-%m-%d') < DATE_FORMAT(a.fechaNacimiento, '00-%m-%d')) > 49 AND DATE_FORMAT(NOW(), '%Y') - DATE_FORMAT(fechaNacimiento, '%Y') - (DATE_FORMAT(NOW(), '00-%m-%d') < DATE_FORMAT(fechaNacimiento, '00-%m-%d')) < 60 THEN 1 ELSE 0 END) AS edad4,
																								SUM(CASE WHEN DATE_FORMAT(NOW(), '%Y') - DATE_FORMAT(a.fechaNacimiento, '%Y') - (DATE_FORMAT(NOW(), '00-%m-%d') < DATE_FORMAT(a.fechaNacimiento, '00-%m-%d')) > 59 THEN 1 ELSE 0 END) AS edad5
																							FROM tbl_personas AS a
																							LEFT JOIN tbl_contratos_personas AS b ON a.IdPersona LIKE b.IdPersona
																							LEFT JOIN tbl_accesos AS c ON a.IdPersona LIKE c.IdPersona
																							LEFT JOIN tbl_anotaciones AS d ON a.IdPersona LIKE d.IdPersona"));

				$aux = array();
				$data = array();
				$aux['total_enc'] = $data1[0]->total_enc;
				$aux['total_preg'] = $data1[0]->total_preg;
				$aux['cont_preg'] = $data1[0]->cont_preg;
				foreach($data2 as $encuesta){
					$aux[$encuesta->Descripcion." cant_enc"] = $encuesta->cant_enc;
					$aux[$encuesta->Descripcion." enc_cont"] = $encuesta->enc_cont;
				}
				array_push($data, $aux);
				
		return view('dashboardpersonas.index',$this->data)->with('data', json_encode($data))->with('personas', $personas);
	}	



	function getUpdate(Request $request, $id = null)
	{
	
		if($id =='')
		{
			if($this->access['is_add'] ==0 )
			return Redirect::to('dashboard')->with('messagetext',\Lang::get('core.note_restric'))->with('msgstatus','error');
		}	
		
		if($id !='')
		{
			if($this->access['is_edit'] ==0 )
			return Redirect::to('dashboard')->with('messagetext',\Lang::get('core.note_restric'))->with('msgstatus','error');
		}				
				
		$this->data['access']		= $this->access;
		return view('dashboardpersonas.form',$this->data);
	}	

	public function getShow( $id = null)
	{
	
		if($this->access['is_detail'] ==0) 
			return Redirect::to('dashboard')
				->with('messagetext', \Lang::get('core.note_restric'))->with('msgstatus','error');
					
		
		$this->data['access']		= $this->access;
		return view('dashboardpersonas.view',$this->data);	
	}	

	function postSave( Request $request)
	{
		
	
	}	

	public function postDelete( Request $request)
	{
		
		if($this->access['is_remove'] ==0) 
			return Redirect::to('dashboard')
				->with('messagetext', \Lang::get('core.note_restric'))->with('msgstatus','error');
		
	}			


}