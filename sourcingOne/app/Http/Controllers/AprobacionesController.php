<?php namespace App\Http\Controllers;

use App\Http\Controllers\controller;
use App\Models\Aprobaciones;
use Illuminate\Http\Request;
use Illuminate\Pagination\LengthAwarePaginator as Paginator;
use Validator, Input, Redirect ;

class AprobacionesController extends Controller {

	protected $layout = "layouts.main";
	protected $data = array();
	public $module = 'aprobaciones';
	static $per_page	= '10';

	public function __construct()
	{
		parent::__construct();
		$this->model = new Aprobaciones();

		$this->info = $this->model->makeInfo( $this->module);
		$this->access = $this->model->validAccess($this->info['id']);

		$this->data = array(
			'pageTitle'			=> 	$this->info['title'],
			'pageNote'			=>  $this->info['note'],
			'pageModule'		=> 'aprobaciones',
			'pageUrl'			=>  url('aprobaciones'),
			'return' 			=> 	self::returnUrl()
		);

	}

	public function getIndex()
	{
		if($this->access['is_view'] ==0)
			return Redirect::to('dashboard')->with('messagetext',\Lang::get('core.note_restric'))->with('msgstatus','error');

		$this->data['access']		= $this->access;
		return view('aprobaciones.index',$this->data);
	}

	public function getShowlist( Request $request)
	{
		$sort = (!is_null($request->input('sort')) ? $request->input('sort') : $this->info['setting']['orderby']);
		$order = (!is_null($request->input('order')) ? $request->input('order') : $this->info['setting']['ordertype']);
		$filter = " ";
		$lintIdEstatus = 0;
		if (!is_null($request->input('IdEstatus'))){
			$lintIdEstatus=$request->input('IdEstatus');
		}else{
			$lintIdEstatus=2;
		}
		if($lintIdEstatus!=6) {
		  $filter = " AND tbl_documentos.IdEstatus=".$lintIdEstatus;
		}
		if(!is_null($request->input('search')))
		{
			$search = 	$this->buildSearch('maps');
			$filter = $search['param'];
			$this->data['search_map'] = $search['maps'];
		}

		$page = $request->input('page', 1);
		$params = array(
			'page'		=> $page ,
			'limit'		=> (!is_null($request->input('rows')) ? filter_var($request->input('rows'),FILTER_VALIDATE_INT) : $this->info['setting']['perpage'] ) ,
			'Estatus'	=> $lintIdEstatus,
			'sort'		=> $sort ,
			'order'		=> $order,
			'params'	=> $filter,
			'global'	=> (isset($this->access['is_global']) ? $this->access['is_global'] : 0 )
		);
		// Get Query
		$results = $this->model->getRows( $params );
		
		$larrResult = array();
		$larrResultTemp = array();
		$i = 0;

		foreach ($results['rows'] as $row) {

			$id = $row->IdDocumento;
			$larrResultTemp = array('id'=> ++$i,
								    'checkbox'=>'<input type="checkbox" class="ids" name="ids[]" value="'.$id.'" /> '
								    );
			foreach ($this->info['config']['grid'] as $field) {
				if($field['view'] =='1') {
					$limited = isset($field['limited']) ? $field['limited'] :'';
					if (\SiteHelpers::filterColumn($limited )){
						$value = \SiteHelpers::formatRows($row->{$field['field']}, $field , $row);
						$larrResultTemp[$field['field']] = $value;
					}
				}
			}
			//$larrResultTemp['action'] = \AjaxHelpers::buttonAction('documentos',$this->access,$id ,$this->info['setting']).\AjaxHelpers::buttonActionInline($id,'IdDocumento');
			$larrResultTemp['action'] = '
					<div class=" action dropup" >
					  <a href="#" onclick="ajaxApproveInLine(\''.$id.'\',5); return false;"  class="btn btn-xs btn-white tips" title=" Aprobar "><i class="fa  fa-check"></i></a>
					  <a href="#" onclick="ajaxApproveInLine(\''.$id.'\',4); return false;"  class="btn btn-xs btn-white tips" title=" Temporal "><i class="fa  fa-clock-o"></i></a>
                      <a href="javascript://ajax" onclick="valores(\''.$id.'\',3); return false;"   class="btn btn-xs btn-white tips" title=" Rechazar"><i class="fa fa-ban"></i></a>
		            </div>';
			$larrResult[] = $larrResultTemp;
		}

		echo json_encode(array("data"=>$larrResult));


	}

	public function postData( Request $request)
	{
		$params = array(
			'page'		=> '',
			'limit'		=> '',
			'Estatus'	=> (is_null($request->input('Estatus')) ? 2 : $request->input('Estatus') ),
			'sort'		=> '',
			'order'		=> '',
			'params'	=> '',
			'global'	=> (isset($this->access['is_global']) ? $this->access['is_global'] : 0 )
		);
		$this->data['param']		= $params;
		$this->data['rowData']		= array();
		$this->data['i']			= 0;
		$this->data['tableGrid'] 	= $this->info['config']['grid'];
		$this->data['tableForm'] 	= $this->info['config']['forms'];
		$this->data['colspan'] 		= \SiteHelpers::viewColSpan($this->info['config']['grid']);
		$this->data['access']		= $this->access;
		$this->data['setting'] 		= $this->info['setting'];
		return view('aprobaciones.table',$this->data);
	}


	function getUpdate(Request $request, $id = null)
	{

		if($id =='')
		{
			if($this->access['is_add'] ==0 )
			return Redirect::to('dashboard')->with('messagetext',\Lang::get('core.note_restric'))->with('msgstatus','error');
		}

		if($id !='')
		{
			if($this->access['is_edit'] ==0 )
			return Redirect::to('dashboard')->with('messagetext',\Lang::get('core.note_restric'))->with('msgstatus','error');
		}

		$row = $this->model->find($id);
		if($row)
		{
			$this->data['row'] 		=  $row;
		} else {
			$this->data['row'] 		= $this->model->getColumnTable('tbl_documentos');
		}
		$this->data['setting'] 		= $this->info['setting'];
		$this->data['fields'] 		=  \AjaxHelpers::fieldLang($this->info['config']['forms']);

		$this->data['id'] = $id;

		return view('aprobaciones.form',$this->data);
	}

	public function getShow( $id = null)
	{

		if($this->access['is_detail'] ==0)
			return Redirect::to('dashboard')
				->with('messagetext', \Lang::get('core.note_restric'))->with('msgstatus','error');

		$row = $this->model->getRow($id);
		if($row)
		{
			$this->data['row'] =  $row;

			$this->data['id'] = $id;
			$this->data['access']		= $this->access;
			$this->data['setting'] 		= $this->info['setting'];
			$this->data['fields'] 		= \AjaxHelpers::fieldLang($this->info['config']['grid']);
			$this->data['subgrid']		= (isset($this->info['config']['subgrid']) ? $this->info['config']['subgrid'] : array());
			return view('aprobaciones.view',$this->data);

		} else {

			return response()->json(array(
				'status'=>'error',
				'message'=> \Lang::get('core.note_error')
			));
		}
	}


	function postCopy( Request $request)
	{

	    foreach(\DB::select("SHOW COLUMNS FROM tbl_documentos ") as $column)
        {
			if( $column->Field != 'IdDocumento')
				$columns[] = $column->Field;
        }
		if(count($request->input('ids')) >=1)
		{

			$toCopy = implode(",",$request->input('ids'));


			$sql = "INSERT INTO tbl_documentos (".implode(",", $columns).") ";
			$sql .= " SELECT ".implode(",", $columns)." FROM tbl_documentos WHERE IdDocumento IN (".$toCopy.")";
			\DB::insert($sql);
			return response()->json(array(
				'status'=>'success',
				'message'=> \Lang::get('core.note_success')
			));

		} else {
			return response()->json(array(
				'status'=>'success',
				'message'=> 'Please select row to copy'
			));
		}


	}

	function postSave( Request $request, $id =0)
	{

		$rules = $this->validateForm();
		$validator = Validator::make($request->all(), $rules);
		if ($validator->passes()) {
			$data = $this->validatePost('tbl_documentos');

			$id = $this->model->insertRow($data , $request->input('IdDocumento'));

			return response()->json(array(
				'status'=>'success',
				'message'=> \Lang::get('core.note_success')
				));

		} else {

			$message = $this->validateListError(  $validator->getMessageBag()->toArray() );
			return response()->json(array(
				'message'	=> $message,
				'status'	=> 'error'
			));
		}

	}

	public function postApprove( Request $request)
	{

		if($this->access['is_edit'] ==0) {
			return response()->json(array(
				'status'=>'error',
				'message'=> \Lang::get('core.note_restric')
			));
			die;

		}
		// delete multipe rows
		if(count($request->input('ids')) >=1)
		{

			if (!$request->input('oneline')) {
			  $larrApprove = $request->input('ids');
			  //$toApprove = implode(",",$larrApprove);
			  $toApprove = str_replace("ids%5B%5D="," ",$larrApprove);
			  $toApprove = str_replace("&",",",$toApprove);
			}else{
				$toApprove = $request->input('ids');
			}
                            	$result = $request->input('result');
			$lintIdEstatus = $request->input('status');
			if ($lintIdEstatus==3)
				$sql = "UPDATE tbl_documentos SET IdEstatus = '.$lintIdEstatus.', Resultado = '$result'  WHERE IdDocumento IN (".$toApprove.")";
			else
				$sql = "UPDATE tbl_documentos SET IdEstatus = ".$lintIdEstatus." WHERE IdDocumento IN (".$toApprove.")";
			\DB::update($sql);
			return response()->json(array(
				'status'=>'success',
				'message'=> \Lang::get('core.note_success')
			));

		} else {
			return response()->json(array(
				'status'=>'error',
				'message'=> \Lang::get('core.note_error')
			));

		}

	}

	public function postDelete( Request $request)
	{

		if($this->access['is_remove'] ==0) {
			return response()->json(array(
				'status'=>'error',
				'message'=> \Lang::get('core.note_restric')
			));
			die;

		}
		// delete multipe rows
		if(count($request->input('ids')) >=1)
		{
			$this->model->destroy($request->input('ids'));

			return response()->json(array(
				'status'=>'success',
				'message'=> \Lang::get('core.note_success_delete')
			));
		} else {
			return response()->json(array(
				'status'=>'error',
				'message'=> \Lang::get('core.note_error')
			));

		}

	}

	public static function display( )
	{
		$mode  = isset($_GET['view']) ? 'view' : 'default' ;
		$model  = new Aprobaciones();
		$info = $model::makeInfo('aprobaciones');

		$data = array(
			'pageTitle'	=> 	$info['title'],
			'pageNote'	=>  $info['note']

		);

		if($mode == 'view')
		{
			$id = $_GET['view'];
			$row = $model::getRow($id);
			if($row)
			{
				$data['row'] =  $row;
				$data['fields'] 		=  \SiteHelpers::fieldLang($info['config']['grid']);
				$data['id'] = $id;
				return view('aprobaciones.public.view',$data);
			}

		} else {

			$page = isset($_GET['page']) ? $_GET['page'] : 1;
			$params = array(
				'page'		=> $page ,
				'limit'		=>  (isset($_GET['rows']) ? filter_var($_GET['rows'],FILTER_VALIDATE_INT) : 10 ) ,
				'sort'		=> 'IdDocumento' ,
				'order'		=> 'asc',
				'params'	=> '',
				'global'	=> 1
			);

			$result = $model::getRows( $params );
			$data['tableGrid'] 	= $info['config']['grid'];
			$data['rowData'] 	= $result['rows'];

			$page = $page >= 1 && filter_var($page, FILTER_VALIDATE_INT) !== false ? $page : 1;
			$pagination = new Paginator($result['rows'], $result['total'], $params['limit']);
			$pagination->setPath('');
			$data['i']			= ($page * $params['limit'])- $params['limit'];
			$data['pagination'] = $pagination;
			return view('aprobaciones.public.index',$data);
		}


	}

	function postSavepublic( Request $request)
	{

		$rules = $this->validateForm();
		$validator = Validator::make($request->all(), $rules);
		if ($validator->passes()) {
			$data = $this->validatePost('tbl_documentos');
			 $this->model->insertRow($data , $request->input('IdDocumento'));
			return  Redirect::back()->with('messagetext','<p class="alert alert-success">'.\Lang::get('core.note_success').'</p>')->with('msgstatus','success');
		} else {

			return  Redirect::back()->with('messagetext','<p class="alert alert-danger">'.\Lang::get('core.note_error').'</p>')->with('msgstatus','error')
			->withErrors($validator)->withInput();

		}

	}

	function postInfoadicional( Request $request) {
      
      $lintIdDocumento = $request->input('iddocumento');
      $lstrQuery = " SELECT tbl_tipo_documento_valor.*, IFNULL(tbl_tipo_documento_data.Display,tbl_documento_valor.Valor) as Valor, tbl_tipos_documentos.Tipo
                     FROM tbl_documentos 
                     INNER JOIN tbl_documento_valor ON tbl_documentos.IdDocumento = tbl_documento_valor.IdDocumento
                     INNER JOIN tbl_tipo_documento_valor ON tbl_tipo_documento_valor.IdTipoDocumentoValor = tbl_documento_valor.IdTipoDocumentoValor
                     LEFT JOIN tbl_tipo_documento_data ON tbl_tipo_documento_data.IdTipoDocumentoValor = tbl_documento_valor.IdTipoDocumentoValor AND tbl_tipo_documento_data.Valor = tbl_documento_valor.Valor
                     INNER JOIN tbl_tipos_documentos ON tbl_tipos_documentos.IdTipoDocumento = tbl_tipo_documento_valor.IdTipoDocumento
                     WHERE tbl_documentos.IdDocumento = ".$lintIdDocumento."";
	  $lobjData = \DB::select($lstrQuery);
      $lstrResult = "";

      if ($lobjData){
        $lstrResult = '<hr />';
	    $lstrResult .= '<div class="clr clear"></div>';
	    $lstrResult .= '<h5> Datos Adicionales  </h5>';

        foreach ($lobjData as $p) {
  				
  				$lstrResult .= '<div class="form-group " >';
  				$lstrResult .= '<label for="'.$p->Etiqueta.'" class=" control-label col-md-4 text-left">'.$p->Etiqueta.'</span></label>';
				$lstrResult .= '<div class="col-md-6">';
				$lstrResult .= '<div class="input-group m-b" style="width:150px !important;">';
			    $lstrResult .= '<span>'.$p->Valor.'</span>';
				$lstrResult .= '</div>';
				$lstrResult .= '</div>';
				$lstrResult .= '<div class="col-md-2"></div>';
				$lstrResult .= '</div>';
  	    }
  	    $lstrResult .= '<hr />';
		$lstrResult .= '<div style="clear:both"></div>';
      }

      echo $lstrResult;
	}

	function postInfotipo( Request $request) {
      
      $lintIdDocumento = $request->input('iddocumento');
      $lstrQuery = " SELECT tbl_tipos_documentos.Tipo
                     FROM tbl_documentos
                     INNER JOIN tbl_tipos_documentos ON tbl_tipos_documentos.IdTipoDocumento = tbl_documentos.IdTipoDocumento
                     WHERE tbl_documentos.IdDocumento = ".$lintIdDocumento."";
	  $lobjData = \DB::select($lstrQuery);

      if ($lobjData){
        foreach ($lobjData as $p) {
        		$lintIdTipo = $p->Tipo;
  				echo json_encode( array("tipo" => $lintIdTipo));
  	    }
      }
	}


}
