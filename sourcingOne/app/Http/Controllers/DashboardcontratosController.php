<?php namespace App\Http\Controllers;

use App\Http\Controllers\controller;
use App\Models\Contratos;
use Illuminate\Http\Request;
use Illuminate\Pagination\LengthAwarePaginator as Paginator;
use Validator, Input, Redirect ;
use DB;

class DashboardcontratosController extends Controller {

	protected $layout = "layouts.main";
	protected $data = array();
	public $module = 'dashboardcontratos';
	static $per_page	= '10';

	public function __construct()
	{

		$this->beforeFilter('csrf', array('on'=>'post'));
		$this->model = new Contratos();
		$this->info = $this->model->makeInfo( $this->module);
		$this->access = $this->model->validAccess($this->info['id']);

		$this->data = array(
			'pageTitle'	=> 	$this->info['title'],
			'pageNote'	=>  $this->info['note'],
			'pageModule'=> 'dashboardcontratos',
			'return'	=> self::returnUrl()

		);
		\App::setLocale(CNF_LANG);
		if (defined('CNF_MULTILANG') && CNF_MULTILANG == '1') {

		$lang = (\Session::get('lang') != "" ? \Session::get('lang') : CNF_LANG);
		\App::setLocale($lang);
		}


	}

	public function getIndex( Request $request )
	{

		if($this->access['is_view'] ==0)
			return Redirect::to('dashboard')
				->with('messagetext', \Lang::get('core.note_restric'))->with('msgstatus','error');

				$resultados = array();

				$cont = Contratos::all();
				$jsContratos = Contratos::all()->toJson();
				$gastos = DB::table('tbl_contrato_tipogasto')->get();
				$extensiones = DB::table('tbl_contrato_extension')->get();
				$personas = DB::table('tbl_contratos_personas')->get();

				$contratos = array(
					'cantidad' => Contratos::count(),
					'total' => Contratos::sum('cont_montoTotal'),
					'vigentes' => Contratos::where('cont_fechaFin','>=',date('Y-m-d'))->count(),
					'personas' => Contratos::join('tbl_contratos_personas','tbl_contrato.contrato_id','=','tbl_contratos_personas.contrato_id')->count(),
					'tipogasto' => array(),
					'extension' => array()
				);

				$i = 0;
				foreach($extensiones as $extension){
					if(!array_key_exists($extension->nombre, $contratos['extension'])){
						$contratos['extension'][] = array(
							'id' => $i,
							'nombre' => $extension->nombre,
							'tag' => $extension->tag,
							'cantidad' => 0,
							'valor' => 0,
							'gastos' => array()
						);
						$j = 0;
					foreach($gastos as $gasto){
						if(!array_key_exists($gasto->nombre_gasto, $contratos['extension'][$i]['gastos'])){
							$contratos['extension'][$i]['gastos'][] = array(
								'id' => $j,
								'nombre' => $gasto->nombre_gasto,
								'tag' => $gasto->tag,
								'cantidad' => 0,
								'valor' => 0
							);
							$j++;
						}
					}
					$i++;
				}
			}

			$i = 0;
			foreach($gastos as $gasto){
				if(!array_key_exists($gasto->nombre_gasto, $contratos['tipogasto'])){
					$contratos['tipogasto'][] = array(
						'id' => $i,
						'nombre' => $gasto->nombre_gasto,
						'tag' => $gasto->tag,
						'cantidad' => 0,
						'valor' => 0,
						'extensiones' => array()
					);
					$j = 0;
				foreach($extensiones as $extension){
					if(!array_key_exists($extension->nombre, $contratos['tipogasto'][$i]['extensiones'])){
						$contratos['tipogasto'][$i]['extensiones'][] = array(
							'id' => $j,
							'nombre' => $extension->nombre,
							'tag' => $extension->tag,
							'cantidad' => 0,
							'valor' => 0
						);
						$j++;
					}
				}
				$i++;
			}
		}

		for($i = 0; $i < count($cont); $i++){


			$aux1 = (int)$cont[$i]['id_tipogasto'];
			$aux2 = (int)$cont[$i]['id_extension'];
			$contratos['tipogasto'][$aux1-1]['extensiones'][$aux2-1]['cantidad'] += 1;
			$contratos['tipogasto'][$aux1-1]['extensiones'][$aux2-1]['valor'] += $cont[$i]['cont_montoTotal'];
			$contratos['extension'][$aux2-1]['gastos'][$aux1-1]['cantidad'] += 1;
			$contratos['extension'][$aux2-1]['gastos'][$aux1-1]['valor'] += $cont[$i]['cont_montoTotal'];
			$contratos['tipogasto'][$aux1-1]['cantidad'] += 1;
			$contratos['tipogasto'][$aux1-1]['valor'] += $cont[$i]['cont_montoTotal'];
			$contratos['extension'][$aux2-1]['cantidad'] += 1;
			$contratos['extension'][$aux2-1]['valor'] += $cont[$i]['cont_montoTotal'];
		}

				$jsContratos = Contratos::join('tbl_contrato_tipogasto', 'tbl_contrato.id_tipogasto', '=', 'tbl_contrato_tipogasto.id_tipogasto')
														->join('tbl_contrato_extension', 'tbl_contrato.id_extension', '=', 'tbl_contrato_extension.id_extension')
														->select('tbl_contrato.contrato_id', 'tbl_contrato.cont_nombre', 'tbl_contrato.cont_proveedor', 'tbl_contrato.cont_montoTotal', 'tbl_contrato.impacto', 'tbl_contrato.complejidad', 'tbl_contrato_tipogasto.*', 'tbl_contrato_extension.*')
														->get();

				$countall = array();
				foreach ($extensiones as $extension) {
					foreach ($gastos as $gasto) {
						$count = array();
						$count[] = $gasto->nombre_gasto;
						$count[] = $extension->nombre;
						$count[] = Contratos::where('id_tipogasto', $gasto->id_tipogasto)->where('id_extension', $extension->id_extension)->count();
						$count[] = Contratos::where('id_tipogasto', $gasto->id_tipogasto)->where('id_extension', $extension->id_extension)->sum('cont_montoTotal');
						$countall[] = $count;
					}
				}
				$jscountall = json_encode($countall);

				return view('dashboardcontratos.index',$this->data)->with('contratos', $contratos)->with('jsContratos', $jsContratos)->with('countAll', $countall)->with('tipoGastos', $gastos)->with('tipoExtensiones', $extensiones)->with('jsCountAll', $jscountall);
	}



	function getUpdate(Request $request, $id = null)
	{

		if($id =='')
		{
			if($this->access['is_add'] ==0 )
			return Redirect::to('dashboard')->with('messagetext',\Lang::get('core.note_restric'))->with('msgstatus','error');
		}

		if($id !='')
		{
			if($this->access['is_edit'] ==0 )
			return Redirect::to('dashboard')->with('messagetext',\Lang::get('core.note_restric'))->with('msgstatus','error');
		}

		$this->data['access']		= $this->access;
		return view('dashboardcontratos.form',$this->data);
	}

	public function getShow( $id = null)
	{

		if($this->access['is_detail'] ==0)
			return Redirect::to('dashboard')
				->with('messagetext', \Lang::get('core.note_restric'))->with('msgstatus','error');


		$this->data['access']		= $this->access;
		return view('dashboardcontratos.view',$this->data);
	}

	function postSave( Request $request)
	{


	}

	public function postDelete( Request $request)
	{

		if($this->access['is_remove'] ==0)
			return Redirect::to('dashboard')
				->with('messagetext', \Lang::get('core.note_restric'))->with('msgstatus','error');

	}


}
