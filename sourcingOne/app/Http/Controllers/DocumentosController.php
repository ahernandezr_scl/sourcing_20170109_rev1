<?php namespace App\Http\Controllers;

use App\Http\Controllers\controller;
use App\Models\Documentos;
use App\Models\Cargaf;
use Illuminate\Http\Request;
use Illuminate\Pagination\LengthAwarePaginator as Paginator;
use Validator, Input, Redirect ;

class DocumentosController extends Controller {

	protected $layout = "layouts.main";
	protected $data = array();
	public $module = 'documentos';
	static $per_page	= '10';

	public function __construct()
	{
		parent::__construct();
		$this->model = new Documentos();
		$this->modelupload = new Cargaf();
		$this->modelview = new  \App\Models\Documentovalor();
		$this->info = $this->model->makeInfo( $this->module);
		$this->access = $this->model->validAccess($this->info['id']);

		$this->data = array(
			'pageTitle'			=> 	$this->info['title'],
			'pageNote'			=>  $this->info['note'],
			'pageModule'		=> 'documentos',
			'pageUrl'			=>  url('documentos'),
			'return' 			=> 	self::returnUrl()
		);

	}

	public function getIndex()
	{
		if($this->access['is_view'] ==0)
			return Redirect::to('dashboard')->with('messagetext',\Lang::get('core.note_restric'))->with('msgstatus','error');

		$this->data['access']		= $this->access;
		return view('documentos.index',$this->data);
	}

	public function getShowlist( Request $request){

		$lintIdUser = \Session::get('uid');
		$lintGroupUser = \MySourcing::GroupUser($lintIdUser);
		$lintLevelUser = \MySourcing::LevelUser($lintIdUser);
		$sort = (!is_null($request->input('sort')) ? $request->input('sort') : $this->info['setting']['orderby']);
		$order = (!is_null($request->input('order')) ? $request->input('order') : $this->info['setting']['ordertype']);
		if ($lintGroupUser>1){
			//Aplicamos un filro para el control de los perfiles
			$filter = " AND tbl_tipos_documentos.group_id = '".$lintGroupUser."' ";
		}else{
			$filter = " ";
		}
        if ($lintLevelUser==4){
          //Aplicamos un filtro especial para solo los contratos relaciados a ese administrador
          $filter .= " AND tbl_documentos.contrato_id IN (select tbl_contrato.contrato_id from tbl_contrato where tbl_contrato.admin_id = ".$lintIdUser.") ";
        }
       	$lintIdEstatus = 1;
		if (!is_null($request->input('IdEstatus'))){
			$lintIdEstatus=$request->input('IdEstatus');
		}
		if($lintIdEstatus==6){
		  $filter .= " ";
		}else{
		  $filter .= " AND tbl_documentos.IdEstatus='".$request->input('IdEstatus')."' ";
		}
		$filter .= " AND tbl_documentos.Entidad != 7 ";
		#echo $filter;
		if(!is_null($request->input('search')))
		{
			$search = 	$this->buildSearch('maps');
			$filter = $search['param'];
			$this->data['search_map'] = $search['maps'];
		}
		$page = $request->input('page', 1);
		$params = array(
			'page'		=> $page ,
			'sort'		=> $sort ,
			'IdEstatus'	=>$lintIdEstatus,
			'order'		=> $order,
			'params'	=> $filter,
			'global'	=> (isset($this->access['is_global']) ? $this->access['is_global'] : 0 )
		);
		// Get Query
		$results = $this->model->getRows( $params );
		#var_dump($results);
		$larrResult = array();
		$larrResultTemp = array();
		$i = 0;

		foreach ($results['rows'] as $row) {

			$id = $row->IdDocumento;
			$larrResultTemp = array('id'=> ++$i,
								    'checkbox'=>'<input type="checkbox" class="ids" name="ids[]" value="'.$id.'" /> '
								    );
			foreach ($this->info['config']['grid'] as $field) {
				if($field['view'] =='1') {
					$limited = isset($field['limited']) ? $field['limited'] :'';
					if (\SiteHelpers::filterColumn($limited )){
						$value = \SiteHelpers::formatRows($row->{$field['field']}, $field , $row);
						$larrResultTemp[$field['field']] = $value;
					}
				}
			}
			//$larrResultTemp['action'] = \AjaxHelpers::buttonAction('documentos',$this->access,$id ,$this->info['setting']).\AjaxHelpers::buttonActionInline($id,'IdDocumento');
			$larrResultTemp['action'] = '<div class=" action dropup"><a href="'.\URL::to('documentos/update/'.$id).'" onclick="SximoModal(this.href,\'Edit Form\'); return false; "  class="btn btn-xs btn-white tips" title="'.\Lang::get('core.btn_edit').'"><i class="fa  fa-upload"></i></a></div>';
			$larrResult[] = $larrResultTemp;
		}

		echo json_encode(array("data"=>$larrResult));

	}

	public function postData( Request $request)
	{
		$params = array(
			'page'		=> '' ,
			'sort'		=> '' ,
			'order'		=> '',
			'params'	=> '',
			'global'	=> (isset($this->access['is_global']) ? $this->access['is_global'] : 0 )
		);
		// Get Query
		$results = $this->model->getRows( $params );
		$params['params'] = " AND tbl_documentos.entidad = 7 ";
		$resultsNew = 		$this->model->getRows( $params );
		$this->data['rowDataDos']   = $resultsNew['rows'];
		
		$params = array(
			'page'		=> '',
			'limit'		=> '',
			'IdEstatus'	=> (is_null($request->input('IdEstatus')) ? 1 : $request->input('IdEstatus') ),
			'sort'		=> '',
			'order'		=> '',
			'params'	=> '',
			'global'	=> (isset($this->access['is_global']) ? $this->access['is_global'] : 0 )
		);
		$this->data['param']		= $params;
		$this->data['rowData']		= array();
		$this->data['i']			= 0;
		$this->data['tableGrid'] 	= $this->info['config']['grid'];
		$this->data['tableForm'] 	= $this->info['config']['forms'];
		$this->data['colspan'] 		= \SiteHelpers::viewColSpan($this->info['config']['grid']);
		$this->data['access']		= $this->access;
		$this->data['setting'] 		= $this->info['setting'];
		return view('documentos.table',$this->data);

	}




	function getUpdate(Request $request, $id = null)
	{

		if($id =='')
		{
			if($this->access['is_add'] ==0 )
			return Redirect::to('dashboard')->with('messagetext',\Lang::get('core.note_restric'))->with('msgstatus','error');
		}

		if($id !='')
		{
			if($this->access['is_edit'] ==0 )
			return Redirect::to('dashboard')->with('messagetext',\Lang::get('core.note_restric'))->with('msgstatus','error');
		}

		$row = $this->model->find($id);
		if($row)
		{
			$this->data['row'] 		=  $row;
		} else {
			$this->data['row'] 		= $this->model->getColumnTable('tbl_documentos');
		}
		$this->data['lrowTipoDocumento'] = \DB::table('tbl_tipos_documentos')->where('IdTipoDocumento',$row['IdTipoDocumento'])->get();
        		$this->data['lrowTipoDocumentoValor'] = \DB::table('tbl_tipo_documento_valor')->where('IdTipoDocumento',$row['IdTipoDocumento'])->get();
		$this->data['setting'] 		= $this->info['setting'];
		$this->data['fields'] 		=  \AjaxHelpers::fieldLang($this->info['config']['forms']);
		$this->data['subform'] = $this->detailview($this->modelview ,  $this->info['config']['subform'] ,$id );
		$this->data['id'] = $id;

		return view('documentos.form',$this->data);
	}

	public function getShow( $id = null)
	{

		if($this->access['is_detail'] ==0)
			return Redirect::to('dashboard')
				->with('messagetext', \Lang::get('core.note_restric'))->with('msgstatus','error');

		$row = $this->model->getRow($id);
		if($row)
		{
			$this->data['row'] =  $row;

			$this->data['id'] = $id;
			$this->data['access']		= $this->access;
			$this->data['setting'] 		= $this->info['setting'];
			$this->data['fields'] 		= \AjaxHelpers::fieldLang($this->info['config']['grid']);
			$this->data['subgrid']		= (isset($this->info['config']['subgrid']) ? $this->info['config']['subgrid'] : array());
			return view('documentos.view',$this->data);

		} else {

			return response()->json(array(
				'status'=>'error',
				'message'=> \Lang::get('core.note_error')
			));
		}
	}


	function postCopy( Request $request)
	{

	    foreach(\DB::select("SHOW COLUMNS FROM tbl_documentos ") as $column)
        {
			if( $column->Field != 'IdDocumento')
				$columns[] = $column->Field;
        }
		if(count($request->input('ids')) >=1)
		{

			$toCopy = implode(",",$request->input('ids'));


			$sql = "INSERT INTO tbl_documentos (".implode(",", $columns).") ";
			$sql .= " SELECT ".implode(",", $columns)." FROM tbl_documentos WHERE IdDocumento IN (".$toCopy.")";
			\DB::select($sql);
			return response()->json(array(
				'status'=>'success',
				'message'=> \Lang::get('core.note_success')
			));

		} else {
			return response()->json(array(
				'status'=>'success',
				'message'=> 'Please select row to copy'
			));
		}


	}

	function postSave( Request $request, $id =0)
	{
		/* Dependiendo del tipo de documento ejecutamos una rutina para cada uno*/
		$lstrResult = "";

		$valor = $request->bulk_Valor;
		$tvalor = $request->bulk_IdTipoDocumentoValor;
		$cont = $request->counter;
		for($i=0; $i<count($valor);$i++){
			if (strlen($valor[$i])==0){
				unset($valor[$i]);
				unset($tvalor[$i]);
				unset($cont[$i]);
			}
		}
		$request['bulk_Valor'] = $valor;
		$request['bulk_IdTipoDocumentoValor'] = $tvalor;
		$request['counter'] = $cont;

		$rules = $this->validateForm();
		$validator = Validator::make($request->all(), $rules);
		if ($validator->passes()) {
			$data = $this->validatePost('tbl_documentos');

			//Validamos el tipo de documento
			if (isset($data['IdTipoDocumento'])) {
				$lobjTipoDocumento = \DB::table('tbl_tipos_documentos')->where('IdTipoDocumento',$data['IdTipoDocumento'])->get();
				if ($lobjTipoDocumento){
				  $lintTipo = $lobjTipoDocumento[0]->Vigencia;
				  $lstrFormula = $lobjTipoDocumento[0]->Formula;
				  if ($lintTipo=="2"){
				  	if ($lstrFormula!=""){
					    $data['FechaVencimiento'] = eval("return ".$lstrFormula.";");
					}
				  }
				}
			}
			if (!isset($data['FechaVencimiento'])){
				$data['FechaVencimiento'] = "";
			}
			$data['Resultado'] = $lstrResult;
			$id = $this->model->insertRow($data , $request->input('IdDocumento'));
			$this->detailviewsave( $this->modelview , $request->all() ,$this->info['config']['subform'] , $id) ;

			/* enviamos a procesar el documento y determinar si necesita algún tratado especial */
			//var_dump($id).'       '.var_dump($data);
			$larrResult = \MySourcing::ProccessDocument($id,$data);

			return response()->json(array(
				'status'=>'success',
				'message'=> \Lang::get('core.note_success')
				));

		} else {

			$message = $this->validateListError(  $validator->getMessageBag()->toArray() );
			return response()->json(array(
				'message'	=> $message,
				'status'	=> 'error'
			));

		}

	}

	public function postDelete( Request $request)
	{

		if($this->access['is_remove'] ==0) {
			return response()->json(array(
				'status'=>'error',
				'message'=> \Lang::get('core.note_restric')
			));
			die;

		}
		// delete multipe rows
		if(count($request->input('ids')) >=1)
		{
			$this->model->destroy($request->input('ids'));
			\DB::table('tbl_documento_valor')->whereIn('IdDocumento',$request->input('ids'))->delete();
			return response()->json(array(
				'status'=>'success',
				'message'=> \Lang::get('core.note_success_delete')
			));
		} else {
			return response()->json(array(
				'status'=>'error',
				'message'=> \Lang::get('core.note_error')
			));

		}

	}

	public static function display( )
	{
		$mode  = isset($_GET['view']) ? 'view' : 'default' ;
		$model  = new Documentos();
		$info = $model::makeInfo('documentos');

		$data = array(
			'pageTitle'	=> 	$info['title'],
			'pageNote'	=>  $info['note']

		);

		if($mode == 'view')
		{
			$id = $_GET['view'];
			$row = $model::getRow($id);
			if($row)
			{
				$data['row'] =  $row;
				$data['fields'] 		=  \SiteHelpers::fieldLang($info['config']['grid']);
				$data['id'] = $id;
				return view('documentos.public.view',$data);
			}

		} else {

			$page = isset($_GET['page']) ? $_GET['page'] : 1;
			$params = array(
				'page'		=> $page ,
				'limit'		=>  (isset($_GET['rows']) ? filter_var($_GET['rows'],FILTER_VALIDATE_INT) : 10 ) ,
				'sort'		=> 'IdDocumento' ,
				'order'		=> 'asc',
				'params'	=> '',
				'global'	=> 1
			);

			$result = $model::getRows( $params );
			$data['tableGrid'] 	= $info['config']['grid'];
			$data['rowData'] 	= $result['rows'];

			$page = $page >= 1 && filter_var($page, FILTER_VALIDATE_INT) !== false ? $page : 1;
			$pagination = new Paginator($result['rows'], $result['total'], $params['limit']);
			$pagination->setPath('');
			$data['i']			= ($page * $params['limit'])- $params['limit'];
			$data['pagination'] = $pagination;
			return view('documentos.public.index',$data);
		}


	}

	function postSavepublic( Request $request)
	{

		$rules = $this->validateForm();
		$validator = Validator::make($request->all(), $rules);
		if ($validator->passes()) {
			$data = $this->validatePost('tbl_documentos');
			 $this->model->insertRow($data , $request->input('IdDocumento'));
			return  Redirect::back()->with('messagetext','<p class="alert alert-success">'.\Lang::get('core.note_success').'</p>')->with('msgstatus','success');
		} else {

			return  Redirect::back()->with('messagetext','<p class="alert alert-danger">'.\Lang::get('core.note_error').'</p>')->with('msgstatus','error')
			->withErrors($validator)->withInput();

		}

	}

}
