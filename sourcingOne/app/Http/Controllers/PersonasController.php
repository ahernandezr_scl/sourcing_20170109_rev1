<?php namespace App\Http\Controllers;

use App\Http\Controllers\controller;
use App\Models\Personas;
use Illuminate\Http\Request;
use Illuminate\Pagination\LengthAwarePaginator as Paginator;
use Validator, Input, Redirect ;

class PersonasController extends Controller {

	protected $layout = "layouts.main";
	protected $data = array();
	public $module = 'personas';
	static $per_page	= '10';

	public function __construct()
	{
		parent::__construct();
		$this->model = new Personas();
		$this->modelview = new  \App\Models\Anotaciones();
		$this->info = $this->model->makeInfo( $this->module);
		$this->access = $this->model->validAccess($this->info['id']);

		$this->data = array(
			'pageTitle'			=> 	$this->info['title'],
			'pageNote'			=>  $this->info['note'],
			'pageModule'		=> 'personas',
			'pageUrl'			=>  url('personas'),
			'return' 			=> 	self::returnUrl()
		);

	}

	public function getIndex()
	{
		if($this->access['is_view'] ==0)
			return Redirect::to('dashboard')->with('messagetext',\Lang::get('core.note_restric'))->with('msgstatus','error');

		$this->data['access']		= $this->access;
		return view('personas.index',$this->data);
	}

	public function postData( Request $request)
	{

		$lintLevelUser = \MySourcing::LevelUser(\Session::get('uid'));
	    $lintIdUser = \Session::get('uid');
	    $lintIdUserContract = 0;

		$sort = (!is_null($request->input('sort')) ? $request->input('sort') : $this->info['setting']['orderby']);
		$order = (!is_null($request->input('order')) ? $request->input('order') : $this->info['setting']['ordertype']);
		// End Filter sort and order for query
		// Filter Search for query
		$filter = '';
		if(!is_null($request->input('search')))
		{
			$search = 	$this->buildSearch('maps');
			$filter = $search['param'];
			$this->data['search_map'] = $search['maps'];
		}

		if ($lintLevelUser==6){
     		if ($lintIdUser!=$lintIdUserContract && $lintIdUserContract > 0 ){
     			$filter = " AND tbl_personas.entry_by_access = ".$lintIdUser." ";
     		}
     	}


		$page = $request->input('page', 1);
		$params = array(
			'page'		=> $page ,
			'limit'		=> (!is_null($request->input('rows')) ? filter_var($request->input('rows'),FILTER_VALIDATE_INT) : $this->info['setting']['perpage'] ) ,
			'sort'		=> $sort ,
			'order'		=> $order,
			'params'	=> $filter,
			'global'	=> (isset($this->access['is_global']) ? $this->access['is_global'] : 0 )
		);
		// Get Query
		$results = $this->model->getRows( $params );

		// Build pagination setting
		$page = $page >= 1 && filter_var($page, FILTER_VALIDATE_INT) !== false ? $page : 1;
		$pagination = new Paginator($results['rows'], $results['total'], $params['limit']);
		$pagination->setPath('personas/data');

		$this->data['param']		= $params;
		$this->data['rowData']		= $results['rows'];
		// Build Pagination
		$this->data['pagination']	= $pagination;
		// Build pager number and append current param GET
		$this->data['pager'] 		= $this->injectPaginate();
		// Row grid Number
		$this->data['i']			= ($page * $params['limit'])- $params['limit'];
		// Grid Configuration
		$this->data['tableGrid'] 	= $this->info['config']['grid'];
		$this->data['tableForm'] 	= $this->info['config']['forms'];
		$this->data['colspan'] 		= \SiteHelpers::viewColSpan($this->info['config']['grid']);
		// Group users permission
		$this->data['access']		= $this->access;
		// Detail from master if any
		$this->data['setting'] 		= $this->info['setting'];

		// Master detail link if any
		$this->data['subgrid']	= (isset($this->info['config']['subgrid']) ? $this->info['config']['subgrid'] : array());

		$this->data['anotaciones']=  \DB::table('tbl_concepto_anotacion')->get();
		// Render into template
		return view('personas.table',$this->data);

	}
	function getLista(Request $request){

        // Get Query
       $sort = (!is_null($request->input('sort')) ? $request->input('sort') : $this->info['setting']['orderby']);
		$order = (!is_null($request->input('order')) ? $request->input('order') : $this->info['setting']['ordertype']);
		// End Filter sort and order for query
		// Filter Search for query
		$filter = '';
		if(!is_null($request->input('search')))
		{
			$search = 	$this->buildSearch('maps');
			$filter = $search['param'];
			$this->data['search_map'] = $search['maps'];
		}
		$params = array(
			'page'		=> '',
			'limit'		=> '',
			'sort'		=> $sort ,
			'order'		=> $order,
			'params'	=> $filter,
			'global'	=> (isset($this->access['is_global']) ? $this->access['is_global'] : 0 )
		);
		// Get Query
		$results = $this->model->getRows( $params );

		$larrResult = array();
		$larrResultTemp = array();
		$i = 0;

		foreach ($results['rows'] as $row) {

			$id = $row->IdPersona;

			$larrResultTemp = array('id'=> ++$i,
								    'checkbox'=>'<input type="checkbox" class="ids" name="ids[]" value="'.$row->IdPersona.'" /> '
								    );
			foreach ($this->info['config']['grid'] as $field) {
				if($field['view'] =='1') {
					$limited = isset($field['limited']) ? $field['limited'] :'';
					if (\SiteHelpers::filterColumn($limited )){
						$value = \SiteHelpers::formatRows($row->{$field['field']}, $field , $row);
						$larrResultTemp[$field['field']] = $value;
					}
				}
			}
			if ($request->input('Nivel')=="6")
			$larrResultTemp['action'] = \AjaxHelpers::buttonAction('personas',$this->access,$id ,$this->info['setting'],3).\AjaxHelpers::buttonActionInline($row->IdPersona,'IdPersona');
			else
			$larrResultTemp['action'] = \AjaxHelpers::buttonAction('personas',$this->access,$id ,$this->info['setting']).\AjaxHelpers::buttonActionInline($row->IdPersona,'IdPersona');
			$larrResult[] = $larrResultTemp;
		}

		echo json_encode(array("data"=>$larrResult));

    }

	function getUpdate(Request $request, $id = null)
	{

		if($id =='')
		{
			if($this->access['is_add'] ==0 )
			return Redirect::to('dashboard')->with('messagetext',\Lang::get('core.note_restric'))->with('msgstatus','error');
		}

		if($id !='')
		{
			if($this->access['is_edit'] ==0 )
			return Redirect::to('dashboard')->with('messagetext',\Lang::get('core.note_restric'))->with('msgstatus','error');
		}

		$row = $this->model->find($id);
		if($row)
		{
			$this->data['row'] 		=  $row;
			$this->data['anotacionesP']=  \DB::table('tbl_anotaciones')
			->join('tb_users', 'tbl_anotaciones.entry_by', '=', 'tb_users.id')
			->join('tbl_concepto_anotacion', 'tbl_anotaciones.IdConceptoAnotacion','=','tbl_concepto_anotacion.IdConceptoAnotacion')
			->select('tbl_anotaciones.IdConceptoAnotacion', 'tbl_anotaciones.entry_by','tbl_anotaciones.entry_by_access','tbl_anotaciones.IdPersona', 'tb_users.first_name', 'tb_users.last_name', 'tb_users.id', 'tbl_concepto_anotacion.Descripcion')
			->where('IdPersona',$row['IdPersona'])->get();


		} else {
			$this->data['row'] 		= $this->model->getColumnTable('tbl_personas');
		}
		$this->data['setting'] 		= $this->info['setting'];
		$this->data['fields'] 		=  \AjaxHelpers::fieldLang($this->info['config']['forms']);
		$this->data['subform'] = $this->detailview($this->modelview ,  $this->info['config']['subform'] ,$id );
		$this->data['id'] = $id;

		return view('personas.form',$this->data);
	}

	public function getShow( $id = null)
	{

		if($this->access['is_detail'] ==0)
			return Redirect::to('dashboard')
				->with('messagetext', \Lang::get('core.note_restric'))->with('msgstatus','error');

		$row = $this->model->getRow($id);
		if($row)
		{
			$this->data['row'] =  $row;

			$this->data['id'] = $id;
			$this->data['access']		= $this->access;
			$this->data['setting'] 		= $this->info['setting'];
			$this->data['fields'] 		= \AjaxHelpers::fieldLang($this->info['config']['grid']);
			$this->data['subgrid']		= (isset($this->info['config']['subgrid']) ? $this->info['config']['subgrid'] : array());
			return view('personas.view',$this->data);

		} else {

			return response()->json(array(
				'status'=>'error',
				'message'=> \Lang::get('core.note_error')
			));
		}
	}


	function postCopy( Request $request)
	{

	    foreach(\DB::select("SHOW COLUMNS FROM tbl_personas ") as $column)
        {
			if( $column->Field != 'IdPersona')
				$columns[] = $column->Field;
        }
		if(count($request->input('ids')) >=1)
		{

			$toCopy = implode(",",$request->input('ids'));


			$sql = "INSERT INTO tbl_personas (".implode(",", $columns).") ";
			$sql .= " SELECT ".implode(",", $columns)." FROM tbl_personas WHERE IdPersona IN (".$toCopy.")";
			\DB::select($sql);
			return response()->json(array(
				'status'=>'success',
				'message'=> \Lang::get('core.note_success')
			));

		} else {
			return response()->json(array(
				'status'=>'success',
				'message'=> 'Please select row to copy'
			));
		}


	}

	function postSave( Request $request, $id =0)
	{
		$lintLevelUser = \MySourcing::LevelUser(\Session::get('uid'));
	    $lintIdUser = \Session::get('uid');
		$rules = $this->validateForm();
		$validator = Validator::make($request->all(), $rules);
		if ($validator->passes()) {
			$data = $this->validatePost('tbl_personas');

			$id = $this->model->insertRow($data , $request->input('IdPersona'));
			if ($lintLevelUser==1){
			  $this->detailviewsave( $this->modelview , $request->all() ,$this->info['config']['subform'] , $id) ;
			}
			return response()->json(array(
				'status'=>'success',
				'message'=> \Lang::get('core.note_success')
				));

		} else {

			$message = $this->validateListError(  $validator->getMessageBag()->toArray() );
			return response()->json(array(
				'message'	=> $message,
				'status'	=> 'error'
			));
		}

	}

	function postMasivo(Request $request, $id =0){
		$destinationPath = "uploads/documents/";

		$lintLevelUser = \MySourcing::LevelUser(\Session::get('uid'));
		$lintIdUserLogin = \Session::get('uid');

		$file = Input::file("FileDataEmployee");
      		$filename = $file->getClientOriginalName();
	  	$extension =$file->getClientOriginalExtension();
      		$rand = rand(1000,100000000);
	  	$newfilename = strtotime(date('Y-m-d H:i:s')).'-'.$rand.'.'.$extension;
	  	$uploadSuccess = $file->move($destinationPath, $newfilename);

		include '../app/Library/PHPExcel/IOFactory.php';

		$inputFileName = $destinationPath.$newfilename;

		try {

		    $objPHPExcel = \PHPExcel_IOFactory::load($inputFileName);

		} catch(Exception $e) {

		    die('Error loading file "'.pathinfo($inputFileName,PATHINFO_BASENAME).'": '.$e->getMessage());

		}

		$allDataInSheet = $objPHPExcel->getActiveSheet()->toArray(null,true,true,true);
		$arrayCount = count($allDataInSheet);

		for($i=2;$i<=$arrayCount;$i++){
			$rut = trim($allDataInSheet[$i]["A"]);
			$rutPer = \DB::table('tbl_personas')->where('RUT',$rut)->get();
			$nombres = trim($allDataInSheet[$i]["B"]);
			$apellidos = trim($allDataInSheet[$i]["C"]);
			//$foto = trim($allDataInSheet[$i]["D"]);
			$direccion = trim($allDataInSheet[$i]["D"]);
			$fechanTem = strtotime(trim($allDataInSheet[$i]["E"]));
			$fechaN = date('Y-m-d',$fechanTem);

			$sexo = strtoupper(trim($allDataInSheet[$i]["F"]));
			if ($sexo=="HOMBRE")
				$valorS=1;
			elseif ($sexo=="MUJER")
				$valorS=2;
			else
				$valorS=0;
			$estadoC = strtoupper(trim($allDataInSheet[$i]["G"]));
			switch ($estadoC) {
				    case "SOLTERO":
				       $valoreEs=1;
				        break;
				    case "CASADO":
				        $valoreEs=2;
				        break;
				    case "DIVORCIADO":
				        $valoreEs=3;
				        break;
				    case "VIUDO":
				        $valoreEs=4;
				        break;
				   default:
       					 $valoreEs=0;
				}
			$asignado = trim($allDataInSheet[$i]["H"]);
			 $fecha = date('Y/m/d');

			 if (array_key_exists('I', $allDataInSheet[$i])) {
			    $contrato = trim($allDataInSheet[$i]["I"]);
			    $datosContrato = \DB::table('tbl_contrato')->select('contrato_id','IdContratista')->where('cont_numero',$contrato)->get();
			}

			 if (array_key_exists('J', $allDataInSheet[$i])) {
			   	$rol = strtoupper(trim($allDataInSheet[$i]["J"]));
			    $datosRol = \DB::table('tbl_roles')->select('IdRol')->where('Descripción','like',$rol)->get();
			    if ($datosRol) {
				    $rolid =  $datosRol[0]->IdRol;
				}else{
			    	$rolid =0;
			    }
			}
			if ((count($rutPer)==0) && (strlen($rut)>0)){
			    if ($lintLevelUser == 6) {
			        $IdPer = \DB::table('tbl_personas')->insertGetId(
            			['RUT' => $rut, 'Nombres' => $nombres, 'Apellidos' => $apellidos, 'Direccion' => $direccion, 'FechaNacimiento' => $fechaN, 'Sexo' => $valorS, 'EstadoCivil' => $valoreEs, 'IdEstatus'=> 1, 'createdOn' => $fecha, 'entry_by'=> $lintIdUserLogin, 'entry_by_access' => $lintIdUserLogin, 'updatedOn'=> NULL ]);
			    }else{
			        $IdPer = \DB::table('tbl_personas')->insertGetId(
            			['RUT' => $rut, 'Nombres' => $nombres, 'Apellidos' => $apellidos, 'Direccion' => $direccion, 'FechaNacimiento' => $fechaN, 'Sexo' => $valorS, 'EstadoCivil' => $valoreEs, 'IdEstatus'=> 1, 'createdOn' => $fecha, 'entry_by'=> $lintIdUserLogin, 'entry_by_access' => 0, 'updatedOn'=> NULL ]);
			    }
			if (isset($datosContrato)){
				if((count($datosContrato)>0)){
				 $contratoid = $datosContrato[0]->contrato_id;
				$contratistaid = $datosContrato[0]->IdContratista;
					if ($lintLevelUser == 6) {
					 	$IdCont = \DB::table('tbl_contratos_personas')->insertGetId(
		            		     ['IdPersona' => $IdPer, 'IdContratista' => $contratistaid, 'contrato_id' => $contratoid, 'IdRol' => $rolid, 'IdEstatus' => 1, 'entry_by'=> $lintIdUserLogin, 'entry_by_access' => $lintIdUserLogin ]);
					}else{
						$IdCont = \DB::table('tbl_contratos_personas')->insertGetId(
		            		     ['IdPersona' => $IdPer, 'IdContratista' => $contratistaid, 'contrato_id' => $contratoid, 'IdRol' => $rolid, 'IdEstatus' => 1, 'entry_by'=> $lintIdUserLogin, 'entry_by_access' => 0 ]);
					}
				}
			}

			}
			else{
				require_once '../app/Library/PHPExcel.php';
				$objPHPExcel2 = new \PHPExcel();
			$objWriter = \PHPExcel_IOFactory::createWriter($objPHPExcel2, "Excel2007");
			$objSheet = $objPHPExcel2->getActiveSheet();
			$objSheet->setTitle('Registros');
			$objSheet->getStyle('A1:R1')->getFont()->setBold(true)->setSize(14);
			$char = "A";
			$objSheet->getCell($char++.'1')->setValue('RUT');
			$objSheet->getCell($char++.'1')->setValue('NOMBRES');
			$objSheet->getCell($char++.'1')->setValue('APELLIDOS');
			$objSheet->getCell($char++.'1')->setValue('DIRECCION');
			$objSheet->getCell($char++.'1')->setValue('FECHA NACIMIENTO');
			$objSheet->getCell($char++.'1')->setValue('SEXO');
			$objSheet->getCell($char++.'1')->setValue('ESTADO CIVIL');

			$objSheet->getStyle('A1:R1000')->getAlignment()->setHorizontal(\PHPExcel_Style_Alignment::HORIZONTAL_CENTER);

			$objWriter = \PHPExcel_IOFactory::createWriter($objPHPExcel2, 'Excel2007');
			$objWriter->save('errores.xlsx');
			}


		}
		return response()->json(array(
				'status'=>'success',
				'message'=> \Lang::get('core.note_success')
				));
	}

	public function postBorrar( Request $request)
	{

		if($this->access['is_edit'] ==0) {
			return response()->json(array(
				'status'=>'error',
				'message'=> \Lang::get('core.note_restric')
			));
			die;

		}
		if(count($request->input('IdPersona')) >=1)
		{

            $Id = $request->input('IdPersona');
            $anot = $request->input('result');
            $fecha = date('Y/m/d');
            $IdUser = $request->input('usuario');

            $contratos = \DB::table('tbl_contratos_personas')
                  ->select('IdPersona','IdContratista','contrato_id')
                  ->where('IdPersona', '=', $Id)
                  ->get();
            if ($contratos){
                   $IdCont = $contratos[0]->contrato_id;
               }else{
               	$IdCont = 0;
               }

	 $IdDoc = \DB::table('tbl_documentos')->insertGetId(
            ['IdTipoDocumento' => 4, 'Entidad' => 3, 'IdEntidad'=> $Id, 'Documento' => NULL, 'DocumentoURL' => NULL, 'FechaVencimiento' => NULL, 'IdEstatus' => 1, 'createdOn' => $fecha, 'entry_by'=> $IdUser, 'entry_by_access' => 0, 'updatedOn'=> NULL, 'FechaEmision'=> NULL, 'Resultado'=> NULL ]);

	      $IdAnotac = \DB::table('tbl_anotaciones')->insertGetId(
		['IdConceptoAnotacion' => $anot, 'IdPersona' => $Id, 'createdOn' => $fecha, 'entry_by'=> $IdUser, 'entry_by_access' => 0, 'updatedOn'=> null ]);

	\DB::table('tbl_personas')->where('IdPersona', $Id)->update(['entry_by_access' => 0]);

	 \DB::table('tbl_contratos_personas')->where('IdPersona', '=', $Id)->where('contrato_id', '=', $IdCont)->delete();

            $acce = \DB::table('tbl_accesos')
                  ->select('IdAcceso')
                  ->where('IdPersona', '=', $Id)
                  ->where('contrato_id', '=', $IdCont)
                  ->get();

              if (sizeof($acce)>0) {
                   $valor = $acce[0]->IdAcceso;
                             \DB::table('tbl_acceso_areas')->where('IdAcceso', '=', $valor)->delete();
                              \DB::table('tbl_accesos')->where('IdAcceso', '=', $valor)->delete();
              }

			return response()->json(array(
				'status'=>'success',
				'message'=> \Lang::get('core.note_success')
			));

		} else {
			return response()->json(array(
				'status'=>'error',
				'message'=> \Lang::get('core.note_error')
			));

		}

	}

	public function postDelete( Request $request)
	{

		if($this->access['is_remove'] ==0) {
			return response()->json(array(
				'status'=>'error',
				'message'=> \Lang::get('core.note_restric')
			));
			die;

		}
		// delete multipe rows
		if(count($request->input('ids')) >=1)
		{
			$this->model->destroy($request->input('ids'));
			\DB::table('tbl_anotaciones')->whereIn('IdPersona',$request->input('ids'))->delete();
			return response()->json(array(
				'status'=>'success',
				'message'=> \Lang::get('core.note_success_delete')
			));
		} else {
			return response()->json(array(
				'status'=>'error',
				'message'=> \Lang::get('core.note_error')
			));

		}

	}

	public static function display( )
	{
		$mode  = isset($_GET['view']) ? 'view' : 'default' ;
		$model  = new Personas();
		$info = $model::makeInfo('personas');

		$data = array(
			'pageTitle'	=> 	$info['title'],
			'pageNote'	=>  $info['note']

		);

		if($mode == 'view')
		{
			$id = $_GET['view'];
			$row = $model::getRow($id);
			if($row)
			{
				$data['row'] =  $row;
				$data['fields'] 		=  \SiteHelpers::fieldLang($info['config']['grid']);
				$data['id'] = $id;
				return view('personas.public.view',$data);
			}

		} else {

			$page = isset($_GET['page']) ? $_GET['page'] : 1;
			$params = array(
				'page'		=> $page ,
				'limit'		=>  (isset($_GET['rows']) ? filter_var($_GET['rows'],FILTER_VALIDATE_INT) : 10 ) ,
				'sort'		=> 'IdPersona' ,
				'order'		=> 'asc',
				'params'	=> '',
				'global'	=> 1
			);

			$result = $model::getRows( $params );
			$data['tableGrid'] 	= $info['config']['grid'];
			$data['rowData'] 	= $result['rows'];

			$page = $page >= 1 && filter_var($page, FILTER_VALIDATE_INT) !== false ? $page : 1;
			$pagination = new Paginator($result['rows'], $result['total'], $params['limit']);
			$pagination->setPath('');
			$data['i']			= ($page * $params['limit'])- $params['limit'];
			$data['pagination'] = $pagination;
			return view('personas.public.index',$data);
		}


	}

	function postSavepublic( Request $request)
	{

		$rules = $this->validateForm();
		$validator = Validator::make($request->all(), $rules);
		if ($validator->passes()) {
			$data = $this->validatePost('tbl_personas');
			 $this->model->insertRow($data , $request->input('IdPersona'));
			return  Redirect::back()->with('messagetext','<p class="alert alert-success">'.\Lang::get('core.note_success').'</p>')->with('msgstatus','success');
		} else {

			return  Redirect::back()->with('messagetext','<p class="alert alert-danger">'.\Lang::get('core.note_error').'</p>')->with('msgstatus','error')
			->withErrors($validator)->withInput();

		}

	}

	public  function postCompruebarut(Request $request)
	{
		$rut = $request->rut;

		$personas = \DB::table('tbl_personas')
		->leftJoin('tbl_anotaciones', 'tbl_personas.IdPersona', '=', 'tbl_anotaciones.IdPersona')
		->select(\DB::raw("tbl_personas.*, tbl_anotaciones.IdAnotacion, tbl_anotaciones.IdConceptoAnotacion, tbl_anotaciones.IdPersona as IdpersonaA, tbl_anotaciones.Observacion"))
		->where('tbl_personas.RUT', '=', $rut)
		->get();
		return response()->json(array(
			'status'=>'sucess',
			'valores'=>$personas,
			'message'=>\Lang::get('core.note_sucess')
			));
	}

	public  function postBuscaanotacion(Request $request)
	{
		$Idperso = $request->persona;

 		$anotacionesP =  \DB::table('tbl_anotaciones')
                    ->join('tb_users', 'tbl_anotaciones.entry_by', '=', 'tb_users.id')
                    ->join('tbl_concepto_anotacion', 'tbl_anotaciones.IdConceptoAnotacion','=','tbl_concepto_anotacion.IdConceptoAnotacion')
                    ->where('IdPersona',$Idperso)->get();
		return response()->json(array(
			'status'=>'sucess',
			'anotaciones'=>$anotacionesP,
			'message'=>\Lang::get('core.note_sucess')
			));
	}


}
