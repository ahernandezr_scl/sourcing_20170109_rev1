<?php 
define('CNF_APPNAME','Sourcing One');
define('CNF_APPDESC','Gestión y control de contratistas');
define('CNF_COMNAME','Sourcing');
define('CNF_EMAIL','sistema@sourcing.cl');
define('CNF_METAKEY','my site , my company  , Larvel Crud');
define('CNF_METADESC','Write description for your site');
define('CNF_GROUP','3');
define('CNF_ACTIVATION','auto');
define('CNF_MULTILANG','1');
define('CNF_LANG','es');
define('CNF_REGIST','false');
define('CNF_FRONT','false');
define('CNF_RECAPTCHA','false');
define('CNF_THEME','default');
define('CNF_RECAPTCHAPUBLICKEY','');
define('CNF_RECAPTCHAPRIVATEKEY','');
define('CNF_MODE','production');
define('CNF_LOGO','backend-logo.png');
define('CNF_ALLOWIP','');
define('CNF_RESTRICIP','192.116.134 , 194.111.606.21 ');
define('CNF_MAIL','phpmail');
define('CNF_DATE','m/d/y');
?>