

		 {!! Form::open(array('url'=>'adjuntoscontratos/savepublic', 'class'=>'form-horizontal','files' => true , 'parsley-validate'=>'','novalidate'=>' ')) !!}

	@if(Session::has('messagetext'))
	  
		   {!! Session::get('messagetext') !!}
	   
	@endif
	<ul class="parsley-error-list">
		@foreach($errors->all() as $error)
			<li>{{ $error }}</li>
		@endforeach
	</ul>		


<div class="col-md-12">
						<fieldset><legend> Adjuntos Contratos</legend>
				{!! Form::hidden('id_adjunto', $row['id_adjunto']) !!}					
									  <div class="form-group  " >
										<label for="Tipo Documento" class=" control-label col-md-4 text-left"> Tipo Documento <span class="asterix"> * </span></label>
										<div class="col-md-6">
										  
					<?php $tipo = explode(',',$row['tipo']);
					$tipo_opt = array( 'CONTRATO' => 'CONTRATO' ,  'BASE LIQUIDACION' => 'BASE LIQUIDACION' ,  'OFERTA TECNICA' => 'OFERTA TECNICA' ,  'ACTA DE INICIO' => 'ACTA DE INICIO' , ); ?>
					<select name='tipo' rows='5' required  class='select2 '  > 
						<?php 
						foreach($tipo_opt as $key=>$val)
						{
							echo "<option  value ='$key' ".($row['tipo'] == $key ? " selected='selected' " : '' ).">$val</option>"; 						
						}						
						?></select> 
										 </div> 
										 <div class="col-md-2">
										 	<a href="#" data-toggle="tooltip" placement="left" class="tips" title="'CONTRATO','BASE LIQUIDACION','OFERTA TECNICA','ACTA DE INICIO'"><i class="icon-question2"></i></a>
										 </div>
									  </div> {!! Form::hidden('id_contrato', $row['id_contrato']) !!}					
									  <div class="form-group  " >
										<label for="Adjunto" class=" control-label col-md-4 text-left"> Adjunto <span class="asterix"> * </span></label>
										<div class="col-md-6">
										  <input  type='file' name='archivo' id='archivo' @if($row['archivo'] =='') class='required' @endif style='width:150px !important;'  />
					 	<div >
						{!! SiteHelpers::showUploadedFile($row['archivo'],'/uploads/contratos/') !!}
						
						</div>					
					 
										 </div> 
										 <div class="col-md-2">
										 	
										 </div>
									  </div> </fieldset>
			</div>
			
			

			<div style="clear:both"></div>	
				
					
				  <div class="form-group">
					<label class="col-sm-4 text-right">&nbsp;</label>
					<div class="col-sm-8">	
					<button type="submit" name="apply" class="btn btn-info btn-sm" ><i class="fa  fa-check-circle"></i> {{ Lang::get('core.sb_apply') }}</button>
					<button type="submit" name="submit" class="btn btn-primary btn-sm" ><i class="fa  fa-save "></i> {{ Lang::get('core.sb_save') }}</button>
				  </div>	  
			
		</div> 
		 
		 {!! Form::close() !!}
		 
   <script type="text/javascript">
	$(document).ready(function() { 
		
		 

		$('.removeCurrentFiles').on('click',function(){
			var removeUrl = $(this).attr('href');
			$.get(removeUrl,function(response){});
			$(this).parent('div').empty();	
			return false;
		});		
		
	});
	</script>		 
