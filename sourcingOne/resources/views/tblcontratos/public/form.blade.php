

		 {!! Form::open(array('url'=>'tblcontratos/savepublic', 'class'=>'form-horizontal','files' => true , 'parsley-validate'=>'','novalidate'=>' ')) !!}

	@if(Session::has('messagetext'))
	  
		   {!! Session::get('messagetext') !!}
	   
	@endif
	<ul class="parsley-error-list">
		@foreach($errors->all() as $error)
			<li>{{ $error }}</li>
		@endforeach
	</ul>		


<div class="col-md-12">
						<fieldset><legend> Contrato</legend>
				{!! Form::hidden('contrato_id', $row['contrato_id']) !!}					
									  <div class="form-group  " >
										<label for="Short Name" class=" control-label col-md-4 text-left"> Short Name </label>
										<div class="col-md-6">
										  {!! Form::text('cont_nombre', $row['cont_nombre'],array('class'=>'form-control', 'placeholder'=>'',   )) !!} 
										 </div> 
										 <div class="col-md-2">
										 	<a href="#" data-toggle="tooltip" placement="left" class="tips" title="Menos de 5 caracteres"><i class="icon-question2"></i></a>
										 </div>
									  </div> 					
									  <div class="form-group  " >
										<label for="IdContratista" class=" control-label col-md-4 text-left"> IdContratista </label>
										<div class="col-md-6">
										  <select name='IdContratista' rows='5' id='IdContratista' class='select2 '   ></select> 
										 </div> 
										 <div class="col-md-2">
										 	
										 </div>
									  </div> 					
									  <div class="form-group  " >
										<label for="RUT" class=" control-label col-md-4 text-left"> RUT </label>
										<div class="col-md-6">
										  {!! Form::text('cont_rutp_sd', $row['cont_rutp_sd'],array('class'=>'form-control', 'placeholder'=>'',   )) !!} 
										 </div> 
										 <div class="col-md-2">
										 	
										 </div>
									  </div> 					
									  <div class="form-group  " >
										<label for="Verified Digit" class=" control-label col-md-4 text-left"> Verified Digit </label>
										<div class="col-md-6">
										  {!! Form::text('cont_rutp_dv', $row['cont_rutp_dv'],array('class'=>'form-control', 'placeholder'=>'',   )) !!} 
										 </div> 
										 <div class="col-md-2">
										 	
										 </div>
									  </div> 					
									  <div class="form-group  " >
										<label for="SAP Number" class=" control-label col-md-4 text-left"> SAP Number </label>
										<div class="col-md-6">
										  {!! Form::text('cont_numero', $row['cont_numero'],array('class'=>'form-control', 'placeholder'=>'',   )) !!} 
										 </div> 
										 <div class="col-md-2">
										 	
										 </div>
									  </div> 					
									  <div class="form-group  " >
										<label for="Provider" class=" control-label col-md-4 text-left"> Provider </label>
										<div class="col-md-6">
										  {!! Form::text('cont_proveedor', $row['cont_proveedor'],array('class'=>'form-control', 'placeholder'=>'',   )) !!} 
										 </div> 
										 <div class="col-md-2">
										 	
										 </div>
									  </div> 					
									  <div class="form-group  " >
										<label for="Category" class=" control-label col-md-4 text-left"> Category </label>
										<div class="col-md-6">
										  <select name='categoria_id' rows='5' id='categoria_id' class='select2 '   ></select> 
										 </div> 
										 <div class="col-md-2">
										 	
										 </div>
									  </div> 					
									  <div class="form-group  " >
										<label for="Start date" class=" control-label col-md-4 text-left"> Start date <span class="asterix"> * </span></label>
										<div class="col-md-6">
										  
				<div class="input-group m-b" style="width:150px !important;">
					{!! Form::text('cont_fechaInicio', $row['cont_fechaInicio'],array('class'=>'form-control date')) !!}
					<span class="input-group-addon"><i class="fa fa-calendar"></i></span>
				</div> 
										 </div> 
										 <div class="col-md-2">
										 	
										 </div>
									  </div> 					
									  <div class="form-group  " >
										<label for="End date" class=" control-label col-md-4 text-left"> End date <span class="asterix"> * </span></label>
										<div class="col-md-6">
										  
				<div class="input-group m-b" style="width:150px !important;">
					{!! Form::text('cont_fechaFin', $row['cont_fechaFin'],array('class'=>'form-control date')) !!}
					<span class="input-group-addon"><i class="fa fa-calendar"></i></span>
				</div> 
										 </div> 
										 <div class="col-md-2">
										 	
										 </div>
									  </div> 					
									  <div class="form-group  " >
										<label for="Contract Fee" class=" control-label col-md-4 text-left"> Contract Fee </label>
										<div class="col-md-6">
										  {!! Form::text('cont_montoTotal', $row['cont_montoTotal'],array('class'=>'form-control', 'placeholder'=>'',   )) !!} 
										 </div> 
										 <div class="col-md-2">
										 	
										 </div>
									  </div> 					
									  <div class="form-group  " >
										<label for="Administrator" class=" control-label col-md-4 text-left"> Administrator </label>
										<div class="col-md-6">
										  <select name='admin_id' rows='5' id='admin_id' class='select2 '   ></select> 
										 </div> 
										 <div class="col-md-2">
										 	
										 </div>
									  </div> 					
									  <div class="form-group  " >
										<label for="Geographic" class=" control-label col-md-4 text-left"> Geographic </label>
										<div class="col-md-6">
										  <select name='geo_id' rows='5' id='geo_id' class='select2 '   ></select> 
										 </div> 
										 <div class="col-md-2">
										 	
										 </div>
									  </div> 					
									  <div class="form-group  " >
										<label for="Function Area" class=" control-label col-md-4 text-left"> Function Area </label>
										<div class="col-md-6">
										  <select name='afuncional_id' rows='5' id='afuncional_id' class='select2 '   ></select> 
										 </div> 
										 <div class="col-md-2">
										 	
										 </div>
									  </div> 					
									  <div class="form-group  " >
										<label for="Segment" class=" control-label col-md-4 text-left"> Segment </label>
										<div class="col-md-6">
										  <select name='segmento_id' rows='5' id='segmento_id' class='select2 '   ></select> 
										 </div> 
										 <div class="col-md-2">
										 	
										 </div>
									  </div> 					
									  <div class="form-group  " >
										<label for="Company" class=" control-label col-md-4 text-left"> Company </label>
										<div class="col-md-6">
										  {!! Form::text('cont_compagnia', $row['cont_compagnia'],array('class'=>'form-control', 'placeholder'=>'',   )) !!} 
										 </div> 
										 <div class="col-md-2">
										 	
										 </div>
									  </div> 					
									  <div class="form-group  " >
										<label for="Center of cost" class=" control-label col-md-4 text-left"> Center of cost </label>
										<div class="col-md-6">
										  <select name='claseCosto_id' rows='5' id='claseCosto_id' class='select2 '   ></select> 
										 </div> 
										 <div class="col-md-2">
										 	
										 </div>
									  </div> {!! Form::hidden('cont_fechaEstado', $row['cont_fechaEstado']) !!}{!! Form::hidden('cont_estado', $row['cont_estado']) !!}					
									  <div class="form-group  " >
										<label for="Description contract" class=" control-label col-md-4 text-left"> Description contract </label>
										<div class="col-md-6">
										  <textarea name='cont_glosaDescriptiva' rows='5' id='cont_glosaDescriptiva' class='form-control '  
				           >{{ $row['cont_glosaDescriptiva'] }}</textarea> 
										 </div> 
										 <div class="col-md-2">
										 	
										 </div>
									  </div> 					
									  <div class="form-group  " >
										<label for="Signature status" class=" control-label col-md-4 text-left"> Signature status </label>
										<div class="col-md-6">
										  
					<?php $firma_id = explode(',',$row['firma_id']);
					$firma_id_opt = array( '0' => 'SIN FIRMAR' ,  '1' => 'FIRMADO' , ); ?>
					<select name='firma_id' rows='5'   class='select2 '  > 
						<?php 
						foreach($firma_id_opt as $key=>$val)
						{
							echo "<option  value ='$key' ".($row['firma_id'] == $key ? " selected='selected' " : '' ).">$val</option>"; 						
						}						
						?></select> 
										 </div> 
										 <div class="col-md-2">
										 	
										 </div>
									  </div> 					
									  <div class="form-group  " >
										<label for="Date's Signature" class=" control-label col-md-4 text-left"> Date's Signature </label>
										<div class="col-md-6">
										  
				<div class="input-group m-b" style="width:150px !important;">
					{!! Form::text('firma_fecha', $row['firma_fecha'],array('class'=>'form-control date')) !!}
					<span class="input-group-addon"><i class="fa fa-calendar"></i></span>
				</div> 
										 </div> 
										 <div class="col-md-2">
										 	
										 </div>
									  </div> </fieldset>
			</div>
			
			

			<div style="clear:both"></div>	
				
					
				  <div class="form-group">
					<label class="col-sm-4 text-right">&nbsp;</label>
					<div class="col-sm-8">	
					<button type="submit" name="apply" class="btn btn-info btn-sm" ><i class="fa  fa-check-circle"></i> {{ Lang::get('core.sb_apply') }}</button>
					<button type="submit" name="submit" class="btn btn-primary btn-sm" ><i class="fa  fa-save "></i> {{ Lang::get('core.sb_save') }}</button>
				  </div>	  
			
		</div> 
		 
		 {!! Form::close() !!}
		 
   <script type="text/javascript">
	$(document).ready(function() { 
		
		
		$("#IdContratista").jCombo("{!! url('tblcontratos/comboselect?filter=tbl_contratistas:IdContratista:RUT|RazonSocial') !!}",
		{  selected_value : '{{ $row["IdContratista"] }}' });
		
		$("#categoria_id").jCombo("{!! url('tblcontratos/comboselect?filter=tbl_contcategorias:categorias_id:cat_nombre') !!}",
		{  selected_value : '{{ $row["categoria_id"] }}' });
		
		$("#admin_id").jCombo("{!! url('tblcontratos/comboselect?filter=tb_users:id:first_name|last_name') !!}",
		{  selected_value : '{{ $row["admin_id"] }}' });
		
		$("#geo_id").jCombo("{!! url('tblcontratos/comboselect?filter=tbl_contgeografico:geo_id:geo_nombre') !!}",
		{  selected_value : '{{ $row["geo_id"] }}' });
		
		$("#afuncional_id").jCombo("{!! url('tblcontratos/comboselect?filter=tbl_contareafuncional:afuncional_id:afuncional_nombre') !!}",
		{  selected_value : '{{ $row["afuncional_id"] }}' });
		
		$("#segmento_id").jCombo("{!! url('tblcontratos/comboselect?filter=tbl_contsegmento:segmento_id:seg_nombre') !!}",
		{  selected_value : '{{ $row["segmento_id"] }}' });
		
		$("#claseCosto_id").jCombo("{!! url('tblcontratos/comboselect?filter=tbl_contclasecosto:claseCosto_id:ccost_nombre') !!}",
		{  selected_value : '{{ $row["claseCosto_id"] }}' });
		 

		$('.removeCurrentFiles').on('click',function(){
			var removeUrl = $(this).attr('href');
			$.get(removeUrl,function(response){});
			$(this).parent('div').empty();	
			return false;
		});		
		
	});
	</script>		 
