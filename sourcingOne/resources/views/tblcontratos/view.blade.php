@if($setting['view-method'] =='native')
<div class="sbox">
	<div class="sbox-title">  
		<h4> <i class="fa fa-table"></i> <?php echo $pageTitle ;?> <small>{{ $pageNote }}</small>
			<a href="javascript:void(0)" class="collapse-close pull-right btn btn-xs btn-danger" onclick="ajaxViewClose('#{{ $pageModule }}')">
			<i class="fa fa fa-times"></i></a>
		</h4>
	 </div>

	<div class="sbox-content"> 
@endif	

		<table class="table table-striped table-bordered" >
			<tbody>	
				
					<tr>
						<td width='30%' class='label-view text-right'>{{ SiteHelpers::activeLang('IdContratista', (isset($fields['IdContratista']['language'])? $fields['IdContratista']['language'] : array())) }}</td>
						<td>{{ $row->IdContratista}} </td>
						
					</tr>
				
					<tr>
						<td width='30%' class='label-view text-right'>{{ SiteHelpers::activeLang('Short Name', (isset($fields['cont_nombre']['language'])? $fields['cont_nombre']['language'] : array())) }}</td>
						<td>{{ $row->cont_nombre}} </td>
						
					</tr>
				
					<tr>
						<td width='30%' class='label-view text-right'>{{ SiteHelpers::activeLang('RUT', (isset($fields['cont_rutp_sd']['language'])? $fields['cont_rutp_sd']['language'] : array())) }}</td>
						<td>{{ $row->cont_rutp_sd}} </td>
						
					</tr>
				
					<tr>
						<td width='30%' class='label-view text-right'>{{ SiteHelpers::activeLang('Verified Digit', (isset($fields['cont_rutp_dv']['language'])? $fields['cont_rutp_dv']['language'] : array())) }}</td>
						<td>{{ $row->cont_rutp_dv}} </td>
						
					</tr>
				
					<tr>
						<td width='30%' class='label-view text-right'>{{ SiteHelpers::activeLang('SAP Number', (isset($fields['cont_numero']['language'])? $fields['cont_numero']['language'] : array())) }}</td>
						<td>{{ $row->cont_numero}} </td>
						
					</tr>
				
					<tr>
						<td width='30%' class='label-view text-right'>{{ SiteHelpers::activeLang('Legal Name', (isset($fields['cont_proveedor']['language'])? $fields['cont_proveedor']['language'] : array())) }}</td>
						<td>{{ $row->cont_proveedor}} </td>
						
					</tr>
				
					<tr>
						<td width='30%' class='label-view text-right'>{{ SiteHelpers::activeLang('Category', (isset($fields['categoria_id']['language'])? $fields['categoria_id']['language'] : array())) }}</td>
						<td>{{ SiteHelpers::formatLookUp($row->categoria_id,'categoria_id','1:tbl_contcategorias:categorias_id:cat_nombre') }} </td>
						
					</tr>
				
					<tr>
						<td width='30%' class='label-view text-right'>{{ SiteHelpers::activeLang('Start date', (isset($fields['cont_fechaInicio']['language'])? $fields['cont_fechaInicio']['language'] : array())) }}</td>
						<td>{{ date('dd/mm/yyyy',strtotime($row->cont_fechaInicio)) }} </td>
						
					</tr>
				
					<tr>
						<td width='30%' class='label-view text-right'>{{ SiteHelpers::activeLang('End date', (isset($fields['cont_fechaFin']['language'])? $fields['cont_fechaFin']['language'] : array())) }}</td>
						<td>{{ date('dd/mm/yyyy',strtotime($row->cont_fechaFin)) }} </td>
						
					</tr>
				
					<tr>
						<td width='30%' class='label-view text-right'>{{ SiteHelpers::activeLang('Contract Fee', (isset($fields['cont_montoTotal']['language'])? $fields['cont_montoTotal']['language'] : array())) }}</td>
						<td>{{ $row->cont_montoTotal}} </td>
						
					</tr>
				
					<tr>
						<td width='30%' class='label-view text-right'>{{ SiteHelpers::activeLang('Administrator', (isset($fields['admin_id']['language'])? $fields['admin_id']['language'] : array())) }}</td>
						<td>{{ SiteHelpers::formatLookUp($row->admin_id,'admin_id','1:tb_users:id:first_name|last_name') }} </td>
						
					</tr>
				
					<tr>
						<td width='30%' class='label-view text-right'>{{ SiteHelpers::activeLang('Segment', (isset($fields['segmento_id']['language'])? $fields['segmento_id']['language'] : array())) }}</td>
						<td>{{ SiteHelpers::formatLookUp($row->segmento_id,'segmento_id','1:tbl_contsegmento:segmento_id:seg_nombre') }} </td>
						
					</tr>
				
					<tr>
						<td width='30%' class='label-view text-right'>{{ SiteHelpers::activeLang('Geographic', (isset($fields['geo_id']['language'])? $fields['geo_id']['language'] : array())) }}</td>
						<td>{{ SiteHelpers::formatLookUp($row->geo_id,'geo_id','1:tbl_contgeografico:geo_id:geo_nombre') }} </td>
						
					</tr>
				
					<tr>
						<td width='30%' class='label-view text-right'>{{ SiteHelpers::activeLang('Company', (isset($fields['cont_compagnia']['language'])? $fields['cont_compagnia']['language'] : array())) }}</td>
						<td>{{ $row->cont_compagnia}} </td>
						
					</tr>
				
					<tr>
						<td width='30%' class='label-view text-right'>{{ SiteHelpers::activeLang('Function Area', (isset($fields['afuncional_id']['language'])? $fields['afuncional_id']['language'] : array())) }}</td>
						<td>{{ SiteHelpers::formatLookUp($row->afuncional_id,'afuncional_id','1:tbl_contareafuncional:afuncional_id:afuncional_nombre') }} </td>
						
					</tr>
				
					<tr>
						<td width='30%' class='label-view text-right'>{{ SiteHelpers::activeLang('Center of cost', (isset($fields['claseCosto_id']['language'])? $fields['claseCosto_id']['language'] : array())) }}</td>
						<td>{{ SiteHelpers::formatLookUp($row->claseCosto_id,'claseCosto_id','1:tbl_contclasecosto:claseCosto_id:ccost_nombre') }} </td>
						
					</tr>
				
					<tr>
						<td width='30%' class='label-view text-right'>{{ SiteHelpers::activeLang('Signature Status', (isset($fields['estado_firma']['language'])? $fields['estado_firma']['language'] : array())) }}</td>
						<td>{{ $row->estado_firma}} </td>
						
					</tr>
				
					<tr>
						<td width='30%' class='label-view text-right'>{{ SiteHelpers::activeLang('Signature Date', (isset($fields['firma_fecha']['language'])? $fields['firma_fecha']['language'] : array())) }}</td>
						<td>{{ $row->firma_fecha}} </td>
						
					</tr>
				
			</tbody>	
		</table>  
			
		 	

@if($setting['form-method'] =='native')
	</div>	
</div>	
@endif	

<script>
$(document).ready(function(){

});
</script>	