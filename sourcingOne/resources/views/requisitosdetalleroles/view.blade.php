@if($setting['view-method'] =='native')
<div class="sbox">
	<div class="sbox-title">  
		<h4> <i class="fa fa-table"></i> <?php echo $pageTitle ;?> <small>{{ $pageNote }}</small>
			<a href="javascript:void(0)" class="collapse-close pull-right btn btn-xs btn-danger" onclick="ajaxViewClose('#{{ $pageModule }}')">
			<i class="fa fa fa-times"></i></a>
		</h4>
	 </div>

	<div class="sbox-content"> 
@endif	

		<table class="table table-striped table-bordered" >
			<tbody>	
				
					<tr>
						<td width='30%' class='label-view text-right'>{{ SiteHelpers::activeLang('IdRequisitoDetalle', (isset($fields['IdRequisitoDetalle']['language'])? $fields['IdRequisitoDetalle']['language'] : array())) }}</td>
						<td>{{ $row->IdRequisitoDetalle}} </td>
						
					</tr>
				
					<tr>
						<td width='30%' class='label-view text-right'>{{ SiteHelpers::activeLang('IdRequisito', (isset($fields['IdRequisito']['language'])? $fields['IdRequisito']['language'] : array())) }}</td>
						<td>{{ $row->IdRequisito}} </td>
						
					</tr>
				
					<tr>
						<td width='30%' class='label-view text-right'>{{ SiteHelpers::activeLang('Entidad', (isset($fields['Entidad']['language'])? $fields['Entidad']['language'] : array())) }}</td>
						<td>{{ $row->Entidad}} </td>
						
					</tr>
				
					<tr>
						<td width='30%' class='label-view text-right'>{{ SiteHelpers::activeLang('Rol', (isset($fields['IdEntidad']['language'])? $fields['IdEntidad']['language'] : array())) }}</td>
						<td>{{ SiteHelpers::formatLookUp($row->IdEntidad,'IdEntidad','1:tbl_roles:IdRol:Descripción') }} </td>
						
					</tr>
				
			</tbody>	
		</table>  
			
		 	

@if($setting['form-method'] =='native')
	</div>	
</div>	
@endif	

<script>
$(document).ready(function(){

});
</script>	