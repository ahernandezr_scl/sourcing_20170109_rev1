<!DOCTYPE html>
<html>
<head>
<?php
$servername = "localhost";
$username = "root";
$password = "8044302";
$year;
$mes;$reg;

#try {
    $conn2 = new PDO("mysql:host=$servername;dbname=andina_koadmin", $username, $password);
    // set the PDO error mode to exception
#    $conn2->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
#   echo "Connected successfully";
#    }
#catch(PDOException $e)
#    {
#    echo "Connection failed: " . $e->getMessage();
#    }


?>

<?php  
  if (isset($_GET["year"])) {
    $year = $_GET["year"];
   }
  else{  
    $year = date("Y");
}

  if (isset($_GET["mes"])) {
    $mes = $_GET["mes"];
   }
  else{  
    $mes = 0;
}

 if (isset($_GET["reg"])) {
    $reg = $_GET["reg"];
   }
  else{  
    $reg = 0;
}

 if (isset($_GET["seg"])) {
    $seg = $_GET["seg"];
   }
  else{  
    $seg = 0;
}

 if (isset($_GET["area"])) {
    $area = $_GET["area"];
   }
  else{  
    $area = 0;
}
?>
<?php
$nom_mes;
    if ($mes==1){
        $nom_mes = 'Ene';
    }
    else  if ($mes==2){
        $nom_mes = 'Feb';
    }
    else  if ($mes==3){
        $nom_mes = 'Mar';
    }
    else  if ($mes==4){
        $nom_mes = 'Abr';
    }
    else  if ($mes==5){
        $nom_mes = 'May';
    }
    else  if ($mes==6){
        $nom_mes = 'Jun';
    }
    else  if ($mes==7){
        $nom_mes = 'Jul';
    }
    else  if ($mes==8){
        $nom_mes = 'Ago';
    }
    else  if ($mes==9){
        $nom_mes = 'Sep';
    }
    else  if ($mes==10){
        $nom_mes = 'Oct';
    }
    else  if ($mes==11){
        $nom_mes = 'Nov';
    }
    else  if ($mes==12){
        $nom_mes = 'Dic';
    }
   

?>

<b>Lista de Contratos</b>
<button type="button" class="btn btn-secondary  pull-right" onclick="myFunction()"> Panel de Control</button>
<br>
<br>
    <div class="content" align="center">
    <form method="post">
   <div style="display: inline-block;width:70px; height:30px;">
      <b>Año</b>
                     <select name="sl_year" id="sl_year" onchange="contratos(this.value,1)">

<?php
                   $sqlyear = ' select distinct Anio from dim_tiempo 
                              where anio between year(current_Date) -5
                              and YEAR(CASE WHEN DAY(CURRENT_DATE) >= 20 THEN DATE_ADD(DATE_ADD(CURRENT_DATE, INTERVAL -1 MONTH), INTERVAL -(DAY(CURRENT_DATE)-1) DAY)  ELSE DATE_ADD(DATE_ADD(CURRENT_DATE, INTERVAL -2 MONTH), INTERVAL -(DAY(CURRENT_DATE)-1) DAY) END)
                              order by anio desc
                            ';
                     $i=0;
                    
                    foreach ($conn2->query($sqlyear) as $row1) {
                           if($row1["Anio"]==$year){
                              echo "<option  value='".$row1["Anio"]."' selected>".$row1["Anio"]."</option>"; 
                           }
                           else
                              echo "<option  value='".$row1["Anio"]."'>".$row1["Anio"]."</option>"; 
                          $i++;
                    }
                    
            ?>

                    </select>

       
       </div>  
       <div style="display: inline-block; width:80px; height:30px;">
            
           <b>Mes</b>
                     <select name="sl_mes" id="sl_mes" onchange="contratos(this.value,2)">

<?php


                   $sqlmes = ' SELECT 0 mes, \'Todos\' NMes3L
                   UNION ALL
                   SELECT DISTINCT mes , NMes3L  FROM dim_tiempo 
                   WHERE fecha_mes <= CASE WHEN DAY(CURRENT_DATE) >= 20 THEN DATE_ADD(DATE_ADD(CURRENT_DATE, INTERVAL -1 MONTH), INTERVAL -(DAY(CURRENT_DATE)-1) DAY)  ELSE DATE_ADD(DATE_ADD(CURRENT_DATE, INTERVAL -2 MONTH), INTERVAL -(DAY(CURRENT_DATE)-1) DAY) END
                   AND  anio = '.$year.' 
                   ORDER BY mes';
                     $i=0;
                    
                    foreach ($conn2->query($sqlmes) as $row1) {
                           if($row1["mes"]==$mes){
                              echo "<option  value='".$row1["mes"]."' selected>".$row1["NMes3L"]."</option>"; 
                           }
                           else
                              echo "<option  value='".$row1["mes"]."'>".$row1["NMes3L"]."</option>"; 
                          $i++;
                    }
                    
            ?>

                   
                    </select>
</div>
  <div style="display: inline-block;width:120px; height:30px;">
                <b>Tipo Faena</b>
                     <select name="sl_reg" id="sl_reg" onchange="contratos(this.value,3)">

<?php
                   $sqlreg = ' SELECT 0 id, \'Todos\' reg
                   UNION ALL
                   SELECT geo_id, geo_nombre FROM tbl_contgeografico';
                     $i=0;
                    
                    foreach ($conn2->query($sqlreg) as $row1) {
                           if($row1["id"]==$reg){
                              echo "<option  value='".$row1["id"]."' selected>".$row1["reg"]."</option>"; 
                           }
                           else
                              echo "<option  value='".$row1["id"]."'>".$row1["reg"]."</option>"; 
                          $i++;
                    }
                    
            ?>

                   
                    </select>
</div>
 <div style="display: inline-block;width:280px; height:30px;">
                <b>Área</b>
                     <select name="sl_area" id="sl_area" onchange="contratos(this.value,4)">

<?php
                   $sqlarea = ' SELECT 0 id, \'Todos\' area 
                   union all
                   SELECT afuncional_id ,afuncional_nombre area FROM tbl_contareafuncional';
                     $i=0;
                    
                    foreach ($conn2->query($sqlarea) as $row1) {
                           if($row1["id"]==$area){
                              echo "<option  value='".$row1["id"]."' selected>".$row1["area"]."</option>"; 
                           }
                           else
                              echo "<option  value='".$row1["id"]."'>".$row1["area"]."</option>"; 
                          $i++;
                    }
                    
            ?>

                   
                    </select>
        </div>
        <div style="display: inline-block;width:280px; height:30px;">

                    <b>Segmento</b>
                     <select name="sl_seg" id="sl_seg" onchange="contratos(this.value,5)">

<?php
                   $sqlseg = ' SELECT 0 id, \'Todos\' nombre
                   union all
                   SELECT segmento_id, seg_nombre FROM tbl_contsegmento';
                     $i=0;
                    
                    foreach ($conn2->query($sqlseg) as $row1) {
                           if($row1["id"]==$seg){
                              echo "<option  value='".$row1["id"]."' selected>".$row1["nombre"]."</option>"; 
                           }
                           else
                              echo "<option  value='".$row1["id"]."'>".$row1["nombre"]."</option>"; 
                          $i++;
                    }
                    
            ?>

                   
                    </select>
          </div>
           <div style="display: inline-block;width:100px; height:30px;">
                <button type="reset" class="btn-xs btn-default " onclick="contratos('0',6)">Limpiar</button>
          </div>
        </form>
        </div>

    <br>
 



<div class="container"  id="tbl_cont" style="background-color: white">
<div id="myOverlay"></div>
<div id="loadingGIF"> <img  src="https://www.mintbox.com/images/loading3.gif" align = "top" height="50%" width="50%"></div>

<dim id="fecha_repo">
   <br><?php 
if ($mes == 0)
  echo '<h3>Año Completo '.$year.'</h3>';
else
  echo '<h3>'.$nom_mes.'-'.$year.'</h3>';
?>
    
     <br>
  </dim>
   <br>
<?php

$sql_cont = 'SELECT  c.contrato_id,cont_numero,cont_proveedor,
CAST(AVG(n) AS UNSIGNED) general
FROM
(
SELECT contrato_id,CAST(COALESCE(AVG(prom_neg),0) AS UNSIGNED) n
FROM
(
SELECT contrato_id,CAST(AVG(kpi) AS DECIMAL(4,1)) prom_neg

FROM
(SELECT  b.contrato_id,kpiDet_fecha,  (SUM(
    CASE WHEN kpi_tipo_calc = 1 THEN
      CASE WHEN kpiDet_puntaje >= kpiDet_meta THEN 1 ELSE 0 END
       WHEN kpi_tipo_calc = 2 THEN
    CASE WHEN kpiDet_puntaje <= kpiDet_meta THEN 1 ELSE 0 END
       WHEN kpi_tipo_calc = 3 THEN
    CASE WHEN kpiDet_puntaje BETWEEN kpiDet_min AND kpiDet_max THEN 1 ELSE 0 END
       WHEN kpi_tipo_calc = 4 THEN 1
      
  END )/
COUNT(DISTINCT a.id_kpi) )*100 kpi
FROM tbl_kpimensual a INNER JOIN tbl_kpigral b
ON a.id_kpi = b.id_kpi
INNER JOIN dim_tiempo ON kpiDet_fecha = fecha
INNER JOIN tbl_contrato c ON c.contrato_id = b.contrato_id
WHERE kpiDet_puntaje IS NOT NULL
AND 
anio = '.$year.' AND 
CASE WHEN '.$mes.'= 0 THEN 1=1 ELSE  mes = '.$mes.' END
AND CASE WHEN '.$reg.'= 0 THEN 1=1 ELSE geo_id = '.$reg.' END
AND CASE WHEN '.$seg.'= 0 THEN 1=1 ELSE segmento_id = '.$seg.' END
AND CASE WHEN '.$area.'= 0 THEN 1=1 ELSE afuncional_id = '.$area.' END
GROUP BY 1,2) asd
GROUP BY 1

UNION ALL

SELECT a.contrato_id,
CASE WHEN AVG(eval_puntaje) >70 THEN 75 + ((CASE WHEN AVG(eval_puntaje) >70 THEN 25/30 ELSE 75/70 END)*(AVG(eval_puntaje) - 70)) 
ELSE 75 - ((CASE WHEN AVG(eval_puntaje) >70 THEN 25/30 ELSE 75/70 END)*(AVG(eval_puntaje) - 70)) END f
FROM tbl_evalcontratistas a INNER JOIN dim_tiempo 
ON eval_fecha = fecha 
INNER JOIN tbl_contrato b ON a.contrato_id = b.contrato_id
WHERE  
eval_puntaje IS NOT NULL
AND 
anio = '.$year.' AND 
CASE WHEN '.$mes.'= 0 THEN 1=1 ELSE  mes = '.$mes.' END
AND CASE WHEN '.$reg.'= 0 THEN 1=1 ELSE geo_id = '.$reg.' END
AND CASE WHEN '.$seg.'= 0 THEN 1=1 ELSE segmento_id = '.$seg.' END
AND CASE WHEN '.$area.'= 0 THEN 1=1 ELSE afuncional_id = '.$area.' END
GROUP BY 1
UNION ALL

SELECT a.contrato_id,CAST(AVG(resultado) AS DECIMAL(4,1)) puntaje
FROM tbl_calidad_adm_cont a INNER JOIN dim_tiempo 
ON fec_rev = fecha
INNER JOIN tbl_contrato b ON a.contrato_id = b.contrato_id
WHERE resultado IS NOT NULL
AND 
anio = '.$year.' AND 
CASE WHEN '.$mes.'= 0 THEN 1=1 ELSE  mes = '.$mes.' END
AND CASE WHEN '.$reg.'= 0 THEN 1=1 ELSE geo_id = '.$reg.' END
AND CASE WHEN '.$seg.'= 0 THEN 1=1 ELSE segmento_id = '.$seg.' END
AND CASE WHEN '.$area.'= 0 THEN 1=1 ELSE afuncional_id = '.$area.' END
GROUP BY 1
) ambito
GROUP BY 1

UNION ALL 

-- financiero

SELECT contrato_id, CAST(COALESCE(AVG(prom_fin),0) AS UNSIGNED) f 
FROM
(SELECT a.contrato_id,
CASE WHEN AVG(valor) > 50 THEN 75 - (AVG(valor) -50) * (CASE WHEN AVG(valor) > 50 THEN 75/50 ELSE 25/50 END) ELSE
75 + (AVG(valor) -50) * (CASE WHEN AVG(valor) > 50 THEN 75/50 ELSE 25/50 END) END prom_fin
FROM tbl_eval_financiera a
INNER JOIN tbl_contrato c ON c.contrato_id = a.contrato_id
INNER JOIN dim_tiempo d ON fecha_validez = d.fecha
WHERE
anio = '.$year.' AND 
CASE WHEN '.$mes.'= 0 THEN 1=1 ELSE  mes = '.$mes.' END
AND CASE WHEN '.$reg.'= 0 THEN 1=1 ELSE geo_id = '.$reg.' END
AND CASE WHEN '.$seg.'= 0 THEN 1=1 ELSE segmento_id = '.$seg.' END
AND CASE WHEN '.$area.'= 0 THEN 1=1 ELSE afuncional_id = '.$area.' END
GROUP BY 1
UNION ALL

SELECT a.contrato_id,
CASE WHEN AVG(per_moros*100) > 30 THEN 75 - ABS(AVG(per_moros*100) -30) * (CASE WHEN AVG(per_moros*100) > 30 THEN 75/67 ELSE 25/13 END) ELSE
75 + ABS(AVG(per_moros*100) -30) * (CASE WHEN AVG(per_moros*100) > 30 THEN 75/67 ELSE 25/13 END) END prom_trib
FROM tbl_morosidad a
INNER JOIN dim_tiempo d ON fecha_Eval = d.fecha
INNER JOIN tbl_contrato c ON c.contrato_id = a.contrato_id
WHERE 

anio = '.$year.' AND 
CASE WHEN '.$mes.'= 0 THEN 1=1 ELSE  mes = '.$mes.' END
AND CASE WHEN '.$reg.'= 0 THEN 1=1 ELSE geo_id = '.$reg.' END
AND CASE WHEN '.$seg.'= 0 THEN 1=1 ELSE segmento_id = '.$seg.' END
AND CASE WHEN '.$area.'= 0 THEN 1=1 ELSE afuncional_id = '.$area.' END
GROUP BY 1
UNION ALL

SELECT a.contrato_id,(1 -( COUNT(DISTINCT (CASE WHEN tCont_eval = \'MALO\' THEN a.contrato_id ELSE 0 END))/ 
COUNT(DISTINCT a.contrato_id)) )*100
FROM
tbl_contratista_tributario a INNER JOIN tbl_contrato b ON a.contrato_id = b.contrato_id
INNER JOIN tbl_contratista_financiero c ON a.contrato_id = c.contrato_id
INNER JOIN dim_tiempo d ON a.tCont_fecha = d.fecha
WHERE 

anio = '.$year.' AND 
CASE WHEN '.$mes.'= 0 THEN 1=1 ELSE  mes = '.$mes.' END
AND CASE WHEN '.$reg.'= 0 THEN 1=1 ELSE geo_id = '.$reg.' END
AND CASE WHEN '.$seg.'= 0 THEN 1=1 ELSE segmento_id = '.$seg.' END
AND CASE WHEN '.$area.'= 0 THEN 1=1 ELSE afuncional_id = '.$area.' END
GROUP BY 1
UNION ALL

SELECT  a.contrato_id,AVG((total_fondos + prendas)/(mutuos + total_impacto))*100
FROM tbl_garantias a INNER JOIN dim_tiempo b
ON a.fecha = b.fecha
INNER JOIN tbl_contrato c ON c.contrato_id = a.contrato_id
WHERE  

anio = '.$year.' AND 
CASE WHEN '.$mes.'= 0 THEN 1=1 ELSE  mes = '.$mes.' END
AND CASE WHEN '.$reg.'= 0 THEN 1=1 ELSE geo_id = '.$reg.' END
AND CASE WHEN '.$seg.'= 0 THEN 1=1 ELSE segmento_id = '.$seg.' END
AND CASE WHEN '.$area.'= 0 THEN 1=1 ELSE afuncional_id = '.$area.' END
GROUP BY 1
) finan
GROUP BY 1
UNION ALL

-- laboral

SELECT contrato_id,CAST(COALESCE(AVG(prom_lab),0) AS UNSIGNED) l
FROM
(SELECT a.contrato_id,
CASE WHEN AVG(valor)*100 > 16 THEN 75 - (ABS(AVG(valor)*100 -16)) * (CASE WHEN AVG(valor)*100 > 16 THEN 75/84 ELSE 25/16 END) ELSE
75 + (ABS(AVG(valor)*100 -16)) * (CASE WHEN AVG(valor)*100 > 16 THEN 75/84 ELSE 25/16 END) END prom_lab
FROM tbl_obliglab_repo a INNER JOIN dim_tiempo b
ON a.fecha = b.fecha
INNER JOIN tbl_contrato c ON c.contrato_id = a.contrato_id
WHERE 
anio = '.$year.' AND 
CASE WHEN '.$mes.'= 0 THEN 1=1 ELSE  mes = '.$mes.' END
AND CASE WHEN '.$reg.'= 0 THEN 1=1 ELSE geo_id = '.$reg.' END
AND CASE WHEN '.$seg.'= 0 THEN 1=1 ELSE segmento_id = '.$seg.' END
AND CASE WHEN '.$area.'= 0 THEN 1=1 ELSE afuncional_id = '.$area.' END
GROUP BY 1
UNION ALL

SELECT  a.contrato_id,((SELECT COUNT(DISTINCT contrato_id) FROM tbl_contrato WHERE cont_fechaFin > CURRENT_DATE) - 
COUNT(DISTINCT a.contrato_id))/(SELECT COUNT(DISTINCT contrato_id) FROM tbl_contrato WHERE cont_fechaFin > CURRENT_DATE) * 100
FROM tbl_contratistacondicionfin a
INNER JOIN dim_tiempo b
ON a.finCont_fecha = b.fecha
INNER JOIN tbl_contrato c ON c.contrato_id = a.contrato_id
WHERE 
anio = '.$year.' AND 
CASE WHEN '.$mes.'= 0 THEN 1=1 ELSE  mes = '.$mes.' END
AND CASE WHEN '.$reg.'= 0 THEN 1=1 ELSE geo_id = '.$reg.' END
AND CASE WHEN '.$seg.'= 0 THEN 1=1 ELSE segmento_id = '.$seg.' END
AND CASE WHEN '.$area.'= 0 THEN 1=1 ELSE afuncional_id = '.$area.' END
GROUP BY 1
UNION ALL

SELECT  a.contrato_id,CAST(AVG((PERC_LAB)) AS DECIMAL(4,1)) puntaje
FROM tbl_fiscalización a INNER JOIN dim_tiempo b
ON a.fecha = b.fecha
INNER JOIN tbl_contrato c ON c.contrato_id = a.contrato_id
WHERE 
anio = '.$year.' AND 
CASE WHEN '.$mes.'= 0 THEN 1=1 ELSE  mes = '.$mes.' END
AND CASE WHEN '.$reg.'= 0 THEN 1=1 ELSE geo_id = '.$reg.' END
AND CASE WHEN '.$seg.'= 0 THEN 1=1 ELSE segmento_id = '.$seg.' END
AND CASE WHEN '.$area.'= 0 THEN 1=1 ELSE afuncional_id = '.$area.' END
GROUP BY 1) lab 
GROUP BY 1
UNION ALL

-- seguridad

SELECT contrato_id, CAST(COALESCE(AVG(prom_seg),0) AS DECIMAL(3,0)) s
FROM
(SELECT a.contrato_id,
CASE WHEN (SUM(n_accid)/SUM(hh)*200000)> 5.2 THEN 0 ELSE
CASE WHEN (SUM(n_accid)/SUM(hh)*200000)> 2.6 THEN 75 - ABS((SUM(n_accid)/SUM(hh)*200000)-2.6) * (CASE WHEN (SUM(n_accid)/SUM(hh)*200000) > 2.6 THEN 75/2.6 ELSE 25/2.6 END) ELSE
75 + ABS((SUM(n_accid)/SUM(hh)*200000) -2.6) * (CASE WHEN (SUM(n_accid)/SUM(hh)*200000) > 2.6 THEN 75/2.6 ELSE 25/2.6 END) END
END prom_seg
FROM tbl_accidentes a
INNER JOIN dim_tiempo d ON a.fecha_informe = d.fecha
INNER JOIN tbl_contrato c ON a.contrato_id = c.contrato_id
WHERE 
anio = '.$year.' AND 
CASE WHEN '.$mes.'= 0 THEN 1=1 ELSE  mes = '.$mes.' END
AND CASE WHEN '.$reg.'= 0 THEN 1=1 ELSE geo_id = '.$reg.' END
AND CASE WHEN '.$seg.'= 0 THEN 1=1 ELSE segmento_id = '.$seg.' END
AND CASE WHEN '.$area.'= 0 THEN 1=1 ELSE afuncional_id = '.$area.' END
GROUP BY 1
UNION ALL

SELECT a.contrato_id,
CASE WHEN (SUM(DIAS_PERD)/SUM(hh)*200000) > 60 THEN 0 ELSE 
CASE WHEN (SUM(DIAS_PERD)/SUM(hh)*200000)> 31.1 THEN 75 - ABS(( SUM(DIAS_PERD)/SUM(hh)*200000 )-31.1) * (CASE WHEN (SUM(DIAS_PERD)/SUM(hh)*200000) > 31.1 THEN 75/31.1 ELSE 25/31.1 END) ELSE
75 + ABS((SUM(DIAS_PERD)/SUM(hh)*200000) -31.1) * (CASE WHEN (SUM(DIAS_PERD)/SUM(hh)*200000) > 31.1 THEN 75/31.1 ELSE 25/31.1 END) END 
END f
FROM tbl_accidentes a
INNER JOIN dim_tiempo d ON a.fecha_informe = d.fecha
INNER JOIN tbl_contrato c ON a.contrato_id = c.contrato_id
WHERE 
anio = '.$year.' AND 
CASE WHEN '.$mes.'= 0 THEN 1=1 ELSE  mes = '.$mes.' END
AND CASE WHEN '.$reg.'= 0 THEN 1=1 ELSE geo_id = '.$reg.' END
AND CASE WHEN '.$seg.'= 0 THEN 1=1 ELSE segmento_id = '.$seg.' END
AND CASE WHEN '.$area.'= 0 THEN 1=1 ELSE afuncional_id = '.$area.' END
GROUP BY 1
UNION ALL

SELECT a.contrato_id,AVG((PERC_COND + perc_equip)/2) 
FROM tbl_fiscalización a
INNER JOIN dim_tiempo d ON a.fecha = d.fecha
INNER JOIN tbl_contrato c ON a.contrato_id = c.contrato_id
 WHERE 
anio = '.$year.' AND 
CASE WHEN '.$mes.'= 0 THEN 1=1 ELSE  mes = '.$mes.' END
AND CASE WHEN '.$reg.'= 0 THEN 1=1 ELSE geo_id = '.$reg.' END
AND CASE WHEN '.$seg.'= 0 THEN 1=1 ELSE segmento_id = '.$seg.' END
AND CASE WHEN '.$area.'= 0 THEN 1=1 ELSE afuncional_id = '.$area.' END
GROUP BY 1) seg
GROUP BY 1
) panel
INNER JOIN tbl_contrato c ON panel.contrato_id = c.contrato_id
GROUP BY 1,2,3;';

     $i=0;
                    $k=0;
                    echo "<table class=\"table table-striped\">";
                    foreach ($conn2->query($sql_cont) as $row1) {
                           
                    
                    
                                   
                    if($k==3){
                      $k=0;
                      echo "</tr>";
                    }

                    if($k==0){
                       echo "<tr>";
                    }
                   
                                              
                        if($row1['general'] < 75)
                        {
                           echo "<td> <div class=\"circle_red\">".$row1['general']."%</div></td>";
                        }
                        else if($row1['general'] >= 75)
                        {
                           echo "<td> <div class=\"circle_green\">".$row1['general']."%</div></td>";
                        }
                        else
                        {
                          echo "<td> <div class=\"circle_grey\">".$row1['general']."%</div></td>";
                        }
                         echo "<td> <a href=\"http://one.sourcing.cl/reportdetcontrato?id=".$row1['contrato_id']."&mes=".$mes."&year=".$year.'&mes='.$mes.'&area='.$area.'&reg='.$reg.'&seg='.$seg."\">".$row1['cont_numero']." - ".$row1['cont_proveedor']."</a></td>";
                       
                      $k++;
               
                    }
                    echo "</tr>";
                    echo "</table>";
                  


?>
</div>
