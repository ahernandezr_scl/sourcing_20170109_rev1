@if($setting['view-method'] =='native')
<div class="sbox">
	<div class="sbox-title">  
		<h4> <i class="fa fa-table"></i> <?php echo $pageTitle ;?> <small>{{ $pageNote }}</small>
			<a href="javascript:void(0)" class="collapse-close pull-right btn btn-xs btn-danger" onclick="ajaxViewClose('#{{ $pageModule }}')">
			<i class="fa fa fa-times"></i></a>
		</h4>
	 </div>

	<div class="sbox-content"> 
@endif	

		<table class="table table-striped table-bordered" >
			<tbody>	
				
					<tr>
						<td width='30%' class='label-view text-right'>{{ SiteHelpers::activeLang('IdCargaDocumento', (isset($fields['IdCargaDocumento']['language'])? $fields['IdCargaDocumento']['language'] : array())) }}</td>
						<td>{{ $row->IdCargaDocumento}} </td>
						
					</tr>
				
					<tr>
						<td width='30%' class='label-view text-right'>{{ SiteHelpers::activeLang('IdTipoDocumento', (isset($fields['IdTipoDocumento']['language'])? $fields['IdTipoDocumento']['language'] : array())) }}</td>
						<td>{{ $row->IdTipoDocumento}} </td>
						
					</tr>
				
					<tr>
						<td width='30%' class='label-view text-right'>{{ SiteHelpers::activeLang('Archivo', (isset($fields['Archivo']['language'])? $fields['Archivo']['language'] : array())) }}</td>
						<td>{{ $row->Archivo}} </td>
						
					</tr>
				
					<tr>
						<td width='30%' class='label-view text-right'>{{ SiteHelpers::activeLang('ArchivoTipo', (isset($fields['ArchivoTipo']['language'])? $fields['ArchivoTipo']['language'] : array())) }}</td>
						<td>{{ $row->ArchivoTipo}} </td>
						
					</tr>
				
					<tr>
						<td width='30%' class='label-view text-right'>{{ SiteHelpers::activeLang('ArchivoPeso', (isset($fields['ArchivoPeso']['language'])? $fields['ArchivoPeso']['language'] : array())) }}</td>
						<td>{{ $row->ArchivoPeso}} </td>
						
					</tr>
				
					<tr>
						<td width='30%' class='label-view text-right'>{{ SiteHelpers::activeLang('ArchivoTexto', (isset($fields['ArchivoTexto']['language'])? $fields['ArchivoTexto']['language'] : array())) }}</td>
						<td>{{ $row->ArchivoTexto}} </td>
						
					</tr>
				
					<tr>
						<td width='30%' class='label-view text-right'>{{ SiteHelpers::activeLang('CreatedOn', (isset($fields['createdOn']['language'])? $fields['createdOn']['language'] : array())) }}</td>
						<td>{{ $row->createdOn}} </td>
						
					</tr>
				
					<tr>
						<td width='30%' class='label-view text-right'>{{ SiteHelpers::activeLang('Entry By', (isset($fields['entry_by']['language'])? $fields['entry_by']['language'] : array())) }}</td>
						<td>{{ $row->entry_by}} </td>
						
					</tr>
				
					<tr>
						<td width='30%' class='label-view text-right'>{{ SiteHelpers::activeLang('UpdatedOn', (isset($fields['updatedOn']['language'])? $fields['updatedOn']['language'] : array())) }}</td>
						<td>{{ $row->updatedOn}} </td>
						
					</tr>
				
					<tr>
						<td width='30%' class='label-view text-right'>{{ SiteHelpers::activeLang('IdEstatus', (isset($fields['IdEstatus']['language'])? $fields['IdEstatus']['language'] : array())) }}</td>
						<td>{{ $row->IdEstatus}} </td>
						
					</tr>
				
			</tbody>	
		</table>  
			
		 	

@if($setting['form-method'] =='native')
	</div>	
</div>	
@endif	

<script>
$(document).ready(function(){

});
</script>	