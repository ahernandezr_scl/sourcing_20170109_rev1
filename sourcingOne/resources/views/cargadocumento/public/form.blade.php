

		 {!! Form::open(array('url'=>'cargadocumento/savepublic', 'class'=>'form-horizontal','files' => true , 'parsley-validate'=>'','novalidate'=>' ')) !!}

	@if(Session::has('messagetext'))
	  
		   {!! Session::get('messagetext') !!}
	   
	@endif
	<ul class="parsley-error-list">
		@foreach($errors->all() as $error)
			<li>{{ $error }}</li>
		@endforeach
	</ul>		


<div class="col-md-12">
						<fieldset><legend> Carga documento</legend>
									
									  <div class="form-group  " >
										<label for="IdCargaDocumento" class=" control-label col-md-4 text-left"> IdCargaDocumento </label>
										<div class="col-md-6">
										  {!! Form::text('IdCargaDocumento', $row['IdCargaDocumento'],array('class'=>'form-control', 'placeholder'=>'',   )) !!} 
										 </div> 
										 <div class="col-md-2">
										 	
										 </div>
									  </div> 					
									  <div class="form-group  " >
										<label for="IdTipoDocumento" class=" control-label col-md-4 text-left"> IdTipoDocumento </label>
										<div class="col-md-6">
										  <select name='IdTipoDocumento' rows='5' id='IdTipoDocumento' class='select2 '   ></select> 
										 </div> 
										 <div class="col-md-2">
										 	
										 </div>
									  </div> 					
									  <div class="form-group  " >
										<label for="Archivo" class=" control-label col-md-4 text-left"> Archivo </label>
										<div class="col-md-6">
										  <textarea name='Archivo' rows='5' id='Archivo' class='form-control '  
				           >{{ $row['Archivo'] }}</textarea> 
										 </div> 
										 <div class="col-md-2">
										 	
										 </div>
									  </div> 					
									  <div class="form-group  " >
										<label for="ArchivoTipo" class=" control-label col-md-4 text-left"> ArchivoTipo </label>
										<div class="col-md-6">
										  {!! Form::text('ArchivoTipo', $row['ArchivoTipo'],array('class'=>'form-control', 'placeholder'=>'',   )) !!} 
										 </div> 
										 <div class="col-md-2">
										 	
										 </div>
									  </div> 					
									  <div class="form-group  " >
										<label for="ArchivoPeso" class=" control-label col-md-4 text-left"> ArchivoPeso </label>
										<div class="col-md-6">
										  {!! Form::text('ArchivoPeso', $row['ArchivoPeso'],array('class'=>'form-control', 'placeholder'=>'',   )) !!} 
										 </div> 
										 <div class="col-md-2">
										 	
										 </div>
									  </div> 					
									  <div class="form-group  " >
										<label for="ArchivoTexto" class=" control-label col-md-4 text-left"> ArchivoTexto </label>
										<div class="col-md-6">
										  <textarea name='ArchivoTexto' rows='5' id='ArchivoTexto' class='form-control '  
				           >{{ $row['ArchivoTexto'] }}</textarea> 
										 </div> 
										 <div class="col-md-2">
										 	
										 </div>
									  </div> 					
									  <div class="form-group  " >
										<label for="CreatedOn" class=" control-label col-md-4 text-left"> CreatedOn </label>
										<div class="col-md-6">
										  
				<div class="input-group m-b" style="width:150px !important;">
					{!! Form::text('createdOn', $row['createdOn'],array('class'=>'form-control datetime', 'style'=>'width:150px !important;')) !!}
					<span class="input-group-addon"><i class="fa fa-calendar"></i></span>
				</div>
				 
										 </div> 
										 <div class="col-md-2">
										 	
										 </div>
									  </div> 					
									  <div class="form-group  " >
										<label for="Entry By" class=" control-label col-md-4 text-left"> Entry By </label>
										<div class="col-md-6">
										  {!! Form::text('entry_by', $row['entry_by'],array('class'=>'form-control', 'placeholder'=>'',   )) !!} 
										 </div> 
										 <div class="col-md-2">
										 	
										 </div>
									  </div> 					
									  <div class="form-group  " >
										<label for="UpdatedOn" class=" control-label col-md-4 text-left"> UpdatedOn </label>
										<div class="col-md-6">
										  
				<div class="input-group m-b" style="width:150px !important;">
					{!! Form::text('updatedOn', $row['updatedOn'],array('class'=>'form-control datetime', 'style'=>'width:150px !important;')) !!}
					<span class="input-group-addon"><i class="fa fa-calendar"></i></span>
				</div>
				 
										 </div> 
										 <div class="col-md-2">
										 	
										 </div>
									  </div> 					
									  <div class="form-group  " >
										<label for="IdEstatus" class=" control-label col-md-4 text-left"> IdEstatus </label>
										<div class="col-md-6">
										  {!! Form::text('IdEstatus', $row['IdEstatus'],array('class'=>'form-control', 'placeholder'=>'',   )) !!} 
										 </div> 
										 <div class="col-md-2">
										 	
										 </div>
									  </div> </fieldset>
			</div>
			
			

			<div style="clear:both"></div>	
				
					
				  <div class="form-group">
					<label class="col-sm-4 text-right">&nbsp;</label>
					<div class="col-sm-8">	
					<button type="submit" name="apply" class="btn btn-info btn-sm" ><i class="fa  fa-check-circle"></i> {{ Lang::get('core.sb_apply') }}</button>
					<button type="submit" name="submit" class="btn btn-primary btn-sm" ><i class="fa  fa-save "></i> {{ Lang::get('core.sb_save') }}</button>
				  </div>	  
			
		</div> 
		 
		 {!! Form::close() !!}
		 
   <script type="text/javascript">
	$(document).ready(function() { 
		
		
		$("#IdTipoDocumento").jCombo("{!! url('cargadocumento/comboselect?filter=tbl_tipos_documentos:IdTipoDocumento:IdTipoDocumento') !!}",
		{  selected_value : '{{ $row["IdTipoDocumento"] }}' });
		 

		$('.removeCurrentFiles').on('click',function(){
			var removeUrl = $(this).attr('href');
			$.get(removeUrl,function(response){});
			$(this).parent('div').empty();	
			return false;
		});		
		
	});
	</script>		 
