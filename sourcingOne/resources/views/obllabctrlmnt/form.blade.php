@extends('layouts.app')

@section('content')

  <div class="page-content row">

 	<div class="page-content-wrapper m-t">


<div class="sbox">
	<div class="sbox-title"> <h3> {{ $pageTitle }} <small>{{ $pageNote }}</small></h3> </div>
	<div class="sbox-content"> 	

		<ul class="parsley-error-list">
			@foreach($errors->all() as $error)
				<li>{{ $error }}</li>
			@endforeach
		</ul>	

		 {!! Form::open(array('url'=>'obllabctrlmnt/save?return='.$return, 'class'=>'form-horizontal','files' => true , 'parsley-validate'=>'','novalidate'=>' ')) !!}
<div class="col-md-12">
						<fieldset><legend> Obligaciones Laborales</legend>
				
									  <div class="form-group  " >
										<label for="Repo Id" class=" control-label col-md-4 text-left"> Repo Id </label>
										<div class="col-md-6">
										  {!! Form::text('repo_id', $row['repo_id'],array('class'=>'form-control', 'placeholder'=>'',   )) !!}
										 </div>
										 <div class="col-md-2">
										 	
										 </div>
									  </div> 
									  <div class="form-group  " >
										<label for="Contrato Id" class=" control-label col-md-4 text-left"> Contrato Id </label>
										<div class="col-md-6">
										  {!! Form::text('contrato_id', $row['contrato_id'],array('class'=>'form-control', 'placeholder'=>'',   )) !!}
										 </div>
										 <div class="col-md-2">
										 	
										 </div>
									  </div> 
									  <div class="form-group  " >
										<label for="Fecha" class=" control-label col-md-4 text-left"> Fecha </label>
										<div class="col-md-6">
										  {!! Form::text('fecha', $row['fecha'],array('class'=>'form-control', 'placeholder'=>'',   )) !!}
										 </div>
										 <div class="col-md-2">
										 	
										 </div>
									  </div> 
									  <div class="form-group  " >
										<label for="Valor" class=" control-label col-md-4 text-left"> Valor </label>
										<div class="col-md-6">
										  {!! Form::text('valor', $row['valor'],array('class'=>'form-control', 'placeholder'=>'',   )) !!}
										 </div>
										 <div class="col-md-2">
										 	
										 </div>
									  </div> </fieldset>
			</div>

			

		
			<div style="clear:both"></div>	
				
					
				  <div class="form-group">
					<label class="col-sm-4 text-right">&nbsp;</label>
					<div class="col-sm-8">	
					<button type="submit" name="apply" class="btn btn-info btn-sm" ><i class="icon-checkmark-circle2"></i> {{ Lang::get('core.sb_apply') }}</button>
					<button type="submit" name="submit" class="btn btn-primary btn-sm" ><i class="icon-bubble-check"></i> {{ Lang::get('core.sb_save') }}</button>
					<button type="button" onclick="location.href='{{ URL::to('obllabctrlmnt?return='.$return) }}' " class="btn btn-warning btn-sm "><i class="icon-cancel-circle2 "></i>  {{ Lang::get('core.sb_cancel') }} </button>
					</div>	  
			
				  </div>
		 
		 {!! Form::close() !!}
	</div>
</div>		 
</div>	
</div>			 
   <script type="text/javascript">
	$(document).ready(function() { 
		
		 
		
		$('.removeMultiFiles').on('click',function(){
			var removeUrl = '{{ url("obllabctrlmnt/removefiles?file=")}}'+$(this).attr('url');
			$(this).parent().remove();
			$.get(removeUrl,function(response){});
			$(this).parent('div').empty();	
			return false;
		});		
		
	});
	</script>		 
@stop