@if($setting['view-method'] =='native')
<div class="sbox">
	<div class="sbox-title">  
		<h4> <i class="fa fa-table"></i> <?php echo $pageTitle ;?> <small>{{ $pageNote }}</small>
			<a href="javascript:void(0)" class="collapse-close pull-right btn btn-xs btn-danger" onclick="ajaxViewClose('#{{ $pageModule }}')">
			<i class="fa fa fa-times"></i></a>
		</h4>
	 </div>

	<div class="sbox-content"> 
@endif	

		<table class="table table-striped table-bordered" >
			<tbody>	
				
					<tr>
						<td width='30%' class='label-view text-right'>{{ SiteHelpers::activeLang('Itemd Id', (isset($fields['itemd_id']['language'])? $fields['itemd_id']['language'] : array())) }}</td>
						<td>{{ $row->itemd_id}} </td>
						
					</tr>
				
					<tr>
						<td width='30%' class='label-view text-right'>{{ SiteHelpers::activeLang('Contrato Id', (isset($fields['contrato_id']['language'])? $fields['contrato_id']['language'] : array())) }}</td>
						<td>{{ $row->contrato_id}} </td>
						
					</tr>
				
					<tr>
						<td width='30%' class='label-view text-right'>{{ SiteHelpers::activeLang('Itemd Zona', (isset($fields['itemd_zona']['language'])? $fields['itemd_zona']['language'] : array())) }}</td>
						<td>{{ $row->itemd_zona}} </td>
						
					</tr>
				
					<tr>
						<td width='30%' class='label-view text-right'>{{ SiteHelpers::activeLang('Itemd Producto', (isset($fields['itemd_producto']['language'])? $fields['itemd_producto']['language'] : array())) }}</td>
						<td>{{ $row->itemd_producto}} </td>
						
					</tr>
				
					<tr>
						<td width='30%' class='label-view text-right'>{{ SiteHelpers::activeLang('Itemd Fecha', (isset($fields['itemd_fecha']['language'])? $fields['itemd_fecha']['language'] : array())) }}</td>
						<td>{{ $row->itemd_fecha}} </td>
						
					</tr>
				
					<tr>
						<td width='30%' class='label-view text-right'>{{ SiteHelpers::activeLang('Itemd Consumo', (isset($fields['itemd_consumo']['language'])? $fields['itemd_consumo']['language'] : array())) }}</td>
						<td>{{ $row->itemd_consumo}} </td>
						
					</tr>
				
					<tr>
						<td width='30%' class='label-view text-right'>{{ SiteHelpers::activeLang('Itemd ProdReal', (isset($fields['itemd_prodReal']['language'])? $fields['itemd_prodReal']['language'] : array())) }}</td>
						<td>{{ $row->itemd_prodReal}} </td>
						
					</tr>
				
					<tr>
						<td width='30%' class='label-view text-right'>{{ SiteHelpers::activeLang('Itemd TiempoReal', (isset($fields['itemd_tiempoReal']['language'])? $fields['itemd_tiempoReal']['language'] : array())) }}</td>
						<td>{{ $row->itemd_tiempoReal}} </td>
						
					</tr>
				
			</tbody>	
		</table>  
			
		 	

@if($setting['form-method'] =='native')
	</div>	
</div>	
@endif	

<script>
$(document).ready(function(){

});
</script>	