

		 {!! Form::open(array('url'=>'tbldiverseydetalle/savepublic', 'class'=>'form-horizontal','files' => true , 'parsley-validate'=>'','novalidate'=>' ')) !!}

	@if(Session::has('messagetext'))
	  
		   {!! Session::get('messagetext') !!}
	   
	@endif
	<ul class="parsley-error-list">
		@foreach($errors->all() as $error)
			<li>{{ $error }}</li>
		@endforeach
	</ul>		


<div class="col-md-12">
						<fieldset><legend> itemized productive detail</legend>
									
									  <div class="form-group  " >
										<label for="Itemd Id" class=" control-label col-md-4 text-left"> Itemd Id </label>
										<div class="col-md-6">
										  {!! Form::text('itemd_id', $row['itemd_id'],array('class'=>'form-control', 'placeholder'=>'',   )) !!} 
										 </div> 
										 <div class="col-md-2">
										 	
										 </div>
									  </div> 					
									  <div class="form-group  " >
										<label for="Contrato Id" class=" control-label col-md-4 text-left"> Contrato Id </label>
										<div class="col-md-6">
										  {!! Form::text('contrato_id', $row['contrato_id'],array('class'=>'form-control', 'placeholder'=>'',   )) !!} 
										 </div> 
										 <div class="col-md-2">
										 	
										 </div>
									  </div> 					
									  <div class="form-group  " >
										<label for="Itemd Zona" class=" control-label col-md-4 text-left"> Itemd Zona </label>
										<div class="col-md-6">
										  {!! Form::text('itemd_zona', $row['itemd_zona'],array('class'=>'form-control', 'placeholder'=>'',   )) !!} 
										 </div> 
										 <div class="col-md-2">
										 	
										 </div>
									  </div> 					
									  <div class="form-group  " >
										<label for="Itemd Producto" class=" control-label col-md-4 text-left"> Itemd Producto </label>
										<div class="col-md-6">
										  {!! Form::text('itemd_producto', $row['itemd_producto'],array('class'=>'form-control', 'placeholder'=>'',   )) !!} 
										 </div> 
										 <div class="col-md-2">
										 	
										 </div>
									  </div> 					
									  <div class="form-group  " >
										<label for="Itemd Fecha" class=" control-label col-md-4 text-left"> Itemd Fecha </label>
										<div class="col-md-6">
										  {!! Form::text('itemd_fecha', $row['itemd_fecha'],array('class'=>'form-control', 'placeholder'=>'',   )) !!} 
										 </div> 
										 <div class="col-md-2">
										 	
										 </div>
									  </div> 					
									  <div class="form-group  " >
										<label for="Itemd Consumo" class=" control-label col-md-4 text-left"> Itemd Consumo </label>
										<div class="col-md-6">
										  {!! Form::text('itemd_consumo', $row['itemd_consumo'],array('class'=>'form-control', 'placeholder'=>'',   )) !!} 
										 </div> 
										 <div class="col-md-2">
										 	
										 </div>
									  </div> 					
									  <div class="form-group  " >
										<label for="Itemd ProdReal" class=" control-label col-md-4 text-left"> Itemd ProdReal </label>
										<div class="col-md-6">
										  {!! Form::text('itemd_prodReal', $row['itemd_prodReal'],array('class'=>'form-control', 'placeholder'=>'',   )) !!} 
										 </div> 
										 <div class="col-md-2">
										 	
										 </div>
									  </div> 					
									  <div class="form-group  " >
										<label for="Itemd TiempoReal" class=" control-label col-md-4 text-left"> Itemd TiempoReal </label>
										<div class="col-md-6">
										  {!! Form::text('itemd_tiempoReal', $row['itemd_tiempoReal'],array('class'=>'form-control', 'placeholder'=>'',   )) !!} 
										 </div> 
										 <div class="col-md-2">
										 	
										 </div>
									  </div> </fieldset>
			</div>
			
			

			<div style="clear:both"></div>	
				
					
				  <div class="form-group">
					<label class="col-sm-4 text-right">&nbsp;</label>
					<div class="col-sm-8">	
					<button type="submit" name="apply" class="btn btn-info btn-sm" ><i class="fa  fa-check-circle"></i> {{ Lang::get('core.sb_apply') }}</button>
					<button type="submit" name="submit" class="btn btn-primary btn-sm" ><i class="fa  fa-save "></i> {{ Lang::get('core.sb_save') }}</button>
				  </div>	  
			
		</div> 
		 
		 {!! Form::close() !!}
		 
   <script type="text/javascript">
	$(document).ready(function() { 
		
		 

		$('.removeCurrentFiles').on('click',function(){
			var removeUrl = $(this).attr('href');
			$.get(removeUrl,function(response){});
			$(this).parent('div').empty();	
			return false;
		});		
		
	});
	</script>		 
