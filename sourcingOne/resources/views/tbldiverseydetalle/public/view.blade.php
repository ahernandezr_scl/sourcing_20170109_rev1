<div class="m-t" style="padding-top:25px;">	
    <div class="row m-b-lg animated fadeInDown delayp1 text-center">
        <h3> {{ $pageTitle }} <small> {{ $pageNote }} </small></h3>
        <hr />       
    </div>
</div>
<div class="m-t">
	<div class="table-responsive" > 	

		<table class="table table-striped table-bordered" >
			<tbody>	
		
			
					<tr>
						<td width='30%' class='label-view text-right'>{{ SiteHelpers::activeLang('Itemd Id', (isset($fields['itemd_id']['language'])? $fields['itemd_id']['language'] : array())) }}</td>
						<td>{{ $row->itemd_id}} </td>
						
					</tr>
				
					<tr>
						<td width='30%' class='label-view text-right'>{{ SiteHelpers::activeLang('Contrato Id', (isset($fields['contrato_id']['language'])? $fields['contrato_id']['language'] : array())) }}</td>
						<td>{{ $row->contrato_id}} </td>
						
					</tr>
				
					<tr>
						<td width='30%' class='label-view text-right'>{{ SiteHelpers::activeLang('Itemd Zona', (isset($fields['itemd_zona']['language'])? $fields['itemd_zona']['language'] : array())) }}</td>
						<td>{{ $row->itemd_zona}} </td>
						
					</tr>
				
					<tr>
						<td width='30%' class='label-view text-right'>{{ SiteHelpers::activeLang('Itemd Producto', (isset($fields['itemd_producto']['language'])? $fields['itemd_producto']['language'] : array())) }}</td>
						<td>{{ $row->itemd_producto}} </td>
						
					</tr>
				
					<tr>
						<td width='30%' class='label-view text-right'>{{ SiteHelpers::activeLang('Itemd Fecha', (isset($fields['itemd_fecha']['language'])? $fields['itemd_fecha']['language'] : array())) }}</td>
						<td>{{ $row->itemd_fecha}} </td>
						
					</tr>
				
					<tr>
						<td width='30%' class='label-view text-right'>{{ SiteHelpers::activeLang('Itemd Consumo', (isset($fields['itemd_consumo']['language'])? $fields['itemd_consumo']['language'] : array())) }}</td>
						<td>{{ $row->itemd_consumo}} </td>
						
					</tr>
				
					<tr>
						<td width='30%' class='label-view text-right'>{{ SiteHelpers::activeLang('Itemd ProdReal', (isset($fields['itemd_prodReal']['language'])? $fields['itemd_prodReal']['language'] : array())) }}</td>
						<td>{{ $row->itemd_prodReal}} </td>
						
					</tr>
				
					<tr>
						<td width='30%' class='label-view text-right'>{{ SiteHelpers::activeLang('Itemd TiempoReal', (isset($fields['itemd_tiempoReal']['language'])? $fields['itemd_tiempoReal']['language'] : array())) }}</td>
						<td>{{ $row->itemd_tiempoReal}} </td>
						
					</tr>
						
					<tr>
						<td width='30%' class='label-view text-right'></td>
						<td> <a href="javascript:history.go(-1)" class="btn btn-primary"> Back To Grid <a> </td>
						
					</tr>					
				
			</tbody>	
		</table>   

	 
	
	</div>
</div>	