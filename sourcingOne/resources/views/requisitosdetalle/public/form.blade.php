

		 {!! Form::open(array('url'=>'requisitosdetalle/savepublic', 'class'=>'form-horizontal','files' => true , 'parsley-validate'=>'','novalidate'=>' ')) !!}

	@if(Session::has('messagetext'))
	  
		   {!! Session::get('messagetext') !!}
	   
	@endif
	<ul class="parsley-error-list">
		@foreach($errors->all() as $error)
			<li>{{ $error }}</li>
		@endforeach
	</ul>		


<div class="col-md-12">
						<fieldset><legend> RequisitosRoles</legend>
									
									  <div class="form-group  " >
										<label for="IdRequisitoDetalle" class=" control-label col-md-4 text-left"> IdRequisitoDetalle </label>
										<div class="col-md-6">
										  {!! Form::text('IdRequisitoDetalle', $row['IdRequisitoDetalle'],array('class'=>'form-control', 'placeholder'=>'',   )) !!} 
										 </div> 
										 <div class="col-md-2">
										 	
										 </div>
									  </div> 					
									  <div class="form-group  " >
										<label for="IdRequisito" class=" control-label col-md-4 text-left"> IdRequisito </label>
										<div class="col-md-6">
										  <select name='IdRequisito' rows='5' id='IdRequisito' class='select2 '   ></select> 
										 </div> 
										 <div class="col-md-2">
										 	
										 </div>
									  </div> 					
									  <div class="form-group  " >
										<label for="Entidad" class=" control-label col-md-4 text-left"> Entidad </label>
										<div class="col-md-6">
										  {!! Form::text('Entidad', $row['Entidad'],array('class'=>'form-control', 'placeholder'=>'',   )) !!} 
										 </div> 
										 <div class="col-md-2">
										 	
										 </div>
									  </div> 					
									  <div class="form-group  " >
										<label for="IdEntidad" class=" control-label col-md-4 text-left"> IdEntidad </label>
										<div class="col-md-6">
										  <select name='IdEntidad[]' multiple rows='5' id='IdEntidad' class='select2 '   ></select> 
										 </div> 
										 <div class="col-md-2">
										 	
										 </div>
									  </div> </fieldset>
			</div>
			
			

			<div style="clear:both"></div>	
				
					
				  <div class="form-group">
					<label class="col-sm-4 text-right">&nbsp;</label>
					<div class="col-sm-8">	
					<button type="submit" name="apply" class="btn btn-info btn-sm" ><i class="fa  fa-check-circle"></i> {{ Lang::get('core.sb_apply') }}</button>
					<button type="submit" name="submit" class="btn btn-primary btn-sm" ><i class="fa  fa-save "></i> {{ Lang::get('core.sb_save') }}</button>
				  </div>	  
			
		</div> 
		 
		 {!! Form::close() !!}
		 
   <script type="text/javascript">
	$(document).ready(function() { 
		
		
		$("#IdRequisito").jCombo("{!! url('requisitosdetalle/comboselect?filter=tbl_requisitos:IdRequisito:IdRequisito') !!}",
		{  selected_value : '{{ $row["IdRequisito"] }}' });
		
		$("#IdEntidad").jCombo("{!! url('requisitosdetalle/comboselect?filter=tbl_roles:IdRol:Descripción') !!}",
		{  selected_value : '{{ $row["IdEntidad"] }}' });
		 

		$('.removeCurrentFiles').on('click',function(){
			var removeUrl = $(this).attr('href');
			$.get(removeUrl,function(response){});
			$(this).parent('div').empty();	
			return false;
		});		
		
	});
	</script>		 
