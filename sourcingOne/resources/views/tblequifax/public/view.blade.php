<div class="m-t" style="padding-top:25px;">	
    <div class="row m-b-lg animated fadeInDown delayp1 text-center">
        <h3> {{ $pageTitle }} <small> {{ $pageNote }} </small></h3>
        <hr />       
    </div>
</div>
<div class="m-t">
	<div class="table-responsive" > 	

		<table class="table table-striped table-bordered" >
			<tbody>	
		
			
					<tr>
						<td width='30%' class='label-view text-right'>{{ SiteHelpers::activeLang('Contract', (isset($fields['contrato_id']['language'])? $fields['contrato_id']['language'] : array())) }}</td>
						<td>{{ SiteHelpers::formatLookUp($row->contrato_id,'contrato_id','1:tbl_contrato:contrato_id:cont_proveedor') }} </td>
						
					</tr>
				
					<tr>
						<td width='30%' class='label-view text-right'>{{ SiteHelpers::activeLang('Date of data', (isset($fields['fec_rev']['language'])? $fields['fec_rev']['language'] : array())) }}</td>
						<td>{{ date('d-m-Y',strtotime($row->fec_rev)) }} </td>
						
					</tr>
				
					<tr>
						<td width='30%' class='label-view text-right'>{{ SiteHelpers::activeLang('Docto unpaid', (isset($fields['tot_doc_imp']['language'])? $fields['tot_doc_imp']['language'] : array())) }}</td>
						<td>{{ $row->tot_doc_imp}} </td>
						
					</tr>
				
					<tr>
						<td width='30%' class='label-view text-right'>{{ SiteHelpers::activeLang('Unpaid Amount', (isset($fields['monto_imp']['language'])? $fields['monto_imp']['language'] : array())) }}</td>
						<td>{{ $row->monto_imp}} </td>
						
					</tr>
				
					<tr>
						<td width='30%' class='label-view text-right'>{{ SiteHelpers::activeLang('Contract Predictor', (isset($fields['pred_emp']['language'])? $fields['pred_emp']['language'] : array())) }}</td>
						<td>{{ $row->pred_emp}} </td>
						
					</tr>
				
					<tr>
						<td width='30%' class='label-view text-right'>{{ SiteHelpers::activeLang('Size contract', (isset($fields['tamano']['language'])? $fields['tamano']['language'] : array())) }}</td>
						<td>{{ $row->tamano}} </td>
						
					</tr>
				
					<tr>
						<td width='30%' class='label-view text-right'>{{ SiteHelpers::activeLang('Heritage', (isset($fields['patrimonio']['language'])? $fields['patrimonio']['language'] : array())) }}</td>
						<td>{{ $row->patrimonio}} </td>
						
					</tr>
				
					<tr>
						<td width='30%' class='label-view text-right'>{{ SiteHelpers::activeLang('Contract range', (isset($fields['rango_emp']['language'])? $fields['rango_emp']['language'] : array())) }}</td>
						<td>{{ $row->rango_emp}} </td>
						
					</tr>
				
					<tr>
						<td width='30%' class='label-view text-right'>{{ SiteHelpers::activeLang('Pledges without transfer', (isset($fields['prendas_sin_desp']['language'])? $fields['prendas_sin_desp']['language'] : array())) }}</td>
						<td>{{ $row->prendas_sin_desp}} </td>
						
					</tr>
				
					<tr>
						<td width='30%' class='label-view text-right'>{{ SiteHelpers::activeLang('Bank count', (isset($fields['num_bancos']['language'])? $fields['num_bancos']['language'] : array())) }}</td>
						<td>{{ $row->num_bancos}} </td>
						
					</tr>
				
					<tr>
						<td width='30%' class='label-view text-right'>{{ SiteHelpers::activeLang('Details', (isset($fields['detalle']['language'])? $fields['detalle']['language'] : array())) }}</td>
						<td>{{ $row->detalle}} </td>
						
					</tr>
						
					<tr>
						<td width='30%' class='label-view text-right'></td>
						<td> <a href="javascript:history.go(-1)" class="btn btn-primary"> Back To Grid <a> </td>
						
					</tr>					
				
			</tbody>	
		</table>   

	 
	
	</div>
</div>	