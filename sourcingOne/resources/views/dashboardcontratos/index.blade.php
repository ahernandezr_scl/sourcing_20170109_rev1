@extends('layouts.app')
@section('content')

<?php
$servername = "localhost";
$username = "root";
$password = "8044302";

#try {
    $conn2 = new PDO("mysql:host=$servername;dbname=andina_koadmin", $username, $password);
    // set the PDO error mode to exception
#    $conn2->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
#   echo "Connected successfully";
#    }
#catch(PDOException $e)
#    {
#    echo "Connection failed: " . $e->getMessage();
#    }


 if (isset($_GET["year"])) {
    $year = $_GET["year"];
   }
  else{ 
	$year = date("Y");
	   
}

  if (isset($_GET["mes"])) {
    $mes = $_GET["mes"];
   }
 else
		{
			$mes = 0;
		}
	


 if (isset($_GET["reg"])) {
    $reg = $_GET["reg"];
   }
  else{  
    $reg = 0;
}

 if (isset($_GET["seg"])) {
    $seg = $_GET["seg"];
   }
  else{  
    $seg = 0;
}

 if (isset($_GET["area"])) {
    $area = $_GET["area"];
   }
  else{  
    $area = 0;
}

$nom_mes;
    if ($mes==1){
        $nom_mes = 'Ene';
    }
    else  if ($mes==2){
        $nom_mes = 'Feb';
    }
    else  if ($mes==3){
        $nom_mes = 'Mar';
    }
    else  if ($mes==4){
        $nom_mes = 'Abr';
    }
    else  if ($mes==5){
        $nom_mes = 'May';
    }
    else  if ($mes==6){
        $nom_mes = 'Jun';
    }
    else  if ($mes==7){
        $nom_mes = 'Jul';
    }
    else  if ($mes==8){
        $nom_mes = 'Ago';
    }
    else  if ($mes==9){
        $nom_mes = 'Sep';
    }
    else  if ($mes==10){
        $nom_mes = 'Oct';
    }
    else  if ($mes==11){
        $nom_mes = 'Nov';
    }
    else  if ($mes==12){
        $nom_mes = 'Dic';
    }
   $color_v = '#3d9b35';
   $color_r = '#e64427';
   $color_n = '#FFC000';


?>
<style>
      #embed-container {
                position: relative;
                padding-bottom: 56.25%;
                height: 0;
                overflow: hidden;
        }
        #embed-container iframe {
                position: absolute;
                top: 0;
                left: 0;
                width: 100%;
                height: 100%;
        }

#myOverlay{position:absolute;height:100%;width:100%;}
#myOverlay{background:white;opacity:.7;z-index:2;display:none;}

#loadingGIF{position:absolute;top:40%;left:45%;z-index:3;display:none;}

</style>

@if(Auth::check() && Auth::user()->group_id == 1)

    <script src="https://code.highcharts.com/highcharts.js"></script>
    <script src="https://code.highcharts.com/modules/exporting.js"></script>
    <script src="http://code.highcharts.com/highcharts-more.js"></script>
                                <tr>
                                    <td> <!--{{ Auth::user()->id }}--></td>
                                      <!--{{ Auth::user()->id }}-->
                              </tr>
    <div class="embed-container">
      <!-- -->
	  
	  	  <div class="content" align="center" id="filtros">
     <form method="post" id="filter">
   <div style="display: inline-block;width:70px; height:30px;">
      <b>Año</b>
                     <select name="sl_year" id="sl_year" onchange="filtros_s(this.value,1)">

<?php
                   $sqlyear = ' select distinct Anio from dim_tiempo 
                              where anio between year(current_Date) -3
                              and YEAR(CURRENT_DATE)
                              order by anio desc
                            ';
                     $i=0;
                    
                    foreach ($conn2->query($sqlyear) as $row1) {
                           if($row1["Anio"]==$year){
                              echo "<option  value='".$row1["Anio"]."' selected>".$row1["Anio"]."</option>"; 
                           }
                           else
                              echo "<option  value='".$row1["Anio"]."'>".$row1["Anio"]."</option>"; 
                          $i++;
                    }
                    
            ?>

                    </select>

       
       </div>  
       <div style="display: inline-block; width:80px; height:30px;">
            
           <b>Mes</b>
                     <select name="sl_mes" id="sl_mes" onchange="filtros_s(this.value,2)">

<?php


                   $sqlmes = ' SELECT 0 mes, \'Todos\' NMes3L
                   UNION ALL
                   SELECT DISTINCT mes , NMes3L  FROM dim_tiempo 
                   WHERE fecha_mes <= CURRENT_DATE
                   AND  anio = '.$year.' 
                   ORDER BY mes';
                     $i=0;
                    
                    foreach ($conn2->query($sqlmes) as $row1) {
                           if($row1["mes"]==$mes){
                              echo "<option  value='".$row1["mes"]."' selected>".$row1["NMes3L"]."</option>"; 
                           }
                           else
                              echo "<option  value='".$row1["mes"]."'>".$row1["NMes3L"]."</option>"; 
                          $i++;
                    }
                    
            ?>

                   
                    </select>
</div>
  <div style="display: inline-block;width:120px; height:30px;">
                <b>Tipo Faena</b>
                     <select name="sl_reg" id="sl_reg" onchange="filtros_s(this.value,3)">

<?php
                   $sqlreg = ' SELECT 0 id, \'Todos\' reg
                   UNION ALL
                   SELECT geo_id, geo_nombre FROM tbl_contgeografico';
                     $i=0;
                    
                    foreach ($conn2->query($sqlreg) as $row1) {
                           if($row1["id"]==$reg){
                              echo "<option  value='".$row1["id"]."' selected>".$row1["reg"]."</option>"; 
                           }
                           else
                              echo "<option  value='".$row1["id"]."'>".$row1["reg"]."</option>"; 
                          $i++;
                    }
                    
            ?>

                   
                    </select>
</div>
 <div style="display: inline-block;width:280px; height:30px;">
                <b>Área</b>
                     <select name="sl_area" id="sl_area" onchange="filtros_s(this.value,4)">

<?php
                   $sqlarea = ' SELECT 0 id, \'Todos\' area 
                   union all
                   SELECT afuncional_id ,afuncional_nombre area FROM tbl_contareafuncional';
                     $i=0;
                    
                    foreach ($conn2->query($sqlarea) as $row1) {
                           if($row1["id"]==$area){
                              echo "<option  value='".$row1["id"]."' selected>".$row1["area"]."</option>"; 
                           }
                           else
                              echo "<option  value='".$row1["id"]."'>".$row1["area"]."</option>"; 
                          $i++;
                    }
                    
            ?>

                   
                    </select>
        </div>
        <div style="display: inline-block;width:280px; height:30px;">

                    <b>Segmento</b>
                     <select name="sl_seg" id="sl_seg" onchange="filtros_s(this.value,5)">

<?php
                   $sqlseg = ' SELECT 0 id, \'Todos\' nombre
                   union all
                   SELECT segmento_id, seg_nombre FROM tbl_contsegmento';
                     $i=0;
                    
                    foreach ($conn2->query($sqlseg) as $row1) {
                           if($row1["id"]==$seg){
                              echo "<option  value='".$row1["id"]."' selected>".$row1["nombre"]."</option>"; 
                           }
                           else
                              echo "<option  value='".$row1["id"]."'>".$row1["nombre"]."</option>"; 
                          $i++;
                    }
                    
            ?>

                   
                    </select>
          </div>
          <div style="display: inline-block;width:100px; height:30px;">
                <button type="reset" class="btn-xs btn-default " onclick="filtros_s('0',6)">Limpiar</button>
          </div>
          </form>
          
        </div>
		<div id="myOverlay"></div>
<div id="loadingGIF"> <img  src="https://www.mintbox.com/images/loading3.gif" align = "top" height="50%" width="50%"></div>

      <div class="row m-t">
        <div class="col-lg-12">
    <div class="row">

      <div class="col-xs-6 col-sm-2">
        <div class="sbox">
        <div class="pull-center">
                <!-- <h4 class="panel-title">Cantidad de Contratos</h4> -->
                <div class="sbox-title" align="center">
                    <!-- <span class="label label-success pull-right">Mes anterior</span> -->
                    <h4>Cantidad de Contratos</h4>
                </div>
                <div class="sbox-content" align="center">
                  <h1 id="cant_cont">{!! $contratos['cantidad'] !!}</h1>
                </div>
                <!-- <h5 class="text-success">2.00% increased</h5>-->
            </div>
        </div>
        </div>
        <div class="col-xs-6 col-sm-4">
        <div class="sbox">
        <div class="pull-center">
                <!-- <h4 class="panel-title">Cantidad de Contratos</h4> -->
                <div class="sbox-title" align="center">
                    <!-- <span class="label label-success pull-right">Mes anterior</span> -->
                    <h4>$ total de contratos</h4>
                </div>
                <div class="sbox-content" align="center">
                  <h1 id="monto_cont">M${!! number_format(round($contratos['total']/1000000, 0)) !!}</h1>
                </div>
                <!-- <h5 class="text-success">2.00% increased</h5>-->
            </div>
        </div>
        </div>
        <div class="col-xs-6 col-sm-3">
        <div class="sbox">
        <div class="pull-center">
                <!-- <h4 class="panel-title">Cantidad de Contratos</h4> -->
                <div class="sbox-title" align="center">
                    <!-- <span class="label label-success pull-right">Mes anterior</span> -->
                    <h4>Contratos Vigentes</h4>
                </div>
                <div class="sbox-content" align="center">
                  <h1 id="cont_cant_v">{!! $contratos['vigentes'] !!}</h1>
                </div>
                <!-- <h5 class="text-success">2.00% increased</h5>-->
            </div>
        </div>
        </div>
        <div class="col-xs-6 col-sm-3">
        <div class="sbox">
        <div class="pull-center">
                <!-- <h4 class="panel-title">Cantidad de Contratos</h4> -->
                <div class="sbox-title" align="center">
                    <!-- <span class="label label-success pull-right">Mes anterior</span> -->
                    <h4>Personas en contratos </h4>
                </div>
                <div class="sbox-content" align="center">
                  <h1 id ="pers_cont">{!! $contratos['personas'] !!}</h1>
                </div>
                <!-- <h5 class="text-success">2.00% increased</h5>-->
            </div>
        </div>
        </div>
        </div>
        </div>
      </div>

        <!-- -->


        <div class="row m-t">
          <div class="col-lg-12">
            <div class="row">

              <!-- Tabla Contratos -->
              <div class="col-lg-7">
                <div class="sbox">
                  <div class="sbox-title">
                    <span class="label label-success pull-right">Mes anterior</span>
                    <h4>Tabla</h4>
                  </div>
                  <div class="sbox-content">
                    <table class='table table-striped'>
                      <?php
                        echo "<tr style='font-weight:bold;'><td>Tipo</td>";
                        foreach ($tipoGastos as $gasto) {
                          echo "<td align='center' colspan='2' class='".strtolower(str_replace(' ', '', $gasto->nombre_gasto))." col' id='".strtolower(str_replace(' ', '', $gasto->nombre_gasto))."col'>".$gasto->nombre_gasto." </td>";
                        }
                        echo "<td align='center' colspan='2'>Total</td></tr><tr><td></td><td align='center'><b>Cant.</b></td><td align='center'><b>Valor ($)</b></td><td align='center'><b>Cant.</b></td><td align='center'><b>Valor ($)</b></td><td align='center'><b>Cant.</b></td><td align='center'><b>Valor ($)</b></td></tr><tr>";
                        foreach ($tipoExtensiones as $extension) {
                          echo "<td style='font-weight:bold' class='".strtolower(str_replace(' ', '', $extension->nombre))." row' id='".strtolower(str_replace(' ', '', $extension->nombre))."row'>".$extension->nombre."</td>";
                          foreach ($tipoGastos as $gasto) {
                            echo "<td align='center' class='".strtolower(str_replace(' ', '', $extension->nombre))." ".strtolower(str_replace(' ', '', $gasto->nombre_gasto))." cant' id='".strtolower(str_replace(' ', '', $extension->nombre)).strtolower(str_replace(' ', '', $gasto->nombre_gasto))."cant'>0</td><td align='center' class='".strtolower(str_replace(' ', '', $extension->nombre))." ".strtolower(str_replace(' ', '', $gasto->nombre_gasto))." valor' id='".strtolower(str_replace(' ', '', $extension->nombre)).strtolower(str_replace(' ', '', $gasto->nombre_gasto))."valor'>0</td>";
                          }
                          echo "<td align='center' class='".strtolower(str_replace(' ', '', $extension->nombre))." total cant' id='".strtolower(str_replace(' ', '', $extension->nombre))."totalcant'>0</td><td align='center' class='".strtolower(str_replace(' ', '', $extension->nombre))." total valor' id='".strtolower(str_replace(' ', '', $extension->nombre))."totalvalor'>0</td></tr>";
                        }
                        echo "<td style='font-weight:bold' id='row final'>Total</td>";
                        foreach ($tipoGastos as $gasto) {
                          echo "<td align='center' class='final ".strtolower(str_replace(' ', '', $gasto->nombre_gasto))." cant' id='final".strtolower(str_replace(' ', '', $gasto->nombre_gasto))."cant'>0</td><td align='center' class='final ".strtolower(str_replace(' ', '', $gasto->nombre_gasto))." valor' id='final".strtolower(str_replace(' ', '', $gasto->nombre_gasto))."valor'>0</td>";
                        }
                        echo "<td align='center' class='final total cant' id='finaltotalcant'>0</td><td align='center' class='final total valor' id='finaltotalvalor'>0</td></tr>";
                      ?>
                    </table>

                      <div class="stat-percent font-bold text-success">
                        <?php
                                $sql = 'SELECT Anio, NMes3L,
                                            YEAR(b.financieroand_fecha) AS AGNO
                                        ,   MONTH(b.financieroand_fecha) AS MES
                                        ,   REPLACE(REPLACE(REPLACE(FORMAT(SUM(b.financieroand_mtoreal), 0), ".", "@"), ",", "."), "@", ",") AS MtoPago FROM tbl_contrato AS a
                                        INNER JOIN tbl_financiero_and b ON a.contrato_id = b.contrato_id
                                        INNER JOIN dim_tiempo t ON b.financieroand_fecha = t.fecha
                                        WHERE
                                            b.financieroand_fecha = DATE_ADD(DATE_ADD(CURRENT_DATE, INTERVAL -2 MONTH), INTERVAL -(DAY(CURRENT_DATE)-1) DAY)
                                         ';
                            foreach ($conn2->query($sql) as $row) {
                                print $row['NMes3L']."-".$row['Anio']."\n";
                                }
                        ?><!--<i class="fa fa-level-up"></i>-->
                      </div>
                      <small>PERIODO</small>
                    </div>
                  </div><!-- /sbox -->
                </div><!-- </div class="col-lg-4"> -->
                <!-- Scatter Plot -->
                  <div class="col-lg-5">
                    <div class="sbox">
                      <div class="sbox-title">
                        <span class="label label-success pull-right">Mes anterior</span>
                        <h4><center>Riesgo de Negocios</h4>
                      </div>
                      <div class="sbox-content">
                        <div style="top: 10px;  left: 0;   width: 100%;" height="300px;background: white;">
                            <div id="scatterContainer" style="height: 185px; min-width: 310px; max-width: 600px; margin: 0 auto"></div>
                        </div>
                        <div class="stat-percent font-bold text-success">
                          <?php
                                  $sql = 'SELECT Anio, NMes3L,
                                              YEAR(b.financieroand_fecha) AS AGNO
                                          ,   MONTH(b.financieroand_fecha) AS MES
                                          ,   REPLACE(REPLACE(REPLACE(FORMAT(SUM(b.financieroand_mtoreal), 0), ".", "@"), ",", "."), "@", ",") AS MtoPago FROM tbl_contrato AS a
                                          INNER JOIN tbl_financiero_and b ON a.contrato_id = b.contrato_id
                                          INNER JOIN dim_tiempo t ON b.financieroand_fecha = t.fecha
                                          WHERE
                                              b.financieroand_fecha = DATE_ADD(DATE_ADD(CURRENT_DATE, INTERVAL -2 MONTH), INTERVAL -(DAY(CURRENT_DATE)-1) DAY)
                                           ';
                              foreach ($conn2->query($sql) as $row) {
                                  print $row['NMes3L']."-".$row['Anio']."\n";
                                  }
                          ?><!--<i class="fa fa-level-up"></i>-->
                        </div>
                        <small>PERIODO</small>
                      </div>
                    </div>
                  </div>
                <!-- /Scatter Plot -->

              </div>
              <!-- /Tabla Contratos -->
                        <div class="row m-t">
                          <div class="col-lg-6">
                            <div class="sbox">
                              <div class="sbox-title">
                                <span class="label label-success pull-right">Mes anterior</span>
                                <h4><center>Contratos por Tipo (Cantidad)</h4>
                              </div>
                              <div class="sbox-content">
                                <div style="top: 10px;  left: 0;   width: 100%;" height="300px;background: white;">
                                    <div id="donutContainer1" style="height: 185px; min-width: 310px; max-width: 600px; margin: 0 auto"></div>
                                </div>
                                <div class="stat-percent font-bold text-success">
                                  <?php
                                          $sql = 'SELECT Anio, NMes3L,
                                                      YEAR(b.financieroand_fecha) AS AGNO
                                                  ,   MONTH(b.financieroand_fecha) AS MES
                                                  ,   REPLACE(REPLACE(REPLACE(FORMAT(SUM(b.financieroand_mtoreal), 0), ".", "@"), ",", "."), "@", ",") AS MtoPago FROM tbl_contrato AS a
                                                  INNER JOIN tbl_financiero_and b ON a.contrato_id = b.contrato_id
                                                  INNER JOIN dim_tiempo t ON b.financieroand_fecha = t.fecha
                                                  WHERE
                                                      b.financieroand_fecha = DATE_ADD(DATE_ADD(CURRENT_DATE, INTERVAL -2 MONTH), INTERVAL -(DAY(CURRENT_DATE)-1) DAY)
                                                   ';
                                      foreach ($conn2->query($sql) as $row) {
                                          print $row['NMes3L']."-".$row['Anio']."\n";
                                          }
                                  ?><!--<i class="fa fa-level-up"></i>-->
                                </div>
                                <small>PERIODO</small>
                              </div>
                            </div>
                          </div>

                          <div class="col-lg-6">
                            <div class="sbox">
                              <div class="sbox-title">
                                <span class="label label-success pull-right">Mes anterior</span>
                                <h4><center>Contratos por Tipo ($)</h4>
                              </div>
                              <div class="sbox-content">
                                <div style="top: 10px;  left: 0;   width: 100%;" height="300px;background: white;">
                                    <div id="donutContainer2" style="height: 185px; min-width: 310px; max-width: 600px; margin: 0 auto"></div>
                                </div>
                                <div class="stat-percent font-bold text-success">
                                  <?php
                                          $sql = 'SELECT Anio, NMes3L,
                                                      YEAR(b.financieroand_fecha) AS AGNO
                                                  ,   MONTH(b.financieroand_fecha) AS MES
                                                  ,   REPLACE(REPLACE(REPLACE(FORMAT(SUM(b.financieroand_mtoreal), 0), ".", "@"), ",", "."), "@", ",") AS MtoPago FROM tbl_contrato AS a
                                                  INNER JOIN tbl_financiero_and b ON a.contrato_id = b.contrato_id
                                                  INNER JOIN dim_tiempo t ON b.financieroand_fecha = t.fecha
                                                  WHERE
                                                      b.financieroand_fecha = DATE_ADD(DATE_ADD(CURRENT_DATE, INTERVAL -2 MONTH), INTERVAL -(DAY(CURRENT_DATE)-1) DAY)
                                                   ';
                                      foreach ($conn2->query($sql) as $row) {
                                          print $row['NMes3L']."-".$row['Anio']."\n";
                                          }
                                  ?><!--<i class="fa fa-level-up"></i>-->
                                </div>
                                <small>PERIODO</small>
                              </div>
                            </div>
                          </div> <!--</div class="col-lg-12"> -->

                        </div><!-- </div class="row-mt"> -->

                        <div class="row m-t">
                          <div class="col-lg-6">
                            <div class="sbox">
                              <div class="sbox-title">
                                <span class="label label-success pull-right">Mes anterior</span>
                                    <h4><center>Contratos por Op/Inv (Cantidad)</h4>
                              </div>
                              <div class="sbox-content">
                                <div style="top: 10px;  left: 0;   width: 100%;" height="300px;background: white;">
                                    <div id="donutContainer3" style="height: 185px; min-width: 310px; max-width: 600px; margin: 0 auto"></div>
                                </div>
                                <div class="stat-percent font-bold text-success">
                                  <?php
                                          $sql = 'SELECT Anio, NMes3L,
                                                      YEAR(b.financieroand_fecha) AS AGNO
                                                  ,   MONTH(b.financieroand_fecha) AS MES
                                                  ,   REPLACE(REPLACE(REPLACE(FORMAT(SUM(b.financieroand_mtoreal), 0), ".", "@"), ",", "."), "@", ",") AS MtoPago FROM tbl_contrato AS a
                                                  INNER JOIN tbl_financiero_and b ON a.contrato_id = b.contrato_id
                                                  INNER JOIN dim_tiempo t ON b.financieroand_fecha = t.fecha
                                                  WHERE
                                                      b.financieroand_fecha = DATE_ADD(DATE_ADD(CURRENT_DATE, INTERVAL -2 MONTH), INTERVAL -(DAY(CURRENT_DATE)-1) DAY)
                                                   ';
                                      foreach ($conn2->query($sql) as $row) {
                                          print $row['NMes3L']."-".$row['Anio']."\n";
                                          }
                                  ?><!--<i class="fa fa-level-up"></i>-->
                                </div>
                                <small>PERIODO</small>
                              </div>
                            </div>
                          </div>

                          <div class="col-lg-6">
                            <div class="sbox">
                              <div class="sbox-title">
                                <span class="label label-success pull-right">Mes anterior</span>
                                    <h4><center>Contratos por Op/Inv ($)</h4>
                              </div>
                              <div class="sbox-content">
                                <div style="top: 10px;  left: 0;   width: 100%;" height="300px;background: white;">
                                    <div id="donutContainer4" style="height: 185px; min-width: 310px; max-width: 600px; margin: 0 auto"></div>
                                </div>
                                <div class="stat-percent font-bold text-success">
                                  <?php
                                          $sql = 'SELECT Anio, NMes3L,
                                                      YEAR(b.financieroand_fecha) AS AGNO
                                                  ,   MONTH(b.financieroand_fecha) AS MES
                                                  ,   REPLACE(REPLACE(REPLACE(FORMAT(SUM(b.financieroand_mtoreal), 0), ".", "@"), ",", "."), "@", ",") AS MtoPago FROM tbl_contrato AS a
                                                  INNER JOIN tbl_financiero_and b ON a.contrato_id = b.contrato_id
                                                  INNER JOIN dim_tiempo t ON b.financieroand_fecha = t.fecha
                                                  WHERE
                                                      b.financieroand_fecha = DATE_ADD(DATE_ADD(CURRENT_DATE, INTERVAL -2 MONTH), INTERVAL -(DAY(CURRENT_DATE)-1) DAY)
                                                   ';
                                      foreach ($conn2->query($sql) as $row) {
                                          print $row['NMes3L']."-".$row['Anio']."\n";
                                          }
                                  ?><!--<i class="fa fa-level-up"></i>-->
                                </div>
                                <small>PERIODO</small>
                              </div>
                            </div>
                          </div> <!--</div class="col-lg-12"> -->
                        </div>
                        <!-- </div class="col-lg-3" style="background-color:#ebebed;"> -->
                    </div>
            </div>


<script type="text/javascript">

    //Llenado inicial de tabla de Tipos de Gastos/Extensión
    $(function () {
      var tabla = {!! $jsCountAll !!};
      var contratos = {!! json_encode($contratos) !!};
      //console.log(tabla);

      for(var i = 0; i < tabla.length; i++){
        $('#'+tabla[i][1].toLowerCase().replace(/\s+/g, '')+tabla[i][0].toLowerCase().replace(/\s+/g, '')+'cant').html(tabla[i][2]);
        $('#'+tabla[i][1].toLowerCase().replace(/\s+/g, '')+tabla[i][0].toLowerCase().replace(/\s+/g, '')+'valor').html(tabla[i][3]);
      }
      console.log(contratos['extension']);
      var totalcant = 0;
      var totalvalor = 0;
      for(var i = 0; i < contratos['extension'].length; i++){
        $('#'+contratos['extension'][i]['nombre'].toLowerCase().replace(/\s+/g, '')+'totalcant').html(contratos['extension'][i]['cantidad']);
        totalcant += contratos['extension'][i]['cantidad'];
        $('#'+contratos['extension'][i]['nombre'].toLowerCase().replace(/\s+/g, '')+'totalvalor').html(contratos['extension'][i]['valor']);
        totalvalor += contratos['extension'][i]['valor'];
      }
      for(var i = 0; i < contratos['tipogasto'].length; i++){
        $('#final'+contratos['tipogasto'][i]['nombre'].toLowerCase().replace(/\s+/g, '')+'cant').html(contratos['tipogasto'][i]['cantidad']);
        $('#final'+contratos['tipogasto'][i]['nombre'].toLowerCase().replace(/\s+/g, '')+'valor').html(contratos['tipogasto'][i]['valor']);
      }
      $('#finaltotalcant').html(totalcant);
      $('#finaltotalvalor').html(totalvalor);
    });



$(function () {

    $('#scatterContainer').highcharts({

        chart: {
            type: 'bubble',
            plotBorderWidth: 1,
            zoomType: 'xy'
        },

        legend: {
            enabled: false
        },

        title: {
            text: false,
            x: -20 //center
        },

        /* subtitle: {
            text: 'Source: <a href="http://www.euromonitor.com/">Euromonitor</a> and <a href="https://data.oecd.org/">OECD</a>'
        }, */
        credits: {
           enabled: false
        },

        xAxis: {
            gridLineWidth: 1,
            title: {
                text: 'Impacto Negocio'
            },
            labels: {
                format: false
            },
            plotLines: [{
                color: 'black',
                dashStyle: 'dot',
                width: 2,
                value: 50,
                label: {
                    rotation: 0,
                    y: 15,
                    style: {
                        fontStyle: 'italic'
                    },
                    text: false
                },
                zIndex: 3
            }]
        },

        yAxis: {
            startOnTick: false,
            endOnTick: false,
            title: {
                text: 'Complejidad'
            },
            labels: {
                format: false
            },
            maxPadding: 0.2,
            plotLines: [{
                color: 'black',
                dashStyle: 'dot',
                width: 2,
                value: 50,
                label: {
                    align: 'right',
                    style: {
                        fontStyle: 'italic'
                    },
                    text: false,
                    x: -10
                },
                zIndex: 3
            }]
        },

        tooltip: {
            useHTML: true,
            headerFormat: '<table>',
            pointFormat: '<tr><th colspan="2"><h3>{point.empresa}</h3></th></tr>' +
                '<tr><th>Complejidad:</th><td>{point.x}</td></tr>' +
                '<tr><th>Impacto Negocio:</th><td>{point.y}</td></tr>' +
                '<tr><th>Tamaño contrato:</th><td>  ${point.z}</td></tr>',
            footerFormat: '</table>',
            followPointer: true
        },

        plotOptions: {
            series: {
                dataLabels: {
                    enabled: true,
                    format: '{point.name}'
                }
            }
        },

        series:[{}]
    });

    var chart = $('#scatterContainer').highcharts();
    <?php for($i = 0; $i < count($jsContratos); $i++){ ?>
      chart.series[0].addPoint({
      x: <?php echo $jsContratos[$i]['complejidad'] ?>,
      y: <?php echo $jsContratos[$i]['impacto'] ?>,
      z: <?php echo $jsContratos[$i]['cont_montoTotal'] ?>,
      name: "<?php echo $jsContratos[$i]['cont_nombre'] ?>",
      empresa: "<?php echo $jsContratos[$i]['cont_proveedor'] ?>",
      color: "red"
    });
    <?php } ?>
});


$(function () {
    var colors = Highcharts.getOptions().colors,
    <?php $aux1 = [];
     for($i = 0; $i < count($contratos['extension']); $i++){
       array_push($aux1, $contratos['extension'][$i]['nombre']);
     }
     ?>

     <?php $aux2 = [];
      for($i = 0; $i < count($contratos['tipogasto']); $i++){
        array_push($aux2, $contratos['tipogasto'][$i]['nombre']);
      }
      ?>

        categories = <?php echo "['".implode("', '", $aux1)."']"; ?>,

        data = [<?php for($i = 0; $i < count($contratos['extension']); $i++){ ?>{
              y: <?php echo $contratos['extension'][$i]['cantidad']; ?>,
              color: colors[<?php echo $i; ?>],
              drilldown: {
                  name: false,
                  categories: <?php echo "['".implode("', '", $aux2)."']"; ?>,
                  data: [<?php for($j = 0; $j < count($contratos['extension'][$i]['gastos']); $j++){echo $contratos['extension'][$i]['gastos'][$j]['cantidad'].", ";} ?>],
                  color: colors[<?php echo $i; ?>]
              }
          },
        <?php } ?>],
        browserData = [],
        versionsData = [],
        i,
        j,
        dataLen = data.length,
        drillDataLen,
        brightness;


    // Build the data arrays
    for (i = 0; i < dataLen; i += 1) {

        // add browser data
        browserData.push({
            name: categories[i],
            y: data[i].y,
            color: data[i].color,
            parentId: i
        });

        // add version data
        drillDataLen = data[i].drilldown.data.length;
        for (j = 0; j < drillDataLen; j += 1) {
            brightness = 0.2 - (j / drillDataLen) / 5;
            versionsData.push({
                name: data[i].drilldown.categories[j],
                y: data[i].drilldown.data[j],
                color: Highcharts.Color(data[i].color).brighten(brightness).get(),
                id: i
            });
        }
    }

    // Create the chart
    Highcharts.chart('donutContainer1', {
        chart: {
            type: 'pie'
        },
        title: {
            text: false
        },
        legend: {
          align: 'left',
          layout: 'vertical',
          verticalAlign: 'top',
          x: 0,
          y: 0
        },
        /* subtitle: {
            text: 'Source: <a href="http://netmarketshare.com/">netmarketshare.com</a>'
        },*/
        credits: {
           enabled: false
        },
        yAxis: {
            title: {
                text: 'Total contratos'
            }
        },
        plotOptions: {
            pie: {
                shadow: false,
                center: ['50%', '50%']
            },
            series: {
              point: {
                events: {
                    legendItemClick: function () {
                        var id = this.parentId,
                            data = this.series.chart.series[1].data;
                        $.each(data, function (i, point) {

                            if (point.id == id) {
                                if(point.visible)
                                    point.setVisible(false);
                                else
                                    point.setVisible(true);
                            }

                        });
                    }
                }
            }
        }
        },
        tooltip: {
            enabled: true
        },
        series: [{
            name: 'Contratos',
            data: browserData,
            size: '60%',
            showInLegend: true,
            dataLabels: {
                formatter: function () {
                    return this.point.name;
                },
                color: '#ffffff',
                distance: -30
            }
        }, {
            name: 'Tipo contratos',
            data: versionsData,
            size: '80%',
            innerSize: '60%',
            dataLabels: {
                formatter: function () {
                    // display only if larger than 1
                    return'<b>' + this.point.name + ':</b> ' + Highcharts.numberFormat(this.percentage, 1) + '%';
                }
            }
        }],

    });
});

$(function () {

    var colors = Highcharts.getOptions().colors,
    <?php $aux1 = [];
     for($i = 0; $i < count($contratos['extension']); $i++){
       array_push($aux1, $contratos['extension'][$i]['nombre']);
     }
     ?>

     <?php $aux2 = [];
      for($i = 0; $i < count($contratos['tipogasto']); $i++){
        array_push($aux2, $contratos['tipogasto'][$i]['nombre']);
      }
      ?>

        categories = <?php echo "['".implode("', '", $aux1)."']"; ?>,

        data = [<?php for($i = 0; $i < count($contratos['extension']); $i++){ ?>{
              y: <?php echo $contratos['extension'][$i]['valor']; ?>,
              color: colors[<?php echo $i; ?>],
              drilldown: {
                  name: false,
                  categories: <?php echo "['".implode("', '", $aux2)."']"; ?>,
                  data: [<?php for($j = 0; $j < count($contratos['extension'][$i]['gastos']); $j++){echo $contratos['extension'][$i]['gastos'][$j]['valor'].", ";} ?>],
                  color: colors[<?php echo $i; ?>]
              }
          },
        <?php } ?>],
        browserData = [],
        versionsData = [],
        i,
        j,
        dataLen = data.length,
        drillDataLen,
        brightness;


    // Build the data arrays
    for (i = 0; i < dataLen; i += 1) {

        // add browser data
        browserData.push({
            name: categories[i],
            y: data[i].y,
            color: data[i].color,
              parentId: i
        });

        // add version data
        drillDataLen = data[i].drilldown.data.length;
        for (j = 0; j < drillDataLen; j += 1) {
            brightness = 0.2 - (j / drillDataLen) / 5;
            versionsData.push({
                name: data[i].drilldown.categories[j],
                y: data[i].drilldown.data[j],
                color: Highcharts.Color(data[i].color).brighten(brightness).get(),
                id: i
            });
        }
    }

    // Create the chart
    Highcharts.chart('donutContainer2', {
        chart: {
            type: 'pie'
        },
        title: {
            text: false
        },
        legend: {
          align: 'left',
          layout: 'vertical',
          verticalAlign: 'top',
          x: 0,
          y: 0
        },
        /* subtitle: {
            text: 'Source: <a href="http://netmarketshare.com/">netmarketshare.com</a>'
        },*/
        credits: {
           enabled: false
        },
        yAxis: {
            title: {
                text: 'Total contratos'
            }
        },
        plotOptions: {
            pie: {
                shadow: false,
                center: ['50%', '50%']
            },
            series: {
              point: {
                events: {
                    legendItemClick: function () {
                        var id = this.parentId,
                            data = this.series.chart.series[1].data;
                        $.each(data, function (i, point) {

                            if (point.id == id) {
                                if(point.visible)
                                    point.setVisible(false);
                                else
                                    point.setVisible(true);
                            }

                        });
                    }
                }
            }
        }
        },
        tooltip: {
            valuePrefix: '$'
        },
        series: [{
            name: 'Contratos',
            data: browserData,
            size: '60%',
            showInLegend: true,
            dataLabels: {
                formatter: function () {
                    return this.point.name;
                },
                color: '#ffffff',
                distance: -30
            }
        }, {
            name: 'Tipo contratos',
            data: versionsData,
            size: '80%',
            innerSize: '60%',
            dataLabels: {
                formatter: function () {
                    // display only if larger than 1
                    return'<b>' + this.point.name + ':</b> ' + Highcharts.numberFormat(this.percentage, 1) + '%';
                }
            }
        }]
    });
});

$(function () {

    var colors = Highcharts.getOptions().colors,
    <?php $aux1 = [];
     for($i = 0; $i < count($contratos['tipogasto']); $i++){
       array_push($aux1, $contratos['tipogasto'][$i]['nombre']);
     }
     ?>

     <?php $aux2 = [];
      for($i = 0; $i < count($contratos['extension']); $i++){
        array_push($aux2, $contratos['extension'][$i]['nombre']);
      }
      ?>

        categories = <?php echo "['".implode("', '", $aux1)."']"; ?>,

        data = [<?php for($i = 0; $i < count($contratos['tipogasto']); $i++){ ?>{
              y: <?php echo $contratos['tipogasto'][$i]['cantidad']; ?>,
              color: colors[<?php echo $i; ?>],
              drilldown: {
                  name: false,
                  categories: <?php echo "['".implode("', '", $aux2)."']"; ?>,
                  data: [<?php for($j = 0; $j < count($contratos['tipogasto'][$i]['extensiones']); $j++){echo $contratos['tipogasto'][$i]['extensiones'][$j]['cantidad'].", ";} ?>],
                  color: colors[<?php echo $i; ?>]
              }
          },
        <?php } ?>],
        browserData = [],
        versionsData = [],
        i,
        j,
        dataLen = data.length,
        drillDataLen,
        brightness;


    // Build the data arrays
    for (i = 0; i < dataLen; i += 1) {

        // add browser data
        browserData.push({
            name: categories[i],
            y: data[i].y,
            color: data[i].color,
              parentId: i
        });

        // add version data
        drillDataLen = data[i].drilldown.data.length;
        for (j = 0; j < drillDataLen; j += 1) {
            brightness = 0.2 - (j / drillDataLen) / 5;
            versionsData.push({
                name: data[i].drilldown.categories[j],
                y: data[i].drilldown.data[j],
                color: Highcharts.Color(data[i].color).brighten(brightness).get(),
                  id:i
            });
        }
    }

    // Create the chart
    Highcharts.chart('donutContainer3', {
        chart: {
            type: 'pie'
        },
        title: {
            text: false
        },
        legend: {
          align: 'left',
          layout: 'vertical',
          verticalAlign: 'top',
          x: 0,
          y: 0
        },
        /* subtitle: {
            text: 'Source: <a href="http://netmarketshare.com/">netmarketshare.com</a>'
        },*/
        credits: {
           enabled: false
        },
        yAxis: {
            title: {
                text: 'Total percent market share'
            }
        },
        plotOptions: {
            pie: {
                shadow: false,
                center: ['50%', '50%']
            },
            series: {
              point: {
                events: {
                    legendItemClick: function () {
                        var id = this.parentId,
                            data = this.series.chart.series[1].data;
                        $.each(data, function (i, point) {

                            if (point.id == id) {
                                if(point.visible)
                                    point.setVisible(false);
                                else
                                    point.setVisible(true);
                            }

                        });
                    }
                }
            }
        }
        },
        tooltip: {
            enabled: true
        },
        series: [{
            name: 'Tipo contrato',
            data: browserData,
            size: '60%',
            showInLegend: true,
            dataLabels: {
                formatter: function () {
                    return  this.point.name;
                },
                color: '#ffffff',
                distance: -30
            }
        }, {
            name: 'Contratos',
            data: versionsData,
            size: '80%',
            innerSize: '60%',
            dataLabels: {
                formatter: function () {
                    // display only if larger than 1
                    return '<b>' + this.point.name + ':</b> ' + Highcharts.numberFormat(this.percentage, 1) + '%';
                }
            }
        }]
    });
});

$(function () {

    var colors = Highcharts.getOptions().colors,
    <?php $aux1 = [];
     for($i = 0; $i < count($contratos['tipogasto']); $i++){
       array_push($aux1, $contratos['tipogasto'][$i]['nombre']);
     }
     ?>

     <?php $aux2 = [];
      for($i = 0; $i < count($contratos['extension']); $i++){
        array_push($aux2, $contratos['extension'][$i]['nombre']);
      }
      ?>

        categories = <?php echo "['".implode("', '", $aux1)."']"; ?>,

        data = [<?php for($i = 0; $i < count($contratos['tipogasto']); $i++){ ?>{
              y: <?php echo $contratos['tipogasto'][$i]['valor']; ?>,
              color: colors[<?php echo $i; ?>],
              drilldown: {
                  name: false,
                  categories: <?php echo "['".implode("', '", $aux2)."']"; ?>,
                  data: [<?php for($j = 0; $j < count($contratos['tipogasto'][$i]['extensiones']); $j++){echo $contratos['tipogasto'][$i]['extensiones'][$j]['valor'].", ";} ?>],
                  color: colors[<?php echo $i; ?>]
              }
          },
        <?php } ?>],
        browserData = [],
        versionsData = [],
        i,
        j,
        dataLen = data.length,
        drillDataLen,
        brightness;


    // Build the data arrays
    for (i = 0; i < dataLen; i += 1) {

        // add browser data
        browserData.push({
            name: categories[i],
            y: data[i].y,
            color: data[i].color,
              parentId: i
        });

        // add version data
        drillDataLen = data[i].drilldown.data.length;
        for (j = 0; j < drillDataLen; j += 1) {
            brightness = 0.2 - (j / drillDataLen) / 5;
            versionsData.push({
                name: data[i].drilldown.categories[j],
                y: data[i].drilldown.data[j],
                color: Highcharts.Color(data[i].color).brighten(brightness).get(),
                id:i
            });
        }
    }

    // Create the chart
    Highcharts.chart('donutContainer4', {
        chart: {
            type: 'pie'
        },
        title: {
            text: false
        },
        legend: {
          align: 'left',
          layout: 'vertical',
          verticalAlign: 'top',
          x: 0,
          y: 0
        },
        /* subtitle: {
            text: 'Source: <a href="http://netmarketshare.com/">netmarketshare.com</a>'
        },*/
        credits: {
           enabled: false
        },
        yAxis: {
            title: {
                text: false
            }
        },
        plotOptions: {
            pie: {
                shadow: false,
                center: ['50%', '50%']
            },
            series: {
              point: {
                events: {
                    legendItemClick: function () {
                        var id = this.parentId,
                            data = this.series.chart.series[1].data;
                        $.each(data, function (i, point) {

                            if (point.id == id) {
                                if(point.visible)
                                    point.setVisible(false);
                                else
                                    point.setVisible(true);
                            }

                        });
                    }
                }
            }
        }
        },
        tooltip: {
            valuePrefix: '$'
        },
        series: [{
            name: 'Tipo contrato',
            data: browserData,
            size: '60%',
            showInLegend: true,
            dataLabels: {
                formatter: function () {
                    return this.point.name;
                },
                color: '#ffffff',
                distance: -30
            }
        }, {
            name: 'Contratos',
            data: versionsData,
            size: '80%',
            innerSize: '60%',
            dataLabels: {
                formatter: function () {
                    // display only if larger than 1
                    return  '<b>' + this.point.name + ':</b> ' + Highcharts.numberFormat(this.percentage, 1) + '%';
                }
            }
        }]
    });
});

function filtros_s(str,num) {
 var mes_m,year_m,reg_m,seg_m,area_m;

 
 if(num ==6 ){
    mes_m = 0;
    year_m = <?php echo date("Y") ?>;
    reg_m = 0;
    seg_m = 0;
    area_m = 0;
  }
  else
 {
    mes_m =document.getElementById("sl_mes").value;
    year_m = document.getElementById("sl_year").value;
    reg_m = document.getElementById("sl_reg").value;
    seg_m = document.getElementById("sl_seg").value;
    area_m = document.getElementById("sl_area").value;
 }
    if (mes_m==1){
        nom_mes = 'Ene';
    }
    else  if (mes_m==2){
        nom_mes = 'Feb';
    }
    else  if (mes_m==3){
        nom_mes = 'Mar';
    }
    else  if (mes_m==4){
        nom_mes = 'Abr';
    }
    else  if (mes_m==5){
        nom_mes = 'May';
    }
    else  if (mes_m==6){
        nom_mes = 'Jun';
    }
    else  if (mes_m==7){
        nom_mes = 'Jul';
    }
    else  if (mes_m==8){
        nom_mes = 'Ago';
    }
    else  if (mes_m==9){
        nom_mes = 'Sep';
    }
    else  if (mes_m==10){
        nom_mes = 'Oct';
    }
    else  if (mes_m==11){
        nom_mes = 'Nov';
    }
    else  if (mes_m==12){
        nom_mes = 'Dic';
    }
    else  if (mes_m==0){
        nom_mes = 'Año';
    }

		var myLoad = document.createElement('img');
		var myCenter = document.createElement('center');
        
		myLoad.setAttribute("src","https://www.mintbox.com/images/loading3.gif");
      
		$('#myOverlay').show();
        $('#loadingGIF').show();

        if (window.XMLHttpRequest) {
            // code for IE7+, Firefox, Chrome, Opera, Safari
            xmlhttptabla = new XMLHttpRequest();
			xmlhttpmont = new XMLHttpRequest();
			xmlhttpcont = new XMLHttpRequest();
			xmlhttpcontv = new XMLHttpRequest();
			xmlhttppers = new XMLHttpRequest();
            xmlhttpmenu = new XMLHttpRequest();
			xmlhttpriesgo = new XMLHttpRequest();
			xmlhttptipo = new XMLHttpRequest();
			xmlhttpext = new XMLHttpRequest();
        } else {
            // code for IE6, IE5
            xmlhttptabla = new ActiveXObject("Microsoft.XMLHTTP");
			xmlhttpmont = new ActiveXObject("Microsoft.XMLHTTP");
			xmlhttpcont = new ActiveXObject("Microsoft.XMLHTTP");
			xmlhttpcontv = new ActiveXObject("Microsoft.XMLHTTP");
			xmlhttppers = new ActiveXObject("Microsoft.XMLHTTP");
            xmlhttpmenu = new ActiveXObject("Microsoft.XMLHTTP");
			xmlhttpriesgo = new ActiveXObject("Microsoft.XMLHTTP");
			xmlhttptipo = new ActiveXObject("Microsoft.XMLHTTP");
			xmlhttpext = new ActiveXObject("Microsoft.XMLHTTP");
        }
        
        
  
        xmlhttptabla.onreadystatechange = function() {
            if (this.readyState == 4 && this.status == 200) {
              console.log(this.responseText);
              var resp_gra = JSON.parse(this.responseText);
			  
			  var cant_per_op = document.getElementById("permanenteoperacionalcant");
			  var cant_np_op = document.getElementById("nopermanenteoperacionalcant");
			  var cant_ot_op = document.getElementById("otmoperacionalcant");
			  var cant_fin_op = document.getElementById("finaloperacionalcant");
			  
			  var monto_per_op = document.getElementById("permanenteoperacionalvalor");
			  var monto_np_op = document.getElementById("nopermanenteoperacionalvalor");
			  var monto_ot_op = document.getElementById("otmoperacionalvalor");
			  var monto_fin_op = document.getElementById("finaloperacionalvalor");
			  
			  var cant_per_in = document.getElementById("permanenteinversióncant");
			  var cant_np_in = document.getElementById("nopermanenteinversióncant");
			  var cant_ot_in = document.getElementById("otminversióncant");
			  var cant_fin_in = document.getElementById("finalinversióncant");
			  
			  var monto_per_in = document.getElementById("permanenteinversiónvalor");
			  var monto_np_in = document.getElementById("nopermanenteinversiónvalor");
			  var monto_ot_in = document.getElementById("otminversiónvalor");
			  var monto_fin_in = document.getElementById("finalinversiónvalor");
			  
			  var cant_tt_per = document.getElementById("permanentetotalcant");
			  var cant_tt_np = document.getElementById("nopermanentetotalcant");
			  var cant_tt_ot = document.getElementById("otmtotalcant");
			  var cant_tt_fin = document.getElementById("finaltotalcant");
			  
			  var monto_tt_per = document.getElementById("permanentetotalvalor");
			  var monto_tt_np = document.getElementById("nopermanentetotalvalor");
			  var monto_tt_ot = document.getElementById("otmtotalvalor");
			  var monto_tt_fin = document.getElementById("finaltotalvalor");
			
						cant_per_op.innerHTML = "";
						cant_np_op.innerHTML = "";
						cant_ot_op.innerHTML = "";
						cant_fin_op.innerHTML = "";
						
						monto_per_op.innerHTML = "";
						monto_np_op.innerHTML = "";
						monto_ot_op.innerHTML = "";
						monto_fin_op.innerHTML = "";
						
						cant_per_in.innerHTML = "";
						cant_np_in.innerHTML = "";
						cant_ot_in.innerHTML = "";
						cant_fin_in.innerHTML = "";
						
						monto_per_op.innerHTML = "";
						monto_np_in.innerHTML = "";
						monto_ot_in.innerHTML = "";
						monto_fin_in.innerHTML = "";
						
						cant_tt_per.innerHTML = "";
						cant_tt_np.innerHTML = "";
						cant_tt_ot.innerHTML = "";
						cant_tt_fin.innerHTML = "";
						
						monto_tt_per.innerHTML = "";
						monto_tt_np.innerHTML = "";
						monto_tt_ot.innerHTML = "";
						monto_tt_fin.innerHTML = "";
						
						//--- asigna valores en tabla
						
						cant_per_op.innerHTML = resp_gra[0].Operacional_cant;
						cant_np_op.innerHTML = resp_gra[1].Operacional_cant;
						cant_ot_op.innerHTML = resp_gra[2].Operacional_cant;
						cant_fin_op.innerHTML = Number(resp_gra[0].Operacional_cant) + Number(resp_gra[1].Operacional_cant) + Number(resp_gra[2].Operacional_cant);
						
						monto_per_op.innerHTML = (Number(resp_gra[0].Operacional_monto)).toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");;
						monto_np_op.innerHTML = (Number(resp_gra[1].Operacional_monto)).toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");;
						monto_ot_op.innerHTML = (Number(resp_gra[2].Operacional_monto)).toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");;
						monto_fin_op.innerHTML = (Number(resp_gra[0].Operacional_monto) + Number(resp_gra[1].Operacional_monto) + Number(resp_gra[2].Operacional_monto)).toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");;
						
						cant_per_in.innerHTML = resp_gra[0].Inversion_cant;
						cant_np_in.innerHTML = resp_gra[1].Inversion_cant;
						cant_ot_in.innerHTML = resp_gra[2].Inversion_cant;
						cant_fin_in.innerHTML = Number(resp_gra[0].Inversion_cant) + Number(resp_gra[1].Inversion_cant) + Number(resp_gra[2].Inversion_cant);
						
						monto_per_in.innerHTML = Number((resp_gra[0].Inversion_monto)).toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");;
						monto_np_in.innerHTML = Number((resp_gra[1].Inversion_monto)).toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");;
						monto_ot_in.innerHTML = Number((resp_gra[2].Inversion_monto)).toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");;
						monto_fin_in.innerHTML =(Number(resp_gra[0].Inversion_monto) + Number(resp_gra[1].Inversion_monto) + Number(resp_gra[2].Inversion_monto)).toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");;
						
						cant_tt_per.innerHTML = Number(resp_gra[0].Inversion_cant) + Number(resp_gra[0].Operacional_cant);
						cant_tt_np.innerHTML = Number(resp_gra[1].Inversion_cant) + Number(resp_gra[1].Operacional_cant);
						cant_tt_ot.innerHTML = Number(resp_gra[2].Inversion_cant) + Number(resp_gra[2].Operacional_cant);
						cant_tt_fin.innerHTML = Number(resp_gra[0].Inversion_cant) + Number(resp_gra[0].Operacional_cant)+
												Number(resp_gra[1].Inversion_cant) + Number(resp_gra[1].Operacional_cant)+
												Number(resp_gra[2].Inversion_cant) + Number(resp_gra[2].Operacional_cant);
						
						monto_tt_per.innerHTML = (Number(resp_gra[0].Inversion_monto) + Number(resp_gra[0].Operacional_monto)).toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");;
						monto_tt_np.innerHTML = (Number(resp_gra[1].Inversion_monto) + Number(resp_gra[1].Operacional_monto)).toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");;
						monto_tt_ot.innerHTML = (Number(resp_gra[2].Inversion_monto) + Number(resp_gra[2].Operacional_monto)).toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");;
						monto_tt_fin.innerHTML = (Number(resp_gra[0].Inversion_monto) + Number(resp_gra[0].Operacional_monto)+
												Number(resp_gra[1].Inversion_monto) + Number(resp_gra[1].Operacional_monto)+
												Number(resp_gra[2].Inversion_monto) + Number(resp_gra[2].Operacional_monto)).toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");;
						
						
						
						

				
			}
        };
		
		
		xmlhttpmont.onreadystatechange = function() {
            if (this.readyState == 4 && this.status == 200) {
              console.log(this.responseText);
              var resp = JSON.parse(this.responseText);
				var monto = document.getElementById("monto_cont");
				monto.innerHTML = "";
				monto.innerHTML = "M$"+(Number(Math.round(resp[0].monto/1000000))).toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
			 
                  
			}
        };
		
		xmlhttpcont.onreadystatechange = function() {
            if (this.readyState == 4 && this.status == 200) {
              console.log(this.responseText);
             
			   var resp = JSON.parse(this.responseText);
				var cont = document.getElementById("cant_cont");
				cont.innerHTML = "";
				cont.innerHTML = resp[0].cant;
			 
               

                 
			}
        };
		
		xmlhttpcontv.onreadystatechange = function() {
            if (this.readyState == 4 && this.status == 200) {
              console.log(this.responseText);
             
               
			   var resp = JSON.parse(this.responseText);
				var cont = document.getElementById("cont_cant_v");
				cont.innerHTML = "";
				cont.innerHTML = resp[0].cant_v;

                 
			}
        };
		
		
		xmlhttpriesgo.onreadystatechange = function() {
            if (this.readyState == 4 && this.status == 200) {
              console.log(this.responseText);
             
               
			   var resp_bub = JSON.parse(this.responseText);
				var chart_bub = $('#scatterContainer').highcharts();

                     
                                                   
                          chart_bub.series[0].setData([]); 

                          
                   //carga datos desde BD      
                     for(i=0;i<resp_bub.length;i++){
                      
                           chart_bub.series[0].addPoint({
                              x: Number(resp_bub[i]['complejidad']), y: Number(resp_bub[i]['impacto']), z:Number(resp_bub[i]['cont_montoTotal']),
                              name: resp_bub[i]['cont_nombre'] ,empresa:resp_bub[i]['cont_proveedor'], color:"red" });
                              
                             
                      } 
					  
					


                 
			}
        };
		
		xmlhttppers.onreadystatechange = function() {
            if (this.readyState == 4 && this.status == 200) {
              console.log(this.responseText);
             
			   var resp = JSON.parse(this.responseText);
				var cont = document.getElementById("pers_cont");
				cont.innerHTML = "";
				cont.innerHTML = resp[0].pers;
               

                 
			}
        };
		
		xmlhttpext.onreadystatechange = function() {
            if (this.readyState == 4 && this.status == 200) {
              console.log(this.responseText);
			  var categ = [];
				var x_values_sub = [], drll_values_sub = [];;
				var x_values = [], drill = [];
				var colors = Highcharts.getOptions().colors;
			   var resp = JSON.parse(this.responseText);
			   drll_values_sub['data'] = [];
			   drll_values_sub['categories'] = [];
			  
			   for (i=0;i<resp.length;i++){
			   categ[i] = resp[i].nombre ;
			   x_values_sub['y'] = Number(resp[i]['cont_oper']) +  Number(resp[i]['cont_inv']);
			   x_values_sub['color'] =  colors[i];
			   drll_values_sub['name'] = false;
			   drll_values_sub['categories'] = ['Operacional', 'Inversión'];
			   drll_values_sub['data'] = [Number(resp[i]['cont_oper']),Number(resp[i]['cont_inv'])];
			   drll_values_sub['color'] = colors[i];
			   x_values.push(x_values_sub);
			   drill.push(drll_values_sub);
			   x_values_sub = {};
			   drll_values_sub={};
			 
			   }
			   
			   
			   
						var 
						categories = categ,
						browserData = [],
						versionsData = [],
						i,
						j,
						dataLen = x_values.length,
						drillDataLen,
						brightness;
				
				
					// Build the data arrays
					for (i = 0; i < dataLen; i += 1) {
				
						// add browser data
						browserData.push({
							name: categories[i],
							y: x_values[i].y,
							color: x_values[i].color,
							parentId: i
						});
				
						// add version data
					
						for (j = 0; j < drill[i].data.length; j += 1) {
							brightness = 0.2 - (j / drill.length) / 5;
							versionsData.push({
								name: drill[i].categories[j],
								y: drill[i].data[j],
								color: Highcharts.Color(drill[i].color).brighten(brightness).get(),
								id: i
							});
						}
					}
					
					var chart_comp = $('#donutContainer1').highcharts();
					chart_comp.series[0].setData([]); 
					chart_comp.series[1].setData([]);
					
					chart_comp.series[0].setData(browserData); 
					chart_comp.series[1].setData(versionsData);
					chart_comp.redraw();
				
               x_values = [];
			   drill = [];
			   
			    for (i=0;i<resp.length;i++){
			   categ[i] = resp[i].nombre ;
			   x_values_sub['y'] = Number(resp[i]['monto_oper']) +  Number(resp[i]['monto_inv']);
			   x_values_sub['color'] =  colors[i];
			   drll_values_sub['name'] = false;
			   drll_values_sub['categories'] = ['Operacional', 'Inversión'];
			   drll_values_sub['data'] = [Number(resp[i]['monto_oper']),Number(resp[i]['monto_inv'])];
			   drll_values_sub['color'] = colors[i];
			   x_values.push(x_values_sub);
			   drill.push(drll_values_sub);
			   x_values_sub = {};
			   drll_values_sub={};
			 
			   }
			   
			   browserData = [];
						versionsData = [];
			   
			   for (i = 0; i < dataLen; i += 1) {
				
						// add browser data
						browserData.push({
							name: categories[i],
							y: x_values[i].y,
							color: x_values[i].color,
							parentId: i
						});
				
						// add version data
					
						for (j = 0; j < drill[i].data.length; j += 1) {
							brightness = 0.2 - (j / drill.length) / 5;
							versionsData.push({
								name: drill[i].categories[j],
								y: drill[i].data[j],
								color: Highcharts.Color(drill[i].color).brighten(brightness).get(),
								id: i
							});
						}
					}
					
					var chart_comp = $('#donutContainer2').highcharts();
					chart_comp.series[0].setData([]); 
					chart_comp.series[1].setData([]);
					
					chart_comp.series[0].setData(browserData); 
					chart_comp.series[1].setData(versionsData);
					chart_comp.redraw();
			   
			   

                 
			}
        };
		
			xmlhttptipo.onreadystatechange = function() {
            if (this.readyState == 4 && this.status == 200) {
              console.log(this.responseText);
             
			   var resp = JSON.parse(this.responseText);
				
			  var categ = [];
				var x_values_sub = [], drll_values_sub = [];;
				var x_values = [], drill = [];
				var colors = Highcharts.getOptions().colors;
			   var resp = JSON.parse(this.responseText);
			   drll_values_sub['data'] = [];
			   drll_values_sub['categories'] = [];
			  
			   for (i=0;i<resp.length;i++){
			   categ[i] = resp[i].nombre_gasto ;
			   x_values_sub['y'] = Number(resp[i]['cont_per']) +  Number(resp[i]['cont_np']) +  Number(resp[i]['cont_ot']);
			   x_values_sub['color'] =  colors[i];
			   drll_values_sub['name'] = false;
			   drll_values_sub['categories'] = ['Permanente', 'No Permanente','OTM'];
			   drll_values_sub['data'] = [Number(resp[i]['cont_per']),Number(resp[i]['cont_np']),Number(resp[i]['cont_ot'])];
			   drll_values_sub['color'] = colors[i];
			   x_values.push(x_values_sub);
			   drill.push(drll_values_sub);
			   x_values_sub = {};
			   drll_values_sub={};
			 
			   }
			   
			   
			   
						var 
						categories = categ,
						browserData = [],
						versionsData = [],
						i,
						j,
						dataLen = x_values.length,
						drillDataLen,
						brightness;
				
				
					// Build the data arrays
					for (i = 0; i < dataLen; i += 1) {
				
						// add browser data
						browserData.push({
							name: categories[i],
							y: x_values[i].y,
							color: x_values[i].color,
							parentId: i
						});
				
						// add version data
					
						for (j = 0; j < drill[i].data.length; j += 1) {
							brightness = 0.2 - (j / drill.length) / 5;
							versionsData.push({
								name: drill[i].categories[j],
								y: drill[i].data[j],
								color: Highcharts.Color(drill[i].color).brighten(brightness).get(),
								id: i
							});
						}
					}
					
					var chart_comp = $('#donutContainer3').highcharts();
					chart_comp.series[0].setData([]); 
					chart_comp.series[1].setData([]);
					
					chart_comp.series[0].setData(browserData); 
					chart_comp.series[1].setData(versionsData);
				
               x_values = [];
			   drill = [];
			   
			    for (i=0;i<resp.length;i++){
			   categ[i] = resp[i].nombre_gasto ;
			   x_values_sub['y'] = Number(resp[i]['monto_per']) +  Number(resp[i]['monto_np'])+Number(resp[i]['monto_ot']);
			   x_values_sub['color'] =  colors[i];
			   drll_values_sub['name'] = false;
			   drll_values_sub['categories'] = ['Permanente', 'No Permanente','OTM'];
			   drll_values_sub['data'] = [Number(resp[i]['monto_per']),Number(resp[i]['monto_np']),Number(resp[i]['monto_ot'])];
			   drll_values_sub['color'] = colors[i];
			   x_values.push(x_values_sub);
			   drill.push(drll_values_sub);
			   x_values_sub = {};
			   drll_values_sub={};
			 
			   }
			   
			   browserData = [];
						versionsData = [];
			   
			   for (i = 0; i < dataLen; i += 1) {
				
						// add browser data
						browserData.push({
							name: categories[i],
							y: x_values[i].y,
							color: x_values[i].color,
							parentId: i
						});
				
						// add version data
					
						for (j = 0; j < drill[i].data.length; j += 1) {
							brightness = 0.2 - (j / drill.length) / 5;
							versionsData.push({
								name: drill[i].categories[j],
								y: drill[i].data[j],
								color: Highcharts.Color(drill[i].color).brighten(brightness).get(),
								id: i
							});
						}
					}
					
					var chart_comp = $('#donutContainer4').highcharts();
					chart_comp.series[0].setData([]); 
					chart_comp.series[1].setData([]);
					
					chart_comp.series[0].setData(browserData); 
					chart_comp.series[1].setData(versionsData);
				
               

                 
                     $('#myOverlay').hide();
                     $('#loadingGIF').hide();
			}
        };

         xmlhttpmenu.onreadystatechange = function() {
            if (this.readyState == 4 && this.status == 200) {
                document.getElementById("filtros").innerHTML = this.responseText;
               
            }
        };
       
    if(num==1)
    {
        
		xmlhttptabla.open("GET","dashboardcontratos/show?year="+str+"&mes="+mes_m+"&reg="+reg_m+"&area="+area_m+"&seg="+seg_m+"&proc=0&tip=4",true);
		xmlhttpcontv.open("GET","dashboardcontratos/show?year="+str+"&mes="+mes_m+"&reg="+reg_m+"&area="+area_m+"&seg="+seg_m+"&proc=0&tip=1",true);
		xmlhttpcont.open("GET","dashboardcontratos/show?year="+str+"&mes="+mes_m+"&reg="+reg_m+"&area="+area_m+"&seg="+seg_m+"&proc=0&tip=0",true);
		xmlhttpmont.open("GET","dashboardcontratos/show?year="+str+"&mes="+mes_m+"&reg="+reg_m+"&area="+area_m+"&seg="+seg_m+"&proc=0&tip=2",true);
		xmlhttppers.open("GET","dashboardcontratos/show?year="+str+"&mes="+mes_m+"&reg="+reg_m+"&area="+area_m+"&seg="+seg_m+"&proc=0&tip=3",true);
		xmlhttpriesgo.open("GET","dashboardcontratos/show?year="+str+"&mes="+mes_m+"&reg="+reg_m+"&area="+area_m+"&seg="+seg_m+"&proc=0&tip=5",true);
		xmlhttpext.open("GET","dashboardcontratos/show?year="+str+"&mes="+mes_m+"&reg="+reg_m+"&area="+area_m+"&seg="+seg_m+"&proc=0&tip=6",true);
		xmlhttptipo.open("GET","dashboardcontratos/show?year="+str+"&mes="+mes_m+"&reg="+reg_m+"&area="+area_m+"&seg="+seg_m+"&proc=0&tip=7",true);
		
        xmlhttpmenu.open("GET","dashboardcontratos/show?year="+str+"&mes="+mes_m+"&reg="+reg_m+"&area="+area_m+"&seg="+seg_m+"&proc=1",true);
      
       xmlhttpmenu.send();
    }
    else if(num==2){
		xmlhttptabla.open("GET","dashboardcontratos/show?year="+year_m+"&mes="+str+"&reg="+reg_m+"&area="+area_m+"&seg="+seg_m+"&proc=0&tip=4",true);
		xmlhttpcontv.open("GET","dashboardcontratos/show?year="+year_m+"&mes="+str+"&reg="+reg_m+"&area="+area_m+"&seg="+seg_m+"&proc=0&tip=1",true);
		xmlhttpcont.open("GET","dashboardcontratos/show?year="+year_m+"&mes="+str+"&reg="+reg_m+"&area="+area_m+"&seg="+seg_m+"&proc=0&tip=0",true);
		xmlhttpmont.open("GET","dashboardcontratos/show?year="+year_m+"&mes="+str+"&reg="+reg_m+"&area="+area_m+"&seg="+seg_m+"&proc=0&tip=2",true);
		xmlhttppers.open("GET","dashboardcontratos/show?year="+year_m+"&mes="+str+"&reg="+reg_m+"&area="+area_m+"&seg="+seg_m+"&proc=0&tip=3",true);
		xmlhttpriesgo.open("GET","dashboardcontratos/show?year="+year_m+"&mes="+str+"&reg="+reg_m+"&area="+area_m+"&seg="+seg_m+"&proc=0&tip=5",true);
	  xmlhttpext.open("GET","dashboardcontratos/show?year="+year_m+"&mes="+str+"&reg="+reg_m+"&area="+area_m+"&seg="+seg_m+"&proc=0&tip=6",true);
		xmlhttptipo.open("GET","dashboardcontratos/show?year="+year_m+"&mes="+str+"&reg="+reg_m+"&area="+area_m+"&seg="+seg_m+"&proc=0&tip=7",true);
		
	}
    else if(num==3){
   		xmlhttptabla.open("GET","dashboardcontratos/show?year="+year_m+"&mes="+mes_m+"&reg="+str+"&area="+area_m+"&seg="+seg_m+"&proc=0&tip=4",true);
		xmlhttpcontv.open("GET","dashboardcontratos/show?year="+year_m+"&mes="+mes_m+"&reg="+str+"&area="+area_m+"&seg="+seg_m+"&proc=0&tip=1",true);
		xmlhttpcont.open("GET","dashboardcontratos/show?year="+year_m+"&mes="+mes_m+"&reg="+str+"&area="+area_m+"&seg="+seg_m+"&proc=0&tip=0",true);
		xmlhttpmont.open("GET","dashboardcontratos/show?year="+year_m+"&mes="+mes_m+"&reg="+str+"&area="+area_m+"&seg="+seg_m+"&proc=0&tip=2",true);
		xmlhttppers.open("GET","dashboardcontratos/show?year="+year_m+"&mes="+mes_m+"&reg="+str+"&area="+area_m+"&seg="+seg_m+"&proc=0&tip=3",true);
		xmlhttpriesgo.open("GET","dashboardcontratos/show?year="+year_m+"&mes="+mes_m+"&reg="+str+"&area="+area_m+"&seg="+seg_m+"&proc=0&tip=5",true);
	  xmlhttpext.open("GET","dashboardcontratos/show?year="+year_m+"&mes="+mes_m+"&reg="+str+"&area="+area_m+"&seg="+seg_m+"&proc=0&tip=6",true);
		xmlhttptipo.open("GET","dashboardcontratos/show?year="+year_m+"&mes="+mes_m+"&reg="+str+"&area="+area_m+"&seg="+seg_m+"&proc=0&tip=7",true);
		
	}
    else if(num==4){
		xmlhttptabla.open("GET","dashboardcontratos/show?year="+year_m+"&mes="+mes_m+"&reg="+reg_m+"&area="+str+"&seg="+seg_m+"&proc=0&tip=4",true);
		xmlhttpcontv.open("GET","dashboardcontratos/show?year="+year_m+"&mes="+mes_m+"&reg="+reg_m+"&area="+str+"&seg="+seg_m+"&proc=0&tip=1",true);
		xmlhttpcont.open("GET","dashboardcontratos/show?year="+year_m+"&mes="+mes_m+"&reg="+reg_m+"&area="+str+"&seg="+seg_m+"&proc=0&tip=0",true);
		xmlhttpmont.open("GET","dashboardcontratos/show?year="+year_m+"&mes="+mes_m+"&reg="+reg_m+"&area="+str+"&seg="+seg_m+"&proc=0&tip=2",true);
		xmlhttppers.open("GET","dashboardcontratos/show?year="+year_m+"&mes="+mes_m+"&reg="+reg_m+"&area="+str+"&seg="+seg_m+"&proc=0&tip=3",true);
		xmlhttpriesgo.open("GET","dashboardcontratos/show?year="+year_m+"&mes="+mes_m+"&reg="+reg_m+"&area="+str+"&seg="+seg_m+"&proc=0&tip=5",true);
	  xmlhttpext.open("GET","dashboardcontratos/show?year="+year_m+"&mes="+mes_m+"&reg="+reg_m+"&area="+str+"&seg="+seg_m+"&proc=0&tip=6",true);
		xmlhttptipo.open("GET","dashboardcontratos/show?year="+year_m+"&mes="+mes_m+"&reg="+reg_m+"&area="+str+"&seg="+seg_m+"&proc=0&tip=7",true);
		
	}
    else if(num==5){
		xmlhttptabla.open("GET","dashboardcontratos/show?year="+year_m+"&mes="+mes_m+"&reg="+reg_m+"&area="+area_m+"&seg="+str+"&proc=0&tip=4",true);
		xmlhttpcontv.open("GET","dashboardcontratos/show?year="+year_m+"&mes="+mes_m+"&reg="+reg_m+"&area="+area_m+"&seg="+str+"&proc=0&tip=1",true);
		xmlhttpcont.open("GET","dashboardcontratos/show?year="+year_m+"&mes="+mes_m+"&reg="+reg_m+"&area="+area_m+"&seg="+str+"&proc=0&tip=0",true);
		xmlhttpmont.open("GET","dashboardcontratos/show?year="+year_m+"&mes="+mes_m+"&reg="+reg_m+"&area="+area_m+"&seg="+str+"&proc=0&tip=2",true);
		xmlhttppers.open("GET","dashboardcontratos/show?year="+year_m+"&mes="+mes_m+"&reg="+reg_m+"&area="+area_m+"&seg="+str+"&proc=0&tip=3",true);
		xmlhttpriesgo.open("GET","dashboardcontratos/show?year="+year_m+"&mes="+mes_m+"&reg="+reg_m+"&area="+area_m+"&seg="+str+"&proc=0&tip=5",true);
	  xmlhttpext.open("GET","dashboardcontratos/show?year="+year_m+"&mes="+mes_m+"&reg="+reg_m+"&area="+area_m+"&seg="+str+"&proc=0&tip=6",true);
		xmlhttptipo.open("GET","dashboardcontratos/show?year="+year_m+"&mes="+mes_m+"&reg="+reg_m+"&area="+area_m+"&seg="+str+"&proc=0&tip=7",true);
		
	}
    else if(num==6)
    {
       xmlhttpmenu.open("GET","dashboardcontratos/show?year="+year_m+"&mes=0&reg=0&area=0&seg=0&proc=1",true);
    	xmlhttptabla.open("GET","dashboardcontratos/show?year="+year_m+"&mes=0&reg=0&area=0&seg=0&proc=0&tip=4",true);
		xmlhttpcontv.open("GET","dashboardcontratos/show?year="+year_m+"&mes=0&reg=0&area=0&seg=0&proc=0&tip=1",true);
		xmlhttpcont.open("GET","dashboardcontratos/show?year="+year_m+"&mes=0&reg=0&area=0&seg=0&proc=0&tip=0",true);
		xmlhttpmont.open("GET","dashboardcontratos/show?year="+year_m+"&mes=0&reg=0&area=0&seg=0&proc=0&tip=2",true);
		xmlhttppers.open("GET","dashboardcontratos/show?year="+year_m+"&mes=0&reg=0&area=0&seg=0&proc=0&tip=3",true);
		xmlhttpriesgo.open("GET","dashboardcontratos/show?year="+year_m+"&mes=0&reg=0&area=0&seg=0&proc=0&tip=5",true);
		xmlhttpext.open("GET","dashboardcontratos/show?year="+year_m+"&mes=&mes=0&reg=0&area=0&seg=0&proc=0&tip=6",true);
		xmlhttptipo.open("GET","dashboardcontratos/show?year="+year_m+"&mes=&mes=0&reg=0&area=0&seg=0&proc=0&tip=7",true);
		
		
       xmlhttpmenu.send();
    }
    
xmlhttptabla.send();
xmlhttpcontv.send();
xmlhttpcont.send();
xmlhttpmont.send();
xmlhttppers.send();
xmlhttpriesgo.send();
xmlhttpext.send();
xmlhttptipo.send();
    
  
}



 </script>

    @endif

 @stop