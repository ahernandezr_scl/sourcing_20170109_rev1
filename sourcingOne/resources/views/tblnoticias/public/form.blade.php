

		 {!! Form::open(array('url'=>'tblnoticias/savepublic', 'class'=>'form-horizontal','files' => true , 'parsley-validate'=>'','novalidate'=>' ')) !!}

	@if(Session::has('messagetext'))
	  
		   {!! Session::get('messagetext') !!}
	   
	@endif
	<ul class="parsley-error-list">
		@foreach($errors->all() as $error)
			<li>{{ $error }}</li>
		@endforeach
	</ul>		


<div class="col-md-12">
						<fieldset><legend> Noticias</legend>
				
									  <div class="form-group  " >
										<label for="Area" class=" control-label col-md-4 text-left"> Area </label>
										<div class="col-md-6">
										  {!! Form::text('not_area', $row['not_area'],array('class'=>'form-control', 'placeholder'=>'',   )) !!}
										 </div>
										 <div class="col-md-2">
										 	
										 </div>
									  </div> 
									  <div class="form-group  " >
										<label for="Section" class=" control-label col-md-4 text-left"> Section </label>
										<div class="col-md-6">
										  {!! Form::text('not_seccion', $row['not_seccion'],array('class'=>'form-control', 'placeholder'=>'',   )) !!}
										 </div>
										 <div class="col-md-2">
										 	
										 </div>
									  </div> 
									  <div class="form-group  " >
										<label for="Publication Date" class=" control-label col-md-4 text-left"> Publication Date </label>
										<div class="col-md-6">
										  
				<div class="input-group m-b" style="width:150px !important;">
					{!! Form::text('not_fechaPub', $row['not_fechaPub'],array('class'=>'form-control datetime', 'style'=>'width:150px !important;')) !!}
					<span class="input-group-addon"><i class="fa fa-calendar"></i></span>
				</div>
				
										 </div>
										 <div class="col-md-2">
										 	
										 </div>
									  </div> 
									  <div class="form-group  " >
										<label for="News" class=" control-label col-md-4 text-left"> News </label>
										<div class="col-md-6">
										  <textarea name='not_publicacion' rows='5' id='editor' class='form-control editor '
						 >{{ $row['not_publicacion'] }}</textarea>
										 </div>
										 <div class="col-md-2">
										 	
										 </div>
									  </div> 
									  <div class="form-group  " >
										<label for="Post Data" class=" control-label col-md-4 text-left"> Post Data </label>
										<div class="col-md-6">
										  {!! Form::text('not_autor', $row['not_autor'],array('class'=>'form-control', 'placeholder'=>'',   )) !!}
										 </div>
										 <div class="col-md-2">
										 	
										 </div>
									  </div> </fieldset>
			</div>

			

			<div style="clear:both"></div>	
				
					
				  <div class="form-group">
					<label class="col-sm-4 text-right">&nbsp;</label>
					<div class="col-sm-8">	
					<button type="submit" name="apply" class="btn btn-info btn-sm" ><i class="fa  fa-check-circle"></i> {{ Lang::get('core.sb_apply') }}</button>
					<button type="submit" name="submit" class="btn btn-primary btn-sm" ><i class="fa  fa-save "></i> {{ Lang::get('core.sb_save') }}</button>
				  </div>	  
			
		</div> 
		 
		 {!! Form::close() !!}
		 
   <script type="text/javascript">
	$(document).ready(function() { 
		
		 

		$('.removeCurrentFiles').on('click',function(){
			var removeUrl = $(this).attr('href');
			$.get(removeUrl,function(response){});
			$(this).parent('div').empty();	
			return false;
		});		
		
	});
	</script>		 
