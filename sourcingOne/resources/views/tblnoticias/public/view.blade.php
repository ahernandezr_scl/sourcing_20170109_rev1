<div class="m-t" style="padding-top:25px;">	
    <div class="row m-b-lg animated fadeInDown delayp1 text-center">
        <h3> {{ $pageTitle }} <small> {{ $pageNote }} </small></h3>
        <hr />       
    </div>
</div>
<div class="m-t">
	<div class="table-responsive" > 	

		<table class="table table-striped table-bordered" >
			<tbody>	
		
			
					<tr>
						<td width='30%' class='label-view text-right'>{{ SiteHelpers::activeLang('Area Noticia', (isset($fields['not_area']['language'])? $fields['not_area']['language'] : array())) }}</td>
						<td>{{ $row->not_area}} </td>

					</tr>
				
					<tr>
						<td width='30%' class='label-view text-right'>{{ SiteHelpers::activeLang('Sección', (isset($fields['not_seccion']['language'])? $fields['not_seccion']['language'] : array())) }}</td>
						<td>{{ $row->not_seccion}} </td>

					</tr>
				
					<tr>
						<td width='30%' class='label-view text-right'>{{ SiteHelpers::activeLang('Fecha Publicación', (isset($fields['not_fechaPub']['language'])? $fields['not_fechaPub']['language'] : array())) }}</td>
						<td>{{ $row->not_fechaPub}} </td>

					</tr>
				
					<tr>
						<td width='30%' class='label-view text-right'>{{ SiteHelpers::activeLang('Publicación', (isset($fields['not_publicacion']['language'])? $fields['not_publicacion']['language'] : array())) }}</td>
						<td>{{ $row->not_publicacion}} </td>

					</tr>
				
					<tr>
						<td width='30%' class='label-view text-right'>{{ SiteHelpers::activeLang('Autor', (isset($fields['not_autor']['language'])? $fields['not_autor']['language'] : array())) }}</td>
						<td>{{ $row->not_autor}} </td>

					</tr>
						
					<tr>
						<td width='30%' class='label-view text-right'></td>
						<td> <a href="javascript:history.go(-1)" class="btn btn-primary"> Back To Grid <a> </td>
						
					</tr>					
				
			</tbody>	
		</table>   

	 
	
	</div>
</div>	