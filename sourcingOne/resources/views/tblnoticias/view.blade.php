@if($setting['view-method'] =='native')
<div class="sbox">
	<div class="sbox-title">  
		<h4> <i class="fa fa-table"></i> <?php echo $pageTitle ;?> <small>{{ $pageNote }}</small>
			<a href="javascript:void(0)" class="collapse-close pull-right btn btn-xs btn-danger" onclick="ajaxViewClose('#{{ $pageModule }}')">
			<i class="fa fa fa-times"></i></a>
		</h4>
	 </div>

	<div class="sbox-content"> 
@endif	

		<table class="table table-striped table-bordered" >
			<tbody>	
				
					<tr>
						<td width='30%' class='label-view text-right'>{{ SiteHelpers::activeLang('Area Noticia', (isset($fields['not_area']['language'])? $fields['not_area']['language'] : array())) }}</td>
						<td>{{ $row->not_area}} </td>

					</tr>
				
					<tr>
						<td width='30%' class='label-view text-right'>{{ SiteHelpers::activeLang('Sección', (isset($fields['not_seccion']['language'])? $fields['not_seccion']['language'] : array())) }}</td>
						<td>{{ $row->not_seccion}} </td>

					</tr>
				
					<tr>
						<td width='30%' class='label-view text-right'>{{ SiteHelpers::activeLang('Fecha Publicación', (isset($fields['not_fechaPub']['language'])? $fields['not_fechaPub']['language'] : array())) }}</td>
						<td>{{ $row->not_fechaPub}} </td>

					</tr>
				
					<tr>
						<td width='30%' class='label-view text-right'>{{ SiteHelpers::activeLang('Publicación', (isset($fields['not_publicacion']['language'])? $fields['not_publicacion']['language'] : array())) }}</td>
						<td>{{ $row->not_publicacion}} </td>

					</tr>
				
					<tr>
						<td width='30%' class='label-view text-right'>{{ SiteHelpers::activeLang('Autor', (isset($fields['not_autor']['language'])? $fields['not_autor']['language'] : array())) }}</td>
						<td>{{ $row->not_autor}} </td>

					</tr>
				
			</tbody>	
		</table>  
			
		 	

@if($setting['form-method'] =='native')
	</div>	
</div>	
@endif	

<script>
$(document).ready(function(){

});
</script>	