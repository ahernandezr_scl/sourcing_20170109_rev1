
@if($setting['form-method'] =='native')
	<div class="sbox">
		<div class="sbox-title">
			<h4> <i class="fa fa-table"></i> <?php echo $pageTitle ;?> <small>{{ $pageNote }}</small>
				<a href="javascript:void(0)" class="collapse-close pull-right btn btn-xs btn-danger" onclick="ajaxViewClose('#{{ $pageModule }}')"><i class="fa fa fa-times"></i></a>
			</h4>
	</div>

	<div class="sbox-content">
@endif
			{!! Form::open(array('url'=>'requisito/save/'.SiteHelpers::encryptID($row['IdRequisito']), 'class'=>'form-horizontal','files' => true , 'parsley-validate'=>'','novalidate'=>' ','id'=> 'requisitoFormAjax')) !!}
			<div class="col-md-12">
						<fieldset><legend> Requisito</legend>
{!! Form::hidden('IdRequisito', $row['IdRequisito']) !!}
									  <div class="form-group  " >
										<label for="Entidad" class=" control-label col-md-4 text-left"> Entidad <span class="asterix"> * </span></label>
										<div class="col-md-6">
										  <select name='Entidad' rows='5' id='Entidad' class='select2 ' required  ></select>
										 </div>
										 <div class="col-md-2">

										 </div>
									  </div>
									  <div class="form-group  " >
										<label for="Tipo Documento" class=" control-label col-md-4 text-left"> Tipo Documento <span class="asterix"> * </span></label>
										<div class="col-md-6">
										  <select name='IdTipoDocumento' rows='5' id='IdTipoDocumento' class='select2 ' required  ></select>
										 </div>
										 <div class="col-md-2">

										 </div>
									  </div> {!! Form::hidden('FechaCumplimiento', $row['FechaCumplimiento']) !!}
									  <div class="form-group  " >
										<label for="Vigencia" class=" control-label col-md-4 text-left"> Vigencia </label>
										<div class="col-md-6">

					<?php $Vigencia = explode(',',$row['Vigencia']);
					$Vigencia_opt = array( '' => 'Seleccione Valor' ,  'Todos' => 'Para todos' ,  'Ahora' => 'De ahora en adelante' , ); ?>
					<select name='Vigencia' rows='5'   class='select2 '  >
						<?php
						foreach($Vigencia_opt as $key=>$val)
						{
							echo "<option  value ='$key' ".($row['Vigencia'] == $key ? " selected='selected' " : '' ).">$val</option>";
						}
						?></select>
										 </div>
										 <div class="col-md-2">

										 </div>
									  </div> {!! Form::hidden('createdOn', $row['createdOn']) !!}{!! Form::hidden('updatedOn', $row['updatedOn']) !!}</fieldset>
			</div>



	<hr />
	<div class="clr clear"></div>
<div id="roles">
	<h5> Roles </h5>

	<div class="table-responsive">
    <table class="table table-striped " id="tablaRole">
        <thead>
			<tr>
				@foreach ($subform['tableGrid'] as $t)
					@if($t['view'] =='1' && $t['field'] !='IdRequisito')
						<th>{{ $t['label'] }}</th>
					@endif
				@endforeach
				<th></th>
			  </tr>

        </thead>

        <tbody>
        @if(count($subform['rowData'])>=1)
            @foreach ($subform['rowData'] as $rows)
            <tr class="clone clonedInput">

			 @foreach ($subform['tableGrid'] as $field)
				 @if($field['view'] =='1' && $field['field'] !='IdRequisito')
				 <td>
				 	{!! SiteHelpers::bulkForm($field['field'] , $subform['tableForm'] , $rows->{$field['field']}) !!}
				 </td>
				 @endif

			 @endforeach
			 <td>
			 	<a onclick=" $(this).parents('.clonedInput').remove(); return false" href="#" class="remove btn btn-xs btn-danger">-</a>
			 	<input type="hidden" name="counter[]">
			 </td>
			@endforeach
			</tr>

		@else
            <tr class="clone clonedInput">

			 @foreach ($subform['tableGrid'] as $field)

				 @if($field['view'] =='1' && $field['field'] !='IdRequisito')
				 <td>
				 	{!! SiteHelpers::bulkForm($field['field'] , $subform['tableForm'] ) !!}
				 </td>
				 @endif

			 @endforeach
			 <td>
			 	<a onclick=" $(this).parents('.clonedInput').remove(); return false" href="#" class="remove btn btn-xs btn-danger">-</a>
			 	<input type="hidden" name="counter[]">
			 </td>

			</tr>


		@endif


        </tbody>

     </table>
     <input type="hidden" name="enable-masterdetail" value="true">
     </div>



	<br /><br />

     <a href="javascript:void(0);" class="addC btn btn-xs btn-info" rel=".clone"><i class="fa fa-plus"></i> New Item</a>
     <hr />
  </div>
       <div id="AreasT">
    <h5> Areas de trabajo </h5>

    <div class="table-responsive">
    <table class="table table-striped " id="tablaArea">
        <thead>
            <tr>
                @foreach ($subformtwo['tableGrid'] as $t)
                    @if($t['view'] =='1' && $t['field'] !='IdRequisito')
                        <th>{{ $t['label'] }}</th>
                    @endif
                @endforeach
                <th></th>
              </tr>

        </thead>

        <tbody>
        @if(count($subformtwo['rowData'])>=1)
            @foreach ($subformtwo['rowData'] as $rows)
            <tr class="cloneTwo clonedInputTwo">

             @foreach ($subformtwo['tableGrid'] as $field)
                 @if($field['view'] =='1' && $field['field'] !='IdRequisito')
                 <td>
                    {!! SiteHelpers::bulkForm($field['field'] , $subformtwo['tableForm'] , $rows->{$field['field']}) !!}
                 </td>
                 @endif

             @endforeach
             <td>
                <a onclick=" $(this).parents('.clonedInputTwo').remove(); return false" href="#" class="remove btn btn-xs btn-danger">-</a>
                <input type="hidden" name="counter[]">
             </td>
            @endforeach
            </tr>

        @else
            <tr class="cloneTwo clonedInputTwo">

             @foreach ($subformtwo['tableGrid'] as $field)

                 @if($field['view'] =='1' && $field['field'] !='IdRequisito')
                 <td>
                    {!! SiteHelpers::bulkForm($field['field'] , $subformtwo['tableForm'] ) !!}
                 </td>
                 @endif

             @endforeach
             <td>
                <a onclick=" $(this).parents('.clonedInputTwo').remove(); return false" href="#" class="remove btn btn-xs btn-danger">-</a>
                <input type="hidden" name="counter[]">
             </td>

            </tr>


        @endif


        </tbody>

     </table>
     <input type="hidden" name="enable-masterdetail" value="true">
     </div>
        <br /><br />

     <a href="javascript:void(0);" class="addC btn btn-xs btn-info" rel=".cloneTwo"><i class="fa fa-plus"></i> New Item</a>
     <hr />
     </div>
			<div style="clear:both"></div>

			<div class="form-group">
				<label class="col-sm-4 text-right">&nbsp;</label>
				<div class="col-sm-8">
					<button type="submit" class="btn btn-primary btn-sm "><i class="icon-checkmark-circle2"></i>  {{ Lang::get('core.sb_save') }} </button>
					<button type="button" onclick="ajaxViewClose('#{{ $pageModule }}')" class="btn btn-success btn-sm"><i class="icon-cancel-circle2 "></i>  {{ Lang::get('core.sb_cancel') }} </button>
				</div>
			</div>
			{!! Form::close() !!}


@if($setting['form-method'] =='native')
	</div>
</div>
@endif


</div>

<script type="text/javascript">
$(document).ready(function() {
		$("#roles").hide();
		$("#AreasT").hide();

		$('#Entidad').on('change', function() {
			console.log(this.value)
   			if(this.value==4){
   		 		var rowCountA= $('#tablaArea tbody tr').length;
   				if (rowCountA>1)
   					$('#tablaArea tbody tr:not(:first)').remove();
   				$("#roles").show();
   				$("#AreasT").hide();
   			}
   			else if(this.value==5){
   				 var rowCountR= $('#tablaRole tbody tr').length;
   				if (rowCountR>1)
   					$('#tablaRole tbody tr:not(:first)').remove();
   				$("#AreasT").show();
   				$("#roles").hide();
   			}else{
   				$("#roles").hide();
   				$("#AreasT").hide();
   			}
		});
  location.origin = location.protocol + "//" + location.host;
  //var base_url = location.origin + '/htdocs/requisitosareastrabajo';
  var base_url = location.origin + '/requisitosareastrabajo';

	$(".bulk_Entidad").change(function(event){
		var lobjIdAreaDeTrabajo = $(this).parent("td").parent("tr").find(".bulk_IdEntidad");
		$.post(base_url+'/areatrabajo',{ id:event.target.value,},function(response,state){
			lobjIdAreaDeTrabajo.empty();
			for (var i =0; i<response.valores.length; i++) {
				lobjIdAreaDeTrabajo.append("<option value='"+response.valores[i].IdAreaTrabajo+"'>"+response.valores[i].Descripcion+"</option>");
			}
		});

	        var lintIdAreaDeTrabajo;
	        var lintExists = 0;
	        lobjIdAreaDeTrabajo.find("option").each(function () {
	            lintIdAreaDeTrabajo = $(this).val();
	            lintExists = 0;
	            $(".bulk_Entidad").each(function () {
	              if ($(this).val()==lintIdAreaDeTrabajo) {
	                lintExists = 1;
	                 return false;
	              }
	            });
	            if (!lintExists){
	               lobjIdAreaDeTrabajo.find("option[value='"+lintIdAreaDeTrabajo+"']").remove();
	            }
	        });
	        lobjIdAreaDeTrabajo.prop( "disabled", false );
	});

		$("#Entidad").jCombo("{!! url('requisito/comboselect?filter=tbl_entidades:IdEntidad:Entidad') !!}",
		{  selected_value : '{{ $row["Entidad"] }}' });

		$("#IdTipoDocumento").jCombo("{!! url('requisito/comboselect?filter=tbl_tipos_documentos:IdTipoDocumento:Descripcion') !!}",
		{  selected_value : '{{ $row["IdTipoDocumento"] }}' });

	$('.addC').relCopy({});
	$('.editor').summernote();
	$('.previewImage').fancybox();
	$('.tips').tooltip();
	$(".select2").select2({ width:"98%"});
	$('.date').datepicker({format:'yyyy-mm-dd',autoClose:true})
	$('.datetime').datetimepicker({format: 'yyyy-mm-dd hh:ii:ss'});
	$('input[type="checkbox"],input[type="radio"]').iCheck({
		checkboxClass: 'icheckbox_square-red',
		radioClass: 'iradio_square-red',
	});
		$('.removeMultiFiles').on('click',function(){
			var removeUrl = '{{ url("requisito/removefiles?file=")}}'+$(this).attr('url');
			$(this).parent().remove();
			$.get(removeUrl,function(response){});
			$(this).parent('div').empty();
			return false;
		});

	var form = $('#requisitoFormAjax');
	form.parsley();
	form.submit(function(){

		if(form.parsley('isValid') == true){
			var options = {
				dataType:      'json',
				beforeSubmit :  showRequest,
				success:       showResponse
			}
			$(this).ajaxSubmit(options);
			return false;

		} else {
			return false;
		}

	});

});

function showRequest()
{
	$('.ajaxLoading').show();
}
function showResponse(data)  {

	if(data.status == 'success')
	{
		ajaxViewClose('#{{ $pageModule }}');
		ajaxFilter('#{{ $pageModule }}','{{ $pageUrl }}/data');
		notyMessage(data.message);
		$('#sximo-modal').modal('hide');
	} else {
		notyMessageError(data.message);
		$('.ajaxLoading').hide();
		return false;
	}
}

</script>
<style type="text/css">
	 @if((count($subform['rowData'])==0) && (count($subformtwo['rowData'])==0))
	#tablaRole tbody tr:first-child {
		display:none;
	}
	#tablaArea tbody tr:first-child {
		display:none;
	}
	@endif
</style>
