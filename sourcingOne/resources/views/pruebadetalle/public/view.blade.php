<div class="m-t" style="padding-top:25px;">	
    <div class="row m-b-lg animated fadeInDown delayp1 text-center">
        <h3> {{ $pageTitle }} <small> {{ $pageNote }} </small></h3>
        <hr />       
    </div>
</div>
<div class="m-t">
	<div class="table-responsive" > 	

		<table class="table table-striped table-bordered" >
			<tbody>	
		
			
					<tr>
						<td width='30%' class='label-view text-right'>{{ SiteHelpers::activeLang('IdPruebaDetalle', (isset($fields['IdPruebaDetalle']['language'])? $fields['IdPruebaDetalle']['language'] : array())) }}</td>
						<td>{{ $row->IdPruebaDetalle}} </td>
						
					</tr>
				
					<tr>
						<td width='30%' class='label-view text-right'>{{ SiteHelpers::activeLang('Prueba', (isset($fields['Prueba']['language'])? $fields['Prueba']['language'] : array())) }}</td>
						<td>{{ $row->Prueba}} </td>
						
					</tr>
				
					<tr>
						<td width='30%' class='label-view text-right'>{{ SiteHelpers::activeLang('Datos', (isset($fields['Datos']['language'])? $fields['Datos']['language'] : array())) }}</td>
						<td>{{ $row->Datos}} </td>
						
					</tr>
						
					<tr>
						<td width='30%' class='label-view text-right'></td>
						<td> <a href="javascript:history.go(-1)" class="btn btn-primary"> Back To Grid <a> </td>
						
					</tr>					
				
			</tbody>	
		</table>   

	 
	
	</div>
</div>	