

		 {!! Form::open(array('url'=>'msgalertascontroller/savepublic', 'class'=>'form-horizontal','files' => true , 'parsley-validate'=>'','novalidate'=>' ')) !!}

	@if(Session::has('messagetext'))
	  
		   {!! Session::get('messagetext') !!}
	   
	@endif
	<ul class="parsley-error-list">
		@foreach($errors->all() as $error)
			<li>{{ $error }}</li>
		@endforeach
	</ul>		


<div class="col-md-12">
						<fieldset><legend> Alertas - Mensaje de Alertas</legend>
				{!! Form::hidden('id_mensaje', $row['id_mensaje']) !!}					
									  <div class="form-group  " >
										<label for="Tipo Alerta" class=" control-label col-md-4 text-left"> Tipo Alerta </label>
										<div class="col-md-6">
										  <select name='tipo_alerta' rows='5' id='tipo_alerta' class='select2 '   ></select> 
										 </div> 
										 <div class="col-md-2">
										 	
										 </div>
									  </div> 					
									  <div class="form-group  " >
										<label for="Texto Mensaje" class=" control-label col-md-4 text-left"> Texto Mensaje <span class="asterix"> * </span></label>
										<div class="col-md-6">
										  <textarea name='texto_mensaje' rows='5' id='texto_mensaje' class='form-control '  
				         required  >{{ $row['texto_mensaje'] }}</textarea> 
										 </div> 
										 <div class="col-md-2">
										 	
										 </div>
									  </div> </fieldset>
			</div>
			
			

			<div style="clear:both"></div>	
				
					
				  <div class="form-group">
					<label class="col-sm-4 text-right">&nbsp;</label>
					<div class="col-sm-8">	
					<button type="submit" name="apply" class="btn btn-info btn-sm" ><i class="fa  fa-check-circle"></i> {{ Lang::get('core.sb_apply') }}</button>
					<button type="submit" name="submit" class="btn btn-primary btn-sm" ><i class="fa  fa-save "></i> {{ Lang::get('core.sb_save') }}</button>
				  </div>	  
			
		</div> 
		 
		 {!! Form::close() !!}
		 
   <script type="text/javascript">
	$(document).ready(function() { 
		
		
		$("#tipo_alerta").jCombo("{!! url('msgalertascontroller/comboselect?filter=tbl_alerta_tpo:tipo_alerta:desc_tipo_alerta') !!}",
		{  selected_value : '{{ $row["tipo_alerta"] }}' });
		 

		$('.removeCurrentFiles').on('click',function(){
			var removeUrl = $(this).attr('href');
			$.get(removeUrl,function(response){});
			$(this).parent('div').empty();	
			return false;
		});		
		
	});
	</script>		 
