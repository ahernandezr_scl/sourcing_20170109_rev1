<div class="m-t" style="padding-top:25px;">	
    <div class="row m-b-lg animated fadeInDown delayp1 text-center">
        <h3> {{ $pageTitle }} <small> {{ $pageNote }} </small></h3>
        <hr />       
    </div>
</div>
<div class="m-t">
	<div class="table-responsive" > 	

		<table class="table table-striped table-bordered" >
			<tbody>	
		
			
					<tr>
						<td width='30%' class='label-view text-right'>{{ SiteHelpers::activeLang('id Mensaje', (isset($fields['id_mensaje']['language'])? $fields['id_mensaje']['language'] : array())) }}</td>
						<td>{{ $row->id_mensaje}} </td>
						
					</tr>
				
					<tr>
						<td width='30%' class='label-view text-right'>{{ SiteHelpers::activeLang('Name of Alert type', (isset($fields['tipo_alerta']['language'])? $fields['tipo_alerta']['language'] : array())) }}</td>
						<td>{{ SiteHelpers::formatLookUp($row->tipo_alerta,'tipo_alerta','1:tbl_alerta_tpo:tipo_alerta:desc_tipo_alerta') }} </td>
						
					</tr>
				
					<tr>
						<td width='30%' class='label-view text-right'>{{ SiteHelpers::activeLang('Texto Mensaje', (isset($fields['texto_mensaje']['language'])? $fields['texto_mensaje']['language'] : array())) }}</td>
						<td>{{ $row->texto_mensaje}} </td>
						
					</tr>
						
					<tr>
						<td width='30%' class='label-view text-right'></td>
						<td> <a href="javascript:history.go(-1)" class="btn btn-primary"> Back To Grid <a> </td>
						
					</tr>					
				
			</tbody>	
		</table>   

	 
	
	</div>
</div>	