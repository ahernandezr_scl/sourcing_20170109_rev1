
@if($setting['form-method'] =='native')
	<div class="sbox">
		<div class="sbox-title">
			<h4> <i class="fa fa-table"></i> <?php echo $pageTitle ;?> <small>{{ $pageNote }}</small>
				<a href="javascript:void(0)" class="collapse-close pull-right btn btn-xs btn-danger" onclick="ajaxViewClose('#{{ $pageModule }}')"><i class="fa fa fa-times"></i></a>
			</h4>
	</div>

	<div class="sbox-content">
@endif
			{!! Form::open(array('url'=>'contratistas/save/'.SiteHelpers::encryptID($row['IdContratista']), 'class'=>'form-horizontal','files' => true , 'parsley-validate'=>'','novalidate'=>' ','id'=> 'contratistasFormAjax')) !!}
			<div class="col-md-12">
						<fieldset><legend> Contratistas</legend>
				{!! Form::hidden('IdContratista', $row['IdContratista'],array('class'=>'form-control', 'id'=>'IdContratista' )) !!}
									  <div class="form-group  " >
										<label for="RUT" class=" control-label col-md-4 text-left"> RUT <span class="asterix"> * </span></label>
										<div class="col-md-6">
										  {!! Form::text('RUT', $row['RUT'],array('class'=>'form-control', 'id'=>'rut', 'placeholder'=>'', 'required'=>'true'  )) !!}
										 </div>
										 <div class="col-md-2">

										 </div>
									  </div>
									  					<div class="form-group  " >
						<label for="Persona" class=" control-label col-md-4 text-left"> SubContratistas:</label>
						<div class="col-md-6">
								<select name='IdSubContratista[]' multiple rows='5' id='IdSubContratista' class='select2 '   ></select>
						</div>
						<div class="col-md-2">

						</div>
					</div>
									  <div class="form-group  " >
										<label for="Razon Social" class=" control-label col-md-4 text-left"> Razon Social <span class="asterix"> * </span></label>
										<div class="col-md-6">
										  {!! Form::text('RazonSocial', $row['RazonSocial'],array('class'=>'form-control', 'placeholder'=>'', 'required'=>'true'  )) !!}
										 </div>
										 <div class="col-md-2">

										 </div>
									  </div>
									  <div class="form-group  " >
										<label for="Nombre Fantasia" class=" control-label col-md-4 text-left"> Nombre Fantasia </label>
										<div class="col-md-6">
										  {!! Form::text('NombreFantasia', $row['NombreFantasia'],array('class'=>'form-control', 'placeholder'=>'',   )) !!}
										 </div>
										 <div class="col-md-2">

										 </div>
									  </div>
									  <div class="form-group  " >
										<label for="Representante" class=" control-label col-md-4 text-left"> Representante </label>
										<div class="col-md-6">
										  {!! Form::text('Representante', $row['Representante'],array('class'=>'form-control', 'placeholder'=>'',   )) !!}
										 </div>
										 <div class="col-md-2">

										 </div>
									  </div>
									  <div class="form-group  " >
										<label for="Representante Fono" class=" control-label col-md-4 text-left"> Representante Fono </label>
										<div class="col-md-6">
										  {!! Form::text('RepresentanteFono', $row['RepresentanteFono'],array('class'=>'form-control', 'placeholder'=>'',   )) !!}
										 </div>
										 <div class="col-md-2">

										 </div>
									  </div>
									  <div class="form-group  " >
										<label for="Representante Email" class=" control-label col-md-4 text-left"> Representante Email </label>
										<div class="col-md-6">
										  {!! Form::email('RepresentanteEmail', $row['RepresentanteEmail'],array('class'=>'form-control', 'placeholder'=>'',   )) !!}
										 </div>
										 <div class="col-md-2">

										 </div>
									  </div>
									  <div class="form-group  " >
										<label for="Direccion" class=" control-label col-md-4 text-left"> Direccion </label>
										<div class="col-md-6">
										  {!! Form::text('Direccion', $row['Direccion'],array('class'=>'form-control', 'placeholder'=>'',   )) !!}
										 </div>
										 <div class="col-md-2">

										 </div>
									  </div>
									  <div class="form-group  " >
										<label for="Fono" class=" control-label col-md-4 text-left"> Fono <span class="asterix"> * </span></label>
										<div class="col-md-6">
										  {!! Form::text('Fono', $row['Fono'],array('class'=>'form-control', 'placeholder'=>'', 'required'=>'true', 'parsley-type'=>'number'   )) !!}
										 </div>
										 <div class="col-md-2">

										 </div>
									  </div>
									  <div class="form-group  " >
										<label for="Email" class=" control-label col-md-4 text-left"> Email <span class="asterix"> * </span></label>
										<div class="col-md-6">
										  {!! Form::email('Email', $row['Email'],array('class'=>'form-control', 'placeholder'=>'', 'required'=>'true', 'parsley-type'=>'email'   )) !!}
										 </div>
										 <div class="col-md-2">

										 </div>
									  </div>
									  <div class="form-group  " >
										<label for="Pagina Web" class=" control-label col-md-4 text-left"> Pagina Web <span class="asterix"> * </span></label>
										<div class="col-md-6">
										  {!! Form::text('PaginaWeb', $row['PaginaWeb'],array('class'=>'form-control', 'placeholder'=>'', 'required'=>'true', 'parsley-type'=>'url'   )) !!}
										 </div>
										 <div class="col-md-2">

										 </div>
									  </div>
									  <div class="form-group  " >
										<label for="tamano" class=" control-label col-md-4 text-left"> Tamaño Empresa </label>
										<div class="col-md-6">
										<?php $tamano = explode(',',$row['tamano']);
					$tam_opt = array( '' => 'Seleccione Valor' ,  '0' => 'PYME' ,  '1' => 'Pequeña' ,  '2' => 'Mediana' ,  '3' => 'Grande' ,); ?>
					<select name='tamano' id='tamano' rows='5'   class='select2 ' >
						<?php
						foreach($tam_opt as $key=>$val)
						{
							echo "<option  value ='$key' ".($row['tamano'] == $key ? " selected='selected' " : '' )." >$val</option>";
						}
						?></select>
										 </div>
										 <div class="col-md-2">

										 </div>
									  </div>
									  <div class="form-group  " >
										<label for="Categoria" class=" control-label col-md-4 text-left"> Categoria </label>
										<div class="col-md-6">
										  <select name='id_categoria' rows='5' id='id_categoria' class='select2 '   ></select>
										 </div>
										 <div class="col-md-2">

										 </div>
									  </div>
									  
										<div class="form-group  " >
										<label for="Asignado a" class=" control-label col-md-4 text-left"> Asignado a </label>
										<div class="col-md-6">
										  <select name='entry_by_access' rows='5' id='entry_by_access' class='select2 '   ></select>
										 </div>
										 <div class="col-md-2">

										 </div>
									  </div>
									  <?php
				                                    if (strlen($row['IdEstatus'])>0):
				                                    ?>
									  <div class="form-group  " >
										<label for="Estatus" class=" control-label col-md-4 text-left"> Estatus </label>
										<div class="col-md-6">

					<?php $IdEstatus = explode(',',$row['IdEstatus']);
					$IdEstatus_opt = array( '' => 'Seleccione Valor' ,  '1' => 'Activo' ,  '2' => 'Suspendido' , ); ?>
					<select name='IdEstatus' rows='5'   class='select2 '  >
						<?php
						foreach($IdEstatus_opt as $key=>$val)
						{
							echo "<option  value ='$key' ".($row['IdEstatus'] == $key ? " selected='selected' " : '' ).">$val</option>";
						}
						?></select>
										 </div>
										 <div class="col-md-2">

										 </div>
									  </div>
 <?php elseif (strlen($row['IdEstatus'])==0): ?>
                                    <input type="hidden" name="IdEstatus" value="1">
                                    <?php endif; ?>
                                     {!! Form::hidden('createdOn', $row['createdOn']) !!}{!! Form::hidden('entry_by', $row['entry_by']) !!}{!! Form::hidden('updatedOn', $row['updatedOn']) !!}</fieldset>
			</div>




			<div style="clear:both"></div>

			<div class="form-group">
				<label class="col-sm-4 text-right">&nbsp;</label>
				<div class="col-sm-8">
					<button id="guardar" type="submit" class="btn btn-primary btn-sm "><i class="icon-checkmark-circle2"></i>  {{ Lang::get('core.sb_save') }} </button>
					<button type="button" onclick="ajaxViewClose('#{{ $pageModule }}')" class="btn btn-success btn-sm"><i class="icon-cancel-circle2 "></i>  {{ Lang::get('core.sb_cancel') }} </button>
				</div>
			</div>
			{!! Form::close() !!}


@if($setting['form-method'] =='native')
	</div>
</div>
@endif


</div>

<script type="text/javascript">
$(document).ready(function() {

	 	$("#entry_by_access").jCombo("{!! url('personas/comboselect?filter=vw_usuarios_contratistas:id:first_name|last_name') !!}",
		{  selected_value : '{{ $row["entry_by_access"] }}' });
		
				
		$("#id_categoria").jCombo("{!! url('personas/comboselect?filter=tbl_contratista_categoria:id_categoria:nombre_categoria') !!}",
		{  selected_value : '{{ $row["id_categoria"] }}' });



<?php if (strlen($row['IdContratista'])>0) : ?>
    var sub =<?php echo json_encode($subContratista );?>;
		$.post('contratistas/datacontratista',{ id:$('#IdContratista').val(),},function(response,state){
			$("#IdSubContratista").empty();
		for (var i =0; i<response.valores.length; i++) {

			$("#IdSubContratista").append("<option  value='"+response.valores[i].IdContratista+"'>"+response.valores[i].RUT+" - "+response.valores[i].RazonSocial+"</option>");
		}
		$("#IdSubContratista > option").each(function(index, el) {
			var $this = $(this);
			$.each(sub, function(key,value) {
				if (el.value==value.SubContratista){
				  $this.prop("selected","selected");
				   $this.trigger("change");
				}

				});
				});

	});
<?php else: ?>
		$.post('contratistas/datacontratista',function(response,state){
			$("#IdSubContratista").empty();
		for (var i =0; i<response.valores.length; i++) {
			$("#IdSubContratista").append("<option value='"+response.valores[i].IdContratista+"'>"+response.valores[i].RUT+" - "+response.valores[i].RazonSocial+"</option>");
		}
	});
<?php endif; ?>

	   /*Codigo para validar RUT*/
       var Fn = {
		    // Valida el rut con su cadena completa "XXXXXXXX-X"
		    validaRut : function (rutCompleto) {
		        if (!/^[0-9]+-[0-9kK]{1}$/.test( rutCompleto ))
		            return false;
		        var tmp     = rutCompleto.split('-');
		        var digv    = tmp[1];
		        var rut     = tmp[0];
		        if ( digv == 'K' ) digv = 'k' ;
		        return (Fn.dv(rut) == digv );
		    },
		    dv : function(T){
		        var M=0,S=1;
		        for(;T;T=Math.floor(T/10))
		            S=(S+T%10*(9-M++%6))%11;
		        return S?S-1:'k';
		    }
		}

		$("#rut").focusout(function(){
		    if (Fn.validaRut( $("#rut").val() )){
		        $("#rut").css("border-color", "#1ab394");
		        $("#guardar").prop("disabled", false);
		    } else {
		        alert("El Rut no es válido");
		        $("#rut").css("border-color", "#ed5565");
		        $("#guardar").prop("disabled", true);
		    }
		});
		/*Fin cogigo RUT*/

    $("#rut").blur(function(){
	$.post('contratistas/compruebarut',{ rut:$('#rut').val(),},
				function(data) {
				if (data.valores==""){
					if($('#guardar').is(':enabled'))
						$("#guardar").prop("disabled", false);
				}
				else{
					alert("El rut ya se encuentra registrado")
					  $("#guardar").prop("disabled", true);
				}
			 });

	});

	$('.editor').summernote();
	$('.previewImage').fancybox();
	$('.tips').tooltip();
	$(".select2").select2({ width:"98%"});
	$('.date').datepicker({format:'yyyy-mm-dd',autoClose:true})
	$('.datetime').datetimepicker({format: 'yyyy-mm-dd hh:ii:ss'});
	$('input[type="checkbox"],input[type="radio"]').iCheck({
		checkboxClass: 'icheckbox_square-red',
		radioClass: 'iradio_square-red',
	});
		$('.removeMultiFiles').on('click',function(){
			var removeUrl = '{{ url("contratistas/removefiles?file=")}}'+$(this).attr('url');
			$(this).parent().remove();
			$.get(removeUrl,function(response){});
			$(this).parent('div').empty();
			return false;
		});

	var form = $('#contratistasFormAjax');
	form.parsley();
	form.submit(function(){

		if(form.parsley('isValid') == true){
			var options = {
				dataType:      'json',
				beforeSubmit :  showRequest,
				success:       showResponse
			}
			$(this).ajaxSubmit(options);
			return false;

		} else {
			return false;
		}

	});

});

function showRequest()
{
	$('.ajaxLoading').show();
}
function showResponse(data)  {

	if(data.status == 'success')
	{
		ajaxViewClose('#{{ $pageModule }}');
		ajaxFilter('#{{ $pageModule }}','{{ $pageUrl }}/data');
		notyMessage(data.message);
		$('#sximo-modal').modal('hide');
	} else {
		notyMessageError(data.message);
		$('.ajaxLoading').hide();
		return false;
	}
}

</script>
