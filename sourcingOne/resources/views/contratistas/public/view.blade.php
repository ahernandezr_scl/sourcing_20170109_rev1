<div class="m-t" style="padding-top:25px;">	
    <div class="row m-b-lg animated fadeInDown delayp1 text-center">
        <h3> {{ $pageTitle }} <small> {{ $pageNote }} </small></h3>
        <hr />       
    </div>
</div>
<div class="m-t">
	<div class="table-responsive" > 	

		<table class="table table-striped table-bordered" >
			<tbody>	
		
			
					<tr>
						<td width='30%' class='label-view text-right'>{{ SiteHelpers::activeLang('IdContratista', (isset($fields['IdContratista']['language'])? $fields['IdContratista']['language'] : array())) }}</td>
						<td>{{ $row->IdContratista}} </td>
						
					</tr>
				
					<tr>
						<td width='30%' class='label-view text-right'>{{ SiteHelpers::activeLang('RUT', (isset($fields['RUT']['language'])? $fields['RUT']['language'] : array())) }}</td>
						<td>{{ $row->RUT}} </td>
						
					</tr>
				
					<tr>
						<td width='30%' class='label-view text-right'>{{ SiteHelpers::activeLang('Legal Name', (isset($fields['RazonSocial']['language'])? $fields['RazonSocial']['language'] : array())) }}</td>
						<td>{{ $row->RazonSocial}} </td>
						
					</tr>
				
					<tr>
						<td width='30%' class='label-view text-right'>{{ SiteHelpers::activeLang('Social Name', (isset($fields['NombreFantasia']['language'])? $fields['NombreFantasia']['language'] : array())) }}</td>
						<td>{{ $row->NombreFantasia}} </td>
						
					</tr>
				
					<tr>
						<td width='30%' class='label-view text-right'>{{ SiteHelpers::activeLang('legal representative', (isset($fields['Representante']['language'])? $fields['Representante']['language'] : array())) }}</td>
						<td>{{ $row->Representante}} </td>
						
					</tr>
				
					<tr>
						<td width='30%' class='label-view text-right'>{{ SiteHelpers::activeLang('legal representative fone', (isset($fields['RepresentanteFono']['language'])? $fields['RepresentanteFono']['language'] : array())) }}</td>
						<td>{{ $row->RepresentanteFono}} </td>
						
					</tr>
				
					<tr>
						<td width='30%' class='label-view text-right'>{{ SiteHelpers::activeLang('legal representative email', (isset($fields['RepresentanteEmail']['language'])? $fields['RepresentanteEmail']['language'] : array())) }}</td>
						<td>{{ $row->RepresentanteEmail}} </td>
						
					</tr>
				
					<tr>
						<td width='30%' class='label-view text-right'>{{ SiteHelpers::activeLang('address', (isset($fields['Direccion']['language'])? $fields['Direccion']['language'] : array())) }}</td>
						<td>{{ $row->Direccion}} </td>
						
					</tr>
				
					<tr>
						<td width='30%' class='label-view text-right'>{{ SiteHelpers::activeLang('Fone', (isset($fields['Fono']['language'])? $fields['Fono']['language'] : array())) }}</td>
						<td>{{ $row->Fono}} </td>
						
					</tr>
				
					<tr>
						<td width='30%' class='label-view text-right'>{{ SiteHelpers::activeLang('Email', (isset($fields['Email']['language'])? $fields['Email']['language'] : array())) }}</td>
						<td>{{ $row->Email}} </td>
						
					</tr>
				
					<tr>
						<td width='30%' class='label-view text-right'>{{ SiteHelpers::activeLang('Web Page', (isset($fields['PaginaWeb']['language'])? $fields['PaginaWeb']['language'] : array())) }}</td>
						<td>{{ $row->PaginaWeb}} </td>
						
					</tr>
				
					<tr>
						<td width='30%' class='label-view text-right'>{{ SiteHelpers::activeLang('Status', (isset($fields['IdEstatus']['language'])? $fields['IdEstatus']['language'] : array())) }}</td>
						<td>{{ $row->IdEstatus}} </td>
						
					</tr>
				
					<tr>
						<td width='30%' class='label-view text-right'>{{ SiteHelpers::activeLang('Created On', (isset($fields['createdOn']['language'])? $fields['createdOn']['language'] : array())) }}</td>
						<td>{{ date('d/m/Y',strtotime($row->createdOn)) }} </td>
						
					</tr>
				
					<tr>
						<td width='30%' class='label-view text-right'>{{ SiteHelpers::activeLang('Entry By', (isset($fields['entry_by']['language'])? $fields['entry_by']['language'] : array())) }}</td>
						<td>{{ SiteHelpers::formatLookUp($row->entry_by,'entry_by','1:tb_users:id:first_name|last_name') }} </td>
						
					</tr>
				
					<tr>
						<td width='30%' class='label-view text-right'>{{ SiteHelpers::activeLang('Updated On', (isset($fields['updatedOn']['language'])? $fields['updatedOn']['language'] : array())) }}</td>
						<td>{{ date('d/m/Y',strtotime($row->updatedOn)) }} </td>
						
					</tr>
						
					<tr>
						<td width='30%' class='label-view text-right'></td>
						<td> <a href="javascript:history.go(-1)" class="btn btn-primary"> Back To Grid <a> </td>
						
					</tr>					
				
			</tbody>	
		</table>   

	 
	
	</div>
</div>	