

		 {!! Form::open(array('url'=>'contratistas/savepublic', 'class'=>'form-horizontal','files' => true , 'parsley-validate'=>'','novalidate'=>' ')) !!}

	@if(Session::has('messagetext'))
	  
		   {!! Session::get('messagetext') !!}
	   
	@endif
	<ul class="parsley-error-list">
		@foreach($errors->all() as $error)
			<li>{{ $error }}</li>
		@endforeach
	</ul>		


<div class="col-md-12">
						<fieldset><legend> Builder</legend>
				{!! Form::hidden('IdContratista', $row['IdContratista']) !!}					
									  <div class="form-group  " >
										<label for="RUT" class=" control-label col-md-4 text-left"> RUT <span class="asterix"> * </span></label>
										<div class="col-md-6">
										  {!! Form::text('RUT', $row['RUT'],array('class'=>'form-control', 'placeholder'=>'', 'required'=>'true'  )) !!} 
										 </div> 
										 <div class="col-md-2">
										 	
										 </div>
									  </div> 					
									  <div class="form-group  " >
										<label for="Legal Name" class=" control-label col-md-4 text-left"> Legal Name <span class="asterix"> * </span></label>
										<div class="col-md-6">
										  {!! Form::text('RazonSocial', $row['RazonSocial'],array('class'=>'form-control', 'placeholder'=>'', 'required'=>'true'  )) !!} 
										 </div> 
										 <div class="col-md-2">
										 	
										 </div>
									  </div> 					
									  <div class="form-group  " >
										<label for="Social Name" class=" control-label col-md-4 text-left"> Social Name </label>
										<div class="col-md-6">
										  {!! Form::text('NombreFantasia', $row['NombreFantasia'],array('class'=>'form-control', 'placeholder'=>'',   )) !!} 
										 </div> 
										 <div class="col-md-2">
										 	
										 </div>
									  </div> 					
									  <div class="form-group  " >
										<label for="legal representative" class=" control-label col-md-4 text-left"> legal representative </label>
										<div class="col-md-6">
										  {!! Form::text('Representante', $row['Representante'],array('class'=>'form-control', 'placeholder'=>'',   )) !!} 
										 </div> 
										 <div class="col-md-2">
										 	
										 </div>
									  </div> 					
									  <div class="form-group  " >
										<label for="legal representative phone" class=" control-label col-md-4 text-left"> legal representative phone </label>
										<div class="col-md-6">
										  {!! Form::text('RepresentanteFono', $row['RepresentanteFono'],array('class'=>'form-control', 'placeholder'=>'',   )) !!} 
										 </div> 
										 <div class="col-md-2">
										 	
										 </div>
									  </div> 					
									  <div class="form-group  " >
										<label for="legal representative email" class=" control-label col-md-4 text-left"> legal representative email </label>
										<div class="col-md-6">
										  {!! Form::text('RepresentanteEmail', $row['RepresentanteEmail'],array('class'=>'form-control', 'placeholder'=>'',   )) !!} 
										 </div> 
										 <div class="col-md-2">
										 	
										 </div>
									  </div> 					
									  <div class="form-group  " >
										<label for="address" class=" control-label col-md-4 text-left"> address </label>
										<div class="col-md-6">
										  {!! Form::text('Direccion', $row['Direccion'],array('class'=>'form-control', 'placeholder'=>'',   )) !!} 
										 </div> 
										 <div class="col-md-2">
										 	
										 </div>
									  </div> 					
									  <div class="form-group  " >
										<label for="Phone Number" class=" control-label col-md-4 text-left"> Phone Number <span class="asterix"> * </span></label>
										<div class="col-md-6">
										  {!! Form::text('Fono', $row['Fono'],array('class'=>'form-control', 'placeholder'=>'', 'required'=>'true', 'parsley-type'=>'number'   )) !!} 
										 </div> 
										 <div class="col-md-2">
										 	
										 </div>
									  </div> 					
									  <div class="form-group  " >
										<label for="Email" class=" control-label col-md-4 text-left"> Email <span class="asterix"> * </span></label>
										<div class="col-md-6">
										  {!! Form::text('Email', $row['Email'],array('class'=>'form-control', 'placeholder'=>'', 'required'=>'true', 'parsley-type'=>'email'   )) !!} 
										 </div> 
										 <div class="col-md-2">
										 	
										 </div>
									  </div> 					
									  <div class="form-group  " >
										<label for="Web Page" class=" control-label col-md-4 text-left"> Web Page <span class="asterix"> * </span></label>
										<div class="col-md-6">
										  {!! Form::text('PaginaWeb', $row['PaginaWeb'],array('class'=>'form-control', 'placeholder'=>'', 'required'=>'true', 'parsley-type'=>'url'   )) !!} 
										 </div> 
										 <div class="col-md-2">
										 	
										 </div>
									  </div> 					
									  <div class="form-group  " >
										<label for="Status" class=" control-label col-md-4 text-left"> Status </label>
										<div class="col-md-6">
										  
					<?php $IdEstatus = explode(',',$row['IdEstatus']);
					$IdEstatus_opt = array( '1' => 'ACTIVO' ,  '2' => 'INACTIVO' , ); ?>
					<select name='IdEstatus' rows='5'   class='select2 '  > 
						<?php 
						foreach($IdEstatus_opt as $key=>$val)
						{
							echo "<option  value ='$key' ".($row['IdEstatus'] == $key ? " selected='selected' " : '' ).">$val</option>"; 						
						}						
						?></select> 
										 </div> 
										 <div class="col-md-2">
										 	
										 </div>
									  </div> {!! Form::hidden('createdOn', $row['createdOn']) !!}{!! Form::hidden('updatedOn', $row['updatedOn']) !!}</fieldset>
			</div>
			
			

			<div style="clear:both"></div>	
				
					
				  <div class="form-group">
					<label class="col-sm-4 text-right">&nbsp;</label>
					<div class="col-sm-8">	
					<button type="submit" name="apply" class="btn btn-info btn-sm" ><i class="fa  fa-check-circle"></i> {{ Lang::get('core.sb_apply') }}</button>
					<button type="submit" name="submit" class="btn btn-primary btn-sm" ><i class="fa  fa-save "></i> {{ Lang::get('core.sb_save') }}</button>
				  </div>	  
			
		</div> 
		 
		 {!! Form::close() !!}
		 
   <script type="text/javascript">
	$(document).ready(function() { 
		
		 

		$('.removeCurrentFiles').on('click',function(){
			var removeUrl = $(this).attr('href');
			$.get(removeUrl,function(response){});
			$(this).parent('div').empty();	
			return false;
		});		
		
	});
	</script>		 
