
@if($setting['form-method'] =='native')
	<div class="sbox">
		<div class="sbox-title">  
			<h4> <i class="fa fa-table"></i> <?php echo $pageTitle ;?> <small>{{ $pageNote }}</small>
				<a href="javascript:void(0)" class="collapse-close pull-right btn btn-xs btn-danger" onclick="ajaxViewClose('#{{ $pageModule }}')"><i class="fa fa fa-times"></i></a>
			</h4>
	</div>

	<div class="sbox-content"> 
@endif	
			{!! Form::open(array('url'=>'requisitos/save/'.SiteHelpers::encryptID($row['IdRequisito']), 'class'=>'form-horizontal','files' => true , 'parsley-validate'=>'','novalidate'=>' ','id'=> 'requisitosFormAjax')) !!}
			<div class="col-md-12">
						<fieldset><legend> requisitos</legend>
				{!! Form::hidden('IdRequisito', $row['IdRequisito']) !!}					
									  <div class="form-group  " >
										<label for="Entidad" class=" control-label col-md-4 text-left"> Entidad <span class="asterix"> * </span></label>
										<div class="col-md-6">
										  
					<?php $Entidad = explode(',',$row['Entidad']);
					$Entidad_opt = array( '1' => 'Contratistas' ,  '2' => 'Contratos' ,  '3' => 'Personas' ,  '6' => 'Centros' , ); ?>
					<select name='Entidad' rows='5' required  class='select2 '  > 
						<?php 
						foreach($Entidad_opt as $key=>$val)
						{
							echo "<option  value ='$key' ".($row['Entidad'] == $key ? " selected='selected' " : '' ).">$val</option>"; 						
						}						
						?></select> 
										 </div> 
										 <div class="col-md-2">
										 	
										 </div>
									  </div> 					
									  <div class="form-group  " >
										<label for="Tipo Documento" class=" control-label col-md-4 text-left"> Tipo Documento <span class="asterix"> * </span></label>
										<div class="col-md-6">
										  <select name='IdTipoDocumento' rows='5' id='IdTipoDocumento' class='select2 ' required  ></select> 
										 </div> 
										 <div class="col-md-2">
										 	
										 </div>
									  </div> {!! Form::hidden('FechaCumplimiento', $row['FechaCumplimiento']) !!}					
									  <div class="form-group  " >
										<label for="Vigencia" class=" control-label col-md-4 text-left"> Aplicado a </label>
										<div class="col-md-6">
										  
					<?php $Vigencia = explode(',',$row['Vigencia']);
					$Vigencia_opt = array( '' => 'Seleccione Valor' ,  'Todos' => 'Para todos' ,  'Ahora' => 'De ahora en adelante' , ); ?>
					<select name='Vigencia' rows='5'   class='select2 '  > 
						<?php 
						foreach($Vigencia_opt as $key=>$val)
						{
							echo "<option  value ='$key' ".($row['Vigencia'] == $key ? " selected='selected' " : '' ).">$val</option>"; 						
						}						
						?></select> 
										 </div> 
										 <div class="col-md-2">
										 	
										 </div>
									  </div> {!! Form::hidden('createdOn', $row['createdOn']) !!}{!! Form::hidden('entry_by', $row['entry_by']) !!}{!! Form::hidden('updatedOn', $row['updatedOn']) !!}</fieldset>
			</div>
			
												
								
						
			<div style="clear:both"></div>	
							
			<div class="form-group">
				<label class="col-sm-4 text-right">&nbsp;</label>
				<div class="col-sm-8">	
					<button type="submit" class="btn btn-primary btn-sm "><i class="icon-checkmark-circle2"></i>  {{ Lang::get('core.sb_save') }} </button>
					<button type="button" onclick="ajaxViewClose('#{{ $pageModule }}')" class="btn btn-success btn-sm"><i class="icon-cancel-circle2 "></i>  {{ Lang::get('core.sb_cancel') }} </button>
				</div>			
			</div> 		 
			{!! Form::close() !!}


@if($setting['form-method'] =='native')
	</div>	
</div>	
@endif	

	
</div>	
			 
<script type="text/javascript">
$(document).ready(function() { 
	
		$("#IdTipoDocumento").jCombo("{!! url('requisitos/comboselect?filter=tbl_tipos_documentos:IdTipoDocumento:Descripcion') !!}",
		{  selected_value : '{{ $row["IdTipoDocumento"] }}' });
		 
	
	$('.editor').summernote();
	$('.previewImage').fancybox();	
	$('.tips').tooltip();	
	$(".select2").select2({ width:"98%"});	
	$('.date').datepicker({format:'yyyy-mm-dd',autoClose:true})
	$('.datetime').datetimepicker({format: 'yyyy-mm-dd hh:ii:ss'}); 
	$('input[type="checkbox"],input[type="radio"]').iCheck({
		checkboxClass: 'icheckbox_square-red',
		radioClass: 'iradio_square-red',
	});			
		$('.removeMultiFiles').on('click',function(){
			var removeUrl = '{{ url("requisitos/removefiles?file=")}}'+$(this).attr('url');
			$(this).parent().remove();
			$.get(removeUrl,function(response){});
			$(this).parent('div').empty();	
			return false;
		});
				
	var form = $('#requisitosFormAjax'); 
	form.parsley();
	form.submit(function(){
		
		if(form.parsley('isValid') == true){			
			var options = { 
				dataType:      'json', 
				beforeSubmit :  showRequest,
				success:       showResponse  
			}  
			$(this).ajaxSubmit(options); 
			return false;
						
		} else {
			return false;
		}		
	
	});

});

function showRequest()
{
	$('.ajaxLoading').show();		
}  
function showResponse(data)  {		
	
	if(data.status == 'success')
	{
		ajaxViewClose('#{{ $pageModule }}');
		ajaxFilter('#{{ $pageModule }}','{{ $pageUrl }}/data');
		notyMessage(data.message);	
		$('#sximo-modal').modal('hide');	
	} else {
		notyMessageError(data.message);	
		$('.ajaxLoading').hide();
		return false;
	}	
}			 

</script>		 