@extends('layouts.app')
@section('content')

<?php
$servername = "localhost";
$username = "root";
$password = "8044302";

#try {
    $conn2 = new PDO("mysql:host=$servername;dbname=andina_koadmin", $username, $password);
    // set the PDO error mode to exception
#    $conn2->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
#   echo "Connected successfully";
#    }
#catch(PDOException $e)
#    {
#    echo "Connection failed: " . $e->getMessage();
#    }
?>

<style type="text/css">
.circle_green
    {
  display: table-cell;
  vertical-align: middle;
  background: #3d9b35;
  font-size: 10px;
  color: white;

    }

.circle_red
{
  display: table-cell;
  vertical-align: middle;
  background: #e64427;
  font-size: 10px;
  color: white;
}

.circle_yellow
{
  display: table-cell;
  vertical-align: middle;
  background: #ffff00;
  font-size: 10px;
  color: black;
}

.circle_grey
{
  display: table-cell;
  vertical-align: middle;
  background: #cec6c0;
  font-size: 10px;
  color: white;
}
.circle {
  position: relative;
  display: inline-block;
  width: 100%;
  height: 0;
  padding: 50% 0;
  border-radius: 50%;
  margin: 5px;

  /* Just making it pretty */
  @shadow: rgba(0, 0, 0, .1);
  @shadow-length: 4px;
  -webkit-box-shadow: 0 @shadow-length 0 0 @shadow;
          box-shadow: 0 @shadow-length 0 0 @shadow;
  text-shadow: 0 @shadow-length 0 @shadow;
  text-align: center;
  line-height: 0px
}
.circle-container{
  display: inline-block;
}

        #embed-container {
                position: relative;
                padding-bottom: 56.25%;
                height: 0;
                overflow: hidden;
        }
        #embed-container iframe {
                position: absolute;
                top: 0;
                left: 0;
                width: 100%;
                height: 100%;
        }
        .sbox-content{
          padding-bottom: 10px;
          border-radius: 5px;
        }
        .table{
          margin-bottom: 10px;
        }
        #page-wrapper{
          background-color: gray;
        }
        .m-t{
          margin-bottom: -15px;
        }
        .inner-row{
          margin-left: -15px;
          margin-right: -15px;
        }
        .page-content{
          margin-left: 0px;
          margin-right: 0px;
        }

</style>

@if(Auth::check() && Auth::user()->group_id == 1)

    <script src="https://code.highcharts.com/highcharts.js"></script>
    <script src="https://code.highcharts.com/modules/exporting.js"></script>
    <script src="http://code.highcharts.com/highcharts-more.js"></script>

<div class="page-content row" style="background-color:gray">

  <div class="row m-t">

    <div class="col-lg-1">
    </div>

    <div class="col-lg-3">
      <div class="sbox">
        <div class="pull-center">
          <div class="sbox-content" align="center">
            <h4>Empresas Contratistas</h4>
            <h1>12</h1>
          </div>
        </div>
      </div>
    </div>

    <div class="col-lg-4">
      <div class="sbox">
        <div class="pull-center">
          <div class="sbox-content" align="center">
            <h4>Contratos Vigentes</h4>
            <h1>10</h1>
          </div>
        </div>
      </div>
    </div>

    <div class="col-lg-3">
      <div class="sbox">
        <div class="pull-center">
          <div class="sbox-content" align="center">
            <h4>Monto Contratos Vigentes</h4>
            <h1>$266,14MM</h1>
          </div>
        </div>
      </div>
    </div>

    <div class="col-lg-1">
    </div>

  </div>


  <div class="row m-t">

    <div class="col-lg-3">
      <div class="sbox">
        <div class="sbox-content" style="min-height: 348px;">
          <a href="{{ action('ReportglobalnegociofisicofinancieroController@getIndex')}}"><h4>Avance Financiero</br>&nbsp</h4></a>
          <div style=" width: 100%;margin: 10px 0 ; overflow-y: auto;">
            <table style='width: 100%;'>
              <tr>
                <td align="center">
                  <a href="{{ action('ReportdetavafisController@getIndex')}}">
                    <div class="circle-container">
                      <div class="circle_yellow circle" style="font-size: 1.3em; font-weight: bold; display: inline-block;" align="center">-7,3%</div>
                    </div>
                  </a>
                </td>
                <td><div style="padding-left: 5px; font-weight: bold; font-size: 0.90em;">($5,6MM real / $6MM Ppto)</div></td>
              </tr>
              <tr>
                <td colspan="2">&nbsp</td>
              </tr>
            </table>
          </div>
          <div id="tabla1" style=" width: 100%;margin: 10px 0 ; overflow-y: auto;" >
            <table style='border: solid 0px;' class='table'>
              <tr align="center" style="font-weight: bold; background-color: #ecf0f2;">
                <td>N° Cttos</td>
                <td>Término</td>
                <td>Estado</th>
              </tr>
              <tr align="center">
                <td style="vertical-align: middle;">3</td>
                <td style="vertical-align: middle;">3 meses</td>
                <td style="vertical-align: middle;">
                  <div class="circle-container">
                    <div class="circle_red circle" style="font-size: 1.3em;">30%</div>
                  </div>
                </td>
              </tr>
              <tr align="center">
                <td style="vertical-align: middle;">1</td>
                <td style="vertical-align: middle;">6 meses</td>
                <td style="vertical-align: middle;">
                  <div class="circle-container">
                    <div class="circle_yellow circle" style="font-size: 1.3em;">10%</div>
                  </div>
                </td>
              </tr>
              <tr align="center">
                <td style="vertical-align: middle;">6</td>
                <td style="vertical-align: middle;">+12 meses</td>
                <td style="vertical-align: middle;">
                  <div class="circle-container">
                    <div class="circle_green circle" style="font-size: 1.3em;">60%</div>
                  <div class="circle-container">
                </td>
              </tr>
            </table>
          </div>
          </div>
        </div><!-- /sbox -->
      </div><!-- </div class="col-lg-4"> -->

      <div class="col-lg-3">

        <div class="row inner-row">

          <div class="col-lg-12">
            <div class="sbox">
              <div class="sbox-content">
                <div style="top: 10px;  left: 0;   width: 100%;" height="300px;background: white;">
                  <a href="{{ action('ReportglobalnegociocumplimientokpiController@getIndex')}}">
                    <h4>KPI Contratos</h4>
                  </a>
                  <div id="tabla2" style=" width: 100%;margin: 10px 0 ; overflow-y: auto;" >
                    <div style=" width: 100%;margin: 10px 0 ; overflow-y: auto;">
                      <table style='width: 100%;'>
                        <tr>
                          <td align="center">
                            <a href="{{ action('ReportglobalnegociocumplimientokpiController@getIndex')}}">
                              <div class="circle-container">
                                <div class="circle_green circle" style="font-size: 1.3em; font-weight: bold; display: inline-block;" align="center">77,3%</div>
                              </div>
                            </a>
                          </td>
                        </tr>
                        <tr>
                          <td align="center"><75%: 5 Cttos <b>(50%)</b></td>
                        </tr>
                      </table>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>

        </div>

        <div class="row inner-row">

          <div class="col-lg-12">
            <div class="sbox">
              <div class="sbox-content">
                <a href="{{ action('ReportambseguridadController@getIndex')}}">
                  <h4>Resultados Accidentabilidad</h4>
                </a>
                <div id="tabla8" style=" width: 100%;margin: 10px 0 ; overflow-y: auto;" >
                  <table style='border: solid 0px;' class='table'>
                    <tr style="font-weight: bold; background-color: #ecf0f2;">
                      <td>Indice</td>
                      <td align="center">Real</td>
                      <td align="center">Ppto</td>
                    </tr>
                    <tr>
                      <td style="font-weight: bold; vertical-align: middle;">Frecuencia</td>
                      <td align="center" style="vertical-align: middle;">
                        <a href="{{ action('ReportglobalseguridadycondicionesaccidentabilidadController@getIndex')}}">
                          <div class="circle-container">
                            <div class="circle_green circle">0,29</div>
                          </div>
                        </a>
                      </td>
                      <td align="center" style="vertical-align: middle;">1,5</td>
                    </tr>
                    <tr>
                      <td style="font-weight: bold; vertical-align: middle;">Gravedad</td>
                      <td align="center" style="vertical-align: middle;">
                        <a href="{{ action('ReportglobalseguridadycondicionesgravedadController@getIndex')}}">
                          <div class="circle-container">
                            <div class="circle_green circle">0,29</div>
                          </div>
                        </a>
                      </td>
                      <td align="center" style="vertical-align: middle;">20</td>
                    </tr>
                  </table>
                </div>
              </div>
            </div><!-- /sbox -->
          </div>

        </div>

      </div>

      <div class="col-lg-3">
        <div class="sbox">
          <div class="sbox-content" style="min-height: 348px;">
            <h4>Personas</br>&nbsp</h4>
            <div id="tabla4" style=" width: 100%;margin: 10px 0 ; overflow-y: auto;">
              <table style='width: 100%; text-align: center;'>
                <tr>
                  <td>Dotación</td>
                  <td>Remun.</td>
                  <td>Obligaciones</br>Laborales</td>
                </tr>
                <tr>
                  <td><a href="{{ action('ReportgloballaboralobligacionesnocubiertasController@getIndex')}}">
                    <div class="circle-container">
                      <div class="circle_green circle" style="font-size: 1.3em;font-weight: bold; display: inline-block;" align="center">8%</div>
                    </div>
                  </a>
                  </td>
                  <td>
                    <a href="{{ action('ReportgloballaboralobligacionesnocubiertasController@getIndex')}}">
                      <div class="circle-container">
                        <div class="circle_red circle" style="font-size: 1.3em;font-weight: bold; display: inline-block;" align="center">3%</div>
                      </div>
                    </a>
                  </td>
                  <td>
                    <a href="{{ action('ReportgloballaboralobligacionesnocubiertasController@getIndex')}}">
                      <div class="circle-container">
                        <div class="circle_green circle" style="font-size: 1.3em;font-weight: bold; display: inline-block;" align="center">3%</div>
                      </div>
                    </a>
                  </td>
                </tr>
                <tr>
                  <td colspan="3">&nbsp</td>
                </tr>
              </table>
              <table style='border: solid 0px;' class='table'>
                <tr align="center" style="font-weight: bold; background-color: #ecf0f2;">
                  <td></td>
                  <td>Real</td>
                  <td>Ppto</td>
                </tr>
                <tr>
                  <td style="font-weight: bold;">Dotación</td>
                  <td align="center" style="vertical-align: middle;">117</td>
                  <td align="center" style="vertical-align: middle;">120</td>
                </tr>
                <tr style="vertical-align: middle;">
                  <td style="font-weight: bold;">Remun.</td>
                  <td align="center" style="vertical-align: middle;">$936M</td>
                  <td align="center" style="vertical-align: middle;">$960M</td>
                </tr>
                <tr style="vertical-align: middle;">
                  <td style="font-weight: bold;">Obligaciones<br>Laborales</td>
                  <td align="center" style="vertical-align: middle;">4,4%</td>
                  <td align="center" style="vertical-align: middle;">0%</td>
                </tr>
              </table>
              &nbsp</br>&nbsp</br>&nbsp</br>&nbsp</br>&nbsp</br>
            </div>
          </div>
        </div>

    </div>

    <div class="col-lg-3">
      <div class="row inner-row">
        <div class="col-lg-12">
          <div class="sbox">
            <div class="sbox-content">
              <div style="top: 10px;  left: 0;   width: 100%;" height="300px;background: white;">
                <a href="{{ action('ReportglobalnegocioevalproveedorController@getIndex')}}">
                  <h4>Evaluación Proveedor</h4>
                </a>
                <div id="tabla3" style=" width: 100%;margin: 10px 0 ; overflow-y: auto;" >
                  <div style=" width: 100%;margin: 10px 0 ; overflow-y: auto;">
                    <table style='width: 100%;'>
                      <tr>
                        <td align="center">
                          <a href="{{ action('ReportglobalnegocioevalproveedorController@getIndex')}}">
                            <div class="circle-container">
                              <div class="circle_green circle" style="font-size: 1.3em; font-weight: bold; display: inline-block;" align="center">71,1%</div>
                            </div>
                          </a>
                        </td>
                      </tr>
                      <tr>
                        <td align="center"><70%: 4 Cttas <b>(40%)</b></td>
                      </tr>
                    </table>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
      <div class="row inner-row">
        <div class="col-lg-12">
          <div class="sbox">
            <div class="sbox-content">
              <a href="{{ action('ReportambfinancieroController@getIndex')}}">
                <h4>Empresas Riesgo Financiero</h4>
              </a>
              <div id="tabla5" style=" width: 100%;margin: 10px 0 ; overflow-y: auto;" >
                <table style='border: solid 0px;' class='table'>
                  <tr align="center" style="font-weight: bold; background-color: #ecf0f2;">
                    <td>Empresas</td>
                    <td>Porcentaje</td>
                    <td>Riesgo</td>
                  </tr>
                  <tr align="center">
                    <td style="vertical-align: middle;">3</td>
                    <td style="vertical-align: middle;">30%</td>
                    <td style="vertical-align: middle;">
                      <a href="{{ action('ReportglobalfinancieroController@getIndex')}}">
                        <div class="circle-container">
                          <div class="circle_red circle">Alto</i></div>
                        </div>
                      </a>
                    </td>
                  </tr>
                  <tr align="center">
                    <td style="vertical-align: middle;">4</td>
                    <td style="vertical-align: middle;">40%</td>
                    <td style="vertical-align: middle;">
                      <a href="{{ action('ReportglobalfinancieroController@getIndex')}}">
                        <div class="circle-container">
                          <div class="circle_yellow circle">Med</i></div>
                        </div>
                      </a>
                    </td>
                  </tr>
                </table>
              </div>
            </div>
          </div>
        </div><!-- /sbox -->
      </div>
    </div><!-- </div class="row-mt"> -->
  </div>
</div>


    @endif

 @stop
