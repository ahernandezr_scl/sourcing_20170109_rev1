<div class="m-t" style="padding-top:25px;">	
    <div class="row m-b-lg animated fadeInDown delayp1 text-center">
        <h3> {{ $pageTitle }} <small> {{ $pageNote }} </small></h3>
        <hr />       
    </div>
</div>
<div class="m-t">
	<div class="table-responsive" > 	

		<table class="table table-striped table-bordered" >
			<tbody>	
		
			
					<tr>
						<td width='30%' class='label-view text-right'>{{ SiteHelpers::activeLang('Id Maq', (isset($fields['id_maq']['language'])? $fields['id_maq']['language'] : array())) }}</td>
						<td>{{ $row->id_maq}} </td>

					</tr>
				
					<tr>
						<td width='30%' class='label-view text-right'>{{ SiteHelpers::activeLang('Serial Number', (isset($fields['num_serie']['language'])? $fields['num_serie']['language'] : array())) }}</td>
						<td>{{ $row->num_serie}} </td>

					</tr>
				
					<tr>
						<td width='30%' class='label-view text-right'>{{ SiteHelpers::activeLang('Name', (isset($fields['Nombre']['language'])? $fields['Nombre']['language'] : array())) }}</td>
						<td>{{ $row->Nombre}} </td>

					</tr>
				
					<tr>
						<td width='30%' class='label-view text-right'>{{ SiteHelpers::activeLang('Model', (isset($fields['modelo']['language'])? $fields['modelo']['language'] : array())) }}</td>
						<td>{{ $row->modelo}} </td>

					</tr>
				
					<tr>
						<td width='30%' class='label-view text-right'>{{ SiteHelpers::activeLang('Year', (isset($fields['Año']['language'])? $fields['Año']['language'] : array())) }}</td>
						<td>{{ $row->Año}} </td>

					</tr>
				
					<tr>
						<td width='30%' class='label-view text-right'>{{ SiteHelpers::activeLang('Machinery Type', (isset($fields['tipo_maq']['language'])? $fields['tipo_maq']['language'] : array())) }}</td>
						<td>{{ SiteHelpers::formatLookUp($row->tipo_maq,'tipo_maq','1:tbl_maquinaria_tipo:id_maq_tpo:tipo_maquinaria') }} </td>

					</tr>
				
					<tr>
						<td width='30%' class='label-view text-right'>{{ SiteHelpers::activeLang('Status', (isset($fields['idEstatus']['language'])? $fields['idEstatus']['language'] : array())) }}</td>
						<td>{{ $row->idEstatus}} </td>

					</tr>
				
					<tr>
						<td width='30%' class='label-view text-right'>{{ SiteHelpers::activeLang('Entry By', (isset($fields['entry_by']['language'])? $fields['entry_by']['language'] : array())) }}</td>
						<td>{{ $row->entry_by}} </td>

					</tr>
				
					<tr>
						<td width='30%' class='label-view text-right'>{{ SiteHelpers::activeLang('Entry By Access', (isset($fields['entry_by_access']['language'])? $fields['entry_by_access']['language'] : array())) }}</td>
						<td>{{ $row->entry_by_access}} </td>

					</tr>
				
					<tr>
						<td width='30%' class='label-view text-right'>{{ SiteHelpers::activeLang('Created On', (isset($fields['created_on']['language'])? $fields['created_on']['language'] : array())) }}</td>
						<td>{{ $row->created_on}} </td>

					</tr>
						
					<tr>
						<td width='30%' class='label-view text-right'></td>
						<td> <a href="javascript:history.go(-1)" class="btn btn-primary"> Back To Grid <a> </td>
						
					</tr>					
				
			</tbody>	
		</table>   

	 
	
	</div>
</div>	