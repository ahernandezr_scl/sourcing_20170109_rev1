

		 {!! Form::open(array('url'=>'maquinctrl/savepublic', 'class'=>'form-horizontal','files' => true , 'parsley-validate'=>'','novalidate'=>' ')) !!}

	@if(Session::has('messagetext'))
	  
		   {!! Session::get('messagetext') !!}
	   
	@endif
	<ul class="parsley-error-list">
		@foreach($errors->all() as $error)
			<li>{{ $error }}</li>
		@endforeach
	</ul>		


<div class="col-md-12">
						<fieldset><legend> Maquinarias</legend>
				
									  <div class="form-group  " >
										<label for="Serial Number" class=" control-label col-md-4 text-left"> Serial Number </label>
										<div class="col-md-6">
										  {!! Form::text('num_serie', $row['num_serie'],array('class'=>'form-control', 'placeholder'=>'',   )) !!}
										 </div>
										 <div class="col-md-2">
										 	
										 </div>
									  </div> 
									  <div class="form-group  " >
										<label for="Name" class=" control-label col-md-4 text-left"> Name </label>
										<div class="col-md-6">
										  <textarea name='Nombre' rows='5' id='Nombre' class='form-control '
				           >{{ $row['Nombre'] }}</textarea>
										 </div>
										 <div class="col-md-2">
										 	
										 </div>
									  </div> 
									  <div class="form-group  " >
										<label for="Model" class=" control-label col-md-4 text-left"> Model </label>
										<div class="col-md-6">
										  {!! Form::text('modelo', $row['modelo'],array('class'=>'form-control', 'placeholder'=>'',   )) !!}
										 </div>
										 <div class="col-md-2">
										 	
										 </div>
									  </div> 
									  <div class="form-group  " >
										<label for="Year" class=" control-label col-md-4 text-left"> Year </label>
										<div class="col-md-6">
										  {!! Form::text('Año', $row['Año'],array('class'=>'form-control', 'placeholder'=>'',   )) !!}
										 </div>
										 <div class="col-md-2">
										 	
										 </div>
									  </div> 
									  <div class="form-group  " >
										<label for="Machinery Type" class=" control-label col-md-4 text-left"> Machinery Type </label>
										<div class="col-md-6">
										  <select name='tipo_maq' rows='5' id='tipo_maq' class='select2 '   ></select>
										 </div>
										 <div class="col-md-2">
										 	
										 </div>
									  </div> 
									  <div class="form-group  " >
										<label for="Status" class=" control-label col-md-4 text-left"> Status </label>
										<div class="col-md-6">
										  
					<?php $idEstatus = explode(',',$row['idEstatus']);
					$idEstatus_opt = array( '1' => 'Activo' ,  '2' => 'Inactivo' , ); ?>
					<select name='idEstatus' rows='5'   class='select2 '  > 
						<?php
						foreach($idEstatus_opt as $key=>$val)
						{
							echo "<option  value ='$key' ".($row['idEstatus'] == $key ? " selected='selected' " : '' ).">$val</option>";
						}
						?></select>
										 </div>
										 <div class="col-md-2">
										 	
										 </div>
									  </div> </fieldset>
			</div>

			

			<div style="clear:both"></div>	
				
					
				  <div class="form-group">
					<label class="col-sm-4 text-right">&nbsp;</label>
					<div class="col-sm-8">	
					<button type="submit" name="apply" class="btn btn-info btn-sm" ><i class="fa  fa-check-circle"></i> {{ Lang::get('core.sb_apply') }}</button>
					<button type="submit" name="submit" class="btn btn-primary btn-sm" ><i class="fa  fa-save "></i> {{ Lang::get('core.sb_save') }}</button>
				  </div>	  
			
		</div> 
		 
		 {!! Form::close() !!}
		 
   <script type="text/javascript">
	$(document).ready(function() { 
		
		
		$("#tipo_maq").jCombo("{!! url('maquinctrl/comboselect?filter=tbl_maquinaria_tipo:id_maq_tpo:tipo_maquinaria') !!}",
		{  selected_value : '{{ $row["tipo_maq"] }}' });
		 

		$('.removeCurrentFiles').on('click',function(){
			var removeUrl = $(this).attr('href');
			$.get(removeUrl,function(response){});
			$(this).parent('div').empty();	
			return false;
		});		
		
	});
	</script>		 
