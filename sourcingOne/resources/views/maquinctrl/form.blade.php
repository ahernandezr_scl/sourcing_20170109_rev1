@extends('layouts.app')

@section('content')

  <div class="page-content row">

 	<div class="page-content-wrapper m-t">


<div class="sbox">
	<div class="sbox-title"> <h3> {{ $pageTitle }} <small>{{ $pageNote }}</small></h3> </div>
	<div class="sbox-content"> 	

		<ul class="parsley-error-list">
			@foreach($errors->all() as $error)
				<li>{{ $error }}</li>
			@endforeach
		</ul>	

		 {!! Form::open(array('url'=>'maquinctrl/save?return='.$return, 'class'=>'form-horizontal','files' => true , 'parsley-validate'=>'','novalidate'=>' ')) !!}
<div class="col-md-12">
						<fieldset><legend> Maquinarias</legend>
				
									  <div class="form-group  " >
										<label for="Serial Number" class=" control-label col-md-4 text-left"> Serial Number </label>
										<div class="col-md-6">
										  {!! Form::text('num_serie', $row['num_serie'],array('class'=>'form-control', 'placeholder'=>'',   )) !!}
										 </div>
										 <div class="col-md-2">
										 	
										 </div>
									  </div> 
									  <div class="form-group  " >
										<label for="Name" class=" control-label col-md-4 text-left"> Name </label>
										<div class="col-md-6">
										  <textarea name='Nombre' rows='5' id='Nombre' class='form-control '
				           >{{ $row['Nombre'] }}</textarea>
										 </div>
										 <div class="col-md-2">
										 	
										 </div>
									  </div> 
									  <div class="form-group  " >
										<label for="Model" class=" control-label col-md-4 text-left"> Model </label>
										<div class="col-md-6">
										  {!! Form::text('modelo', $row['modelo'],array('class'=>'form-control', 'placeholder'=>'',   )) !!}
										 </div>
										 <div class="col-md-2">
										 	
										 </div>
									  </div> 
									  <div class="form-group  " >
										<label for="Year" class=" control-label col-md-4 text-left"> Year </label>
										<div class="col-md-6">
										  {!! Form::text('Año', $row['Año'],array('class'=>'form-control', 'placeholder'=>'',   )) !!}
										 </div>
										 <div class="col-md-2">
										 	
										 </div>
									  </div> 
									  <div class="form-group  " >
										<label for="Machinery Type" class=" control-label col-md-4 text-left"> Machinery Type </label>
										<div class="col-md-6">
										  <select name='tipo_maq' rows='5' id='tipo_maq' class='select2 '   ></select>
										 </div>
										 <div class="col-md-2">
										 	
										 </div>
									  </div> 
									  <div class="form-group  " >
										<label for="Status" class=" control-label col-md-4 text-left"> Status </label>
										<div class="col-md-6">
										  
					<?php $idEstatus = explode(',',$row['idEstatus']);
					$idEstatus_opt = array( '1' => 'Activo' ,  '2' => 'Inactivo' , ); ?>
					<select name='idEstatus' rows='5'   class='select2 '  > 
						<?php
						foreach($idEstatus_opt as $key=>$val)
						{
							echo "<option  value ='$key' ".($row['idEstatus'] == $key ? " selected='selected' " : '' ).">$val</option>";
						}
						?></select>
										 </div>
										 <div class="col-md-2">
										 	
										 </div>
									  </div> </fieldset>
			</div>

			

		
			<div style="clear:both"></div>	
				
					
				  <div class="form-group">
					<label class="col-sm-4 text-right">&nbsp;</label>
					<div class="col-sm-8">	
					<button type="submit" name="apply" class="btn btn-info btn-sm" ><i class="icon-checkmark-circle2"></i> {{ Lang::get('core.sb_apply') }}</button>
					<button type="submit" name="submit" class="btn btn-primary btn-sm" ><i class="icon-bubble-check"></i> {{ Lang::get('core.sb_save') }}</button>
					<button type="button" onclick="location.href='{{ URL::to('maquinctrl?return='.$return) }}' " class="btn btn-warning btn-sm "><i class="icon-cancel-circle2 "></i>  {{ Lang::get('core.sb_cancel') }} </button>
					</div>	  
			
				  </div>
		 
		 {!! Form::close() !!}
	</div>
</div>		 
</div>	
</div>			 
   <script type="text/javascript">
	$(document).ready(function() { 
		
		
		$("#tipo_maq").jCombo("{!! url('maquinctrl/comboselect?filter=tbl_maquinaria_tipo:id_maq_tpo:tipo_maquinaria') !!}",
		{  selected_value : '{{ $row["tipo_maq"] }}' });
		 
		
		$('.removeMultiFiles').on('click',function(){
			var removeUrl = '{{ url("maquinctrl/removefiles?file=")}}'+$(this).attr('url');
			$(this).parent().remove();
			$.get(removeUrl,function(response){});
			$(this).parent('div').empty();	
			return false;
		});		
		
	});
	</script>		 
@stop