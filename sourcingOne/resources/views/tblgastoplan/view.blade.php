@if($setting['view-method'] =='native')
<div class="sbox">
	<div class="sbox-title">  
		<h4> <i class="fa fa-table"></i> <?php echo $pageTitle ;?> <small>{{ $pageNote }}</small>
			<a href="javascript:void(0)" class="collapse-close pull-right btn btn-xs btn-danger" onclick="ajaxViewClose('#{{ $pageModule }}')">
			<i class="fa fa fa-times"></i></a>
		</h4>
	 </div>

	<div class="sbox-content"> 
@endif	

		<table class="table table-striped table-bordered" >
			<tbody>	
				
					<tr>
						<td width='30%' class='label-view text-right'>{{ SiteHelpers::activeLang('Financieroand Id', (isset($fields['financieroand_id']['language'])? $fields['financieroand_id']['language'] : array())) }}</td>
						<td>{{ $row->financieroand_id}} </td>
						
					</tr>
				
					<tr>
						<td width='30%' class='label-view text-right'>{{ SiteHelpers::activeLang('Contrato Id', (isset($fields['contrato_id']['language'])? $fields['contrato_id']['language'] : array())) }}</td>
						<td>{{ $row->contrato_id}} </td>
						
					</tr>
				
					<tr>
						<td width='30%' class='label-view text-right'>{{ SiteHelpers::activeLang('Financieroand Fecha', (isset($fields['financieroand_fecha']['language'])? $fields['financieroand_fecha']['language'] : array())) }}</td>
						<td>{{ $row->financieroand_fecha}} </td>
						
					</tr>
				
					<tr>
						<td width='30%' class='label-view text-right'>{{ SiteHelpers::activeLang('Financieroand Mtoreal', (isset($fields['financieroand_mtoreal']['language'])? $fields['financieroand_mtoreal']['language'] : array())) }}</td>
						<td>{{ $row->financieroand_mtoreal}} </td>
						
					</tr>
				
					<tr>
						<td width='30%' class='label-view text-right'>{{ SiteHelpers::activeLang('Financieroand Mtoplan', (isset($fields['financieroand_mtoplan']['language'])? $fields['financieroand_mtoplan']['language'] : array())) }}</td>
						<td>{{ $row->financieroand_mtoplan}} </td>
						
					</tr>
				
			</tbody>	
		</table>  
			
		 	

@if($setting['form-method'] =='native')
	</div>	
</div>	
@endif	

<script>
$(document).ready(function(){

});
</script>	