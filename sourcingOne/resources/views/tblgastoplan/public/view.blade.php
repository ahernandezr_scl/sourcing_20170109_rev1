<div class="m-t" style="padding-top:25px;">	
    <div class="row m-b-lg animated fadeInDown delayp1 text-center">
        <h3> {{ $pageTitle }} <small> {{ $pageNote }} </small></h3>
        <hr />       
    </div>
</div>
<div class="m-t">
	<div class="table-responsive" > 	

		<table class="table table-striped table-bordered" >
			<tbody>	
		
			
					<tr>
						<td width='30%' class='label-view text-right'>{{ SiteHelpers::activeLang('Financieroand Id', (isset($fields['financieroand_id']['language'])? $fields['financieroand_id']['language'] : array())) }}</td>
						<td>{{ $row->financieroand_id}} </td>
						
					</tr>
				
					<tr>
						<td width='30%' class='label-view text-right'>{{ SiteHelpers::activeLang('Contrato Id', (isset($fields['contrato_id']['language'])? $fields['contrato_id']['language'] : array())) }}</td>
						<td>{{ $row->contrato_id}} </td>
						
					</tr>
				
					<tr>
						<td width='30%' class='label-view text-right'>{{ SiteHelpers::activeLang('Financieroand Fecha', (isset($fields['financieroand_fecha']['language'])? $fields['financieroand_fecha']['language'] : array())) }}</td>
						<td>{{ $row->financieroand_fecha}} </td>
						
					</tr>
				
					<tr>
						<td width='30%' class='label-view text-right'>{{ SiteHelpers::activeLang('Financieroand Mtoreal', (isset($fields['financieroand_mtoreal']['language'])? $fields['financieroand_mtoreal']['language'] : array())) }}</td>
						<td>{{ $row->financieroand_mtoreal}} </td>
						
					</tr>
				
					<tr>
						<td width='30%' class='label-view text-right'>{{ SiteHelpers::activeLang('Financieroand Mtoplan', (isset($fields['financieroand_mtoplan']['language'])? $fields['financieroand_mtoplan']['language'] : array())) }}</td>
						<td>{{ $row->financieroand_mtoplan}} </td>
						
					</tr>
						
					<tr>
						<td width='30%' class='label-view text-right'></td>
						<td> <a href="javascript:history.go(-1)" class="btn btn-primary"> Back To Grid <a> </td>
						
					</tr>					
				
			</tbody>	
		</table>   

	 
	
	</div>
</div>	