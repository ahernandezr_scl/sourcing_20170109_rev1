

		 {!! Form::open(array('url'=>'tblgastoplan/savepublic', 'class'=>'form-horizontal','files' => true , 'parsley-validate'=>'','novalidate'=>' ')) !!}

	@if(Session::has('messagetext'))
	  
		   {!! Session::get('messagetext') !!}
	   
	@endif
	<ul class="parsley-error-list">
		@foreach($errors->all() as $error)
			<li>{{ $error }}</li>
		@endforeach
	</ul>		


<div class="col-md-12">
						<fieldset><legend> Gasto y Plan</legend>
									
									  <div class="form-group  " >
										<label for="Financieroand Id" class=" control-label col-md-4 text-left"> Financieroand Id </label>
										<div class="col-md-6">
										  {!! Form::text('financieroand_id', $row['financieroand_id'],array('class'=>'form-control', 'placeholder'=>'',   )) !!} 
										 </div> 
										 <div class="col-md-2">
										 	
										 </div>
									  </div> 					
									  <div class="form-group  " >
										<label for="Contrato Id" class=" control-label col-md-4 text-left"> Contrato Id </label>
										<div class="col-md-6">
										  {!! Form::text('contrato_id', $row['contrato_id'],array('class'=>'form-control', 'placeholder'=>'',   )) !!} 
										 </div> 
										 <div class="col-md-2">
										 	
										 </div>
									  </div> 					
									  <div class="form-group  " >
										<label for="Financieroand Fecha" class=" control-label col-md-4 text-left"> Financieroand Fecha </label>
										<div class="col-md-6">
										  {!! Form::text('financieroand_fecha', $row['financieroand_fecha'],array('class'=>'form-control', 'placeholder'=>'',   )) !!} 
										 </div> 
										 <div class="col-md-2">
										 	
										 </div>
									  </div> 					
									  <div class="form-group  " >
										<label for="Financieroand Mtoreal" class=" control-label col-md-4 text-left"> Financieroand Mtoreal </label>
										<div class="col-md-6">
										  {!! Form::text('financieroand_mtoreal', $row['financieroand_mtoreal'],array('class'=>'form-control', 'placeholder'=>'',   )) !!} 
										 </div> 
										 <div class="col-md-2">
										 	
										 </div>
									  </div> 					
									  <div class="form-group  " >
										<label for="Financieroand Mtoplan" class=" control-label col-md-4 text-left"> Financieroand Mtoplan </label>
										<div class="col-md-6">
										  {!! Form::text('financieroand_mtoplan', $row['financieroand_mtoplan'],array('class'=>'form-control', 'placeholder'=>'',   )) !!} 
										 </div> 
										 <div class="col-md-2">
										 	
										 </div>
									  </div> </fieldset>
			</div>
			
			

			<div style="clear:both"></div>	
				
					
				  <div class="form-group">
					<label class="col-sm-4 text-right">&nbsp;</label>
					<div class="col-sm-8">	
					<button type="submit" name="apply" class="btn btn-info btn-sm" ><i class="fa  fa-check-circle"></i> {{ Lang::get('core.sb_apply') }}</button>
					<button type="submit" name="submit" class="btn btn-primary btn-sm" ><i class="fa  fa-save "></i> {{ Lang::get('core.sb_save') }}</button>
				  </div>	  
			
		</div> 
		 
		 {!! Form::close() !!}
		 
   <script type="text/javascript">
	$(document).ready(function() { 
		
		 

		$('.removeCurrentFiles').on('click',function(){
			var removeUrl = $(this).attr('href');
			$.get(removeUrl,function(response){});
			$(this).parent('div').empty();	
			return false;
		});		
		
	});
	</script>		 
