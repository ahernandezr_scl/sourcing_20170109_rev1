<div class="m-t" style="padding-top:25px;">	
    <div class="row m-b-lg animated fadeInDown delayp1 text-center">
        <h3> {{ $pageTitle }} <small> {{ $pageNote }} </small></h3>
        <hr />       
    </div>
</div>
<div class="m-t">
	<div class="table-responsive" > 	

		<table class="table table-striped table-bordered" >
			<tbody>	
		
			
					<tr>
						<td width='30%' class='label-view text-right'>{{ SiteHelpers::activeLang('Contract', (isset($fields['contrato_id']['language'])? $fields['contrato_id']['language'] : array())) }}</td>
						<td>{{ SiteHelpers::formatLookUp($row->contrato_id,'contrato_id','1:tbl_contrato:contrato_id:cont_numero|cont_proveedor') }} </td>
						
					</tr>
				
					<tr>
						<td width='30%' class='label-view text-right'>{{ SiteHelpers::activeLang('KPI Name', (isset($fields['kpi_nombreIndicador']['language'])? $fields['kpi_nombreIndicador']['language'] : array())) }}</td>
						<td>{{ $row->kpi_nombreIndicador}} </td>
						
					</tr>
				
					<tr>
						<td width='30%' class='label-view text-right'>{{ SiteHelpers::activeLang('KPI formula description', (isset($fields['kpi_descripcionFormula']['language'])? $fields['kpi_descripcionFormula']['language'] : array())) }}</td>
						<td>{{ $row->kpi_descripcionFormula}} </td>
						
					</tr>
				
					<tr>
						<td width='30%' class='label-view text-right'>{{ SiteHelpers::activeLang('Unit of measurement', (isset($fields['kpi_unidadMedida']['language'])? $fields['kpi_unidadMedida']['language'] : array())) }}</td>
						<td>{{ $row->kpi_unidadMedida}} </td>
						
					</tr>
				
					<tr>
						<td width='30%' class='label-view text-right'>{{ SiteHelpers::activeLang('Periodicity', (isset($fields['kpi_periodicidad']['language'])? $fields['kpi_periodicidad']['language'] : array())) }}</td>
						<td>{{ $row->kpi_periodicidad}} </td>
						
					</tr>
						
					<tr>
						<td width='30%' class='label-view text-right'></td>
						<td> <a href="javascript:history.go(-1)" class="btn btn-primary"> Back To Grid <a> </td>
						
					</tr>					
				
			</tbody>	
		</table>   

	 
	
	</div>
</div>	