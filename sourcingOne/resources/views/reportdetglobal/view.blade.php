<?php
$servername = "localhost";
$username = "root";
$password = "8044302";
$year;
$mes;$reg;

#try {
    $conn2 = new PDO("mysql:host=$servername;dbname=andina_koadmin", $username, $password);
    // set the PDO error mode to exception
#    $conn2->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
#   echo "Connected successfully";
#    }
#catch(PDOException $e)
#    {
#    echo "Connection failed: " . $e->getMessage();
#    }


?>

<?php  
  if (isset($_GET["year"])) {
    $year = $_GET["year"];
   }
  else{  
    $year = date("Y");
}

  if (isset($_GET["mes"])) {
    $mes = $_GET["mes"];
   }
  else{  
    $mes = 0;
}

 if (isset($_GET["reg"])) {
    $reg = $_GET["reg"];
   }
  else{  
    $reg = 0;
}

 if (isset($_GET["seg"])) {
    $seg = $_GET["seg"];
   }
  else{  
    $seg = 0;
}

 if (isset($_GET["area"])) {
    $area = $_GET["area"];
   }
  else{  
    $area = 0;
}

 if (isset($_GET["proc"])) {
    $proc = $_GET["proc"];
   }
  else{  
    $proc = 0;
}

 if (isset($_GET["ind"])) {
    $ind = $_GET["ind"];
   }
  else{  
    $ind = 0;
}

 if (isset($_GET["tip"])) {
    $tip = $_GET["tip"];
   }
  else{  
    $tip = 0;
}
?>


  
<?php 
if($proc == 0){
     
  
  if($ind =="adm"){
    $sql_bub='SELECT  num, cont_nombre sigla, avg(resultado) valor, CAST(SUM(financieroand_mtoreal) AS DECIMAL(20,0))  dinero, cont_proveedor razon_soc,a.contrato_id id,
 \'Auto Evaluación Calidad Administración de Contrato\' titulo, 75 lim, 0 lim2,0 t_graf, 0 tp_med

FROM
tbl_calidad_adm_cont a INNER JOIN (SELECT  @i:=@i+1 AS num, b.* FROM tbl_contrato b,
    (SELECT @i:=0) c ) b ON a.contrato_id = b.contrato_id
INNER JOIN dim_tiempo d ON a.fec_rev = d.fecha
INNER JOIN tbl_financiero_and c ON a.contrato_id = c.contrato_id and c.financieroand_fecha = d.fecha
WHERE resultado IS NOT NULL
AND 
case when  '.$mes.'=0 then Anio = '.$year.' else

trimestre  = (SELECT DISTINCT trimestre FROM dim_tiempo
WHERE mes= '.$mes.' AND Anio = '.$year.') AND Anio = '.$year.'
end 

and case when '.$area.' = 0 then 1=1 else afuncional_id = '.$area.' end
and case when '.$reg.' = 0 then 1=1 else geo_id = '.$reg.' end
and case when '.$seg.' = 0 then 1=1 else segmento_id = '.$seg.' end
group by 1,2,5,6';

    $sql_bul='SELECT CAST(AVG(resultado) AS DECIMAL(4,1)) puntaje ,0 a,75 b, 75 c, 75 d, 75 e, 100 f, \'Calidad Adm de Contrato\' titulo, 0 tp_gr, 0 med, 1 amb

FROM tbl_calidad_adm_cont a INNER JOIN dim_tiempo b 
ON fec_rev = fecha
inner join tbl_contrato c on c.contrato_id = a.contrato_id
WHERE resultado IS NOT NULL
AND 
case when '.$mes.'=0 then Anio = '.$year.' else
trimestre  = (SELECT DISTINCT trimestre FROM dim_tiempo WHERE mes= '.$mes.' AND Anio = '.$year.') AND Anio = '.$year.'
end
and case when '.$area.' = 0 then 1=1 else afuncional_id = '.$area.' end
and case when '.$reg.' = 0 then 1=1 else geo_id = '.$reg.' end
and case when '.$seg.' = 0 then 1=1 else segmento_id = '.$seg.' end';

    $sql_lin='SELECT \'Real\' serie,NTrimestre fecha, CAST(AVG(resultado) AS DECIMAL(4,1)) valor, 

 \'Auto Evaluación Calidad Administración de Contrato\' titulo, \'Promedio resultado respuestas encuesta Trimestral\' sub, 0 med
FROM tbl_calidad_adm_cont a INNER JOIN dim_tiempo 
ON fec_rev = fecha
inner join tbl_contrato c on c.contrato_id = a.contrato_id
WHERE resultado IS NOT NULL
and case when '.$year.'!=\'\' then Anio = '.$year.' else 1=1 end
and case when '.$area.' = 0 then 1=1 else afuncional_id = '.$area.' end
and case when '.$reg.' = 0 then 1=1 else geo_id = '.$reg.' end
and case when '.$seg.' = 0 then 1=1 else segmento_id = '.$seg.' end
GROUP BY 1,2

UNION ALL

SELECT DISTINCT \'Meta\' Serie, NTrimestre, CASE WHEN Trimestre IN (1,2) THEN 60 ELSE 70 END meta , 
 \'Auto Evaluaciòn Calidad Administración de Contrato\' titulo, \'Promedio resultado respuestas encuesta Trimestral\' sub  , 0 med

FROM dim_tiempo 

WHERE anio = '.$year.' AND dia = 1 AND mes >= (SELECT MIN(MONTH(fec_rev)) FROM tbl_calidad_adm_cont WHERE YEAR(fec_rev) = '.$year.')';
  } 
    
  else if($ind =="kpi"){

    $sql_bub ='
        SELECT num, cont_nombre sigla, CAST(AVG(kpi) AS DECIMAL(4,1)) valor,
         CAST(SUM(financieroand_mtoreal) AS DECIMAL(20,0)) dinero, cont_proveedor razon_soc,contrato_id id,  \'Cumplimiento KPI por empresa\' titulo, 
        75 lim, 0 lim2,0 t_graf, 0 tp_med


        FROM

        (SELECT num, b.contrato_id,kpiDet_fecha,financieroand_mtoreal, cont_proveedor,cont_nombre, 

        (SUM(
          CASE WHEN kpi_tipo_calc = 1 THEN
            CASE WHEN kpiDet_puntaje >= kpiDet_meta THEN 1 ELSE 0 END
               WHEN kpi_tipo_calc = 2 THEN
            CASE WHEN kpiDet_puntaje <= kpiDet_meta THEN 1 ELSE 0 END
               WHEN kpi_tipo_calc = 3 THEN
            CASE WHEN kpiDet_puntaje BETWEEN kpiDet_min AND kpiDet_max THEN 1 ELSE 0 END
               WHEN kpi_tipo_calc = 4 THEN 1
              
          END )/
        COUNT(DISTINCT a.id_kpi) )*100 kpi
        FROM tbl_kpimensual a INNER JOIN tbl_kpigral b
        ON a.id_kpi = b.id_kpi
        INNER JOIN (SELECT  @i:=@i+1 AS num, b.* FROM tbl_contrato b,
    (SELECT @i:=0) c ) c ON b.contrato_id = c.contrato_id
        INNER JOIN tbl_financiero_and d ON b.contrato_id = d.contrato_id and d.financieroand_fecha = a.kpiDet_fecha
        INNER JOIN dim_tiempo ON kpiDet_fecha = fecha
        WHERE 
         kpiDet_puntaje IS NOT NULL
         AND
        CASE WHEN  '.$mes.'=0 THEN Anio = '.$year.' ELSE
         mes= '.$mes.' AND Anio = '.$year.'
        end
        and case when '.$area.' = 0 then 1=1 else afuncional_id = '.$area.' end
        and case when '.$reg.' = 0 then 1=1 else geo_id = '.$reg.' end
        and case when '.$seg.' = 0 then 1=1 else segmento_id = '.$seg.' end

        GROUP BY 1,2,3,4,5,6) asd
        GROUP BY 1,2,5,6';

   $sql_bul='SELECT CAST(AVG(kpi) AS DECIMAL(4,1)) puntaje,0 a,75 b, 75 c, 75 d, 75 e, 100 f,  \'KPIs\' titulo, 0 tp_gr, 0 med, 1 amb

      FROM
      (SELECT  b.contrato_id,kpiDet_fecha,  

      (SUM(
        CASE WHEN kpi_tipo_calc = 1 THEN
          CASE WHEN kpiDet_puntaje >= kpiDet_meta THEN 1 ELSE 0 END
             WHEN kpi_tipo_calc = 2 THEN
          CASE WHEN kpiDet_puntaje <= kpiDet_meta THEN 1 ELSE 0 END
             WHEN kpi_tipo_calc = 3 THEN
          CASE WHEN kpiDet_puntaje BETWEEN kpiDet_min AND kpiDet_max THEN 1 ELSE 0 END
             WHEN kpi_tipo_calc = 4 THEN 1
            
        END )/
      COUNT(DISTINCT a.id_kpi) )*100 kpi
      FROM tbl_kpimensual a INNER JOIN tbl_kpigral b
      ON a.id_kpi = b.id_kpi
      INNER JOIN dim_tiempo ON kpiDet_fecha = fecha
      inner join tbl_contrato c on c.contrato_id = b.contrato_id
      WHERE kpiDet_puntaje IS NOT NULL
      AND 
      case when '.$mes.'=0 then Anio = '.$year.' else
       mes= '.$mes.' AND Anio = '.$year.'
      end
      and case when '.$area.' = 0 then 1=1 else afuncional_id = '.$area.' end
      and case when '.$reg.' = 0 then 1=1 else geo_id = '.$reg.' end
      and case when '.$seg.' = 0 then 1=1 else segmento_id = '.$seg.' end

      GROUP BY 1,2) asd';
    $sql_lin='
        SELECT \'Real\' serie, fec fecha, cast(AVG(kpi) as DECIMAL(4,1)) valor, 
         \'Promedio Cumplimiento KPI empresas\' titulo, \'(Cumplimiento KPI: KPI Positivos respecto KPIs Totales)\' sub, 0 med
        FROM
        (SELECT  b.contrato_id,Fecha_mes fec,  

        (SUM(
          CASE WHEN kpi_tipo_calc = 1 THEN
            CASE WHEN kpiDet_puntaje >= kpiDet_meta THEN 1 ELSE 0 END
               WHEN kpi_tipo_calc = 2 THEN
            CASE WHEN kpiDet_puntaje <= kpiDet_meta THEN 1 ELSE 0 END
               WHEN kpi_tipo_calc = 3 THEN
            CASE WHEN kpiDet_puntaje BETWEEN kpiDet_min AND kpiDet_max THEN 1 ELSE 0 END
               WHEN kpi_tipo_calc = 4 THEN 1
              
          END )/
        COUNT(DISTINCT a.id_kpi) )*100 kpi
        FROM tbl_kpimensual a INNER JOIN tbl_kpigral b
        ON a.id_kpi = b.id_kpi
        INNER JOIN dim_tiempo ON kpiDet_fecha = fecha
        inner join tbl_contrato c on c.contrato_id = b.contrato_id
        WHERE kpiDet_puntaje IS NOT NULL
        AND CASE WHEN '.$year.'!=\'\' THEN Anio = '.$year.' ELSE 1=1 END
        and case when '.$area.' = 0 then 1=1 else afuncional_id = '.$area.' end
        and case when '.$reg.' = 0 then 1=1 else geo_id = '.$reg.' end
        and case when '.$seg.' = 0 then 1=1 else segmento_id = '.$seg.' end

        GROUP BY 1,2) asd
        GROUP BY 1,2

        UNION ALL

        SELECT DISTINCT \'Meta\' Serie,concat(concat(NMes3L,\'-\'),anio), 
        CASE WHEN mes BETWEEN 1 AND 3 THEN 50
        WHEN mes BETWEEN 4 AND 6 THEN 60
        WHEN mes BETWEEN 7 AND 9 THEN 65
        WHEN mes BETWEEN 10 AND 12 THEN 75
          END meta,

         \'Promedio Cumplimiento KPI empresas\' titulo, \'(Cumplimiento KPI: KPI Positivos respecto KPIs Totales)\' sub, 0 med

        FROM dim_tiempo WHERE anio = '.$year.' AND dia = 1 AND mes >= (SELECT MIN(MONTH(kpiDet_fecha)) FROM tbl_kpimensual WHERE YEAR(kpiDet_fecha) = '.$year.')



        ';
  } 
   
  else if($ind =="dym") 
   {
    $sql_bub = 'SELECT  num, cont_nombre sigla, 
    cast(avg(per_moros*100) as DECIMAL(4,1)) valor,
    CAST(SUM(financieroand_mtoreal) AS DECIMAL(20,0)) dinero, 
    cont_proveedor razon_soc,
        a.contrato_id id, 
         \'Dispersión Deuda y Morosidad Financiera\' titulo, 30 lim, 
     0 lim2,1 t_graf, 0 tp_med
     
        FROM
        tbl_morosidad a INNER JOIN (SELECT  @i:=@i+1 AS num, b.* FROM tbl_contrato b,
    (SELECT @i:=0) c ) b ON a.contrato_id = b.contrato_id
        INNER JOIN tbl_financiero_and c ON a.contrato_id = c.contrato_id and c.financieroand_fecha = a.fecha_eval
        INNER JOIN dim_tiempo d ON a.fecha_Eval = d.fecha
        WHERE
        case when  '.$mes.'=0 then Anio = '.$year.' else
        mes = '.$mes.' AND Anio = '.$year.'

        end 
        and case when '.$area.' = 0 then 1=1 else afuncional_id = '.$area.' end
        and case when '.$reg.' = 0 then 1=1 else geo_id = '.$reg.' end
        and case when '.$seg.' = 0 then 1=1 else segmento_id = '.$seg.' end
        group by 1,2,5,6';

    $sql_bul = 'SELECT cast(AVG(per_moros*100) as DECIMAL(4,1))  puntaje,0 a,30 b, 0 c, 0 d, 30 e, 100 f,   \'Deuda y Morosidad\' titulo, 1 tp_gr, 0 med, 2 amb


      FROM tbl_morosidad b
      INNER JOIN dim_tiempo d ON fecha_Eval = d.fecha
      inner join tbl_contrato c on c.contrato_id = b.contrato_id
      WHERE 
      case when  '.$mes.'=0 then Anio = '.$year.' else
      mes= '.$mes.' AND Anio = '.$year.'

      end 
      and case when '.$area.' = 0 then 1=1 else afuncional_id = '.$area.' end
      and case when '.$reg.' = 0 then 1=1 else geo_id = '.$reg.' end
      and case when '.$seg.' = 0 then 1=1 else segmento_id = '.$seg.' end';

    $sql_lin =' 
          SELECT \'Real\' serie,fecha_mes fecha,CAST(AVG(per_moros)*100 AS DECIMAL(4,1)) valor, 
         \'Predictor Empresarial Promedio Contratista\' titulo, \'(promedio de resultados por empresa según predictor Equifax)\' sub, 0 med

        FROM tbl_morosidad a
        INNER JOIN dim_tiempo b ON fecha_Eval = fecha
        inner join tbl_contrato c on c.contrato_id = a.contrato_id
        where 
         Anio = '.$year.'
        and case when '.$area.' = 0 then 1=1 else afuncional_id = '.$area.' end
        and case when '.$reg.' = 0 then 1=1 else geo_id = '.$reg.' end
        and case when '.$seg.' = 0 then 1=1 else segmento_id = '.$seg.' end
        GROUP BY 1,2

        UNION ALL

        SELECT DISTINCT \'Meta\' Serie, concat(concat(NMes3L,\'-\'),anio), 30  
        , \'Predictor Empresarial Promedio Contratista\' titulo, \'(promedio de resultados por empresa según predictor Equifax)\' sub, 0 med
        FROM dim_tiempo
        WHERE anio = '.$year.' AND dia = 1 and mes >= (select min(month(fecha_Eval)) from tbl_morosidad where year(fecha_Eval) = '.$year.')';
  } 

  else if($ind =="esf") 
   {
    $sql_bub ='
                SELECT  num, cont_nombre sigla, valor, CAST(SUM(financieroand_mtoreal) AS DECIMAL(20,0)) dinero, cont_proveedor razon_soc,a.contrato_id id,

                 \'Dispersión Evaluación Estado Financiero\' titulo, 22.5 lim, 0 lim2,1 t_graf, 0 tp_med
                FROM
                tbl_eval_financiera a INNER JOIN (SELECT  @i:=@i+1 AS num, b.* FROM tbl_contrato b,
    (SELECT @i:=0) c ) b ON a.contrato_id = b.contrato_id
                INNER JOIN tbl_financiero_and c ON a.contrato_id = c.contrato_id 
                INNER JOIN dim_tiempo d ON a.fecha_validez = d.fecha

                where 

                CASE WHEN  '.$mes.'=0 THEN Anio = '.$year.' ELSE
                mes= '.$mes.' AND Anio = '.$year.'

                END 
                AND case when '.$area.' = 0 then 1=1 else afuncional_id = '.$area.' end
                and case when '.$reg.' = 0 then 1=1 else geo_id = '.$reg.' end
                and case when '.$seg.' = 0 then 1=1 else segmento_id = '.$seg.' end

                GROUP BY 1,2,5,6';
    $sql_bul='SELECT cast(AVG(valor) as decimal(4,1)) puntaje ,0 a,22.5 b, 22.5 c, 22.5 d, 22.5 e, 100 f, \'Evaluación Estado Financiero\' titulo, 1 tp_gr, 0 med, 2 amb
              FROM tbl_eval_financiera a
              inner join tbl_contrato c on c.contrato_id = a.contrato_id
              inner join dim_tiempo b on a.fecha_validez = b.fecha
              where 
              case when  '.$mes.'=0 then Anio = '.$year.' else
              mes= '.$mes.' AND Anio = '.$year.'

              end 
              and case when '.$area.' = 0 then 1=1 else afuncional_id = '.$area.' end
              and case when '.$reg.' = 0 then 1=1 else geo_id = '.$reg.' end
              and case when '.$seg.' = 0 then 1=1 else segmento_id = '.$seg.' end';
    $sql_lin='SELECT \'Real\' serie, anio fecha,cast(AVG(valor) as DECIMAL(4,1)) valor
              , \'Promedio probabilidad de riesgo financiero\' titulo, \'(Promedio resultados por empresa)\' sub, 0 med

              FROM tbl_eval_financiera a
              inner join tbl_contrato b on a.contrato_id = b.contrato_id
              inner join dim_tiempo c on a.fecha_validez = c.fecha
              where 
              Anio between year(current_date)-3 and year(current_date)
              and case when '.$area.' = 0 then 1=1 else afuncional_id = '.$area.' end
              and case when '.$reg.' = 0 then 1=1 else geo_id = '.$reg.' end
              and case when '.$seg.' = 0 then 1=1 else segmento_id = '.$seg.' end
              group by 1,2

              UNION ALL

              SELECT DISTINCT \'Meta\' Serie, anio, 50
              , \'Promedio probabilidad de riesgo financiero\' titulo, \'(Promedio resultados por empresa)\' sub, 0 med  FROM 
              dim_tiempo

              WHERE Anio BETWEEN YEAR(CURRENT_DATE)-3 AND YEAR(CURRENT_DATE)
              AND Anio >= (SELECT MIN(YEAR(fecha_validez)) FROM tbl_eval_financiera)';
  }
  else if($ind =="evp") 
  {
    $sql_bub ='
              SELECT  num, cont_nombre sigla, AVG(eval_puntaje) valor, CAST(SUM(financieroand_mtoreal) AS DECIMAL(20,0)) dinero,
               cont_proveedor razon_soc,a.contrato_id id
              , \'Dispersión Evaluación Proveedor\' titulo, 70 lim, 50 lim2,0 t_graf, 0 tp_med
              FROM
              tbl_evalcontratistas a INNER JOIN (SELECT  @i:=@i+1 AS num, b.* FROM tbl_contrato b,
    (SELECT @i:=0) c ) b ON a.contrato_id = b.contrato_id
              INNER JOIN tbl_financiero_and c ON a.contrato_id = c.contrato_id and c.financieroand_fecha = a.eval_fecha
              INNER JOIN dim_tiempo 
              ON eval_fecha = fecha
              WHERE eval_puntaje IS NOT NULL
              AND  

              CASE WHEN  '.$mes.'=0 THEN Anio = '.$year.' ELSE
              mes IN (SELECT DISTINCT mes FROM dim_tiempo 
              WHERE NSemestre  = (SELECT DISTINCT NSemestre FROM dim_tiempo
              WHERE mes= '.$mes.' AND Anio = '.$year.'))
              end
              and case when '.$area.' = 0 then 1=1 else afuncional_id = '.$area.' end
              and case when '.$reg.' = 0 then 1=1 else geo_id = '.$reg.' end
              and case when '.$seg.' = 0 then 1=1 else segmento_id = '.$seg.' end

              GROUP BY 1,2,5,6';
    $sql_bul = 'Select cast( AVG(eval_puntaje) as decimal(4,1)) puntaje ,0 a,50 b, 50 c, 70 d, 70 e, 100 f, \'Evaluación de Proveedor\' titulo, 0 tp_gr, 0 med, 1 amb

            FROM tbl_evalcontratistas a INNER JOIN dim_tiempo 
            ON eval_fecha = fecha 
            INNER JOIN tbl_contrato b ON a.contrato_id = b.contrato_id
            where  
            eval_puntaje IS NOT NULL
            AND 
            case when '.$mes.'=0 then Anio = '.$year.' else
            semestre  = (SELECT DISTINCT semestre FROM dim_tiempo WHERE mes= '.$mes.' AND Anio = '.$year.') AND Anio = '.$year.'
            end
            and case when '.$area.' = 0 then 1=1 else afuncional_id = '.$area.' end
            and case when '.$reg.' = 0 then 1=1 else geo_id = '.$reg.' end
            and case when '.$seg.' = 0 then 1=1 else segmento_id = '.$seg.' end';
    $sql_lin = 'SELECT \'Real\' serie,NSemestre fecha, CAST(AVG(eval_puntaje) AS DECIMAL(4,1)) valor, 0 med
                , \'Evaluación Proveedor\' titulo, \'Promedio evaluación de empresas\' sub

                FROM tbl_evalcontratistas a INNER JOIN dim_tiempo 
                ON eval_fecha = fecha
                INNER JOIN tbl_contrato b ON a.contrato_id = b.contrato_id
                WHERE eval_puntaje IS NOT NULL
                AND Anio = '.$year.' 
                AND CASE WHEN '.$area.' = 0 THEN 1=1 ELSE afuncional_id = '.$area.' END
                AND CASE WHEN '.$reg.' = 0 THEN 1=1 ELSE geo_id = '.$reg.' END
                AND CASE WHEN '.$seg.' = 0 THEN 1=1 ELSE segmento_id = '.$seg.' END

                GROUP BY 1,2

                UNION ALL

                SELECT DISTINCT \'Meta\' Serie, NSemestre, 70 o , 0 med
                , \'Evaluación Proveedor\' titulo, \'Promedio evaluación de empresas\' sub
                 FROM dim_tiempo

                WHERE anio = '.$year.' AND dia = 1 AND mes >= (SELECT MIN(MONTH(eval_fecha)) FROM tbl_evalcontratistas WHERE YEAR(eval_fecha) = '.$year.')';
  } 
  else if($ind =="fct") 
  {
    $sql_bub = 'SELECT  num, cont_nombre sigla, CAST(AVG((PERC_COND + perc_equip)/2) AS DECIMAL(4,1)) valor, CAST(SUM(financieroand_mtoreal) AS DECIMAL(20,0)) dinero,
           cont_proveedor razon_soc,a.contrato_id id 
          ,\'Dispersión Fiscalización Condiciones de Trabajo\' titulo,  75 lim, 0 lim2,0 t_graf, 0 tp_med

          FROM
          tbl_fiscalización a INNER JOIN (SELECT  @i:=@i+1 AS num, b.* FROM tbl_contrato b,
    (SELECT @i:=0) c ) b ON a.contrato_id = b.contrato_id
          INNER JOIN tbl_financiero_and c ON a.contrato_id = c.contrato_id and c.financieroand_fecha = a.fecha
          INNER JOIN dim_tiempo d ON a.fecha = d.fecha
          WHERE
          CASE WHEN  '.$mes.'=0 THEN Anio = '.$year.' ELSE
          mes = '.$mes.' AND Anio = '.$year.'

          END 
          and case when '.$area.' = 0 then 1=1 else afuncional_id = '.$area.' end
          and case when '.$reg.' = 0 then 1=1 else geo_id = '.$reg.' end
          and case when '.$seg.' = 0 then 1=1 else segmento_id = '.$seg.' end
          GROUP BY 1,2,5,6';
    $sql_bul = 'SELECT CAST(AVG((PERC_COND + perc_equip)/2) AS DECIMAL(4,1)) puntaje
              ,0 a,75 b, 75 c, 75 d, 75 e,100 f, \'Fiscalización Condiciones de Trabajo\' titulo, 0 tp_gr, 0 med, 4 amb

              FROM tbl_fiscalización a
              INNER JOIN dim_tiempo d ON a.fecha = d.fecha
              inner join tbl_contrato c on c.contrato_id = a.contrato_id
               WHERE 
              CASE WHEN  '.$mes.'=0 THEN Anio = '.$year.' ELSE
              mes= '.$mes.' AND Anio = '.$year.'

              END
              and case when '.$area.' = 0 then 1=1 else afuncional_id = '.$area.' end
              and case when '.$reg.' = 0 then 1=1 else geo_id = '.$reg.' end
              and case when '.$seg.' = 0 then 1=1 else segmento_id = '.$seg.' end';
    $sql_lin = 'SELECT \'Real\' serie,Fecha_mes fecha ,CAST(AVG((PERC_COND + perc_equip)/2) AS DECIMAL(4,1)) valor,  
                \'Fiscalización Condiciones de Trabajo\' titulo,  \'(Promedio % porcentaje cumplimiento)\' sub, 0 med

                FROM tbl_fiscalización a
                INNER JOIN dim_tiempo b ON a.fecha = b.fecha
                inner join tbl_contrato c on c.contrato_id = a.contrato_id
                WHERE 
                Anio = '.$year.'
                and case when '.$area.' = 0 then 1=1 else afuncional_id = '.$area.' end
                and case when '.$reg.' = 0 then 1=1 else geo_id = '.$reg.' end
                and case when '.$seg.' = 0 then 1=1 else segmento_id = '.$seg.' end
                GROUP BY 1,2

                UNION ALL

                SELECT DISTINCT \'Meta\' Serie, concat(concat(NMes3L,\'-\'),anio), case when mes between 1 and 6 then 80 else 90 end ,
                \'Fiscalización Condiciones de Trabajo\' titulo,  \'(Promedio % porcentaje cumplimiento)\' sub, 0 med
                FROM dim_tiempo
                WHERE anio = '.$year.' AND dia = 1 AND mes >= (SELECT MIN(MONTH(fecha)) FROM tbl_fiscalización WHERE YEAR(fecha) = '.$year.')';
  } 
  else if($ind =="fla") 
{
    $sql_bub = 'SELECT  num, cont_nombre sigla, CAST(AVG((perc_lab)) AS DECIMAL(4,1)) valor,
              CAST(SUM(financieroand_mtoreal) AS DECIMAL(20,0)) dinero,
               cont_proveedor razon_soc,a.contrato_id id
              ,\'Dispersión Fiscalización Laboral\' titulo,  75 lim, 0 lim2,0 t_graf, 0 tp_med
              FROM
              tbl_fiscalización a INNER JOIN (SELECT  @i:=@i+1 AS num, b.* FROM tbl_contrato b,
    (SELECT @i:=0) c ) b ON a.contrato_id = b.contrato_id
              INNER JOIN tbl_financiero_and c ON a.contrato_id = c.contrato_id and c.financieroand_fecha = a.fecha
              INNER JOIN dim_tiempo d ON a.fecha = d.fecha
              WHERE 
              CASE WHEN  '.$mes.'=0 THEN Anio = '.$year.' ELSE
              mes = '.$mes.' AND Anio = '.$year.'

              END 
              and case when '.$area.' = 0 then 1=1 else afuncional_id = '.$area.' end
              and case when '.$reg.' = 0 then 1=1 else geo_id = '.$reg.' end
              and case when '.$seg.' = 0 then 1=1 else segmento_id = '.$seg.' end

              GROUP BY 1,2,5,6';
    $sql_bul = 'SELECT  CAST(AVG((PERC_LAB)) AS DECIMAL(4,1)) puntaje
              ,0 a,75 b, 75 c, 75 d, 75 e, 100 f, \'Fiscalización Laboral\' titulo, 0 tp_gr, 0 med, 3 amb
              FROM tbl_fiscalización a INNER JOIN dim_tiempo b
              ON a.fecha = b.fecha
              inner join tbl_contrato c on c.contrato_id = a.contrato_id
              WHERE CASE WHEN  '.$mes.'=0 THEN Anio = '.$year.' ELSE
              mes = '.$mes.' AND Anio = '.$year.' END
              and case when '.$area.' = 0 then 1=1 else afuncional_id = '.$area.' end
              and case when '.$reg.' = 0 then 1=1 else geo_id = '.$reg.' end
              and case when '.$seg.' = 0 then 1=1 else segmento_id = '.$seg.' end';
    $sql_lin = 'SELECT \'Real\' serie ,fecha_mes fecha ,cast(AVG(perc_lab) as decimal(4,1)) valor,
                  \'Fiscalización Laboral\' titulo,  \'(Promedio % porcentaje cumplimiento)\' sub , 0 med

              FROM tbl_fiscalización a
              INNER JOIN dim_tiempo b
              ON a.fecha = b.fecha
              inner join tbl_contrato c on c.contrato_id = a.contrato_id
              WHERE   Anio = '.$year.' 
              and case when '.$area.' = 0 then 1=1 else afuncional_id = '.$area.' end
              and case when '.$reg.' = 0 then 1=1 else geo_id = '.$reg.' end
              and case when '.$seg.' = 0 then 1=1 else segmento_id = '.$seg.' end
              GROUP BY 1,2

              UNION ALL

              SELECT DISTINCT \'Meta\' Serie, concat(concat(NMes3L,\'-\'),anio), CASE WHEN mes BETWEEN 1 AND 6 THEN 70 ELSE 80 END,
              \'Fiscalización Laboral\' titulo,  \'(Promedio % porcentaje cumplimiento)\' sub, 0 med FROM dim_tiempo 
              WHERE anio = '.$year.' AND dia = 1 AND mes >= (SELECT MIN(MONTH(fecha)) FROM tbl_fiscalización WHERE YEAR(fecha) = '.$year.')
              ';
  } 
  else if($ind =="gar") 
{
    $sql_bub = 'SELECT  num, cont_nombre sigla, CAST(AVG((total_fondos + prendas)/(mutuos + total_impacto))*100 AS DECIMAL(4,1)) valor,
               CAST(SUM(financieroand_mtoreal) AS DECIMAL(20,0)) dinero, cont_proveedor razon_soc,a.contrato_id id, 
              \'Dispersión Garantías y Respaldos\' titulo,  75 lim, 0 lim2,0 t_graf, 0 tp_med

              FROM
              tbl_garantias a INNER JOIN (SELECT  @i:=@i+1 AS num, b.* FROM tbl_contrato b,
    (SELECT @i:=0) c ) b ON a.contrato_id = b.contrato_id
              INNER JOIN tbl_financiero_and  c ON a.contrato_id = c.contrato_id and c.financieroand_fecha = a.fecha
              INNER JOIN dim_tiempo d ON a.fecha = d.fecha
              WHERE 
              CASE WHEN  '.$mes.'=0 THEN Anio = '.$year.' ELSE
              mes = '.$mes.' AND Anio = '.$year.'
              and case when '.$area.' = 0 then 1=1 else afuncional_id = '.$area.' end
              and case when '.$reg.' = 0 then 1=1 else geo_id = '.$reg.' end
              and case when '.$seg.' = 0 then 1=1 else segmento_id = '.$seg.' end
              END 
              GROUP BY 1,2,5,6';
    $sql_bul = 'SELECT  CAST(AVG((total_fondos + prendas)/(mutuos + total_impacto))*100 AS DECIMAL(4,1)) puntaje
                ,0 a,75 b, 75 c, 75 d, 75 e, 100 f,\'Garantías y Respaldos\' titulo, 0 tp_gr, 0 med, 2 amb
                FROM tbl_garantias a INNER JOIN dim_tiempo b
                ON a.fecha = b.fecha
                inner join tbl_contrato c on c.contrato_id = a.contrato_id
                WHERE CASE WHEN  '.$mes.'=0 THEN Anio = '.$year.' ELSE
                mes = '.$mes.' AND Anio = '.$year.' END
                and case when '.$area.' = 0 then 1=1 else afuncional_id = '.$area.' end
                and case when '.$reg.' = 0 then 1=1 else geo_id = '.$reg.' end
                and case when '.$seg.' = 0 then 1=1 else segmento_id = '.$seg.' end';
    $sql_lin = 'SELECT \'Real\' serie, Fecha_mes fecha , 
                CAST(AVG((total_fondos + prendas)/(mutuos + total_impacto))*100 AS DECIMAL(4,1)) valor ,
                \'Garantías y Respaldos\' titulo,  \'(Promedio de cobertura de cada empresa)\' sub, 0 med

                FROM tbl_garantias a INNER JOIN dim_tiempo b
                ON a.fecha = b.fecha
                inner join tbl_contrato c on c.contrato_id = a.contrato_id
                WHERE  Anio = '.$year.' 
                and case when '.$area.' = 0 then 1=1 else afuncional_id = '.$area.' end
                and case when '.$reg.' = 0 then 1=1 else geo_id = '.$reg.' end
                and case when '.$seg.' = 0 then 1=1 else segmento_id = '.$seg.' end
                GROUP BY 1,2

                UNION ALL

                SELECT DISTINCT \'Meta\' Serie, concat(concat(NMes3L,\'-\'),anio), case when semestre = 1 then 50 else 75 end  meta, 
                \'Garantías y Respaldos\' titulo,  \'(Promedio de cobertura de cada empresa)\' sub, 0 med
                 FROM dim_tiempo 
                WHERE anio = '.$year.' AND dia = 1 AND mes >= (SELECT MIN(MONTH(fecha)) FROM tbl_garantias WHERE YEAR(fecha) = '.$year.')';
  } 
  else if($ind =="acc") 
 {
    $sql_bub = 'SELECT  num, cont_nombre sigla, CAST(SUM(n_accid)/SUM(hh)*200000 AS DECIMAL(4,2)) valor, 
                CAST(SUM(financieroand_mtoreal) AS DECIMAL(20,0)) dinero, cont_proveedor razon_soc,a.contrato_id id
                ,\'Dispersión LTIR\' titulo,  2.6 lim, 0 lim2,1 t_graf, 3 tp_med
                FROM
                tbl_accidentes a INNER JOIN (SELECT  @i:=@i+1 AS num, b.* FROM tbl_contrato b,
    (SELECT @i:=0) c ) b ON a.contrato_id = b.contrato_id
                INNER JOIN tbl_financiero_and c ON a.contrato_id = c.contrato_id and c.financieroand_fecha = a.fecha_informe
                INNER JOIN dim_tiempo d ON a.fecha_informe = d.fecha
                WHERE
                CASE WHEN  '.$mes.'=0 THEN Anio = '.$year.' ELSE
                mes = '.$mes.' AND Anio = '.$year.'

                END 
                and case when '.$area.' = 0 then 1=1 else afuncional_id = '.$area.' end
                and case when '.$reg.' = 0 then 1=1 else geo_id = '.$reg.' end
                and case when '.$seg.' = 0 then 1=1 else segmento_id = '.$seg.' end
                GROUP BY 1,2,5,6';
    $sql_bul = 'SELECT CAST(case when (SUM(n_accid)/SUM(hh)*200000 ) > 5.2 then 5.2 else (SUM(n_accid)/SUM(hh)*200000 ) end AS DECIMAL(4,2)) puntaje
                ,0 a,2.6 b, 2.6 c, 2.6 d, 2.6 e,5.2 f, \'Índice Accidentabilidad\' titulo, 1 tp_gr, 1 med, 4 amb

                FROM tbl_accidentes a
                INNER JOIN dim_tiempo d ON a.fecha_informe = d.fecha
                inner join tbl_contrato c on c.contrato_id = a.contrato_id
                 WHERE 
                CASE WHEN  '.$mes.'=0 THEN Anio = '.$year.' ELSE
                mes= '.$mes.' AND Anio = '.$year.'

                END

                and case when '.$area.' = 0 then 1=1 else afuncional_id = '.$area.' end
                and case when '.$reg.' = 0 then 1=1 else geo_id = '.$reg.' end
                and case when '.$seg.' = 0 then 1=1 else segmento_id = '.$seg.' end';
    $sql_lin = '
                SELECT \'Real\' serie,Fecha_mes  fecha,CAST(SUM(n_accid)/SUM(hh)*200000 AS DECIMAL(4,1)) valor, 
                \'Índice LTIR\' titulo,  \'(N° Accidentes/HH *200000)\' sub, 1 med
                FROM tbl_accidentes a
                INNER JOIN dim_tiempo ON fecha_informe = fecha
                inner join tbl_contrato c on c.contrato_id = a.contrato_id
                WHERE 
                CASE WHEN '.$year.'!=\'\' THEN Anio = '.$year.' ELSE 1=1 END
                and case when '.$area.' = 0 then 1=1 else afuncional_id = '.$area.' end
                and case when '.$reg.' = 0 then 1=1 else geo_id = '.$reg.' end
                and case when '.$seg.' = 0 then 1=1 else segmento_id = '.$seg.' end

                GROUP BY 1,2

                UNION ALL

                SELECT DISTINCT \'Meta\' Serie, concat(concat(NMes3L,\'-\'),anio), 1.5,
                \'Índice LTIR\' titulo,  \'(N° Accidentes/HH *200000)\' sub , 1 med
                 FROM dim_tiempo
                WHERE anio = '.$year.' AND dia = 1 AND mes >= (SELECT MIN(MONTH(fecha_informe)) FROM tbl_accidentes WHERE YEAR(fecha_informe) = '.$year.')';
  } 
  else if($ind =="gra") 
  {
    $sql_bub = 'SELECT  num, cont_nombre sigla, CAST(SUM(DIAS_PERD)/SUM(hh)*200000 AS DECIMAL(4,2)) valor , 
          CAST(SUM(financieroand_mtoreal) AS DECIMAL(20,0)) dinero, cont_proveedor razon_soc,a.contrato_id id

          ,\'Dispersión LTISR\' titulo,  31.1 lim, 0 lim2,1 t_graf, 3 tp_med
          FROM
          tbl_accidentes a INNER JOIN (SELECT  @i:=@i+1 AS num, b.* FROM tbl_contrato b,
    (SELECT @i:=0) c ) b ON a.contrato_id = b.contrato_id
          INNER JOIN tbl_financiero_and c ON a.contrato_id = c.contrato_id and c.financieroand_fecha  = a.fecha_informe
          INNER JOIN dim_tiempo d ON a.fecha_informe = d.fecha
          WHERE
          CASE WHEN  '.$mes.'=0 THEN Anio = '.$year.' ELSE
          mes = '.$mes.' AND Anio = '.$year.'

          END 
          and case when '.$area.' = 0 then 1=1 else afuncional_id = '.$area.' end
          and case when '.$reg.' = 0 then 1=1 else geo_id = '.$reg.' end
          and case when '.$seg.' = 0 then 1=1 else segmento_id = '.$seg.' end

          GROUP BY 1,2,5,6';
    $sql_bul = 'SELECT CAST(case when (SUM(DIAS_PERD)/SUM(hh)*200000) > 60 then 60 else SUM(DIAS_PERD)/SUM(hh)*200000 end AS DECIMAL(4,2)) puntaje
                ,0 a,31.1 b, 31.1 c, 31.1 d, 31.1 e, 60 f
                ,\'Índice Gravedad\' titulo, 1 tp_gr, 1 med, 4 amb
                FROM tbl_accidentes a
                INNER JOIN dim_tiempo d ON a.fecha_informe = d.fecha
                inner join tbl_contrato c on c.contrato_id = a.contrato_id
                 WHERE 
                CASE WHEN  '.$mes.'=0 THEN Anio = '.$year.' ELSE
                mes= '.$mes.' AND Anio = '.$year.'

                END
                and case when '.$area.' = 0 then 1=1 else afuncional_id = '.$area.' end
                and case when '.$reg.' = 0 then 1=1 else geo_id = '.$reg.' end
                and case when '.$seg.' = 0 then 1=1 else segmento_id = '.$seg.' end';
    $sql_lin = 'SELECT \'Real\' serie,Fecha_mes  fecha ,CAST(SUM(DIAS_PERD)/SUM(hh)*200000 AS DECIMAL(4,1)) valor ,
                \'Índice LTISR\' titulo,  \'(N° Días Perdidos/HH *200000)\' sub , 1 med
                FROM tbl_accidentes a
                INNER JOIN dim_tiempo ON fecha_informe = fecha
                inner join tbl_contrato c on c.contrato_id = a.contrato_id
                WHERE 
                CASE WHEN '.$year.'!=\'\' THEN Anio = '.$year.' ELSE 1=1 END
                and case when '.$area.' = 0 then 1=1 else afuncional_id = '.$area.' end
                and case when '.$reg.' = 0 then 1=1 else geo_id = '.$reg.' end
                and case when '.$seg.' = 0 then 1=1 else segmento_id = '.$seg.' end
                GROUP BY 1,2

                UNION ALL

                SELECT DISTINCT \'Meta\' Serie, concat(concat(NMes3L,\'-\'),anio), 20,
                \'Índice LTISR\' titulo,  \'(N° Días Perdidos/HH *200000)\' sub , 1 med
                 FROM dim_tiempo
                WHERE anio = '.$year.' AND dia = 1 AND mes >= (SELECT MIN(MONTH(fecha_Eval)) FROM tbl_morosidad WHERE YEAR(fecha_Eval) = '.$year.')
';
  }  
  else if($ind =="mit") {
    $sql_bub = 'SELECT   num, cont_nombre sigla, COALESCE(COUNT(DISTINCT c.contrato_id ),0)  valor,
                 CAST(SUM(financieroand_mtoreal) AS DECIMAL(20,0))  dinero,
                 cont_proveedor razon_soc,a.contrato_id id
                ,\'Multas en Inspección del Trabajo\' titulo,  0.5 lim, 0 lim2 ,1 t_graf, 2 tp_med

                FROM 

                ((SELECT  @i:=@i+1 AS num, b.* FROM tbl_contrato b,
    (SELECT @i:=0) c ) a 
                CROSS JOIN (SELECT DISTINCT fecha,mes,Anio FROM dim_tiempo WHERE fecha 
                BETWEEN (SELECT MIN(finCont_fecha) FROM tbl_contratistacondicionfin WHERE YEAR(finCont_fecha) = '.$year.') AND DATE_ADD(CURRENT_DATE , INTERVAL - (DAY(CURRENT_DATE)) DAY)
                AND dia = 1 ) z
                )
                INNER JOIN tbl_financiero_and d ON a.contrato_id = d.contrato_id AND d.financieroand_fecha = z.fecha
                LEFT JOIN (tbl_contratistacondicionfin c 
                INNER JOIN dim_tiempo b
                ON b.fecha = c.finCont_fecha) ON a.contrato_id = c.contrato_id AND z.fecha = b.fecha

                WHERE cont_fechaFin >= CURRENT_DATE

                 AND CASE WHEN  '.$mes.'=0 THEN z.Anio ='.$year.' ELSE
                 z.mes = '.$mes.' AND z.Anio = '.$year.' END
                AND CASE WHEN '.$area.' = 0 THEN 1=1 ELSE afuncional_id = '.$area.' END
                AND CASE WHEN '.$reg.' = 0 THEN 1=1 ELSE geo_id = '.$reg.' END
                AND CASE WHEN '.$seg.' = 0 THEN 1=1 ELSE segmento_id = '.$seg.' END
                GROUP BY 1,2,5,6';
    $sql_bul = 'SELECT  COUNT(DISTINCT a.contrato_id) puntaje,
                0 a,1 b, 1 c, 1 d, 1 e, 22 f,\'${prm_tipo}\' tipo, \'Multas Inspección del Trabajo\' titulo, 1 tp_gr, 1 med, 3 amb
                FROM tbl_contratistacondicionfin a
                INNER JOIN dim_tiempo b
                ON a.finCont_fecha = b.fecha
                INNER JOIN tbl_contrato c ON c.contrato_id = a.contrato_id
                WHERE CASE WHEN  '.$mes.'=0 THEN Anio = '.$year.' ELSE
                mes = '.$mes.' AND Anio = '.$year.' END
                AND CASE WHEN '.$area.' = 0 THEN 1=1 ELSE afuncional_id = '.$area.' END
                AND CASE WHEN '.$reg.' = 0 THEN 1=1 ELSE geo_id = '.$reg.' END
                AND CASE WHEN '.$seg.' = 0 THEN 1=1 ELSE segmento_id = '.$seg.' END';
    $sql_lin = 'SELECT \'Real\' serie,Fecha_mes fecha, COUNT(DISTINCT a.contrato_id) valor,
                \'Multas Inspección de Trabajo\' titulo,  \'N° empresas diferentes con multas\' sub , 1 med
                FROM tbl_contratistacondicionfin a
                INNER JOIN tbl_contrato c ON c.contrato_id = a.contrato_id
                INNER JOIN dim_tiempo b
                ON a.finCont_fecha = b.fecha
                WHERE Anio = '.$year.'
                AND CASE WHEN '.$area.' = 0 THEN 1=1 ELSE afuncional_id = '.$area.' END
                AND CASE WHEN '.$reg.' = 0 THEN 1=1 ELSE geo_id = '.$reg.' END
                AND CASE WHEN '.$seg.' = 0 THEN 1=1 ELSE segmento_id = '.$seg.' END
                AND mes <= (SELECT MAX(MONTH(finCont_fecha)) FROM tbl_contratistacondicionfin WHERE YEAR(finCont_fecha) = '.$year.')


                GROUP BY 1,2

                UNION ALL

                SELECT DISTINCT \'Meta\' Serie, CONCAT(CONCAT(NMes3L,\'-\'),anio), 1 , \'Multas Inspección de Trabajo\' titulo,  \'N° empresas diferentes con multas\' sub, 1 med
                 FROM dim_tiempo
                WHERE anio = '.$year.' AND dia = 1 AND mes BETWEEN (SELECT MIN(MONTH(finCont_fecha)) FROM tbl_contratistacondicionfin WHERE YEAR(finCont_fecha) = '.$year.') 
                AND (SELECT MAX(MONTH(finCont_fecha)) FROM tbl_contratistacondicionfin WHERE YEAR(finCont_fecha) = '.$year.')
                ';
  } 
    
  else if($ind =="obl"){
    $sql_bub = 'SELECT  num, cont_nombre sigla, CAST(AVG((valor))*100 AS DECIMAL(4,1)) valor,  cast(SUM(financieroand_mtoreal) as decimal(20,0)) dinero, 
                cont_proveedor razon_soc,a.contrato_id id
                ,\'Dispersión Obligaciones Laborales\' titulo,  16 lim, 0 lim2 ,1 t_graf, 0 tp_med
                FROM
                tbl_obliglab_repo a INNER JOIN (SELECT  @i:=@i+1 AS num, b.* FROM tbl_contrato b,
    (SELECT @i:=0) c ) b ON a.contrato_id = b.contrato_id
                INNER JOIN tbl_financiero_and c ON a.contrato_id = c.contrato_id and c.financieroand_fecha = a.fecha
                INNER JOIN dim_tiempo d ON a.fecha = d.fecha
                WHERE valor IS NOT NULL
                AND 
                CASE WHEN  '.$mes.'=0 THEN Anio = '.$year.' ELSE
                mes = '.$mes.' AND Anio = '.$year.'

                 END 
                 and case when '.$area.' = 0 then 1=1 else afuncional_id = '.$area.' end
                and case when '.$reg.' = 0 then 1=1 else geo_id = '.$reg.' end
                and case when '.$seg.' = 0 then 1=1 else segmento_id = '.$seg.' end

                GROUP BY 1,2,5,6';
    $sql_bul = 'SELECT  CAST(AVG(valor)*100 AS DECIMAL(4,1)) puntaje,0 a,16 b, 16 c, 16 d, 16 e, 100 f,
                \'Obligaciones Laborales\' titulo, 1 tp_gr, 0 med, 3 amb

              FROM tbl_obliglab_repo a INNER JOIN dim_tiempo b
              ON a.fecha = b.fecha
              inner join tbl_contrato c on c.contrato_id = a.contrato_id
              WHERE CASE WHEN  '.$mes.'=0 THEN Anio = '.$year.' ELSE
              mes = '.$mes.' AND Anio = '.$year.' END
              and case when '.$area.' = 0 then 1=1 else afuncional_id = '.$area.' end
              and case when '.$reg.' = 0 then 1=1 else geo_id = '.$reg.' end
              and case when '.$seg.' = 0 then 1=1 else segmento_id = '.$seg.' end';
    $sql_lin = 'SELECT \'Real\' serie, Fecha_mes fecha, CAST(AVG(valor)*100 AS DECIMAL(4,1)) valor,
                \'Potencial Incumplimiento Obligaciones Laborales\' titulo,  \'Promedio de porcentaje de trabajadores no informados o no pagados sobre total trabajadores ultimos 12 meses\' sub
                , 0 med
                FROM tbl_obliglab_repo a INNER JOIN dim_tiempo b
                ON a.fecha = b.fecha
                inner join tbl_contrato c on c.contrato_id = a.contrato_id
                WHERE  Anio = '.$year.'
                and case when '.$area.' = 0 then 1=1 else afuncional_id = '.$area.' end
                and case when '.$reg.' = 0 then 1=1 else geo_id = '.$reg.' end
                and case when '.$seg.' = 0 then 1=1 else segmento_id = '.$seg.' end

                GROUP BY 1,2

                UNION ALL

                SELECT DISTINCT \'Meta\' Serie, concat(concat(NMes3L,\'-\'),anio), 8
                ,
                \'Potencial Incumplimiento Obligaciones Laborales\' titulo,  \'Promedio de porcentaje de trabajadores no informados o no pagados sobre total trabajadores ultimos 12 meses\' sub
                , 0 med  FROM dim_tiempo 
                WHERE anio = '.$year.' AND dia = 1 AND mes >= (SELECT MIN(MONTH(fecha)) FROM tbl_obliglab_repo  WHERE YEAR(fecha) = '.$year.')

                ';
  }  
    
  else if($ind =="tri"){
    $sql_bub = 'SELECT  num, cont_nombre sigla, CASE WHEN tCont_eval = \'BUENO\' THEN 0 ELSE 1 END  valor,  cast(SUM(financieroand_mtoreal) as decimal(20,0)) dinero,
               cont_proveedor razon_soc,a.contrato_id id
              ,\'Dispersión Situación Tributaria\' titulo,  0.5 lim, 0 lim2 ,1 t_graf, 1 tp_med
              FROM
              tbl_contratista_tributario a INNER JOIN (SELECT  @i:=@i+1 AS num, b.* FROM tbl_contrato b,
    (SELECT @i:=0) c ) b ON a.contrato_id = b.contrato_id
              INNER JOIN tbl_financiero_and c ON a.contrato_id = c.contrato_id and c.financieroand_fecha =  a.tCont_fecha
              INNER JOIN dim_tiempo d ON a.tCont_fecha = d.fecha
              WHERE case when  '.$mes.'=0 then Anio = '.$year.' else
              mes = '.$mes.' AND Anio = '.$year.'
              END
              and case when '.$area.' = 0 then 1=1 else afuncional_id = '.$area.' end
              and case when '.$reg.' = 0 then 1=1 else geo_id = '.$reg.' end
              and case when '.$seg.' = 0 then 1=1 else segmento_id = '.$seg.' end
              group by 1,2,5,6';
    $sql_bul = 'SELECT cast((SUM(CASE WHEN tCont_eval = \'MALO\' THEN 1 ELSE 0 END)/ 
              COUNT( a.contrato_id))*100 as DECIMAL(4,1)) puntaje,0 a,4 b, 4 c, 4 d, 4 e, 100 f, \'Situación Tributaria\' titulo, 1 tp_gr, 0 med, 2 amb
              FROM
              tbl_contratista_tributario a INNER JOIN tbl_contrato b ON a.contrato_id = b.contrato_id
              INNER JOIN dim_tiempo d ON a.tCont_fecha = d.fecha
              WHERE 
              case when  '.$mes.'=0 then Anio = '.$year.' else
              mes= '.$mes.' AND Anio = '.$year.'

              end 
                
              and case when '.$area.' = 0 then 1=1 else afuncional_id = '.$area.' end
              and case when '.$reg.' = 0 then 1=1 else geo_id = '.$reg.' end
              and case when '.$seg.' = 0 then 1=1 else segmento_id = '.$seg.' end
              ';
    $sql_lin = 'SELECT \'Real\' serie, Fecha_mes fecha, 
                cast((SUM(CASE WHEN tCont_eval = \'BUENO\' THEN 0 ELSE 1 END)  
                /(SELECT COUNT(DISTINCT contrato_id) 
                FROM tbl_contratista_tributario WHERE tCont_fecha = a.tCont_fecha)) *100 as DECIMAL(4,1)) valor,
                \'Situación Tributaria\' titulo,  \'% Empresas con conflicto respecto de total de Empresas\' sub , 0 med



                FROM tbl_contratista_tributario a
                inner join tbl_contrato b on a.contrato_id = b.contrato_id
                inner join dim_tiempo c on a.tCont_fecha = c.fecha
                where 
                case when '.$year.'!=\'\' then Anio = '.$year.' else 1=1 end
                 
                and case when '.$area.' = 0 then 1=1 else afuncional_id = '.$area.' end
                and case when '.$reg.' = 0 then 1=1 else geo_id = '.$reg.' end
                and case when '.$seg.' = 0 then 1=1 else segmento_id = '.$seg.' end
                GROUP BY tCont_fecha

                UNION ALL

                SELECT DISTINCT \'Meta\' Serie, concat(concat(NMes3L,\'-\'),anio), 10,
                \'Situación Tributaria\' titulo,  \'% Empresas con conflicto respecto de total de Empresas\' sub, 0 med
                  FROM dim_tiempo
                WHERE anio = '.$year.' AND dia = 1 AND mes >= (SELECT MIN(MONTH(tCont_fecha)) FROM tbl_contratista_tributario WHERE YEAR(tCont_fecha) = '.$year.')';
  }  

    if($tip==1)
    {
      $sqlgr = $sql_bub;
    }
    else if($tip == 2)
    {
      $sqlgr = $sql_bul;

    }
    else if($tip == 3){
      $sqlgr = $sql_lin;

    }

                    $rawgraf = array();
                    $i=0;
                    
                    $row1 = $conn2->query($sqlgr);
                    $results = $row1->fetchAll(PDO::FETCH_ASSOC);       
                       
                    echo json_encode($results);
                  
}
else
{ ?>
 <form method="post">
   <div style="display: inline-block;width:70px; height:30px;">
      <b>Año</b>
                     <select name="sl_year" id="sl_year" onchange="contratos(this.value,1)">

<?php
                   $sqlyear = ' select distinct Anio from dim_tiempo 
                              where anio between year(current_Date) -5
                              and YEAR(CASE WHEN DAY(CURRENT_DATE) >= 20 THEN DATE_ADD(DATE_ADD(CURRENT_DATE, INTERVAL -1 MONTH), INTERVAL -(DAY(CURRENT_DATE)-1) DAY)  ELSE DATE_ADD(DATE_ADD(CURRENT_DATE, INTERVAL -2 MONTH), INTERVAL -(DAY(CURRENT_DATE)-1) DAY) END)
                              order by anio desc
                            ';
                     $i=0;
                    
                    foreach ($conn2->query($sqlyear) as $row1) {
                           if($row1["Anio"]==$year){
                              echo "<option  value='".$row1["Anio"]."' selected>".$row1["Anio"]."</option>"; 
                           }
                           else
                              echo "<option  value='".$row1["Anio"]."'>".$row1["Anio"]."</option>"; 
                          $i++;
                    }
                    
            ?>

                    </select>

       
       </div>  
       <div style="display: inline-block; width:80px; height:30px;">
            
           <b>Mes</b>
                     <select name="sl_mes" id="sl_mes" onchange="contratos(this.value,2)">

<?php


                   $sqlmes = ' SELECT 0 mes, \'Todos\' NMes3L
                   UNION ALL
                   SELECT DISTINCT mes , NMes3L  FROM dim_tiempo 
                   WHERE fecha_mes <= CASE WHEN DAY(CURRENT_DATE) >= 20 THEN DATE_ADD(DATE_ADD(CURRENT_DATE, INTERVAL -1 MONTH), INTERVAL -(DAY(CURRENT_DATE)-1) DAY)  ELSE DATE_ADD(DATE_ADD(CURRENT_DATE, INTERVAL -2 MONTH), INTERVAL -(DAY(CURRENT_DATE)-1) DAY) END
                   AND  anio = '.$year.' 
                   ORDER BY mes';
                     $i=0;
                    
                    foreach ($conn2->query($sqlmes) as $row1) {
                           if($row1["mes"]==$mes){
                              echo "<option  value='".$row1["mes"]."' selected>".$row1["NMes3L"]."</option>"; 
                           }
                           else
                              echo "<option  value='".$row1["mes"]."'>".$row1["NMes3L"]."</option>"; 
                          $i++;
                    }
                    
            ?>

                   
                    </select>
</div>
  <div style="display: inline-block;width:120px; height:30px;">
                <b>Tipo Faena</b>
                     <select name="sl_reg" id="sl_reg" onchange="contratos(this.value,3)">

<?php
                   $sqlreg = ' SELECT 0 id, \'Todos\' reg
                   UNION ALL
                   SELECT geo_id, geo_nombre FROM tbl_contgeografico';
                     $i=0;
                    
                    foreach ($conn2->query($sqlreg) as $row1) {
                           if($row1["id"]==$reg){
                              echo "<option  value='".$row1["id"]."' selected>".$row1["reg"]."</option>"; 
                           }
                           else
                              echo "<option  value='".$row1["id"]."'>".$row1["reg"]."</option>"; 
                          $i++;
                    }
                    
            ?>

                   
                    </select>
</div>
 <div style="display: inline-block;width:280px; height:30px;">
                <b>Área</b>
                     <select name="sl_area" id="sl_area" onchange="contratos(this.value,4)">

<?php
                   $sqlarea = ' SELECT 0 id, \'Todos\' area 
                   union all
                   SELECT afuncional_id ,afuncional_nombre area FROM tbl_contareafuncional';
                     $i=0;
                    
                    foreach ($conn2->query($sqlarea) as $row1) {
                           if($row1["id"]==$area){
                              echo "<option  value='".$row1["id"]."' selected>".$row1["area"]."</option>"; 
                           }
                           else
                              echo "<option  value='".$row1["id"]."'>".$row1["area"]."</option>"; 
                          $i++;
                    }
                    
            ?>

                   
                    </select>
        </div>
        <div style="display: inline-block;width:280px; height:30px;">

                    <b>Segmento</b>
                     <select name="sl_seg" id="sl_seg" onchange="contratos(this.value,5)">

<?php
                   $sqlseg = ' SELECT 0 id, \'Todos\' nombre
                   union all
                   SELECT segmento_id, seg_nombre FROM tbl_contsegmento';
                     $i=0;
                    
                    foreach ($conn2->query($sqlseg) as $row1) {
                           if($row1["id"]==$seg){
                              echo "<option  value='".$row1["id"]."' selected>".$row1["nombre"]."</option>"; 
                           }
                           else
                              echo "<option  value='".$row1["id"]."'>".$row1["nombre"]."</option>"; 
                          $i++;
                    }
                    
            ?>

                   
                    </select>
          </div>
          <div style="display: inline-block;width:100px; height:30px;">
                <button type="reset" class="btn-xs btn-default " onclick="contratos('0',6)">Limpiar</button>
          </div>
        </form>

<?php
}
                 ?>               
