<div class="m-t" style="padding-top:25px;">	
    <div class="row m-b-lg animated fadeInDown delayp1 text-center">
        <h3> {{ $pageTitle }} <small> {{ $pageNote }} </small></h3>
        <hr />       
    </div>
</div>
<div class="m-t">
	<div class="table-responsive" > 	

		<table class="table table-striped table-bordered" >
			<tbody>	
		
			
					<tr>
						<td width='30%' class='label-view text-right'>{{ SiteHelpers::activeLang('IdDocumentoValor', (isset($fields['IdDocumentoValor']['language'])? $fields['IdDocumentoValor']['language'] : array())) }}</td>
						<td>{{ $row->IdDocumentoValor}} </td>

					</tr>
				
					<tr>
						<td width='30%' class='label-view text-right'>{{ SiteHelpers::activeLang('Documento', (isset($fields['IdDocumento']['language'])? $fields['IdDocumento']['language'] : array())) }}</td>
						<td>{{ $row->IdDocumento}} </td>

					</tr>
				
					<tr>
						<td width='30%' class='label-view text-right'>{{ SiteHelpers::activeLang('Tipo Documento Valor', (isset($fields['IdTipoDocumentoValor']['language'])? $fields['IdTipoDocumentoValor']['language'] : array())) }}</td>
						<td>{{ $row->IdTipoDocumentoValor}} </td>

					</tr>
				
					<tr>
						<td width='30%' class='label-view text-right'>{{ SiteHelpers::activeLang('Valor', (isset($fields['Valor']['language'])? $fields['Valor']['language'] : array())) }}</td>
						<td>{{ $row->Valor}} </td>

					</tr>
						
					<tr>
						<td width='30%' class='label-view text-right'></td>
						<td> <a href="javascript:history.go(-1)" class="btn btn-primary"> Back To Grid <a> </td>
						
					</tr>					
				
			</tbody>	
		</table>   

	 
	
	</div>
</div>	