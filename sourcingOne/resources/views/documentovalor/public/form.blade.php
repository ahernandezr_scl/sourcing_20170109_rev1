

		 {!! Form::open(array('url'=>'documentovalor/savepublic', 'class'=>'form-horizontal','files' => true , 'parsley-validate'=>'','novalidate'=>' ')) !!}

	@if(Session::has('messagetext'))
	  
		   {!! Session::get('messagetext') !!}
	   
	@endif
	<ul class="parsley-error-list">
		@foreach($errors->all() as $error)
			<li>{{ $error }}</li>
		@endforeach
	</ul>		


<div class="col-md-12">
						<fieldset><legend> Documentos Valores</legend>
				{!! Form::hidden('IdDocumentoValor', $row['IdDocumentoValor']) !!}
									  <div class="form-group  " >
										<label for="Documento" class=" control-label col-md-4 text-left"> Documento </label>
										<div class="col-md-6">
										  <select name='IdDocumento' rows='5' id='IdDocumento' class='select2 '   ></select>
										 </div>
										 <div class="col-md-2">
										 	
										 </div>
									  </div> 
									  <div class="form-group  " >
										<label for="IdTipoDocumentoValor" class=" control-label col-md-4 text-left"> IdTipoDocumentoValor </label>
										<div class="col-md-6">
										  <select name='IdTipoDocumentoValor' rows='5' id='IdTipoDocumentoValor' class='select2 '   ></select>
										 </div>
										 <div class="col-md-2">
										 	
										 </div>
									  </div> 
									  <div class="form-group  " >
										<label for="Valor" class=" control-label col-md-4 text-left"> Valor </label>
										<div class="col-md-6">
										  {!! Form::text('Valor', $row['Valor'],array('class'=>'form-control', 'placeholder'=>'',   )) !!}
										 </div>
										 <div class="col-md-2">
										 	
										 </div>
									  </div> </fieldset>
			</div>

			

			<div style="clear:both"></div>	
				
					
				  <div class="form-group">
					<label class="col-sm-4 text-right">&nbsp;</label>
					<div class="col-sm-8">	
					<button type="submit" name="apply" class="btn btn-info btn-sm" ><i class="fa  fa-check-circle"></i> {{ Lang::get('core.sb_apply') }}</button>
					<button type="submit" name="submit" class="btn btn-primary btn-sm" ><i class="fa  fa-save "></i> {{ Lang::get('core.sb_save') }}</button>
				  </div>	  
			
		</div> 
		 
		 {!! Form::close() !!}
		 
   <script type="text/javascript">
	$(document).ready(function() { 
		
		
		$("#IdDocumento").jCombo("{!! url('documentovalor/comboselect?filter=tbl_documentos:IdDocumento:IdDocumento') !!}",
		{  selected_value : '{{ $row["IdDocumento"] }}' });
		
		$("#IdTipoDocumentoValor").jCombo("{!! url('documentovalor/comboselect?filter=tbl_tipo_documento_valor:IdTipoDocumentoValor:IdTipoDocumentoValor') !!}",
		{  selected_value : '{{ $row["IdTipoDocumentoValor"] }}' });
		 

		$('.removeCurrentFiles').on('click',function(){
			var removeUrl = $(this).attr('href');
			$.get(removeUrl,function(response){});
			$(this).parent('div').empty();	
			return false;
		});		
		
	});
	</script>		 
