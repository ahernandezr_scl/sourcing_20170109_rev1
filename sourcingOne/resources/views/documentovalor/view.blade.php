@if($setting['view-method'] =='native')
<div class="sbox">
	<div class="sbox-title">  
		<h4> <i class="fa fa-table"></i> <?php echo $pageTitle ;?> <small>{{ $pageNote }}</small>
			<a href="javascript:void(0)" class="collapse-close pull-right btn btn-xs btn-danger" onclick="ajaxViewClose('#{{ $pageModule }}')">
			<i class="fa fa fa-times"></i></a>
		</h4>
	 </div>

	<div class="sbox-content"> 
@endif	

		<table class="table table-striped table-bordered" >
			<tbody>	
				
					<tr>
						<td width='30%' class='label-view text-right'>{{ SiteHelpers::activeLang('IdDocumentoValor', (isset($fields['IdDocumentoValor']['language'])? $fields['IdDocumentoValor']['language'] : array())) }}</td>
						<td>{{ $row->IdDocumentoValor}} </td>

					</tr>
				
					<tr>
						<td width='30%' class='label-view text-right'>{{ SiteHelpers::activeLang('Documento', (isset($fields['IdDocumento']['language'])? $fields['IdDocumento']['language'] : array())) }}</td>
						<td>{{ $row->IdDocumento}} </td>

					</tr>
				
					<tr>
						<td width='30%' class='label-view text-right'>{{ SiteHelpers::activeLang('Tipo Documento Valor', (isset($fields['IdTipoDocumentoValor']['language'])? $fields['IdTipoDocumentoValor']['language'] : array())) }}</td>
						<td>{{ $row->IdTipoDocumentoValor}} </td>

					</tr>
				
					<tr>
						<td width='30%' class='label-view text-right'>{{ SiteHelpers::activeLang('Valor', (isset($fields['Valor']['language'])? $fields['Valor']['language'] : array())) }}</td>
						<td>{{ $row->Valor}} </td>

					</tr>
				
			</tbody>	
		</table>  
			
		 	

@if($setting['form-method'] =='native')
	</div>	
</div>	
@endif	

<script>
$(document).ready(function(){

});
</script>	