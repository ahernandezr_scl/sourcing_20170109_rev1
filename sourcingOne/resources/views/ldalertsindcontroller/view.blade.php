@extends('layouts.app')

@section('content')
<div class="page-content row">
	 
	 
 	<div class="page-content-wrapper m-t">   

<div class="sbox "> 
	<div class="sbox-title"> 

	 <h3> {{ $pageTitle }} <small>{{ $pageNote }}</small></h3>

	 	<div class="sbox-tools">
	   		<a href="{{ URL::to('ldalertsindcontroller?return='.$return) }}" class="tips btn btn-xs btn-white pull-right" title="{{ Lang::get('core.btn_back') }}"><i class="fa fa-arrow-circle-left"></i>&nbsp;{{ Lang::get('core.btn_back') }}</a>
			
			@if($access['is_add'] ==1)
	   		<a href="{{ URL::to('ldalertsindcontroller/update/'.$id.'?return='.$return) }}" class="tips btn btn-xs btn-white pull-right" title="{{ Lang::get('core.btn_edit') }}"><i class="fa fa-edit"></i>&nbsp;{{ Lang::get('core.btn_edit') }}</a>
			@endif 
		</div>
	</div>
	<div class="sbox-content" > 	

		<table class="table table-striped table-bordered" >
			<tbody>	
		
					<tr>
						<td width='30%' class='label-view text-right'>{{ SiteHelpers::activeLang('Alert Id', (isset($fields['alertas_id']['language'])? $fields['alertas_id']['language'] : array())) }}</td>
						<td>{{ $row->alertas_id}} </td>
						
					</tr>
				
					<tr>
						<td width='30%' class='label-view text-right'>{{ SiteHelpers::activeLang('Contrato', (isset($fields['contrato_id']['language'])? $fields['contrato_id']['language'] : array())) }}</td>
						<td>{{ SiteHelpers::formatLookUp($row->contrato_id,'contrato_id','1:tbl_contrato:contrato_id:cont_numero|cont_proveedor') }} </td>
						
					</tr>
				
					<tr>
						<td width='30%' class='label-view text-right'>{{ SiteHelpers::activeLang('Subambito', (isset($fields['subambito_id']['language'])? $fields['subambito_id']['language'] : array())) }}</td>
						<td>{{ SiteHelpers::formatLookUp($row->subambito_id,'subambito_id','1:tbl_planes_y_programas_ambitosub:subambito_id:ambito_nombre') }} </td>
						
					</tr>
				
					<tr>
						<td width='30%' class='label-view text-right'>{{ SiteHelpers::activeLang('Tipo Alerta', (isset($fields['tipo_alerta']['language'])? $fields['tipo_alerta']['language'] : array())) }}</td>
						<td>{{ SiteHelpers::formatLookUp($row->tipo_alerta,'tipo_alerta','1:tbl_alerta_tpo:tipo_alerta:desc_tipo_alerta') }} </td>
						
					</tr>
				
					<tr>
						<td width='30%' class='label-view text-right'>{{ SiteHelpers::activeLang('Mensaje', (isset($fields['id_mensaje']['language'])? $fields['id_mensaje']['language'] : array())) }}</td>
						<td>{{ SiteHelpers::formatLookUp($row->id_mensaje,'id_mensaje','1:tbl_alerta_msg:id_mensaje:texto_mensaje') }} </td>
						
					</tr>
				
					<tr>
						<td width='30%' class='label-view text-right'>{{ SiteHelpers::activeLang('Fecha Inicio Alerta', (isset($fields['fecha_ini']['language'])? $fields['fecha_ini']['language'] : array())) }}</td>
						<td>{{ date('d/m/Y',strtotime($row->fecha_ini)) }} </td>
						
					</tr>
				
					<tr>
						<td width='30%' class='label-view text-right'>{{ SiteHelpers::activeLang('Fecha Fin Alerta', (isset($fields['fecha_fin']['language'])? $fields['fecha_fin']['language'] : array())) }}</td>
						<td>{{ date('d/m/Y',strtotime($row->fecha_fin)) }} </td>
						
					</tr>
				
					<tr>
						<td width='30%' class='label-view text-right'>{{ SiteHelpers::activeLang('Estado Alerta', (isset($fields['estado_al']['language'])? $fields['estado_al']['language'] : array())) }}</td>
						<td>{{ $row->estado_al}} </td>
						
					</tr>
				
					<tr>
						<td width='30%' class='label-view text-right'>{{ SiteHelpers::activeLang('Tipo Programación', (isset($fields['tipo_prog']['language'])? $fields['tipo_prog']['language'] : array())) }}</td>
						<td>{{ $row->tipo_prog}} </td>
						
					</tr>
				
					<tr>
						<td width='30%' class='label-view text-right'>{{ SiteHelpers::activeLang('Criticidad de Alerta', (isset($fields['id_crit']['language'])? $fields['id_crit']['language'] : array())) }}</td>
						<td>{{ SiteHelpers::formatLookUp($row->id_crit,'id_crit','1:tbl_alerta_crit:id_crit:desc_crit') }} </td>
						
					</tr>
				
					<tr>
						<td width='30%' class='label-view text-right'>{{ SiteHelpers::activeLang('Fecha último aviso', (isset($fields['fec_ult_alerta']['language'])? $fields['fec_ult_alerta']['language'] : array())) }}</td>
						<td>{{ date('d/m/Y',strtotime($row->fec_ult_alerta)) }} </td>
						
					</tr>
				
			</tbody>	
		</table>   
	
	</div>
</div>	

	</div>
</div>
	  
@stop