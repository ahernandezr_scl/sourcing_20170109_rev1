

		 {!! Form::open(array('url'=>'ldalertsindcontroller/savepublic', 'class'=>'form-horizontal','files' => true , 'parsley-validate'=>'','novalidate'=>' ')) !!}

	@if(Session::has('messagetext'))
	  
		   {!! Session::get('messagetext') !!}
	   
	@endif
	<ul class="parsley-error-list">
		@foreach($errors->all() as $error)
			<li>{{ $error }}</li>
		@endforeach
	</ul>		


<div class="col-md-12">
						<fieldset><legend> Alertas - Carga Manual Alertas Indicadores</legend>
				{!! Form::hidden('alertas_id', $row['alertas_id']) !!}					
									  <div class="form-group  " >
										<label for="Contrato" class=" control-label col-md-4 text-left"> Contrato <span class="asterix"> * </span></label>
										<div class="col-md-6">
										  <select name='contrato_id' rows='5' id='contrato_id' class='select2 ' required  ></select> 
										 </div> 
										 <div class="col-md-2">
										 	
										 </div>
									  </div> 					
									  <div class="form-group  " >
										<label for="Subambito" class=" control-label col-md-4 text-left"> Subambito <span class="asterix"> * </span></label>
										<div class="col-md-6">
										  <select name='subambito_id' rows='5' id='subambito_id' class='select2 ' required  ></select> 
										 </div> 
										 <div class="col-md-2">
										 	
										 </div>
									  </div> 					
									  <div class="form-group  " >
										<label for="Tipo Alerta" class=" control-label col-md-4 text-left"> Tipo Alerta <span class="asterix"> * </span></label>
										<div class="col-md-6">
										  
					<?php $tipo_alerta = explode(',',$row['tipo_alerta']);
					$tipo_alerta_opt = array( '1' => 'Indicadores' , ); ?>
					<select name='tipo_alerta' rows='5' required  class='select2 '  > 
						<?php 
						foreach($tipo_alerta_opt as $key=>$val)
						{
							echo "<option  value ='$key' ".($row['tipo_alerta'] == $key ? " selected='selected' " : '' ).">$val</option>"; 						
						}						
						?></select> 
										 </div> 
										 <div class="col-md-2">
										 	
										 </div>
									  </div> 					
									  <div class="form-group  " >
										<label for="Mensaje" class=" control-label col-md-4 text-left"> Mensaje <span class="asterix"> * </span></label>
										<div class="col-md-6">
										  <select name='id_mensaje' rows='5' id='id_mensaje' class='select2 ' required  ></select> 
										 </div> 
										 <div class="col-md-2">
										 	
										 </div>
									  </div> 					
									  <div class="form-group  " >
										<label for="Fecha Inicio Alerta" class=" control-label col-md-4 text-left"> Fecha Inicio Alerta <span class="asterix"> * </span></label>
										<div class="col-md-6">
										  
				<div class="input-group m-b" style="width:150px !important;">
					{!! Form::text('fecha_ini', $row['fecha_ini'],array('class'=>'form-control date')) !!}
					<span class="input-group-addon"><i class="fa fa-calendar"></i></span>
				</div> 
										 </div> 
										 <div class="col-md-2">
										 	
										 </div>
									  </div> 					
									  <div class="form-group  " >
										<label for="Fecha Fin Alerta" class=" control-label col-md-4 text-left"> Fecha Fin Alerta <span class="asterix"> * </span></label>
										<div class="col-md-6">
										  
				<div class="input-group m-b" style="width:150px !important;">
					{!! Form::text('fecha_fin', $row['fecha_fin'],array('class'=>'form-control date')) !!}
					<span class="input-group-addon"><i class="fa fa-calendar"></i></span>
				</div> 
										 </div> 
										 <div class="col-md-2">
										 	
										 </div>
									  </div> 					
									  <div class="form-group  " >
										<label for="Estado Alerta" class=" control-label col-md-4 text-left"> Estado Alerta <span class="asterix"> * </span></label>
										<div class="col-md-6">
										  
					<?php $alerta_activa = explode(',',$row['alerta_activa']);
					$alerta_activa_opt = array( '0' => 'Desactivada' ,  '1' => 'Activada' , ); ?>
					<select name='alerta_activa' rows='5' required  class='select2 '  > 
						<?php 
						foreach($alerta_activa_opt as $key=>$val)
						{
							echo "<option  value ='$key' ".($row['alerta_activa'] == $key ? " selected='selected' " : '' ).">$val</option>"; 						
						}						
						?></select> 
										 </div> 
										 <div class="col-md-2">
										 	
										 </div>
									  </div> 					
									  <div class="form-group  " >
										<label for="Tipo Programación" class=" control-label col-md-4 text-left"> Tipo Programación <span class="asterix"> * </span></label>
										<div class="col-md-6">
										  
					<?php $tipo_prog = explode(',',$row['tipo_prog']);
					$tipo_prog_opt = array( 'MANUAL' => 'Manual' , ); ?>
					<select name='tipo_prog' rows='5' required  class='select2 '  > 
						<?php 
						foreach($tipo_prog_opt as $key=>$val)
						{
							echo "<option  value ='$key' ".($row['tipo_prog'] == $key ? " selected='selected' " : '' ).">$val</option>"; 						
						}						
						?></select> 
										 </div> 
										 <div class="col-md-2">
										 	
										 </div>
									  </div> 					
									  <div class="form-group  " >
										<label for="Criticidad de Alerta" class=" control-label col-md-4 text-left"> Criticidad de Alerta <span class="asterix"> * </span></label>
										<div class="col-md-6">
										  <select name='id_crit' rows='5' id='id_crit' class='select2 ' required  ></select> 
										 </div> 
										 <div class="col-md-2">
										 	
										 </div>
									  </div> </fieldset>
			</div>
			
			

			<div style="clear:both"></div>	
				
					
				  <div class="form-group">
					<label class="col-sm-4 text-right">&nbsp;</label>
					<div class="col-sm-8">	
					<button type="submit" name="apply" class="btn btn-info btn-sm" ><i class="fa  fa-check-circle"></i> {{ Lang::get('core.sb_apply') }}</button>
					<button type="submit" name="submit" class="btn btn-primary btn-sm" ><i class="fa  fa-save "></i> {{ Lang::get('core.sb_save') }}</button>
				  </div>	  
			
		</div> 
		 
		 {!! Form::close() !!}
		 
   <script type="text/javascript">
	$(document).ready(function() { 
		
		
		$("#contrato_id").jCombo("{!! url('ldalertsindcontroller/comboselect?filter=tbl_contrato:contrato_id:cont_numero|cont_proveedor') !!}",
		{  selected_value : '{{ $row["contrato_id"] }}' });
		
		$("#subambito_id").jCombo("{!! url('ldalertsindcontroller/comboselect?filter=tbl_planes_y_programas_ambitosub:subambito_id:ambito_nombre') !!}",
		{  selected_value : '{{ $row["subambito_id"] }}' });
		
		$("#id_mensaje").jCombo("{!! url('ldalertsindcontroller/comboselect?filter=tbl_alerta_msg:id_mensaje:texto_mensaje') !!}",
		{  selected_value : '{{ $row["id_mensaje"] }}' });
		
		$("#id_crit").jCombo("{!! url('ldalertsindcontroller/comboselect?filter=tbl_alerta_crit:id_crit:desc_crit') !!}",
		{  selected_value : '{{ $row["id_crit"] }}' });
		 

		$('.removeCurrentFiles').on('click',function(){
			var removeUrl = $(this).attr('href');
			$.get(removeUrl,function(response){});
			$(this).parent('div').empty();	
			return false;
		});		
		
	});
	</script>		 
