<div class="m-t" style="padding-top:25px;">	
    <div class="row m-b-lg animated fadeInDown delayp1 text-center">
        <h3> {{ $pageTitle }} <small> {{ $pageNote }} </small></h3>
        <hr />       
    </div>
</div>
<div class="m-t">
	<div class="table-responsive" > 	

		<table class="table table-striped table-bordered" >
			<tbody>	
		
			
					<tr>
						<td width='30%' class='label-view text-right'>{{ SiteHelpers::activeLang('Alertas Id', (isset($fields['alertas_id']['language'])? $fields['alertas_id']['language'] : array())) }}</td>
						<td>{{ $row->alertas_id}} </td>
						
					</tr>
				
					<tr>
						<td width='30%' class='label-view text-right'>{{ SiteHelpers::activeLang('Contrato Id', (isset($fields['contrato_id']['language'])? $fields['contrato_id']['language'] : array())) }}</td>
						<td>{{ $row->contrato_id}} </td>
						
					</tr>
				
					<tr>
						<td width='30%' class='label-view text-right'>{{ SiteHelpers::activeLang('Subambito Id', (isset($fields['subambito_id']['language'])? $fields['subambito_id']['language'] : array())) }}</td>
						<td>{{ $row->subambito_id}} </td>
						
					</tr>
				
					<tr>
						<td width='30%' class='label-view text-right'>{{ SiteHelpers::activeLang('Contratista Id', (isset($fields['contratista_id']['language'])? $fields['contratista_id']['language'] : array())) }}</td>
						<td>{{ $row->contratista_id}} </td>
						
					</tr>
				
					<tr>
						<td width='30%' class='label-view text-right'>{{ SiteHelpers::activeLang('Persona Id', (isset($fields['persona_id']['language'])? $fields['persona_id']['language'] : array())) }}</td>
						<td>{{ $row->persona_id}} </td>
						
					</tr>
				
					<tr>
						<td width='30%' class='label-view text-right'>{{ SiteHelpers::activeLang('Documento Id', (isset($fields['documento_id']['language'])? $fields['documento_id']['language'] : array())) }}</td>
						<td>{{ $row->documento_id}} </td>
						
					</tr>
				
					<tr>
						<td width='30%' class='label-view text-right'>{{ SiteHelpers::activeLang('Tipo Alerta', (isset($fields['tipo_alerta']['language'])? $fields['tipo_alerta']['language'] : array())) }}</td>
						<td>{{ $row->tipo_alerta}} </td>
						
					</tr>
				
					<tr>
						<td width='30%' class='label-view text-right'>{{ SiteHelpers::activeLang('Id Mensaje', (isset($fields['id_mensaje']['language'])? $fields['id_mensaje']['language'] : array())) }}</td>
						<td>{{ $row->id_mensaje}} </td>
						
					</tr>
				
					<tr>
						<td width='30%' class='label-view text-right'>{{ SiteHelpers::activeLang('Fecha Ini', (isset($fields['fecha_ini']['language'])? $fields['fecha_ini']['language'] : array())) }}</td>
						<td>{{ $row->fecha_ini}} </td>
						
					</tr>
				
					<tr>
						<td width='30%' class='label-view text-right'>{{ SiteHelpers::activeLang('Fecha Fin', (isset($fields['fecha_fin']['language'])? $fields['fecha_fin']['language'] : array())) }}</td>
						<td>{{ $row->fecha_fin}} </td>
						
					</tr>
				
					<tr>
						<td width='30%' class='label-view text-right'>{{ SiteHelpers::activeLang('Alerta Activa', (isset($fields['alerta_activa']['language'])? $fields['alerta_activa']['language'] : array())) }}</td>
						<td>{{ $row->alerta_activa}} </td>
						
					</tr>
				
					<tr>
						<td width='30%' class='label-view text-right'>{{ SiteHelpers::activeLang('Tipo Prog', (isset($fields['tipo_prog']['language'])? $fields['tipo_prog']['language'] : array())) }}</td>
						<td>{{ $row->tipo_prog}} </td>
						
					</tr>
				
					<tr>
						<td width='30%' class='label-view text-right'>{{ SiteHelpers::activeLang('Id Crit', (isset($fields['id_crit']['language'])? $fields['id_crit']['language'] : array())) }}</td>
						<td>{{ $row->id_crit}} </td>
						
					</tr>
				
					<tr>
						<td width='30%' class='label-view text-right'>{{ SiteHelpers::activeLang('Fec Ult Alerta', (isset($fields['fec_ult_alerta']['language'])? $fields['fec_ult_alerta']['language'] : array())) }}</td>
						<td>{{ $row->fec_ult_alerta}} </td>
						
					</tr>
						
					<tr>
						<td width='30%' class='label-view text-right'></td>
						<td> <a href="javascript:history.go(-1)" class="btn btn-primary"> Back To Grid <a> </td>
						
					</tr>					
				
			</tbody>	
		</table>   

	 
	
	</div>
</div>	