

		 {!! Form::open(array('url'=>'ldalertscontroller/savepublic', 'class'=>'form-horizontal','files' => true , 'parsley-validate'=>'','novalidate'=>' ')) !!}

	@if(Session::has('messagetext'))
	  
		   {!! Session::get('messagetext') !!}
	   
	@endif
	<ul class="parsley-error-list">
		@foreach($errors->all() as $error)
			<li>{{ $error }}</li>
		@endforeach
	</ul>		


<div class="col-md-12">
						<fieldset><legend> Alertas - Carga Manual Alertas</legend>
									
									  <div class="form-group  " >
										<label for="Alertas Id" class=" control-label col-md-4 text-left"> Alertas Id </label>
										<div class="col-md-6">
										  {!! Form::text('alertas_id', $row['alertas_id'],array('class'=>'form-control', 'placeholder'=>'',   )) !!} 
										 </div> 
										 <div class="col-md-2">
										 	
										 </div>
									  </div> 					
									  <div class="form-group  " >
										<label for="Contrato Id" class=" control-label col-md-4 text-left"> Contrato Id </label>
										<div class="col-md-6">
										  {!! Form::text('contrato_id', $row['contrato_id'],array('class'=>'form-control', 'placeholder'=>'',   )) !!} 
										 </div> 
										 <div class="col-md-2">
										 	
										 </div>
									  </div> 					
									  <div class="form-group  " >
										<label for="Subambito Id" class=" control-label col-md-4 text-left"> Subambito Id </label>
										<div class="col-md-6">
										  {!! Form::text('subambito_id', $row['subambito_id'],array('class'=>'form-control', 'placeholder'=>'',   )) !!} 
										 </div> 
										 <div class="col-md-2">
										 	
										 </div>
									  </div> 					
									  <div class="form-group  " >
										<label for="Contratista Id" class=" control-label col-md-4 text-left"> Contratista Id </label>
										<div class="col-md-6">
										  {!! Form::text('contratista_id', $row['contratista_id'],array('class'=>'form-control', 'placeholder'=>'',   )) !!} 
										 </div> 
										 <div class="col-md-2">
										 	
										 </div>
									  </div> 					
									  <div class="form-group  " >
										<label for="Persona Id" class=" control-label col-md-4 text-left"> Persona Id </label>
										<div class="col-md-6">
										  {!! Form::text('persona_id', $row['persona_id'],array('class'=>'form-control', 'placeholder'=>'',   )) !!} 
										 </div> 
										 <div class="col-md-2">
										 	
										 </div>
									  </div> 					
									  <div class="form-group  " >
										<label for="Documento Id" class=" control-label col-md-4 text-left"> Documento Id </label>
										<div class="col-md-6">
										  {!! Form::text('documento_id', $row['documento_id'],array('class'=>'form-control', 'placeholder'=>'',   )) !!} 
										 </div> 
										 <div class="col-md-2">
										 	
										 </div>
									  </div> 					
									  <div class="form-group  " >
										<label for="Tipo Alerta" class=" control-label col-md-4 text-left"> Tipo Alerta </label>
										<div class="col-md-6">
										  {!! Form::text('tipo_alerta', $row['tipo_alerta'],array('class'=>'form-control', 'placeholder'=>'',   )) !!} 
										 </div> 
										 <div class="col-md-2">
										 	
										 </div>
									  </div> 					
									  <div class="form-group  " >
										<label for="Id Mensaje" class=" control-label col-md-4 text-left"> Id Mensaje </label>
										<div class="col-md-6">
										  {!! Form::text('id_mensaje', $row['id_mensaje'],array('class'=>'form-control', 'placeholder'=>'',   )) !!} 
										 </div> 
										 <div class="col-md-2">
										 	
										 </div>
									  </div> 					
									  <div class="form-group  " >
										<label for="Fecha Ini" class=" control-label col-md-4 text-left"> Fecha Ini </label>
										<div class="col-md-6">
										  {!! Form::text('fecha_ini', $row['fecha_ini'],array('class'=>'form-control', 'placeholder'=>'',   )) !!} 
										 </div> 
										 <div class="col-md-2">
										 	
										 </div>
									  </div> 					
									  <div class="form-group  " >
										<label for="Fecha Fin" class=" control-label col-md-4 text-left"> Fecha Fin </label>
										<div class="col-md-6">
										  {!! Form::text('fecha_fin', $row['fecha_fin'],array('class'=>'form-control', 'placeholder'=>'',   )) !!} 
										 </div> 
										 <div class="col-md-2">
										 	
										 </div>
									  </div> 					
									  <div class="form-group  " >
										<label for="Alerta Activa" class=" control-label col-md-4 text-left"> Alerta Activa </label>
										<div class="col-md-6">
										  {!! Form::text('alerta_activa', $row['alerta_activa'],array('class'=>'form-control', 'placeholder'=>'',   )) !!} 
										 </div> 
										 <div class="col-md-2">
										 	
										 </div>
									  </div> 					
									  <div class="form-group  " >
										<label for="Tipo Prog" class=" control-label col-md-4 text-left"> Tipo Prog </label>
										<div class="col-md-6">
										  {!! Form::text('tipo_prog', $row['tipo_prog'],array('class'=>'form-control', 'placeholder'=>'',   )) !!} 
										 </div> 
										 <div class="col-md-2">
										 	
										 </div>
									  </div> 					
									  <div class="form-group  " >
										<label for="Id Crit" class=" control-label col-md-4 text-left"> Id Crit </label>
										<div class="col-md-6">
										  {!! Form::text('id_crit', $row['id_crit'],array('class'=>'form-control', 'placeholder'=>'',   )) !!} 
										 </div> 
										 <div class="col-md-2">
										 	
										 </div>
									  </div> 					
									  <div class="form-group  " >
										<label for="Fec Ult Alerta" class=" control-label col-md-4 text-left"> Fec Ult Alerta </label>
										<div class="col-md-6">
										  {!! Form::text('fec_ult_alerta', $row['fec_ult_alerta'],array('class'=>'form-control', 'placeholder'=>'',   )) !!} 
										 </div> 
										 <div class="col-md-2">
										 	
										 </div>
									  </div> </fieldset>
			</div>
			
			

			<div style="clear:both"></div>	
				
					
				  <div class="form-group">
					<label class="col-sm-4 text-right">&nbsp;</label>
					<div class="col-sm-8">	
					<button type="submit" name="apply" class="btn btn-info btn-sm" ><i class="fa  fa-check-circle"></i> {{ Lang::get('core.sb_apply') }}</button>
					<button type="submit" name="submit" class="btn btn-primary btn-sm" ><i class="fa  fa-save "></i> {{ Lang::get('core.sb_save') }}</button>
				  </div>	  
			
		</div> 
		 
		 {!! Form::close() !!}
		 
   <script type="text/javascript">
	$(document).ready(function() { 
		
		 

		$('.removeCurrentFiles').on('click',function(){
			var removeUrl = $(this).attr('href');
			$.get(removeUrl,function(response){});
			$(this).parent('div').empty();	
			return false;
		});		
		
	});
	</script>		 
