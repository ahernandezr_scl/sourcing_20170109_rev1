

		 {!! Form::open(array('url'=>'contratoitem/savepublic', 'class'=>'form-horizontal','files' => true , 'parsley-validate'=>'','novalidate'=>' ')) !!}

	@if(Session::has('messagetext'))
	  
		   {!! Session::get('messagetext') !!}
	   
	@endif
	<ul class="parsley-error-list">
		@foreach($errors->all() as $error)
			<li>{{ $error }}</li>
		@endforeach
	</ul>		


<div class="col-md-12">
						<fieldset><legend> contratoitem</legend>
				{!! Form::hidden('id_cont_items', $row['id_cont_items']) !!}{!! Form::hidden('id_contrato', $row['id_contrato']) !!}					
									  <div class="form-group  " >
										<label for="Cantidad" class=" control-label col-md-4 text-left"> Cantidad <span class="asterix"> * </span></label>
										<div class="col-md-6">
										  {!! Form::text('cantidad', $row['cantidad'],array('class'=>'form-control', 'placeholder'=>'', 'required'=>'true', 'parsley-type'=>'number'   )) !!} 
										 </div> 
										 <div class="col-md-2">
										 	
										 </div>
									  </div> 					
									  <div class="form-group  " >
										<label for="Servicio" class=" control-label col-md-4 text-left"> Servicio <span class="asterix"> * </span></label>
										<div class="col-md-6">
										  <select name='servicio_id' rows='5' id='servicio_id' class='select2 ' required  ></select> 
										 </div> 
										 <div class="col-md-2">
										 	
										 </div>
									  </div> 					
									  <div class="form-group  " >
										<label for="Precio Unitario" class=" control-label col-md-4 text-left"> Precio Unitario <span class="asterix"> * </span></label>
										<div class="col-md-6">
										  {!! Form::text('precio_unitario', $row['precio_unitario'],array('class'=>'form-control', 'placeholder'=>'', 'required'=>'true', 'parsley-type'=>'number'   )) !!} 
										 </div> 
										 <div class="col-md-2">
										 	
										 </div>
									  </div> 					
									  <div class="form-group  " >
										<label for="Precio Item" class=" control-label col-md-4 text-left"> Precio Item <span class="asterix"> * </span></label>
										<div class="col-md-6">
										  {!! Form::text('precio_item', $row['precio_item'],array('class'=>'form-control', 'placeholder'=>'', 'required'=>'true', 'parsley-type'=>'number'   )) !!} 
										 </div> 
										 <div class="col-md-2">
										 	
										 </div>
									  </div> 					
									  <div class="form-group  " >
										<label for="Estado" class=" control-label col-md-4 text-left"> Estado </label>
										<div class="col-md-6">
										  
					<?php $estado_id = explode(',',$row['estado_id']);
					$estado_id_opt = array( 'ACTIVO' => 'ACTIVO' ,  'BLOQUEADO' => 'BLOQUEADO' , ); ?>
					<select name='estado_id' rows='5'   class='select2 '  > 
						<?php 
						foreach($estado_id_opt as $key=>$val)
						{
							echo "<option  value ='$key' ".($row['estado_id'] == $key ? " selected='selected' " : '' ).">$val</option>"; 						
						}						
						?></select> 
										 </div> 
										 <div class="col-md-2">
										 	
										 </div>
									  </div> </fieldset>
			</div>
			
			

			<div style="clear:both"></div>	
				
					
				  <div class="form-group">
					<label class="col-sm-4 text-right">&nbsp;</label>
					<div class="col-sm-8">	
					<button type="submit" name="apply" class="btn btn-info btn-sm" ><i class="fa  fa-check-circle"></i> {{ Lang::get('core.sb_apply') }}</button>
					<button type="submit" name="submit" class="btn btn-primary btn-sm" ><i class="fa  fa-save "></i> {{ Lang::get('core.sb_save') }}</button>
				  </div>	  
			
		</div> 
		 
		 {!! Form::close() !!}
		 
   <script type="text/javascript">
	$(document).ready(function() { 
		
		
		$("#servicio_id").jCombo("{!! url('contratoitem/comboselect?filter=sb_services:id_service:code|name') !!}",
		{  selected_value : '{{ $row["servicio_id"] }}' });
		 

		$('.removeCurrentFiles').on('click',function(){
			var removeUrl = $(this).attr('href');
			$.get(removeUrl,function(response){});
			$(this).parent('div').empty();	
			return false;
		});		
		
	});
	</script>		 
