<div class="m-t" style="padding-top:25px;">	
    <div class="row m-b-lg animated fadeInDown delayp1 text-center">
        <h3> {{ $pageTitle }} <small> {{ $pageNote }} </small></h3>
        <hr />       
    </div>
</div>
<div class="m-t">
	<div class="table-responsive" > 	

		<table class="table table-striped table-bordered" >
			<tbody>	
		
			
					<tr>
						<td width='30%' class='label-view text-right'>{{ SiteHelpers::activeLang('Cantidad', (isset($fields['cantidad']['language'])? $fields['cantidad']['language'] : array())) }}</td>
						<td>{{ $row->cantidad}} </td>
						
					</tr>
				
					<tr>
						<td width='30%' class='label-view text-right'>{{ SiteHelpers::activeLang('Precio Unitario', (isset($fields['precio_unitario']['language'])? $fields['precio_unitario']['language'] : array())) }}</td>
						<td>{{ $row->precio_unitario}} </td>
						
					</tr>
				
					<tr>
						<td width='30%' class='label-view text-right'>{{ SiteHelpers::activeLang('Precio Item', (isset($fields['precio_item']['language'])? $fields['precio_item']['language'] : array())) }}</td>
						<td>{{ $row->precio_item}} </td>
						
					</tr>
				
					<tr>
						<td width='30%' class='label-view text-right'>{{ SiteHelpers::activeLang('Servicio', (isset($fields['servicio_id']['language'])? $fields['servicio_id']['language'] : array())) }}</td>
						<td>{{ $row->servicio_id}} </td>
						
					</tr>
				
					<tr>
						<td width='30%' class='label-view text-right'>{{ SiteHelpers::activeLang('Estado', (isset($fields['estado_id']['language'])? $fields['estado_id']['language'] : array())) }}</td>
						<td>{{ $row->estado_id}} </td>
						
					</tr>
						
					<tr>
						<td width='30%' class='label-view text-right'></td>
						<td> <a href="javascript:history.go(-1)" class="btn btn-primary"> Back To Grid <a> </td>
						
					</tr>					
				
			</tbody>	
		</table>   

	 
	
	</div>
</div>	