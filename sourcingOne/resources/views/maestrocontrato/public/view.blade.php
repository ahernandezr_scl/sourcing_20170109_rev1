<div class="m-t" style="padding-top:25px;">	
    <div class="row m-b-lg animated fadeInDown delayp1 text-center">
        <h3> {{ $pageTitle }} <small> {{ $pageNote }} </small></h3>
        <hr />       
    </div>
</div>
<div class="m-t">
	<div class="table-responsive" > 	

		<table class="table table-striped table-bordered" >
			<tbody>	
		
			
					<tr>
						<td width='30%' class='label-view text-right'>{{ SiteHelpers::activeLang('Nro Contrato', (isset($fields['nro_contrato']['language'])? $fields['nro_contrato']['language'] : array())) }}</td>
						<td>{{ $row->nro_contrato}} </td>
						
					</tr>
				
					<tr>
						<td width='30%' class='label-view text-right'>{{ SiteHelpers::activeLang('Nombre Contrato', (isset($fields['nombre_contrato']['language'])? $fields['nombre_contrato']['language'] : array())) }}</td>
						<td>{{ $row->nombre_contrato}} </td>
						
					</tr>
				
					<tr>
						<td width='30%' class='label-view text-right'>{{ SiteHelpers::activeLang('Contratista Id', (isset($fields['contratista_id']['language'])? $fields['contratista_id']['language'] : array())) }}</td>
						<td>{{ $row->contratista_id}} </td>
						
					</tr>
				
					<tr>
						<td width='30%' class='label-view text-right'>{{ SiteHelpers::activeLang('Mandante Id', (isset($fields['mandante_id']['language'])? $fields['mandante_id']['language'] : array())) }}</td>
						<td>{{ $row->mandante_id}} </td>
						
					</tr>
				
					<tr>
						<td width='30%' class='label-view text-right'>{{ SiteHelpers::activeLang('Nro Sap', (isset($fields['nro_sap']['language'])? $fields['nro_sap']['language'] : array())) }}</td>
						<td>{{ $row->nro_sap}} </td>
						
					</tr>
				
					<tr>
						<td width='30%' class='label-view text-right'>{{ SiteHelpers::activeLang('Tiposervicio Id', (isset($fields['tiposervicio_id']['language'])? $fields['tiposervicio_id']['language'] : array())) }}</td>
						<td>{{ $row->tiposervicio_id}} </td>
						
					</tr>
				
					<tr>
						<td width='30%' class='label-view text-right'>{{ SiteHelpers::activeLang('Tipolibro Id', (isset($fields['tipolibro_id']['language'])? $fields['tipolibro_id']['language'] : array())) }}</td>
						<td>{{ $row->tipolibro_id}} </td>
						
					</tr>
				
					<tr>
						<td width='30%' class='label-view text-right'>{{ SiteHelpers::activeLang('Estado Id', (isset($fields['estado_id']['language'])? $fields['estado_id']['language'] : array())) }}</td>
						<td>{{ $row->estado_id}} </td>
						
					</tr>
				
					<tr>
						<td width='30%' class='label-view text-right'>{{ SiteHelpers::activeLang('Subestado Id', (isset($fields['subestado_id']['language'])? $fields['subestado_id']['language'] : array())) }}</td>
						<td>{{ $row->subestado_id}} </td>
						
					</tr>
				
					<tr>
						<td width='30%' class='label-view text-right'>{{ SiteHelpers::activeLang('Suscripcion', (isset($fields['suscripcion']['language'])? $fields['suscripcion']['language'] : array())) }}</td>
						<td>{{ $row->suscripcion}} </td>
						
					</tr>
				
					<tr>
						<td width='30%' class='label-view text-right'>{{ SiteHelpers::activeLang('Inicio', (isset($fields['inicio']['language'])? $fields['inicio']['language'] : array())) }}</td>
						<td>{{ $row->inicio}} </td>
						
					</tr>
				
					<tr>
						<td width='30%' class='label-view text-right'>{{ SiteHelpers::activeLang('Termino', (isset($fields['termino']['language'])? $fields['termino']['language'] : array())) }}</td>
						<td>{{ $row->termino}} </td>
						
					</tr>
				
					<tr>
						<td width='30%' class='label-view text-right'>{{ SiteHelpers::activeLang('Monto Inicio', (isset($fields['monto_inicio']['language'])? $fields['monto_inicio']['language'] : array())) }}</td>
						<td>{{ $row->monto_inicio}} </td>
						
					</tr>
				
					<tr>
						<td width='30%' class='label-view text-right'>{{ SiteHelpers::activeLang('Monedainicio Id', (isset($fields['monedainicio_id']['language'])? $fields['monedainicio_id']['language'] : array())) }}</td>
						<td>{{ $row->monedainicio_id}} </td>
						
					</tr>
				
					<tr>
						<td width='30%' class='label-view text-right'>{{ SiteHelpers::activeLang('Monto Actualizado', (isset($fields['monto_actualizado']['language'])? $fields['monto_actualizado']['language'] : array())) }}</td>
						<td>{{ $row->monto_actualizado}} </td>
						
					</tr>
				
					<tr>
						<td width='30%' class='label-view text-right'>{{ SiteHelpers::activeLang('Monedaact Id', (isset($fields['monedaact_id']['language'])? $fields['monedaact_id']['language'] : array())) }}</td>
						<td>{{ $row->monedaact_id}} </td>
						
					</tr>
				
					<tr>
						<td width='30%' class='label-view text-right'>{{ SiteHelpers::activeLang('Empresa Id', (isset($fields['empresa_id']['language'])? $fields['empresa_id']['language'] : array())) }}</td>
						<td>{{ $row->empresa_id}} </td>
						
					</tr>
				
					<tr>
						<td width='30%' class='label-view text-right'>{{ SiteHelpers::activeLang('Rut', (isset($fields['rut']['language'])? $fields['rut']['language'] : array())) }}</td>
						<td>{{ $row->rut}} </td>
						
					</tr>
				
					<tr>
						<td width='30%' class='label-view text-right'>{{ SiteHelpers::activeLang('Razon Social', (isset($fields['razon_social']['language'])? $fields['razon_social']['language'] : array())) }}</td>
						<td>{{ $row->razon_social}} </td>
						
					</tr>
						
					<tr>
						<td width='30%' class='label-view text-right'></td>
						<td> <a href="javascript:history.go(-1)" class="btn btn-primary"> Back To Grid <a> </td>
						
					</tr>					
				
			</tbody>	
		</table>   

	 
	
	</div>
</div>	