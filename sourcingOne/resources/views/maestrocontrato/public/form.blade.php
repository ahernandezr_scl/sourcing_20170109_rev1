

		 {!! Form::open(array('url'=>'maestrocontrato/savepublic', 'class'=>'form-horizontal','files' => true , 'parsley-validate'=>'','novalidate'=>' ')) !!}

	@if(Session::has('messagetext'))
	  
		   {!! Session::get('messagetext') !!}
	   
	@endif
	<ul class="parsley-error-list">
		@foreach($errors->all() as $error)
			<li>{{ $error }}</li>
		@endforeach
	</ul>		


<div class="col-md-6">
						<fieldset><legend> Maestro Contrato</legend>
				{!! Form::hidden('id_contrato', $row['id_contrato']) !!}					
									  <div class="form-group  " >
										<label for="Nro Contrato" class=" control-label col-md-4 text-left"> Nro Contrato <span class="asterix"> * </span></label>
										<div class="col-md-6">
										  {!! Form::text('nro_contrato', $row['nro_contrato'],array('class'=>'form-control', 'placeholder'=>'',   )) !!} 
										 </div> 
										 <div class="col-md-2">
										 	
										 </div>
									  </div> 					
									  <div class="form-group  " >
										<label for="Nombre Contrato" class=" control-label col-md-4 text-left"> Nombre Contrato <span class="asterix"> * </span></label>
										<div class="col-md-6">
										  {!! Form::text('nombre_contrato', $row['nombre_contrato'],array('class'=>'form-control', 'placeholder'=>'', 'required'=>'true'  )) !!} 
										 </div> 
										 <div class="col-md-2">
										 	
										 </div>
									  </div> 					
									  <div class="form-group  " >
										<label for="Nro Sap" class=" control-label col-md-4 text-left"> Nro Sap </label>
										<div class="col-md-6">
										  {!! Form::text('nro_sap', $row['nro_sap'],array('class'=>'form-control', 'placeholder'=>'',   )) !!} 
										 </div> 
										 <div class="col-md-2">
										 	
										 </div>
									  </div> 					
									  <div class="form-group  " >
										<label for="Tipo Servicio" class=" control-label col-md-4 text-left"> Tipo Servicio <span class="asterix"> * </span></label>
										<div class="col-md-6">
										  
					<?php $tiposervicio_id = explode(',',$row['tiposervicio_id']);
					$tiposervicio_id_opt = array( 'Servicio 1' => 'Servicio 1' ,  'Servicio 2' => 'Servicio 2' ,  'Servicio 3' => 'Servicio 3' , ); ?>
					<select name='tiposervicio_id' rows='5' required  class='select2 '  > 
						<?php 
						foreach($tiposervicio_id_opt as $key=>$val)
						{
							echo "<option  value ='$key' ".($row['tiposervicio_id'] == $key ? " selected='selected' " : '' ).">$val</option>"; 						
						}						
						?></select> 
										 </div> 
										 <div class="col-md-2">
										 	
										 </div>
									  </div> 					
									  <div class="form-group  " >
										<label for="Tipo Libro" class=" control-label col-md-4 text-left"> Tipo Libro <span class="asterix"> * </span></label>
										<div class="col-md-6">
										  
					<?php $tipolibro_id = explode(',',$row['tipolibro_id']);
					$tipolibro_id_opt = array( 'libro1' => 'Libro 1' ,  'libro2' => 'Libro 2' ,  'libro3' => 'Libro 3' , ); ?>
					<select name='tipolibro_id' rows='5' required  class='select2 '  > 
						<?php 
						foreach($tipolibro_id_opt as $key=>$val)
						{
							echo "<option  value ='$key' ".($row['tipolibro_id'] == $key ? " selected='selected' " : '' ).">$val</option>"; 						
						}						
						?></select> 
										 </div> 
										 <div class="col-md-2">
										 	
										 </div>
									  </div> 					
									  <div class="form-group  " >
										<label for="Estado" class=" control-label col-md-4 text-left"> Estado <span class="asterix"> * </span></label>
										<div class="col-md-6">
										  
					<?php $estado_id = explode(',',$row['estado_id']);
					$estado_id_opt = array( 'Vigente' => 'Vigente' ,  'Terminado en Plazo' => 'Terminado en Plazo' ,  'Terminado en Monto' => 'Terminado en Monto' ,  'Finiquitado' => 'Finiquitado' ,  'No Vigente' => 'No Vigente' , ); ?>
					<select name='estado_id' rows='5' required  class='select2 '  > 
						<?php 
						foreach($estado_id_opt as $key=>$val)
						{
							echo "<option  value ='$key' ".($row['estado_id'] == $key ? " selected='selected' " : '' ).">$val</option>"; 						
						}						
						?></select> 
										 </div> 
										 <div class="col-md-2">
										 	
										 </div>
									  </div> 					
									  <div class="form-group  " >
										<label for="SubEstado" class=" control-label col-md-4 text-left"> SubEstado <span class="asterix"> * </span></label>
										<div class="col-md-6">
										  
					<?php $subestado_id = explode(',',$row['subestado_id']);
					$subestado_id_opt = array( 'Pre-activo' => 'Pre-activo' ,  'Activo full' => 'Activo full' ,  'Activo Parcial' => 'Activo Parcial' ,  'No activo' => 'No activo' , ); ?>
					<select name='subestado_id' rows='5' required  class='select2 '  > 
						<?php 
						foreach($subestado_id_opt as $key=>$val)
						{
							echo "<option  value ='$key' ".($row['subestado_id'] == $key ? " selected='selected' " : '' ).">$val</option>"; 						
						}						
						?></select> 
										 </div> 
										 <div class="col-md-2">
										 	
										 </div>
									  </div> 					
									  <div class="form-group  " >
										<label for="Contratista" class=" control-label col-md-4 text-left"> Contratista <span class="asterix"> * </span></label>
										<div class="col-md-6">
										  <select name='contratista_id' rows='5' id='contratista_id' class='select2 ' required  ></select> 
										 </div> 
										 <div class="col-md-2">
										 	
										 </div>
									  </div> 					
									  <div class="form-group  " >
										<label for="Mandante" class=" control-label col-md-4 text-left"> Mandante <span class="asterix"> * </span></label>
										<div class="col-md-6">
										  <select name='mandante_id' rows='5' id='mandante_id' class='select2 ' required  ></select> 
										 </div> 
										 <div class="col-md-2">
										 	
										 </div>
									  </div> </fieldset>
			</div>
			
			<div class="col-md-6">
						<fieldset><legend> Detalle</legend>
									
									  <div class="form-group  " >
										<label for="Empresa" class=" control-label col-md-4 text-left"> Empresa <span class="asterix"> * </span></label>
										<div class="col-md-6">
										  <select name='empresa_id' rows='5' id='empresa_id' class='select2 ' required  ></select> 
										 </div> 
										 <div class="col-md-2">
										 	
										 </div>
									  </div> 					
									  <div class="form-group  " >
										<label for="Rut" class=" control-label col-md-4 text-left"> Rut </label>
										<div class="col-md-6">
										  {!! Form::text('rut', $row['rut'],array('class'=>'form-control', 'placeholder'=>'',   )) !!} 
										 </div> 
										 <div class="col-md-2">
										 	
										 </div>
									  </div> 					
									  <div class="form-group  " >
										<label for="Razon Social" class=" control-label col-md-4 text-left"> Razon Social </label>
										<div class="col-md-6">
										  {!! Form::text('razon_social', $row['razon_social'],array('class'=>'form-control', 'placeholder'=>'',   )) !!} 
										 </div> 
										 <div class="col-md-2">
										 	
										 </div>
									  </div> 					
									  <div class="form-group  " >
										<label for="Suscripcion" class=" control-label col-md-4 text-left"> Suscripcion </label>
										<div class="col-md-6">
										  
				<div class="input-group m-b" style="width:150px !important;">
					{!! Form::text('suscripcion', $row['suscripcion'],array('class'=>'form-control date')) !!}
					<span class="input-group-addon"><i class="fa fa-calendar"></i></span>
				</div> 
										 </div> 
										 <div class="col-md-2">
										 	
										 </div>
									  </div> 					
									  <div class="form-group  " >
										<label for="Inicio" class=" control-label col-md-4 text-left"> Inicio </label>
										<div class="col-md-6">
										  
				<div class="input-group m-b" style="width:150px !important;">
					{!! Form::text('inicio', $row['inicio'],array('class'=>'form-control date')) !!}
					<span class="input-group-addon"><i class="fa fa-calendar"></i></span>
				</div> 
										 </div> 
										 <div class="col-md-2">
										 	
										 </div>
									  </div> 					
									  <div class="form-group  " >
										<label for="Termino" class=" control-label col-md-4 text-left"> Termino </label>
										<div class="col-md-6">
										  
				<div class="input-group m-b" style="width:150px !important;">
					{!! Form::text('termino', $row['termino'],array('class'=>'form-control date')) !!}
					<span class="input-group-addon"><i class="fa fa-calendar"></i></span>
				</div> 
										 </div> 
										 <div class="col-md-2">
										 	
										 </div>
									  </div> 					
									  <div class="form-group  " >
										<label for="Monto Inicio" class=" control-label col-md-4 text-left"> Monto Inicio </label>
										<div class="col-md-6">
										  {!! Form::text('monto_inicio', $row['monto_inicio'],array('class'=>'form-control', 'placeholder'=>'',   )) !!} 
										 </div> 
										 <div class="col-md-2">
										 	
										 </div>
									  </div> 					
									  <div class="form-group  " >
										<label for="Moneda" class=" control-label col-md-4 text-left"> Moneda </label>
										<div class="col-md-6">
										  
					<?php $monedainicio_id = explode(',',$row['monedainicio_id']);
					$monedainicio_id_opt = array( 'dolar' => 'Dolar' ,  'cls' => 'Pesos Chilenos' ,  'uf' => 'UF' , ); ?>
					<select name='monedainicio_id' rows='5'   class='select2 '  > 
						<?php 
						foreach($monedainicio_id_opt as $key=>$val)
						{
							echo "<option  value ='$key' ".($row['monedainicio_id'] == $key ? " selected='selected' " : '' ).">$val</option>"; 						
						}						
						?></select> 
										 </div> 
										 <div class="col-md-2">
										 	
										 </div>
									  </div> 					
									  <div class="form-group  " >
										<label for="Monto Actualizado" class=" control-label col-md-4 text-left"> Monto Actualizado </label>
										<div class="col-md-6">
										  {!! Form::text('monto_actualizado', $row['monto_actualizado'],array('class'=>'form-control', 'placeholder'=>'',   )) !!} 
										 </div> 
										 <div class="col-md-2">
										 	
										 </div>
									  </div> 					
									  <div class="form-group  " >
										<label for="Moneda" class=" control-label col-md-4 text-left"> Moneda </label>
										<div class="col-md-6">
										  
					<?php $monedaact_id = explode(',',$row['monedaact_id']);
					$monedaact_id_opt = array( 'dolar' => 'Dolar' ,  'cls' => 'Pesos Chilenos' ,  'uf' => 'UF' , ); ?>
					<select name='monedaact_id' rows='5'   class='select2 '  > 
						<?php 
						foreach($monedaact_id_opt as $key=>$val)
						{
							echo "<option  value ='$key' ".($row['monedaact_id'] == $key ? " selected='selected' " : '' ).">$val</option>"; 						
						}						
						?></select> 
										 </div> 
										 <div class="col-md-2">
										 	
										 </div>
									  </div> </fieldset>
			</div>
			
			

			<div style="clear:both"></div>	
				
					
				  <div class="form-group">
					<label class="col-sm-4 text-right">&nbsp;</label>
					<div class="col-sm-8">	
					<button type="submit" name="apply" class="btn btn-info btn-sm" ><i class="fa  fa-check-circle"></i> {{ Lang::get('core.sb_apply') }}</button>
					<button type="submit" name="submit" class="btn btn-primary btn-sm" ><i class="fa  fa-save "></i> {{ Lang::get('core.sb_save') }}</button>
				  </div>	  
			
		</div> 
		 
		 {!! Form::close() !!}
		 
   <script type="text/javascript">
	$(document).ready(function() { 
		$('.addC').relCopy({});
		
		$("#contratista_id").jCombo("{!! url('maestrocontrato/comboselect?filter=tb_users:id:last_name|first_name') !!}",
		{  selected_value : '{{ $row["contratista_id"] }}' });
		
		$("#mandante_id").jCombo("{!! url('maestrocontrato/comboselect?filter=tb_users:id:last_name|first_name') !!}",
		{  selected_value : '{{ $row["mandante_id"] }}' });
		
		$("#empresa_id").jCombo("{!! url('maestrocontrato/comboselect?filter=sb_empresa:id_empresa:rut|nombre') !!}",
		{  selected_value : '{{ $row["empresa_id"] }}' });
		 

		$('.removeCurrentFiles').on('click',function(){
			var removeUrl = $(this).attr('href');
			$.get(removeUrl,function(response){});
			$(this).parent('div').empty();	
			return false;
		});		
		
	});
	</script>		 
