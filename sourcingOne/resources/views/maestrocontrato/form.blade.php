@extends('layouts.app')

@section('content')

  <div class="page-content row">
    <!-- Page header -->

 
 	<div class="page-content-wrapper m-t">


<div class="sbox">
	<div class="sbox-title"> <h3> {{ $pageTitle }} <small>{{ $pageNote }}</small></h3>
		<div class="sbox-tools" >
			<a href="{{ url($pageModule.'?return='.$return) }}" class="btn btn-xs btn-white tips"  title="{{ Lang::get('core.btn_back') }}" ><i class="icon-backward"></i> {{ Lang::get('core.btn_back') }} </a> 
		</div>
	</div>
	<div class="sbox-content"> 	

		<ul class="parsley-error-list">
			@foreach($errors->all() as $error)
				<li>{{ $error }}</li>
			@endforeach
		</ul>	

		
			  <!-- Nav tabs YOSUA-->
			  <ul class="nav nav-tabs" role="tablist">
			  	<li role="presentation" class="active"><a href="#form_entry" aria-controls="form_entry" role="tab" data-toggle="tab"><b>{{ $pageTitle }} : </b></a></li>
			  	 @if($row['id_contrato'] !== '')	<li role="presentation" ><a href="#masterdetailform" aria-controls="masterdetailform" role="tab" data-toggle="tab"><b> Detalle : </b></a></li>
			  	<li role="presentation" ><a href="#adjuntos" aria-controls="adjuntos" role="tab" data-toggle="tab"><b> Adjuntos : </b></a></li> @endif
			 </ul>

		 		{!! Form::open(array('url'=>'maestrocontrato/save?return='.$return, 'class'=>'form-horizontal','files' => true , 'parsley-validate'=>'','novalidate'=>' ')) !!}
 			<!-- Tab panes -->
  			<div class="tab-content m-t">
	  			<div role="tabpanel" class="tab-pane active" id="form_entry">

					
<div class="col-md-6">
						<fieldset><legend> Maestro Contrato</legend>
				{!! Form::hidden('id_contrato', $row['id_contrato']) !!}					
									  <div class="form-group  " >
										<label for="Nro Contrato" class=" control-label col-md-4 text-left"> Nro Contrato <span class="asterix"> * </span></label>
										<div class="col-md-6">
										  {!! Form::text('nro_contrato', $row['nro_contrato'],array('class'=>'form-control', 'placeholder'=>'',   )) !!} 
										 </div> 
										 <div class="col-md-2">
										 	
										 </div>
									  </div> 					
									  <div class="form-group  " >
										<label for="Nombre Contrato" class=" control-label col-md-4 text-left"> Nombre Contrato <span class="asterix"> * </span></label>
										<div class="col-md-6">
										  {!! Form::text('nombre_contrato', $row['nombre_contrato'],array('class'=>'form-control', 'placeholder'=>'', 'required'=>'true'  )) !!} 
										 </div> 
										 <div class="col-md-2">
										 	
										 </div>
									  </div> 					
									  <div class="form-group  " >
										<label for="Nro Sap" class=" control-label col-md-4 text-left"> Nro Sap </label>
										<div class="col-md-6">
										  {!! Form::text('nro_sap', $row['nro_sap'],array('class'=>'form-control', 'placeholder'=>'',   )) !!} 
										 </div> 
										 <div class="col-md-2">
										 	
										 </div>
									  </div> 					
									  <div class="form-group  " >
										<label for="Tipo Servicio" class=" control-label col-md-4 text-left"> Tipo Servicio <span class="asterix"> * </span></label>
										<div class="col-md-6">
										  
					<?php $tiposervicio_id = explode(',',$row['tiposervicio_id']);
					$tiposervicio_id_opt = array( 'Servicio 1' => 'Servicio 1' ,  'Servicio 2' => 'Servicio 2' ,  'Servicio 3' => 'Servicio 3' , ); ?>
					<select name='tiposervicio_id' rows='5' required  class='select2 '  > 
						<?php 
						foreach($tiposervicio_id_opt as $key=>$val)
						{
							echo "<option  value ='$key' ".($row['tiposervicio_id'] == $key ? " selected='selected' " : '' ).">$val</option>"; 						
						}						
						?></select> 
										 </div> 
										 <div class="col-md-2">
										 	
										 </div>
									  </div> 					
									  <div class="form-group  " >
										<label for="Estado" class=" control-label col-md-4 text-left"> Estado <span class="asterix"> * </span></label>
										<div class="col-md-6">
										  
					<?php $estado_id = explode(',',$row['estado_id']);
					$estado_id_opt = array( 'Vigente' => 'Vigente' ,  'Terminado en Plazo' => 'Terminado en Plazo' ,  'Terminado en Monto' => 'Terminado en Monto' ,  'Finiquitado' => 'Finiquitado' ,  'No Vigente' => 'No Vigente' , ); ?>
					<select name='estado_id' rows='5' required  class='select2 '  > 
						<?php 
						foreach($estado_id_opt as $key=>$val)
						{
							echo "<option  value ='$key' ".($row['estado_id'] == $key ? " selected='selected' " : '' ).">$val</option>"; 						
						}						
						?></select> 
										 </div> 
										 <div class="col-md-2">
										 	
										 </div>
									  </div> 					
									  <div class="form-group  " >
										<label for="SubEstado" class=" control-label col-md-4 text-left"> SubEstado <span class="asterix"> * </span></label>
										<div class="col-md-6">
										  
					<?php $subestado_id = explode(',',$row['subestado_id']);
					$subestado_id_opt = array( 'Pre-activo' => 'Pre-activo' ,  'Activo full' => 'Activo full' ,  'Activo Parcial' => 'Activo Parcial' ,  'No activo' => 'No activo' , ); ?>
					<select name='subestado_id' rows='5' required  class='select2 '  > 
						<?php 
						foreach($subestado_id_opt as $key=>$val)
						{
							echo "<option  value ='$key' ".($row['subestado_id'] == $key ? " selected='selected' " : '' ).">$val</option>"; 						
						}						
						?></select> 
										 </div> 
										 <div class="col-md-2">
										 	
										 </div>
									  </div> 					
									  <div class="form-group  " >
										<label for="AdC Contratista" class=" control-label col-md-4 text-left"> AdC Contratista <span class="asterix"> * </span></label>
										<div class="col-md-6">
										  <select name='contratista_id' rows='5' id='contratista_id' class='select2 ' required  ></select> 
										 </div> 
										 <div class="col-md-2">
										 	
										 </div>
									  </div> 					
									  <div class="form-group  " >
										<label for="AdC Mandante" class=" control-label col-md-4 text-left"> AdC Mandante <span class="asterix"> * </span></label>
										<div class="col-md-6">
										  <select name='mandante_id' rows='5' id='mandante_id' class='select2 ' required  ></select> 
										 </div> 
										 <div class="col-md-2">
										 	
										 </div>
									  </div> </fieldset>
					</div>
			
			<div class="col-md-6">
						<fieldset><legend> Detalle</legend>
									
									  <div class="form-group  " >
										<label for="Empresa" class=" control-label col-md-3 text-left"> Empresa <span class="asterix"> * </span></label>
										<div class="col-md-9">
										  <select name='empresa_id' rows='5' id='empresa_id' class='select2 ' required  onchange="empresaSelected(this)"></select> 
										 </div> 
										 <div class="col-md-2">
										 </div>
									  </div>
									  <div class="form-group" >
										<label for="Rut" class=" control-label col-md-1 text-left"> Rut:</label>
										<label class=" control-label col-md-2 text-left" id="rutC"></label>
										<label for="Razon Social" class=" control-label col-md-1 text-left"> Razon Social:</label>
										<label class=" control-label col-md-4 text-left" id="razonC"></label>
										<label for="Direccion" class=" control-label col-md-1 text-left"> Dir:</label>
										<label class=" control-label col-md-3 text-left" id="direccionC"></label>
										
									  </div>				
									  <div class="form-group" >
										
										<label for="Telefono" class=" control-label col-md-1 text-left"> Tel:</label>
										<label class=" control-label col-md-2 text-left" id="telC"></label>
										<label for="Email" class=" control-label col-md-1 text-left"> Email:</label>
										<label class=" control-label col-md-4 text-left" id="emailC"></label>
										<label for="Rep. Legal" class=" control-label col-md-1 text-left"> R.Legal:</label>
										<label class=" control-label col-md-3 text-left" id="replegal1C"></label>
									  </div>

									  <div class="form-group  " >
										<label for="Fecha de Suscripción del Contrato" class=" control-label col-md-4 text-left"> Fecha de Suscripción del Contrato </label>
										<div class="col-md-6">
										  
										<div class="input-group m-b" style="width:150px !important;">
											{!! Form::text('suscripcion', $row['suscripcion'],array('class'=>'form-control date')) !!}
											<span class="input-group-addon"><i class="fa fa-calendar"></i></span>
										</div> 
										 </div> 
										 <div class="col-md-2">
										 	
										 </div>
									  </div> 					
									  <div class="form-group  " >
										<label for="Fecha de Término" class=" control-label col-md-4 text-left"> Fecha de Término </label>
										<div class="col-md-6">
										  
				<div class="input-group m-b" style="width:150px !important;">
					{!! Form::text('termino', $row['termino'],array('class'=>'form-control date')) !!}
					<span class="input-group-addon"><i class="fa fa-calendar"></i></span>
				</div> 
										 </div> 
										 <div class="col-md-2">
										 	
										 </div>
									  </div> 					
									  <div class="form-group  " >
										<label for="Fecha de Vigencia" class=" control-label col-md-4 text-left"> Fecha de Vigencia </label>
										<div class="col-md-6">
										  
				<div class="input-group m-b" style="width:150px !important;">
					{!! Form::text('vigencia', $row['vigencia'],array('class'=>'form-control date')) !!}
					<span class="input-group-addon"><i class="fa fa-calendar"></i></span>
				</div> 
										 </div> 
										 <div class="col-md-2">
										 	
										 </div>
									  </div> 					
									  <div class="form-group  " >
										<label for="Monto total vigente" class=" control-label col-md-4 text-left"> Monto total vigente </label>
										<div class="col-md-6">
										  {!! Form::text('monto_inicio', $row['monto_inicio'],array('class'=>'form-control', 'placeholder'=>'',   )) !!} 
										 </div> 
										 <div class="col-md-2">
										 	
										 </div>
									  </div> 					
									  <div class="form-group  " >
										<label for="Moneda" class=" control-label col-md-4 text-left"> Moneda </label>
										<div class="col-md-6">
										  
					<?php $monedainicio_id = explode(',',$row['monedainicio_id']);
					$monedainicio_id_opt = array( 'dolar' => 'Dolar' ,  'cls' => 'Pesos Chilenos' ,  'uf' => 'UF' , ); ?>
					<select name='monedainicio_id' rows='5'   class='select2 '  > 
						<?php 
						foreach($monedainicio_id_opt as $key=>$val)
						{
							echo "<option  value ='$key' ".($row['monedainicio_id'] == $key ? " selected='selected' " : '' ).">$val</option>"; 						
						}						
						?></select> 
										 </div> 
										 <div class="col-md-2">
										 	
										 </div>
									  </div> </fieldset>
			</div>
			
			
			
	</div>	
@if($row['id_contrato'] !== '')	
	<div role="tabpanel" class="tab-pane" id="masterdetailform">
					 			
	<hr />
	<div class="clr clear"></div>
	
	<h5> Items </h5>
	
	<div class="table-responsive">
    <table class="table table-striped ">
        <thead>
			<tr>
				@foreach ($subform['tableGrid'] as $t)
					@if($t['view'] =='1' && $t['field'] !='id_contrato')
						<th>{{ $t['label'] }}</th>
					@endif
				@endforeach
				<th></th>	
			  </tr>

        </thead>

        <tbody>
        @if(count($subform['rowData'])>=1)
            @foreach ($subform['rowData'] as $rows)
            <tr class="clone clonedInput">
									
			 @foreach ($subform['tableGrid'] as $field)
				 @if($field['view'] =='1' && $field['field'] !='id_contrato')
				 <td>					 
				 	{!! SiteHelpers::bulkForm($field['field'] , $subform['tableForm'] , $rows->{$field['field']}) !!}							 
				 </td>
				 @endif					 
			 
			 @endforeach
			 <td>
			 	<a onclick=" $(this).parents('.clonedInput').remove(); return false" href="#" class="remove btn btn-xs btn-danger">-</a>
			 	<input type="hidden" name="counter[]">
			 </td>
			@endforeach
			</tr> 

		@else
            <tr class="clone clonedInput">
									
			 @foreach ($subform['tableGrid'] as $field)

				 @if($field['view'] =='1' && $field['field'] !='id_contrato')
				 <td>					 
				 	{!! SiteHelpers::bulkForm($field['field'] , $subform['tableForm'] ) !!}							 
				 </td>
				 @endif					 
			 
			 @endforeach
			 <td>
			 	<a onclick=" $(this).parents('.clonedInput').remove(); return false" href="#" class="remove btn btn-xs btn-danger">-</a>
			 	<input type="hidden" name="counter[]">
			 </td>
			
			</tr> 

		
		@endif	


        </tbody>	

     </table>  
     <input type="hidden" name="enable-masterdetail" value="true">
     </div>
	<br /><br />
     
     <a href="javascript:void(0);" class="addC btn btn-xs btn-info" rel=".clone"><i class="fa fa-plus"></i> New Item</a>
				
	</div>

	<div role="tabpanel" class="tab-pane" id="adjuntos">
			<div id="adjuntos-content"></div>	
			
					<a href="../../adjuntoscontratos/update?contratoid={{$row['id_contrato']}}" class="btn btn-xs btn-info" target="_blank"><i class="fa fa-plus"></i>Nuevo Adjunto</a>	
							
	</div>
	@endif	
</div><!--close tab panel-->

			<div style="clear:both"></div>	
				
					
				  <div class="form-group">
					<label class="col-sm-4 text-right">&nbsp;</label>
					<div class="col-sm-8">	
					<button type="submit" name="apply" class="btn btn-info btn-sm" ><i class="icon-checkmark-circle2"></i> {{ Lang::get('core.sb_apply') }}</button>
					<button type="submit" name="submit" class="btn btn-primary btn-sm" ><i class="icon-bubble-check"></i> {{ Lang::get('core.sb_save') }}</button>
					<button type="button" onclick="location.href='{{ URL::to('maestrocontrato?return='.$return) }}' " class="btn btn-warning btn-sm "><i class="icon-cancel-circle2 "></i>  {{ Lang::get('core.sb_cancel') }} </button>
					</div>	  
			
				  </div> 
		 
		 {!! Form::close() !!}
	</div>
</div>		 
</div>	
</div>		
<script type="text/javascript">
	$(function(){
					$('#adjuntos-content').load('../lookup/Adjuntos-maestrocontrato-id_contrato-adjuntoscontratos-sb_contrato_adjuntos-id_contrato-{{$row['id_contrato']}}')
			})

</script>	 
   <script type="text/javascript">
	$(document).ready(function() { 
		$('.addC').relCopy({});
		
		$(".date[name=suscripcion]").datepicker("update", new Date);	
		$("#contratista_id").jCombo("{!! url('maestrocontrato/comboselect?filter=tb_users:id:last_name|first_name') !!}",
		{  selected_value : '{{ $row["contratista_id"] }}' });
		
		$("#mandante_id").jCombo("{!! url('maestrocontrato/comboselect?filter=tb_users:id:last_name|first_name') !!}",
		{  selected_value : '{{ $row["mandante_id"] }}' });
		
		$("#empresa_id").jCombo("{!! url('maestrocontrato/comboselect?filter=sb_empresa:id_empresa:rut|nombre') !!}",
		{  selected_value : '{{ $row["empresa_id"] }}' });
		 

		$('.removeMultiFiles').on('click',function(){
			var removeUrl = '{{ url("maestrocontrato/removefiles?file=")}}'+$(this).attr('url');
			$(this).parent().remove();
			$.get(removeUrl,function(response){});
			$(this).parent('div').empty();	
			return false;
		});		


		
	});
	function empresaSelected(data){
			console.log('value selected::  ' + data.selectedOptions[0].value);
			var id = data.selectedOptions[0].value;
			if(id!== ""){
				//buscamos contratista seleccionado
				$.ajax({
					dataType 	: "json",
					url 		: 'http://'+window.location.hostname+'/mccep/public/api/v1/registrocont?id=' +id,
					method		: "GET",
					success 	: function(data){
						console.log("data:: " +  data.rut);
						$('#rutC').html(data.rut);
						$('#razonC').html(data.nombre);
						$('#direccionC').html(data.direccion);
						$('#emailC').html(data.correo);
						$('#telC').html(data.telefono);
						$('#replegal1C').html(data.representante_legal1);
						//$('#tipo').html(data.tipo);
					}
				});
			}
		}
	</script>		 
@stop