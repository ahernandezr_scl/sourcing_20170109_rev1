<div class="m-t" style="padding-top:25px;">	
    <div class="row m-b-lg animated fadeInDown delayp1 text-center">
        <h3> {{ $pageTitle }} <small> {{ $pageNote }} </small></h3>
        <hr />       
    </div>
</div>
<div class="m-t">
	<div class="table-responsive" > 	

		<table class="table table-striped table-bordered" >
			<tbody>	
		
			
					<tr>
						<td width='30%' class='label-view text-right'>{{ SiteHelpers::activeLang('Globaldiversey Id', (isset($fields['globaldiversey_id']['language'])? $fields['globaldiversey_id']['language'] : array())) }}</td>
						<td>{{ $row->globaldiversey_id}} </td>
						
					</tr>
				
					<tr>
						<td width='30%' class='label-view text-right'>{{ SiteHelpers::activeLang('Contrato Id', (isset($fields['contrato_id']['language'])? $fields['contrato_id']['language'] : array())) }}</td>
						<td>{{ SiteHelpers::formatLookUp($row->contrato_id,'contrato_id','1:tbl_contrato:contrato_id:cont_proveedor') }} </td>
						
					</tr>
				
					<tr>
						<td width='30%' class='label-view text-right'>{{ SiteHelpers::activeLang('GlosaTotal', (isset($fields['glosaTotal']['language'])? $fields['glosaTotal']['language'] : array())) }}</td>
						<td>{{ $row->glosaTotal}} </td>
						
					</tr>
				
					<tr>
						<td width='30%' class='label-view text-right'>{{ SiteHelpers::activeLang('Total1', (isset($fields['total1']['language'])? $fields['total1']['language'] : array())) }}</td>
						<td>{{ $row->total1}} </td>
						
					</tr>
				
					<tr>
						<td width='30%' class='label-view text-right'>{{ SiteHelpers::activeLang('Total2', (isset($fields['total2']['language'])? $fields['total2']['language'] : array())) }}</td>
						<td>{{ $row->total2}} </td>
						
					</tr>
				
					<tr>
						<td width='30%' class='label-view text-right'>{{ SiteHelpers::activeLang('Total3', (isset($fields['total3']['language'])? $fields['total3']['language'] : array())) }}</td>
						<td>{{ $row->total3}} </td>
						
					</tr>
				
					<tr>
						<td width='30%' class='label-view text-right'>{{ SiteHelpers::activeLang('Comentario', (isset($fields['comentario']['language'])? $fields['comentario']['language'] : array())) }}</td>
						<td>{{ $row->comentario}} </td>
						
					</tr>
				
					<tr>
						<td width='30%' class='label-view text-right'>{{ SiteHelpers::activeLang('FechaIngreso', (isset($fields['FechaIngreso']['language'])? $fields['FechaIngreso']['language'] : array())) }}</td>
						<td>{{ $row->FechaIngreso}} </td>
						
					</tr>
						
					<tr>
						<td width='30%' class='label-view text-right'></td>
						<td> <a href="javascript:history.go(-1)" class="btn btn-primary"> Back To Grid <a> </td>
						
					</tr>					
				
			</tbody>	
		</table>   

	 
	
	</div>
</div>	