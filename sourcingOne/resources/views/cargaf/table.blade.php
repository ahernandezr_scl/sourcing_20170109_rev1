<!-- Modal -->
<div id="myModal" class="modal fade" role="dialog">
  <div class="modal-dialog">
    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Detalle</h4>
      </div>
      <div id="ModalViewPDF" class="modal-body">
        
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
      </div>
    </div>

  </div>
</div>
<?php usort($tableGrid, "SiteHelpers::_sort"); ?>
     
     <!-- Incluimos la libería para que carguemos el archivo por ajax -->
     <link href="{{ asset('sximo/css/plugins/fileinput/fileinput.css')}}" media="all" rel="stylesheet" type="text/css" />
     <script src="{{ asset('sximo/js/plugins/fileinput/fileinput.min.js')}}" type="text/javascript"></script>
     <script src="{{ asset('sximo/js/plugins/fileinput/fileinput_locale_es.js')}}" type="text/javascript"></script>
     <!-- Incluimos la libería para que carguemos el archivo por ajax -->

<div class="sbox">
	<div class="sbox-title"> 
	<h3><i class="fa fa-table"></i> <?php echo $pageTitle ;?> <small>{{ $pageNote }}</small></h3>
		
		<div class="sbox-tools" >
			<a href="javascript:void(0)" class="btn btn-xs btn-white tips" title="Clear Search" onclick="reloadData('#{{ $pageModule }}','cargaf/data?search=')"><i class="fa fa-trash-o"></i> Clear Search </a>
			<a href="javascript:void(0)" class="btn btn-xs btn-white tips" title="Reload Data" onclick="reloadData('#{{ $pageModule }}','cargaf/data?return={{ $return }}')"><i class="fa fa-refresh"></i></a>
			@if(Session::get('gid') ==1)
			<a href="{{ url('sximo/module/config/'.$pageModule) }}" class="btn btn-xs btn-white tips" title=" {{ Lang::get('core.btn_config') }}" ><i class="fa fa-cog"></i></a>
			@endif
		</div>
	</div>
	<div class="sbox-content"> 	

	<div id="inputfileup" class="row">
	  <div class="col-xs-12">
		  <div class="form-group  " >
			<label for="Archivo" class=" control-label col-md-4 text-left"> Archivo <span class="asterix"> * </span></label>
			<div class="col-md-6">
			  <input id="FileDataEmployee" name="FileDataEmployee" type="file" class="file" data-show-preview="false" multiple data-allowed-file-extensions='["pdf"]'>
			 </div> 
			 <div class="col-md-2">
			 	
			 </div>
		  </div>
	  </div>
	</div>

	 {!! (isset($search_map) ? $search_map : '') !!}
	 
	 <?php echo Form::open(array('url'=>'cargaf/delete/', 'class'=>'form-horizontal' ,'id' =>'SximoTable'  ,'data-parsley-validate'=>'' )) ;?>
<div class="table-responsive">	
	@if(count($rowData)>=1)
    <table class="table table-striped  " id="{{ $pageModule }}Table">
        <thead>
			<tr>
				<th width="20"> No </th>
				<th width="30"> <input type="checkbox" class="checkall" /></th>		
				@if($setting['view-method']=='expand')<th width="30" style="width: 30px;">  </th> @endif			
				<?php foreach ($tableGrid as $t) :
					if($t['view'] =='1'):
						$limited = isset($t['limited']) ? $t['limited'] :'';
						if(SiteHelpers::filterColumn($limited ))
						{
							echo '<th align="'.$t['align'].'" width="'.$t['width'].'">'.\SiteHelpers::activeLang($t['label'],(isset($t['language'])? $t['language'] : array())).'</th>';				
						} 
					endif;
				endforeach; ?>
				<th width="100" class="text-right"><?php echo Lang::get('core.btn_action') ;?></th>
			  </tr>
        </thead>

        <tbody>
        	@if($access['is_add'] =='1' && $setting['inline']=='true')
			<tr id="form-0" >
				<td> # </td>
				<td> </td>
				@if($setting['view-method']=='expand') <td> </td> @endif
				@foreach ($tableGrid as $t)
					@if($t['view'] =='1')
					<?php $limited = isset($t['limited']) ? $t['limited'] :''; ?>
						@if(SiteHelpers::filterColumn($limited ))
						<td data-form="{{ $t['field'] }}" data-form-type="{{ AjaxHelpers::inlineFormType($t['field'],$tableForm)}}">
							{!! SiteHelpers::transForm($t['field'] , $tableForm) !!}								
						</td>
						@endif
					@endif
				@endforeach
				<td width="100" class="text-right">
					<button onclick="saved('form-0')" class="btn btn-primary btn-xs" type="button"><i class="icon-checkmark-circle2"></i></button>
				</td>
			  </tr>	 
			  @endif        
			
           		<?php foreach ($rowData as $row) : 
           			  $id = $row->IdCargaDocumento;
           		?>
                <tr class="editable" id="form-{{ $row->IdCargaDocumento }}">
					<td class="number"> <?php echo ++$i;?>  </td>
					<td ><input type="checkbox" class="ids" name="ids[]" value="<?php echo $row->IdCargaDocumento ;?>" />  </td>					
					@if($setting['view-method']=='expand')
					<td><a href="javascript:void(0)" class="expandable" rel="#row-{{ $row->IdCargaDocumento }}" data-url="{{ url('cargaf/show/'.$id) }}"><i class="fa fa-plus " ></i></a></td>								
					@endif			
					 <?php foreach ($tableGrid as $field) :
					 	if($field['view'] =='1') : 
							$value = SiteHelpers::formatRows($row->{$field['field']}, $field , $row);
						 	?>
						 	<?php $limited = isset($field['limited']) ? $field['limited'] :''; ?>
						 	@if(SiteHelpers::filterColumn($limited ))
								 <td align="<?php echo $field['align'];?>" data-values="{{ $row->{$field['field']} }}" data-field="{{ $field['field'] }}" data-format="{{ htmlentities($value) }}">					 
									{!! $value !!}							 
								 </td>
							@endif	
						 <?php endif;					 
						endforeach; 
					  ?>
				 <td data-values="action" data-key="<?php echo $row->IdCargaDocumento ;?>" width="100" class="text-right">
					{!! AjaxHelpers::buttonAction('cargaf',$access,$id ,$setting) !!}
					{!! AjaxHelpers::buttonActionInline($row->IdCargaDocumento,'IdCargaDocumento') !!}		
				</td>			 
                </tr>
                @if($setting['view-method']=='expand')
                <tr style="display:none" class="expanded" id="row-{{ $row->IdCargaDocumento }}">
                	<td class="number"></td>
                	<td></td>
                	<td></td>
                	<td colspan="{{ $colspan}}" class="data"></td>
                	<td></td>
                </tr>
                @endif				
            <?php endforeach;?>
              
        </tbody>
      
    </table>
	@else

	<div style="margin:100px 0; text-align:center;">
	
		<p> No Record Found </p>
	</div>
	
	@endif		
	
	</div>
	<?php echo Form::close() ;?>
	@include('ajaxfooter')
	
	</div>
</div>	
	
	@if($setting['inline'] =='true') @include('sximo.module.utility.inlinegrid') @endif
<script>
function ViewPDF(url){
		PDFObject.embed("uploads/documents/"+url, "#ModalViewPDF");
		$("#myModal").modal();
	  }
$(document).ready(function() {
	  	$("#FileDataEmployee").fileinput('refresh',
		        {
		          browseLabel: ' Seleccionar', 
		          removeLabel: ' Eliminar',
		          uploadLabel: ' Cargar',
		          uploadUrl: '{{ asset("cargadocumento")}}',
        		  extra: {idtipodocumento: 1}
		        }
		    );
		    $('#FileDataEmployee').on('fileunlock', function(event, filestack, extraData) {
			    reloadData('#{{ $pageModule }}','cargaf/data?return={{ $return }}');
			});

	$('.tips').tooltip();	
	$('input[type="checkbox"],input[type="radio"]').iCheck({
		checkboxClass: 'icheckbox_square-red',
		radioClass: 'iradio_square-red',
	});	
	$('#{{ $pageModule }}Table .checkall').on('ifChecked',function(){
		$('#{{ $pageModule }}Table input[type="checkbox"]').iCheck('check');
	});
	$('#{{ $pageModule }}Table .checkall').on('ifUnchecked',function(){
		$('#{{ $pageModule }}Table input[type="checkbox"]').iCheck('uncheck');
	});	
	
	$('#{{ $pageModule }}Paginate .pagination li a').click(function() {
		var url = $(this).attr('href');
		reloadData('#{{ $pageModule }}',url);		
		return false ;
	});

	<?php if($setting['view-method'] =='expand') :
			echo AjaxHelpers::htmlExpandGrid();
		endif;
	 ?>	
});		
</script>	
<style>
.table th.right { text-align:right !important;}
.table th.center { text-align:center !important;}
#inputfileup {
			display: block;
			padding:20px 0px;
		}
		.btn-file, .fileinput-remove-button, .fileinput-cancel-button, .fileinput-upload-button {
			padding-top: 5px!important;
			padding-bottom: 4px!important;
		}
		.kv-upload-progress {
			display:none!important;
		}
</style>
