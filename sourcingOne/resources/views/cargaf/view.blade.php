@if($setting['view-method'] =='native')
<div class="sbox">
	<div class="sbox-title">  
		<h4> <i class="fa fa-table"></i> <?php echo $pageTitle ;?> <small>{{ $pageNote }}</small>
			<a href="javascript:void(0)" class="collapse-close pull-right btn btn-xs btn-danger" onclick="ajaxViewClose('#{{ $pageModule }}')">
			<i class="fa fa fa-times"></i></a>
		</h4>
	 </div>

	<div class="sbox-content"> 
@endif	

		<table class="table table-striped table-bordered" >
			<tbody>	
				
					<tr>
						<td width='30%' class='label-view text-right'>{{ SiteHelpers::activeLang('IdCargaDocumento', (isset($fields['IdCargaDocumento']['language'])? $fields['IdCargaDocumento']['language'] : array())) }}</td>
						<td>{{ $row->IdCargaDocumento}} </td>
						
					</tr>
				
					<tr>
						<td width='30%' class='label-view text-right'>{{ SiteHelpers::activeLang('Document Type', (isset($fields['IdTipoDocumento']['language'])? $fields['IdTipoDocumento']['language'] : array())) }}</td>
						<td>{{ SiteHelpers::formatLookUp($row->IdTipoDocumento,'IdTipoDocumento','1:tbl_tipos_documentos:IdTipoDocumento:Descripcion') }} </td>
						
					</tr>
				
					<tr>
						<td width='30%' class='label-view text-right'>{{ SiteHelpers::activeLang('Archivo', (isset($fields['Archivo']['language'])? $fields['Archivo']['language'] : array())) }}</td>
						<td>{!! SiteHelpers::formatRows($row->Archivo,$fields['Archivo'],$row ) !!} </td>
						
					</tr>
				
					<tr>
						<td width='30%' class='label-view text-right'>{{ SiteHelpers::activeLang('ArchivoTipo', (isset($fields['ArchivoTipo']['language'])? $fields['ArchivoTipo']['language'] : array())) }}</td>
						<td>{{ $row->ArchivoTipo}} </td>
						
					</tr>
				
					<tr>
						<td width='30%' class='label-view text-right'>{{ SiteHelpers::activeLang('Archivo', (isset($fields['ArchivoUrl']['language'])? $fields['ArchivoUrl']['language'] : array())) }}</td>
						<td><a href="{{$row->ArchivoUrl}}">{{ $row->ArchivoUrl}} </a> </td>
						
					</tr>
				
					<tr>
						<td width='30%' class='label-view text-right'>{{ SiteHelpers::activeLang('ArchivoPeso', (isset($fields['ArchivoPeso']['language'])? $fields['ArchivoPeso']['language'] : array())) }}</td>
						<td>{{ $row->ArchivoPeso}} </td>
						
					</tr>
				
					<tr>
						<td width='30%' class='label-view text-right'>{{ SiteHelpers::activeLang('ArchivoTexto', (isset($fields['ArchivoTexto']['language'])? $fields['ArchivoTexto']['language'] : array())) }}</td>
						<td>{{ $row->ArchivoTexto}} </td>
						
					</tr>
				
					<tr>
						<td width='30%' class='label-view text-right'>{{ SiteHelpers::activeLang('Status', (isset($fields['IdEstatus']['language'])? $fields['IdEstatus']['language'] : array())) }}</td>
						<td>{{ MySourcing::CargaDocumentoEstatus($row->{{$row->IdEstatus}}) }} </td>
						
					</tr>
				
					<tr>
						<td width='30%' class='label-view text-right'>{{ SiteHelpers::activeLang('Resultado', (isset($fields['Resultado']['language'])? $fields['Resultado']['language'] : array())) }}</td>
						<td>{{ $row->Resultado}} </td>
						
					</tr>
				
					<tr>
						<td width='30%' class='label-view text-right'>{{ SiteHelpers::activeLang('Created On', (isset($fields['createdOn']['language'])? $fields['createdOn']['language'] : array())) }}</td>
						<td>{{ date('d/m/Y',strtotime($row->createdOn)) }} </td>
						
					</tr>
				
					<tr>
						<td width='30%' class='label-view text-right'>{{ SiteHelpers::activeLang('Entry By', (isset($fields['entry_by']['language'])? $fields['entry_by']['language'] : array())) }}</td>
						<td> </td>
						
					</tr>
				
					<tr>
						<td width='30%' class='label-view text-right'>{{ SiteHelpers::activeLang('UpdatedOn', (isset($fields['updatedOn']['language'])? $fields['updatedOn']['language'] : array())) }}</td>
						<td>{{ $row->updatedOn}} </td>
						
					</tr>
				
			</tbody>	
		</table>  
			
		 	

@if($setting['form-method'] =='native')
	</div>	
</div>	
@endif	

<script>
$(document).ready(function(){

});
</script>	