<div class="m-t" style="padding-top:25px;">	
    <div class="row m-b-lg animated fadeInDown delayp1 text-center">
        <h3> {{ $pageTitle }} <small> {{ $pageNote }} </small></h3>
        <hr />       
    </div>
</div>
<div class="m-t">
	<div class="table-responsive" > 	

		<table class="table table-striped table-bordered" >
			<tbody>	
		
			
					<tr>
						<td width='30%' class='label-view text-right'>{{ SiteHelpers::activeLang('Id Asig', (isset($fields['id_Asig']['language'])? $fields['id_Asig']['language'] : array())) }}</td>
						<td>{{ $row->id_Asig}} </td>
						
					</tr>
				
					<tr>
						<td width='30%' class='label-view text-right'>{{ SiteHelpers::activeLang('Contrato Id', (isset($fields['contrato_id']['language'])? $fields['contrato_id']['language'] : array())) }}</td>
						<td>{{ $row->contrato_id}} </td>
						
					</tr>
				
					<tr>
						<td width='30%' class='label-view text-right'>{{ SiteHelpers::activeLang('IdPersona', (isset($fields['IdPersona']['language'])? $fields['IdPersona']['language'] : array())) }}</td>
						<td>{{ $row->IdPersona}} </td>
						
					</tr>
				
					<tr>
						<td width='30%' class='label-view text-right'>{{ SiteHelpers::activeLang('IdRol', (isset($fields['idRol']['language'])? $fields['idRol']['language'] : array())) }}</td>
						<td>{{ $row->idRol}} </td>
						
					</tr>
				
					<tr>
						<td width='30%' class='label-view text-right'>{{ SiteHelpers::activeLang('IdCentro', (isset($fields['IdCentro']['language'])? $fields['IdCentro']['language'] : array())) }}</td>
						<td>{{ $row->IdCentro}} </td>
						
					</tr>
				
					<tr>
						<td width='30%' class='label-view text-right'>{{ SiteHelpers::activeLang('IdAreaTrabajo', (isset($fields['IdAreaTrabajo']['language'])? $fields['IdAreaTrabajo']['language'] : array())) }}</td>
						<td>{{ SiteHelpers::formatLookUp($row->IdAreaTrabajo,'IdAreaTrabajo','1:tbl_area_de_trabajo:IdAreaTrabajo:Descripcion') }} </td>
						
					</tr>
						
					<tr>
						<td width='30%' class='label-view text-right'></td>
						<td> <a href="javascript:history.go(-1)" class="btn btn-primary"> Back To Grid <a> </td>
						
					</tr>					
				
			</tbody>	
		</table>   

	 
	
	</div>
</div>	