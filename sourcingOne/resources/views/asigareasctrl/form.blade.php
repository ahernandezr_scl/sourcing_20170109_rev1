@extends('layouts.app')

@section('content')

  <div class="page-content row">

 	<div class="page-content-wrapper m-t">


<div class="sbox">
	<div class="sbox-title"> <h3> {{ $pageTitle }} <small>{{ $pageNote }}</small></h3> </div>
	<div class="sbox-content"> 	

		<ul class="parsley-error-list">
			@foreach($errors->all() as $error)
				<li>{{ $error }}</li>
			@endforeach
		</ul>	

		 {!! Form::open(array('url'=>'asigareasctrl/save?return='.$return, 'class'=>'form-horizontal','files' => true , 'parsley-validate'=>'','novalidate'=>' ')) !!}
<div class="col-md-12">
						<fieldset><legend> Asignación de Áreas</legend>
				{!! Form::hidden('id_Asig', $row['id_Asig']) !!}					
									  <div class="form-group  " >
										<label for="Contrato Id" class=" control-label col-md-4 text-left"> Contrato Id </label>
										<div class="col-md-6">
										  <select name='contrato_id' rows='5' id='contrato_id' class='select2 '   ></select> 
										 </div> 
										 <div class="col-md-2">
										 	
										 </div>
									  </div> 					
									  <div class="form-group  " >
										<label for="IdPersona" class=" control-label col-md-4 text-left"> IdPersona </label>
										<div class="col-md-6">
										  <select name='IdPersona' rows='5' id='IdPersona' class='select2 '   ></select> 
										 </div> 
										 <div class="col-md-2">
										 	
										 </div>
									  </div> 					
									  <div class="form-group  " >
										<label for="IdRol" class=" control-label col-md-4 text-left"> IdRol </label>
										<div class="col-md-6">
										  <select name='idRol' rows='5' id='idRol' class='select2 '   ></select> 
										 </div> 
										 <div class="col-md-2">
										 	
										 </div>
									  </div> 					
									  <div class="form-group  " >
										<label for="IdCentro" class=" control-label col-md-4 text-left"> IdCentro <span class="asterix"> * </span></label>
										<div class="col-md-6">
										  <select name='IdCentro[]' multiple rows='5' id='IdCentro' class='select2 ' required  ></select> 
										 </div> 
										 <div class="col-md-2">
										 	
										 </div>
									  </div> 					
									  <div class="form-group  " >
										<label for="IdAreaTrabajo" class=" control-label col-md-4 text-left"> IdAreaTrabajo <span class="asterix"> * </span></label>
										<div class="col-md-6">
										  <select name='IdAreaTrabajo[]' multiple rows='5' id='IdAreaTrabajo' class='select2 ' required  ></select> 
										 </div> 
										 <div class="col-md-2">
										 	
										 </div>
									  </div> </fieldset>
			</div>
			
			

		
			<div style="clear:both"></div>	
				
					
				  <div class="form-group">
					<label class="col-sm-4 text-right">&nbsp;</label>
					<div class="col-sm-8">	
					<button type="submit" name="apply" class="btn btn-info btn-sm" ><i class="icon-checkmark-circle2"></i> {{ Lang::get('core.sb_apply') }}</button>
					<button type="submit" name="submit" class="btn btn-primary btn-sm" ><i class="icon-bubble-check"></i> {{ Lang::get('core.sb_save') }}</button>
					<button type="button" onclick="location.href='{{ URL::to('asigareasctrl?return='.$return) }}' " class="btn btn-warning btn-sm "><i class="icon-cancel-circle2 "></i>  {{ Lang::get('core.sb_cancel') }} </button>
					</div>	  
			
				  </div>
		 
		 {!! Form::close() !!}
	</div>
</div>		 
</div>	
</div>			 
   <script type="text/javascript">
	$(document).ready(function() { 
		
		
		$("#contrato_id").jCombo("{!! url('asigareasctrl/comboselect?filter=tbl_contrato:contrato_id:cont_numero') !!}",
		{  selected_value : '{{ $row["contrato_id"] }}' });
		
		$("#IdPersona").jCombo("{!! url('asigareasctrl/comboselect?filter=tbl_personas:IdPersona:') !!}",
		{  selected_value : '{{ $row["IdPersona"] }}' });
		
		$("#idRol").jCombo("{!! url('asigareasctrl/comboselect?filter=tbl_roles:IdRol:Codigo|Descripción') !!}",
		{  selected_value : '{{ $row["idRol"] }}' });
		
		$("#IdCentro").jCombo("{!! url('asigareasctrl/comboselect?filter=tbl_centro:IdCentro:Descripcion') !!}",
		{  selected_value : '{{ $row["IdCentro"] }}' });
		
		$("#IdAreaTrabajo").jCombo("{!! url('asigareasctrl/comboselect?filter=tbl_area_de_trabajo:IdAreaTrabajo:') !!}&parent=IdCentro:",
		{  parent: '#IdCentro', selected_value : '{{ $row["IdAreaTrabajo"] }}' });
		 
		
		$('.removeMultiFiles').on('click',function(){
			var removeUrl = '{{ url("asigareasctrl/removefiles?file=")}}'+$(this).attr('url');
			$(this).parent().remove();
			$.get(removeUrl,function(response){});
			$(this).parent('div').empty();	
			return false;
		});		
		
	});
	</script>		 
@stop