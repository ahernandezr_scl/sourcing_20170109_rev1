

		 {!! Form::open(array('url'=>'tblfiscalizaciondetalle/savepublic', 'class'=>'form-horizontal','files' => true , 'parsley-validate'=>'','novalidate'=>' ')) !!}

	@if(Session::has('messagetext'))
	  
		   {!! Session::get('messagetext') !!}
	   
	@endif
	<ul class="parsley-error-list">
		@foreach($errors->all() as $error)
			<li>{{ $error }}</li>
		@endforeach
	</ul>		


<div class="col-md-12">
						<fieldset><legend> Detail control</legend>
									
									  <div class="form-group  " >
										<label for="Contract" class=" control-label col-md-4 text-left"> Contract </label>
										<div class="col-md-6">
										  <select name='contrato_id' rows='5' id='contrato_id' class='select2 '   ></select> 
										 </div> 
										 <div class="col-md-2">
										 	
										 </div>
									  </div> 					
									  <div class="form-group  " >
										<label for="Date of control" class=" control-label col-md-4 text-left"> Date of control </label>
										<div class="col-md-6">
										  
				<div class="input-group m-b" style="width:150px !important;">
					{!! Form::text('fiscaliza_fecha', $row['fiscaliza_fecha'],array('class'=>'form-control date')) !!}
					<span class="input-group-addon"><i class="fa fa-calendar"></i></span>
				</div> 
										 </div> 
										 <div class="col-md-2">
										 	
										 </div>
									  </div> 					
									  <div class="form-group  " >
										<label for="Ambit" class=" control-label col-md-4 text-left"> Ambit </label>
										<div class="col-md-6">
										  <select name='fiscalizaambito_id' rows='5' id='fiscalizaambito_id' class='select2 '   ></select> 
										 </div> 
										 <div class="col-md-2">
										 	
										 </div>
									  </div> 					
									  <div class="form-group  " >
										<label for="Kind" class=" control-label col-md-4 text-left"> Kind </label>
										<div class="col-md-6">
										  <select name='fiscalizatipo_id' rows='5' id='fiscalizatipo_id' class='select2 '   ></select> 
										 </div> 
										 <div class="col-md-2">
										 	
										 </div>
									  </div> 					
									  <div class="form-group  " >
										<label for="Description question" class=" control-label col-md-4 text-left"> Description question </label>
										<div class="col-md-6">
										  {!! Form::text('fiscalizapregunta_glosa', $row['fiscalizapregunta_glosa'],array('class'=>'form-control', 'placeholder'=>'',   )) !!} 
										 </div> 
										 <div class="col-md-2">
										 	
										 </div>
									  </div> 					
									  <div class="form-group  " >
										<label for="Evaluation" class=" control-label col-md-4 text-left"> Evaluation </label>
										<div class="col-md-6">
										  <select name='fiscalizaeval_id' rows='5' id='fiscalizaeval_id' class='select2 '   ></select> 
										 </div> 
										 <div class="col-md-2">
										 	
										 </div>
									  </div> </fieldset>
			</div>
			
			

			<div style="clear:both"></div>	
				
					
				  <div class="form-group">
					<label class="col-sm-4 text-right">&nbsp;</label>
					<div class="col-sm-8">	
					<button type="submit" name="apply" class="btn btn-info btn-sm" ><i class="fa  fa-check-circle"></i> {{ Lang::get('core.sb_apply') }}</button>
					<button type="submit" name="submit" class="btn btn-primary btn-sm" ><i class="fa  fa-save "></i> {{ Lang::get('core.sb_save') }}</button>
				  </div>	  
			
		</div> 
		 
		 {!! Form::close() !!}
		 
   <script type="text/javascript">
	$(document).ready(function() { 
		
		
		$("#contrato_id").jCombo("{!! url('tblfiscalizaciondetalle/comboselect?filter=tbl_contrato:contrato_id:cont_proveedor') !!}",
		{  selected_value : '{{ $row["contrato_id"] }}' });
		
		$("#fiscalizaambito_id").jCombo("{!! url('tblfiscalizaciondetalle/comboselect?filter=tbl_fiscalizacion_ambito:fiscalizaambito_id:fiscalizaambito_nombre') !!}",
		{  selected_value : '{{ $row["fiscalizaambito_id"] }}' });
		
		$("#fiscalizatipo_id").jCombo("{!! url('tblfiscalizaciondetalle/comboselect?filter=tbl_fiscalizacion_tipo:fiscalizatipo_id:fiscalizatipo_nombre') !!}",
		{  selected_value : '{{ $row["fiscalizatipo_id"] }}' });
		
		$("#fiscalizaeval_id").jCombo("{!! url('tblfiscalizaciondetalle/comboselect?filter=tbl_fiscalizacion_nota:fiscalizaeval_id:ficalizaeval_descripcion') !!}",
		{  selected_value : '{{ $row["fiscalizaeval_id"] }}' });
		 

		$('.removeCurrentFiles').on('click',function(){
			var removeUrl = $(this).attr('href');
			$.get(removeUrl,function(response){});
			$(this).parent('div').empty();	
			return false;
		});		
		
	});
	</script>		 
