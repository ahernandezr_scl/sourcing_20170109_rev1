@if($setting['view-method'] =='native')
<div class="sbox">
	<div class="sbox-title">  
		<h4> <i class="fa fa-table"></i> <?php echo $pageTitle ;?> <small>{{ $pageNote }}</small>
			<a href="javascript:void(0)" class="collapse-close pull-right btn btn-xs btn-danger" onclick="ajaxViewClose('#{{ $pageModule }}')">
			<i class="fa fa fa-times"></i></a>
		</h4>
	 </div>

	<div class="sbox-content"> 
@endif	

		<table class="table table-striped table-bordered" >
			<tbody>	
				
					<tr>
						<td width='30%' class='label-view text-right'>{{ SiteHelpers::activeLang('Contract', (isset($fields['contrato_id']['language'])? $fields['contrato_id']['language'] : array())) }}</td>
						<td>{{ SiteHelpers::formatLookUp($row->contrato_id,'contrato_id','1:tbl_contrato:contrato_id:cont_proveedor') }} </td>
						
					</tr>
				
					<tr>
						<td width='30%' class='label-view text-right'>{{ SiteHelpers::activeLang('Date of control', (isset($fields['fiscaliza_fecha']['language'])? $fields['fiscaliza_fecha']['language'] : array())) }}</td>
						<td>{{ date('d-m-Y',strtotime($row->fiscaliza_fecha)) }} </td>
						
					</tr>
				
					<tr>
						<td width='30%' class='label-view text-right'>{{ SiteHelpers::activeLang('Ambit', (isset($fields['fiscalizaambito_id']['language'])? $fields['fiscalizaambito_id']['language'] : array())) }}</td>
						<td>{{ SiteHelpers::formatLookUp($row->fiscalizaambito_id,'fiscalizaambito_id','1:tbl_fiscalizacion_ambito:fiscalizaambito_id:fiscalizaambito_nombre') }} </td>
						
					</tr>
				
					<tr>
						<td width='30%' class='label-view text-right'>{{ SiteHelpers::activeLang('Kind', (isset($fields['fiscalizatipo_id']['language'])? $fields['fiscalizatipo_id']['language'] : array())) }}</td>
						<td>{{ SiteHelpers::formatLookUp($row->fiscalizatipo_id,'fiscalizatipo_id','1:tbl_fiscalizacion_tipo:fiscalizatipo_id:fiscalizatipo_nombre') }} </td>
						
					</tr>
				
					<tr>
						<td width='30%' class='label-view text-right'>{{ SiteHelpers::activeLang('Description question', (isset($fields['fiscalizapregunta_glosa']['language'])? $fields['fiscalizapregunta_glosa']['language'] : array())) }}</td>
						<td>{{ $row->fiscalizapregunta_glosa}} </td>
						
					</tr>
				
					<tr>
						<td width='30%' class='label-view text-right'>{{ SiteHelpers::activeLang('Evaluation', (isset($fields['fiscalizaeval_id']['language'])? $fields['fiscalizaeval_id']['language'] : array())) }}</td>
						<td>{{ SiteHelpers::formatLookUp($row->fiscalizaeval_id,'fiscalizaeval_id','1:tbl_fiscalizacion_nota:fiscalizaeval_id:ficalizaeval_descripcion') }} </td>
						
					</tr>
				
			</tbody>	
		</table>  
			
		 	

@if($setting['form-method'] =='native')
	</div>	
</div>	
@endif	

<script>
$(document).ready(function(){

});
</script>	