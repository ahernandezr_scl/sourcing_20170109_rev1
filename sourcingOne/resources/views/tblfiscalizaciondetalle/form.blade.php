
@if($setting['form-method'] =='native')
	<div class="sbox">
		<div class="sbox-title">  
			<h4> <i class="fa fa-table"></i> <?php echo $pageTitle ;?> <small>{{ $pageNote }}</small>
				<a href="javascript:void(0)" class="collapse-close pull-right btn btn-xs btn-danger" onclick="ajaxViewClose('#{{ $pageModule }}')"><i class="fa fa fa-times"></i></a>
			</h4>
	</div>

	<div class="sbox-content"> 
@endif	
			{!! Form::open(array('url'=>'tblfiscalizaciondetalle/save/'.SiteHelpers::encryptID($row['fiscalizadetalle_id']), 'class'=>'form-horizontal','files' => true , 'parsley-validate'=>'','novalidate'=>' ','id'=> 'tblfiscalizaciondetalleFormAjax')) !!}
			<div class="col-md-12">
						<fieldset><legend> Detail control</legend>
									
									  <div class="form-group  " >
										<label for="Contract" class=" control-label col-md-4 text-left"> Contract </label>
										<div class="col-md-6">
										  <select name='contrato_id' rows='5' id='contrato_id' class='select2 '   ></select> 
										 </div> 
										 <div class="col-md-2">
										 	
										 </div>
									  </div> 					
									  <div class="form-group  " >
										<label for="Date of control" class=" control-label col-md-4 text-left"> Date of control </label>
										<div class="col-md-6">
										  
				<div class="input-group m-b" style="width:150px !important;">
					{!! Form::text('fiscaliza_fecha', $row['fiscaliza_fecha'],array('class'=>'form-control date')) !!}
					<span class="input-group-addon"><i class="fa fa-calendar"></i></span>
				</div> 
										 </div> 
										 <div class="col-md-2">
										 	
										 </div>
									  </div> 					
									  <div class="form-group  " >
										<label for="Ambit" class=" control-label col-md-4 text-left"> Ambit </label>
										<div class="col-md-6">
										  <select name='fiscalizaambito_id' rows='5' id='fiscalizaambito_id' class='select2 '   ></select> 
										 </div> 
										 <div class="col-md-2">
										 	
										 </div>
									  </div> 					
									  <div class="form-group  " >
										<label for="Kind" class=" control-label col-md-4 text-left"> Kind </label>
										<div class="col-md-6">
										  <select name='fiscalizatipo_id' rows='5' id='fiscalizatipo_id' class='select2 '   ></select> 
										 </div> 
										 <div class="col-md-2">
										 	
										 </div>
									  </div> 					
									  <div class="form-group  " >
										<label for="Description question" class=" control-label col-md-4 text-left"> Description question </label>
										<div class="col-md-6">
										  {!! Form::text('fiscalizapregunta_glosa', $row['fiscalizapregunta_glosa'],array('class'=>'form-control', 'placeholder'=>'',   )) !!} 
										 </div> 
										 <div class="col-md-2">
										 	
										 </div>
									  </div> 					
									  <div class="form-group  " >
										<label for="Evaluation" class=" control-label col-md-4 text-left"> Evaluation </label>
										<div class="col-md-6">
										  <select name='fiscalizaeval_id' rows='5' id='fiscalizaeval_id' class='select2 '   ></select> 
										 </div> 
										 <div class="col-md-2">
										 	
										 </div>
									  </div> </fieldset>
			</div>
			
												
								
						
			<div style="clear:both"></div>	
							
			<div class="form-group">
				<label class="col-sm-4 text-right">&nbsp;</label>
				<div class="col-sm-8">	
					<button type="submit" class="btn btn-primary btn-sm "><i class="icon-checkmark-circle2"></i>  {{ Lang::get('core.sb_save') }} </button>
					<button type="button" onclick="ajaxViewClose('#{{ $pageModule }}')" class="btn btn-success btn-sm"><i class="icon-cancel-circle2 "></i>  {{ Lang::get('core.sb_cancel') }} </button>
				</div>			
			</div> 		 
			{!! Form::close() !!}


@if($setting['form-method'] =='native')
	</div>	
</div>	
@endif	

	
</div>	
			 
<script type="text/javascript">
$(document).ready(function() { 
	
		$("#contrato_id").jCombo("{!! url('tblfiscalizaciondetalle/comboselect?filter=tbl_contrato:contrato_id:cont_proveedor') !!}",
		{  selected_value : '{{ $row["contrato_id"] }}' });
		
		$("#fiscalizaambito_id").jCombo("{!! url('tblfiscalizaciondetalle/comboselect?filter=tbl_fiscalizacion_ambito:fiscalizaambito_id:fiscalizaambito_nombre') !!}",
		{  selected_value : '{{ $row["fiscalizaambito_id"] }}' });
		
		$("#fiscalizatipo_id").jCombo("{!! url('tblfiscalizaciondetalle/comboselect?filter=tbl_fiscalizacion_tipo:fiscalizatipo_id:fiscalizatipo_nombre') !!}",
		{  selected_value : '{{ $row["fiscalizatipo_id"] }}' });
		
		$("#fiscalizaeval_id").jCombo("{!! url('tblfiscalizaciondetalle/comboselect?filter=tbl_fiscalizacion_nota:fiscalizaeval_id:ficalizaeval_descripcion') !!}",
		{  selected_value : '{{ $row["fiscalizaeval_id"] }}' });
		 
	
	$('.editor').summernote();
	$('.previewImage').fancybox();	
	$('.tips').tooltip();	
	$(".select2").select2({ width:"98%"});	
	$('.date').datepicker({format:'yyyy-mm-dd',autoClose:true})
	$('.datetime').datetimepicker({format: 'yyyy-mm-dd hh:ii:ss'}); 
	$('input[type="checkbox"],input[type="radio"]').iCheck({
		checkboxClass: 'icheckbox_square-red',
		radioClass: 'iradio_square-red',
	});			
		$('.removeMultiFiles').on('click',function(){
			var removeUrl = '{{ url("tblfiscalizaciondetalle/removefiles?file=")}}'+$(this).attr('url');
			$(this).parent().remove();
			$.get(removeUrl,function(response){});
			$(this).parent('div').empty();	
			return false;
		});
				
	var form = $('#tblfiscalizaciondetalleFormAjax'); 
	form.parsley();
	form.submit(function(){
		
		if(form.parsley('isValid') == true){			
			var options = { 
				dataType:      'json', 
				beforeSubmit :  showRequest,
				success:       showResponse  
			}  
			$(this).ajaxSubmit(options); 
			return false;
						
		} else {
			return false;
		}		
	
	});

});

function showRequest()
{
	$('.ajaxLoading').show();		
}  
function showResponse(data)  {		
	
	if(data.status == 'success')
	{
		ajaxViewClose('#{{ $pageModule }}');
		ajaxFilter('#{{ $pageModule }}','{{ $pageUrl }}/data');
		notyMessage(data.message);	
		$('#sximo-modal').modal('hide');	
	} else {
		notyMessageError(data.message);	
		$('.ajaxLoading').hide();
		return false;
	}	
}			 

</script>		 