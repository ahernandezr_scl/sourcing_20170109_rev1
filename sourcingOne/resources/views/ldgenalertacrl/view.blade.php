@extends('layouts.app')

@section('content')
<div class="page-content row">
	 
	 
 	<div class="page-content-wrapper m-t">   

<div class="sbox "> 
	<div class="sbox-title"> 

	 <h3> {{ $pageTitle }} <small>{{ $pageNote }}</small></h3>

	 	<div class="sbox-tools">
	   		<a href="{{ URL::to('ldgenalertacrl?return='.$return) }}" class="tips btn btn-xs btn-white pull-right" title="{{ Lang::get('core.btn_back') }}"><i class="fa fa-arrow-circle-left"></i>&nbsp;{{ Lang::get('core.btn_back') }}</a>
			
			@if($access['is_add'] ==1)
	   		<a href="{{ URL::to('ldgenalertacrl/update/'.$id.'?return='.$return) }}" class="tips btn btn-xs btn-white pull-right" title="{{ Lang::get('core.btn_edit') }}"><i class="fa fa-edit"></i>&nbsp;{{ Lang::get('core.btn_edit') }}</a>
			@endif 
		</div>
	</div>
	<div class="sbox-content" > 	

		<table class="table table-striped table-bordered" >
			<tbody>	
		
					<tr>
						<td width='30%' class='label-view text-right'>{{ SiteHelpers::activeLang('Contrato', (isset($fields['contrato_id']['language'])? $fields['contrato_id']['language'] : array())) }}</td>
						<td>{{ $row->contrato_id}} </td>
						
					</tr>
				
					<tr>
						<td width='30%' class='label-view text-right'>{{ SiteHelpers::activeLang('Contratista', (isset($fields['contratista_id']['language'])? $fields['contratista_id']['language'] : array())) }}</td>
						<td>{{ $row->contratista_id}} </td>
						
					</tr>
				
					<tr>
						<td width='30%' class='label-view text-right'>{{ SiteHelpers::activeLang('Persona', (isset($fields['persona_id']['language'])? $fields['persona_id']['language'] : array())) }}</td>
						<td>{{ $row->persona_id}} </td>
						
					</tr>
				
					<tr>
						<td width='30%' class='label-view text-right'>{{ SiteHelpers::activeLang('Documento', (isset($fields['documento_id']['language'])? $fields['documento_id']['language'] : array())) }}</td>
						<td>{{ $row->documento_id}} </td>
						
					</tr>
				
					<tr>
						<td width='30%' class='label-view text-right'>{{ SiteHelpers::activeLang('Tipo Alerta', (isset($fields['tipo_alerta']['language'])? $fields['tipo_alerta']['language'] : array())) }}</td>
						<td>{{ $row->tipo_alerta}} </td>
						
					</tr>
				
					<tr>
						<td width='30%' class='label-view text-right'>{{ SiteHelpers::activeLang('Mensaje', (isset($fields['id_mensaje']['language'])? $fields['id_mensaje']['language'] : array())) }}</td>
						<td>{{ $row->id_mensaje}} </td>
						
					</tr>
				
					<tr>
						<td width='30%' class='label-view text-right'>{{ SiteHelpers::activeLang('Fecha Inicio Alerta', (isset($fields['fecha_ini']['language'])? $fields['fecha_ini']['language'] : array())) }}</td>
						<td>{{ date('d/m/Y',strtotime($row->fecha_ini)) }} </td>
						
					</tr>
				
					<tr>
						<td width='30%' class='label-view text-right'>{{ SiteHelpers::activeLang('Fecha Fin Alerta', (isset($fields['fecha_fin']['language'])? $fields['fecha_fin']['language'] : array())) }}</td>
						<td>{{ date('d/m/Y',strtotime($row->fecha_fin)) }} </td>
						
					</tr>
				
					<tr>
						<td width='30%' class='label-view text-right'>{{ SiteHelpers::activeLang('Tipo Programación', (isset($fields['tipo_prog']['language'])? $fields['tipo_prog']['language'] : array())) }}</td>
						<td>{{ $row->tipo_prog}} </td>
						
					</tr>
				
					<tr>
						<td width='30%' class='label-view text-right'>{{ SiteHelpers::activeLang('Criticidad de Alerta', (isset($fields['id_crit']['language'])? $fields['id_crit']['language'] : array())) }}</td>
						<td>{{ $row->id_crit}} </td>
						
					</tr>
				
					<tr>
						<td width='30%' class='label-view text-right'>{{ SiteHelpers::activeLang('Fecha último aviso', (isset($fields['fec_ult_alerta']['language'])? $fields['fec_ult_alerta']['language'] : array())) }}</td>
						<td>{{ date('d/m/Y',strtotime($row->fec_ult_alerta)) }} </td>
						
					</tr>
				
					<tr>
						<td width='30%' class='label-view text-right'>{{ SiteHelpers::activeLang('Estado Alerta', (isset($fields['estado_al']['language'])? $fields['estado_al']['language'] : array())) }}</td>
						<td>{{ $row->estado_al}} </td>
						
					</tr>
				
			</tbody>	
		</table>   
	
	</div>
</div>	

	</div>
</div>
	  
@stop