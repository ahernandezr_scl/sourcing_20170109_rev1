

		 {!! Form::open(array('url'=>'ldgenalertacrl/savepublic', 'class'=>'form-horizontal','files' => true , 'parsley-validate'=>'','novalidate'=>' ')) !!}

	@if(Session::has('messagetext'))
	  
		   {!! Session::get('messagetext') !!}
	   
	@endif
	<ul class="parsley-error-list">
		@foreach($errors->all() as $error)
			<li>{{ $error }}</li>
		@endforeach
	</ul>		


<div class="col-md-12">
						<fieldset><legend> Alertas - Carga Manual Alertas General</legend>
				{!! Form::hidden('alertas_id', $row['alertas_id']) !!}					
									  <div class="form-group  " >
										<label for="Contrato" class=" control-label col-md-4 text-left"> Contrato </label>
										<div class="col-md-6">
										  <select name='contrato_id' rows='5' id='contrato_id' class='select2 '   ></select> 
										 </div> 
										 <div class="col-md-2">
										 	
										 </div>
									  </div> 					
									  <div class="form-group  " >
										<label for="Contratista" class=" control-label col-md-4 text-left"> Contratista </label>
										<div class="col-md-6">
										  <select name='contratista_id' rows='5' id='contratista_id' class='select2 '   ></select> 
										 </div> 
										 <div class="col-md-2">
										 	
										 </div>
									  </div> 					
									  <div class="form-group  " >
										<label for="Persona" class=" control-label col-md-4 text-left"> Persona </label>
										<div class="col-md-6">
										  <select name='persona_id' rows='5' id='persona_id' class='select2 '   ></select> 
										 </div> 
										 <div class="col-md-2">
										 	
										 </div>
									  </div> 					
									  <div class="form-group  " >
										<label for="Documento" class=" control-label col-md-4 text-left"> Documento </label>
										<div class="col-md-6">
										  <select name='documento_id' rows='5' id='documento_id' class='select2 '   ></select> 
										 </div> 
										 <div class="col-md-2">
										 	
										 </div>
									  </div> 					
									  <div class="form-group  " >
										<label for="Tipo Alerta" class=" control-label col-md-4 text-left"> Tipo Alerta </label>
										<div class="col-md-6">
										  <select name='tipo_alerta' rows='5' id='tipo_alerta' class='select2 '   ></select> 
										 </div> 
										 <div class="col-md-2">
										 	
										 </div>
									  </div> 					
									  <div class="form-group  " >
										<label for="Mensaje" class=" control-label col-md-4 text-left"> Mensaje </label>
										<div class="col-md-6">
										  <select name='id_mensaje' rows='5' id='id_mensaje' class='select2 '   ></select> 
										 </div> 
										 <div class="col-md-2">
										 	
										 </div>
									  </div> 					
									  <div class="form-group  " >
										<label for="Fecha Inicio Alerta" class=" control-label col-md-4 text-left"> Fecha Inicio Alerta </label>
										<div class="col-md-6">
										  
				<div class="input-group m-b" style="width:150px !important;">
					{!! Form::text('fecha_ini', $row['fecha_ini'],array('class'=>'form-control date')) !!}
					<span class="input-group-addon"><i class="fa fa-calendar"></i></span>
				</div> 
										 </div> 
										 <div class="col-md-2">
										 	
										 </div>
									  </div> 					
									  <div class="form-group  " >
										<label for="Fecha Fin Alerta" class=" control-label col-md-4 text-left"> Fecha Fin Alerta </label>
										<div class="col-md-6">
										  
				<div class="input-group m-b" style="width:150px !important;">
					{!! Form::text('fecha_fin', $row['fecha_fin'],array('class'=>'form-control date')) !!}
					<span class="input-group-addon"><i class="fa fa-calendar"></i></span>
				</div> 
										 </div> 
										 <div class="col-md-2">
										 	
										 </div>
									  </div> 					
									  <div class="form-group  " >
										<label for="Estado Alerta" class=" control-label col-md-4 text-left"> Estado Alerta </label>
										<div class="col-md-6">
										  
					<?php $alerta_activa = explode(',',$row['alerta_activa']);
					$alerta_activa_opt = array( '0' => 'Desactivada' ,  '1' => 'Activada' , ); ?>
					<select name='alerta_activa' rows='5'   class='select2 '  > 
						<?php 
						foreach($alerta_activa_opt as $key=>$val)
						{
							echo "<option  value ='$key' ".($row['alerta_activa'] == $key ? " selected='selected' " : '' ).">$val</option>"; 						
						}						
						?></select> 
										 </div> 
										 <div class="col-md-2">
										 	
										 </div>
									  </div> 					
									  <div class="form-group  " >
										<label for="Tipo Programación" class=" control-label col-md-4 text-left"> Tipo Programación </label>
										<div class="col-md-6">
										  
					<?php $tipo_prog = explode(',',$row['tipo_prog']);
					$tipo_prog_opt = array( 'MANUAL' => 'Manual' , ); ?>
					<select name='tipo_prog' rows='5'   class='select2 '  > 
						<?php 
						foreach($tipo_prog_opt as $key=>$val)
						{
							echo "<option  value ='$key' ".($row['tipo_prog'] == $key ? " selected='selected' " : '' ).">$val</option>"; 						
						}						
						?></select> 
										 </div> 
										 <div class="col-md-2">
										 	
										 </div>
									  </div> 					
									  <div class="form-group  " >
										<label for="Criticidad de Alerta" class=" control-label col-md-4 text-left"> Criticidad de Alerta </label>
										<div class="col-md-6">
										  <select name='id_crit' rows='5' id='id_crit' class='select2 '   ></select> 
										 </div> 
										 <div class="col-md-2">
										 	
										 </div>
									  </div> </fieldset>
			</div>
			
			

			<div style="clear:both"></div>	
				
					
				  <div class="form-group">
					<label class="col-sm-4 text-right">&nbsp;</label>
					<div class="col-sm-8">	
					<button type="submit" name="apply" class="btn btn-info btn-sm" ><i class="fa  fa-check-circle"></i> {{ Lang::get('core.sb_apply') }}</button>
					<button type="submit" name="submit" class="btn btn-primary btn-sm" ><i class="fa  fa-save "></i> {{ Lang::get('core.sb_save') }}</button>
				  </div>	  
			
		</div> 
		 
		 {!! Form::close() !!}
		 
   <script type="text/javascript">
	$(document).ready(function() { 
		
		
		$("#contrato_id").jCombo("{!! url('ldgenalertacrl/comboselect?filter=tbl_contrato:contrato_id:cont_numero|cont_proveedor') !!}",
		{  selected_value : '{{ $row["contrato_id"] }}' });
		
		$("#contratista_id").jCombo("{!! url('ldgenalertacrl/comboselect?filter=tbl_contratistas:IdContratista:RUT|NombreCorto') !!}",
		{  selected_value : '{{ $row["contratista_id"] }}' });
		
		$("#persona_id").jCombo("{!! url('ldgenalertacrl/comboselect?filter=tbl_personas:IdPersona:RUT|Nombres|Apellidos') !!}",
		{  selected_value : '{{ $row["persona_id"] }}' });
		
		$("#documento_id").jCombo("{!! url('ldgenalertacrl/comboselect?filter=tbl_tipos_documentos:IdTipoDocumento:Descripcion') !!}",
		{  selected_value : '{{ $row["documento_id"] }}' });
		
		$("#tipo_alerta").jCombo("{!! url('ldgenalertacrl/comboselect?filter=tbl_alerta_tpo:tipo_alerta:desc_tipo_alerta') !!}",
		{  selected_value : '{{ $row["tipo_alerta"] }}' });
		
		$("#id_mensaje").jCombo("{!! url('ldgenalertacrl/comboselect?filter=tbl_alerta_msg:id_mensaje:texto_mensaje') !!}&parent=tipo_alerta:",
		{  parent: '#tipo_alerta', selected_value : '{{ $row["id_mensaje"] }}' });
		
		$("#id_crit").jCombo("{!! url('ldgenalertacrl/comboselect?filter=tbl_alerta_crit:id_crit:desc_crit') !!}",
		{  selected_value : '{{ $row["id_crit"] }}' });
		 

		$('.removeCurrentFiles').on('click',function(){
			var removeUrl = $(this).attr('href');
			$.get(removeUrl,function(response){});
			$(this).parent('div').empty();	
			return false;
		});		
		
	});
	</script>		 
