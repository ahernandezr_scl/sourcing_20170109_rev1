<div class="m-t" style="padding-top:25px;">	
    <div class="row m-b-lg animated fadeInDown delayp1 text-center">
        <h3> {{ $pageTitle }} <small> {{ $pageNote }} </small></h3>
        <hr />       
    </div>
</div>
<div class="m-t">
	<div class="table-responsive" > 	

		<table class="table table-striped table-bordered" >
			<tbody>	
		
			
					<tr>
						<td width='30%' class='label-view text-right'>{{ SiteHelpers::activeLang('Contrato', (isset($fields['contrato_id']['language'])? $fields['contrato_id']['language'] : array())) }}</td>
						<td>{{ $row->contrato_id}} </td>
						
					</tr>
				
					<tr>
						<td width='30%' class='label-view text-right'>{{ SiteHelpers::activeLang('Contratista', (isset($fields['contratista_id']['language'])? $fields['contratista_id']['language'] : array())) }}</td>
						<td>{{ $row->contratista_id}} </td>
						
					</tr>
				
					<tr>
						<td width='30%' class='label-view text-right'>{{ SiteHelpers::activeLang('Persona', (isset($fields['persona_id']['language'])? $fields['persona_id']['language'] : array())) }}</td>
						<td>{{ $row->persona_id}} </td>
						
					</tr>
				
					<tr>
						<td width='30%' class='label-view text-right'>{{ SiteHelpers::activeLang('Documento', (isset($fields['documento_id']['language'])? $fields['documento_id']['language'] : array())) }}</td>
						<td>{{ $row->documento_id}} </td>
						
					</tr>
				
					<tr>
						<td width='30%' class='label-view text-right'>{{ SiteHelpers::activeLang('Tipo Alerta', (isset($fields['tipo_alerta']['language'])? $fields['tipo_alerta']['language'] : array())) }}</td>
						<td>{{ $row->tipo_alerta}} </td>
						
					</tr>
				
					<tr>
						<td width='30%' class='label-view text-right'>{{ SiteHelpers::activeLang('Mensaje', (isset($fields['id_mensaje']['language'])? $fields['id_mensaje']['language'] : array())) }}</td>
						<td>{{ $row->id_mensaje}} </td>
						
					</tr>
				
					<tr>
						<td width='30%' class='label-view text-right'>{{ SiteHelpers::activeLang('Fecha Inicio Alerta', (isset($fields['fecha_ini']['language'])? $fields['fecha_ini']['language'] : array())) }}</td>
						<td>{{ date('d/m/Y',strtotime($row->fecha_ini)) }} </td>
						
					</tr>
				
					<tr>
						<td width='30%' class='label-view text-right'>{{ SiteHelpers::activeLang('Fecha Fin Alerta', (isset($fields['fecha_fin']['language'])? $fields['fecha_fin']['language'] : array())) }}</td>
						<td>{{ date('d/m/Y',strtotime($row->fecha_fin)) }} </td>
						
					</tr>
				
					<tr>
						<td width='30%' class='label-view text-right'>{{ SiteHelpers::activeLang('Tipo Programación', (isset($fields['tipo_prog']['language'])? $fields['tipo_prog']['language'] : array())) }}</td>
						<td>{{ $row->tipo_prog}} </td>
						
					</tr>
				
					<tr>
						<td width='30%' class='label-view text-right'>{{ SiteHelpers::activeLang('Criticidad de Alerta', (isset($fields['id_crit']['language'])? $fields['id_crit']['language'] : array())) }}</td>
						<td>{{ $row->id_crit}} </td>
						
					</tr>
				
					<tr>
						<td width='30%' class='label-view text-right'>{{ SiteHelpers::activeLang('Fecha último aviso', (isset($fields['fec_ult_alerta']['language'])? $fields['fec_ult_alerta']['language'] : array())) }}</td>
						<td>{{ date('d/m/Y',strtotime($row->fec_ult_alerta)) }} </td>
						
					</tr>
				
					<tr>
						<td width='30%' class='label-view text-right'>{{ SiteHelpers::activeLang('Estado Alerta', (isset($fields['estado_al']['language'])? $fields['estado_al']['language'] : array())) }}</td>
						<td>{{ $row->estado_al}} </td>
						
					</tr>
						
					<tr>
						<td width='30%' class='label-view text-right'></td>
						<td> <a href="javascript:history.go(-1)" class="btn btn-primary"> Back To Grid <a> </td>
						
					</tr>					
				
			</tbody>	
		</table>   

	 
	
	</div>
</div>	