

		 {!! Form::open(array('url'=>'tipodocumentos/savepublic', 'class'=>'form-horizontal','files' => true , 'parsley-validate'=>'','novalidate'=>' ')) !!}

	@if(Session::has('messagetext'))
	  
		   {!! Session::get('messagetext') !!}
	   
	@endif
	<ul class="parsley-error-list">
		@foreach($errors->all() as $error)
			<li>{{ $error }}</li>
		@endforeach
	</ul>		


<div class="col-md-12">
						<fieldset><legend> Documents</legend>
				{!! Form::hidden('IdTipoDocumento', $row['IdTipoDocumento']) !!}
									  <div class="form-group  " >
										<label for="Formato" class=" control-label col-md-4 text-left"> Formato <span class="asterix"> * </span></label>
										<div class="col-md-6">
										  
					<?php $IdFormato = explode(',',$row['IdFormato']);
					$IdFormato_opt = array( 'application/pdf' => 'PDF' , ); ?>
					<select name='IdFormato' rows='5' required  class='select2 '  > 
						<?php
						foreach($IdFormato_opt as $key=>$val)
						{
							echo "<option  value ='$key' ".($row['IdFormato'] == $key ? " selected='selected' " : '' ).">$val</option>";
						}
						?></select>
										 </div>
										 <div class="col-md-2">
										 	
										 </div>
									  </div> 
									  <div class="form-group  " >
										<label for="Icono Formato" class=" control-label col-md-4 text-left"> Icono Formato </label>
										<div class="col-md-6">
										  {!! Form::text('IconoFormato', $row['IconoFormato'],array('class'=>'form-control', 'placeholder'=>'',   )) !!}
										 </div>
										 <div class="col-md-2">
										 	
										 </div>
									  </div> 
									  <div class="form-group  " >
										<label for="Descripción" class=" control-label col-md-4 text-left"> Descripción <span class="asterix"> * </span></label>
										<div class="col-md-6">
										  {!! Form::text('Descripcion', $row['Descripcion'],array('class'=>'form-control', 'placeholder'=>'', 'required'=>'true'  )) !!}
										 </div>
										 <div class="col-md-2">
										 	
										 </div>
									  </div> 
									  <div class="form-group  " >
										<label for="Vigencia" class=" control-label col-md-4 text-left"> Vigencia <span class="asterix"> * </span></label>
										<div class="col-md-6">
										  {!! Form::text('Vigencia', $row['Vigencia'],array('class'=>'form-control', 'placeholder'=>'', 'required'=>'true', 'parsley-type'=>'number'   )) !!}
										 </div>
										 <div class="col-md-2">
										 	
										 </div>
									  </div> 
									  <div class="form-group  " >
										<label for="BloqueaAcceso" class=" control-label col-md-4 text-left"> BloqueaAcceso </label>
										<div class="col-md-6">
										  <textarea name='BloqueaAcceso' rows='5' id='BloqueaAcceso' class='form-control '
				           >{{ $row['BloqueaAcceso'] }}</textarea>
										 </div>
										 <div class="col-md-2">
										 	
										 </div>
									  </div> 
									  <div class="form-group  " >
										<label for="Tipo" class=" control-label col-md-4 text-left"> Tipo </label>
										<div class="col-md-6">
										  <textarea name='Tipo' rows='5' id='Tipo' class='form-control '
				           >{{ $row['Tipo'] }}</textarea>
										 </div>
										 <div class="col-md-2">
										 	
										 </div>
									  </div> {!! Form::hidden('createdOn', $row['createdOn']) !!}{!! Form::hidden('updatedOn', $row['updatedOn']) !!}</fieldset>
			</div>

			

			<div style="clear:both"></div>	
				
					
				  <div class="form-group">
					<label class="col-sm-4 text-right">&nbsp;</label>
					<div class="col-sm-8">	
					<button type="submit" name="apply" class="btn btn-info btn-sm" ><i class="fa  fa-check-circle"></i> {{ Lang::get('core.sb_apply') }}</button>
					<button type="submit" name="submit" class="btn btn-primary btn-sm" ><i class="fa  fa-save "></i> {{ Lang::get('core.sb_save') }}</button>
				  </div>	  
			
		</div> 
		 
		 {!! Form::close() !!}
		 
   <script type="text/javascript">
	$(document).ready(function() { 
		$('.addC').relCopy({});
		 

		$('.removeCurrentFiles').on('click',function(){
			var removeUrl = $(this).attr('href');
			$.get(removeUrl,function(response){});
			$(this).parent('div').empty();	
			return false;
		});		
		
	});
	</script>		 
