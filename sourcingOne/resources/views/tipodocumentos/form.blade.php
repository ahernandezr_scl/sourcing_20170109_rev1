<div id="configurarModal" class="modal fade" role="dialog">
  <div class="modal-dialog">
    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Valores Asociados</h4>
      </div>
      <div class="modal-body">
	<div id="ValoresA" style="display: block;">
	  	<div class="table-responsive">
	  		<table class="table table-striped " id="tvalores">
	  		<thead>
	  		<tr>
			  <th>Valor</th>
			  <th>Display</th>
			  <th></th>
	  		</tr>
	 	 	</thead>
	  		<tbody>
	  		<tr class="cloneValoreA clonedInput">
				  <td><input type="text" id="bulk_Valor" name="bulk_Valor[]" class="form-control input-sm bulk_Valor" value=""></td>
				  <td><input type="text" id="bulk_Display" name="bulk_Display[]" class="form-control input-sm bulk_Display" value="">
				  <input type="text" name="ident" id="ident" value=""></td>
				  <td><a  onclick=" $(this).parents('.clonedInput').remove(); return false"  href="#" class="remove btn btn-xs btn-danger">-</a><input type="hidden" name="counterValoreA[]"></td>
	 		</tr>
	 		</tbody>
			 </table>
	 	<input type="hidden" name="enable-masterdetail" value="true">
	 	</div>
	 	<br><br>
	     <a href="javascript:void(0);" class="addC btn btn-xs btn-info" rel=".cloneValoreA"><i class="fa fa-plus"></i> New Item</a>
	 <hr>
	 </div>
      </div>
      <div class="modal-footer">
         <input type="hidden" name="" id="idEsta" />
         <button type="button" id="GuardarDatos" class="btn btn-default" onclick="">Guardar</button>
         <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
      </div>
    </div>

  </div>
</div>
@if($setting['form-method'] =='native')
	<div class="sbox">
		<div class="sbox-title">
			<h4> <i class="fa fa-table"></i> <?php echo $pageTitle ;?> <small>{{ $pageNote }}</small>
				<a href="javascript:void(0)" class="collapse-close pull-right btn btn-xs btn-danger" onclick="ajaxViewClose('#{{ $pageModule }}')"><i class="fa fa fa-times"></i></a>
			</h4>
	</div>

	<div class="sbox-content">
@endif
			{!! Form::open(array('url'=>'tipodocumentos/save/'.SiteHelpers::encryptID($row['IdTipoDocumento']), 'class'=>'form-horizontal','files' => true , 'parsley-validate'=>'','novalidate'=>' ','id'=> 'tipodocumentosFormAjax')) !!}
			<input type="hidden" name="tipodocumentovalor" id="tipodocumentovalor" value=""> 
						<div class="col-md-12">
						<fieldset><legend> Tipo Documentos</legend>
				{!! Form::hidden('IdTipoDocumento', $row['IdTipoDocumento']) !!}
				<div class="form-group  " >
				<label for="Tipo" class=" control-label col-md-4 text-left"> Tipo </label>
					<div class="col-md-6">

					<?php $Tipo = explode(',',$row['Tipo']);
					$Tipo_opt = array( '' => 'Seleccione Valor' ,  '1' => 'Adjunto' ,  '2' => 'No Adjunto' , ); ?>
					<select name='Tipo' id='Tipo' rows='5'   class='select2 '  >
							<?php
							foreach($Tipo_opt as $key=>$val)
							{
								echo "<option  value ='$key' ".($row['Tipo'] == $key ? " selected='selected' " : '' ).">$val</option>";
							}
							?></select>
						</div>
						<div class="col-md-2">

						</div>
					</div>
									  <div class="form-group  " id="DFormato" >
										<label for="Formato" class=" control-label col-md-4 text-left"> Formato <span class="asterix"> * </span></label>
										<div class="col-md-6">

					<?php $IdFormato = explode(',',$row['IdFormato']);
					$IdFormato_opt = array( 'application/pdf' => 'PDF' , ); ?>
					<select name='IdFormato' rows='5' required  class='select2 '  >
						<?php
						foreach($IdFormato_opt as $key=>$val)
						{
							echo "<option  value ='$key' ".($row['IdFormato'] == $key ? " selected='selected' " : '' ).">$val</option>";
						}
						?></select>
										 </div>
										 <div class="col-md-2">

										 </div>
									  </div>
									  <div class="form-group  " >
										<label for="Segmento" class=" control-label col-md-4 text-left"> Perfil</label>
										<div class="col-md-6">
											<select name='group_id' rows='5' id='group_id' class='select2 '></select>
										</div>
										<div class="col-md-2">
										</div>
									</div>
									<div class="form-group  " >
										<label for="Segmento" class=" control-label col-md-4 text-left"> Entidad</label>
										<div class="col-md-6">
											<select name='Entidad' rows='5' id='Entidad' class='select2 '></select>
										</div>
										<div class="col-md-2">
										</div>
									</div>
									  <div class="form-group  " >
										<label for="Descripción" class=" control-label col-md-4 text-left"> Descripción <span class="asterix"> * </span></label>
										<div class="col-md-6">
										  {!! Form::text('Descripcion', $row['Descripcion'],array('class'=>'form-control', 'placeholder'=>'', 'required'=>'true'  )) !!}
										 </div>
										 <div class="col-md-2">

										 </div>
									  </div> {!! Form::hidden('createdOn', $row['createdOn']) !!}
									  <div class="form-group  " >
										<label for="Vigencia" class=" control-label col-md-4 text-left"> Vigencia <span class="asterix"> * </span></label>
										<div class="col-md-6">
									                    <?php $Vigencia = explode(',',$row['Vigencia']);
								                    $Vigencia_opt = array( '' => 'Seleccione Valor' ,  '1' => 'Solicitar' ,  '2' => 'Programable' , '3' => 'Una sola vez' ,); ?>
								                    <select name='Vigencia' id='Vigencia' rows='5'   class='select2 '  >
								                            <?php
								                            foreach($Vigencia_opt as $key=>$val)
								                            {
								                                echo "<option  value ='$key' ".($row['Vigencia'] == $key ? " selected='selected' " : '' ).">$val</option>";
								                            }
								                            ?></select>
								                        </div>
								                        <div class="col-md-2">
										 </div>
									  </div>
									 <div class="form-group" id="DFormula" >
						                                        <label for="Formula" class=" control-label col-md-4 text-left"> Formula </label>
						                                        <div class="col-md-6">
						                                          {!! Form::text('Formula', $row['Formula'],array('id'=>'Formula','class'=>'form-control', 'placeholder'=>'' )) !!}
						                                         </div>
						                                         <div class="col-md-2">

						                                         </div>
						                                      </div>
									  <div class="form-group"  >
									  	<label for="Todos" class=" control-label col-md-4 text-left">  </label>
									  	<div class="col-md-6">
									  		<input type="checkbox" id="Bloquea"  name="Bloquea"> Bloquea Acceso
									  		<input type="hidden" id="BloqueaAcceso"  name="BloqueaAcceso" value="<?php echo $row['BloqueaAcceso'] ?>">
									  	</div>
									  	<div class="col-md-2">
									  	</div>
									  </div>
									  {!! Form::hidden('updatedOn', $row['updatedOn']) !!}</fieldset>
			</div>



	<hr />
	<div class="clr clear"></div>

	<h5> Valores Asociados </h5>

	<div class="table-responsive">
    <table class="table table-striped " id="TvaloresDocumentos">
        <thead>
			<tr>
				@foreach ($subform['tableGrid'] as $t)
					@if($t['view'] =='1' && $t['field'] !='IdTipoDocumento')
						<th>{{ $t['label'] }}</th>
					@endif
				@endforeach
				<th></th>
			  </tr>

        </thead>

        <tbody>
        	<?php $lintIndexRow = 1; ?>
        	<tr class="clone clonedInput">

				 @foreach ($subform['tableGrid'] as $field)

					 @if($field['view'] =='1' && $field['field'] !='IdTipoDocumento')
					 <td>
					 	{!! SiteHelpers::bulkForm($field['field'] , $subform['tableForm'] ) !!}
					 </td>
					 @endif

				 @endforeach
				 <td><input type="hidden"  name="IdDocumentoValor[]" id="IdDocumentoValor" value=""></td>
				 <td>
				 	<a id="remove" onclick=" $(this).parents('.clonedInput').remove(); return false" href="#" class="remove btn btn-xs btn-danger">-</a>
			<a style="display:none" id="editForm" class="text-info editForm" role="button" onclick="DatosModal(id);">
				<i class="icon-table"></i></a>
				 	<input type="hidden" name="counter[]">
				 </td>

			</tr>
              @foreach ($tipovalor as $rows)
              
              <tr class="clone clonedInput copy<?php echo $lintIndexRow; ?>">
				 <td>
				<input type="text" id="bulk_Etiqueta<?php echo $lintIndexRow+1; ?>" name="bulk_Etiqueta[]" class="form-control input-sm bulk_Etiqueta" data-parsley-required="true" value="<?php echo $rows->Etiqueta?>">
				 </td>
				 <td><select name="bulk_TipoValor[]" id="bulk_TipoValor<?php echo $lintIndexRow+1; ?>" class="form-control input-sm bulk_TipoValor">
				 	<option value="Texto" <?=($rows->TipoValor=="Texto")?"selected":"" ?>>Texto</option>
				 	<option value="Numérico" <?=($rows->TipoValor=="Numérico")?"selected":"" ?>>Numérico</option>
				 	<option value="Fecha" <?=($rows->TipoValor=="Fecha")?"selected":"" ?>>Fecha</option>
				 	<option value="Radio" <?=($rows->TipoValor=="Radio")?"selected":"" ?>>Radio</option>
				 	<option value="CheckBox" <?=($rows->TipoValor=="CheckBox")?"selected":"" ?>>Checkbox</option>
				 	<option value="Select Option" <?=($rows->TipoValor=="Select Option")?"selected":"" ?>>Select Option</option>
				 	</select>
				 </td>
				 <td><select name="bulk_Requerido[]" id="bulk_Requerido<?php echo $lintIndexRow+1; ?>" class="form-control input-sm bulk_Requerido">
				 	<option value="SI" <?=($rows->Requerido=="SI")?"selected":"" ?>>SI</option>
				 	<option value="No" <?=($rows->Requerido=="No")?"selected":"" ?>>NO</option>
				 </select>
				 </td>
				 <td><input type="hidden"  name="IdDocumentoValor[]" id="IdDocumentoValor<?php echo $lintIndexRow+1; ?>" value="<?php echo $rows->IdTipoDocumentoValor?>"></td>
			 <td>
			 	<a onclick=" $(this).parents('.clonedInput').remove(); return false" href="#" class="remove btn btn-xs btn-danger">-</a>
			 	<a style="<?php if ( ($rows->TipoValor!="Radio") && ($rows->TipoValor!="CheckBox") && ($rows->TipoValor!="Select Option")) { echo "display:none"; } ?>" id="editForm<?php echo $lintIndexRow+1; ?>" class="text-info editForm" role="button" onclick="DatosModal(id);">
					<i class="icon-table"></i>
				</a>
			 	<input type="hidden" name="counter[]">
			 </td>
			</tr>
			<?php $lintIndexRow += 1; ?>
			@endforeach

        </tbody>

     </table>
     <input type="hidden" name="enable-masterdetail" value="true">
     </div>
	<br /><br />

     <a href="javascript:void(0);" class="addC btn btn-xs btn-info" rel=".clone"><i class="fa fa-plus"></i> New Item</a>
     <hr />

			<div style="clear:both"></div>

			<div class="form-group">
				<label class="col-sm-4 text-right">&nbsp;</label>
				<div class="col-sm-8">
					<button type="submit" class="btn btn-primary btn-sm "><i class="icon-checkmark-circle2"></i>  {{ Lang::get('core.sb_save') }} </button>
					<button type="button" onclick="ajaxViewClose('#{{ $pageModule }}')" class="btn btn-success btn-sm"><i class="icon-cancel-circle2 "></i>  {{ Lang::get('core.sb_cancel') }} </button>
				</div>
			</div>
			{!! Form::close() !!}


@if($setting['form-method'] =='native')
	</div>
</div>
@endif


</div>

<script type="text/javascript">

var currentrow = "";
var documentstypes = {
	<?php
		  $IdTipoDocumentoValor = 0;
		  $Display = "";
		  if (isset($tipodatos )){
		  $lintIdIndex = 1;
		  foreach ($tipodatos as $value) {
		  	if ($value->Valor || $value->Display) {
		  	  if ($IdTipoDocumentoValor!=$value->IdTipoDocumentoValor){
		  		if ($IdTipoDocumentoValor!=0){
		  		  echo ' ], "display":['.$Display.']},';
		  		  $Display="";
		  		}
		  		$IdTipoDocumentoValor = $value->IdTipoDocumentoValor;
		  		$lintIdIndex += 1;
		  		echo ' "'.$lintIdIndex.'": { "valores":[';
		  		echo ' "'.$value->Valor.'", ';
		  		$Display .= " \"".$value->Display."\", ";
		  	  }else{
		  		echo ' "'.$value->Valor.'", ';
		  		$Display .= " \"".$value->Display."\", ";
		  	  }
		  	 }else{
		  	 	$lintIdIndex += 1;
		  	 }
		  }
		  if ($IdTipoDocumentoValor!=0){
		  	echo ' ], "display":['.$Display.']} ';
		  }
		}
	?>
};

function DatosModal(pintIdValor){
	
	//limpiamos la tabla
	$("#tvalores tbody tr").remove();
	//obtenemos el id
	var lintIdValor = pintIdValor;
	lintIdValor = lintIdValor.replace("editForm","");
	//declaramos la variable que va a devolver el objeto
	lintIndex = 0;
	if (documentstypes[lintIdValor]){
		console.log(documentstypes[lintIdValor]);
		documentstypes[lintIdValor]["valores"].forEach(function(valores) {
			if (valores){
				$("#tvalores tbody").append('<tr class="cloneValoreA clonedInput"> <td><input type="text" id="bulk_Valor" name="bulk_Valor[]" class="form-control input-sm bulk_Valor" value="'+valores+'"></td> <td><input type="text" id="bulk_Display" name="bulk_Display[]" class="form-control input-sm bulk_Display" value="'+documentstypes[lintIdValor]["display"][lintIndex]+'"> </td> <td><a  onclick=" $(this).parents(\'.clonedInput\').remove(); return false"  href="#" class="remove btn btn-xs btn-danger">-</a><input type="hidden" name="counterValoreA[]"></td> </tr>');
			}
			lintIndex += 1;
		});
	}else{
		documentstypes[lintIdValor] = {valores:[], display:[]};
	}
	currentrow = lintIdValor;
	$("#tvalores tbody").append('<tr class="cloneValoreA clonedInput"> <td><input type="text" id="bulk_Valor" name="bulk_Valor[]" class="form-control input-sm bulk_Valor" value=""></td> <td><input type="text" id="bulk_Display" name="bulk_Display[]" class="form-control input-sm bulk_Display" value=""> </td> <td><a  onclick=" $(this).parents(\'.clonedInput\').remove(); return false"  href="#" class="remove btn btn-xs btn-danger">-</a><input type="hidden" name="counterValoreA[]"></td> </tr>');
	$('#configurarModal').modal('show');
	console.log(documentstypes);
}

$(document).ready(function() {

	var datas = "";
	$("#GuardarDatos").click(function() {
		var valores = $("input[name='bulk_Valor[]']").map(function(){return $(this).val();}).get();
		var display = $("input[name='bulk_Display[]']").map(function(){return $(this).val();}).get();
		datas = {valores:valores, display:display};
		documentstypes[currentrow] = datas;
 		$('#configurarModal').modal('hide');
 		console.log(JSON.stringify(documentstypes));
 		$('#tipodocumentovalor').val(JSON.stringify(documentstypes));
	});


		$("#DFormato").hide();

	if ($("#Vigencia").val()==2)
		$("#DFormula").show();
	else
		$("#DFormula").hide();

	if ($("#Tipo").val()==1)
		$("#DFormato").show();
	else
		$("#DFormato").hide();


	$("#Tipo").on('change', function() {
		if( this.value==1){
			$("#DFormato").show();
		}
		else{
			$("#DFormato").hide();
		}
	});

	$("#Vigencia").on('change', function() {
		if( this.value==2){
			$("#DFormula").show();
		}
		else{
			$("#DFormula").hide();
		}
	});

	$(".bulk_TipoValor").on('change', function() {
	      var res = (this.id).split("Valor");
	      var fieldId = "#editForm" + res[1];
		 if(this.value=="Radio" || this.value=="CheckBox" || this.value=="Select Option"){
		               $(fieldId).show();
		            }
		            else{
		            	$(fieldId).hide();
		            }
	});




	<?php if ( strlen($row['IdTipoDocumento'])==0): ?>
	$('#BloqueaAcceso').val("NO");
<?php else: ?>
if ($('#BloqueaAcceso').val()=="SI")
$("#Bloquea").iCheck('check');
<?php endif; ?>

	$("#Bloquea").on("ifChanged", function (event) {
		if($("#Bloquea").is(':checked') ){
			$('#BloqueaAcceso').val("SI");
		}else{
			$('#BloqueaAcceso').val("NO");
		}

	});

$("#group_id").jCombo("{!! url('contratos/comboselect?filter=tb_groups:group_id:name') !!}",
	{  selected_value : '{{ $row["group_id"] }}' });

$("#Entidad").jCombo("{!! url('contratos/comboselect?filter=tbl_entidades:IdEntidad:Entidad') !!}",
	{  selected_value : '{{ $row["Entidad"] }}' });

	$('.addC').relCopy({});
	$('.editor').summernote();
	$('.previewImage').fancybox();
	$('.tips').tooltip();
	$(".select2").select2({ width:"98%"});
	$('.date').datepicker({format:'yyyy-mm-dd',autoClose:true})
	$('.datetime').datetimepicker({format: 'yyyy-mm-dd hh:ii:ss'});
	$('input[type="checkbox"],input[type="radio"]').iCheck({
		checkboxClass: 'icheckbox_square-red',
		radioClass: 'iradio_square-red',
	});
		$('.removeMultiFiles').on('click',function(){
			var removeUrl = '{{ url("tipodocumentos/removefiles?file=")}}'+$(this).attr('url');
			$(this).parent().remove();
			$.get(removeUrl,function(response){});
			$(this).parent('div').empty();
			return false;
		});

	var form = $('#tipodocumentosFormAjax');
	form.parsley();
	form.submit(function(){

		if(form.parsley('isValid') == true){
			var options = {
				dataType:      'json',
				data:{valores:datas},
				beforeSubmit :  showRequest,
				success:       showResponse
			}
			$(this).ajaxSubmit(options);
			return false;

		} else {
			return false;
		}

	});



});

function showRequest()
{
	$('.ajaxLoading').show();
}
function showResponse(data)  {

	if(data.status == 'success')
	{
		ajaxViewClose('#{{ $pageModule }}');
		ajaxFilter('#{{ $pageModule }}','{{ $pageUrl }}/data');
		notyMessage(data.message);
		$('#sximo-modal').modal('hide');
	} else {
		notyMessageError(data.message);
		$('.ajaxLoading').hide();
		return false;
	}
}

</script>
<style type="text/css">
    #TvaloresDocumentos tbody tr:first-child {
		display:none;
	}
	#tvalores tbody tr:last-child {
		display:none;
	}
</style>
