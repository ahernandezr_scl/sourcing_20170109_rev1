

		 {!! Form::open(array('url'=>'porteria/savepublic', 'class'=>'form-horizontal','files' => true , 'parsley-validate'=>'','novalidate'=>' ')) !!}

	@if(Session::has('messagetext'))
	  
		   {!! Session::get('messagetext') !!}
	   
	@endif
	<ul class="parsley-error-list">
		@foreach($errors->all() as $error)
			<li>{{ $error }}</li>
		@endforeach
	</ul>		


<div class="col-md-12">
						<fieldset><legend> porteria</legend>
				{!! Form::hidden('IdAcceso', $row['IdAcceso']) !!}
									  <div class="form-group  " >
										<label for="Centro" class=" control-label col-md-4 text-left"> Centro <span class="asterix"> * </span></label>
										<div class="col-md-6">
										  <select name='contrato_id' rows='5' id='contrato_id' class='select2 ' required  ></select>
										 </div>
										 <div class="col-md-2">
										 	
										 </div>
									  </div> 
									  <div class="form-group  " >
										<label for="Area" class=" control-label col-md-4 text-left"> Area <span class="asterix"> * </span></label>
										<div class="col-md-6">
										  <select name='IdPersona' rows='5' id='IdPersona' class='select2 ' required  ></select>
										 </div>
										 <div class="col-md-2">
										 	
										 </div>
									  </div> 
									  <div class="form-group  " >
										<label for="Rut" class=" control-label col-md-4 text-left"> Rut <span class="asterix"> * </span></label>
										<div class="col-md-6">
										  {!! Form::text('IdTipoAcceso', $row['IdTipoAcceso'],array('class'=>'form-control', 'placeholder'=>'', 'required'=>'true'  )) !!}
										 </div>
										 <div class="col-md-2">
										 	
										 </div>
									  </div> {!! Form::hidden('FechaInicio', $row['FechaInicio']) !!}{!! Form::hidden('FechaFinal', $row['FechaFinal']) !!}{!! Form::hidden('IdEstatus', $row['IdEstatus']) !!}</fieldset>
			</div>

			

			<div style="clear:both"></div>	
				
					
				  <div class="form-group">
					<label class="col-sm-4 text-right">&nbsp;</label>
					<div class="col-sm-8">	
					<button type="submit" name="apply" class="btn btn-info btn-sm" ><i class="fa  fa-check-circle"></i> {{ Lang::get('core.sb_apply') }}</button>
					<button type="submit" name="submit" class="btn btn-primary btn-sm" ><i class="fa  fa-save "></i> {{ Lang::get('core.sb_save') }}</button>
				  </div>	  
			
		</div> 
		 
		 {!! Form::close() !!}
		 
   <script type="text/javascript">
	$(document).ready(function() { 
		
		
		$("#contrato_id").jCombo("{!! url('porteria/comboselect?filter=tbl_centro:IdCentro:Descripcion') !!}",
		{  selected_value : '{{ $row["contrato_id"] }}' });
		
		$("#IdPersona").jCombo("{!! url('porteria/comboselect?filter=tbl_area_de_trabajo:IdAreaTrabajo:Descripcion') !!}&parent=contrato_id:",
		{  parent: '#contrato_id', selected_value : '{{ $row["IdPersona"] }}' });
		 

		$('.removeCurrentFiles').on('click',function(){
			var removeUrl = $(this).attr('href');
			$.get(removeUrl,function(response){});
			$(this).parent('div').empty();	
			return false;
		});		
		
	});
	</script>		 
