@extends('layouts.app')

@section('content')
<div class="page-content row">
	 
	 
 	<div class="page-content-wrapper m-t">   

<div class="sbox "> 
	<div class="sbox-title"> 

	 <h3> {{ $pageTitle }} <small>{{ $pageNote }}</small></h3>

	 	<div class="sbox-tools">
	   		<a href="{{ URL::to('porteria?return='.$return) }}" class="tips btn btn-xs btn-white pull-right" title="{{ Lang::get('core.btn_back') }}"><i class="fa fa-arrow-circle-left"></i>&nbsp;{{ Lang::get('core.btn_back') }}</a>
			
			@if($access['is_add'] ==1)
	   		<a href="{{ URL::to('porteria/update/'.$id.'?return='.$return) }}" class="tips btn btn-xs btn-white pull-right" title="{{ Lang::get('core.btn_edit') }}"><i class="fa fa-edit"></i>&nbsp;{{ Lang::get('core.btn_edit') }}</a>
			@endif 
		</div>
	</div>
	<div class="sbox-content" > 	

		<table class="table table-striped table-bordered" >
			<tbody>	
		
					<tr>
						<td width='30%' class='label-view text-right'>{{ SiteHelpers::activeLang('IdAcceso', (isset($fields['IdAcceso']['language'])? $fields['IdAcceso']['language'] : array())) }}</td>
						<td>{{ $row->IdAcceso}} </td>

					</tr>
				
					<tr>
						<td width='30%' class='label-view text-right'>{{ SiteHelpers::activeLang('IdTipoAcceso', (isset($fields['IdTipoAcceso']['language'])? $fields['IdTipoAcceso']['language'] : array())) }}</td>
						<td>{{ $row->IdTipoAcceso}} </td>

					</tr>
				
					<tr>
						<td width='30%' class='label-view text-right'>{{ SiteHelpers::activeLang('IdPersona', (isset($fields['IdPersona']['language'])? $fields['IdPersona']['language'] : array())) }}</td>
						<td>{{ $row->IdPersona}} </td>

					</tr>
				
					<tr>
						<td width='30%' class='label-view text-right'>{{ SiteHelpers::activeLang('Contrato Id', (isset($fields['contrato_id']['language'])? $fields['contrato_id']['language'] : array())) }}</td>
						<td>{{ $row->contrato_id}} </td>

					</tr>
				
					<tr>
						<td width='30%' class='label-view text-right'>{{ SiteHelpers::activeLang('FechaInicio', (isset($fields['FechaInicio']['language'])? $fields['FechaInicio']['language'] : array())) }}</td>
						<td>{{ $row->FechaInicio}} </td>

					</tr>
				
					<tr>
						<td width='30%' class='label-view text-right'>{{ SiteHelpers::activeLang('FechaFinal', (isset($fields['FechaFinal']['language'])? $fields['FechaFinal']['language'] : array())) }}</td>
						<td>{{ $row->FechaFinal}} </td>

					</tr>
				
					<tr>
						<td width='30%' class='label-view text-right'>{{ SiteHelpers::activeLang('IdEstatus', (isset($fields['IdEstatus']['language'])? $fields['IdEstatus']['language'] : array())) }}</td>
						<td>{{ $row->IdEstatus}} </td>

					</tr>
				
			</tbody>	
		</table>   
	
	</div>
</div>	

	</div>
</div>
	  
@stop