@extends('layouts.app')

@section('content')

  <div class="page-content row">

 	<div class="page-content-wrapper m-t">


<div class="sbox">
	<div class="sbox-title"> <h3> {{ $pageTitle }} <small>{{ $pageNote }}</small></h3> </div>
	<div class="sbox-content">

		<ul class="parsley-error-list">
			@foreach($errors->all() as $error)
				<li>{{ $error }}</li>
			@endforeach
		</ul>

		 {!! Form::open(array('url'=>'porteria/save?return='.$return, 'class'=>'form-horizontal','files' => true , 'parsley-validate'=>'','novalidate'=>' ')) !!}
<div class="col-md-12">
						<fieldset><legend> porteria</legend>
				{!! Form::hidden('IdAcceso', $row['IdAcceso']) !!}
									  <div class="form-group  " >
										<label for="Centro" class=" control-label col-md-4 text-left"> Centro <span class="asterix"> * </span></label>
										<div class="col-md-6">
										  <select name='contrato_id' rows='5' id='contrato_id' class='select2 '   ></select>
										 </div>
										 <div class="col-md-2">

										 </div>
									  </div>
									  <div class="form-group  " >
										<label for="Area" class=" control-label col-md-4 text-left"> Area <span class="asterix"> * </span></label>
										<div class="col-md-6">
										  <select name='IdPersona' rows='5' id='IdPersona' class='select2 '   ></select>
										 </div>
										 <div class="col-md-2">

										 </div>
									  </div>
									  <div class="form-group  " >
										<label for="Rut" class=" control-label col-md-4 text-left"> Rut <span class="asterix"> * </span></label>
										<div class="col-md-6">
										  {!! Form::text('IdTipoAcceso', $row['IdTipoAcceso'],array('class'=>'form-control','id'=>'rut', 'placeholder'=>'' )) !!}
										 </div>
										 <div class="col-md-2">

										 </div>
									  </div>
								<div class="resultado">
									  <div class="form-group  " >
										<label for="Rut" class=" control-label col-md-4 text-left"> Foto </label>
										<div class="col-md-6">
										  {!! Form::image('', 'btnSub', array('id'=>'Foto')) !!}
										 </div>
										 <div class="col-md-2">

										 </div>
									  </div>
									  <div class="form-group  " >
										<label for="Rut" class=" control-label col-md-4 text-left"> Nombre </label>
										<div class="col-md-6">
										  {!! Form::text('IdTipoAcceso', $row['IdTipoAcceso'],array('class'=>'form-control','id'=>'Nombre', 'placeholder'=>'','disabled'=>'disabled' )) !!}
										 </div>
										 <div class="col-md-2">

										 </div>
									  </div>
									  <div class="form-group  " >
										<label for="Rut" class=" control-label col-md-4 text-left"> Apellido </label>
										<div class="col-md-6">
										  {!! Form::text('IdTipoAcceso', $row['IdTipoAcceso'],array('class'=>'form-control','id'=>'Apellido', 'placeholder'=>'','disabled'=>'disabled' )) !!}
										 </div>
										 <div class="col-md-2">

										 </div>
									  </div>
									  <div class="form-group  " >
										<label for="Rut" class=" control-label col-md-4 text-left"> Acceso </label>
										<div class="col-md-6">
										  {!! Form::text('IdTipoAcceso', $row['IdTipoAcceso'],array('class'=>'form-control','id'=>'Permiso', 'placeholder'=>'','disabled'=>'disabled' )) !!}
										 </div>
										 <div class="col-md-2">

										 </div>
									  </div>

											  <div class="form-group">
					<label class="col-sm-4 text-right">&nbsp;</label>
					<div class="col-sm-8">
					<button type="button" id="buscar" class="btn btn-info btn-sm"><i class="icon-checkmark-circle2"></i>  Nueva Busqueda </button>
					</div>

				  </div>

								</div>
									  {!! Form::hidden('FechaInicio', $row['FechaInicio']) !!}{!! Form::hidden('FechaFinal', $row['FechaFinal']) !!}{!! Form::hidden('IdEstatus', $row['IdEstatus']) !!}</fieldset>
			</div>




			<div style="clear:both"></div>




		 {!! Form::close() !!}
	</div>
</div>
</div>
</div>
   <script type="text/javascript">
	$(document).ready(function() {


		$("#contrato_id").jCombo("{!! url('porteria/comboselect?filter=tbl_centro:IdCentro:Descripcion') !!}",
		{  selected_value : '{{ $row["contrato_id"] }}' });

		$("#IdPersona").jCombo("{!! url('porteria/comboselect?filter=tbl_area_de_trabajo:IdAreaTrabajo:Descripcion') !!}&parent=IdCentro:",
		{  parent: '#contrato_id', selected_value : '{{ $row["IdPersona"] }}' });

		function sleep(delay) {
		        var start = new Date().getTime();
		        while (new Date().getTime() < start + delay);
    		 }

		$("#rut").prop('disabled', true);
   		$('#IdPersona').on('change', function() {
   			$("#rut").prop('disabled', false);
		});
		$(".resultado").hide();
		$("#rut").blur(function(){
			if ($('#IdPersona').val()=="")
				alert("Seleccione un area de trabajo")
			else if ($('#rut').val()=="")
				alert("Ingrese un numero de Rut")
			else{
				$.post('porteria/compruebarut',{ rut:$('#rut').val(),},
				function(data) {
				if (data.valores==""){
					alert("El rut no se encuentra registrado")
				}
				else{
					$.post('porteria/validarrut',{ rut:$('#rut').val(),area:$('#IdPersona').val(),},
								function(data) {
								if (data.valores==""){
									alert("No tiene acceso")
								}
								else{
									$(".resultado").show();
									$('#Foto').attr('src', 'uploads/employee/'+data.valores[0].ArchivoFoto);
									$("#Nombre").val(data.valores[0].Nombres)
									$("#Apellido").val(data.valores[0].Apellidos)
									if (data.valores[0].acceso==1||data.valores[0].acceso==3) {
										if (data.valores[0].area==null){
											$("#Permiso").val("No tiene acceso");	
										}else{
											$("#Permiso").val("Aprobado");
										}
									}else{
										$("#Permiso").val("No tiene acceso");
									}
									//location.reload();
								}
							 });
				}
			 });

		      }
		});

		$('#rut').keypress(function (e) {
		 var key = e.which;
		 if(key == 13)  // the enter key code
		  {
		    $("#rut").blur();
		    return false;
		  }
		});

		$('#buscar').on('click',function(){
			location.reload();
		});

		$('.removeMultiFiles').on('click',function(){
			var removeUrl = '{{ url("porteria/removefiles?file=")}}'+$(this).attr('url');
			$(this).parent().remove();
			$.get(removeUrl,function(response){});
			$(this).parent('div').empty();
			return false;
		});

	});
	</script>
@stop
