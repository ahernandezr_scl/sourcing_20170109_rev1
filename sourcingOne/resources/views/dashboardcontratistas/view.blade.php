<?php
$servername = "localhost";
$username = "root";
$password = "8044302";
$year;
$mes;$reg;

#try {
    $conn2 = new PDO("mysql:host=$servername;dbname=andina_koadmin", $username, $password);
    // set the PDO error mode to exception
#    $conn2->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
#   echo "Connected successfully";
#    }
#catch(PDOException $e)
#    {
#    echo "Connection failed: " . $e->getMessage();
#    }


?>

<?php  
  if (isset($_GET["year"])) {
    $year = $_GET["year"];
   }
  else{  
    $year = date("Y");
}

  if (isset($_GET["mes"])) {
    $mes = $_GET["mes"];
   }
  else{  
    $mes = 0;
}

 if (isset($_GET["reg"])) {
    $reg = $_GET["reg"];
   }
  else{  
    $reg = 0;
}

 if (isset($_GET["seg"])) {
    $seg = $_GET["seg"];
   }
  else{  
    $seg = 0;
}

 if (isset($_GET["area"])) {
    $area = $_GET["area"];
   }
  else{  
    $area = 0;
}


 if (isset($_GET["proc"])) {
    $proc = $_GET["proc"];
   }
  else{  
    $proc = 0;
}

 if (isset($_GET["tip"])) {
    $tip = $_GET["tip"];
   }
  else{  
    $tip = 0;
}
?>
<?php
$nom_mes;
    if ($mes==1){
        $nom_mes = 'Ene';
    }
    else  if ($mes==2){
        $nom_mes = 'Feb';
    }
    else  if ($mes==3){
        $nom_mes = 'Mar';
    }
    else  if ($mes==4){
        $nom_mes = 'Abr';
    }
    else  if ($mes==5){
        $nom_mes = 'May';
    }
    else  if ($mes==6){
        $nom_mes = 'Jun';
    }
    else  if ($mes==7){
        $nom_mes = 'Jul';
    }
    else  if ($mes==8){
        $nom_mes = 'Ago';
    }
    else  if ($mes==9){
        $nom_mes = 'Sep';
    }
    else  if ($mes==10){
        $nom_mes = 'Oct';
    }
    else  if ($mes==11){
        $nom_mes = 'Nov';
    }
    else  if ($mes==12){
        $nom_mes = 'Dic';
    }
   $color_v = '#3d9b35';
   $color_r = '#e64427';
   $color_n = '#FFC000';

?>

  
<?php 
if($proc == 0){
     
                  $sql_tab = 'SELECT COUNT(DISTINCT a.IdContratista) total_cont,
                COUNT(DISTINCT CASE WHEN b.contrato_id IS NULL AND a.riesgo = 1 THEN a.IdContratista END ) sin_alto,
                COUNT(DISTINCT CASE WHEN b.contrato_id IS NULL AND a.riesgo = 0 THEN a.IdContratista END ) sin_bajo,
                COUNT(DISTINCT CASE WHEN b.contrato_id IS NOT NULL  AND a.riesgo = 1 THEN a.IdContratista END ) con_alto,
                COUNT(DISTINCT CASE WHEN b.contrato_id IS NOT NULL AND a.riesgo = 0 THEN a.IdContratista END ) con_bajo,
                COUNT(DISTINCT CASE WHEN b.contrato_id IS NULL AND d.IdEntidad IS  NULL THEN a.IdContratista END ) sin_completo,
                COUNT(DISTINCT CASE WHEN b.contrato_id IS NULL AND d.IdEntidad IS  NOT NULL THEN a.IdContratista END ) sin_incompleto,
                COUNT(DISTINCT CASE WHEN b.contrato_id IS NOT NULL AND d.IdEntidad IS  NULL THEN a.IdContratista END ) con_completo,
                COUNT(DISTINCT CASE WHEN b.contrato_id IS NOT NULL AND d.IdEntidad IS  NOT NULL THEN a.IdContratista END ) con_incompleto,
								COUNT(DISTINCT CASE WHEN a.tamano = 0 THEN a.IdContratista END ) pyme,
								COUNT(DISTINCT CASE WHEN a.tamano = 1 THEN a.IdContratista END ) peq,
								COUNT(DISTINCT CASE WHEN a.tamano = 2 THEN a.IdContratista END ) med,
								COUNT(DISTINCT CASE WHEN a.tamano = 3 THEN a.IdContratista END ) gran


				FROM tbl_contratistas a
				LEFT JOIN tbl_contrato b ON a.IdContratista = b.IdContratista
				LEFT JOIN (SELECT IdEntidad FROM tbl_documentos WHERE Entidad = 1 AND IdEstatus != 5) d ON a.IdContratista = d.IdEntidad
				 WHERE 
					CASE WHEN '.$seg.' = 0 THEN 1=1 ELSE  b.segmento_id = '.$seg.' END
					AND CASE WHEN '.$reg.' = 0 THEN 1=1 ELSE  b.geo_id = '.$reg.' END
					AND CASE WHEN '.$area.' = 0 THEN 1=1 ELSE  b.afuncional_id = '.$area.' END
					AND CASE WHEN '.$mes.' = 0 THEN YEAR(a.createdOn) <= '.$year.' ELSE a.createdOn < CONCAT(CONCAT(CONCAT(\''.$year.'\',\'-\'),\''.$mes.'\'+1),\'-01\') END';
					
				$sql_dist = 'SELECT r.id,
							COUNT(DISTINCT cn.IdContratista) AS count
					
							FROM dim_region r
							LEFT JOIN tbl_centro c ON r.id = c.IdRegion
							LEFT JOIN tbl_contratos_centros cc ON cc.IdCentro = c.IdCentro
							left JOIN ( SELECT co.contrato_id,ca.IdContratista FROM tbl_contrato co 
							INNER JOIN tbl_contratistas ca ON co.IdContratista = ca.IdContratista
							
							WHERE 
							CASE WHEN '.$seg.' = 0 THEN 1=1 ELSE  co.segmento_id = '.$seg.' END
							AND CASE WHEN '.$reg.' = 0 THEN 1=1 ELSE  co.geo_id = '.$reg.' END
							AND CASE WHEN '.$area.' = 0 THEN 1=1 ELSE  co.afuncional_id = '.$area.' END
							AND CASE WHEN '.$mes.' = 0 THEN YEAR(ca.createdOn) <= '.$year.' ELSE ca.createdOn < CONCAT(CONCAT(CONCAT(\''.$year.'\',\'-\'),\''.$mes.'\'+1),\'-01\') END
							) cn ON cn.contrato_id = cc.contrato_id
							GROUP BY 1';
		
				$sql_categ= 'SELECT cc.id_categoria,
							COUNT(distinct ca.IdContratista) AS valor
						
							FROM tbl_contratista_categoria cc
							LEFT JOIN (SELECT co.contrato_id,ca.IdContratista,id_categoria FROM tbl_contrato co 
							INNER JOIN tbl_contratistas ca ON co.IdContratista = ca.IdContratista
							WHERE 
							CASE WHEN '.$seg.' = 0 THEN 1=1 ELSE  co.segmento_id = '.$seg.' END
							AND CASE WHEN '.$reg.' = 0 THEN 1=1 ELSE  co.geo_id = '.$reg.' END
							AND CASE WHEN '.$area.' = 0 THEN 1=1 ELSE  co.afuncional_id = '.$area.' END
							AND CASE WHEN '.$mes.' = 0 THEN YEAR(ca.createdOn) <= '.$year.' ELSE ca.createdOn < CONCAT(CONCAT(CONCAT(\''.$year.'\',\'-\'),\''.$mes.'\'+1),\'-01\') END
							) ca 
							
							ON cc.id_categoria = ca.id_categoria
							
				GROUP BY 1';
  if($tip==1)
    {
      $sqlgr = $sql_dist;
    }
    else if($tip == 2)
    {
      $sqlgr = $sql_categ;

    }
  
	else if($tip == 0){
      $sqlgr = $sql_tab;

    }


				   $rawgraf = array();
                    $i=0;

                    $row1 = $conn2->query($sqlgr);
                    $results = $row1->fetchAll(PDO::FETCH_ASSOC);       
                       
                    echo json_encode($results);
                    
                  
}
else if($proc == 1)
{ ?> 
 <form method="post" >
   <div style="display: inline-block;width:70px; height:30px;">
      <b>Año</b>
                     <select name="sl_year" id="sl_year" onchange="filtros_s(this.value,1)">

<?php
                   $sqlyear = ' select distinct Anio from dim_tiempo 
                              where anio between year(current_Date) -3
                              and YEAR(CURRENT_DATE)
                              order by anio desc
                            ';
                     $i=0;
                    
                    foreach ($conn2->query($sqlyear) as $row1) {
                           if($row1["Anio"]==$year){
                              echo "<option  value='".$row1["Anio"]."' selected>".$row1["Anio"]."</option>"; 
                           }
                           else
                              echo "<option  value='".$row1["Anio"]."'>".$row1["Anio"]."</option>"; 
                          $i++;
                    }
                    
            ?>

                    </select>

       
       </div>  
       <div style="display: inline-block; width:80px; height:30px;">
            
           <b>Mes</b>
                     <select name="sl_mes" id="sl_mes" onchange="filtros_s(this.value,2)">

<?php


                   $sqlmes = ' SELECT 0 mes, \'Todos\' NMes3L
                   UNION ALL
                   SELECT DISTINCT mes , NMes3L  FROM dim_tiempo 
                   WHERE fecha_mes <= CURRENT_DATE
                   AND  anio = '.$year.' 
                   ORDER BY mes';
                     $i=0;
                    
                    foreach ($conn2->query($sqlmes) as $row1) {
                           if($row1["mes"]==$mes){
                              echo "<option  value='".$row1["mes"]."' selected>".$row1["NMes3L"]."</option>"; 
                           }
                           else
                              echo "<option  value='".$row1["mes"]."'>".$row1["NMes3L"]."</option>"; 
                          $i++;
                    }
                    
            ?>

                   
                    </select>
</div>
  <div style="display: inline-block;width:120px; height:30px;">
                <b>Tipo Faena</b>
                     <select name="sl_reg" id="sl_reg" onchange="filtros_s(this.value,3)">

<?php
                   $sqlreg = ' SELECT 0 id, \'Todos\' reg
                   UNION ALL
                   SELECT geo_id, geo_nombre FROM tbl_contgeografico';
                     $i=0;
                    
                    foreach ($conn2->query($sqlreg) as $row1) {
                           if($row1["id"]==$reg){
                              echo "<option  value='".$row1["id"]."' selected>".$row1["reg"]."</option>"; 
                           }
                           else
                              echo "<option  value='".$row1["id"]."'>".$row1["reg"]."</option>"; 
                          $i++;
                    }
                    
            ?>

                   
                    </select>
</div>
 <div style="display: inline-block;width:280px; height:30px;">
                <b>Área</b>
                     <select name="sl_area" id="sl_area" onchange="filtros_s(this.value,4)">

<?php
                   $sqlarea = ' SELECT 0 id, \'Todos\' area 
                   union all
                   SELECT afuncional_id ,afuncional_nombre area FROM tbl_contareafuncional';
                     $i=0;
                    
                    foreach ($conn2->query($sqlarea) as $row1) {
                           if($row1["id"]==$area){
                              echo "<option  value='".$row1["id"]."' selected>".$row1["area"]."</option>"; 
                           }
                           else
                              echo "<option  value='".$row1["id"]."'>".$row1["area"]."</option>"; 
                          $i++;
                    }
                    
            ?>

                   
                    </select>
        </div>
        <div style="display: inline-block;width:280px; height:30px;">

                    <b>Segmento</b>
                     <select name="sl_seg" id="sl_seg" onchange="filtros_s(this.value,5)">

<?php
                   $sqlseg = ' SELECT 0 id, \'Todos\' nombre
                   union all
                   SELECT segmento_id, seg_nombre FROM tbl_contsegmento';
                     $i=0;
                    
                    foreach ($conn2->query($sqlseg) as $row1) {
                           if($row1["id"]==$seg){
                              echo "<option  value='".$row1["id"]."' selected>".$row1["nombre"]."</option>"; 
                           }
                           else
                              echo "<option  value='".$row1["id"]."'>".$row1["nombre"]."</option>"; 
                          $i++;
                    }
                    
            ?>

                   
                    </select>
          </div>
             <div style="display: inline-block;width:100px; height:30px;">
                <button type="reset" class="btn-xs btn-default " onclick="filtros_s('0',6)">Limpiar</button>
          </div>
</form>

<?php
}

?>

