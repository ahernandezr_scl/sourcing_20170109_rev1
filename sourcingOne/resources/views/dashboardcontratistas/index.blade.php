@extends('layouts.app')
@section('content')

<?php
$servername = "localhost";
$username = "root";
$password = "8044302";

#try {
    $conn2 = new PDO("mysql:host=$servername;dbname=andina_koadmin", $username, $password);
    // set the PDO error mode to exception
#    $conn2->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
#   echo "Connected successfully";
#    }
#catch(PDOException $e)
#    {
#    echo "Connection failed: " . $e->getMessage();
#    }

 if (isset($_GET["year"])) {
    $year = $_GET["year"];
   }
  else{ 
	$year = date("Y");
	   
}

  if (isset($_GET["mes"])) {
    $mes = $_GET["mes"];
   }
 else
		{
			$mes = 0;
		}
	


 if (isset($_GET["reg"])) {
    $reg = $_GET["reg"];
   }
  else{  
    $reg = 0;
}

 if (isset($_GET["seg"])) {
    $seg = $_GET["seg"];
   }
  else{  
    $seg = 0;
}

 if (isset($_GET["area"])) {
    $area = $_GET["area"];
   }
  else{  
    $area = 0;
}

$nom_mes;
    if ($mes==1){
        $nom_mes = 'Ene';
    }
    else  if ($mes==2){
        $nom_mes = 'Feb';
    }
    else  if ($mes==3){
        $nom_mes = 'Mar';
    }
    else  if ($mes==4){
        $nom_mes = 'Abr';
    }
    else  if ($mes==5){
        $nom_mes = 'May';
    }
    else  if ($mes==6){
        $nom_mes = 'Jun';
    }
    else  if ($mes==7){
        $nom_mes = 'Jul';
    }
    else  if ($mes==8){
        $nom_mes = 'Ago';
    }
    else  if ($mes==9){
        $nom_mes = 'Sep';
    }
    else  if ($mes==10){
        $nom_mes = 'Oct';
    }
    else  if ($mes==11){
        $nom_mes = 'Nov';
    }
    else  if ($mes==12){
        $nom_mes = 'Dic';
    }
   $color_v = '#3d9b35';
   $color_r = '#e64427';
   $color_n = '#FFC000';


?>
<style>
      #embed-container {
                position: relative;
                padding-bottom: 56.25%;
                height: 0;
                overflow: hidden;
        }
        #embed-container iframe {
                position: absolute;
                top: 0;
                left: 0;
                width: 100%;
                height: 100%;
        }

#myOverlay{position:absolute;height:100%;width:100%;}
#myOverlay{background:white;opacity:.7;z-index:2;display:none;}

#loadingGIF{position:absolute;top:40%;left:45%;z-index:3;display:none;}

</style>

@if(Auth::check() && Auth::user()->group_id == 1)

    <script src="https://code.highcharts.com/highcharts.js"></script>
    <script src="https://code.highcharts.com/modules/exporting.js"></script>
    <script src="http://code.highcharts.com/highcharts-more.js"></script>
                                <tr>
                                    <td> <!--{{ Auth::user()->id }}--></td>
                                        <!--{{ Auth::user()->id }}-->
                              </tr>
    <div class="embed-container">
        <!-- -->
		  <div class="content" align="center" id="filtros">
     <form method="post" id="filter">
   <div style="display: inline-block;width:70px; height:30px;">
      <b>Año</b>
                     <select name="sl_year" id="sl_year" onchange="filtros_s(this.value,1)">

<?php
                   $sqlyear = ' select distinct Anio from dim_tiempo 
                              where anio between year(current_Date) -3
                              and YEAR(CURRENT_DATE)
                              order by anio desc
                            ';
                     $i=0;
                    
                    foreach ($conn2->query($sqlyear) as $row1) {
                           if($row1["Anio"]==$year){
                              echo "<option  value='".$row1["Anio"]."' selected>".$row1["Anio"]."</option>"; 
                           }
                           else
                              echo "<option  value='".$row1["Anio"]."'>".$row1["Anio"]."</option>"; 
                          $i++;
                    }
                    
            ?>

                    </select>

       
       </div>  
       <div style="display: inline-block; width:80px; height:30px;">
            
           <b>Mes</b>
                     <select name="sl_mes" id="sl_mes" onchange="filtros_s(this.value,2)">

<?php


                   $sqlmes = ' SELECT 0 mes, \'Todos\' NMes3L
                   UNION ALL
                   SELECT DISTINCT mes , NMes3L  FROM dim_tiempo 
                   WHERE fecha_mes <= CURRENT_DATE
                   AND  anio = '.$year.' 
                   ORDER BY mes';
                     $i=0;
                    
                    foreach ($conn2->query($sqlmes) as $row1) {
                           if($row1["mes"]==$mes){
                              echo "<option  value='".$row1["mes"]."' selected>".$row1["NMes3L"]."</option>"; 
                           }
                           else
                              echo "<option  value='".$row1["mes"]."'>".$row1["NMes3L"]."</option>"; 
                          $i++;
                    }
                    
            ?>

                   
                    </select>
</div>
  <div style="display: inline-block;width:120px; height:30px;">
                <b>Tipo Faena</b>
                     <select name="sl_reg" id="sl_reg" onchange="filtros_s(this.value,3)">

<?php
                   $sqlreg = ' SELECT 0 id, \'Todos\' reg
                   UNION ALL
                   SELECT geo_id, geo_nombre FROM tbl_contgeografico';
                     $i=0;
                    
                    foreach ($conn2->query($sqlreg) as $row1) {
                           if($row1["id"]==$reg){
                              echo "<option  value='".$row1["id"]."' selected>".$row1["reg"]."</option>"; 
                           }
                           else
                              echo "<option  value='".$row1["id"]."'>".$row1["reg"]."</option>"; 
                          $i++;
                    }
                    
            ?>

                   
                    </select>
</div>
 <div style="display: inline-block;width:280px; height:30px;">
                <b>Área</b>
                     <select name="sl_area" id="sl_area" onchange="filtros_s(this.value,4)">

<?php
                   $sqlarea = ' SELECT 0 id, \'Todos\' area 
                   union all
                   SELECT afuncional_id ,afuncional_nombre area FROM tbl_contareafuncional';
                     $i=0;
                    
                    foreach ($conn2->query($sqlarea) as $row1) {
                           if($row1["id"]==$area){
                              echo "<option  value='".$row1["id"]."' selected>".$row1["area"]."</option>"; 
                           }
                           else
                              echo "<option  value='".$row1["id"]."'>".$row1["area"]."</option>"; 
                          $i++;
                    }
                    
            ?>

                   
                    </select>
        </div>
        <div style="display: inline-block;width:280px; height:30px;">

                    <b>Segmento</b>
                     <select name="sl_seg" id="sl_seg" onchange="filtros_s(this.value,5)">

<?php
                   $sqlseg = ' SELECT 0 id, \'Todos\' nombre
                   union all
                   SELECT segmento_id, seg_nombre FROM tbl_contsegmento';
                     $i=0;
                    
                    foreach ($conn2->query($sqlseg) as $row1) {
                           if($row1["id"]==$seg){
                              echo "<option  value='".$row1["id"]."' selected>".$row1["nombre"]."</option>"; 
                           }
                           else
                              echo "<option  value='".$row1["id"]."'>".$row1["nombre"]."</option>"; 
                          $i++;
                    }
                    
            ?>

                   
                    </select>
          </div>
          <div style="display: inline-block;width:100px; height:30px;">
                <button type="reset" class="btn-xs btn-default " onclick="filtros_s('0',6)">Limpiar</button>
          </div>
          </form>
          
        </div>
		<div id="myOverlay"></div>
<div id="loadingGIF"> <img  src="https://www.mintbox.com/images/loading3.gif" align = "top" height="50%" width="50%"></div>

        <div class="row m-t">
          <div class="col-lg-12">
            <div class="row">
              <!-- Tabla Contratos -->
              <div class="col-lg-4">
                <div class="sbox">
                  <div class="sbox-title">
                    <span class="label label-success pull-right">Mes anterior</span>
                    <h4>Contratista</h4>
                  </div>
                  <div class="sbox-content" >
                    <table class='table table-striped' id="tbl_cont">
                      <tr style='font-weight:bold;'>
                        <td align="center">Contratistas</td>
                        <td align="center">Cantidad</td>
                        <td align="center" colspan="2">Riesgo</td>
                      </tr>
                      <tr>
                        <td></td>
                        <td></td>
                        <td align="center"><b>Alto</b></td>
                        <td align="center"><b>Bajo</b></td>
                      </tr>
                      <tr>
                        <td>Con Contrato</td>
                        <td align="right" id="tbl_cant_con">{!! $data[0]->con_alto + $data[0]->con_bajo !!}</td>
                        <td align="right" id="tbl_alto_con">{!! $data[0]->con_alto !!}</td>
                        <td align="right" id="tbl_bajo_con">{!! $data[0]->con_bajo !!}</td>
                      </tr>
                      <tr>
                        <td>Sin Contrato</td>
                        <td align="right" id="tbl_cant_sin">{!! $data[0]->sin_alto + $data[0]->sin_bajo !!}</td>
                        <td align="right" id="tbl_alto_sin">{!! $data[0]->sin_alto !!}</td>
                        <td align="right" id="tbl_bajo_sin">{!! $data[0]->sin_bajo !!}</td>
                      </tr>
                      <tr>
                        <td style='font-weight:bold;'>Total</td>
                        <td align="right" id="tbl_cant_tot">{!! $data[0]->con_alto + $data[0]->con_bajo + $data[0]->sin_alto + $data[0]->sin_bajo !!}</td>
                        <td align="right" id="tbl_alto_tot">{!! $data[0]->con_alto + $data[0]->sin_alto !!}</td>
                        <td align="right" id="tbl_bajo_tot">{!! $data[0]->con_bajo + $data[0]->sin_bajo !!}</td>
                      </tr>
                      </table>
                      <div class="stat-percent font-bold text-success">
                        <?php
                                $sql = 'SELECT Anio, NMes3L,
                                            YEAR(b.financieroand_fecha) AS AGNO
                                        ,   MONTH(b.financieroand_fecha) AS MES
                                        ,   REPLACE(REPLACE(REPLACE(FORMAT(SUM(b.financieroand_mtoreal), 0), ".", "@"), ",", "."), "@", ",") AS MtoPago FROM tbl_contrato AS a
                                        INNER JOIN tbl_financiero_and b ON a.contrato_id = b.contrato_id
                                        INNER JOIN dim_tiempo t ON b.financieroand_fecha = t.fecha
                                        WHERE
                                            b.financieroand_fecha = DATE_ADD(DATE_ADD(CURRENT_DATE, INTERVAL -2 MONTH), INTERVAL -(DAY(CURRENT_DATE)-1) DAY)
                                         ';
                            foreach ($conn2->query($sql) as $row) {
                                print $row['NMes3L']."-".$row['Anio']."\n";
                                }
                        ?><!--<i class="fa fa-level-up"></i>-->
                      </div>
                      <small>PERIODO</small>
                    </div>
                  </div><!-- /sbox -->
                </div><!-- </div class="col-lg-4"> -->
                <!-- Scatter Plot -->
                  <div class="col-lg-8">
                    <div class="sbox-content">
                      <div style="top: 10px;  left: 0;   width: 100%;" height="300px;background: white;">
                          <h4><center>Porcentaje de Completitud de Datos</h4>
                          <div id="donutContainer1" style="height: 185px; min-width: 310px; max-width: 600px; margin: 0 auto"></div>
                      </div>
                    </div>
                  </div>
                <!-- /Scatter Plot -->

              </div>

              <div class="row m-t">
                <div class="col-lg-6">
                  <div class="sbox-content">
                    <div style="top: 10px;  left: 0;   width: 100%;" height="300px;background: white;">
                        <h4><center>Distribución de Empresas por Tamaño</h4>
                        <div id="emptyPieContainer" style="height: 185px; min-width: 310px; max-width: 600px; margin: 0 auto"></div>
                    </div>
                  </div>
                </div>

                <div class="col-lg-6">
                  <div class="sbox-content">
                    <div style="top: 10px;  left: 0;   width: 100%;" height="300px;background: white;">
                        <h4><center>Distribución a Nivel Nacional</h4>
                        <div id="columnContainer2" style="height: 185px; min-width: 310px; max-width: 600px; margin: 0 auto"></div>
                    </div>
                  </div>
                </div>
              </div><!-- </div class="row-mt"> -->

              <div class="row m-t">
                <div class="col-lg-6">
                  <div class="sbox-content">
                    <div style="top: 10px;  left: 0;   width: 100%;" height="300px;background: white;">
                        <h4><center>Categorías de Empresas Proveedoras</h4>
                        <div id="columnContainer1" style="height: 185px; min-width: 310px; max-width: 600px; margin: 0 auto"></div>
                    </div>
                  </div>
                </div>
              </div><!-- </div class="row-mt"> -->
            </div>
          </div>


<script type="text/javascript">
$(function () {

    var colors = Highcharts.getOptions().colors,
        categories = ['Con ctto.', 'Sin ctto.'],
        data = [{
            y: {!! $data[0]->con_alto + $data[0]->con_bajo !!},
            color: colors[0],
            drilldown: {
                name: 'Completitud',
                categories: ['Completo', 'Incompleto'],
                data: [{!! $data[0]->con_completo !!}, {!! $data[0]->con_incompleto !!}],
                color: colors[0]
            }
        }, {
            y: {!! $data[0]->sin_alto + $data[0]->sin_bajo !!},
            color: colors[1],
            drilldown: {
                name: 'Completitud',
                categories: ['Completo', 'Incompleto'],
                data: [{!! $data[0]->sin_completo !!}, {!! $data[0]->sin_incompleto !!}],
                color: colors[0]
            }
        }],
        browserData = [],
        versionsData = [],
        i,
        j,
        dataLen = data.length,
        drillDataLen,
        brightness;


    // Build the data arrays
    for (i = 0; i < dataLen; i += 1) {

        // add browser data
        browserData.push({
            name: categories[i],
            y: data[i].y,
            color: data[i].color,
            parentId: i
        });

        // add version data
        drillDataLen = data[i].drilldown.data.length;
        for (j = 0; j < drillDataLen; j += 1) {
            brightness = 0.2 - (j / drillDataLen) / 5;
            versionsData.push({
                name: data[i].drilldown.categories[j],
                y: data[i].drilldown.data[j],
                color: Highcharts.Color(data[i].color).brighten(brightness).get(),
				 id: i
            });
        }
    }

    // Create the chart
    Highcharts.chart('donutContainer1', {
        chart: {
            type: 'pie'
        },
        title: {
            text: false
        },
        legend: {
          align: 'left',
          layout: 'vertical',
          verticalAlign: 'top',
          x: 0,
          y: 0
        },
        credits: {
           enabled: false
        },
        yAxis: {
            title: false,
          },
        plotOptions: {
            pie: {
                shadow: false,
                center: ['50%', '50%']
            },
            series: {
              point: {
                events: {
                    legendItemClick: function () {
                        var id = this.parentId,
                            data = this.series.chart.series[1].data;
                        $.each(data, function (i, point) {

                            if (point.id == id) {
                                if(point.visible)
                                    point.setVisible(false);
                                else
                                    point.setVisible(true);
                            }

                        });
                    }
                }
            }
        }
        },
        tooltip: {
            valueSuffix: ''
        },
        series: [{
            name: 'Estado Contrato',
            data: browserData,
            size: '60%',
            showInLegend: true,
            dataLabels: {
                formatter: function () {
                    return this.y > 5 ? this.point.name : null;
                },
                color: '#ffffff',
                distance: -30
            }
        }, {
            name: 'Completitud',
            data: versionsData,
            size: '80%',
            innerSize: '60%',
            dataLabels: {
                formatter: function () {
                    // display only if larger than 1
                    return this.y > 1 ? '<b>' + this.point.name + ':</b> ' + Highcharts.numberFormat(this.percentag, 1) : null;
                }
            }
        }],
    });
});

$(function () {
    Highcharts.chart('emptyPieContainer', {
        chart: {
            plotBackgroundColor: null,
            plotBorderWidth: null,
            plotShadow: false,
            type: 'pie',
            events: {
                load: addTitle,
                redraw: addTitle,
            },
        },
        title: false,
        credits: {
           enabled: false
        },
        tooltip: {
            pointFormat: '{series.name}: <b>{point.percentage:.1f}%</b>'
        },
        plotOptions: {
            pie: {
                allowPointSelect: true,
                cursor: 'pointer',
                innerSize: '70%',
                dataLabels: {
                    enabled: true,
                    format: '<b>{point.name}</b>: {point.percentage:.1f} %',
                    style: {
                        color: (Highcharts.theme && Highcharts.theme.contrastTextColor) || 'black'
                    }
                }
            }
        },

        series: [{
            name: 'Tipo de empresas',
            colorByPoint: true,
            data: [{
                name: 'PYME',
                y: {!! $data[0]->pyme !!}
            }, {
                name: 'Pequeña',
                y: {!! $data[0]->peq !!},
            }, {
                name: 'Mediana',
                y: {!! $data[0]->med !!}
            }, {
                name: 'Grande',
                y: {!! $data[0]->gran !!}
            }]
        }],
    });
});

$(function () {
    Highcharts.chart('columnContainer1', {
        chart: {
            type: 'column'
        },
        title: {
            text: false
        },
        credits: {
           enabled: false
        },
        legend: false,
        xAxis: {
            categories: [
                'Bienes y Productos',
                'Construcción',
                'Ingeniería y Consultoría',
                'Servicios Generales'
            ],
            crosshair: true
        },
        yAxis: {
            min: 0,
            title: {
                text: false
            }
        },
        tooltip: {
            headerFormat: '<span style="font-size:10px">{point.key}</span><table>',
            pointFormat: '<tr><td style="color:{series.color};padding:0">{series.name}: </td>' +
                '<td style="padding:0"><b>{point.y}</b></td></tr>',
            footerFormat: '</table>',
            shared: true,
            useHTML: true
        },
        plotOptions: {
            column: {
                pointPadding: 0.2,
                borderWidth: 0
            },
            series:{
              dataLabels: {
                  enabled: true
              },
            }
        },
        series: [{
            name: 'Tipos de Empresas',
            data: [{!! $cat !!}]

        }]
    });
});

$(function () {
    Highcharts.chart('columnContainer2', {
        chart: {
            type: 'column'
        },
        title: {
            text: false
        },
        credits: {
           enabled: false
        },
        legend: {
            enabled: false
        },
        xAxis: {
            categories: [
                'I',
                'II',
                'III',
                'IV',
                'V',
                'VI',
                'VII',
                'VIII',
                'IX',
                'X',
                'XI',
                'XII',
                'RM',
                'XIV',
                'XV'
            ],
            crosshair: true
        },
        yAxis: {
            min: 0,
            title: {
                text: false
            }
        },
        tooltip: {
            headerFormat: '<span style="font-size:10px">{point.key}</span><table>',
            pointFormat: '<tr><td style="color:{series.color};padding:0">{series.name}: </td>' +
                '<td style="padding:0"><b>{point.y}</b></td></tr>',
            footerFormat: '</table>',
            shared: true,
            useHTML: true
        },
        plotOptions: {
            column: {
                pointPadding: 0.2,
                borderWidth: 0
            },
            series:{
              dataLabels: {
                  enabled: true
              },
            }
        },
        series: [{
            name: 'Cantidad Empresas',
            data: [{!! $distr !!}]
        }]
    });
});

function addTitle() {

            if (this.title) {
                this.title.destroy();
            }

            var r = this.renderer,
                x = this.series[0].center[0] + this.plotLeft,
                y = this.series[0].center[1] + this.plotTop;
            this.title = r.text({!! $data[0]->pyme + $data[0]->peq + $data[0]->med + $data[0]->gran !!}+'<br>Empresas', 0, 0)
                .css({
                color: '#4572A7',
                fontSize: '12px'
            }).hide()
                .add();

            var bbox = this.title.getBBox();
            this.title.attr({
                x: x - (bbox.width / 2),
                y: y
            }).show();
        }

		
		
function filtros_s(str,num) {
 var mes_m,year_m,reg_m,seg_m,area_m;

 
 if(num ==6 ){
    mes_m = 0;
    year_m = <?php echo date("Y") ?>;
    reg_m = 0;
    seg_m = 0;
    area_m = 0;
  }
  else
 {
    mes_m =document.getElementById("sl_mes").value;
    year_m = document.getElementById("sl_year").value;
    reg_m = document.getElementById("sl_reg").value;
    seg_m = document.getElementById("sl_seg").value;
    area_m = document.getElementById("sl_area").value;
 }
    if (mes_m==1){
        nom_mes = 'Ene';
    }
    else  if (mes_m==2){
        nom_mes = 'Feb';
    }
    else  if (mes_m==3){
        nom_mes = 'Mar';
    }
    else  if (mes_m==4){
        nom_mes = 'Abr';
    }
    else  if (mes_m==5){
        nom_mes = 'May';
    }
    else  if (mes_m==6){
        nom_mes = 'Jun';
    }
    else  if (mes_m==7){
        nom_mes = 'Jul';
    }
    else  if (mes_m==8){
        nom_mes = 'Ago';
    }
    else  if (mes_m==9){
        nom_mes = 'Sep';
    }
    else  if (mes_m==10){
        nom_mes = 'Oct';
    }
    else  if (mes_m==11){
        nom_mes = 'Nov';
    }
    else  if (mes_m==12){
        nom_mes = 'Dic';
    }
    else  if (mes_m==0){
        nom_mes = 'Año';
    }

		var myLoad = document.createElement('img');
		var myCenter = document.createElement('center');
        
		myLoad.setAttribute("src","https://www.mintbox.com/images/loading3.gif");
      
		$('#myOverlay').show();
        $('#loadingGIF').show();

        if (window.XMLHttpRequest) {
            // code for IE7+, Firefox, Chrome, Opera, Safari
            xmlhttptabla = new XMLHttpRequest();
			xmlhttpnac = new XMLHttpRequest();
			xmlhttpcateg = new XMLHttpRequest();
            xmlhttpmenu = new XMLHttpRequest();
        } else {
            // code for IE6, IE5
            xmlhttptabla = new ActiveXObject("Microsoft.XMLHTTP");
			xmlhttpnac = new ActiveXObject("Microsoft.XMLHTTP");
			xmlhttpcateg = new ActiveXObject("Microsoft.XMLHTTP");
            xmlhttpmenu = new ActiveXObject("Microsoft.XMLHTTP");
        }
        
        
  
        xmlhttptabla.onreadystatechange = function() {
            if (this.readyState == 4 && this.status == 200) {
              console.log(this.responseText);
              var resp_gra = JSON.parse(this.responseText);
			  var tb_con_cant = document.getElementById('tbl_cant_con');
			  var tb_sin_cant = document.getElementById('tbl_cant_sin');
			  var tb_tot_cant = document.getElementById('tbl_cant_tot');
			  
			  var tb_con_alto = document.getElementById('tbl_alto_con');
			  var tb_sin_alto = document.getElementById('tbl_alto_sin');
			  var tb_tot_alto = document.getElementById('tbl_alto_tot');
			  
			  var tb_con_bajo = document.getElementById('tbl_bajo_con');
			  var tb_sin_bajo = document.getElementById('tbl_bajo_sin');
			  var tb_tot_bajo = document.getElementById('tbl_bajo_tot');
			  
			        var mes_m =document.getElementById("sl_mes").value;
					var year_m = document.getElementById("sl_year").value;
					var reg_m = document.getElementById("sl_reg").value;
					var seg_m = document.getElementById("sl_seg").value;
					var area_m = document.getElementById("sl_area").value;
					var color_v = '#3d9b35';
					var color_r = '#e64427';
					var color_n = '#FFC000';
					
					tb_con_cant.innerHTML = "";
					tb_sin_cant.innerHTML = "";
					tb_tot_cant.innerHTML = "";
					
					tb_con_alto.innerHTML = "";
					tb_sin_alto.innerHTML = "";
					tb_tot_alto.innerHTML = "";
					
					tb_con_bajo.innerHTML = "";
					tb_sin_bajo.innerHTML = "";
					tb_tot_bajo.innerHTML = "";
				
					
					//----------------
						
					tb_con_cant.innerHTML = Number(resp_gra[0]['con_alto']) + Number(resp_gra[0]['con_bajo']);
					tb_sin_cant.innerHTML = Number(resp_gra[0]['sin_alto']) + Number(resp_gra[0]['sin_bajo']);
					tb_tot_cant.innerHTML = Number(resp_gra[0]['con_alto']) + Number(resp_gra[0]['con_bajo']) + Number(resp_gra[0]['sin_alto']) + Number(resp_gra[0]['sin_bajo']);
					
					tb_con_alto.innerHTML = Number(resp_gra[0]['con_alto']) ;
					tb_sin_alto.innerHTML = Number(resp_gra[0]['sin_alto']);
					tb_tot_alto.innerHTML = Number(resp_gra[0]['sin_alto'])+Number(resp_gra[0]['con_alto']);
					
					tb_con_bajo.innerHTML = Number(resp_gra[0]['con_bajo']);
					tb_sin_bajo.innerHTML = Number(resp_gra[0]['sin_bajo']);
					tb_tot_bajo.innerHTML = Number(resp_gra[0]['sin_bajo']) + Number(resp_gra[0]['con_bajo']);
					
					//----------- completitud de datos
					
					
					var colors = Highcharts.getOptions().colors,
						categories = ['Con ctto.', 'Sin ctto.'],
						data = [{
							y:  Number(resp_gra[0]['con_bajo']) +  Number(resp_gra[0]['con_alto']),
							color: colors[0],
							drilldown: {
								name: 'Completitud',
								categories: ['Completo', 'Incompleto'],
								data: [Number(resp_gra[0]['con_completo']), Number(resp_gra[0]['con_incompleto'])],
								color: colors[0]
							}
						}, {
							y: Number(resp_gra[0]['sin_bajo']) +  Number(resp_gra[0]['sin_alto']),
							color: colors[1],
							drilldown: {
								name: 'Completitud',
								categories: ['Completo', 'Incompleto'],
								data: [Number(resp_gra[0]['sin_completo']), Number(resp_gra[0]['sin_incompleto'])],
								color: colors[0]
							}
						}],
						browserData = [],
						versionsData = [],
						i,
						j,
						dataLen = data.length,
						drillDataLen,
						brightness;
				
				
					// Build the data arrays
					for (i = 0; i < dataLen; i += 1) {
				
						// add browser data
						browserData.push({
							name: categories[i],
							y: data[i].y,
							color: data[i].color,
							parentId: i
						});
				
						// add version data
						drillDataLen = data[i].drilldown.data.length;
						for (j = 0; j < drillDataLen; j += 1) {
							brightness = 0.2 - (j / drillDataLen) / 5;
							versionsData.push({
								name: data[i].drilldown.categories[j],
								y: data[i].drilldown.data[j],
								color: Highcharts.Color(data[i].color).brighten(brightness).get(),
								id: i
							});
						}
					}
					
					var chart_comp = $('#donutContainer1').highcharts();
					chart_comp.series[0].setData([]); 
					chart_comp.series[1].setData([]);
					
					chart_comp.series[0].setData(browserData); 
					chart_comp.series[1].setData(versionsData);
					
					
					
					 Highcharts.chart('emptyPieContainer', {
								chart: {
									plotBackgroundColor: null,
									plotBorderWidth: null,
									plotShadow: false,
									type: 'pie',
									
								},
								title: false,
								credits: {
									enabled: false
								},
								tooltip: {
									pointFormat: '{series.name}: <b>{point.percentage:.1f}%</b>'
								},
								plotOptions: {
									pie: {
										allowPointSelect: true,
										cursor: 'pointer',
										innerSize: '70%',
										dataLabels: {
											enabled: true,
											format: '<b>{point.name}</b>: {point.percentage:.1f} %',
											style: {
												color: (Highcharts.theme && Highcharts.theme.contrastTextColor) || 'black'
											}
										}
									}
								},
							
								series: [{
									name: 'Tipo de empresas',
									colorByPoint: true,
									data: [{
										name: 'PYME',
										y: Number(resp_gra[0]['pyme']),
									}, {
										name: 'Pequeña',
										y: Number(resp_gra[0]['peq']),
									}, {
										name: 'Mediana',
										y: Number(resp_gra[0]['med'])
									}, {
										name: 'Grande',
										y: Number(resp_gra[0]['gran'])
									}]
								}],
							});
							var chart_dist = $('#emptyPieContainer').highcharts();
							
							 if (chart_dist.title) {
										chart_dist.title.destroy();
									}
							
									var r = chart_dist.renderer,
										x = chart_dist.series[0].center[0] + chart_dist.plotLeft,
										y = chart_dist.series[0].center[1] + chart_dist.plotTop;
									chart_dist.title = r.text(resp_gra[0]['total_cont']+'<br>Empresas', 0, 0)
										.css({
										color: '#4572A7',
										fontSize: '12px'
									}).hide()
										.add();
							
									var bbox = chart_dist.title.getBBox();
									chart_dist.title.attr({
										x: x - (bbox.width / 2),
										y: y
									}).show();
						//	chart_dist.setTitle({text: resp_gra[0]['total_cont'] + 'Empresas'});
						
						$('#myOverlay').hide();
						$('#loadingGIF').hide();

				
			}
        };
		
		
		xmlhttpnac.onreadystatechange = function() {
            if (this.readyState == 4 && this.status == 200) {
              console.log(this.responseText);
              var resp_dist = JSON.parse(this.responseText);
               
			   var chart_dist = $('#columnContainer2').highcharts();
			   
			   var datos_r = [];
			    for (i=0;i<resp_dist.length;i++)
                      {
                          
                              datos_r[i]=Number(resp_dist[i].count);
						  }
					  
				chart_dist.series[0].setData(datos_r);
                  
			}
        };
		
		xmlhttpcateg.onreadystatechange = function() {
            if (this.readyState == 4 && this.status == 200) {
              console.log(this.responseText);
              var resp_cat = JSON.parse(this.responseText);
               
			   var chart_cat = $('#columnContainer1').highcharts();
			   
			   var datos_r = [];
			    for (i=0;i<resp_cat.length;i++)
                      {
                          
                              datos_r[i]=Number(resp_cat[i].valor);
						  }
						  
				chart_cat.series[0].setData([]);	  
				chart_cat.series[0].setData(datos_r);
               

                 
			}
        };

         xmlhttpmenu.onreadystatechange = function() {
            if (this.readyState == 4 && this.status == 200) {
                document.getElementById("filtros").innerHTML = this.responseText;
               
            }
        };
       
    if(num==1)
    {
        
		xmlhttptabla.open("GET","dashboardcontratistas/show?year="+str+"&mes="+mes_m+"&reg="+reg_m+"&area="+area_m+"&seg="+seg_m+"&proc=0&tip=0",true);
		xmlhttpnac.open("GET","dashboardcontratistas/show?year="+str+"&mes="+mes_m+"&reg="+reg_m+"&area="+area_m+"&seg="+seg_m+"&proc=0&tip=1",true);
		xmlhttpcateg.open("GET","dashboardcontratistas/show?year="+str+"&mes="+mes_m+"&reg="+reg_m+"&area="+area_m+"&seg="+seg_m+"&proc=0&tip=2",true);
		
       xmlhttpmenu.open("GET","dashboardcontratistas/show?year="+str+"&mes="+mes_m+"&reg="+reg_m+"&area="+area_m+"&seg="+seg_m+"&proc=1",true);
      
       xmlhttpmenu.send();
    }
    else if(num==2){
      xmlhttptabla.open("GET","dashboardcontratistas/show?year="+year_m+"&mes="+str+"&reg="+reg_m+"&area="+area_m+"&seg="+seg_m+"&proc=0&tip=0",true);
	  xmlhttpnac.open("GET","dashboardcontratistas/show?year="+year_m+"&mes="+str+"&reg="+reg_m+"&area="+area_m+"&seg="+seg_m+"&proc=0&tip=1",true);
		xmlhttpcateg.open("GET","dashboardcontratistas/show?year="+year_m+"&mes="+str+"&reg="+reg_m+"&area="+area_m+"&seg="+seg_m+"&proc=0&tip=2",true);
		
	  
	}
    else if(num==3){
      xmlhttptabla.open("GET","dashboardcontratistas/show?year="+year_m+"&mes="+mes_m+"&reg="+str+"&area="+area_m+"&seg="+seg_m+"&proc=0&tip=0",true);
	  xmlhttpnac.open("GET","dashboardcontratistas/show?year="+year_m+"&mes="+mes_m+"&reg="+str+"&area="+area_m+"&seg="+seg_m+"&proc=0&tip=1",true);
		xmlhttpcateg.open("GET","dashboardcontratistas/show?year="+year_m+"&mes="+mes_m+"&reg="+str+"&area="+area_m+"&seg="+seg_m+"&proc=0&tip=2",true);
		
	}
    else if(num==4){
      xmlhttptabla.open("GET","dashboardcontratistas/show?year="+year_m+"&mes="+mes_m+"&reg="+reg_m+"&area="+str+"&seg="+seg_m+"&proc=0&tip=0",true);
	  xmlhttpnac.open("GET","dashboardcontratistas/show?year="+year_m+"&mes="+mes_m+"&reg="+reg_m+"&area="+str+"&seg="+seg_m+"&proc=0&tip=1",true);
		xmlhttpcateg.open("GET","dashboardcontratistas/show?year="+year_m+"&mes="+mes_m+"&reg="+reg_m+"&area="+str+"&seg="+seg_m+"&proc=0&tip=2",true);
		
	}
    else if(num==5){
      xmlhttptabla.open("GET","dashboardcontratistas/show?year="+year_m+"&mes="+mes_m+"&reg="+reg_m+"&area="+area_m+"&seg="+str+"&proc=0&tip=0",true);
	  xmlhttpnac.open("GET","dashboardcontratistas/show?year="+year_m+"&mes="+mes_m+"&reg="+reg_m+"&area="+area_m+"&seg="+str+"&proc=0&tip=1",true);
		xmlhttpcateg.open("GET","dashboardcontratistas/show?year="+year_m+"&mes="+mes_m+"&reg="+reg_m+"&area="+area_m+"&seg="+str+"&proc=0&tip=2",true);
		
	}
    else if(num==6)
    {
       xmlhttpmenu.open("GET","dashboardcontratistas/show?year="+year_m+"&mes=0&reg=0&area=0&seg=0&proc=1",true);
       xmlhttptabla.open("GET","dashboardcontratistas/show?year="+year_m+"&mes=0&reg=0&area=0&seg=0&tip=0&proc=0",true);
	   xmlhttpnac.open("GET","dashboardcontratistas/show?year="+year_m+"&mes=0&reg=0&area=0&seg=0&tip=1&proc=0",true);
		xmlhttpcateg.open("GET","dashboardcontratistas/show?year="+year_m+"&mes=0&reg=0&area=0&seg=0&tip=2&proc=0",true);
		
       xmlhttpmenu.send();
    }
    
xmlhttptabla.send();
xmlhttpnac.send();
xmlhttpcateg.send();
    
  
}
 </script>

    @endif

 @stop
