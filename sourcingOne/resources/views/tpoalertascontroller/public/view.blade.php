<div class="m-t" style="padding-top:25px;">	
    <div class="row m-b-lg animated fadeInDown delayp1 text-center">
        <h3> {{ $pageTitle }} <small> {{ $pageNote }} </small></h3>
        <hr />       
    </div>
</div>
<div class="m-t">
	<div class="table-responsive" > 	

		<table class="table table-striped table-bordered" >
			<tbody>	
		
			
					<tr>
						<td width='30%' class='label-view text-right'>{{ SiteHelpers::activeLang('Alert id Type', (isset($fields['tipo_alerta']['language'])? $fields['tipo_alerta']['language'] : array())) }}</td>
						<td>{{ $row->tipo_alerta}} </td>
						
					</tr>
				
					<tr>
						<td width='30%' class='label-view text-right'>{{ SiteHelpers::activeLang('Name of Alert type', (isset($fields['desc_tipo_alerta']['language'])? $fields['desc_tipo_alerta']['language'] : array())) }}</td>
						<td>{{ $row->desc_tipo_alerta}} </td>
						
					</tr>
				
					<tr>
						<td width='30%' class='label-view text-right'>{{ SiteHelpers::activeLang('Days of currency', (isset($fields['period']['language'])? $fields['period']['language'] : array())) }}</td>
						<td>{{ $row->period}} </td>
						
					</tr>
						
					<tr>
						<td width='30%' class='label-view text-right'></td>
						<td> <a href="javascript:history.go(-1)" class="btn btn-primary"> Back To Grid <a> </td>
						
					</tr>					
				
			</tbody>	
		</table>   

	 
	
	</div>
</div>	