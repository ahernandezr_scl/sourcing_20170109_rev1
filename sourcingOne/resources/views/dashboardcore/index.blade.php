@extends('layouts.app')
@section('content')

<?php
$servername = "localhost";
$username = "root";
$password = "8044302";

#try {
    $conn2 = new PDO("mysql:host=$servername;dbname=andina_koadmin", $username, $password);
    // set the PDO error mode to exception
#    $conn2->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
#   echo "Connected successfully";
#    }
#catch(PDOException $e)
#    {
#    echo "Connection failed: " . $e->getMessage();
#    }

 if (isset($_GET["year"])) {
    $year = $_GET["year"];
   }
  else{ 
	$year = date("Y");
	   
}

  if (isset($_GET["mes"])) {
    $mes = $_GET["mes"];
   }
 else
		{
			$mes = 0;
		}
	


 if (isset($_GET["reg"])) {
    $reg = $_GET["reg"];
   }
  else{  
    $reg = 0;
}

 if (isset($_GET["seg"])) {
    $seg = $_GET["seg"];
   }
  else{  
    $seg = 0;
}

 if (isset($_GET["area"])) {
    $area = $_GET["area"];
   }
  else{  
    $area = 0;
}

$nom_mes;
    if ($mes==1){
        $nom_mes = 'Ene';
    }
    else  if ($mes==2){
        $nom_mes = 'Feb';
    }
    else  if ($mes==3){
        $nom_mes = 'Mar';
    }
    else  if ($mes==4){
        $nom_mes = 'Abr';
    }
    else  if ($mes==5){
        $nom_mes = 'May';
    }
    else  if ($mes==6){
        $nom_mes = 'Jun';
    }
    else  if ($mes==7){
        $nom_mes = 'Jul';
    }
    else  if ($mes==8){
        $nom_mes = 'Ago';
    }
    else  if ($mes==9){
        $nom_mes = 'Sep';
    }
    else  if ($mes==10){
        $nom_mes = 'Oct';
    }
    else  if ($mes==11){
        $nom_mes = 'Nov';
    }
    else  if ($mes==12){
        $nom_mes = 'Dic';
    }
   $color_v = '#3d9b35';
   $color_r = '#e64427';
   $color_n = '#FFC000';


?>
<style>
      #embed-container {
                position: relative;
                padding-bottom: 56.25%;
                height: 0;
                overflow: hidden;
        }
        #embed-container iframe {
                position: absolute;
                top: 0;
                left: 0;
                width: 100%;
                height: 100%;
        }

#myOverlay{position:absolute;height:100%;width:100%;}
#myOverlay{background:white;opacity:.7;z-index:2;display:none;}

#loadingGIF{position:absolute;top:40%;left:45%;z-index:3;display:none;}

</style>

@if(Auth::check() && Auth::user()->group_id == 1)

    <script src="https://code.highcharts.com/highcharts.js"></script>
    <script src="https://code.highcharts.com/modules/exporting.js"></script>
    <script src="http://code.highcharts.com/highcharts-more.js"></script>
                                <tr>
                                    <td> <!--{{ Auth::user()->id }}--></td>
                                    	<!--{{ Auth::user()->id }}-->
                              </tr>
    <div class="embed-container">
	    <!-- -->
		<div class="content" align="center" id="filtros">
     <form method="post" id="filter">
   <div style="display: inline-block;width:70px; height:30px;">
      <b>Año</b>
                     <select name="sl_year" id="sl_year" onchange="filtros_s(this.value,1)">

<?php
                   $sqlyear = ' select distinct Anio from dim_tiempo 
                              where anio between year(current_Date) -3
                              and YEAR(CURRENT_DATE)
                              order by anio desc
                            ';
                     $i=0;
                    
                    foreach ($conn2->query($sqlyear) as $row1) {
                           if($row1["Anio"]==$year){
                              echo "<option  value='".$row1["Anio"]."' selected>".$row1["Anio"]."</option>"; 
                           }
                           else
                              echo "<option  value='".$row1["Anio"]."'>".$row1["Anio"]."</option>"; 
                          $i++;
                    }
                    
            ?>

                    </select>

       
       </div>  
       <div style="display: inline-block; width:80px; height:30px;">
            
           <b>Mes</b>
                     <select name="sl_mes" id="sl_mes" onchange="filtros_s(this.value,2)">

<?php


                   $sqlmes = ' SELECT 0 mes, \'Todos\' NMes3L
                   UNION ALL
                   SELECT DISTINCT mes , NMes3L  FROM dim_tiempo 
                   WHERE fecha_mes <= CURRENT_DATE
                   AND  anio = '.$year.' 
                   ORDER BY mes';
                     $i=0;
                    
                    foreach ($conn2->query($sqlmes) as $row1) {
                           if($row1["mes"]==$mes){
                              echo "<option  value='".$row1["mes"]."' selected>".$row1["NMes3L"]."</option>"; 
                           }
                           else
                              echo "<option  value='".$row1["mes"]."'>".$row1["NMes3L"]."</option>"; 
                          $i++;
                    }
                    
            ?>

                   
                    </select>
</div>
  <div style="display: inline-block;width:120px; height:30px;">
                <b>Tipo Faena</b>
                     <select name="sl_reg" id="sl_reg" onchange="filtros_s(this.value,3)">

<?php
                   $sqlreg = ' SELECT 0 id, \'Todos\' reg
                   UNION ALL
                   SELECT geo_id, geo_nombre FROM tbl_contgeografico';
                     $i=0;
                    
                    foreach ($conn2->query($sqlreg) as $row1) {
                           if($row1["id"]==$reg){
                              echo "<option  value='".$row1["id"]."' selected>".$row1["reg"]."</option>"; 
                           }
                           else
                              echo "<option  value='".$row1["id"]."'>".$row1["reg"]."</option>"; 
                          $i++;
                    }
                    
            ?>

                   
                    </select>
</div>
 <div style="display: inline-block;width:280px; height:30px;">
                <b>Área</b>
                     <select name="sl_area" id="sl_area" onchange="filtros_s(this.value,4)">

<?php
                   $sqlarea = ' SELECT 0 id, \'Todos\' area 
                   union all
                   SELECT afuncional_id ,afuncional_nombre area FROM tbl_contareafuncional';
                     $i=0;
                    
                    foreach ($conn2->query($sqlarea) as $row1) {
                           if($row1["id"]==$area){
                              echo "<option  value='".$row1["id"]."' selected>".$row1["area"]."</option>"; 
                           }
                           else
                              echo "<option  value='".$row1["id"]."'>".$row1["area"]."</option>"; 
                          $i++;
                    }
                    
            ?>

                   
                    </select>
        </div>
        <div style="display: inline-block;width:280px; height:30px;">

                    <b>Segmento</b>
                     <select name="sl_seg" id="sl_seg" onchange="filtros_s(this.value,5)">

<?php
                   $sqlseg = ' SELECT 0 id, \'Todos\' nombre
                   union all
                   SELECT segmento_id, seg_nombre FROM tbl_contsegmento';
                     $i=0;
                    
                    foreach ($conn2->query($sqlseg) as $row1) {
                           if($row1["id"]==$seg){
                              echo "<option  value='".$row1["id"]."' selected>".$row1["nombre"]."</option>"; 
                           }
                           else
                              echo "<option  value='".$row1["id"]."'>".$row1["nombre"]."</option>"; 
                          $i++;
                    }
                    
            ?>

                   
                    </select>
          </div>
          <div style="display: inline-block;width:100px; height:30px;">
                <button type="reset" class="btn-xs btn-default " onclick="filtros_s('0',6)">Limpiar</button>
          </div>
          </form>
          
        </div>
				<div id="myOverlay"></div>
<div id="loadingGIF"> <img  src="https://www.mintbox.com/images/loading3.gif" align = "top" height="50%" width="50%"></div>

        <div class="row m-t">
          <div class="col-lg-12">
            <div class="row">
              <!-- Tabla Contratos -->
              <div class="col-lg-8">
                <div class="sbox-content">
                  <div style="top: 10px;  left: 0;   width: 100%;" height="300px;background: white;">
                      <h4><center>Estado de Solicitudes</h4>
                      <div id="donutContainer1" style="height: 185px; min-width: 310px; max-width: 600px; margin: 0 auto"></div>
                  </div>
                </div>
                </div><!-- </div class="col-lg-4"> -->
                <!-- Scatter Plot -->

              </div>

              <div class="row m-t">
                <div class="col-lg-8">
                  <div class="sbox">
                    <div class="sbox-title">
                     
                      <h4>Estado Solicitudes según Entidad</h4>
                    </div>
                    <div class="sbox-content"  >
                      <table class='table table-striped' id="tabla_sol_ent">
                        <tr style='font-weight:bold;'>
                          <td align="center">Entidad</td>
                          <td align="center">Contratistas</td>
                          <td align="center">Contratos</td>
                          <td align="center">Personas</td>
                          <td align="center">Roles</td>
                          <td align="center">Áreas de Trabajo</td>
                          <td align="center">Centros</td>
                          
                          <td align="center">Total</td>
                        </tr>
                        <?php $totales = array();
                        for($i = 0; $i < count($status); $i++){ ?>
                          <?php if($i == 0){ echo '<tr><td align="left" style="font-weight:bold;">Por Cargar</td>';
                          } elseif($i == 1) { echo '<tr><td align="left" style="font-weight:bold;">Por Aprobar</td>';
                          } elseif($i == 2) { echo '<tr><td align="left" style="font-weight:bold;">No Aprobado</td>';
                          } elseif($i == 3) { echo '<tr><td align="left" style="font-weight:bold;">Temporal</td>';
                          } elseif($i == 4) { echo '<tr><td align="left" style="font-weight:bold;">Aprobado</td>';
                          } else { echo '<tr><td align="left" style="font-weight:bold;"></td>';
                          }
                          $aux = 0;
                          for($j = 0; $j < count($status[$i]); $j++){
                            echo '<td align="center">'.$status[$i][$j]->COUNT.'</td>';
                            $aux += $status[$i][$j]->COUNT;
                            if(isset($totales[$j])){
                              $totales[$j] += $status[$i][$j]->COUNT;
                            } else{
                              $totales[$j] = $status[$i][$j]->COUNT;
                            }
                          }

                          echo '<td align="center">'.$aux.'</td></tr>';
                        }
                        echo '<tr><td align="left" style="font-weight:bold;">Total</td>';
                        $final = 0;
                        for($i = 0; $i < count($totales); $i++){
                          echo '<td align="center">'.$totales[$i].'</td>';
                          $final += $totales[$i];
                        }
                        echo '<td align="center">'.$final.'</td></tr>';?>
                        </table>
                       
                      </div>
                    </div><!-- /sbox -->
                </div>

                <div class="col-lg-4">
                  <div class="sbox">
                    <div class="sbox-title">
                      
                      <h4>Documentos Por Cargar según Entidad</h4>
                    </div>
                    <div class="sbox-content"  >
                      <table class='table table-striped' id="tabla_doc_ent">
                        <tr>
                          <td align="left" style='font-weight:bold;'>Entidad</td>
                          <td align="center" style='font-weight:bold;'>30 Días</td>
                          <td align="center" style='font-weight:bold;'>90 Días</td>
                          <td align="center" style='font-weight:bold;'>180 Días</td>
                        </tr>
                        <?php foreach($vencePorCargar as $dato){
                          echo '<tr><td align="left" style="font-weight:bold;">'.$dato->nombreEntidad.'</td>';
                          echo '<td align="center">'.$dato->f1.'</td>';
                          echo '<td align="center">'.$dato->f2.'</td>';
                          echo '<td align="center">'.$dato->f3.'</td></tr>';
                        } ?>
                        </table>
                        
                      </div>
                    </div><!-- /sbox -->
                </div>
              </div><!-- </div class="row-mt"> -->

                           <div class="row m-t">
                <div class="col-lg-4">
                  <div class="sbox">
                    <div class="sbox-title">
                     
                      <h4>Solicitudes Por Aprobar</h4>
                    </div>
                    <div class="sbox-content">
                      <table class='table table-striped' id="tabla_apr">
                        <tr style='font-weight:bold;'>
                          <td align="center">Nombre EECC</td>
                          <td align="center">Cantidad</td>
                        </tr>
                        <?php $flag = false;
                        foreach($listas['pap'] as $tmp){
							
                          if($tmp->val != 0){
							echo '<tr>';
                            echo '<td>'.$tmp->RazonSocial.'</td>';
                            echo '<td align="center">'.$tmp->val.'</td>';
							echo '</tr>';
                            $flag = true;
                          }
                        }
                        if(!$flag){
                          echo '<td align="center" colspan="2">No se han encontrado solicitudes.</td>';
                        } ?>
                        </table>
                        
                      </div>
                    </div><!-- /sbox -->
                </div>
                <div class="col-lg-4">
                  <div class="sbox">
                    <div class="sbox-title">
                      
                      <h4>Solicitudes Temporales</h4>
                    </div>
                    <div class="sbox-content">
                      <table class='table table-striped' id="tabla_tmp">
                        <tr style='font-weight:bold;'>
                          <td align="center">Nombre EECC</td>
                          <td align="center">Cantidad</td>
                        </tr>
                        <?php $flag = false;
                        foreach($listas['tmp'] as $tmp){
                          if($tmp->val != 0){
							echo '</tr>';
                            echo '<td>'.$tmp->RazonSocial.'</td>';
                            echo '<td align="center">'.$tmp->val.'</td>';
							echo '</tr>';
                            $flag = true;
                          }
                        }
                        if(!$flag){
                          echo '<td align="center" colspan="2">No se han encontrado solicitudes.</td>';
                        } ?>
                        </table>
                        
                      </div>
                    </div><!-- /sbox -->
                </div>
                <div class="col-lg-4">
                  <div class="sbox">
                    <div class="sbox-title">
                      
                      <h4>Solicitudes No Aprobadas</h4>
                    </div>
                    <div class="sbox-content">
                      <table class='table table-striped' id="tabla_napr">
                        <tr style='font-weight:bold;'>
                          <td align="center">Nombre EECC</td>
                          <td align="center">Cantidad</td>
                        </tr>
                        <?php $flag = false;
                        foreach($listas['na'] as $tmp){
                          if($tmp->val != 0){
							echo '</tr>';
                            echo '<td>'.$tmp->RazonSocial.'</td>';
                            echo '<td align="center">'.$tmp->val.'</td>';
							echo '</tr>';
                            $flag = true;
                          }
                        }
                        if(!$flag){
                          echo '<td align="center" colspan="2">No se han encontrado solicitudes.</td>';
                        } ?>
                        </table>
                       
                      </div>
                    </div><!-- /sbox -->
                </div>
              </div><!-- </div class="row-mt"> -->
            </div>
          </div>

<script type="text/javascript">
$(function () {
    Highcharts.chart('donutContainer1', {
        chart: {
            plotBackgroundColor: null,
            plotBorderWidth: null,
            plotShadow: false,
            type: 'pie'
        },
        title: false,
        tooltip: {
            pointFormat: '{series.name}: <b>{point.y:.1f}</b>'
        },
        legend: {
          align: 'left',
          layout: 'vertical',
          verticalAlign: 'top',
          x: 0,
          y: 0
        },
        credits: false,
        plotOptions: {
            pie: {
                allowPointSelect: true,
                cursor: 'pointer',
                dataLabels: {
                    enabled: true,
                    format: '<b>{point.name}</b>: {point.percentage:.1f} %',
                    style: {
                        color: (Highcharts.theme && Highcharts.theme.contrastTextColor) || 'black'
                    }
                }
            }
        },
        series: [{
            name: 'Solicitudes',
            colorByPoint: true,
            showInLegend: true,
            data: [{
                name: '{!! $chart[0]->nombre !!}',
                y: {!! $chart[0]->val !!}
            }, {
                name: '{!! $chart[1]->nombre !!}',
                y: {!! $chart[1]->val !!},
                sliced: true,
                selected: true
            }, {
                name: '{!! $chart[2]->nombre !!}',
                y: {!! $chart[2]->val !!}
            }, {
                name: '{!! $chart[3]->nombre !!}',
                y: {!! $chart[3]->val !!}
            }, {
                name: '{!! $chart[4]->nombre !!}',
                y: {!! $chart[4]->val !!}
            }],

        }]
    });
});

function filtros_s(str,num) {
 var mes_m,year_m,reg_m,seg_m,area_m;

 
 if(num ==6 ){
    mes_m = 0;
    year_m = <?php echo date("Y") ?>;
    reg_m = 0;
    seg_m = 0;
    area_m = 0;
  }
  else
 {
    mes_m =document.getElementById("sl_mes").value;
    year_m = document.getElementById("sl_year").value;
    reg_m = document.getElementById("sl_reg").value;
    seg_m = document.getElementById("sl_seg").value;
    area_m = document.getElementById("sl_area").value;
 }
    if (mes_m==1){
        nom_mes = 'Ene';
    }
    else  if (mes_m==2){
        nom_mes = 'Feb';
    }
    else  if (mes_m==3){
        nom_mes = 'Mar';
    }
    else  if (mes_m==4){
        nom_mes = 'Abr';
    }
    else  if (mes_m==5){
        nom_mes = 'May';
    }
    else  if (mes_m==6){
        nom_mes = 'Jun';
    }
    else  if (mes_m==7){
        nom_mes = 'Jul';
    }
    else  if (mes_m==8){
        nom_mes = 'Ago';
    }
    else  if (mes_m==9){
        nom_mes = 'Sep';
    }
    else  if (mes_m==10){
        nom_mes = 'Oct';
    }
    else  if (mes_m==11){
        nom_mes = 'Nov';
    }
    else  if (mes_m==12){
        nom_mes = 'Dic';
    }
    else  if (mes_m==0){
        nom_mes = 'Año';
    }

		var myLoad = document.createElement('img');
		var myCenter = document.createElement('center');
        
		myLoad.setAttribute("src","https://www.mintbox.com/images/loading3.gif");
      
		$('#myOverlay').show();
        $('#loadingGIF').show();

        if (window.XMLHttpRequest) {
            // code for IE7+, Firefox, Chrome, Opera, Safari
            xmlhttptabla = new XMLHttpRequest();
			xmlhttpgrsol = new XMLHttpRequest();
			xmlhttptbdoc = new XMLHttpRequest();
			xmlhttptbapr = new XMLHttpRequest();
			xmlhttptbtmp = new XMLHttpRequest();
            xmlhttpmenu = new XMLHttpRequest();
			xmlhttptbnap = new XMLHttpRequest();

        } else {
            // code for IE6, IE5
            xmlhttptabla = new ActiveXObject("Microsoft.XMLHTTP");
			xmlhttpgrsol = new ActiveXObject("Microsoft.XMLHTTP");
			xmlhttptbdoc = new ActiveXObject("Microsoft.XMLHTTP");
			xmlhttptbapr = new ActiveXObject("Microsoft.XMLHTTP");
			xmlhttptbtmp = new ActiveXObject("Microsoft.XMLHTTP");
            xmlhttpmenu = new ActiveXObject("Microsoft.XMLHTTP");
			xmlhttptbnap = new ActiveXObject("Microsoft.XMLHTTP");
			
        }
        
        
  
        xmlhttptabla.onreadystatechange = function() {
            if (this.readyState == 4 && this.status == 200) {
              console.log(this.responseText);
             
			    var resp = JSON.parse(this.responseText);
				var tabla = document.getElementById("tabla_sol_ent");
				var myTBody = document.createElement('tbody');
				var myTr = document.createElement('tr');
				var myTd = document.createElement('td');
				var total=0;
				
				//tabla_doc_ent
				tabla.innerHTML = "";
				
				var myTable = document.createElement('table');
				myTable.className = 'table table-striped';
				
				myTd.setAttribute("style","font-weight:bold;");	
				tabla.appendChild(myTBody).appendChild(myTr.cloneNode());
				myTd.setAttribute("align","center");		
				   tabla.lastChild.lastChild.appendChild(myTd.cloneNode()).innerHTML = "Entidad";
				   
				    for (i = 0; i < resp.length; i += 1) {
						tabla.lastChild.lastChild.appendChild(myTd.cloneNode()).innerHTML = resp[i].nombreEntidad
					
					}
					tabla.lastChild.lastChild.appendChild(myTd.cloneNode()).innerHTML = "Total";
				 myTd.removeAttribute("style","font-weight:bold;");	
				   myTd.removeAttribute("align","center");
				   tabla.lastChild.appendChild(myTr.cloneNode());
				   myTd.setAttribute("align","left");		
				   myTd.setAttribute("style","font-weight:bold;");	
				   tabla.lastChild.lastChild.appendChild(myTd.cloneNode()).innerHTML = "Por Cargar";
				   myTd.removeAttribute("style","font-weight:bold;");	
				   myTd.removeAttribute("align","left");
				   myTd.setAttribute("align","center");
				   
				    for (i = 0; i < resp.length; i += 1) {
						
						tabla.lastChild.lastChild.appendChild(myTd.cloneNode()).innerHTML = resp[i].c_pc;
						total = total + Number(resp[i].c_pc);
					
					}
					
					tabla.lastChild.lastChild.appendChild(myTd.cloneNode()).innerHTML = total;
				 total = 0;
				   myTd.removeAttribute("align","center");
				   tabla.lastChild.appendChild(myTr.cloneNode());
				    myTd.setAttribute("align","left");		
					myTd.setAttribute("style","font-weight:bold;");	
				   tabla.lastChild.lastChild.appendChild(myTd.cloneNode()).innerHTML = "Por Aprobar";
				   myTd.removeAttribute("style","font-weight:bold;");	
				   myTd.removeAttribute("align","left");
				   myTd.setAttribute("align","center");
				    for (i = 0; i < resp.length; i += 1) {
						tabla.lastChild.lastChild.appendChild(myTd.cloneNode()).innerHTML = resp[i].c_pa;
						total = total + Number(resp[i].c_pa);
					
					}
					tabla.lastChild.lastChild.appendChild(myTd.cloneNode()).innerHTML = total;
				 total = 0;
				 
				   myTd.removeAttribute("align","center");
				   tabla.lastChild.appendChild(myTr.cloneNode());
				    myTd.setAttribute("align","left");	
					myTd.setAttribute("style","font-weight:bold;");						
				   tabla.lastChild.lastChild.appendChild(myTd.cloneNode()).innerHTML = "No Aprobado";
				   myTd.removeAttribute("style","font-weight:bold;");	
				   myTd.removeAttribute("align","left");
				   myTd.setAttribute("align","center");
				    for (i = 0; i < resp.length; i += 1) {
						tabla.lastChild.lastChild.appendChild(myTd.cloneNode()).innerHTML = resp[i].c_na;
						total = total + Number(resp[i].c_na);
					
					}
					tabla.lastChild.lastChild.appendChild(myTd.cloneNode()).innerHTML = total;
				 total = 0;
				 
				   myTd.removeAttribute("align","center");
				   tabla.lastChild.appendChild(myTr.cloneNode());
				    myTd.setAttribute("align","left");		
					myTd.setAttribute("style","font-weight:bold;");	
				   tabla.lastChild.lastChild.appendChild(myTd.cloneNode()).innerHTML = "Temporal";
				   myTd.removeAttribute("style","font-weight:bold;");	
				   myTd.removeAttribute("align","left");
				   myTd.setAttribute("align","center");
				    for (i = 0; i < resp.length; i += 1) {
						tabla.lastChild.lastChild.appendChild(myTd.cloneNode()).innerHTML = resp[i].c_tm;
						total = total + Number(resp[i].c_tm);
					
					}
					tabla.lastChild.lastChild.appendChild(myTd.cloneNode()).innerHTML = total;
				 total = 0;
				 
				   myTd.removeAttribute("align","center");
				   tabla.lastChild.appendChild(myTr.cloneNode());
				    myTd.setAttribute("align","left");		
					myTd.setAttribute("style","font-weight:bold;");	
				   tabla.lastChild.lastChild.appendChild(myTd.cloneNode()).innerHTML = "Aprobado";
				   myTd.removeAttribute("style","font-weight:bold;");	
				   myTd.removeAttribute("align","left");
				   myTd.setAttribute("align","center");
				    for (i = 0; i < resp.length; i += 1) {
						tabla.lastChild.lastChild.appendChild(myTd.cloneNode()).innerHTML = resp[i].c_ap;
						total = total + Number(resp[i].c_ap);
					
					}
					tabla.lastChild.lastChild.appendChild(myTd.cloneNode()).innerHTML = total;
				 total = 0;
				 
				   myTd.removeAttribute("align","center");
				 
					myTd.setAttribute("style","font-weight:bold;");	
					tabla.lastChild.appendChild(myTr.cloneNode());
					myTd.setAttribute("align","left");	
					tabla.lastChild.lastChild.appendChild(myTd.cloneNode()).innerHTML = "Total";
					myTd.removeAttribute("style","font-weight:bold;");	
				   myTd.removeAttribute("align","left");
				   myTd.setAttribute("align","center");
					for (i = 0; i < resp.length; i += 1) {
						tabla.lastChild.lastChild.appendChild(myTd.cloneNode()).innerHTML = Number(resp[i].c_pc)+Number(resp[i].c_tm)+Number(resp[i].c_pa)+Number(resp[i].c_na)+Number(resp[i].c_ap);
						total = total + Number(resp[i].c_pc)+Number(resp[i].c_tm)+Number(resp[i].c_pa)+Number(resp[i].c_na)+Number(resp[i].c_ap);
					
					}
					tabla.lastChild.lastChild.appendChild(myTd.cloneNode()).innerHTML = total;
					myTd.removeAttribute("align","center");
          /*     for (i = 0; i < resp.length; i += 1) {
				   tabla.lastChild.appendChild(myTr.cloneNode());
				   myTd.setAttribute("style","font-weight:bold;");	
				   myTd.setAttribute("align","left");	
				   tabla.lastChild.lastChild.appendChild(myTd.cloneNode()).innerHTML = resp[i].nombreEntidad;
				   myTd.removeAttribute("align","left");		
				   myTd.setAttribute("align","center");	
				   myTd.removeAttribute("style","font-weight:bold;");	
				   tabla.lastChild.lastChild.appendChild(myTd.cloneNode()).innerHTML = resp[i].f1;
				   tabla.lastChild.lastChild.appendChild(myTd.cloneNode()).innerHTML = resp[i].f2;
				   tabla.lastChild.lastChild.appendChild(myTd.cloneNode()).innerHTML = resp[i].f3;
				   myTd.removeAttribute("align","center");
			   
			   }*/

				
			}
        };
		
		
		xmlhttpgrsol.onreadystatechange = function() {
            if (this.readyState == 4 && this.status == 200) {
              console.log(this.responseText);
              var resp = JSON.parse(this.responseText);
			  var data = [];
			for (i = 0; i < resp.length; i += 1) {
				
						// add data
						data.push({
							name: resp[i].nombre,
							y: Number(resp[i].val)
						});
			}
			var chart_comp = $('#donutContainer1').highcharts();
			chart_comp.series[0].setData([]);
			
			chart_comp.series[0].setData(data);
			
			
			 
                  
			}
        };
		
		xmlhttptbdoc.onreadystatechange = function() {
            if (this.readyState == 4 && this.status == 200) {
              console.log(this.responseText);
             
			   var resp = JSON.parse(this.responseText);
				var tbdoc = document.getElementById("tabla_doc_ent");
				var myTBody = document.createElement('tbody');
				var myTr = document.createElement('tr');
				var myTd = document.createElement('td');
				
				//tabla_doc_ent
				tbdoc.innerHTML = "";
				
				var myTable = document.createElement('table');
				myTable.className = 'table table-striped';
				
				myTd.setAttribute("style","font-weight:bold;");	
				tbdoc.appendChild(myTBody).appendChild(myTr.cloneNode());
				myTd.setAttribute("align","left");		
				   tbdoc.lastChild.lastChild.appendChild(myTd.cloneNode()).innerHTML = "Entidad";
				   myTd.removeAttribute("align","left");		
				   myTd.setAttribute("align","center");	
									   
				   tbdoc.lastChild.lastChild.appendChild(myTd.cloneNode()).innerHTML = "30 Días";
				   tbdoc.lastChild.lastChild.appendChild(myTd.cloneNode()).innerHTML = "90 Días";
				   tbdoc.lastChild.lastChild.appendChild(myTd.cloneNode()).innerHTML = "100 Días";
				   myTd.removeAttribute("align","center");
				   
				
               for (i = 0; i < resp.length; i += 1) {
				   tbdoc.lastChild.appendChild(myTr.cloneNode());
				   myTd.setAttribute("style","font-weight:bold;");	
				   myTd.setAttribute("align","left");	
				   tbdoc.lastChild.lastChild.appendChild(myTd.cloneNode()).innerHTML = resp[i].nombreEntidad;
				   myTd.removeAttribute("align","left");		
				   myTd.setAttribute("align","center");	
				   myTd.removeAttribute("style","font-weight:bold;");	
				   tbdoc.lastChild.lastChild.appendChild(myTd.cloneNode()).innerHTML = resp[i].f1;
				   tbdoc.lastChild.lastChild.appendChild(myTd.cloneNode()).innerHTML = resp[i].f2;
				   tbdoc.lastChild.lastChild.appendChild(myTd.cloneNode()).innerHTML = resp[i].f3;
				   myTd.removeAttribute("align","center");
			   
			   }

                 
			}
        };
		
		xmlhttptbapr.onreadystatechange = function() {
            if (this.readyState == 4 && this.status == 200) {
              console.log(this.responseText);
             
               
			   var resp = JSON.parse(this.responseText);
			//tabla_apr
			
			 
				var tbapr = document.getElementById("tabla_apr");
				var myTBody = document.createElement('tbody');
				var myTr = document.createElement('tr');
				var myTd = document.createElement('td');
				tbapr.innerHTML = "";
				myTd.setAttribute("style","font-weight:bold;");	
				myTd.setAttribute("align","center");	
				tbapr.appendChild(myTBody).appendChild(myTr.cloneNode());
				tbapr.lastChild.lastChild.appendChild(myTd.cloneNode()).innerHTML = "Nombre EECC";
				tbapr.lastChild.lastChild.appendChild(myTd.cloneNode()).innerHTML = "Cantidad";
				myTd.removeAttribute("align","center");	
				myTd.removeAttribute("style","font-weight:bold;");	
				if (resp.length == 0){
					tbapr.lastChild.appendChild(myTr.cloneNode());
					myTd.setAttribute("colspan","2");
					myTd.setAttribute("align","center");
						tbapr.lastChild.lastChild.appendChild(myTd.cloneNode()).innerHTML = "No se han encontrado solicitudes.";
					myTd.removeAttribute("align","center");	
				}
				else {
					myTd.setAttribute("align","left");	
					for (i = 0; i < resp.length; i += 1) {
						tbapr.lastChild.appendChild(myTr.cloneNode());
						myTd.setAttribute("align","left");	
						tbapr.lastChild.lastChild.appendChild(myTd.cloneNode()).innerHTML = resp[i].RazonSocial;
						myTd.removeAttribute("align","left");	
						myTd.setAttribute("align","center");	
						tbapr.lastChild.lastChild.appendChild(myTd.cloneNode()).innerHTML = resp[i].val;
						myTd.removeAttribute("align","center");
						
					}
					myTd.removeAttribute("align","left");	
				}
				
                 
			}
        };
		
		
		xmlhttptbtmp.onreadystatechange = function() {
            if (this.readyState == 4 && this.status == 200) {
              console.log(this.responseText);
                            
			  
               
			   var resp = JSON.parse(this.responseText);
			//tabla_tmp
			
			 
				var tbapr = document.getElementById("tabla_tmp");
				var myTBody = document.createElement('tbody');
				var myTr = document.createElement('tr');
				var myTd = document.createElement('td');
				tbapr.innerHTML = "";
				myTd.setAttribute("style","font-weight:bold;");	
				myTd.setAttribute("align","center");	
				tbapr.appendChild(myTBody).appendChild(myTr.cloneNode());
				tbapr.lastChild.lastChild.appendChild(myTd.cloneNode()).innerHTML = "Nombre EECC";
				tbapr.lastChild.lastChild.appendChild(myTd.cloneNode()).innerHTML = "Cantidad";
				myTd.removeAttribute("align","center");	
				myTd.removeAttribute("style","font-weight:bold;");	
				if (resp.length == 0){
					tbapr.lastChild.appendChild(myTr.cloneNode());
					myTd.setAttribute("colspan","2");
					myTd.setAttribute("align","center");
						tbapr.lastChild.lastChild.appendChild(myTd.cloneNode()).innerHTML = "No se han encontrado solicitudes.";
					myTd.removeAttribute("align","center");	
				}
				else {
					myTd.setAttribute("align","left");	
					for (i = 0; i < resp.length; i += 1) {
						tbapr.lastChild.appendChild(myTr.cloneNode());
						myTd.setAttribute("align","left");	
						tbapr.lastChild.lastChild.appendChild(myTd.cloneNode()).innerHTML = resp[i].RazonSocial;
						myTd.removeAttribute("align","left");	
						myTd.setAttribute("align","center");	
						tbapr.lastChild.lastChild.appendChild(myTd.cloneNode()).innerHTML = resp[i].val;
						myTd.removeAttribute("align","center");
						
					}
					myTd.removeAttribute("align","left");	
				}
			

                 
			}
        };
		
		xmlhttptbnap.onreadystatechange = function() {
            if (this.readyState == 4 && this.status == 200) {
              console.log(this.responseText);
             
               
			   var resp = JSON.parse(this.responseText);
			//tabla_napr
			
			 
				var tbapr = document.getElementById("tabla_napr");
				var myTBody = document.createElement('tbody');
				var myTr = document.createElement('tr');
				var myTd = document.createElement('td');
				tbapr.innerHTML = "";
				myTd.setAttribute("style","font-weight:bold;");	
				myTd.setAttribute("align","center");	
				tbapr.appendChild(myTBody).appendChild(myTr.cloneNode());
				tbapr.lastChild.lastChild.appendChild(myTd.cloneNode()).innerHTML = "Nombre EECC";
				tbapr.lastChild.lastChild.appendChild(myTd.cloneNode()).innerHTML = "Cantidad";
				myTd.removeAttribute("align","center");	
				myTd.removeAttribute("style","font-weight:bold;");	
				if (resp.length == 0){
					tbapr.lastChild.appendChild(myTr.cloneNode());
					myTd.setAttribute("colspan","2");
					myTd.setAttribute("align","center");
						tbapr.lastChild.lastChild.appendChild(myTd.cloneNode()).innerHTML = "No se han encontrado solicitudes.";
					myTd.removeAttribute("align","center");	
				}
				else {
					
					for (i = 0; i < resp.length; i += 1) {
						tbapr.lastChild.appendChild(myTr.cloneNode());
						myTd.setAttribute("align","left");	
						tbapr.lastChild.lastChild.appendChild(myTd.cloneNode()).innerHTML = resp[i].RazonSocial;
						myTd.removeAttribute("align","left");	
						myTd.setAttribute("align","center");	
						tbapr.lastChild.lastChild.appendChild(myTd.cloneNode()).innerHTML = resp[i].val;
						myTd.removeAttribute("align","center");
						
					}
					
				}
				   $('#myOverlay').hide();
                     $('#loadingGIF').hide();
                 
			}
        };
		
               

         xmlhttpmenu.onreadystatechange = function() {
            if (this.readyState == 4 && this.status == 200) {
                document.getElementById("filtros").innerHTML = this.responseText;
               
            }
        };
       
    if(num==1)
    {
        
		xmlhttptabla.open("GET","dashboardcore/show?year="+str+"&mes="+mes_m+"&reg="+reg_m+"&area="+area_m+"&seg="+seg_m+"&proc=0&tip=4",true);
		xmlhttpgrsol.open("GET","dashboardcore/show?year="+str+"&mes="+mes_m+"&reg="+reg_m+"&area="+area_m+"&seg="+seg_m+"&proc=0&tip=1",true);
		xmlhttptbdoc.open("GET","dashboardcore/show?year="+str+"&mes="+mes_m+"&reg="+reg_m+"&area="+area_m+"&seg="+seg_m+"&proc=0&tip=0",true);
		xmlhttptbapr.open("GET","dashboardcore/show?year="+str+"&mes="+mes_m+"&reg="+reg_m+"&area="+area_m+"&seg="+seg_m+"&proc=0&tip=2",true);
		xmlhttptbtmp.open("GET","dashboardcore/show?year="+str+"&mes="+mes_m+"&reg="+reg_m+"&area="+area_m+"&seg="+seg_m+"&proc=0&tip=3",true);
		xmlhttptbnap.open("GET","dashboardcore/show?year="+str+"&mes="+mes_m+"&reg="+reg_m+"&area="+area_m+"&seg="+seg_m+"&proc=0&tip=5",true);
		
        xmlhttpmenu.open("GET","dashboardcore/show?year="+str+"&mes="+mes_m+"&reg="+reg_m+"&area="+area_m+"&seg="+seg_m+"&proc=1",true);
      
       xmlhttpmenu.send();
    }
    else if(num==2){
		xmlhttptabla.open("GET","dashboardcore/show?year="+year_m+"&mes="+str+"&reg="+reg_m+"&area="+area_m+"&seg="+seg_m+"&proc=0&tip=4",true);
		xmlhttpgrsol.open("GET","dashboardcore/show?year="+year_m+"&mes="+str+"&reg="+reg_m+"&area="+area_m+"&seg="+seg_m+"&proc=0&tip=1",true);
		xmlhttptbdoc.open("GET","dashboardcore/show?year="+year_m+"&mes="+str+"&reg="+reg_m+"&area="+area_m+"&seg="+seg_m+"&proc=0&tip=0",true);
		xmlhttptbapr.open("GET","dashboardcore/show?year="+year_m+"&mes="+str+"&reg="+reg_m+"&area="+area_m+"&seg="+seg_m+"&proc=0&tip=2",true);
		xmlhttptbtmp.open("GET","dashboardcore/show?year="+year_m+"&mes="+str+"&reg="+reg_m+"&area="+area_m+"&seg="+seg_m+"&proc=0&tip=3",true);
		xmlhttptbnap.open("GET","dashboardcore/show?year="+year_m+"&mes="+str+"&reg="+reg_m+"&area="+area_m+"&seg="+seg_m+"&proc=0&tip=5",true);
	
	}
    else if(num==3){
   		xmlhttptabla.open("GET","dashboardcore/show?year="+year_m+"&mes="+mes_m+"&reg="+str+"&area="+area_m+"&seg="+seg_m+"&proc=0&tip=4",true);
		xmlhttpgrsol.open("GET","dashboardcore/show?year="+year_m+"&mes="+mes_m+"&reg="+str+"&area="+area_m+"&seg="+seg_m+"&proc=0&tip=1",true);
		xmlhttptbdoc.open("GET","dashboardcore/show?year="+year_m+"&mes="+mes_m+"&reg="+str+"&area="+area_m+"&seg="+seg_m+"&proc=0&tip=0",true);
		xmlhttptbapr.open("GET","dashboardcore/show?year="+year_m+"&mes="+mes_m+"&reg="+str+"&area="+area_m+"&seg="+seg_m+"&proc=0&tip=2",true);
		xmlhttptbtmp.open("GET","dashboardcore/show?year="+year_m+"&mes="+mes_m+"&reg="+str+"&area="+area_m+"&seg="+seg_m+"&proc=0&tip=3",true);
		xmlhttptbnap.open("GET","dashboardcore/show?year="+year_m+"&mes="+mes_m+"&reg="+str+"&area="+area_m+"&seg="+seg_m+"&proc=0&tip=5",true);
		
	}
    else if(num==4){
		xmlhttptabla.open("GET","dashboardcore/show?year="+year_m+"&mes="+mes_m+"&reg="+reg_m+"&area="+str+"&seg="+seg_m+"&proc=0&tip=4",true);
		xmlhttpgrsol.open("GET","dashboardcore/show?year="+year_m+"&mes="+mes_m+"&reg="+reg_m+"&area="+str+"&seg="+seg_m+"&proc=0&tip=1",true);
		xmlhttptbdoc.open("GET","dashboardcore/show?year="+year_m+"&mes="+mes_m+"&reg="+reg_m+"&area="+str+"&seg="+seg_m+"&proc=0&tip=0",true);
		xmlhttptbapr.open("GET","dashboardcore/show?year="+year_m+"&mes="+mes_m+"&reg="+reg_m+"&area="+str+"&seg="+seg_m+"&proc=0&tip=2",true);
		xmlhttptbtmp.open("GET","dashboardcore/show?year="+year_m+"&mes="+mes_m+"&reg="+reg_m+"&area="+str+"&seg="+seg_m+"&proc=0&tip=3",true);
		xmlhttptbnap.open("GET","dashboardcore/show?year="+year_m+"&mes="+mes_m+"&reg="+reg_m+"&area="+str+"&seg="+seg_m+"&proc=0&tip=5",true);
	 
	}
    else if(num==5){
		xmlhttptabla.open("GET","dashboardcore/show?year="+year_m+"&mes="+mes_m+"&reg="+reg_m+"&area="+area_m+"&seg="+str+"&proc=0&tip=4",true);
		xmlhttpgrsol.open("GET","dashboardcore/show?year="+year_m+"&mes="+mes_m+"&reg="+reg_m+"&area="+area_m+"&seg="+str+"&proc=0&tip=1",true);
		xmlhttptbdoc.open("GET","dashboardcore/show?year="+year_m+"&mes="+mes_m+"&reg="+reg_m+"&area="+area_m+"&seg="+str+"&proc=0&tip=0",true);
		xmlhttptbapr.open("GET","dashboardcore/show?year="+year_m+"&mes="+mes_m+"&reg="+reg_m+"&area="+area_m+"&seg="+str+"&proc=0&tip=2",true);
		xmlhttptbtmp.open("GET","dashboardcore/show?year="+year_m+"&mes="+mes_m+"&reg="+reg_m+"&area="+area_m+"&seg="+str+"&proc=0&tip=3",true);
		xmlhttptbnap.open("GET","dashboardcore/show?year="+year_m+"&mes="+mes_m+"&reg="+reg_m+"&area="+area_m+"&seg="+str+"&proc=0&tip=5",true);
	   
	}
    else if(num==6)
    {
       xmlhttpmenu.open("GET","dashboardcore/show?year="+year_m+"&mes=0&reg=0&area=0&seg=0&proc=1",true);
    	xmlhttptabla.open("GET","dashboardcore/show?year="+year_m+"&mes=0&reg=0&area=0&seg=0&proc=0&tip=4",true);
		xmlhttpgrsol.open("GET","dashboardcore/show?year="+year_m+"&mes=0&reg=0&area=0&seg=0&proc=0&tip=1",true);
		xmlhttptbdoc.open("GET","dashboardcore/show?year="+year_m+"&mes=0&reg=0&area=0&seg=0&proc=0&tip=0",true);
		xmlhttptbapr.open("GET","dashboardcore/show?year="+year_m+"&mes=0&reg=0&area=0&seg=0&proc=0&tip=2",true);
		xmlhttptbtmp.open("GET","dashboardcore/show?year="+year_m+"&mes=0&reg=0&area=0&seg=0&proc=0&tip=3",true);
		xmlhttptbnap.open("GET","dashboardcore/show?year="+year_m+"&mes=0&reg=0&area=0&seg=0&proc=0&tip=5",true);
		
		
       xmlhttpmenu.send();
    }
    
xmlhttptabla.send();
xmlhttpgrsol.send();
xmlhttptbdoc.send();
xmlhttptbapr.send();
xmlhttptbtmp.send();
xmlhttptbnap.send();

    
  
}




 </script>

    @endif

 @stop
