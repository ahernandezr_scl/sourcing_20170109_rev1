<?php
$servername = "localhost";
$username = "root";
$password = "8044302";
$year;
$mes;$reg;

#try {
    $conn2 = new PDO("mysql:host=$servername;dbname=andina_koadmin", $username, $password);
    // set the PDO error mode to exception
#    $conn2->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
#   echo "Connected successfully";
#    }
#catch(PDOException $e)
#    {
#    echo "Connection failed: " . $e->getMessage();
#    }


?>

<?php  
  if (isset($_GET["year"])) {
    $year = $_GET["year"];
   }
  else{  
    $year = date("Y");
}

  if (isset($_GET["mes"])) {
    $mes = $_GET["mes"];
   }
  else{  
    $mes = 0;
}

 if (isset($_GET["reg"])) {
    $reg = $_GET["reg"];
   }
  else{  
    $reg = 0;
}

 if (isset($_GET["seg"])) {
    $seg = $_GET["seg"];
   }
  else{  
    $seg = 0;
}

 if (isset($_GET["area"])) {
    $area = $_GET["area"];
   }
  else{  
    $area = 0;
}


 if (isset($_GET["proc"])) {
    $proc = $_GET["proc"];
   }
  else{  
    $proc = 0;
}

 if (isset($_GET["tip"])) {
    $tip = $_GET["tip"];
   }
  else{  
    $tip = 0;
}
?>
<?php
$nom_mes;
    if ($mes==1){
        $nom_mes = 'Ene';
    }
    else  if ($mes==2){
        $nom_mes = 'Feb';
    }
    else  if ($mes==3){
        $nom_mes = 'Mar';
    }
    else  if ($mes==4){
        $nom_mes = 'Abr';
    }
    else  if ($mes==5){
        $nom_mes = 'May';
    }
    else  if ($mes==6){
        $nom_mes = 'Jun';
    }
    else  if ($mes==7){
        $nom_mes = 'Jul';
    }
    else  if ($mes==8){
        $nom_mes = 'Ago';
    }
    else  if ($mes==9){
        $nom_mes = 'Sep';
    }
    else  if ($mes==10){
        $nom_mes = 'Oct';
    }
    else  if ($mes==11){
        $nom_mes = 'Nov';
    }
    else  if ($mes==12){
        $nom_mes = 'Dic';
    }
   $color_v = '#3d9b35';
   $color_r = '#e64427';
   $color_n = '#FFC000';

?>


  
<?php 
if($proc == 0){
     
                $sql_tabla = 'SELECT e.IdEntidad AS id, e.Entidad AS nombreEntidad,
								COUNT(CASE  WHEN d.IdEstatus = 1 THEN d.idDocumento END)  c_pc,
								COUNT(CASE  WHEN d.IdEstatus = 2 THEN d.idDocumento END) c_pa,
								COUNT(CASE  WHEN d.IdEstatus = 3 THEN d.idDocumento END)  c_na,
								COUNT(CASE  WHEN d.IdEstatus = 4 THEN d.idDocumento END) c_tm,
								COUNT(CASE  WHEN d.IdEstatus = 5 THEN d.idDocumento END) c_ap
								
								FROM tbl_entidades e
								LEFT JOIN (SELECT IdEstatus, entidad,idDocumento  FROM tbl_documentos d INNER JOIN tbl_contrato c ON d.contrato_id = c.contrato_id
								where 
									CASE WHEN '.$seg.' = 0 THEN 1=1 ELSE  segmento_id = '.$seg.' END
									AND CASE WHEN '.$reg.' = 0 THEN 1=1 ELSE  geo_id = '.$reg.' END
									AND CASE WHEN '.$area.' = 0 THEN 1=1 ELSE  afuncional_id = '.$area.' END
									AND CASE WHEN '.$mes.' = 0 THEN YEAR(cont_fechaFin) >= '.$year.' 
									ELSE cont_fechaFin >= CONCAT(CONCAT(CONCAT(\''.$year.'\',\'-\'),\''.$mes.'\'),\'-01\') 
									and cont_fechaInicio <= CONCAT(CONCAT(CONCAT(\''.$year.'\',\'-\'),\''.$mes.'\'),\'-01\') 
									END
								)
								d ON  e.IdEntidad = d.Entidad 
								GROUP BY 1,2
					';
					
				$sql_sol = 'SELECT nombre, COUNT(IdEstatus) AS val FROM (SELECT IdEstatus, entidad,idDocumento  
				
								FROM tbl_documentos d INNER JOIN tbl_contrato c ON d.contrato_id = c.contrato_id
								where 
									CASE WHEN '.$seg.' = 0 THEN 1=1 ELSE  segmento_id = '.$seg.' END
									AND CASE WHEN '.$reg.' = 0 THEN 1=1 ELSE  geo_id = '.$reg.' END
									AND CASE WHEN '.$area.' = 0 THEN 1=1 ELSE  afuncional_id = '.$area.' END
									AND CASE WHEN '.$mes.' = 0 THEN YEAR(cont_fechaFin) >= '.$year.' 
									ELSE cont_fechaFin >= CONCAT(CONCAT(CONCAT(\''.$year.'\',\'-\'),\''.$mes.'\'),\'-01\') 
									and cont_fechaInicio <= CONCAT(CONCAT(CONCAT(\''.$year.'\',\'-\'),\''.$mes.'\'),\'-01\') 
									END
								) a
								RIGHT JOIN

								(SELECT 1 categ, "Por cargar" nombre
								UNION ALL
								SELECT 2, "Por aprobar" nombre
								UNION ALL
								SELECT 3, "No aprobado" nombre
								UNION
								SELECT 4, "Temporal" nombre
								UNION
								SELECT 5, "Aprobado" nombre) AS t ON a.IdEstatus = t.categ

								GROUP BY 1';
					
					
				$sql_doc = 'SELECT e.IdEntidad  id, e.Entidad  nombreEntidad,
						       COUNT(CASE WHEN DATEDIFF(CURRENT_DATE,d.createdOn) BETWEEN 0 AND 30 THEN d.Entidad END) AS f1,
						       COUNT(CASE WHEN DATEDIFF(CURRENT_DATE,d.createdOn) BETWEEN 31 AND 90 THEN d.Entidad END) AS f2,
						       COUNT(CASE WHEN DATEDIFF(CURRENT_DATE,d.createdOn) BETWEEN 91 AND 180 THEN d.Entidad END) AS f3
						FROM tbl_entidades e
						LEFT JOIN (SELECT IdEstatus, entidad,idDocumento,d.createdOn  FROM tbl_documentos d
						INNER JOIN tbl_contrato c ON d.contrato_id = c.contrato_id
						WHERE 
					CASE WHEN '.$seg.' = 0 THEN 1=1 ELSE  segmento_id = '.$seg.' END
					AND CASE WHEN '.$reg.' = 0 THEN 1=1 ELSE  geo_id = '.$reg.' END
					AND CASE WHEN '.$area.' = 0 THEN 1=1 ELSE  afuncional_id = '.$area.' END
					AND CASE WHEN '.$mes.' = 0 THEN YEAR(cont_fechaFin) >= '.$year.' ELSE cont_fechaFin >= CONCAT(CONCAT(CONCAT(\''.$year.'\',\'-\'),\''.$mes.'\'+1),\'-01\') END
					
						) d ON  e.IdEntidad = d.Entidad AND d.idEstatus = 1
						GROUP BY 1;
				 ';
					
				$sql_apr = 'SELECT a.RazonSocial, COUNT(d.IdDocumento) AS val
							FROM tbl_contratistas a
							LEFT JOIN tbl_contrato c ON a.IdContratista = c.IdContratista
							LEFT JOIN tbl_documentos d ON d.contrato_id = c.contrato_id AND d.IdEstatus = 2

							WHERE 
								CASE WHEN '.$seg.' = 0 THEN 1=1 ELSE  c.segmento_id = '.$seg.' END
								AND CASE WHEN '.$reg.' = 0 THEN 1=1 ELSE  c.geo_id = '.$reg.' END
								AND CASE WHEN '.$area.' = 0 THEN 1=1 ELSE  c.afuncional_id = '.$area.' END
								AND CASE WHEN '.$mes.' = 0 THEN YEAR(c.cont_fechaFin) >= '.$year.' ELSE c.cont_fechaFin >= CONCAT(CONCAT(CONCAT(\''.$year.'\',\'-\'),\''.$mes.'\'+1),\'-01\') END
								GROUP BY 1
								HAVING val >0';
					
				$sql_tmp ='SELECT a.RazonSocial, COUNT(d.IdDocumento) AS val
							FROM tbl_contratistas a
							LEFT JOIN tbl_contrato c ON a.IdContratista = c.IdContratista
							LEFT JOIN tbl_documentos d ON d.contrato_id = c.contrato_id AND d.IdEstatus = 4

							WHERE 
								CASE WHEN '.$seg.' = 0 THEN 1=1 ELSE  c.segmento_id = '.$seg.' END
								AND CASE WHEN '.$reg.' = 0 THEN 1=1 ELSE  c.geo_id = '.$reg.' END
								AND CASE WHEN '.$area.' = 0 THEN 1=1 ELSE  c.afuncional_id = '.$area.' END
								AND CASE WHEN '.$mes.' = 0 THEN YEAR(c.cont_fechaFin) >= '.$year.' ELSE c.cont_fechaFin >= CONCAT(CONCAT(CONCAT(\''.$year.'\',\'-\'),\''.$mes.'\'+1),\'-01\') END
								GROUP BY 1
								HAVING val >0';
				$sql_nap = 'SELECT a.RazonSocial, COUNT(d.IdDocumento) AS val
							FROM tbl_contratistas a
							LEFT JOIN tbl_contrato c ON a.IdContratista = c.IdContratista
							LEFT JOIN tbl_documentos d ON d.contrato_id = c.contrato_id AND d.IdEstatus = 3

							WHERE 
								CASE WHEN '.$seg.' = 0 THEN 1=1 ELSE  c.segmento_id = '.$seg.' END
								AND CASE WHEN '.$reg.' = 0 THEN 1=1 ELSE  c.geo_id = '.$reg.' END
								AND CASE WHEN '.$area.' = 0 THEN 1=1 ELSE  c.afuncional_id = '.$area.' END
								AND CASE WHEN '.$mes.' = 0 THEN YEAR(c.cont_fechaFin) >= '.$year.' ELSE c.cont_fechaFin >= CONCAT(CONCAT(CONCAT(\''.$year.'\',\'-\'),\''.$mes.'\'+1),\'-01\') END
								GROUP BY 1
								HAVING val >0';
						
				
  if($tip==0)
    {
      $sqlgr = $sql_doc;
    }
    else if($tip == 1)
    {
      $sqlgr = $sql_sol;

    }
  
	else if($tip == 2){
      $sqlgr = $sql_apr;

    }
	else if($tip == 3){
      $sqlgr = $sql_tmp;

    }
	else if($tip == 4){
      $sqlgr = $sql_tabla;

    }
	else if($tip == 5){
      $sqlgr = $sql_nap;

    }
	


				   $rawgraf = array();
                    $i=0;

                    $row1 = $conn2->query($sqlgr);
                    $results = $row1->fetchAll(PDO::FETCH_ASSOC);       
                       
                    echo json_encode($results);
                    
                  
}
else if($proc == 1)
{ ?> 
 <form method="post" >
   <div style="display: inline-block;width:70px; height:30px;">
      <b>Año</b>
                     <select name="sl_year" id="sl_year" onchange="filtros_s(this.value,1)">

<?php
                   $sqlyear = ' select distinct Anio from dim_tiempo 
                              where anio between year(current_Date) -3
                              and YEAR(CURRENT_DATE)
                              order by anio desc
                            ';
                     $i=0;
                    
                    foreach ($conn2->query($sqlyear) as $row1) {
                           if($row1["Anio"]==$year){
                              echo "<option  value='".$row1["Anio"]."' selected>".$row1["Anio"]."</option>"; 
                           }
                           else
                              echo "<option  value='".$row1["Anio"]."'>".$row1["Anio"]."</option>"; 
                          $i++;
                    }
                    
            ?>

                    </select>

       
       </div>  
       <div style="display: inline-block; width:80px; height:30px;">
            
           <b>Mes</b>
                     <select name="sl_mes" id="sl_mes" onchange="filtros_s(this.value,2)">

<?php


                   $sqlmes = ' SELECT 0 mes, \'Todos\' NMes3L
                   UNION ALL
                   SELECT DISTINCT mes , NMes3L  FROM dim_tiempo 
                   WHERE fecha_mes <= CURRENT_DATE
                   AND  anio = '.$year.' 
                   ORDER BY mes';
                     $i=0;
                    
                    foreach ($conn2->query($sqlmes) as $row1) {
                           if($row1["mes"]==$mes){
                              echo "<option  value='".$row1["mes"]."' selected>".$row1["NMes3L"]."</option>"; 
                           }
                           else
                              echo "<option  value='".$row1["mes"]."'>".$row1["NMes3L"]."</option>"; 
                          $i++;
                    }
                    
            ?>

                   
                    </select>
</div>
  <div style="display: inline-block;width:120px; height:30px;">
                <b>Tipo Faena</b>
                     <select name="sl_reg" id="sl_reg" onchange="filtros_s(this.value,3)">

<?php
                   $sqlreg = ' SELECT 0 id, \'Todos\' reg
                   UNION ALL
                   SELECT geo_id, geo_nombre FROM tbl_contgeografico';
                     $i=0;
                    
                    foreach ($conn2->query($sqlreg) as $row1) {
                           if($row1["id"]==$reg){
                              echo "<option  value='".$row1["id"]."' selected>".$row1["reg"]."</option>"; 
                           }
                           else
                              echo "<option  value='".$row1["id"]."'>".$row1["reg"]."</option>"; 
                          $i++;
                    }
                    
            ?>

                   
                    </select>
</div>
 <div style="display: inline-block;width:280px; height:30px;">
                <b>Área</b>
                     <select name="sl_area" id="sl_area" onchange="filtros_s(this.value,4)">

<?php
                   $sqlarea = ' SELECT 0 id, \'Todos\' area 
                   union all
                   SELECT afuncional_id ,afuncional_nombre area FROM tbl_contareafuncional';
                     $i=0;
                    
                    foreach ($conn2->query($sqlarea) as $row1) {
                           if($row1["id"]==$area){
                              echo "<option  value='".$row1["id"]."' selected>".$row1["area"]."</option>"; 
                           }
                           else
                              echo "<option  value='".$row1["id"]."'>".$row1["area"]."</option>"; 
                          $i++;
                    }
                    
            ?>

                   
                    </select>
        </div>
        <div style="display: inline-block;width:280px; height:30px;">

                    <b>Segmento</b>
                     <select name="sl_seg" id="sl_seg" onchange="filtros_s(this.value,5)">

<?php
                   $sqlseg = ' SELECT 0 id, \'Todos\' nombre
                   union all
                   SELECT segmento_id, seg_nombre FROM tbl_contsegmento';
                     $i=0;
                    
                    foreach ($conn2->query($sqlseg) as $row1) {
                           if($row1["id"]==$seg){
                              echo "<option  value='".$row1["id"]."' selected>".$row1["nombre"]."</option>"; 
                           }
                           else
                              echo "<option  value='".$row1["id"]."'>".$row1["nombre"]."</option>"; 
                          $i++;
                    }
                    
            ?>

                   
                    </select>
          </div>
             <div style="display: inline-block;width:100px; height:30px;">
                <button type="reset" class="btn-xs btn-default " onclick="filtros_s('0',6)">Limpiar</button>
          </div>
</form>

<?php
}

?>

