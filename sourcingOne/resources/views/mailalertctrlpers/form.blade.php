@extends('layouts.app')

@section('content')

  <div class="page-content row">

 	<div class="page-content-wrapper m-t">


<div class="sbox">
	<div class="sbox-title"> <h3> {{ $pageTitle }} <small>{{ $pageNote }}</small></h3> </div>
	<div class="sbox-content"> 	

		<ul class="parsley-error-list">
			@foreach($errors->all() as $error)
				<li>{{ $error }}</li>
			@endforeach
		</ul>	

		 {!! Form::open(array('url'=>'mailalertctrlpers/save?return='.$return, 'class'=>'form-horizontal','files' => true , 'parsley-validate'=>'','novalidate'=>' ')) !!}
<div class="col-md-12">
						<fieldset><legend> Alertas - Lista Mail Personas</legend>
				{!! Form::hidden('id_pers_alert', $row['id_pers_alert']) !!}
									  <div class="form-group  " >
										<label for="Id Persona" class=" control-label col-md-4 text-left"> Id Persona </label>
										<div class="col-md-6">
										  <select name='id_persona' rows='5' id='id_persona' class='select2 '   ></select>
										 </div>
										 <div class="col-md-2">
										 	
										 </div>
									  </div> 
									  <div class="form-group  " >
										<label for="Persona Mail" class=" control-label col-md-4 text-left"> Persona Mail </label>
										<div class="col-md-6">
										  {!! Form::text('persona_mail', $row['persona_mail'],array('class'=>'form-control', 'placeholder'=>'',   )) !!}
										 </div>
										 <div class="col-md-2">
										 	
										 </div>
									  </div> 
									  <div class="form-group  " >
										<label for="Alerta Mail" class=" control-label col-md-4 text-left"> Alerta Mail </label>
										<div class="col-md-6">
										  
					<label class='radio radio-inline'>
					<input type='radio' name='alerta_mail' value ='1'  @if($row['alerta_mail'] == '1') checked="checked" @endif > Si </label>
					<label class='radio radio-inline'>
					<input type='radio' name='alerta_mail' value ='0'  @if($row['alerta_mail'] == '0') checked="checked" @endif > No </label>
										 </div>
										 <div class="col-md-2">
										 	
										 </div>
									  </div> </fieldset>
			</div>

			

		
			<div style="clear:both"></div>	
				
					
				  <div class="form-group">
					<label class="col-sm-4 text-right">&nbsp;</label>
					<div class="col-sm-8">	
					<button type="submit" name="apply" class="btn btn-info btn-sm" ><i class="icon-checkmark-circle2"></i> {{ Lang::get('core.sb_apply') }}</button>
					<button type="submit" name="submit" class="btn btn-primary btn-sm" ><i class="icon-bubble-check"></i> {{ Lang::get('core.sb_save') }}</button>
					<button type="button" onclick="location.href='{{ URL::to('mailalertctrlpers?return='.$return) }}' " class="btn btn-warning btn-sm "><i class="icon-cancel-circle2 "></i>  {{ Lang::get('core.sb_cancel') }} </button>
					</div>	  
			
				  </div>
		 
		 {!! Form::close() !!}
	</div>
</div>		 
</div>	
</div>			 
   <script type="text/javascript">
	$(document).ready(function() { 
		
		
		$("#id_persona").jCombo("{!! url('mailalertctrlpers/comboselect?filter=tbl_personas:IdPersona:RUT|Nombres|Apellidos') !!}",
		{  selected_value : '{{ $row["id_persona"] }}' });
		 
		
		$('.removeMultiFiles').on('click',function(){
			var removeUrl = '{{ url("mailalertctrlpers/removefiles?file=")}}'+$(this).attr('url');
			$(this).parent().remove();
			$.get(removeUrl,function(response){});
			$(this).parent('div').empty();	
			return false;
		});		
		
	});
	</script>		 
@stop