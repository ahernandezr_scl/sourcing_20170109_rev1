

		 {!! Form::open(array('url'=>'mailalertctrlpers/savepublic', 'class'=>'form-horizontal','files' => true , 'parsley-validate'=>'','novalidate'=>' ')) !!}

	@if(Session::has('messagetext'))
	  
		   {!! Session::get('messagetext') !!}
	   
	@endif
	<ul class="parsley-error-list">
		@foreach($errors->all() as $error)
			<li>{{ $error }}</li>
		@endforeach
	</ul>		


<div class="col-md-12">
						<fieldset><legend> Alertas - Lista Mail Personas</legend>
				{!! Form::hidden('id_pers_alert', $row['id_pers_alert']) !!}
									  <div class="form-group  " >
										<label for="Id Persona" class=" control-label col-md-4 text-left"> Id Persona </label>
										<div class="col-md-6">
										  <select name='id_persona' rows='5' id='id_persona' class='select2 '   ></select>
										 </div>
										 <div class="col-md-2">
										 	
										 </div>
									  </div> 
									  <div class="form-group  " >
										<label for="Persona Mail" class=" control-label col-md-4 text-left"> Persona Mail </label>
										<div class="col-md-6">
										  {!! Form::text('persona_mail', $row['persona_mail'],array('class'=>'form-control', 'placeholder'=>'',   )) !!}
										 </div>
										 <div class="col-md-2">
										 	
										 </div>
									  </div> 
									  <div class="form-group  " >
										<label for="Alerta Mail" class=" control-label col-md-4 text-left"> Alerta Mail </label>
										<div class="col-md-6">
										  
					<label class='radio radio-inline'>
					<input type='radio' name='alerta_mail' value ='1'  @if($row['alerta_mail'] == '1') checked="checked" @endif > Si </label>
					<label class='radio radio-inline'>
					<input type='radio' name='alerta_mail' value ='0'  @if($row['alerta_mail'] == '0') checked="checked" @endif > No </label>
										 </div>
										 <div class="col-md-2">
										 	
										 </div>
									  </div> </fieldset>
			</div>

			

			<div style="clear:both"></div>	
				
					
				  <div class="form-group">
					<label class="col-sm-4 text-right">&nbsp;</label>
					<div class="col-sm-8">	
					<button type="submit" name="apply" class="btn btn-info btn-sm" ><i class="fa  fa-check-circle"></i> {{ Lang::get('core.sb_apply') }}</button>
					<button type="submit" name="submit" class="btn btn-primary btn-sm" ><i class="fa  fa-save "></i> {{ Lang::get('core.sb_save') }}</button>
				  </div>	  
			
		</div> 
		 
		 {!! Form::close() !!}
		 
   <script type="text/javascript">
	$(document).ready(function() { 
		
		
		$("#id_persona").jCombo("{!! url('mailalertctrlpers/comboselect?filter=tbl_personas:IdPersona:RUT|Nombres|Apellidos') !!}",
		{  selected_value : '{{ $row["id_persona"] }}' });
		 

		$('.removeCurrentFiles').on('click',function(){
			var removeUrl = $(this).attr('href');
			$.get(removeUrl,function(response){});
			$(this).parent('div').empty();	
			return false;
		});		
		
	});
	</script>		 
