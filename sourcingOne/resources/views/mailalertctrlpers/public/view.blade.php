<div class="m-t" style="padding-top:25px;">	
    <div class="row m-b-lg animated fadeInDown delayp1 text-center">
        <h3> {{ $pageTitle }} <small> {{ $pageNote }} </small></h3>
        <hr />       
    </div>
</div>
<div class="m-t">
	<div class="table-responsive" > 	

		<table class="table table-striped table-bordered" >
			<tbody>	
		
			
					<tr>
						<td width='30%' class='label-view text-right'>{{ SiteHelpers::activeLang('Id Persona', (isset($fields['id_persona']['language'])? $fields['id_persona']['language'] : array())) }}</td>
						<td>{{ SiteHelpers::formatLookUp($row->id_persona,'id_persona','1:tbl_personas:IdPersona:RUT|Nombres|Apellidos') }} </td>

					</tr>
				
					<tr>
						<td width='30%' class='label-view text-right'>{{ SiteHelpers::activeLang('Persona Mail', (isset($fields['persona_mail']['language'])? $fields['persona_mail']['language'] : array())) }}</td>
						<td>{{ $row->persona_mail}} </td>

					</tr>
				
					<tr>
						<td width='30%' class='label-view text-right'>{{ SiteHelpers::activeLang('Alerta Mail', (isset($fields['alerta_mail']['language'])? $fields['alerta_mail']['language'] : array())) }}</td>
						<td>{{ $row->alerta_mail}} </td>

					</tr>
				
					<tr>
						<td width='30%' class='label-view text-right'>{{ SiteHelpers::activeLang('Alerta Dashboard', (isset($fields['alerta_dashboard']['language'])? $fields['alerta_dashboard']['language'] : array())) }}</td>
						<td>{{ $row->alerta_dashboard}} </td>

					</tr>
				
					<tr>
						<td width='30%' class='label-view text-right'>{{ SiteHelpers::activeLang('CreatedOn', (isset($fields['createdOn']['language'])? $fields['createdOn']['language'] : array())) }}</td>
						<td>{{ $row->createdOn}} </td>

					</tr>
				
					<tr>
						<td width='30%' class='label-view text-right'>{{ SiteHelpers::activeLang('Entry By', (isset($fields['entry_by']['language'])? $fields['entry_by']['language'] : array())) }}</td>
						<td>{{ $row->entry_by}} </td>

					</tr>
				
					<tr>
						<td width='30%' class='label-view text-right'>{{ SiteHelpers::activeLang('UpdatedOn', (isset($fields['updatedOn']['language'])? $fields['updatedOn']['language'] : array())) }}</td>
						<td>{{ $row->updatedOn}} </td>

					</tr>
				
					<tr>
						<td width='30%' class='label-view text-right'>{{ SiteHelpers::activeLang('Alerta Mail', (isset($fields['alerta_m']['language'])? $fields['alerta_m']['language'] : array())) }}</td>
						<td>{{ $row->alerta_m}} </td>

					</tr>
				
					<tr>
						<td width='30%' class='label-view text-right'>{{ SiteHelpers::activeLang('Alerta Dashboard', (isset($fields['alerta_d']['language'])? $fields['alerta_d']['language'] : array())) }}</td>
						<td>{{ $row->alerta_d}} </td>

					</tr>
						
					<tr>
						<td width='30%' class='label-view text-right'></td>
						<td> <a href="javascript:history.go(-1)" class="btn btn-primary"> Back To Grid <a> </td>
						
					</tr>					
				
			</tbody>	
		</table>   

	 
	
	</div>
</div>	