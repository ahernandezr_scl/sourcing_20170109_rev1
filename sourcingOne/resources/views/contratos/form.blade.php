
<?php $lintLevelUser = \MySourcing::LevelUser(Session::get('uid')); ?>
<?php
	if ($lintLevelUser=="6") {
		$lintIdUser = Session::get('uid');
	}else{
		$lintIdUser = '';
	}
?>
<!-- Modal -->
<div id="desvincularModal" class="modal fade" role="dialog">
  <div class="modal-dialog">
    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Desvincular persona</h4>
      </div>
      <div class="modal-body">
        <div class="form-group  ">
			<label for="razon" class=" control-label col-md-4 text-left"> Motivo </label>
			<div class="col-md-6">
				<select name="razon" id="razon" class="form-control" >
			    	<option value="">Seleccione un motivo</option>
			    		<option value="1">Cambio de Contrato</option>
					<option value="2">Finiquito</option>
			    </select>
			</div>
			<div class="col-md-2">

			</div>
		</div>
		<br><br>
		 <div class="form-group" id="anotac">
			<label for="anotacion" class=" control-label col-md-4 text-left"> Anotación para el trabajador </label>
			<div class="col-md-6">
				 <select name="anotacion" id="anotacion" class="form-control" >
			    	<option value="">Seleccione una Anotacion</option>
			    	<?php foreach($anotaciones as $rows)
					echo "<option value='$rows->IdConceptoAnotacion'> $rows->Descripcion </option>"
			    	 ?>

			    </select>
			</div>
			<div class="col-md-2">

			</div>
		</div>
      </div>
      <div class="modal-footer">
          <input type="hidden" name="" id="idEsta">
         <button type="button" id="GuardarI" class="btn btn-default">Guardar</button>

        <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
      </div>
    </div>

  </div>
</div>

<div class="sbox">

	<div class="sbox-title">
		<h4> <i class="fa fa-table"></i> <?php echo $pageTitle ;?> <small>{{ $pageNote }}</small>
			<a href="javascript:void(0)" class="collapse-close pull-right btn btn-xs btn-danger" onclick="ajaxViewClose('#{{ $pageModule }}')"><i class="fa fa fa-times"></i></a>
		</h4>
	</div>

	<div class="sbox-content">
		{!! Form::open(array('url'=>'contratos/save/'.SiteHelpers::encryptID($row['contrato_id']), 'class'=>'form-horizontal','files' => true , 'parsley-validate'=>'','novalidate'=>' ','id'=> 'contratosFormAjax')) !!}

	<div class="sbox-content">
			<ul class="nav nav-tabs">
			<li class="active"><a href="#Contratos" data-toggle="tab">Contratos</a></li>
			<li class=""><a href="#Centros" data-toggle="tab">Centros Operacionales</a></li>
			@if ($lintIdUser == $row['entry_by_access'] || $lintLevelUser !=6)
			<li class=""><a href="#Requisitos" data-toggle="tab">Req. Cont / Ctta</a></li>
			@endif
			<li class=""><a href="#Personas" data-toggle="tab">Personas</a></li>
			<li id="LoadDataCentros" class=""><a href="#PersonasCentroGrid" data-toggle="tab">Persona->Centro</a></li>
		</ul>
		<div class="tab-content">

			<div class="tab-pane m-t active" id="Contratos">
				  {!! Form::hidden('contrato_id', $row['contrato_id'],array('class'=>'form-control','id'=>'contrato_id')) !!}
				{!! Form::hidden('createdOn', $row['createdOn']) !!}
				{!! Form::hidden('updatedOn', $row['updatedOn']) !!}
				{!! Form::hidden('entry_by', $row['entry_by']) !!}
				{!! Form::hidden('entry_by_access', $row['entry_by_access']) !!}
				{!! Form::hidden('categoria_id', '') !!}
				@if ($lintLevelUser=="6")
				  {!! Form::hidden('contrato_id', $row['contrato_id'],array('class'=>'form-control','id'=>'contrato_id')) !!}
				  {!! Form::hidden('createdOn', $row['createdOn']) !!}
				  {!! Form::hidden('entry_by', $row['entry_by']) !!}
				  {!! Form::hidden('categoria_id', '') !!}
				  {!! Form::hidden('updatedOn', $row['updatedOn']) !!}
				  {!! Form::hidden('IdContratista', $row['IdContratista']) !!}
				  {!! Form::hidden('cont_numero', $row['cont_numero']) !!}
				  {!! Form::hidden('cont_estado', $row['cont_estado']) !!}
				  {!! Form::hidden('admin_id', $row['admin_id']) !!}
				  {!! Form::hidden('segmento_id', $row['segmento_id']) !!}
				  {!! Form::hidden('afuncional_id', $row['afuncional_id']) !!}
				  {!! Form::hidden('claseCosto_id', $row['claseCosto_id']) !!}
				  {!! Form::hidden('id_tipogasto', $row['id_tipogasto']) !!}
				  {!! Form::hidden('id_extension', $row['id_extension']) !!}
				  {!! Form::hidden('impacto', $row['impacto']) !!}
				  {!! Form::hidden('complejidad', $row['complejidad']) !!}
				  {!! Form::hidden('geo_id', $row['geo_id']) !!}
				  {!! Form::hidden('cont_fechaInicio', $row['cont_fechaInicio']) !!}
				  {!! Form::hidden('cont_fechaFin', $row['cont_fechaFin']) !!}
				  {!! Form::hidden('cont_montoTotal', $row['cont_montoTotal']) !!}
				  {!! Form::hidden('cont_montoTotal', $row['cont_garantia']) !!}
				  
				  {!! Form::hidden('id_riesgo', $row['id_riesgo']) !!}
				<div class="form-group  " >
					<label for="Numero" class=" control-label col-md-4 text-left"> Número SAP </label>
					<div class="col-md-6">
						<label>{!! $row['cont_numero'] !!}</label>
					</div>
					<div class="col-md-2">

					</div>
				</div>
				<div class="form-group  " >
					<label for="Fecha Inicio" class=" control-label col-md-4 text-left"> Fecha Inicio </label>
					<div class="col-md-6">
					<?php
					$ContFI = strtotime($row['cont_fechaInicio']);
					$formatFI = date('d-m-Y',$ContFI); ?>
					<label>{!! $formatFI !!}</label>
					</div>
					<div class="col-md-2">

					</div>
				</div>
				<div class="form-group  " >
					<label for="Fecha Fin" class=" control-label col-md-4 text-left"> Fecha Fin </label>
					<div class="col-md-6">
						<?php
					$ContFF = strtotime($row['cont_fechaFin']);
					$formatFF = date('d-m-Y',$ContFF); ?>
						<label>{!! $formatFF !!}</label>
					</div>
					<div class="col-md-2">

					</div>
				</div>
				@if ($lintIdUser == $row['entry_by_access'])
				<div class="form-group  " >
					<label for="Monto Total" class=" control-label col-md-4 text-left"> Monto Total en $ </label>
					<div class="col-md-6">

						<label>{{ SiteHelpers::currNumber($row['cont_montoTotal']) }}</label>
					</div>
					<div class="col-md-2">

					</div>
				</div>
				
				<div class="form-group  " >
					<label for="Garantia" class=" control-label col-md-4 text-left"> Garantía </label>
					<div class="col-md-6">

						<label>{{ SiteHelpers::currNumber($row['cont_garantia']) }}</label>
					</div>
					<div class="col-md-2">

					</div>
				</div>
				@endif
				<div class="form-group  " >
					<label for="Administrador" class=" control-label col-md-4 text-left"> Administrador Contrato Mandante <span class="asterix"> * </span></label>
					<div class="col-md-6">
						<label for="Admin_id"></label>
					</div>
					<div class="col-md-2">

					</div>
				</div>
				<div class="form-group  " >
					<label for="Segmento" class=" control-label col-md-4 text-left"> Segmento <span class="asterix"> * </span></label>
					<div class="col-md-6">
						<label for="Segmento_id"></label>
					</div>
					<div class="col-md-2">

					</div>
				</div>
				<div class="form-group  " >
					<label for="Geográfico" class=" control-label col-md-4 text-left"> Geográfico </label>
					<div class="col-md-6">
						<label for="Geo_id"></label>
					</div>
					<div class="col-md-2">

					</div>
				</div>
				<div class="form-group  " >
					<label for="Área Funcional" class=" control-label col-md-4 text-left"> Área Funcional <span class="asterix"> * </span></label>
					<div class="col-md-6">
						<label for="Afuncional_id"></label>
					</div>
					<div class="col-md-2">

					</div>
				</div>
				<div class="form-group  " >
					<label for="Clase Costo" class=" control-label col-md-4 text-left"> Clase Costo <span class="asterix"> * </span></label>
					<div class="col-md-6">
						<label for="ClaseCosto_id"></label>
					</div>
					<div class="col-md-2">

					</div>
				</div>

				<div class="form-group  " >
					<label for="Tipo Gasto" class=" control-label col-md-4 text-left"> Tipo Gasto <span class="asterix"> * </span></label>
					<div class="col-md-6">
						<label for="TipoGasto_id"></label>
					</div>
					<div class="col-md-2">

					</div>
				</div>

				<div class="form-group  " >
					<label for="Extension" class=" control-label col-md-4 text-left"> Extensión <span class="asterix"> * </span></label>
					<div class="col-md-6">
						<label for="Extension_id"></label>
					</div>
					<div class="col-md-2">

					</div>
				</div>
				
				<div class="form-group  " >
					<label for="Impacto" class=" control-label col-md-4 text-left"> Impacto <span class="asterix"> * </span></label>
					<div class="col-md-6">
						<label for="Impacto"></label>
					</div>
					<div class="col-md-2">

					</div>
				</div>
				
				<div class="form-group  " >
					<label for="Complejidad" class=" control-label col-md-4 text-left"> Complejidad <span class="asterix"> * </span></label>
					<div class="col-md-6">
						<label for="Complejidad"></label>
					</div>
					<div class="col-md-2">

					</div>
				</div>
				@else
				<div class="form-group  " >
					<label for="Contratista" class=" control-label col-md-4 text-left"> Contratista <span class="asterix"> * </span></label>
					<div class="col-md-6">
						<select name='IdContratista' rows='5' id='IdContratista' class='select2 ' required  ></select>
					</div>
					<div class="col-md-2">

					</div>
				</div>
				<div class="form-group  " >
					<label for="Persona" class=" control-label col-md-4 text-left"> SubContratistas:</label>
					<div class="col-md-6">
						<select name='IdSubContratista[]' multiple rows='5' id='IdSubContratista' class='select2 '   ></select>
					</div>
					<div class="col-md-2">

					</div>
				</div>
				<div class="form-group  " >
					<label for="Numero" class=" control-label col-md-4 text-left"> Número SAP </label>
					<div class="col-md-6">
						{!! Form::text('cont_numero', $row['cont_numero'],array('class'=>'form-control', 'placeholder'=>'')) !!}
					</div>
					<div class="col-md-2">

					</div>
				</div>
				<div class="form-group  " >
					<label for="Fecha Inicio" class=" control-label col-md-4 text-left"> Fecha Inicio </label>
					<div class="col-md-6">
						<div class="input-group m-b" style="width:150px !important;">
							{!! Form::text('cont_fechaInicio', $row['cont_fechaInicio'],array('class'=>'form-control date' )) !!}
							<span class="input-group-addon"><i class="fa fa-calendar"></i></span>
						</div>
					</div>
					<div class="col-md-2">

					</div>
				</div>
				<div class="form-group  " >
					<label for="Fecha Fin" class=" control-label col-md-4 text-left"> Fecha Fin </label>
					<div class="col-md-6">
						<div class="input-group m-b" style="width:150px !important;">
							{!! Form::text('cont_fechaFin', $row['cont_fechaFin'],array('class'=>'form-control date')) !!}
							<span class="input-group-addon"><i class="fa fa-calendar"></i></span>
						</div>
					</div>
					<div class="col-md-2">

					</div>
				</div>
				<div class="form-group  " >
					<label for="Monto Total" class=" control-label col-md-4 text-left"> Monto Total en $ </label>
					<div class="col-md-6">
						{!! Form::text('cont_montoTotal', $row['cont_montoTotal'],array('class'=>'form-control', 'placeholder'=>''   )) !!}
					</div>
					<div class="col-md-2">

					</div>
				</div>
				
				<div class="form-group  " >
					<label for="Garantia" class=" control-label col-md-4 text-left"> Garantía </label>
					<div class="col-md-6">
						{!! Form::text('cont_garantia', $row['cont_garantia'],array('class'=>'form-control', 'placeholder'=>''   )) !!}
					</div>
					<div class="col-md-2">

					</div>
				</div>
				<div class="form-group  " >
					<label for="Administrador" class=" control-label col-md-4 text-left"> Administrador Contrato Mandante <span class="asterix"> * </span></label>
					<div class="col-md-6">
						<select name='admin_id' rows='5' id='admin_id' class='select2 ' required  ></select>
					</div>
					<div class="col-md-2">

					</div>
				</div>
				<div class="form-group  " >
					<label for="Segmento" class=" control-label col-md-4 text-left"> Segmento <span class="asterix"> * </span></label>
					<div class="col-md-6">
						<select name='segmento_id' rows='5' id='segmento_id' class='select2 ' required  ></select>
					</div>
					<div class="col-md-2">

					</div>
				</div>
				<div class="form-group  " >
					<label for="Geográfico" class=" control-label col-md-4 text-left"> Geográfico <span class="asterix"> * </span></label>
					<div class="col-md-6">
						<select name='geo_id' rows='5' id='geo_id' class='select2 ' required  ></select>
					</div>
					<div class="col-md-2">

					</div>
				</div>
				<div class="form-group  " >
					<label for="Área Funcional" class=" control-label col-md-4 text-left"> Área Funcional <span class="asterix"> * </span></label>
					<div class="col-md-6">
						<select name='afuncional_id' rows='5' id='afuncional_id' class='select2 ' required  ></select>
					</div>
					<div class="col-md-2">

					</div>
				</div>
				<div class="form-group  " >
					<label for="Clase Costo" class=" control-label col-md-4 text-left"> Clase Costo <span class="asterix"> * </span></label>
					<div class="col-md-6">
						<select name='claseCosto_id' rows='5' id='claseCosto_id' class='select2 ' required  ></select>
					</div>
					<div class="col-md-2">

					</div>
				</div>

				<div class="form-group  " >
					<label for="Tipo Gasto" class=" control-label col-md-4 text-left"> Tipo Gasto <span class="asterix"> * </span></label>
					<div class="col-md-6">
						<select name='id_tipogasto' rows='5' id='id_tipogasto' class='select2 ' required  ></select>
					</div>
					<div class="col-md-2">

					</div>
				</div>

				<div class="form-group  " >
					<label for="Extension" class=" control-label col-md-4 text-left"> Extensión <span class="asterix"> * </span></label>
					<div class="col-md-6">
						<select name='id_extension' rows='5' id='id_extension' class='select2 ' required  ></select>
					</div>
					<div class="col-md-2">

					</div>
				</div>
				
				<div class="form-group  " >
					<label for="Impacto" class=" control-label col-md-4 text-left"> Impacto </label>
					<div class="col-md-6">
						{!! Form::text('impacto', $row['impacto'],array('class'=>'form-control', 'placeholder'=>''   )) !!}
					</div>
					<div class="col-md-2">

					</div>
				</div>
				
				<div class="form-group  " >
					<label for="Complejidad" class=" control-label col-md-4 text-left"> Complejidad </label>
					<div class="col-md-6">
						{!! Form::text('complejidad', $row['complejidad'],array('class'=>'form-control', 'placeholder'=>''   )) !!}
					</div>
					<div class="col-md-2">

					</div>
				</div>
				<?php
				if (strlen($row['cont_estado'])>0):
					?>
				<div class="form-group  " >
					<label for="Estatus" class=" control-label col-md-4 text-left"> Estatus </label>
					<div class="col-md-6">

						<?php $cont_estado = explode(',',$row['cont_estado']);
						$con_estado_opt = array( '' => 'Seleccione Valor' ,  '1' => 'Activo' ,  '2' => 'Inactivo' , ); ?>
						<select name='cont_estado' rows='5'   class='select2 '  >
							<?php
							foreach($con_estado_opt as $key=>$val)
							{
								echo "<option  value ='$key' ".($row['cont_estado'] == $key ? " selected='selected' " : '' ).">$val</option>";
							}
							?></select>
						</div>
						<div class="col-md-2">

						</div>
					</div>
				<?php elseif (strlen($row['cont_estado'])==0): ?>
					<input type="hidden" name="cont_estado" value="1">
				<?php endif; ?>
			    @endif
			</div>

			<div class="tab-pane m-t " id="Centros">
				<!-- Inicio de personas -->
				<hr />
				<div class="clr clear"></div>

				<h5> Centros </h5>
@if ($lintLevelUser=="6")
					<div id="ContratoC" class="table-responsive">
					<table  class="table table-striped ">
						<thead>
							<tr>
								<th>Descripcion</th>
								<th></th>
							</tr>
						</thead>
						<tbody>
							<tr class="clonedInput">
								<td>
									<input type="hidden" name="bulktwo_IdCentro[]" id="bulktwo_IdCentro" value="">
								</td>
							</tr>
							@foreach ($rowContrCentros as $rows)
							<tr class="cloneTwo clonedInputTwo">
								<td>
								<?php echo $rows->Descripcion ?>
									<input type="hidden" name="bulktwo_IdCentro[]" id="bulktwo_IdCentro" class="bulktwo_IdCentro" value="<?=$rows->IdCentro?>">
								</td>
							</tr>
							@endforeach
						</tbody>
					</table>
					<input type="hidden" name="enable-masterdetail" value="true">
					<input type="hidden" name="Guardar" value="1">
				</div>
			@else
				<div class="table-responsive">
					<table class="table table-striped ">
						<thead>
							<tr>
								@foreach ($subformtwo['tableGrid'] as $t)
								@if($t['view'] =='1' && $t['field'] !='contrato_id')
								<th>{{ $t['label'] }}</th>
								@endif
								@endforeach
								<th></th>
							</tr>

						</thead>

						<tbody id="centrostabla">
							@if(count($subformtwo['rowData'])>=1)
							@foreach ($subformtwo['rowData'] as $rows)
							<tr class="cloneTwo clonedInputTwo">

								@foreach ($subformtwo['tableGrid'] as $field)
								@if($field['view'] =='1' && $field['field'] !='contrato_id')
								<td>
									{!! SiteHelpers::bulkForm($field['field'] , $subformtwo['tableForm'] , $rows->{$field['field']},'two') !!}
								</td>
								@endif

								@endforeach
								<td>
									<a onclick=" $(this).parents('.clonedInputTwo').remove(); return false" href="#" class="remove btn btn-xs btn-danger">-</a>
									<input type="hidden" name="countertwo[]">
								</td>
								@endforeach
							</tr>

							@else
							<tr class="cloneTwo clonedInputTwo">

								@foreach ($subformtwo['tableGrid'] as $field)

								@if($field['view'] =='1' && $field['field'] !='contrato_id')
								<td>
									{!! SiteHelpers::bulkForm($field['field'] , $subformtwo['tableForm'],'','two' ) !!}
								</td>
								@endif

								@endforeach
								<td>
									<a onclick=" $(this).parents('.clonedInputTwo').remove(); return false" href="#" class="remove btn btn-xs btn-danger">-</a>
									<input type="hidden" name="countertwo[]">
								</td>

							</tr>
							@endif
						</tbody>

					</table>
					<input type="hidden" name="enable-masterdetail" value="true">
				</div>
				<br /><br />
				<a href="javascript:void(0);" class="addC btn btn-xs btn-info" rel=".cloneTwo"><i class="fa fa-plus"></i> New Item</a>
				<input type="hidden" name="Guardar" value="0">

				<hr />

	   @endif
				<!-- Final de personas -->
			</div>

			<div class="tab-pane m-t " id="Personas">

				<!-- Inicio de personas -->
				<hr />
				<div class="clr clear"></div>

				<h5> Personas </h5>

				<div class="table-responsive">
					<table class="table table-striped ">
						<thead>
							<tr>
								@if ($lintIdUser==$row['entry_by_access'] && $countcountratistas)
								<th>
								  Asociado
								</th>
								@endif
								@foreach ($subform['tableGrid'] as $t)
								@if($t['view'] =='1' && $t['field'] !='contrato_id')
								<th>{{ $t['label'] }}</th>
								@endif
								@endforeach
								<th></th>
							</tr>

						</thead>
						<tbody>
						<?php $a=1; ?>
							@if(count($subform['rowData'])>=1)

							<tr class="clone clonedInput">
								@if ($lintIdUser==$row['entry_by_access'] && $countcountratistas)
								<td>
								  -
								</td>
								@endif
								@foreach ($subform['tableGrid'] as $field)
								
								@if($field['view'] =='1' && $field['field'] !='contrato_id')
								<td>
								  @if ($field['field']=="IdPersona")
								    <select id="bulk_IdPersona" name="bulk_IdPersona[]" class="form-control bulk_IdPersona select3">
									</select>
								  @else
									{!! SiteHelpers::bulkForm($field['field'] , $subform['tableForm'], '', '', $lintIdUser ) !!}
								  @endif
								</td>
								@endif

								@endforeach
								<td>
									<input type="hidden" id="bulk_entry_by_access" name="bulk_entry_by_access[]" value="<?php echo $lintIdUser; ?>" />
								</td>
								<td>
									<a onclick="removepeople($(this),0,0);" href="#" class="remove btn btn-xs btn-danger">-</a>
									<input type="hidden" name="counter[]">
								</td>

						<?php
						if ($lintLevelUser=="6") {
							$lintIdUser = Session::get('uid');
						}else{
							$lintIdUser = '';
						}
						?>

						<tbody>
							@if(count($subform['rowData'])>=1)
							@foreach ($subform['rowData'] as $rows)
							<tr class="clone clonedInput">
								<?php $a++; ?>
								@if ($lintIdUser==$row['entry_by_access'] && $countcountratistas)
								<td>
								  <?php echo $rows->{'first_name'}.' '.$rows->{'last_name'}; ?>
								</td>
								@endif
								@foreach ($subform['tableGrid'] as $field)
								@if($field['view'] =='1' && $field['field'] !='contrato_id')
								<td>
									@if ($field['field'] != 'IdPersona' )
									  {!! SiteHelpers::bulkForm($field['field'] , $subform['tableForm'], $rows->{$field['field']}) !!}
									@else
									  <?php echo $rows->{'RUT'}." ".$rows->{'Nombres'}." ".$rows->{'Apellidos'}; ?>
									  <input type="hidden" id="bulk_IdPersona" name="bulk_IdPersona[]" class="bulk_IdPersona" value="<?php echo $rows->{'IdPersona'}; ?>">
									@endif
								</td>
								@endif

								@endforeach
									<input type="hidden" id="bulk_entry_by_access" name="bulk_entry_by_access[]" value="<?php echo $rows->{'entry_by_access'}; ?>" />
								<td>
								</td>
								<td>
									<a id="removeP" onclick="removepeople($(this),1,<?php echo $a?>);" href="#" class="remove btn btn-xs btn-danger">-</a>
									<input type="hidden" name="counter[]">
									<input type="hidden" value="<?php echo $a?>" name="valorC" id="valorC" class="valorC" >
								</td>
								@endforeach
							</tr>

							@else


							<tr class="clone clonedInput">

								@if ($lintIdUser==$row['entry_by_access'] && $countcountratistas)
								<td>
								  -
								</td>
								@endif
								@foreach ($subform['tableGrid'] as $field)

								@if($field['view'] =='1' && $field['field'] !='contrato_id')
								<td>
							  	  @if ($field['field']=="IdPersona")
								    <select id="bulk_IdPersona" name="bulk_IdPersona[]" class="form-control bulk_IdPersona select3">
									</select>
								  @else
									{!! SiteHelpers::bulkForm($field['field'] , $subform['tableForm'], '', '', $lintIdUser ) !!}
								  @endif
								</td>
								@endif

								@endforeach
								<td>
									<input type="hidden" id="bulk_entry_by_access" name="bulk_entry_by_access[]" value="<?php echo $lintIdUser; ?>" />
								</td>
								<td>
									<a onclick="removepeople($(this),0);" href="#" class="remove btn btn-xs btn-danger">-</a>
									<input type="hidden" name="counter[]">
								</td>

							</tr>


							@endif


						</tbody>

					</table>
					<input type="hidden" name="enable-masterdetail" value="true">
				</div>
				<br /><br />

				<a href="javascript:void(0);" class="addCPeople btn btn-xs btn-info" rel=".clone"><i class="fa fa-plus"></i> New Item</a>
				<hr />
				<!-- Final de personas -->
			</div>
			@if ($lintIdUser == $row['entry_by_access'] || $lintLevelUser !=6)
			<div class="tab-pane m-t " id="Requisitos">
				<h5> Contratistas </h5>
				<div id="RequisitosContratista" class="table-responsive">
					<table id="TablaDContratistas" class="table table-striped ">
						<thead>
							<tr>
								<th>Descripcion</th>
								<th></th>
							</tr>

						</thead>
						<tbody>
							@foreach ($rowRContratistas as $rows)
							<tr class="clone clonedInput">
								<td>
								<?php echo $rows->Descripcion ?>
								<?php if (strlen($row['contrato_id'])>0) : ?>
		      					<input type="hidden" name="bulk_Documento[]" id="Documento" value="<?=$rows->IdDocumento?>">
		      					<?php endif ?>
								<input type="hidden" name="bulk_IdRequisito[]" id="IdRequisito" value="<?=$rows->IdRequisito?>">
								<input type="hidden" name="bulk_IdTipoDocumento[]" id="IdTipoDocumento" value="<?=$rows->IdTipoDocumento?>">
								<input type="hidden" name="bulk_Entidad[]" id="Entidad" value="<?=$rows->Entidad?>">
								</td>
								<?php if ($lintLevelUser!="6") : ?>
								<td>
									<a onclick=" $(this).parents('.clonedInput').remove(); return false" href="#" class="remove btn btn-xs btn-danger">-</a>
									<input type="hidden" name="counter[]">
								</td>
								<?php endif ?>
							</tr>
							@endforeach
							<tr class="cloneContratistas clonedInput">
								<td>
								  <select name="bulk_IdTipoDocumento[]" class="bulk_IdTipoDocumento bulk_IdTipoDocumentoContratistas">
								  </select>
								  <input type="hidden" name="bulk_Documento[]" id="Documento" >
								  <input type="hidden" name="bulk_IdRequisito[]" id="IdRequisito" >
								  <input type="hidden" name="bulk_Entidad[]" id="Entidad" value="1">
								  </td>
								<td>
									<a onclick=" $(this).parents('.clonedInput').remove(); return false" href="#" class="remove btn btn-xs btn-danger">-</a>
									<input type="hidden" name="counter[]">
								</td>
							</tr>
						</tbody>
					</table>
					<input type="hidden" name="enable-masterdetail" value="true">
				</div>
				<br />
				<?php if ($lintLevelUser!="6") : ?>
		 		<a href="javascript:void(0);" id="BcloneContratistas" class="addCReq btn btn-xs btn-info" rel=".cloneContratistas"><i class="fa fa-plus"></i> Nuevo requisito </a>
		 		<?php endif ?>
		 		<hr />
		 		<h5> Contratos </h5>
				<div id="RequisitosContrato" class="table-responsive">
					<table class="table table-striped ">
						<thead>
							<tr>
								<th>Descripcion</th>
								<th></th>
							</tr>

						</thead>
						<tbody>
							@foreach ($rowRContratos as $rows)
							<tr class="clone clonedInput">
								<td>
								<?php echo $rows->Descripcion ?>
								<?php if (strlen($row['contrato_id'])>0) : ?>
		      					<input type="hidden" name="bulk_Documento[]" id="Documento" value="<?=$rows->IdDocumento?>">
		      					<?php endif ?>
								<input type="hidden" name="bulk_IdRequisito[]" id="IdRequisito" value="<?=$rows->IdRequisito?>">.
								<input type="hidden" name="bulk_IdTipoDocumento[]" id="IdTipoDocumento" value="<?=$rows->IdTipoDocumento?>">
								<input type="hidden" name="bulk_Entidad[]" id="Entidad" value="<?=$rows->Entidad?>">
								</td>
								<?php if ($lintLevelUser!="6") : ?>
								<td>
									<a onclick=" $(this).parents('.clonedInput').remove(); return false" href="#" class="remove btn btn-xs btn-danger">-</a>
									<input type="hidden" name="counter[]">
								</td>
								<?php endif ?>
							</tr>
							@endforeach
							<tr class="cloneContratos clonedInput">
								<td>
								  <select name="bulk_IdTipoDocumento[]" class="bulk_IdTipoDocumento bulk_IdTipoDocumentoContratos">
								  </select>
								  <input type="hidden" name="bulk_Documento[]" id="Documento" value="">
								  <input type="hidden" name="bulk_IdRequisito[]" id="IdRequisito" value="">
								  <input type="hidden" name="bulk_Entidad[]" id="Entidad" value="2">
								</td>
								<td>
									<a onclick=" $(this).parents('.clonedInput').remove(); return false" href="#" class="remove btn btn-xs btn-danger">-</a>
									<input type="hidden" name="counter[]">
								</td>
							</tr>
						</tbody>
					</table>
					<input type="hidden" name="enable-masterdetail" value="true">
				</div>
				<br />
				<?php if ($lintLevelUser!="6") : ?>
		 		<a href="javascript:void(0);" class="addCReq btn btn-xs btn-info" rel=".cloneContratos"><i class="fa fa-plus"></i> Nuevo requisito </a>
		 		<?php endif ?>
		 		<hr />
				<h5> Centros </h5>
				<div id="RequisitosCentros" class="table-responsive">
					<table class="table table-striped ">
						<thead>
							<tr>
								<th>Descripcion</th>
								<th></th>
							</tr>

						</thead>
						<tbody>
							@foreach ($rowRCentros as $rows)
							<tr class="clone clonedInput">
								<td>
								<?php echo $rows->Descripcion ?>
								<?php if (strlen($row['contrato_id'])>0) : ?>
		      					<input type="hidden" name="bulk_Documento[]" id="Documento" value="<?=$rows->IdDocumento?>">
		      					<?php endif ?>
								<input type="hidden" name="bulk_IdRequisito[]" id="IdRequisito" value="<?=$rows->IdRequisito?>">
								<input type="hidden" name="bulk_IdTipoDocumento[]" id="IdTipoDocumento" value="<?=$rows->IdTipoDocumento?>">
								<input type="hidden" name="bulk_Entidad[]" id="Entidad" value="<?=$rows->Entidad?>">
								</td>
								<?php if ($lintLevelUser!="6") : ?>
								<td>
									<a onclick=" $(this).parents('.clonedInput').remove(); return false" href="#" class="remove btn btn-xs btn-danger">-</a>
									<input type="hidden" name="counter[]">
								</td>
								<?php endif ?>
							</tr>
							@endforeach
							<tr class="cloneCentros clonedInput">
								<td>
								  <select  name="bulk_IdTipoDocumento[]" class="bulk_IdTipoDocumento bulk_IdTipoDocumentoCentros">
								  </select>
								  <input type="hidden" name="bulk_Documento[]" id="Documento" value="">
								  <input type="hidden" name="bulk_IdRequisito[]" id="IdRequisito" value="">
								  <input type="hidden" name="bulk_Entidad[]" id="Entidad" value="6">
								</td>
								<td>
									<a onclick=" $(this).parents('.clonedInput').remove(); return false" href="#" class="remove btn btn-xs btn-danger">-</a>
									<input type="hidden" name="counter[]">
								</td>
							</tr>
						</tbody>
					</table>
					<input type="hidden" name="enable-masterdetail" value="true">
				</div>
				<br />
				<?php if ($lintLevelUser!="6") : ?>
		 		<a href="javascript:void(0);" class="addCReq btn btn-xs btn-info" rel=".cloneCentros"><i class="fa fa-plus"></i> Nuevo requisito </a>
		 		<?php endif ?>
		 		<hr />
			</div>
			@endif
			<div class="tab-pane m-t " id="PersonasCentroGrid">

				<div class="table-responsive">
					<table class="table table-striped " id="tablaPersonaCentro">
						<thead>
							<tr>
								<th width="20"> No </th>
						                        <th width="30"> </th>
						                        <th width="30"> Persona </th>
						                        <th width="30"> Rol </th>
						                      <!--  @foreach ($areasT as $rows)
						                          <th width="30" class="areaTH"> <?php echo $rows->Descripcion ?> </th>
								  @endforeach -->
							</tr>

						</thead>
						<tbody id="DataCentros" >
						<?php $i=0; ?>
						  @foreach ($PersonasCont as $rows)
						  <tr id="idpeopleTR<?php echo $rows->IdPersona ;?>">
						  	  <td class="number"> <?php echo ++$i;?>  </td>
						  	  <td class="check"><input type="checkbox" class="ids checkpeopleall" name="IdPersona[]" value="<?php echo $rows->IdPersona ;?>" /></td>
						  	  <td class="nombre"><?php echo $rows->Rut.' '.$rows->Nombres.' '.$rows->Apellidos ?></td>
						  	  <td class="rol"  id-rol="<?php echo $rows->IdRol ?>"><?php echo $rows->Roles ?></td>

						  	    <!-- @foreach ($areasT as $rows)
						                         <td align="center" class="areaTB"><input type="checkbox"  name="IdAreaTrabajoP[]" id="<?=$rows->IdAreaTrabajo ?>"  data-parsley-checkmin="1"/>
								<input type="hidden" name="IdCentroT" id="IdCentroT" value="<?=$rows->IdCentro?>">
						                         </td>
								  @endforeach -->
						  </tr>
						  @endforeach
						</tbody>
					</table>
				</div>
			</div>

			<div style="clear:both"></div>
			<hr>
	</div>
	<div class="form-group">
		<label class="col-sm-4 text-right">&nbsp;</label>
		<div class="col-sm-8">
			<button type="submit" class="btn btn-primary btn-sm "><i class="icon-checkmark-circle2"></i>  {{ Lang::get('core.sb_save') }} </button>
			<button type="button" onclick="ajaxViewClose('#{{ $pageModule }}')" class="btn btn-success btn-sm"><i class="icon-cancel-circle2 "></i>  {{ Lang::get('core.sb_cancel') }} </button>
		</div>
	</div>
{!! Form::close() !!}
		</div>
	</div>


</div>

<script type="text/javascript">

		$('#anotac').hide();

		$('#razon').on('change', function() {
			if (this.value==2){
				$('#anotac').show();
				$("#anotacion").val('');
				$("#anotacion").change(function() {
			         	if ($("#anotacion").val()!='')
			               $('#GuardarI').prop("disabled", false );
				});
			}
 			else if (this.value==1){
 			 	$('#anotac').show();
 			 	$("#anotacion").val('');
 			 	$('#GuardarI').prop("disabled", false );
 			 }
 			else{
 				$('#GuardarI').prop("disabled", true );
 				$('#anotac').hide();
 			}
		});


    	var people = {
    		 @foreach ($PersonasCont as $rows)
			   	  <?php echo $rows->IdPersona.": {idaction:''},"; ?>
		     @endforeach
    	};
    	var peopleaccess = {
    		<?php
    		  $lintIdPersona = 0;
    		  foreach ($rowPersonasAreas as $value) {
    		  	if ($lintIdPersona!=$value->IdPersona){
    		  		if ($lintIdPersona!=0){
    		  		  echo ' ], ';
    		  		}
    		  		$lintIdPersona = $value->IdPersona;
    		  		echo '"'.$value->IdPersona.'":';
    		  		echo ' [ 0, '.$value->IdAreaTrabajo.', ';
    		  	}else{
    		  		echo ' '.$value->IdAreaTrabajo.', ';
    		  	}
    		  }
    		  if ($lintIdPersona!=0){
    		  	echo ' ] ';
    		  }
			?>
    	};

	function loadaccess(){
		var items = Object.keys(peopleaccess);
		items.forEach(function(idpeople) {
			valueidpersona = 'idpeopleTR'+idpeople;
			peopleaccess[idpeople].forEach(function(idarea) {
			   $("#"+valueidpersona+" #"+idarea).prop( "checked", true );
			});
		});
		$('input[type="checkbox"],input[type="radio"]').iCheck('update');
	}
	var valthisP = "";
function removepeople(valthis,opcion,iden){
	if (valthis.parents('.clonedInput').find('.valorC').val()=="")
		opcion=0;
		valthisP = valthis;
		console.log(opcion)
if (opcion==1){
	$('#desvincularModal').modal('show');
	$('#razon').val('');
	$('#GuardarI').prop("disabled", true );
	if ($('#razon').val()=='')
		$('#anotac').hide();

	  var fieldId = "#bulk_IdPersona" + iden;
	  //alert(fieldId + $("#bulk_IdPersona" + iden).val());
		$('#idEsta').val($(fieldId).val());
	}
else{
	lobjPeople = people[valthis.parents('.clonedInput').find('.bulk_IdPersona').val().toString()];
	if (lobjPeople){
		if ( lobjPeople.hasOwnProperty('idaction')){
		  lobjPeople.idaction='D';
		}
	}
	valthis.parents('.clonedInput').remove();
	return false;
}

}

$(document).ready(function() {

	$("#GuardarI").click(function() {
		user = $("#entry_by").val();
		contratoN = $("#contrato_id").val();
  		anotacion = $("#anotacion").val()
  		razon = $("#razon").val()
  		id = $('#idEsta').val()
		datas = {IdPersona:id, contrato:contratoN, usuario:user, anotacion:anotacion, razon:razon};
		console.log(datas)
		$('.ajaxLoading').show();
		$.post( '{{$pageModule}}/borrar' ,datas, function( data ) {
			if(data.status == 'success')
			{
				$('.ajaxLoading').hide();
				notyMessage(data.message);
			                $('#desvincularModal').modal('hide');
			                valthisP.parents('.clonedInput').remove();
				return false;
			} else {
				$('.ajaxLoading').hide();
				notyMessageError(data.message);
				return false;
			}
		},"json");
	});

<?php if(count($subform['rowData'])>=1): ?>
	$('.bulk_IdPersona').each(function(index) {
	{  selected_value : '{{ $row["admin_id"] }}' });
	});

<?php endif; ?>


/* Subcontratistas */
<?php if (strlen($row['contrato_id'])>0) : ?>
var sub =<?php echo json_encode($subContratista );?>;
var contratista =<?php echo $row["IdContratista"] ?>;
		$.post('contratos/datacontratista',{ id:contratista,},function(response,state){
				$("#IdSubContratista").empty();
			for (var i =0; i<response.valores.length; i++) {
				$("#IdSubContratista").append("<option value='"+response.valores[i].IdContratista+"'>"+response.valores[i].RazonSocial+"</option>");
			}
			$("#IdSubContratista > option").each(function(index, el) {
			var $this = $(this);
			$.each(sub, function(key,value) {
				if (el.value==value.IdSubContratista){
				  $this.prop("selected","selected");
				   $this.trigger("change");
				}

				});
				});
		});
<?php else: ?>
$("#IdSubContratista").prop("disabled",true);
	$("#IdContratista").change(function(){
		$.post('contratos/datacontratista',{ id:$(this).val(),},function(response,state){
				$("#IdSubContratista").empty();
				if (response.valores.length>0)
					$("#IdSubContratista").prop("disabled",false);
			for (var i =0; i<response.valores.length; i++) {
				$("#IdSubContratista").append("<option value='"+response.valores[i].IdContratista+"'>"+response.valores[i].RazonSocial+"</option>");
			}
		});
	});
<?php endif; ?>
/* Subcontratistas */


$(".bulktwo_IdCentro").focus(function(){
	var lobjIdCentro = $(this);
	$(".bulktwo_IdCentro").each(function () {
		if ($(this).val()!=lobjIdCentro.val()) {
	  	    	lobjIdCentro.find("option[value='"+$(this).val()+"']").remove(); //elimino el valor
	  	    }
	  	});
});
$(".bulktwo_IdCentro").change(function(){
	var lobjIdCentro = $(this);
	lobjIdCentro.attr("eval-val-combo","true");
	$(".bulktwo_IdCentro").each(function () { //recorro todos los datos que se llamen igual que el
		if ($(this).attr("eval-val-combo")!="true"){
			if ($(this).val()==lobjIdCentro.val()) {
				lobjIdCentro.val("");
				notyMessageError("Este registro ya fue seleccionado.");
			}
		}
	});
	lobjIdCentro.attr("eval-val-combo","false");
});

$(".bulk_IdPersona").focus(function(){
	var lobjIdCentro = $(this);
	$(".bulk_IdPersona").each(function () {
	    if ($(this).val()!=lobjIdCentro.val()) {
  	    	lobjIdCentro.find("option[value='"+$(this).val()+"']").remove(); //elimino el valor
  	    }
  	});
});

$(".bulk_IdPersona").change(function(){
	var lobjIdCentro = $(this);
	lobjIdCentro.attr("eval-val-combo","true");
		$(".bulk_IdPersona").each(function () { //recorro todos los datos que se llamen igual que el
			if ($(this).attr("eval-val-combo")!="true"){
				if ($(this).val()==lobjIdCentro.val()) {
					lobjIdCentro.val("");
					notyMessageError("Este registro ya fue seleccionado.");
				}
			}
		});
		lobjIdCentro.attr("eval-val-combo","false");
	});

$(".bulk_IdCentro").focus(function(){
	$(this).html($("#bulk_IdCentro:first").html());
	var lobjIdCentro = $(this);
	var lintIdCentro;
	var lintExists = 0;
	$(this).find("option").each(function () {
		lintIdCentro = $(this).val();
		lintExists = 0;
		$(".bulktwo_IdCentro").each(function () {
			if ($(this).val()==lintIdCentro) {
				lintExists = 1;
			}
		});
		if (!lintExists){
			lobjIdCentro.find("option[value='"+lintIdCentro+"']").remove();
		}
	});
});

$('#tablaPersonaCentro .checkpeopleall').on('ifChecked',function(){
	$('#idpeopleTR'+$(this).attr("value")+' input[type="checkbox"]').iCheck('check');
});
$('#tablaPersonaCentro .checkpeopleall').on('ifUnchecked',function(){
	$('#idpeopleTR'+$(this).attr("value")+' input[type="checkbox"]').iCheck('uncheck');
});

function updateView(){
    var lobj;
	var items = Object.keys(people);
	var table = $('#tablaPersonaCentro').DataTable();
	items.forEach(function(idpeople) {

		if (people[idpeople].idaction=="I"){

		  //= '<tr id="idpeopleTR'+idpeople+'">';
		  lstrInsert = '<td class="number"></td>';
		  lstrInsert += '<td class="check"><input type="checkbox" class="ids checkpeopleall" name="IdPersona[]" value="'+idpeople+'" /></td>';
		  lstrInsert += '<td class="nombre">'+people[idpeople].name+'</td>';
		  lstrInsert += '<td class="rol" id-rol="'+people[idpeople].idrol+'">'+people[idpeople].rol+'</td>';
		  $(".areaTH").each(function () {
			lstrInsert += '<td align="center" class="areaTB">';
			lstrInsert += '  <input type="checkbox"  name="IdAreaTrabajoP[]" id="'+$(this).attr('data-th-idarea')+'"/>';
			lstrInsert += '  <input type="hidden" name="IdCentroT" id="IdCentroT" value="">';
			lstrInsert += '</td>';
		  });
		  //lstrInsert += '</tr>';
	      lobj = $('<tr id="idpeopleTR'+idpeople+'">').append(lstrInsert);
	      table.row.add(lobj).draw();
	    }else if (people[idpeople].idaction=="D"){
		   $("#idpeopleTR"+idpeople+"").remove();
		   delete people[idpeople];
	    }else{
          $("#idpeopleTR"+idpeople+" .rol").attr("id-rol",people[idpeople].idrol);
	      $("#idpeopleTR"+idpeople+" .rol").html(people[idpeople].rol);
	    }

	});
 	//table.draw();

 	$('#tablaPersonaCentro').DataTable();

	$('input[type="checkbox"],input[type="radio"]').iCheck({checkboxClass: 'icheckbox_square-red',radioClass: 'iradio_square-red'});

	$('#tablaPersonaCentro .checkpeopleall').on('ifChecked',function(){
		$('#idpeopleTR'+$(this).attr("value")+' input[type="checkbox"]').iCheck('check');
	});
	$('#tablaPersonaCentro .checkpeopleall').on('ifUnchecked',function(){
		$('#idpeopleTR'+$(this).attr("value")+' input[type="checkbox"]').iCheck('uncheck');
	});

}
var lintIdCentroCurrent = '0';


$("#LoadDataCentros").click(function(){

	$('.ajaxLoading').show();
	var lintContador = 0;
	$("#Filtrocentros").jCombo("{!! url('contratos/comboselect?filter=tbl_centro:IdCentro:Descripcion') !!} "
		,{  selected_value : lintIdCentroCurrent }
		);

	$(".bulk_IdPersona").each(function () {
		    if ($(this).val()!=0) { //Verificamos si la persona fue seleccionada

				lintContador += 1;
		    	lintPersona = $(this).val();
		    	lstrPersona = $(this).find("option:selected").text();
	  	        lintRol = $(this).parent("td").parent("tr").find(".bulk_IdRol").val();
	  	        lstrRol = $(this).parent("td").parent("tr").find(".bulk_IdRol option:selected").text();

		    	if (!people.hasOwnProperty($(this).val())) {
		    		lintIdAccion = 'I';
		    	}else{
		    		lintIdAccion = 'U';
		    	}

		    	people[lintPersona] = { name: lstrPersona, idrol: lintRol, rol: lstrRol, idaction: lintIdAccion};

	  	    }
	});
	updateView();
	loadaccess();
	$('.ajaxLoading').hide();
});

	$("#admin_id").jCombo("{!! url('contratos/comboselect?filter=vw_usuarios_no_contratistas:id:first_name|last_name') !!}",
	{  selected_value : '{{ $row["admin_id"] }}' });

$("#IdContratista").jCombo("{!! url('contratos/comboselect?filter=tbl_contratistas:IdContratista:RUT|RazonSocial') !!}",
	{  selected_value : '{{ $row["IdContratista"] }}' });

@if ($lintLevelUser==6)
$(".bulk_IdPersona").jCombo("{!! url('contratospersonas/comboselect?filter=vw_personas_sin_contratos:IdPersona:RUT|Nombres|Apellidos') !!}&parent=entry_by_access:<?php echo $lintIdUser; ?>",
	{ selected_value : ''});
@else
$(".bulk_IdPersona").jCombo("{!! url('contratospersonas/comboselect?filter=vw_personas_sin_contratos:IdPersona:RUT|Nombres|Apellidos') !!}",
	{  selected_value : ''});
@endif

$(".bulk_IdTipoDocumentoContratistas").jCombo("{!! url('contratos/comboselect?filter=tbl_tipos_documentos:IdTipoDocumento:Descripcion') !!}&parent=Entidad:1");
$(".bulk_IdTipoDocumentoContratos").jCombo("{!! url('contratos/comboselect?filter=tbl_tipos_documentos:IdTipoDocumento:Descripcion') !!}&parent=Entidad:2");
$(".bulk_IdTipoDocumentoCentros").jCombo("{!! url('contratos/comboselect?filter=tbl_tipos_documentos:IdTipoDocumento:Descripcion') !!}&parent=Entidad:6");

$("#categoria_id").jCombo("{!! url('contratos/comboselect?filter=tbl_contcategorias:categorias_id:cat_nombre') !!}",
	{  selected_value : '{{ $row["categoria_id"] }}' });

$("#segmento_id").jCombo("{!! url('contratos/comboselect?filter=tbl_contsegmento:segmento_id:seg_nombre') !!}",
	{  selected_value : '{{ $row["segmento_id"] }}' });

$("#geo_id").jCombo("{!! url('contratos/comboselect?filter=tbl_contgeografico:geo_id:geo_nombre') !!}",
	{  selected_value : '{{ $row["geo_id"] }}' });

$("#afuncional_id").jCombo("{!! url('contratos/comboselect?filter=tbl_contareafuncional:afuncional_id:afuncional_nombre') !!}",
	{  selected_value : '{{ $row["afuncional_id"] }}' });

$("#claseCosto_id").jCombo("{!! url('contratos/comboselect?filter=tbl_contclasecosto:claseCosto_id:ccost_nombre') !!}",
	{  selected_value : '{{ $row["claseCosto_id"] }}' });

<?php if ($lintLevelUser=="6") : ?>
	{  selected_value : '{{ $row["id_tipogasto"] }}' });

$("#id_extension").jCombo("{!! url('contratos/comboselect?filter=tbl_contrato_extension:id_extension:nombre') !!}",
	{  selected_value : '{{ $row["id_extension"] }}' });



$.post('contratos/datainformacion',{ id:$("#contrato_id").val(),},function(response,state){

				console.log(response.valores)
//				$("label[for='TipoGasto_id']").html(response.valores[0].nombre);
//				$("label[for='Extension_id']").html(response.valores[0].nombre);
				$("label[for='Segmento_id']").html(response.valores[0].seg_nombre);
				$("label[for='Admin_id']").html(response.valores[0].first_name+' '+response.valores[0].last_name);
				$("label[for='Geo_id']").html(response.valores[0].geo_nombre);
				$("label[for='ClaseCosto_id']").html(response.valores[0].ccost_nombre);
				$("label[for='Afuncional_id']").html(response.valores[0].afuncional_nombre);
				


		});
<?php endif ?>




$('.addC').relCopy({});
$('.addCReq').relCopy({clearInputs: false});
$('.addCPeople').relCopy({clearInputs: false});
$('.editor').summernote();
$('.previewImage').fancybox();
$('.tips').tooltip();
$(".select2").select2({width:"98%"});
$('.date').datepicker({format:'yyyy-mm-dd',autoClose:true})
$('.datetime').datetimepicker({format: 'yyyy-mm-dd hh:ii:ss'});

$('input[type="checkbox"],input[type="radio"]').iCheck({
	checkboxClass: 'icheckbox_square-red',
	radioClass: 'iradio_square-red',
});
	$('#tablaPersonaCentro .checkall').on('ifChecked',function(){
		$('#tablaPersonaCentro input[type="checkbox"]').iCheck('check');
	});
	$('#tablaPersonaCentro .checkall').on('ifUnchecked',function(){
		$('#tablaPersonaCentro input[type="checkbox"]').iCheck('uncheck');
	});

	$('#tablaPersonaCentro').DataTable({
        "dom": 'Tfigtlp',
        "tableTools": {
            "sSwfPath": "sximo/js/plugins/dataTables/swf/copy_csv_xls_pdf.swf"
        },
        "language": {
            "url": "sximo/js/plugins/dataTables/language/Spanish.json"
        },
        "info": false,
        "bPaginate": false,
        "columnDefs": [
                        { "orderable": false, "targets": 0 },
                        { "orderable": false, "targets": 1 }
                      ],
        "initComplete": function(settings, json) {

        	limprimir = '<label style="float: left;">';
        	limprimir += '  Centros:';
			limprimir += '	<select name="Filtrocentros" id="Filtrocentros" class="bulk_IdCentro form-control input-sm"></select>';
			limprimir += '</label>';
            $('#tablaPersonaCentro_wrapper .DTTT_container').before(limprimir);

            $("#Filtrocentros").focus(function(){
				var lobjIdCentro = $("#Filtrocentros");
				var lintIdCentro;
				var lintExists = 0;
				lobjIdCentro.find("option").each(function () {
					lintIdCentro = $(this).val();
					lintExists = 0;
					$(".bulktwo_IdCentro").each(function () {
						if ($(this).val()==lintIdCentro) {
							lintExists = 1;
						}
					});
					if (!lintExists){
						if (lintIdCentro) {
						  lobjIdCentro.find("option[value='"+lintIdCentro+"']").remove();
						}
					}
				});
				console.log(lintIdCentroCurrent);

			});

            $("#Filtrocentros").on("change",function(event){
				if ($(this).val()!==''){
					lintIdCentroCurrent = $(this).val();
					$.post('contratos/datos',{ IdCentro:$(this).val()}, function(data) {
						$("#tablaPersonaCentro .areaTH").remove();
						$("#tablaPersonaCentro .areaTB").remove();
						for( i = 0; i < data.areasT.length;i++){
						  $("<th width='30' data-th-idarea='"+data.areasT[i].IdAreaTrabajo+"'' class='areaTH'><input class='checkareaall' type='checkbox' id='"+data.areasT[i].IdAreaTrabajo+"' value='"+data.areasT[i].IdAreaTrabajo+"' /> "+data.areasT[i].Descripcion+"</th>").appendTo("#tablaPersonaCentro thead tr" );
						  $("<td align='center' class='areaTB checkareaall"+data.areasT[i].IdAreaTrabajo+"'><input class='checkarea' type='checkbox'  name='IdAreaTrabajoP[]' id='"+data.areasT[i].IdAreaTrabajo+"' value='"+data.areasT[i].IdAreaTrabajo+"' /><input type='hidden' name='IdCentroT'  id='"+data.areasT[i].IdCentro+"' value='"+data.areasT[i].IdCentro+"' ></td>")
						     .appendTo("#tablaPersonaCentro tbody tr");
						}
						$('input[type="checkbox"],input[type="radio"]').iCheck({
							checkboxClass: 'icheckbox_square-red',
							radioClass: 'iradio_square-red',
						});
						$('.checkarea').on('ifChecked',function(){
						  valueidpersona = $(this).parent("div").parent("td").parent("tr").attr("id");
						  valueidpersona = valueidpersona.replace("idpeopleTR","");
						  if (peopleaccess[valueidpersona]){
							peopleaccess[valueidpersona].push($(this).attr('id'));
			  			  }else{
			  			  	peopleaccess[valueidpersona] = [$(this).attr('id')];
			  			  }
						  console.log(JSON.stringify(peopleaccess));
						});
						$('.checkarea').on('ifUnchecked',function(){
						  valueidpersona = $(this).parent("div").parent("td").parent("tr").attr("id");
						  valueidpersona = valueidpersona.replace("idpeopleTR","");
						  valueidarea = $(this).attr('id');
						  indexarraccess = peopleaccess[valueidpersona].indexOf(valueidarea)
						  //delete peopleaccess[valueidpersona][indexarraccess];
						  peopleaccess[valueidpersona].splice(indexarraccess, 1);
						  console.log(JSON.stringify(peopleaccess));
						});
						$('#tablaPersonaCentro .checkpeopleall').on('ifChecked',function(){
							$('#idpeopleTR'+$(this).attr("value")+' input[type="checkbox"]').iCheck('check');
						});
						$('#tablaPersonaCentro .checkpeopleall').on('ifUnchecked',function(){
							$('#idpeopleTR'+$(this).attr("value")+' input[type="checkbox"]').iCheck('uncheck');
						});
						$('#tablaPersonaCentro .checkpeopleall').iCheck('uncheck');
						//alert($("#idpeopleTR31 #22").attr("id"));

						$('#tablaPersonaCentro .checkareaall').on('ifChecked',function(){
							$('.checkareaall'+$(this).attr("value")+' input[type="checkbox"]').iCheck('check');
						});
						$('#tablaPersonaCentro .checkareaall').on('ifUnchecked',function(){
							$('.checkareaall'+$(this).attr("value")+' input[type="checkbox"]').iCheck('uncheck');
						});
						loadaccess();
					});
				}
			});

        }
    });

$('.removeMultiFiles').on('click',function(){
	var removeUrl = '{{ url("contratos/removefiles?file=")}}'+$(this).attr('url');
	$(this).parent().remove();
	$.get(removeUrl,function(response){});
	$(this).parent('div').empty();
	return false;
});

var form = $('#contratosFormAjax');
form.parsley();
form.submit(function(){

	if(form.parsley('isValid') == true){
		var options = {
			dataType:      'json',
			data:{access:peopleaccess},
			beforeSubmit :  showRequest,
			success:       showResponse
		}
		$(this).ajaxSubmit(options);
		return false;

	} else {
		return false;
	}

});

});

function showRequest()
{
	$('.ajaxLoading').show();
}
function showResponse(data)  {

	if(data.status == 'success')
	{
		ajaxViewClose('#{{ $pageModule }}');
		ajaxFilter('#{{ $pageModule }}','{{ $pageUrl }}/data');
		notyMessage(data.message);
		$('#sximo-modal').modal('hide');
	} else {
		notyMessageError(data.message);
		$('.ajaxLoading').hide();
		return false;
	}
}

</script>
<style type="text/css">

	@if(count($subformtwo['rowData'])==0)
	#Centros table tbody tr:first-child {
		display:none;
	}
	@endif
	@if(count($subform['rowData'])==0)
	#Personas table tbody tr:first-child {
		display:none;
	}
	@endif
	.table th.right { text-align:right !important;}
.table th.center { text-align:center !important;}
/* DATATABLES */
.table-responsive {
	   border:none;
}
table.dataTable thead .sorting,
table.dataTable thead .sorting_asc:after,
table.dataTable thead .sorting_desc,
table.dataTable thead .sorting_asc_disabled,
table.dataTable thead .sorting_desc_disabled {
  background: transparent;
}
.dataTables_wrapper {
  padding-bottom: 30px;
}
.dataTables_length {
  float: left;
}
body.DTTT_Print {
  background: #fff;
}
.DTTT_Print #page-wrapper {
  margin: 0;
  background: #fff;
}
button.DTTT_button,
div.DTTT_button,
a.DTTT_button {
  border: 1px solid #e7eaec;
  background: #fff;
  color: #676a6c;
  box-shadow: none;
  padding: 6px 8px;
}
button.DTTT_button:hover,
div.DTTT_button:hover,
a.DTTT_button:hover {
  border: 1px solid #d2d2d2;
  background: #fff;
  color: #676a6c;
  box-shadow: none;
  padding: 6px 8px;
}
button.DTTT_button:hover:not(.DTTT_disabled),
div.DTTT_button:hover:not(.DTTT_disabled),
a.DTTT_button:hover:not(.DTTT_disabled) {
  border: 1px solid #d2d2d2;
  background: #fff;
  box-shadow: none;
}
.dataTables_filter label {
  margin-right: 5px;
}
.table thead {
	border-top:1px solid #999999;
}
.table-responsive {
	padding-top:15px;
}
	#RequisitosContratista table tbody tr:last-child {
		display:none;
	}
	#RequisitosContrato table tbody tr:last-child {
		display:none;
	}
	#RequisitosCentros table tbody tr:last-child {
		display:none;
	}
</style>
