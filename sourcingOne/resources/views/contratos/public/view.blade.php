<div class="m-t" style="padding-top:25px;">	
    <div class="row m-b-lg animated fadeInDown delayp1 text-center">
        <h3> {{ $pageTitle }} <small> {{ $pageNote }} </small></h3>
        <hr />       
    </div>
</div>
<div class="m-t">
	<div class="table-responsive" > 	

		<table class="table table-striped table-bordered" >
			<tbody>	
		
			
					<tr>
						<td width='30%' class='label-view text-right'>{{ SiteHelpers::activeLang('Contratista', (isset($fields['IdContratista']['language'])? $fields['IdContratista']['language'] : array())) }}</td>
						<td>{{ SiteHelpers::formatLookUp($row->IdContratista,'IdContratista','1:tbl_contratistas:IdContratista:RazonSocial') }} </td>
						
					</tr>
				
					<tr>
						<td width='30%' class='label-view text-right'>{{ SiteHelpers::activeLang('Numero', (isset($fields['cont_numero']['language'])? $fields['cont_numero']['language'] : array())) }}</td>
						<td>{{ $row->cont_numero}} </td>
						
					</tr>
				
					<tr>
						<td width='30%' class='label-view text-right'>{{ SiteHelpers::activeLang('Categoria', (isset($fields['categoria_id']['language'])? $fields['categoria_id']['language'] : array())) }}</td>
						<td>{{ SiteHelpers::formatLookUp($row->categoria_id,'categoria_id','1:tbl_contcategorias:categorias_id:cat_nombre') }} </td>
						
					</tr>
				
					<tr>
						<td width='30%' class='label-view text-right'>{{ SiteHelpers::activeLang('Fecha Inicio', (isset($fields['cont_fechaInicio']['language'])? $fields['cont_fechaInicio']['language'] : array())) }}</td>
						<td>{{ $row->cont_fechaInicio}} </td>
						
					</tr>
				
					<tr>
						<td width='30%' class='label-view text-right'>{{ SiteHelpers::activeLang('Fecha Fin', (isset($fields['cont_fechaFin']['language'])? $fields['cont_fechaFin']['language'] : array())) }}</td>
						<td>{{ $row->cont_fechaFin}} </td>
						
					</tr>
				
					<tr>
						<td width='30%' class='label-view text-right'>{{ SiteHelpers::activeLang('Monto Total', (isset($fields['cont_montoTotal']['language'])? $fields['cont_montoTotal']['language'] : array())) }}</td>
						<td>{{ $row->cont_montoTotal}} </td>
						
					</tr>
				
					<tr>
						<td width='30%' class='label-view text-right'>{{ SiteHelpers::activeLang('Administrador', (isset($fields['admin_id']['language'])? $fields['admin_id']['language'] : array())) }}</td>
						<td>{{ SiteHelpers::formatLookUp($row->admin_id,'admin_id','1:tb_users:id:first_name|last_name') }} </td>
						
					</tr>
				
					<tr>
						<td width='30%' class='label-view text-right'>{{ SiteHelpers::activeLang('Segmento', (isset($fields['segmento_id']['language'])? $fields['segmento_id']['language'] : array())) }}</td>
						<td>{{ SiteHelpers::formatLookUp($row->segmento_id,'segmento_id','1:tbl_contsegmento:segmento_id:seg_nombre') }} </td>
						
					</tr>
				
					<tr>
						<td width='30%' class='label-view text-right'>{{ SiteHelpers::activeLang('Geográfico', (isset($fields['geo_id']['language'])? $fields['geo_id']['language'] : array())) }}</td>
						<td>{{ SiteHelpers::formatLookUp($row->geo_id,'geo_id','1:tbl_contgeografico:geo_id:geo_nombre') }} </td>
						
					</tr>
				
					<tr>
						<td width='30%' class='label-view text-right'>{{ SiteHelpers::activeLang('Área Funcional', (isset($fields['afuncional_id']['language'])? $fields['afuncional_id']['language'] : array())) }}</td>
						<td>{{ SiteHelpers::formatLookUp($row->afuncional_id,'afuncional_id','1:tbl_contareafuncional:afuncional_id:afuncional_nombre') }} </td>
						
					</tr>
				
					<tr>
						<td width='30%' class='label-view text-right'>{{ SiteHelpers::activeLang('Clase Costo', (isset($fields['claseCosto_id']['language'])? $fields['claseCosto_id']['language'] : array())) }}</td>
						<td>{{ SiteHelpers::formatLookUp($row->claseCosto_id,'claseCosto_id','1:tbl_contclasecosto:claseCosto_id:ccost_nombre') }} </td>
						
					</tr>
				
					<tr>
						<td width='30%' class='label-view text-right'>{{ SiteHelpers::activeLang('Fecha Estado', (isset($fields['cont_fechaEstado']['language'])? $fields['cont_fechaEstado']['language'] : array())) }}</td>
						<td>{{ $row->cont_fechaEstado}} </td>
						
					</tr>
				
					<tr>
						<td width='30%' class='label-view text-right'>{{ SiteHelpers::activeLang('Glosa', (isset($fields['cont_glosaDescriptiva']['language'])? $fields['cont_glosaDescriptiva']['language'] : array())) }}</td>
						<td>{{ $row->cont_glosaDescriptiva}} </td>
						
					</tr>
				
					<tr>
						<td width='30%' class='label-view text-right'>{{ SiteHelpers::activeLang('Estado', (isset($fields['cont_estado']['language'])? $fields['cont_estado']['language'] : array())) }}</td>
						<td>{{ $row->cont_estado}} </td>
						
					</tr>
						
					<tr>
						<td width='30%' class='label-view text-right'></td>
						<td> <a href="javascript:history.go(-1)" class="btn btn-primary"> Back To Grid <a> </td>
						
					</tr>					
				
			</tbody>	
		</table>   

	 
	
	</div>
</div>	