

		 {!! Form::open(array('url'=>'contratos/savepublic', 'class'=>'form-horizontal','files' => true , 'parsley-validate'=>'','novalidate'=>' ')) !!}

	@if(Session::has('messagetext'))
	  
		   {!! Session::get('messagetext') !!}
	   
	@endif
	<ul class="parsley-error-list">
		@foreach($errors->all() as $error)
			<li>{{ $error }}</li>
		@endforeach
	</ul>		


<ul class="nav nav-tabs"><li class="active"><a href="#Contratos" data-toggle="tab">Contratos</a></li>
				<li class=""><a href="#Personas" data-toggle="tab">Personas</a></li>
				</ul><div class="tab-content"><div class="tab-pane m-t active" id="Contratos"> 
				{!! Form::hidden('contrato_id', $row['contrato_id']) !!}					
									  <div class="form-group  " >
										<label for="Contratista" class=" control-label col-md-4 text-left"> Contratista <span class="asterix"> * </span></label>
										<div class="col-md-6">
										  <select name='IdContratista' rows='5' id='IdContratista' class='select2 ' required  ></select> 
										 </div> 
										 <div class="col-md-2">
										 	
										 </div>
									  </div> 					
									  <div class="form-group  " >
										<label for="Numero" class=" control-label col-md-4 text-left"> Numero </label>
										<div class="col-md-6">
										  {!! Form::text('cont_numero', $row['cont_numero'],array('class'=>'form-control', 'placeholder'=>'',   )) !!} 
										 </div> 
										 <div class="col-md-2">
										 	
										 </div>
									  </div> 					
									  <div class="form-group  " >
										<label for="Categoria" class=" control-label col-md-4 text-left"> Categoria <span class="asterix"> * </span></label>
										<div class="col-md-6">
										  <select name='categoria_id' rows='5' id='categoria_id' class='select2 ' required  ></select> 
										 </div> 
										 <div class="col-md-2">
										 	
										 </div>
									  </div> 					
									  <div class="form-group  " >
										<label for="Fecha Inicio" class=" control-label col-md-4 text-left"> Fecha Inicio </label>
										<div class="col-md-6">
										  
				<div class="input-group m-b" style="width:150px !important;">
					{!! Form::text('cont_fechaInicio', $row['cont_fechaInicio'],array('class'=>'form-control date')) !!}
					<span class="input-group-addon"><i class="fa fa-calendar"></i></span>
				</div> 
										 </div> 
										 <div class="col-md-2">
										 	
										 </div>
									  </div> 					
									  <div class="form-group  " >
										<label for="Fecha Fin" class=" control-label col-md-4 text-left"> Fecha Fin </label>
										<div class="col-md-6">
										  
				<div class="input-group m-b" style="width:150px !important;">
					{!! Form::text('cont_fechaFin', $row['cont_fechaFin'],array('class'=>'form-control date')) !!}
					<span class="input-group-addon"><i class="fa fa-calendar"></i></span>
				</div> 
										 </div> 
										 <div class="col-md-2">
										 	
										 </div>
									  </div> 					
									  <div class="form-group  " >
										<label for="Monto Total" class=" control-label col-md-4 text-left"> Monto Total </label>
										<div class="col-md-6">
										  {!! Form::text('cont_montoTotal', $row['cont_montoTotal'],array('class'=>'form-control', 'placeholder'=>'',   )) !!} 
										 </div> 
										 <div class="col-md-2">
										 	
										 </div>
									  </div> 					
									  <div class="form-group  " >
										<label for="Administrador" class=" control-label col-md-4 text-left"> Administrador <span class="asterix"> * </span></label>
										<div class="col-md-6">
										  <select name='admin_id' rows='5' id='admin_id' class='select2 ' required  ></select> 
										 </div> 
										 <div class="col-md-2">
										 	
										 </div>
									  </div> 					
									  <div class="form-group  " >
										<label for="Segmento" class=" control-label col-md-4 text-left"> Segmento <span class="asterix"> * </span></label>
										<div class="col-md-6">
										  <select name='segmento_id' rows='5' id='segmento_id' class='select2 ' required  ></select> 
										 </div> 
										 <div class="col-md-2">
										 	
										 </div>
									  </div> 					
									  <div class="form-group  " >
										<label for="Geográfico" class=" control-label col-md-4 text-left"> Geográfico <span class="asterix"> * </span></label>
										<div class="col-md-6">
										  <select name='geo_id' rows='5' id='geo_id' class='select2 ' required  ></select> 
										 </div> 
										 <div class="col-md-2">
										 	
										 </div>
									  </div> 					
									  <div class="form-group  " >
										<label for="Área Funcional" class=" control-label col-md-4 text-left"> Área Funcional <span class="asterix"> * </span></label>
										<div class="col-md-6">
										  <select name='afuncional_id' rows='5' id='afuncional_id' class='select2 ' required  ></select> 
										 </div> 
										 <div class="col-md-2">
										 	
										 </div>
									  </div> 					
									  <div class="form-group  " >
										<label for="Clase Costo" class=" control-label col-md-4 text-left"> Clase Costo <span class="asterix"> * </span></label>
										<div class="col-md-6">
										  <select name='claseCosto_id' rows='5' id='claseCosto_id' class='select2 ' required  ></select> 
										 </div> 
										 <div class="col-md-2">
										 	
										 </div>
									  </div>

									  <div class="form-group  " >
											<label for="Tipo Gasto" class=" control-label col-md-4 text-left"> Tipo Gasto <span class="asterix"> * </span></label>
											<div class="col-md-6">
												<select name='id_tipogasto' rows='5' id='id_tipogasto' class='select2 ' required  ></select>
											</div>
											<div class="col-md-2">

											</div>
										</div>

										<div class="form-group  " >
											<label for="Extension" class=" control-label col-md-4 text-left"> Extensión <span class="asterix"> * </span></label>
											<div class="col-md-6">
												<select name='id_extension' rows='5' id='id_extension' class='select2 ' required  ></select>
											</div>
											<div class="col-md-2">

											</div>
										</div> 					
									  <div class="form-group  " >
										<label for="Estado" class=" control-label col-md-4 text-left"> Estado </label>
										<div class="col-md-6">
										  
					<?php $cont_estado = explode(',',$row['cont_estado']);
					$cont_estado_opt = array( '' => 'Seleccione Valor' ,  'Activo' => 'Activo' ,  'Suspendido' => 'Suspendido' , ); ?>
					<select name='cont_estado' rows='5'   class='select2 '  > 
						<?php 
						foreach($cont_estado_opt as $key=>$val)
						{
							echo "<option  value ='$key' ".($row['cont_estado'] == $key ? " selected='selected' " : '' ).">$val</option>"; 						
						}						
						?></select> 
										 </div> 
										 <div class="col-md-2">
										 	
										 </div>
									  </div> 
			</div>
			
			<div class="tab-pane m-t " id="Personas"> 
				
			</div>
			
			

			<div style="clear:both"></div>	
				
					
				  <div class="form-group">
					<label class="col-sm-4 text-right">&nbsp;</label>
					<div class="col-sm-8">	
					<button type="submit" name="apply" class="btn btn-info btn-sm" ><i class="fa  fa-check-circle"></i> {{ Lang::get('core.sb_apply') }}</button>
					<button type="submit" name="submit" class="btn btn-primary btn-sm" ><i class="fa  fa-save "></i> {{ Lang::get('core.sb_save') }}</button>
				  </div>	  
			
		</div> 
		 
		 {!! Form::close() !!}
		 
   <script type="text/javascript">
	$(document).ready(function() { 
		$('.addC').relCopy({});
		
		$("#IdContratista").jCombo("{!! url('contratos/comboselect?filter=tbl_contratistas:IdContratista:RUT|RazonSocial') !!}",
		{  selected_value : '{{ $row["IdContratista"] }}' });
		
		$("#categoria_id").jCombo("{!! url('contratos/comboselect?filter=tbl_contcategorias:categorias_id:cat_nombre') !!}",
		{  selected_value : '{{ $row["categoria_id"] }}' });
		
		$("#admin_id").jCombo("{!! url('contratos/comboselect?filter=tb_users:id:first_name|last_name') !!}",
		{  selected_value : '{{ $row["admin_id"] }}' });
		
		$("#segmento_id").jCombo("{!! url('contratos/comboselect?filter=tbl_contsegmento:segmento_id:seg_nombre') !!}",
		{  selected_value : '{{ $row["segmento_id"] }}' });
		
		$("#geo_id").jCombo("{!! url('contratos/comboselect?filter=tbl_contgeografico:geo_id:geo_nombre') !!}",
		{  selected_value : '{{ $row["geo_id"] }}' });
		
		$("#afuncional_id").jCombo("{!! url('contratos/comboselect?filter=tbl_contareafuncional:afuncional_id:afuncional_nombre') !!}",
		{  selected_value : '{{ $row["afuncional_id"] }}' });
		
		$("#claseCosto_id").jCombo("{!! url('contratos/comboselect?filter=tbl_contclasecosto:claseCosto_id:ccost_nombre') !!}",
		{  selected_value : '{{ $row["claseCosto_id"] }}' });

		$("#id_tipogasto").jCombo("{!! url('contratos/comboselect?filter=tbl_contrato_tipogasto:id_tipogasto:nombre_gasto') !!}",
		{  selected_value : '{{ $row["id_tipogasto"] }}' });

		$("#id_extension").jCombo("{!! url('contratos/comboselect?filter=tbl_contrato_extension:id_extension:nombre_ext') !!}",
		{  selected_value : '{{ $row["id_extension"] }}' });
			 

		$('.removeCurrentFiles').on('click',function(){
			var removeUrl = $(this).attr('href');
			$.get(removeUrl,function(response){});
			$(this).parent('div').empty();	
			return false;
		});		
		
	});
	</script>		 
