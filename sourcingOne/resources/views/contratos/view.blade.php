@if($setting['view-method'] =='native')
<div class="sbox">
	<div class="sbox-title">
		<h4> <i class="fa fa-table"></i> <?php echo $pageTitle ;?> <small>{{ $pageNote }}</small>
			<a href="javascript:void(0)" class="collapse-close pull-right btn btn-xs btn-danger" onclick="ajaxViewClose('#{{ $pageModule }}')">
			<i class="fa fa fa-times"></i></a>
		</h4>
	 </div>

	<div class="sbox-content">
@endif

  <!-- Nav tabs -->
  <ul class="nav nav-tabs" role="tablist">
  	<li role="presentation" class="active"><a href="#home{{ $row->contrato_id }}" aria-controls="home" role="tab" data-toggle="tab">  {{ $pageTitle}} :   View Detail </a></li>
	@foreach($subgrid as $sub)
		<li role="presentation"><a href="#{{ str_replace(" ","_",$sub['title']) }}{{ $row->{$sub['master_key']} }}" aria-controls="profile" role="tab" data-toggle="tab">{{ $pageTitle}} :  {{ $sub['title'] }}</a></li>
	@endforeach
  </ul>


  <!-- Tab panes -->
  <div class="tab-content m-t">
  	<div role="tabpanel" class="tab-pane active" id="home{{ $row->contrato_id }}">

		<div class="table-responsive" >
			<table class="table table-striped table-bordered" >
				<tbody>

					<tr>
						<td width='30%' class='label-view text-right'>{{ SiteHelpers::activeLang('Contratista', (isset($fields['IdContratista']['language'])? $fields['IdContratista']['language'] : array())) }}</td>
						<td>{{ SiteHelpers::formatLookUp($row->IdContratista,'IdContratista','1:tbl_contratistas:IdContratista:RazonSocial') }} </td>

					</tr>

					<tr>
						<td width='30%' class='label-view text-right'>{{ SiteHelpers::activeLang('Numero', (isset($fields['cont_numero']['language'])? $fields['cont_numero']['language'] : array())) }}</td>
						<td>{{ $row->cont_numero}} </td>

					</tr>

					<tr>
						<td width='30%' class='label-view text-right'>{{ SiteHelpers::activeLang('Categoria', (isset($fields['categoria_id']['language'])? $fields['categoria_id']['language'] : array())) }}</td>
						<td>{{ SiteHelpers::formatLookUp($row->categoria_id,'categoria_id','1:tbl_contcategorias:categorias_id:cat_nombre') }} </td>

					</tr>

					<tr>
						<td width='30%' class='label-view text-right'>{{ SiteHelpers::activeLang('Fecha Inicio', (isset($fields['cont_fechaInicio']['language'])? $fields['cont_fechaInicio']['language'] : array())) }}</td>
						<td>{{ $row->cont_fechaInicio}} </td>

					</tr>

					<tr>
						<td width='30%' class='label-view text-right'>{{ SiteHelpers::activeLang('Fecha Fin', (isset($fields['cont_fechaFin']['language'])? $fields['cont_fechaFin']['language'] : array())) }}</td>
						<td>{{ $row->cont_fechaFin}} </td>

					</tr>

					<tr>
						<td width='30%' class='label-view text-right'>{{ SiteHelpers::activeLang('Monto Total', (isset($fields['cont_montoTotal']['language'])? $fields['cont_montoTotal']['language'] : array())) }}</td>
						<td>{{ $row->cont_montoTotal}} </td>

					</tr>
					
					<tr>
						<td width='30%' class='label-view text-right'>Garantía</td>
						<td>{{ $row->cont_garantia}} </td>

					</tr>

					<tr>
						<td width='30%' class='label-view text-right'>{{ SiteHelpers::activeLang('Administrador', (isset($fields['admin_id']['language'])? $fields['admin_id']['language'] : array())) }}</td>
						<td>{{ SiteHelpers::formatLookUp($row->admin_id,'admin_id','1:tb_users:id:first_name|last_name') }} </td>

					</tr>

					<tr>
						<td width='30%' class='label-view text-right'>{{ SiteHelpers::activeLang('Segmento', (isset($fields['segmento_id']['language'])? $fields['segmento_id']['language'] : array())) }}</td>
						<td>{{ SiteHelpers::formatLookUp($row->segmento_id,'segmento_id','1:tbl_contsegmento:segmento_id:seg_nombre') }} </td>

					</tr>

					<tr>
						<td width='30%' class='label-view text-right'>{{ SiteHelpers::activeLang('Geográfico', (isset($fields['geo_id']['language'])? $fields['geo_id']['language'] : array())) }}</td>
						<td>{{ SiteHelpers::formatLookUp($row->geo_id,'geo_id','1:tbl_contgeografico:geo_id:geo_nombre') }} </td>

					</tr>

					<tr>
						<td width='30%' class='label-view text-right'>{{ SiteHelpers::activeLang('Área Funcional', (isset($fields['afuncional_id']['language'])? $fields['afuncional_id']['language'] : array())) }}</td>
						<td>{{ SiteHelpers::formatLookUp($row->afuncional_id,'afuncional_id','1:tbl_contareafuncional:afuncional_id:afuncional_nombre') }} </td>

					</tr>

					<tr>
						<td width='30%' class='label-view text-right'>{{ SiteHelpers::activeLang('Clase Costo', (isset($fields['claseCosto_id']['language'])? $fields['claseCosto_id']['language'] : array())) }}</td>
						<td>{{ SiteHelpers::formatLookUp($row->claseCosto_id,'claseCosto_id','1:tbl_contclasecosto:claseCosto_id:ccost_nombre') }} </td>

					</tr>
					
					<tr>
						<td width='30%' class='label-view text-right'>{{ SiteHelpers::activeLang('Tipo Gasto', (isset($fields['id_tipogasto']['language'])? $fields['id_tipogasto']['language'] : array())) }}</td>
						<td>{{ SiteHelpers::formatLookUp($row->id_tipogasto,'id_tipogasto','1:tbl_contrato_tipogasto:id_tipogasto:nombre_gasto') }} </td>
						
					</tr>

					<tr>
						<td width='30%' class='label-view text-right'>{{ SiteHelpers::activeLang('Extensión', (isset($fields['id_extension']['language'])? $fields['id_extension']['language'] : array())) }}</td>
						<td>{{ SiteHelpers::formatLookUp($row->id_extension,'id_extension','1:tbl_contrato_extension:id_extension:nombre') }} </td>
						
					</tr>
					
					<tr>
						<td width='30%' class='label-view text-right'>{{ SiteHelpers::activeLang('Complejidad', (isset($fields['complejidad']['language'])? $fields['complejidad']['language'] : array())) }}</td>
						<td>{{ $row->complejidad}} </td>

					</tr>

					<tr>
						<td width='30%' class='label-view text-right'>{{ SiteHelpers::activeLang('Impacto', (isset($fields['impacto']['language'])? $fields['impacto']['language'] : array())) }}</td>
						<td>{{ $row->impacto}} </td>

					</tr>


					<tr>
						<td width='30%' class='label-view text-right'>{{ SiteHelpers::activeLang('Fecha Estado', (isset($fields['cont_fechaEstado']['language'])? $fields['cont_fechaEstado']['language'] : array())) }}</td>
						<td>{{ $row->cont_fechaEstado}} </td>

					</tr>

					<tr>
						<td width='30%' class='label-view text-right'>{{ SiteHelpers::activeLang('Glosa', (isset($fields['cont_glosaDescriptiva']['language'])? $fields['cont_glosaDescriptiva']['language'] : array())) }}</td>
						<td>{{ $row->cont_glosaDescriptiva}} </td>

					</tr>

					<tr>
						<td width='30%' class='label-view text-right'>{{ SiteHelpers::activeLang('Estado', (isset($fields['cont_estado']['language'])? $fields['cont_estado']['language'] : array())) }}</td>
						<td>{{ $row->cont_estado}} </td>

					</tr>

				</tbody>
			</table>
		</div>

  	</div>
  	@foreach($subgrid as $sub)
  	<div role="tabpanel" class="tab-pane" id="{{ str_replace(" ","_",$sub['title']) }}{{ $row->{$sub['master_key']} }}"></div>
  	@endforeach
  </div>



@if($setting['form-method'] =='native')
	</div>
</div>
@endif

<script type="text/javascript">
	$(function(){
		<?php foreach($subgrid as $sub) { ?>
			$('#{{ str_replace(" ","_",$sub['title']) }}{{ $row->{$sub['master_key']} }}').load('{!! url($sub['module']."/lookup/".implode("-",$sub)."-".$row->{$sub['master_key']})!!}')
		<?php } ?>


	})

</script>
