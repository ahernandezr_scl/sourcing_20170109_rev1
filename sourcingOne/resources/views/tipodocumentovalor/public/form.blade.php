

		 {!! Form::open(array('url'=>'tipodocumentovalor/savepublic', 'class'=>'form-horizontal','files' => true , 'parsley-validate'=>'','novalidate'=>' ')) !!}

	@if(Session::has('messagetext'))
	  
		   {!! Session::get('messagetext') !!}
	   
	@endif
	<ul class="parsley-error-list">
		@foreach($errors->all() as $error)
			<li>{{ $error }}</li>
		@endforeach
	</ul>		


<div class="col-md-12">
						<fieldset><legend> Tipo Documentos Valor</legend>
				{!! Form::hidden('IdTipoDocumentoValor', $row['IdTipoDocumentoValor']) !!}
									  <div class="form-group  " >
										<label for="Tipo Documento" class=" control-label col-md-4 text-left"> Tipo Documento </label>
										<div class="col-md-6">
										  <select name='IdTipoDocumento' rows='5' id='IdTipoDocumento' class='select2 '   ></select>
										 </div>
										 <div class="col-md-2">
										 	
										 </div>
									  </div> 
									  <div class="form-group  " >
										<label for="Etiqueta" class=" control-label col-md-4 text-left"> Etiqueta <span class="asterix"> * </span></label>
										<div class="col-md-6">
										  {!! Form::text('Etiqueta', $row['Etiqueta'],array('class'=>'form-control', 'placeholder'=>'', 'required'=>'true'  )) !!}
										 </div>
										 <div class="col-md-2">
										 	
										 </div>
									  </div> 
									  <div class="form-group  " >
										<label for="Tipo Valor" class=" control-label col-md-4 text-left"> Tipo Valor <span class="asterix"> * </span></label>
										<div class="col-md-6">
										  
					<?php $TipoValor = explode(',',$row['TipoValor']);
					$TipoValor_opt = array( 'Texto' => 'Texto' ,  'Numérico' => 'Numérico' ,  'Fecha' => 'Fecha' ,  'Radio' => 'Radio' ,  'CheckBox' => 'CheckBox' ,  'Select Option' => 'Select Option' , ); ?>
					<select name='TipoValor' rows='5' required  class='select2 '  > 
						<?php
						foreach($TipoValor_opt as $key=>$val)
						{
							echo "<option  value ='$key' ".($row['TipoValor'] == $key ? " selected='selected' " : '' ).">$val</option>";
						}
						?></select>
										 </div>
										 <div class="col-md-2">
										 	
										 </div>
									  </div> 
									  <div class="form-group  " >
										<label for="Requerido" class=" control-label col-md-4 text-left"> Requerido </label>
										<div class="col-md-6">
										  
					<?php $Requerido = explode(',',$row['Requerido']);
					$Requerido_opt = array( 'SI' => 'SI' ,  'No' => 'NO' , ); ?>
					<select name='Requerido' rows='5'   class='select2 '  > 
						<?php
						foreach($Requerido_opt as $key=>$val)
						{
							echo "<option  value ='$key' ".($row['Requerido'] == $key ? " selected='selected' " : '' ).">$val</option>";
						}
						?></select>
										 </div>
										 <div class="col-md-2">
										 	
										 </div>
									  </div> </fieldset>
			</div>

			

			<div style="clear:both"></div>	
				
					
				  <div class="form-group">
					<label class="col-sm-4 text-right">&nbsp;</label>
					<div class="col-sm-8">	
					<button type="submit" name="apply" class="btn btn-info btn-sm" ><i class="fa  fa-check-circle"></i> {{ Lang::get('core.sb_apply') }}</button>
					<button type="submit" name="submit" class="btn btn-primary btn-sm" ><i class="fa  fa-save "></i> {{ Lang::get('core.sb_save') }}</button>
				  </div>	  
			
		</div> 
		 
		 {!! Form::close() !!}
		 
   <script type="text/javascript">
	$(document).ready(function() { 
		$('.addC').relCopy({});
		
		$("#IdTipoDocumento").jCombo("{!! url('tipodocumentovalor/comboselect?filter=tbl_tipos_documentos:IdTipoDocumento:Descripcion') !!}",
		{  selected_value : '{{ $row["IdTipoDocumento"] }}' });
		 

		$('.removeCurrentFiles').on('click',function(){
			var removeUrl = $(this).attr('href');
			$.get(removeUrl,function(response){});
			$(this).parent('div').empty();	
			return false;
		});		
		
	});
	</script>		 
