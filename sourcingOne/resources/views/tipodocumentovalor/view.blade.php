@if($setting['view-method'] =='native')
<div class="sbox">
	<div class="sbox-title">  
		<h4> <i class="fa fa-table"></i> <?php echo $pageTitle ;?> <small>{{ $pageNote }}</small>
			<a href="javascript:void(0)" class="collapse-close pull-right btn btn-xs btn-danger" onclick="ajaxViewClose('#{{ $pageModule }}')">
			<i class="fa fa fa-times"></i></a>
		</h4>
	 </div>

	<div class="sbox-content"> 
@endif	

		<table class="table table-striped table-bordered" >
			<tbody>	
				
					<tr>
						<td width='30%' class='label-view text-right'>{{ SiteHelpers::activeLang('IdTipoDocumentoValor', (isset($fields['IdTipoDocumentoValor']['language'])? $fields['IdTipoDocumentoValor']['language'] : array())) }}</td>
						<td>{{ $row->IdTipoDocumentoValor}} </td>

					</tr>
				
					<tr>
						<td width='30%' class='label-view text-right'>{{ SiteHelpers::activeLang('Tipo Documento', (isset($fields['IdTipoDocumento']['language'])? $fields['IdTipoDocumento']['language'] : array())) }}</td>
						<td>{{ $row->IdTipoDocumento}} </td>

					</tr>
				
					<tr>
						<td width='30%' class='label-view text-right'>{{ SiteHelpers::activeLang('Etiqueta', (isset($fields['Etiqueta']['language'])? $fields['Etiqueta']['language'] : array())) }}</td>
						<td>{{ $row->Etiqueta}} </td>

					</tr>
				
					<tr>
						<td width='30%' class='label-view text-right'>{{ SiteHelpers::activeLang('Tipo Valor', (isset($fields['TipoValor']['language'])? $fields['TipoValor']['language'] : array())) }}</td>
						<td>{{ $row->TipoValor}} </td>

					</tr>
				
					<tr>
						<td width='30%' class='label-view text-right'>{{ SiteHelpers::activeLang('Requerido', (isset($fields['Requerido']['language'])? $fields['Requerido']['language'] : array())) }}</td>
						<td>{{ $row->Requerido}} </td>

					</tr>
				
			</tbody>	
		</table>  
			
		 	

@if($setting['form-method'] =='native')
	</div>	
</div>	
@endif	

<script>
$(document).ready(function(){

});
</script>	