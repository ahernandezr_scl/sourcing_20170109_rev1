
@if($setting['form-method'] =='native')
	<div class="sbox">
		<div class="sbox-title">
			<h4> <i class="fa fa-table"></i> <?php echo $pageTitle ;?> <small>{{ $pageNote }}</small>
				<a href="javascript:void(0)" class="collapse-close pull-right btn btn-xs btn-danger" onclick="ajaxViewClose('#{{ $pageModule }}')"><i class="fa fa fa-times"></i></a>
			</h4>
	</div>

	<div class="sbox-content">
@endif
			{!! Form::open(array('url'=>'tipodocumentovalor/save/'.SiteHelpers::encryptID($row['IdTipoDocumentoValor']), 'class'=>'form-horizontal','files' => true , 'parsley-validate'=>'','novalidate'=>' ','id'=> 'tipodocumentovalorFormAjax')) !!}
			<div class="col-md-12">
						<fieldset><legend> Tipo Documentos Valor</legend>
				{!! Form::hidden('IdTipoDocumentoValor', $row['IdTipoDocumentoValor']) !!}
									  <div class="form-group  " >
										<label for="Tipo Documento" class=" control-label col-md-4 text-left"> Tipo Documento </label>
										<div class="col-md-6">
										  <select name='IdTipoDocumento' rows='5' id='IdTipoDocumento' class='select2 '   ></select>
										 </div>
										 <div class="col-md-2">

										 </div>
									  </div>
									  <div class="form-group  " >
										<label for="Etiqueta" class=" control-label col-md-4 text-left"> Etiqueta <span class="asterix"> * </span></label>
										<div class="col-md-6">
										  {!! Form::text('Etiqueta', $row['Etiqueta'],array('class'=>'form-control', 'placeholder'=>'', 'required'=>'true'  )) !!}
										 </div>
										 <div class="col-md-2">

										 </div>
									  </div>
									  <div class="form-group  " >
										<label for="Tipo Valor" class=" control-label col-md-4 text-left"> Tipo Valor <span class="asterix"> * </span></label>
										<div class="col-md-6">

					<?php $TipoValor = explode(',',$row['TipoValor']);
					$TipoValor_opt = array( 'Texto' => 'Texto' ,  'Numérico' => 'Numérico' ,  'Fecha' => 'Fecha' ,  'Radio' => 'Radio' ,  'CheckBox' => 'CheckBox' ,  'Select Option' => 'Select Option' , ); ?>
					<select name='TipoValor' rows='5' required  class='select2 '  >
						<?php
						foreach($TipoValor_opt as $key=>$val)
						{
							echo "<option  value ='$key' ".($row['TipoValor'] == $key ? " selected='selected' " : '' ).">$val</option>";
						}
						?></select>
										 </div>
										 <div class="col-md-2">

										 </div>
									  </div>
									  <div class="form-group  " >
										<label for="Requerido" class=" control-label col-md-4 text-left"> Requerido </label>
										<div class="col-md-6">

					<?php $Requerido = explode(',',$row['Requerido']);
					$Requerido_opt = array( 'SI' => 'SI' ,  'No' => 'NO' , ); ?>
					<select name='Requerido' rows='5'   class='select2 '  >
						<?php
						foreach($Requerido_opt as $key=>$val)
						{
							echo "<option  value ='$key' ".($row['Requerido'] == $key ? " selected='selected' " : '' ).">$val</option>";
						}
						?></select>
										 </div>
										 <div class="col-md-2">

										 </div>
									  </div> </fieldset>
			</div>



	<hr />
	<div class="clr clear"></div>

	<h5> Datos </h5>

	<div class="table-responsive">
    <table class="table table-striped ">
        <thead>
			<tr>
				@foreach ($subform['tableGrid'] as $t)
					@if($t['view'] =='1' && $t['field'] !='IdTipoDocumentoValor')
						<th>{{ $t['label'] }}</th>
					@endif
				@endforeach
				<th></th>
			  </tr>

        </thead>

        <tbody>
        @if(count($subform['rowData'])>=1)
            @foreach ($subform['rowData'] as $rows)
            <tr class="clone clonedInput">

			 @foreach ($subform['tableGrid'] as $field)
				 @if($field['view'] =='1' && $field['field'] !='IdTipoDocumentoValor')
				 <td>
				 	{!! SiteHelpers::bulkForm($field['field'] , $subform['tableForm'] , $rows->{$field['field']}) !!}
				 </td>
				 @endif

			 @endforeach
			 <td>
			 	<a onclick=" $(this).parents('.clonedInput').remove(); return false" href="#" class="remove btn btn-xs btn-danger">-</a>
			 	<input type="hidden" name="counter[]">
			 </td>
			@endforeach
			</tr>

		@else
            <tr class="clone clonedInput">

			 @foreach ($subform['tableGrid'] as $field)

				 @if($field['view'] =='1' && $field['field'] !='IdTipoDocumentoValor')
				 <td>
				 	{!! SiteHelpers::bulkForm($field['field'] , $subform['tableForm'] ) !!}
				 </td>
				 @endif

			 @endforeach
			 <td>
			 	<a onclick=" $(this).parents('.clonedInput').remove(); return false" href="#" class="remove btn btn-xs btn-danger">-</a>
			 	<input type="hidden" name="counter[]">
			 </td>

			</tr>


		@endif


        </tbody>

     </table>
     <input type="hidden" name="enable-masterdetail" value="true">
     </div>
	<br /><br />

     <a href="javascript:void(0);" class="addC btn btn-xs btn-info" rel=".clone"><i class="fa fa-plus"></i> New Item</a>
     <hr />

			<div style="clear:both"></div>

			<div class="form-group">
				<label class="col-sm-4 text-right">&nbsp;</label>
				<div class="col-sm-8">
					<button type="submit" class="btn btn-primary btn-sm "><i class="icon-checkmark-circle2"></i>  {{ Lang::get('core.sb_save') }} </button>
					<button type="button" onclick="ajaxViewClose('#{{ $pageModule }}')" class="btn btn-success btn-sm"><i class="icon-cancel-circle2 "></i>  {{ Lang::get('core.sb_cancel') }} </button>
				</div>
			</div>
			{!! Form::close() !!}


@if($setting['form-method'] =='native')
	</div>
</div>
@endif


</div>

<script type="text/javascript">
$(document).ready(function() {

		<?php if (strlen($row['IdTipoDocumentoValor'])==0) : ?>
 $("#ValoresA").hide();
 <? endif; ?>

		$("#IdTipoDocumento").jCombo("{!! url('tipodocumentovalor/comboselect?filter=tbl_tipos_documentos:IdTipoDocumento:Descripcion') !!}",
		{  selected_value : '{{ $row["IdTipoDocumento"] }}' });


	$('#TipoValor').on('change', function() {
	            console.log(this.value)
	            if(this.value=="Radio" || this.value=="CheckBox" || this.value=="Select Option"){
	                $("#ValoresA").show();
	            }
	            else{
	            	$("#ValoresA").hide();
	            }

        });

	$('.addC').relCopy({});
	$('.editor').summernote();
	$('.previewImage').fancybox();
	$('.tips').tooltip();
	$(".select2").select2({ width:"98%"});
	$('.date').datepicker({format:'yyyy-mm-dd',autoClose:true})
	$('.datetime').datetimepicker({format: 'yyyy-mm-dd hh:ii:ss'});
	$('input[type="checkbox"],input[type="radio"]').iCheck({
		checkboxClass: 'icheckbox_square-red',
		radioClass: 'iradio_square-red',
	});
		$('.removeMultiFiles').on('click',function(){
			var removeUrl = '{{ url("tipodocumentovalor/removefiles?file=")}}'+$(this).attr('url');
			$(this).parent().remove();
			$.get(removeUrl,function(response){});
			$(this).parent('div').empty();
			return false;
		});

	var form = $('#tipodocumentovalorFormAjax');
	form.parsley();
	form.submit(function(){

		if(form.parsley('isValid') == true){
			var options = {
				dataType:      'json',
				beforeSubmit :  showRequest,
				success:       showResponse
			}
			$(this).ajaxSubmit(options);
			return false;

		} else {
			return false;
		}

	});

});

function showRequest()
{
	$('.ajaxLoading').show();
}
function showResponse(data)  {

	if(data.status == 'success')
	{
		ajaxViewClose('#{{ $pageModule }}');
		ajaxFilter('#{{ $pageModule }}','{{ $pageUrl }}/data');
		notyMessage(data.message);
		$('#sximo-modal').modal('hide');
	} else {
		notyMessageError(data.message);
		$('.ajaxLoading').hide();
		return false;
	}
}

</script>
<style type="text/css">
  @if(count($subform['rowData'])==0)
	#tvalores tbody tr:last-child {
		display:none;
	}
    @endif
</style>
