<div class="m-t" style="padding-top:25px;">	
    <div class="row m-b-lg animated fadeInDown delayp1 text-center">
        <h3> {{ $pageTitle }} <small> {{ $pageNote }} </small></h3>
        <hr />       
    </div>
</div>
<div class="m-t">
	<div class="table-responsive" > 	

		<table class="table table-striped table-bordered" >
			<tbody>	
		
			
					<tr>
						<td width='30%' class='label-view text-right'>{{ SiteHelpers::activeLang('Contrato', (isset($fields['contrato_id']['language'])? $fields['contrato_id']['language'] : array())) }}</td>
						<td>{{ SiteHelpers::formatLookUp($row->contrato_id,'contrato_id','1:tbl_contrato:contrato_id:cont_numero|cont_proveedor') }} </td>

					</tr>
				
					<tr>
						<td width='30%' class='label-view text-right'>{{ SiteHelpers::activeLang('Ámbito', (isset($fields['ambito_id']['language'])? $fields['ambito_id']['language'] : array())) }}</td>
						<td>{{ SiteHelpers::formatLookUp($row->ambito_id,'ambito_id','1:tbl_planes_y_programas_ambito:ambito_id:ambito_nombre') }} </td>

					</tr>
				
					<tr>
						<td width='30%' class='label-view text-right'>{{ SiteHelpers::activeLang('Subámbito', (isset($fields['subambito_id']['language'])? $fields['subambito_id']['language'] : array())) }}</td>
						<td>{{ SiteHelpers::formatLookUp($row->subambito_id,'subambito_id','1:tbl_planes_y_programas_ambitosub:subambito_id:ambito_nombre') }} </td>

					</tr>
				
					<tr>
						<td width='30%' class='label-view text-right'>{{ SiteHelpers::activeLang('Tipo', (isset($fields['programas_idTipo']['language'])? $fields['programas_idTipo']['language'] : array())) }}</td>
						<td>{{ SiteHelpers::formatLookUp($row->programas_idTipo,'programas_idTipo','1:tbl_planes_y_programas_tip:programasTipo_id:programasTipo_nombre') }} </td>

					</tr>
				
					<tr>
						<td width='30%' class='label-view text-right'>{{ SiteHelpers::activeLang('Descripción', (isset($fields['programas_idDescripcion']['language'])? $fields['programas_idDescripcion']['language'] : array())) }}</td>
						<td>{{ $row->programas_idDescripcion}} </td>

					</tr>
				
					<tr>
						<td width='30%' class='label-view text-right'>{{ SiteHelpers::activeLang('Fecha Inicio', (isset($fields['programas_idFechaInicio']['language'])? $fields['programas_idFechaInicio']['language'] : array())) }}</td>
						<td>{{ date('d/m/Y',strtotime($row->programas_idFechaInicio)) }} </td>

					</tr>
				
					<tr>
						<td width='30%' class='label-view text-right'>{{ SiteHelpers::activeLang('Fecha Fin', (isset($fields['programas_idFechaFin']['language'])? $fields['programas_idFechaFin']['language'] : array())) }}</td>
						<td>{{ date('d/m/Y',strtotime($row->programas_idFechaFin)) }} </td>

					</tr>
				
					<tr>
						<td width='30%' class='label-view text-right'>{{ SiteHelpers::activeLang('Hito', (isset($fields['programas_hito']['language'])? $fields['programas_hito']['language'] : array())) }}</td>
						<td>{{ $row->programas_hito}} </td>

					</tr>
						
					<tr>
						<td width='30%' class='label-view text-right'></td>
						<td> <a href="javascript:history.go(-1)" class="btn btn-primary"> Back To Grid <a> </td>
						
					</tr>					
				
			</tbody>	
		</table>   

	 
	
	</div>
</div>	