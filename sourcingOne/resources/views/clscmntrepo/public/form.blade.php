

		 {!! Form::open(array('url'=>'clscmntrepo/savepublic', 'class'=>'form-horizontal','files' => true , 'parsley-validate'=>'','novalidate'=>' ')) !!}

	@if(Session::has('messagetext'))
	  
		   {!! Session::get('messagetext') !!}
	   
	@endif
	<ul class="parsley-error-list">
		@foreach($errors->all() as $error)
			<li>{{ $error }}</li>
		@endforeach
	</ul>		


<div class="col-md-12">
						<fieldset><legend> Comments and Action Plans</legend>
				{!! Form::hidden('programas_id', $row['programas_id']) !!}
									  <div class="form-group  " >
										<label for="Contrato" class=" control-label col-md-4 text-left"> Contrato </label>
										<div class="col-md-6">
										  <select name='contrato_id' rows='5' id='contrato_id' class='select2 '   ></select>
										 </div>
										 <div class="col-md-2">
										 	
										 </div>
									  </div> 
									  <div class="form-group  " >
										<label for="Ámbito" class=" control-label col-md-4 text-left"> Ámbito </label>
										<div class="col-md-6">
										  <select name='ambito_id' rows='5' id='ambito_id' class='select2 '   ></select>
										 </div>
										 <div class="col-md-2">
										 	
										 </div>
									  </div> 
									  <div class="form-group  " >
										<label for="Subámbito" class=" control-label col-md-4 text-left"> Subámbito </label>
										<div class="col-md-6">
										  <select name='subambito_id' rows='5' id='subambito_id' class='select2 '   ></select>
										 </div>
										 <div class="col-md-2">
										 	
										 </div>
									  </div> 
									  <div class="form-group  " >
										<label for="Tipo" class=" control-label col-md-4 text-left"> Tipo </label>
										<div class="col-md-6">
										  
					<?php $programas_idTipo = explode(',',$row['programas_idTipo']);
					$programas_idTipo_opt = array( '1' => 'Comentarios' ,  '2' => 'Planes de Acción' , ); ?>
					<select name='programas_idTipo' rows='5'   class='select2 '  > 
						<?php
						foreach($programas_idTipo_opt as $key=>$val)
						{
							echo "<option  value ='$key' ".($row['programas_idTipo'] == $key ? " selected='selected' " : '' ).">$val</option>";
						}
						?></select>
										 </div>
										 <div class="col-md-2">
										 	
										 </div>
									  </div> 
									  <div class="form-group  " >
										<label for="Descripción" class=" control-label col-md-4 text-left"> Descripción </label>
										<div class="col-md-6">
										  <textarea name='programas_idDescripcion' rows='5' id='programas_idDescripcion' class='form-control '
				           >{{ $row['programas_idDescripcion'] }}</textarea>
										 </div>
										 <div class="col-md-2">
										 	
										 </div>
									  </div> 
									  <div class="form-group  " >
										<label for="Fecha Inicio" class=" control-label col-md-4 text-left"> Fecha Inicio </label>
										<div class="col-md-6">
										  
				<div class="input-group m-b" style="width:150px !important;">
					{!! Form::text('programas_idFechaInicio', $row['programas_idFechaInicio'],array('class'=>'form-control date')) !!}
					<span class="input-group-addon"><i class="fa fa-calendar"></i></span>
				</div>
										 </div>
										 <div class="col-md-2">
										 	
										 </div>
									  </div> 
									  <div class="form-group  " >
										<label for="Fecha Fin" class=" control-label col-md-4 text-left"> Fecha Fin </label>
										<div class="col-md-6">
										  
				<div class="input-group m-b" style="width:150px !important;">
					{!! Form::text('programas_idFechaFin', $row['programas_idFechaFin'],array('class'=>'form-control date')) !!}
					<span class="input-group-addon"><i class="fa fa-calendar"></i></span>
				</div>
										 </div>
										 <div class="col-md-2">
										 	
										 </div>
									  </div> </fieldset>
			</div>

			

			<div style="clear:both"></div>	
				
					
				  <div class="form-group">
					<label class="col-sm-4 text-right">&nbsp;</label>
					<div class="col-sm-8">	
					<button type="submit" name="apply" class="btn btn-info btn-sm" ><i class="fa  fa-check-circle"></i> {{ Lang::get('core.sb_apply') }}</button>
					<button type="submit" name="submit" class="btn btn-primary btn-sm" ><i class="fa  fa-save "></i> {{ Lang::get('core.sb_save') }}</button>
				  </div>	  
			
		</div> 
		 
		 {!! Form::close() !!}
		 
   <script type="text/javascript">
	$(document).ready(function() { 
		
		
		$("#contrato_id").jCombo("{!! url('clscmntrepo/comboselect?filter=tbl_contrato:contrato_id:cont_numero|cont_proveedor') !!}",
		{  selected_value : '{{ $row["contrato_id"] }}' });
		
		$("#ambito_id").jCombo("{!! url('clscmntrepo/comboselect?filter=tbl_planes_y_programas_ambito:ambito_id:ambito_nombre') !!}",
		{  selected_value : '{{ $row["ambito_id"] }}' });
		
		$("#subambito_id").jCombo("{!! url('clscmntrepo/comboselect?filter=tbl_planes_y_programas_ambitosub:subambito_id:ambito_nombre') !!}&parent=ambito_id:",
		{  parent: '#ambito_id', selected_value : '{{ $row["subambito_id"] }}' });
		 

		$('.removeCurrentFiles').on('click',function(){
			var removeUrl = $(this).attr('href');
			$.get(removeUrl,function(response){});
			$(this).parent('div').empty();	
			return false;
		});		
		
	});
	</script>		 
