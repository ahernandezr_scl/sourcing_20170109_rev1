@extends('layouts.app')
@section('content')

<?php
$servername = "localhost";
$username = "root";
$password = "8044302";

#try {
    $conn2 = new PDO("mysql:host=$servername;dbname=andina_koadmin", $username, $password);
    // set the PDO error mode to exception
#    $conn2->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
#   echo "Connected successfully";
#    }
#catch(PDOException $e)
#    {
#    echo "Connection failed: " . $e->getMessage();
#    }
?>

<style type="text/css">
.circle_green
    {
  display: table-cell;
  vertical-align: middle;
  background: #3d9b35;
  font-size: 10px;
  color: white;

    }

.circle_red
{
  display: table-cell;
  vertical-align: middle;
  background: #e64427;
  font-size: 10px;
  color: white;
}

.circle_yellow
{
  display: table-cell;
  vertical-align: middle;
  background: #ffff00;
  font-size: 10px;
  color: black;
}

.circle_grey
{
  display: table-cell;
  vertical-align: middle;
  background: #cec6c0;
  font-size: 10px;
  color: white;
}
.circle {
  position: relative;
  display: inline-block;
  width: 100%;
  height: 0;
  padding: 50% 0;
  border-radius: 50%;
  margin: 5px;

  /* Just making it pretty */
  @shadow: rgba(0, 0, 0, .1);
  @shadow-length: 4px;
  -webkit-box-shadow: 0 @shadow-length 0 0 @shadow;
          box-shadow: 0 @shadow-length 0 0 @shadow;
  text-shadow: 0 @shadow-length 0 @shadow;
  text-align: center;
  line-height: 0px
}
.circle-container{
  display: inline-block;
}

        #embed-container {
                position: relative;
                padding-bottom: 56.25%;
                height: 0;
                overflow: hidden;
        }
        #embed-container iframe {
                position: absolute;
                top: 0;
                left: 0;
                width: 100%;
                height: 100%;
        }
        .sbox-content{
          padding-bottom: 10px;
          border-radius: 5px;
        }
        .table{
          margin-bottom: 10px;
        }
        #page-wrapper{
          background-color: gray;
        }
        .m-t{
          margin-bottom: -15px;
        }
        .inner-row{
          margin-left: -15px;
          margin-right: -15px;
        }
        .page-content{
          margin-left: 0px;
          margin-right: 0px;
        }
		.redireccion{
          background-color: #d7dee5;
          display: inline-block;;
          border-radius: 5px;
          padding-left: 5px;
          padding-right: 5px;
        }

</style>

@if(Auth::check() && Auth::user()->group_id == 1)

    <script src="https://code.highcharts.com/highcharts.js"></script>
    <script src="https://code.highcharts.com/modules/exporting.js"></script>
    <script src="http://code.highcharts.com/highcharts-more.js"></script>

<div class="page-content row" style="background-color:gray">

  <div class="row m-t">

    <div class="col-lg-1">
    </div>

    <div class="col-lg-3">
      <div class="sbox">
        <div class="pull-center">
          <div class="sbox-content" align="center">
            <h4>Empresas Contratistas</h4>
			  <a href="{{ action('DashboardcontratistasController@getIndex')}}">
			  <div class="redireccion">
			    <h1>12</h1>
			  </div>
			  </a>
          </div>
        </div>
      </div>
    </div>

    <div class="col-lg-4">
      <div class="sbox">
        <div class="pull-center">
          <div class="sbox-content" align="center">
            <h4>Contratos Vigentes</h4>
			  <a href="{{ action('DashboardcontratosController@getIndex')}}">
			  <div class="redireccion">
                <h1>10</h1>
			  </div>
			  </a>
          </div>
        </div>
      </div>
    </div>

    <div class="col-lg-3">
      <div class="sbox">
        <div class="pull-center">
          <div class="sbox-content" align="center">
            <h4>Monto Contratos Vigentes</h4>
			  <a href="{{ action('DashboardcontratosController@getIndex')}}">
			  <div class="redireccion">
			    <h1>MM$255,71</h1>
			  </div>
			  </a>
          </div>
        </div>
      </div>
    </div>

    <div class="col-lg-1">
    </div>

  </div>


  <div class="row m-t">

    <div class="col-lg-3">
      <div class="sbox">
        <div class="sbox-content" style="min-height: 348px;">
          <a href="{{ action('ReportglobalnegociofisicofinancieroController@getIndex')}}"><h4>Avance Financiero</br>&nbsp</h4></a>
          <div style=" width: 100%;margin: 10px 0 ; overflow-y: auto;">
            <table style='width: 100%;'>
              <tr>
                <td align="center">
                  <a href="{{ action('ReportdetavafisController@getIndex')}}">
                    <div class="circle-container">
                      <div class="circle_yellow circle" style="font-size: 1.3em; font-weight: bold; display: inline-block;" align="center">-7,3%</div>
                    </div>
                  </a>
                </td>
                <td><div style="padding-left: 5px; font-weight: bold; font-size: 0.90em;">($5,6MM real / $6MM Ppto)</div></td>
              </tr>
              <tr>
                <td colspan="2">&nbsp</td>
              </tr>
            </table>
          </div>
          <div id="tabla1" style=" width: 100%;margin: 10px 0 ; overflow-y: auto;" >
            <table style='border: solid 0px;' class='table'>
              <tr align="center" style="font-weight: bold; background-color: #ecf0f2;">
                <td>N° Cttos</td>
                <td>Término</td>
                <td>Estado</th>
              </tr>
              <tr align="center">
                <td style="vertical-align: middle;">3</td>
                <td style="vertical-align: middle;">3 meses</td>
                <td style="vertical-align: middle;">
                  <div class="circle-container">
                    <div class="circle_red circle" style="font-size: 1.3em;">30%</div>
                  </div>
                </td>
              </tr>
              <tr align="center">
                <td style="vertical-align: middle;">1</td>
                <td style="vertical-align: middle;">6 meses</td>
                <td style="vertical-align: middle;">
                  <div class="circle-container">
                    <div class="circle_yellow circle" style="font-size: 1.3em;">10%</div>
                  </div>
                </td>
              </tr>
              <tr align="center">
                <td style="vertical-align: middle;">6</td>
                <td style="vertical-align: middle;">+12 meses</td>
                <td style="vertical-align: middle;">
                  <div class="circle-container">
                    <div class="circle_green circle" style="font-size: 1.3em;">60%</div>
                  <div class="circle-container">
                </td>
              </tr>
            </table>
          </div>
          </div>
        </div><!-- /sbox -->
      </div><!-- </div class="col-lg-4"> -->

      <div class="col-lg-3">

        <div class="row inner-row">

          <div class="col-lg-12">
            <div class="sbox">
              <div class="sbox-content">
                <div style="top: 10px;  left: 0;   width: 100%;" height="300px;background: white;">
                  <a href="{{ action('ReportglobalnegociocumplimientokpiController@getIndex')}}">
                    <h4>KPI Contratos</h4>
                  </a>
                  <div id="tabla2" style=" width: 100%;margin: 10px 0 ; overflow-y: auto;" >
                    <div style=" width: 100%;margin: 10px 0 ; overflow-y: auto;">
                      <table style='width: 100%;'>
                        <tr>
                          <td align="center">
                            <a href="{{ action('ReportglobalnegociocumplimientokpiController@getIndex')}}">
                              <div class="circle-container">
                                <div class="circle_green circle" style="font-size: 1.3em; font-weight: bold; display: inline-block;" align="center">77,3%</div>
                              </div>
                            </a>
                          </td>
                        </tr>
                        <tr>
                          <td align="center"><75%: 5 Cttos <b>(50%)</b></td>
                        </tr>
                      </table>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>

        </div>

        <div class="row inner-row">

          <div class="col-lg-12">
            <div class="sbox">
              <div class="sbox-content">
                <a href="{{ action('ReportambseguridadController@getIndex')}}">
                  <h4>Resultados Accidentabilidad</h4>
                </a>
                <div id="tabla8" style=" width: 100%;margin: 10px 0 ; overflow-y: auto;" >
                  <table style='border: solid 0px;' class='table'>
                    <tr style="font-weight: bold; background-color: #ecf0f2;">
                      <td>Indice</td>
                      <td align="center">Real</td>
                      <td align="center">Ppto</td>
                    </tr>
                    <tr>
                      <td style="font-weight: bold; vertical-align: middle;">Frecuencia</td>
                      <td align="center" style="vertical-align: middle;">
                        <a href="{{ action('ReportglobalseguridadycondicionesaccidentabilidadController@getIndex')}}">
                          <div class="circle-container">
                            <div class="circle_green circle">0,29</div>
                          </div>
                        </a>
                      </td>
                      <td align="center" style="vertical-align: middle;">1,5</td>
                    </tr>
                    <tr>
                      <td style="font-weight: bold; vertical-align: middle;">Gravedad</td>
                      <td align="center" style="vertical-align: middle;">
                        <a href="{{ action('ReportglobalseguridadycondicionesgravedadController@getIndex')}}">
                          <div class="circle-container">
                            <div class="circle_green circle">0,29</div>
                          </div>
                        </a>
                      </td>
                      <td align="center" style="vertical-align: middle;">20</td>
                    </tr>
                  </table>
                </div>
              </div>
            </div><!-- /sbox -->
          </div>

        </div>

      </div>

      <div class="col-lg-3">
        <div class="sbox">
          <div class="sbox-content" style="min-height: 348px;">
            <h4>Personas</br>&nbsp</h4>
            <div id="tabla4" style=" width: 100%;margin: 10px 0 ; overflow-y: auto;">
              <table style='width: 100%; text-align: center;'>
                <tr>
                  <td>Dotación</td>
                  <td>Remun.</td>
                  <td>Obligaciones</br>Laborales</td>
                </tr>
                <tr>
                  <td><a href="{{ action('DashboardpersonasController@getIndex')}}">
                    <div class="circle-container">
                      <div class="circle_green circle" style="font-size: 1.3em;font-weight: bold; display: inline-block;" align="center">8%</div>
                    </div>
                  </a>
                  </td>
                  <td>
                    <a href="{{ action('DashboardpersonasController@getIndex')}}">
                      <div class="circle-container">
                        <div class="circle_red circle" style="font-size: 1.3em;font-weight: bold; display: inline-block;" align="center">3%</div>
                      </div>
                    </a>
                  </td>
                  <td>
                    <a href="{{ action('ReportgloballaboralobligacionesnocubiertasController@getIndex')}}">
                      <div class="circle-container">
                        <div class="circle_green circle" style="font-size: 1.3em;font-weight: bold; display: inline-block;" align="center">3%</div>
                      </div>
                    </a>
                  </td>
                </tr>
                <tr>
                  <td colspan="3">&nbsp</td>
                </tr>
              </table>
              <table style='border: solid 0px;' class='table'>
                <tr align="center" style="font-weight: bold; background-color: #ecf0f2;">
                  <td></td>
                  <td>Real</td>
                  <td>Ppto</td>
                </tr>
                <tr>
                  <td style="font-weight: bold;">Dotación</td>
                  <td align="center" style="vertical-align: middle;">117</td>
                  <td align="center" style="vertical-align: middle;">120</td>
                </tr>
                <tr style="vertical-align: middle;">
                  <td style="font-weight: bold;">Remun.</td>
                  <td align="center" style="vertical-align: middle;">$936M</td>
                  <td align="center" style="vertical-align: middle;">$960M</td>
                </tr>
                <tr style="vertical-align: middle;">
                  <td style="font-weight: bold;">Obligaciones<br>Laborales</td>
                  <td align="center" style="vertical-align: middle;">4,4%</td>
                  <td align="center" style="vertical-align: middle;">0%</td>
                </tr>
              </table>
              &nbsp</br>&nbsp</br>&nbsp</br>&nbsp</br>&nbsp</br>
            </div>
          </div>
        </div>

    </div>

    <div class="col-lg-3">
      <div class="row inner-row">
        <div class="col-lg-12">
          <div class="sbox">
            <div class="sbox-content">
              <div style="top: 10px;  left: 0;   width: 100%;" height="300px;background: white;">
                <a href="{{ action('ReportglobalnegocioevalproveedorController@getIndex')}}">
                  <h4>Evaluación Proveedor</h4>
                </a>
                <div id="tabla3" style=" width: 100%;margin: 10px 0 ; overflow-y: auto;" >
                  <div style=" width: 100%;margin: 10px 0 ; overflow-y: auto;">
                    <table style='width: 100%;'>
                      <tr>
                        <td align="center">
                          <a href="{{ action('ReportglobalnegocioevalproveedorController@getIndex')}}">
                            <div class="circle-container">
                              <div class="circle_green circle" style="font-size: 1.3em; font-weight: bold; display: inline-block;" align="center">71,1%</div>
                            </div>
                          </a>
                        </td>
                      </tr>
                      <tr>
                        <td align="center"><70%: 4 Cttas <b>(40%)</b></td>
                      </tr>
                    </table>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
      <div class="row inner-row">
        <div class="col-lg-12">
          <div class="sbox">
            <div class="sbox-content">
              <a href="{{ action('ReportambfinancieroController@getIndex')}}">
                <h4>Empresas Riesgo Financiero</h4>
              </a>
              <div id="tabla5" style=" width: 100%;margin: 10px 0 ; overflow-y: auto;" >
                <table style='border: solid 0px;' class='table'>
                  <tr align="center" style="font-weight: bold; background-color: #ecf0f2;">
                    <td>Empresas</td>
                    <td>Porcentaje</td>
                    <td>Riesgo</td>
                  </tr>
                  <tr align="center">
                    <td style="vertical-align: middle;">3</td>
                    <td style="vertical-align: middle;">30%</td>
                    <td style="vertical-align: middle;">
                      <a href="{{ action('ReportglobalfinancieroController@getIndex')}}">
                        <div class="circle-container">
                          <div class="circle_red circle">Alto</i></div>
                        </div>
                      </a>
                    </td>
                  </tr>
                  <tr align="center">
                    <td style="vertical-align: middle;">4</td>
                    <td style="vertical-align: middle;">40%</td>
                    <td style="vertical-align: middle;">
                      <a href="{{ action('ReportglobalfinancieroController@getIndex')}}">
                        <div class="circle-container">
                          <div class="circle_yellow circle">Med</i></div>
                        </div>
                      </a>
                    </td>
                  </tr>
                </table>
              </div>
            </div>
          </div>
        </div><!-- /sbox -->
      </div>
    </div><!-- </div class="row-mt"> -->
  </div>
</div>


    @endif
   @if(Auth::check() && Auth::user()->group_id == 6)
        <!-- contratistas / 6-->
    
          
    <script src="https://code.highcharts.com/highcharts.js"></script>
    <script src="https://code.highcharts.com/modules/exporting.js"></script>
                                <tr>
                                    <td> <!--{{ Auth::user()->id }}--></td>
                                    <!--{{ Auth::user()->id }}-->
                              </tr>
        <div class="row m-t">  
                    <div class="col-lg-9">
 <div class="col-lg-6">
                                <div class="sbox">

                                    <div class="sbox-title">
                                        <span class="label label-success pull-right">Mes anterior</span>
                                        <h4>Ultima Facturación</h4>
                                    </div>
                                    <div class="sbox-content">
                                    <table class='table table-striped'> 
                                            <tr style='font-weight:bold;'><td>Tipo</td><td align="center"> Financiero</td> </tr>
                                            <tr ><td>Consumo</td>
                                                <td align="right"><a href="{{ URL::to('') }}">
                                               <?php
                                                $sql = 'select 
                                                            YEAR(b.financieroand_fecha) AS AGNO
                                                        ,   MONTH(b.financieroand_fecha) AS MES
                                                        ,   REPLACE(REPLACE(REPLACE(FORMAT(SUM(b.financieroand_mtoreal), 0), ".", "@"), ",", "."), "@", ",") as MtoPago from tbl_contrato as a 
                                                        inner join tbl_financiero_and as b on a.contrato_id = b.contrato_id
                                                        where 
                                                            b.financieroand_fecha = CASE WHEN DAY(CURRENT_DATE) >= 20 THEN DATE_ADD(DATE_ADD(CURRENT_DATE, INTERVAL -1 MONTH), INTERVAL -(DAY(CURRENT_DATE)-1) DAY)  ELSE DATE_ADD(DATE_ADD(CURRENT_DATE, INTERVAL -2 MONTH), INTERVAL -(DAY(CURRENT_DATE)-1) DAY) END 
                                                         and a.entry_by_access = '.Auth::user()->id;
                                                foreach ($conn2->query($sql) as $row) {
                                                print "$".$row['MtoPago'] . "\n";
                                                }
                                            ?></a>
                                            </td>
                                           
                                        <tr><td>Presupuesto </td>
                                            <td align="right"><a href="{{ URL::to('') }}"> <?php
                                                $sql = 'select 
                                                            YEAR(b.financieroand_fecha) AS AGNO
                                                        ,   MONTH(b.financieroand_fecha) AS MES
                                                        ,   REPLACE(REPLACE(REPLACE(FORMAT(SUM(b.financieroand_mtoplan), 0), ".", "@"), ",", "."), "@", ",") as MtoPago from tbl_contrato as a 
                                                        inner join tbl_financiero_and as b on a.contrato_id = b.contrato_id
                                                        where 
                                                            b.financieroand_fecha = CASE WHEN DAY(CURRENT_DATE) >= 20 THEN DATE_ADD(DATE_ADD(CURRENT_DATE, INTERVAL -1 MONTH), INTERVAL -(DAY(CURRENT_DATE)-1) DAY)  ELSE DATE_ADD(DATE_ADD(CURRENT_DATE, INTERVAL -2 MONTH), INTERVAL -(DAY(CURRENT_DATE)-1) DAY) END 
                                                         and a.entry_by_access = '.Auth::user()->id;
                                                foreach ($conn2->query($sql) as $row) {
                                                print "$".$row['MtoPago'] . "\n";
                                                }
                                            ?></a>
                                        </td>
                                                                               <tr>
                                        </table>
                                        <div class="stat-percent font-bold text-success">
                                        <?php
                                                $sql = 'SELECT Anio, NMes3L,
                                                            YEAR(b.financieroand_fecha) AS AGNO
                                                        ,   MONTH(b.financieroand_fecha) AS MES
                                                        ,   REPLACE(REPLACE(REPLACE(FORMAT(SUM(b.financieroand_mtoreal), 0), ".", "@"), ",", "."), "@", ",") AS MtoPago FROM tbl_contrato AS a 
                                                        INNER JOIN tbl_financiero_and b ON a.contrato_id = b.contrato_id
                                                        INNER JOIN dim_tiempo t ON b.financieroand_fecha = t.fecha
                                                        WHERE 
                                                            b.financieroand_fecha = CASE WHEN DAY(CURRENT_DATE) >= 20 THEN DATE_ADD(DATE_ADD(CURRENT_DATE, INTERVAL -1 MONTH), INTERVAL -(DAY(CURRENT_DATE)-1) DAY)  ELSE DATE_ADD(DATE_ADD(CURRENT_DATE, INTERVAL -2 MONTH), INTERVAL -(DAY(CURRENT_DATE)-1) DAY) END
                                                           
                                                         ';
                                            foreach ($conn2->query($sql) as $row) {
                                                print $row['NMes3L']."-".$row['Anio']."\n";
                                                }
                                        ?>
                                        <!--<i class="fa fa-level-up"></i>--></div>
                                        <small>PERIODO</small>
                                    </div>

                                </div><!-- /sbox -->
                            </div><!-- </div class="col-lg-4"> -->
                         <div class="row">
                            <div class="col-lg-5"> 
                                <div class="sbox">

                                    <div class="sbox-title">
                                        <span class="label label-warning pull-right">Mes anterior</span>
                                        <h4>Dotación (personas)</h4>
                                    </div> 
                                    
                                    <div class="sbox-content">
                                        <table class='table table-striped'> 
                                            <tr align="center"><th>Mes Actual</th><th> Mes Anterior</th> <th>Posibles Vinculación</th><th>Posible desvinculación</th></tr>
                                            <tr align="right"><td>
                                            <?php
                                                $sqlp = 'SELECT COUNT(DISTINCT p.rut) personas
                                                            FROM 
                                                            tbl_personas p 
                                                            INNER JOIN tbl_contratos_personas cp
                                                            ON p.IdPersona = cp.IdPersona
                                                            INNER JOIN tbl_contratistas c ON c.IdContratista = cp.IdContratista
                                                            
                                                            WHERE 
                                                             c.entry_by_access='.Auth::user()->id.' ';
                                                            
                                                foreach ($conn2->query($sqlp) as $row) {
                                                print $row['personas'] . "\n";
                                                }
                                            ?>
                                        </td>
                                        <td>
                                          <?php
                                                $sqlp = 'SELECT COUNT(DISTINCT fe.rut) personas

                                                            FROM 
                                                            tbl_f30_1_empleados fe INNER JOIN tbl_f30_1 f ON fe.IdF301 = f.IdF301
                                                            INNER JOIN tbl_personas p ON p.IdPersona = fe.IdPersona
                                                            INNER JOIN tbl_contratistas c ON c.IdContratista = f.IdContratista
                                                            WHERE 
                                                            Periodo =  CASE WHEN DAY(CURRENT_DATE) >= 20 THEN DATE_ADD(DATE_ADD(CURRENT_DATE, INTERVAL -1 MONTH), INTERVAL -(DAY(CURRENT_DATE)-1) DAY)  ELSE DATE_ADD(DATE_ADD(CURRENT_DATE, INTERVAL -2 MONTH), INTERVAL -(DAY(CURRENT_DATE)-1) DAY) END
                                                            AND c.entry_by_access='.Auth::user()->id.' ';
                                                            
                                                            
                                                foreach ($conn2->query($sqlp) as $row) {
                                                print $row['personas'] . "\n";
                                                }
                                            ?>
                                        </td>
                                        <td>
                                        <?php
                                                $sqlp = 'SELECT COUNT(DISTINCT fe.rut) personas
                                                            FROM 
                                                            tbl_f30_1_empleados fe 
                                                            INNER JOIN tbl_f30_1 f ON fe.IdF301 = f.IdF301
                                                            INNER JOIN tbl_contratistas c ON c.IdContratista = f.IdContratista
                                                            WHERE 
                                                            IdPersona IS NULL
                                                            AND 
                                                            Periodo =  CASE WHEN DAY(CURRENT_DATE) >= 20 THEN DATE_ADD(DATE_ADD(CURRENT_DATE, INTERVAL -1 MONTH), INTERVAL -(DAY(CURRENT_DATE)-1) DAY)  ELSE DATE_ADD(DATE_ADD(CURRENT_DATE, INTERVAL -2 MONTH), INTERVAL -(DAY(CURRENT_DATE)-1) DAY) END

                                                            AND c.entry_by_access= '.Auth::user()->id.' ';
                                                                                                                        
                                                foreach ($conn2->query($sqlp) as $row) {
                                                print $row['personas'] . "\n";
                                                }
                                            ?>
                                        </td>
                                        <td>
                                        <?php
                                                $sqlp = 'SELECT CASE WHEN (pm-pma) >= 0 THEN pm-pma ELSE 0 END pers
                                                            FROM
                                                            (
                                                            SELECT SUM(personas_mes) pm, SUM(personas_mes_ant) pma
                                                            
                                                            FROM
                                                            (
                                                            SELECT 0 personas_mes,COUNT(DISTINCT fe.rut) personas_mes_ant
                                                            
                                                            FROM 
                                                            tbl_f30_1_empleados fe INNER JOIN tbl_f30_1 f ON fe.IdF301 = f.IdF301
                                                            INNER JOIN tbl_personas p ON p.IdPersona = fe.IdPersona
                                                            INNER JOIN tbl_contratistas c ON c.IdContratista = f.IdContratista
                                                            WHERE 
                                                            
                                                            Periodo =  CASE WHEN DAY(CURRENT_DATE) >= 20 THEN DATE_ADD(DATE_ADD(CURRENT_DATE, INTERVAL -1 MONTH), INTERVAL -(DAY(CURRENT_DATE)-1) DAY)  ELSE DATE_ADD(DATE_ADD(CURRENT_DATE, INTERVAL -2 MONTH), INTERVAL -(DAY(CURRENT_DATE)-1) DAY) END
                                                            AND c.entry_by_access= '.Auth::user()->id.' 
                                                            
                                                            UNION ALL
                                                            
                                                            SELECT 0,COUNT(DISTINCT fe.rut) personas_mes_ant
                                                            
                                                            FROM 
                                                            tbl_f30_1_empleados fe INNER JOIN tbl_f30_1 f ON fe.IdF301 = f.IdF301
                                                            INNER JOIN tbl_personas p ON p.IdPersona = fe.IdPersona
                                                            INNER JOIN tbl_contratistas c ON c.IdContratista = f.IdContratista
                                                            WHERE 
                                                            
                                                            Periodo =  CASE WHEN DAY(CURRENT_DATE) >= 20 THEN DATE_ADD(DATE_ADD(CURRENT_DATE, INTERVAL -1 MONTH), INTERVAL -(DAY(CURRENT_DATE)-1) DAY)  ELSE DATE_ADD(DATE_ADD(CURRENT_DATE, INTERVAL -2 MONTH), INTERVAL -(DAY(CURRENT_DATE)-1) DAY) END
                                                            AND c.entry_by_access= '.Auth::user()->id.' 
                                                            ) c ) c
                                                            ';
                                                                                                                        
                                                foreach ($conn2->query($sqlp) as $row) {
                                                print $row['pers'] . "\n";
                                                }
                                            ?>
                                        </td>
                                        </tr>
                                        </table>
                                        
                                        
                                        
                                        
                                        <div class="stat-percent font-bold text-warning">
                                           <?php
                                                $sql = 'SELECT Anio, NMes3L,
                                                            YEAR(b.financieroand_fecha) AS AGNO
                                                        ,   MONTH(b.financieroand_fecha) AS MES
                                                        ,   REPLACE(REPLACE(REPLACE(FORMAT(SUM(b.financieroand_mtoreal), 0), ".", "@"), ",", "."), "@", ",") AS MtoPago 
                                                        FROM tbl_contrato AS a 
                                                        INNER JOIN tbl_financiero_and b ON a.contrato_id = b.contrato_id
                                                        INNER JOIN dim_tiempo t ON b.financieroand_fecha = t.fecha
                                                        inner join tbl_contratistas c on a.IdContratista = c.IdContratista
                                                        WHERE 
                                                            b.financieroand_fecha = CASE WHEN DAY(CURRENT_DATE) >= 20 THEN DATE_ADD(DATE_ADD(CURRENT_DATE, INTERVAL -1 MONTH), INTERVAL -(DAY(CURRENT_DATE)-1) DAY)  ELSE DATE_ADD(DATE_ADD(CURRENT_DATE, INTERVAL -2 MONTH), INTERVAL -(DAY(CURRENT_DATE)-1) DAY) END
                                                            AND c.entry_by_access= '.Auth::user()->id.'
                                                         ';
                                            foreach ($conn2->query($sql) as $row) {
                                                print $row['NMes3L']."-".$row['Anio']."\n";
                                                }
                                        ?>
                                        </div>
                                        <small>PERIODO</small>
                                    </div>

                                </div>
                            </div>
						</div>
							 <div class="row m-t">
                           <div class="col-lg-7">
                                <div class="sbox">                          
                                    <div class="sbox-content">
                                        <div style="top: 10px;  left: 0;   width: 100%;" height="600px;background: white;"> 
                                        
                                        <h4><center>Visión General <?php print $row['NMes3L']."-".$row['Anio'] ?></h4>
                                            <div height="300px" id="container" style=" width: 445px;margin-left: auto; margin-right: auto; height: 300px; background: white;" >
                                                <p style="padding: 80px 0; font-size: 20px; text-align: center;">
                                               
                                                </p>

                                            </div>
                                            <div class="stat-percent font-bold text-info">
                                           <?php
                                                $sql = 'SELECT Anio, NMes3L,
                                                            YEAR(b.financieroand_fecha) AS AGNO
                                                        ,   MONTH(b.financieroand_fecha) AS MES
                                                        ,   REPLACE(REPLACE(REPLACE(FORMAT(SUM(b.financieroand_mtoreal), 0), ".", "@"), ",", "."), "@", ",") AS MtoPago FROM tbl_contrato AS a 
                                                        INNER JOIN tbl_financiero_and b ON a.contrato_id = b.contrato_id
                                                        INNER JOIN dim_tiempo t ON b.financieroand_fecha = t.fecha
                                                        WHERE 
                                                            b.financieroand_fecha = CASE WHEN DAY(CURRENT_DATE) >= 20 THEN DATE_ADD(DATE_ADD(CURRENT_DATE, INTERVAL -1 MONTH), INTERVAL -(DAY(CURRENT_DATE)-1) DAY)  ELSE DATE_ADD(DATE_ADD(CURRENT_DATE, INTERVAL -2 MONTH), INTERVAL -(DAY(CURRENT_DATE)-1) DAY) END
                                                           
                                                         ';
                                            foreach ($conn2->query($sql) as $row) {
                                                print $row['NMes3L']."-".$row['Anio']."\n";
                                                }
                                        ?>
                                        <!--<i class="fa fa-level-up"></i>--></div>
                                        <small>PERIODO</small>
                                        </div>
                                    </div><!-- /sbox-content -->
                                </div> <!-- /sbox -->
                            </div> <!--</div class="col-lg-12"> -->
                        
<div class="col-lg-5">
                                <div class="sbox">                          
                                    <div class="sbox-content">
                                    <?php $sqltb = 'SELECT  c.contrato_id,fecha,cont_numero,cont_proveedor,
YEAR(fecha) anio,
MONTH(fecha) mes,
CAST(SUM(n) AS UNSIGNED) Negocio,
CAST(SUM(f) AS UNSIGNED) Financiero ,
CAST(SUM(l) AS UNSIGNED) Laboral, 
CAST(SUM(s) AS UNSIGNED) seguridad,  
CAST(AVG(n+f+l+s) AS UNSIGNED) general
FROM
(
SELECT contrato_id,fecha,CAST(COALESCE(AVG(prom_neg),0) AS UNSIGNED) n,0 f,0 l ,0 s
FROM
(
SELECT contrato_id,kpiDet_fecha fecha,CAST(AVG(kpi) AS DECIMAL(4,1)) prom_neg

FROM
(SELECT  b.contrato_id,kpiDet_fecha,  (SUM(CASE WHEN kpi_tipo_calc = 1 THEN CASE WHEN kpiDet_puntaje >= kpiDet_meta THEN 1 ELSE 0 END WHEN kpi_tipo_calc = 2 THEN CASE WHEN kpiDet_puntaje <= kpiDet_meta THEN 1 ELSE 0 END WHEN kpi_tipo_calc = 3 THEN CASE WHEN kpiDet_puntaje BETWEEN kpiDet_min AND kpiDet_max THEN 1 ELSE 0 END WHEN kpi_tipo_calc = 4 THEN 1 END )/
COUNT(DISTINCT a.id_kpi) )*100 kpi
FROM tbl_kpimensual a INNER JOIN tbl_kpigral b
ON a.id_kpi = b.id_kpi
INNER JOIN dim_tiempo ON kpiDet_fecha = fecha
INNER JOIN tbl_contrato c ON c.contrato_id = b.contrato_id
WHERE kpiDet_puntaje IS NOT NULL
AND 
fecha_mes =  CASE WHEN DAY(CURRENT_DATE) >= 20 THEN DATE_ADD(DATE_ADD(CURRENT_DATE, INTERVAL -1 MONTH), INTERVAL -(DAY(CURRENT_DATE)-1) DAY)  ELSE DATE_ADD(DATE_ADD(CURRENT_DATE, INTERVAL -2 MONTH), INTERVAL -(DAY(CURRENT_DATE)-1) DAY) END
GROUP BY 1,2) asd
group by 1,2

UNION ALL

SELECT a.contrato_id,CASE WHEN DAY(CURRENT_DATE) >= 20 THEN DATE_ADD(DATE_ADD(CURRENT_DATE, INTERVAL -1 MONTH), INTERVAL -(DAY(CURRENT_DATE)-1) DAY)  ELSE DATE_ADD(DATE_ADD(CURRENT_DATE, INTERVAL -2 MONTH), INTERVAL -(DAY(CURRENT_DATE)-1) DAY) END,
CASE WHEN AVG(eval_puntaje) >70 THEN 75 + ((CASE WHEN AVG(eval_puntaje) >70 THEN 25/30 ELSE 75/70 END)*(AVG(eval_puntaje) - 70)) 
ELSE 75 - ((CASE WHEN AVG(eval_puntaje) >70 THEN 25/30 ELSE 75/70 END)*(AVG(eval_puntaje) - 70)) END f
FROM tbl_evalcontratistas a INNER JOIN dim_tiempo 
ON eval_fecha = fecha 
INNER JOIN tbl_contrato b ON a.contrato_id = b.contrato_id
WHERE  
eval_puntaje IS NOT NULL
AND 
fecha_mes IN (SELECT DISTINCT fecha_mes FROM dim_tiempo WHERE semestre  = (SELECT DISTINCT semestre FROM dim_tiempo WHERE fecha_mes =  CASE WHEN DAY(CURRENT_DATE) >= 20 THEN DATE_ADD(DATE_ADD(CURRENT_DATE, INTERVAL -1 MONTH), INTERVAL -(DAY(CURRENT_DATE)-1) DAY)  ELSE DATE_ADD(DATE_ADD(CURRENT_DATE, INTERVAL -2 MONTH), INTERVAL -(DAY(CURRENT_DATE)-1) DAY) END) AND Anio =YEAR(CASE WHEN DAY(CURRENT_DATE) >= 20 THEN DATE_ADD(DATE_ADD(CURRENT_DATE, INTERVAL -1 MONTH), INTERVAL -(DAY(CURRENT_DATE)-1) DAY)  ELSE DATE_ADD(DATE_ADD(CURRENT_DATE, INTERVAL -2 MONTH), INTERVAL -(DAY(CURRENT_DATE)-1) DAY) END))
group by 1,2
UNION ALL

SELECT a.contrato_id,CASE WHEN DAY(CURRENT_DATE) >= 20 THEN DATE_ADD(DATE_ADD(CURRENT_DATE, INTERVAL -1 MONTH), INTERVAL -(DAY(CURRENT_DATE)-1) DAY)  ELSE DATE_ADD(DATE_ADD(CURRENT_DATE, INTERVAL -2 MONTH), INTERVAL -(DAY(CURRENT_DATE)-1) DAY) END,CAST(AVG(resultado) AS DECIMAL(4,1)) puntaje
FROM tbl_calidad_adm_cont a INNER JOIN dim_tiempo 
ON fec_rev = fecha
INNER JOIN tbl_contrato b ON a.contrato_id = b.contrato_id
WHERE resultado IS NOT NULL
AND 
fecha_mes IN (SELECT DISTINCT fecha_mes FROM dim_tiempo WHERE trimestre  = (SELECT DISTINCT trimestre FROM dim_tiempo WHERE fecha_mes =  CASE WHEN DAY(CURRENT_DATE) >= 20 THEN DATE_ADD(DATE_ADD(CURRENT_DATE, INTERVAL -1 MONTH), INTERVAL -(DAY(CURRENT_DATE)-1) DAY)  ELSE DATE_ADD(DATE_ADD(CURRENT_DATE, INTERVAL -2 MONTH), INTERVAL -(DAY(CURRENT_DATE)-1) DAY) END) AND Anio =YEAR(CASE WHEN DAY(CURRENT_DATE) >= 20 THEN DATE_ADD(DATE_ADD(CURRENT_DATE, INTERVAL -1 MONTH), INTERVAL -(DAY(CURRENT_DATE)-1) DAY)  ELSE DATE_ADD(DATE_ADD(CURRENT_DATE, INTERVAL -2 MONTH), INTERVAL -(DAY(CURRENT_DATE)-1) DAY) END))
group by 1,2
) ambito
group by 1,2

UNION ALL 

-- financiero

SELECT contrato_id,fecha,0, CAST(COALESCE(AVG(prom_fin),0) AS UNSIGNED) f ,0,0
FROM
(SELECT a.contrato_id,fecha_validez fecha,
CASE WHEN AVG(valor) > 50 THEN 75 - (AVG(valor) -50) * (CASE WHEN AVG(valor) > 50 THEN 75/50 ELSE 25/50 END) ELSE
75 + (AVG(valor) -50) * (CASE WHEN AVG(valor) > 50 THEN 75/50 ELSE 25/50 END) END prom_fin
FROM tbl_eval_financiera a
INNER JOIN tbl_contrato c ON c.contrato_id = a.contrato_id
INNER JOIN dim_tiempo d ON fecha_validez = d.fecha
WHERE
fecha_mes =  CASE WHEN DAY(CURRENT_DATE) >= 20 THEN DATE_ADD(DATE_ADD(CURRENT_DATE, INTERVAL -1 MONTH), INTERVAL -(DAY(CURRENT_DATE)-1) DAY)  ELSE DATE_ADD(DATE_ADD(CURRENT_DATE, INTERVAL -2 MONTH), INTERVAL -(DAY(CURRENT_DATE)-1) DAY) END
group by 1,2
UNION ALL

SELECT a.contrato_id,fecha_Eval,
CASE WHEN AVG(per_moros*100) > 30 THEN 75 - ABS(AVG(per_moros*100) -30) * (CASE WHEN AVG(per_moros*100) > 30 THEN 75/67 ELSE 25/13 END) ELSE
75 + ABS(AVG(per_moros*100) -30) * (CASE WHEN AVG(per_moros*100) > 30 THEN 75/67 ELSE 25/13 END) END prom_trib
FROM tbl_morosidad a
INNER JOIN dim_tiempo d ON fecha_Eval = d.fecha
INNER JOIN tbl_contrato c ON c.contrato_id = a.contrato_id
WHERE 
fecha_mes =  CASE WHEN DAY(CURRENT_DATE) >= 20 THEN DATE_ADD(DATE_ADD(CURRENT_DATE, INTERVAL -1 MONTH), INTERVAL -(DAY(CURRENT_DATE)-1) DAY)  ELSE DATE_ADD(DATE_ADD(CURRENT_DATE, INTERVAL -2 MONTH), INTERVAL -(DAY(CURRENT_DATE)-1) DAY) END
GROUP BY 1,2
UNION ALL

SELECT a.contrato_id,tCont_fecha,(1 -( COUNT(DISTINCT (CASE WHEN tCont_eval = \'MALO\' THEN a.contrato_id ELSE 0 END))/ 
COUNT(DISTINCT a.contrato_id)) )*100
FROM
tbl_contratista_tributario a INNER JOIN tbl_contrato b ON a.contrato_id = b.contrato_id

INNER JOIN dim_tiempo d ON a.tCont_fecha = d.fecha
WHERE 
fecha_mes =  CASE WHEN DAY(CURRENT_DATE) >= 20 THEN DATE_ADD(DATE_ADD(CURRENT_DATE, INTERVAL -1 MONTH), INTERVAL -(DAY(CURRENT_DATE)-1) DAY)  ELSE DATE_ADD(DATE_ADD(CURRENT_DATE, INTERVAL -2 MONTH), INTERVAL -(DAY(CURRENT_DATE)-1) DAY) END
GROUP BY 1,2
UNION ALL

SELECT  a.contrato_id,a.fecha,AVG((total_fondos + prendas)/(mutuos + total_impacto))*100
FROM tbl_garantias a INNER JOIN dim_tiempo b
ON a.fecha = b.fecha
INNER JOIN tbl_contrato c ON c.contrato_id = a.contrato_id
WHERE  
fecha_mes =  CASE WHEN DAY(CURRENT_DATE) >= 20 THEN DATE_ADD(DATE_ADD(CURRENT_DATE, INTERVAL -1 MONTH), INTERVAL -(DAY(CURRENT_DATE)-1) DAY)  ELSE DATE_ADD(DATE_ADD(CURRENT_DATE, INTERVAL -2 MONTH), INTERVAL -(DAY(CURRENT_DATE)-1) DAY) END
GROUP BY 1,2
) finan
GROUP BY 1,2
UNION ALL

-- laboral

SELECT contrato_id,fecha,0,0,CAST(COALESCE(AVG(prom_lab),0) AS UNSIGNED) l,0
FROM
(SELECT a.contrato_id,a.fecha,
CASE WHEN AVG(valor)*100 > 16 THEN 75 - (ABS(AVG(valor)*100 -16)) * (CASE WHEN AVG(valor)*100 > 16 THEN 75/84 ELSE 25/16 END) ELSE
75 + (ABS(AVG(valor)*100 -16)) * (CASE WHEN AVG(valor)*100 > 16 THEN 75/84 ELSE 25/16 END) END prom_lab
FROM tbl_obliglab_repo a INNER JOIN dim_tiempo b
ON a.fecha = b.fecha
INNER JOIN tbl_contrato c ON c.contrato_id = a.contrato_id
WHERE fecha_mes =  CASE WHEN DAY(CURRENT_DATE) >= 20 THEN DATE_ADD(DATE_ADD(CURRENT_DATE, INTERVAL -1 MONTH), INTERVAL -(DAY(CURRENT_DATE)-1) DAY)  ELSE DATE_ADD(DATE_ADD(CURRENT_DATE, INTERVAL -2 MONTH), INTERVAL -(DAY(CURRENT_DATE)-1) DAY) END
GROUP BY 1,2
UNION ALL

SELECT  a.contrato_id,finCont_fecha,((SELECT COUNT(DISTINCT contrato_id) FROM tbl_contrato WHERE cont_fechaFin > CURRENT_DATE) - 
COUNT(DISTINCT a.contrato_id))/(SELECT COUNT(DISTINCT contrato_id) FROM tbl_contrato WHERE cont_fechaFin > CURRENT_DATE) * 100
FROM tbl_contratistacondicionfin a
INNER JOIN dim_tiempo b
ON a.finCont_fecha = b.fecha
INNER JOIN tbl_contrato c ON c.contrato_id = a.contrato_id
WHERE fecha_mes =  CASE WHEN DAY(CURRENT_DATE) >= 20 THEN DATE_ADD(DATE_ADD(CURRENT_DATE, INTERVAL -1 MONTH), INTERVAL -(DAY(CURRENT_DATE)-1) DAY)  ELSE DATE_ADD(DATE_ADD(CURRENT_DATE, INTERVAL -2 MONTH), INTERVAL -(DAY(CURRENT_DATE)-1) DAY) END
GROUP BY 1,2
UNION ALL

SELECT  a.contrato_id,a.fecha,CAST(AVG((PERC_LAB)) AS DECIMAL(4,1)) puntaje
FROM tbl_fiscalización a INNER JOIN dim_tiempo b
ON a.fecha = b.fecha
INNER JOIN tbl_contrato c ON c.contrato_id = a.contrato_id
WHERE fecha_mes =  CASE WHEN DAY(CURRENT_DATE) >= 20 THEN DATE_ADD(DATE_ADD(CURRENT_DATE, INTERVAL -1 MONTH), INTERVAL -(DAY(CURRENT_DATE)-1) DAY)  ELSE DATE_ADD(DATE_ADD(CURRENT_DATE, INTERVAL -2 MONTH), INTERVAL -(DAY(CURRENT_DATE)-1) DAY) END
GROUP BY 1,2) lab 
GROUP BY 1,2
UNION ALL

-- seguridad

SELECT contrato_id,fecha,0,0,0, CAST(COALESCE(AVG(prom_seg),0) AS DECIMAL(3,0)) s
FROM
(SELECT a.contrato_id,fecha_informe fecha,
CASE WHEN (SUM(n_accid)/SUM(hh)*200000)> 5.2 THEN 0 ELSE
CASE WHEN (SUM(n_accid)/SUM(hh)*200000)> 2.6 THEN 75 - ABS((SUM(n_accid)/SUM(hh)*200000)-2.6) * (CASE WHEN (SUM(n_accid)/SUM(hh)*200000) > 2.6 THEN 75/2.6 ELSE 25/2.6 END) ELSE
75 + ABS((SUM(n_accid)/SUM(hh)*200000) -2.6) * (CASE WHEN (SUM(n_accid)/SUM(hh)*200000) > 2.6 THEN 75/2.6 ELSE 25/2.6 END) END
END prom_seg
FROM tbl_accidentes a
INNER JOIN dim_tiempo d ON a.fecha_informe = d.fecha
INNER JOIN tbl_contrato c ON a.contrato_id = c.contrato_id
WHERE 
fecha_mes =  CASE WHEN DAY(CURRENT_DATE) >= 20 THEN DATE_ADD(DATE_ADD(CURRENT_DATE, INTERVAL -1 MONTH), INTERVAL -(DAY(CURRENT_DATE)-1) DAY)  ELSE DATE_ADD(DATE_ADD(CURRENT_DATE, INTERVAL -2 MONTH), INTERVAL -(DAY(CURRENT_DATE)-1) DAY) END
GROUP BY 1,2
UNION ALL

SELECT a.contrato_id,fecha_informe,
CASE WHEN (SUM(DIAS_PERD)/SUM(hh)*200000) > 60 THEN 0 ELSE 
CASE WHEN (SUM(DIAS_PERD)/SUM(hh)*200000)> 31.1 THEN 75 - ABS(( SUM(DIAS_PERD)/SUM(hh)*200000 )-31.1) * (CASE WHEN (SUM(DIAS_PERD)/SUM(hh)*200000) > 31.1 THEN 75/31.1 ELSE 25/31.1 END) ELSE
75 + ABS((SUM(DIAS_PERD)/SUM(hh)*200000) -31.1) * (CASE WHEN (SUM(DIAS_PERD)/SUM(hh)*200000) > 31.1 THEN 75/31.1 ELSE 25/31.1 END) END 
END f
FROM tbl_accidentes a
INNER JOIN dim_tiempo d ON a.fecha_informe = d.fecha
INNER JOIN tbl_contrato c ON a.contrato_id = c.contrato_id
WHERE 
fecha_mes =  CASE WHEN DAY(CURRENT_DATE) >= 20 THEN DATE_ADD(DATE_ADD(CURRENT_DATE, INTERVAL -1 MONTH), INTERVAL -(DAY(CURRENT_DATE)-1) DAY)  ELSE DATE_ADD(DATE_ADD(CURRENT_DATE, INTERVAL -2 MONTH), INTERVAL -(DAY(CURRENT_DATE)-1) DAY) END
GROUP BY 1,2
UNION ALL

SELECT a.contrato_id,a.fecha,AVG((PERC_COND + perc_equip)/2) 
FROM tbl_fiscalización a
INNER JOIN dim_tiempo d ON a.fecha = d.fecha
INNER JOIN tbl_contrato c ON a.contrato_id = c.contrato_id
 WHERE 
fecha_mes =  CASE WHEN DAY(CURRENT_DATE) >= 20 THEN DATE_ADD(DATE_ADD(CURRENT_DATE, INTERVAL -1 MONTH), INTERVAL -(DAY(CURRENT_DATE)-1) DAY)  ELSE DATE_ADD(DATE_ADD(CURRENT_DATE, INTERVAL -2 MONTH), INTERVAL -(DAY(CURRENT_DATE)-1) DAY) END
GROUP BY 1,2) seg
GROUP BY 1,2
) panel
RIGHT JOIN tbl_contrato c ON panel.contrato_id = c.contrato_id
INNER JOIN tbl_contratistas cn ON cn.IdContratista = c.IdContratista
                                                                WHERE
                                                                cont_fechaFin >= CURRENT_DATE
                                                                AND cn.entry_by_access = '.Auth::user()->id.'
GROUP BY 1,2,3,4';
                                                                
                                                                $rawcont = array();
                                                                $j=0;
                                                                foreach ($conn2->query($sqltb) as $row){
                                                                    $rawcont[$j] = $row;
                                                                    $j++;
                                                                }

                                        ?>                      
                                    <div>  <div class="stat-percent font-bold text-info">    <?php
                                                $sql = 'SELECT Anio, NMes3L,
                                                            YEAR(b.financieroand_fecha) AS AGNO
                                                        ,   MONTH(b.financieroand_fecha) AS MES
                                                        ,   REPLACE(REPLACE(REPLACE(FORMAT(SUM(b.financieroand_mtoreal), 0), ".", "@"), ",", "."), "@", ",") AS MtoPago FROM tbl_contrato AS a 
                                                        INNER JOIN tbl_financiero_and b ON a.contrato_id = b.contrato_id
                                                        INNER JOIN dim_tiempo t ON b.financieroand_fecha = t.fecha
                                                        WHERE 
                                                            b.financieroand_fecha = CASE WHEN DAY(CURRENT_DATE) >= 20 THEN DATE_ADD(DATE_ADD(CURRENT_DATE, INTERVAL -1 MONTH), INTERVAL -(DAY(CURRENT_DATE)-1) DAY)  ELSE DATE_ADD(DATE_ADD(CURRENT_DATE, INTERVAL -2 MONTH), INTERVAL -(DAY(CURRENT_DATE)-1) DAY) END
                                                           
                                                         ';
                                            foreach ($conn2->query($sql) as $row) {
                                                print $row['NMes3L']."-".$row['Anio']."\n";
                                                }
                                        ?></div>
                                        <h4>Contratos Asignados (<?php
                                                $sql = 'select COUNT(DISTINCT b.contrato_id) as CantidadContratos from tbl_contrato as b where cont_fechaFin >= current_date ; ';
                                                foreach ($conn2->query($sql) as $row) {


                                                print $row['CantidadContratos'] ;
                                                }
                                                
                                            ?>)</h4>
                                            <div height="191" id="tabla" style=" width: 100%;margin: 10px 0 ; height: 300px;  overflow-y: auto;" >
                                                <?php
                                                        echo "<table style='border: solid 0px;' class='table table-striped'>";
                                                        echo "<tr><th>Estado </th><th><center> Contrato</th></tr>";
                                                        $j=0;                           
                                                        for($j = 0 ;$j<count($rawcont);$j++){
                                                             
                                                                     echo "<tr>";
                                                                    if($rawcont[$j]['general'] < 75 &&  $rawcont[$j]['general'] >0 )
                                                                    {
                                                                        echo "<td> <div class=\"circle_red\">".$rawcont[$j]['general']."%</div></td>";
                                                                    }
                                                                    else if($rawcont[$j]['general'] >= 75)
                                                                    {
                                                                        echo "<td> <div class=\"circle_green\">".$rawcont[$j]['general']."%</div></td>";
                                                                    }
                                                                    else
                                                                    {
                                                                        echo "<td> <div class=\"circle_grey\">".$rawcont[$j]['general']."%</div></td>";
                                                                    }
                                                                    echo "<td> <a href=\"http://one.sourcing.cl/reportdetcontrato?id=".$rawcont[$j]['contrato_id']."&mes=".$rawcont[$j]['mes']."&year=".$rawcont[$j]['anio']."\">".$rawcont[$j]['cont_numero']." - ".$rawcont[$j]['cont_proveedor']."</a></td>";
                                                                    //echo "<td> ".$rawcont[$j]['cont_numero']." - ".$rawcont[$j]['cont_proveedor']."</td>";
                                                                    echo "</tr>";
                                                                }
                                                    
                                                        
                                                        echo "</table>";
                                                        ?>
                                                        

                                               
                                            </div>
                                        </div>
                                    </div><!-- /sbox-content -->
                                </div> <!-- /sbox -->


                            </div>
                        </div><!-- </div class="row-mt"> -->
						
						 <div class="row m-t">
                         <div class="col-lg-12">
                                <div class="sbox">                          
                                    <div class="sbox-content">
                                        <div> <h4><center> Evolución por Ámbito</h4>
                                            <div height="300px" id="grafico" style=" margin: 10px 0 ; background: #ffffff;margin-left: auto; margin-right: auto;width: 100%;" >
                                                <p style="padding: 80px 0; font-size: 20px;">
                                               
                                                </p>

                                            </div>
                                        </div>
                                    </div><!-- /sbox-content -->
                                </div> <!-- /sbox -->
                            </div> <!--</div class="col-lg-12"> -->
                        
                        </div>

                                <?php $sqlnt = 'SELECT not_area,not_seccion, not_fechaPub,not_publicacion,not_autor,
                                DATE(not_fechaPub) fecha, TIME(not_fechaPub) tiem,
                                dia, NMES3L, anio
                                
                                FROM tbl_noticiasglobales
                                INNER JOIN dim_tiempo ON Fecha = DATE(not_fechaPub)
                                
                                WHERE not_activa IS NULL
                                
                                ORDER BY not_fechaPub DESC';
                                
                                $rawnot = array();
                                $j=0;
                                foreach ($conn2->query($sqlnt) as $row){
                                    $rawnot[$j] = $row;
                                    $j++;
                                }
                                    ?>
                        <div class="row">
                            <div class="col-lg-6">
                                
                                <div class="sbox">
                                    <div class="sbox-title">
                                        <h4>Noticias relevantes!!  
                                            <!--<a class="pull-right" href="#" data-dismiss="alert">
                                                <i class="fa fa-times"></i>&nbsp;
                                            </a>
                                            <a class="pull-right" href="#">
                                                <i class="fa fa-wrench"></i>&nbsp;
                                            </a>-->
                                            <a class="pull-right" href="#collapseTwo" data-parent="#accordion" data-toggle="collapse">
                                                <i class="fa fa-chevron-up"></i>&nbsp;                                    
                                            </a>
                                        </h4>
                                    </div><!-- </div class="sbox-title"> -->


                                     <div id="collapseTwo" class="collapse in" style="height:  430px; overflow-y: scroll;">    
                                        <div class="sbox-content" id="1" style="background-color:white;">
                                            <div class="feed-activity-list">
                                                      <?php
                                                    
                                                        $j=0;                           
                                                        for($j = 0 ;$j<count($rawnot);$j++){
                                                        ?>  
                                                        <div class="row m-t">
                                                            <div class="col-xs-3">
                                                                    <?php echo  $rawnot[$j]["dia"]." de ".$rawnot[$j]["NMES3L"].", ".$rawnot[$j]["anio"]."<br>".$rawnot[$j]["tiem"];
                                                                    ?>
                                                        
                                                            </div>
                                                               <p class="m-b-xs"> 
                                                               <div class="alert alert-success fade in block-inner col-xs-7" >

                                                                <p><h4><?php 

                                                                    echo $rawnot[$j]["not_area"];

                                                                ?></h4><br><b>
                                                                <?php 

                                                                    echo $rawnot[$j]["not_seccion"];

                                                                ?>
                                                                </b>
                                                                </p>
                                                                <?php 

                                                                    echo $rawnot[$j]["not_publicacion"];
                                                                ?>
                                                                </div>
                                                               </p>
                                                        </div>
                                                             <?php
                                                                             }
                                    ?>
                                            </div>
                                        </div><!-- </div class="sbox-content">  --> 
                                    </div>
                                </div>
                            </div><!-- </div class="col-lg-6"> -->
							
                            <div class="col-lg-6">
                                <div class="sbox">
                                       <?php $sqltb = 'SELECT fecha_registro,Nmes3l, anio,dia, DATE_FORMAT(fecha_registro,\'%d-%m-%Y\') fecha, 
                                                    DATE_FORMAT(fecha_registro,\'%H:%i\') hora,
                                                    fecha_fin, tp.tipo_alerta, desc_tipo_alerta, texto_mensaje, desc_crit,a.id_crit,
                                                    c.cont_numero, c.cont_proveedor,c.contrato_id, td.Descripcion, a.subambito_id,ambito_nombre,
                                                    p.RUT,p.Nombres,p.Apellidos,cn.RUT rut_cn, cn.RazonSocial
                                                    
                                                    FROM tbl_alertas a INNER JOIN tbl_alerta_tpo tp ON a.tipo_alerta = tp.tipo_alerta
                                                    INNER JOIN tbl_alerta_msg am ON am.id_mensaje = a.id_mensaje
                                                    INNER JOIN tbl_alerta_crit ac ON ac.id_crit = a.id_crit
                                                    INNER JOIN tbl_contrato c ON a.contrato_id = c.contrato_id
                                                    INNER JOIN dim_tiempo t ON DATE_FORMAT(fecha_registro,\'%Y-%m-%d\') = fecha
                                                    LEFT JOIN tbl_planes_y_programas_ambitosub sa ON a.subambito_id = sa.subambito_id
                                                    LEFT JOIN tbl_documentos d ON a.documento_id = d.IdDocumento
                                                    LEFT JOIN tbl_tipos_documentos td ON d.IdTipoDocumento = td.IdTipoDocumento
                                                    LEFT JOIN tbl_personas p ON p.IdPersona = a.persona_id
                                                    LEFT JOIN tbl_contratistas cn ON cn.IdContratista = a.contratista_id
                                                    
                                                    WHERE 
                                                    cn.entry_by_access =  '.Auth::user()->id.' 
                                                    AND alerta_activa = 1
                                                    AND fecha_registro IS NOT NULL
                                                    ORDER BY a.id_crit DESC, fecha_registro
                                                    ';
                                                                
                                                    $rawcont = array();
                                                    $j=0;
                                                    foreach ($conn2->query($sqltb) as $row){
                                                        $rawcont[$j] = $row;
                                                        $j++;
                                                    }
                                            
                                                $sqlal = 'select SUM(CASE WHEN id_crit = 3 THEN 1 ELSE 0 END) grave, SUM(CASE WHEN id_crit = 2 THEN 1 ELSE 0 END) critico, SUM(CASE WHEN id_crit = 1 THEN 1 ELSE 0 END) adv
                                                    
                                                    FROM tbl_alertas a 
                                                    INNER JOIN tbl_contrato c ON a.contrato_id = c.contrato_id
                                                    LEFT JOIN tbl_personas p ON p.IdPersona = a.persona_id
                                                    LEFT JOIN tbl_contratistas cn ON cn.IdContratista = a.contratista_id
                                                    
                                                    WHERE 
                                                    cn.entry_by_access =  '.Auth::user()->id.' 
                                                    AND alerta_activa = 1
                                                    AND fecha_registro IS NOT NULL
                                                
                                                    ';
                                                                
                                                    $rawsum = array();
                                                    $k=0;
                                                    foreach ($conn2->query($sqlal) as $row1){
                                                        $rawsum[$j] = $row1;
                                                        $k++;
                                                    }   
                                                    
                                                    
                                        ?>
									
                                    <div class="sbox-title">
                                        <h4>Alertas &nbsp;<small class="text-navy text-danger"><b>Graves (<?php echo  $rawsum[$j]["grave"]?>)  &nbsp;</b></small>
                                        <small class="text-navy text-warning"><b>Críticas (<?php echo  $rawsum[$j]["critico"]?>)  &nbsp;</b></small>
                                        <small class="text-navy text-info"><b>Advertencias (<?php echo  $rawsum[$j]["adv"]?>)  &nbsp;</b></small>
                                            <!--<a class="pull-right" href="#" data-dismiss="alert">
                                                <i class="fa fa-times"></i>&nbsp;
                                            </a>
                                            <a class="pull-right" href="#">
                                                <i class="fa fa-wrench"></i>&nbsp;
                                            </a>-->
                                            <a class="pull-right" href="#collapseThree" data-parent="#accordion" data-toggle="collapse">
                                                <i class="fa fa-chevron-up"></i>&nbsp;                                    
                                            </a>
                                        </h4>
                                    </div><!-- </div class="sbox-title"> -->

                                    <div id="collapseThree" class="collapse in" style="height: 430px; overflow-y: scroll;" >    
                                        <div class="sbox-content" id="1">
                                        
                                            <div id="collapseThree">                                               
                                                   <?php
                                                    
                                                        $j=0;                           
                                                        for($j = 0 ;$j<count($rawcont);$j++){
                                                        ?>                      
                                                        <div class="row m-t">
                                                            <div class="col-xs-3">
                                                                    <?php echo  $rawcont[$j]["dia"]." de ".$rawcont[$j]["Nmes3l"].", ".$rawcont[$j]["anio"]."<br>".$rawcont[$j]["hora"]
                                                                    ?>
                                                                <br>
                                                                <?php
                                                                            if ($rawcont[$j]["id_crit"] == 1)
                                                                                echo "<small class=\"text-navy text-info\"><b> ".$rawcont[$j]["desc_crit"]."</b></small>";
                                                                            else if ($rawcont[$j]["id_crit"] == 2)
                                                                                echo "<small class=\"text-navy text-warning\"><b> ".$rawcont[$j]["desc_crit"]."</b></small>";
                                                                            else  if ($rawcont[$j]["id_crit"] == 3)
                                                                                echo "<small class=\"text-navy text-danger\"><b> ".$rawcont[$j]["desc_crit"]."</b></small>";
                                                                    ?>
                                                            </div>
                                                            <?php
                                                                            if ($rawcont[$j]["id_crit"] == 1)
                                                                                echo "<div class=\"alert alert-info fade in block-inner col-xs-7\" >";
                                                                            else if ($rawcont[$j]["id_crit"] == 2)
                                                                                echo "<div class=\"alert alert-warning fade in block-inner col-xs-7\" >";
                                                                            else  if ($rawcont[$j]["id_crit"] == 3)
                                                                                echo "<div class=\"alert alert-danger fade in block-inner col-xs-7\" >";
                                                                    ?>
                                                                <p class="m-b-xs"> 
                                                                <?php 
                                                                    if ( $rawcont[$j]["tipo_alerta"] == 1)
                                                                        echo "<i class=\"fa fa-bar-chart-o\"></i> ";
                                                                    elseif ($rawcont[$j]["tipo_alerta"] == 2)
                                                                        echo "<i class=\"fa fa-exclamation-triangle\"></i> ";
                                                                    elseif ($rawcont[$j]["tipo_alerta"] == 3)
                                                                        echo "<i class=\"fa fa-print\"></i> ";
                                                                    elseif ($rawcont[$j]["tipo_alerta"] == 5)
                                                                        echo "<i class=\"fa fa-ban\"></i> ";
                                                                    elseif ($rawcont[$j]["tipo_alerta"] == 6)
                                                                        echo "<i class=\"fa  fa-file-o\"></i> ";
                                                                    elseif ($rawcont[$j]["tipo_alerta"] == 7)
                                                                        echo "<i class=\"fa  fa-calendar-o\"></i> ";
                                                                    elseif ($rawcont[$j]["tipo_alerta"] == 8)
                                                                        echo "<i class=\"fa  fa-calendar-o\"></i> ";
                                                                    elseif ($rawcont[$j]["tipo_alerta"] == 9)
                                                                        echo "<i class=\"fa  fa-calendar-o\"></i> ";
                                                                    elseif ($rawcont[$j]["tipo_alerta"] == 10)
                                                                        echo "<i class=\"fa  fa-file-text\"></i> ";
                                                                    elseif ($rawcont[$j]["tipo_alerta"] == 11)
                                                                        echo "<i class=\"fa  fa-edit\"></i> ";
                                                                    elseif ($rawcont[$j]["tipo_alerta"] == 12)
                                                                        echo "<i class=\"fa  fa-file-text\"></i> ";
                                                                    else
                                                                        echo "<i class=\"fa  fa-suitcase\"></i> ";
                                                                    ?>
                                                                &nbsp;<strong> <?php echo  $rawcont[$j]["desc_tipo_alerta"]  ?></strong></p>
                                                                <p>
                                                                
                                                                <?php 
                                                                    if ( $rawcont[$j]["tipo_alerta"] == 1){ 
                                                                    echo  "Para el contrato (".$rawcont[$j]["cont_numero"].") ".$rawcont[$j]["cont_proveedor"].", no hay datos asociados para el indicador <b>".$rawcont[$j]["ambito_nombre"]."</b> y <br>" ;
                                                                     echo  $rawcont[$j]["texto_mensaje"] ;
                                                                    
                                                                    }
                                                                    
                                                                    elseif ($rawcont[$j]["tipo_alerta"] == 2){
                                                                        echo "Para el contrato (".$rawcont[$j]["cont_numero"].") ".$rawcont[$j]["cont_proveedor"]." ";
                                                                        echo  $rawcont[$j]["texto_mensaje"] ;
                                                                        
                                                                        
                                                                    }
                                                                        
                                                                    elseif ($rawcont[$j]["tipo_alerta"] == 3){
                                                                        echo "El Documento \"".$rawcont[$j]["Descripcion"]."\" asociado al contrato (".$rawcont[$j]["cont_numero"].") ".$rawcont[$j]["cont_proveedor"]." <br> se encuentra con problemas de vigencia";
                                                                        // echo  $rawcont[$j]["texto_mensaje"] ;
                                                                    }
                                                                
                                                                    elseif ($rawcont[$j]["tipo_alerta"] == 5){
                                                                        echo "El Documento \"".$rawcont[$j]["Descripcion"]."\" asociado al contrato (".$rawcont[$j]["cont_numero"].") ".$rawcont[$j]["cont_proveedor"]." <br> se encuentra rechazado";
                                                                        //echo  $rawcont[$j]["texto_mensaje"] ;
                                                                    }
                                                                    
                                                                    elseif ($rawcont[$j]["tipo_alerta"] == 6){
                                                                        echo "El Documento \"".$rawcont[$j]["Descripcion"]."\" asociado al contrato (".$rawcont[$j]["cont_numero"].") ".$rawcont[$j]["cont_proveedor"]." <br> se encuentra con errores de carga";
                                                                        //echo  $rawcont[$j]["texto_mensaje"] ;
                                                                    }
                                                                        
                                                                    elseif ($rawcont[$j]["tipo_alerta"] == 7){
                                                                        echo "El contrato de la persona Rut ".$rawcont[$j]["RUT"]."  ".$rawcont[$j]["Nombres"]." ".$rawcont[$j]["Apellidos"]." está por vencer";
                                                                        //echo  $rawcont[$j]["texto_mensaje"] ;
                                                                    }
                                                                    elseif ($rawcont[$j]["tipo_alerta"] == 8)
                                                                    {
                                                                        echo "El Documento \"".$rawcont[$j]["Descripcion"]."\" asociado a la persona Rut ".$rawcont[$j]["RUT"]."  ".$rawcont[$j]["Nombres"]." ".$rawcont[$j]["Apellidos"]." <br> se encuentra con problemas de vigencia";
                                                                        //echo  $rawcont[$j]["texto_mensaje"] ;
                                                                    }
                                                                    elseif ($rawcont[$j]["tipo_alerta"] == 9){
                                                                        echo "El Documento \"".$rawcont[$j]["Descripcion"]."\" asociado al Contratista Rut ".$rawcont[$j]["rut_cn"]."  ".$rawcont[$j]["RazonSocial"]." <br> se encuentra con problemas de vigencia";
                                                                        //echo  $rawcont[$j]["texto_mensaje"] ;
                                                                    }
                                                                    elseif ($rawcont[$j]["tipo_alerta"] == 10){
                                                                        echo "El Documento F30-1 asociado al Contratista Rut ".$rawcont[$j]["rut_cn"]."  ".$rawcont[$j]["RazonSocial"]." <br> ";
                                                                        echo  $rawcont[$j]["texto_mensaje"] ;
                                                                    }
                                                                    elseif ($rawcont[$j]["tipo_alerta"] == 11){
                                                                        echo "El contrato (".$rawcont[$j]["cont_numero"].") ".$rawcont[$j]["cont_proveedor"]." no se encuentra firmado";
                                                                        //echo  $rawcont[$j]["texto_mensaje"] ;
                                                                    }
                                                                    elseif ($rawcont[$j]["tipo_alerta"] == 12){
                                                                            echo  $rawcont[$j]["texto_mensaje"] ;
                                                                    }
                                                                    else{
                                                                        echo  $rawcont[$j]["texto_mensaje"] ;
                                                                    }
                                                                    ?>
                                                                
                                                                
                                                                
                                                                
                                                                
                                                                </p>
                                                            </div>                                
                                                        </div>
                                                    <?php } ?>
                                                      
                                             
                                            </div>

                                        </div><!-- </div class="sbox-content">  --> 
                                    </div>                     
                                </div><!-- /sbox -->
                            </div><!-- </div class="col-lg-6"> -->
                        </div><!-- </div class="row"> -->

                    </div><!-- </div class="col-lg-9"> -->

                        <div class="col-lg-3" style="background-color:#ebebed;">
                            
                            <div class="" style="height: 1196px;">

                                    <div class="m-t-md">
                                        <h4>Mensajes y Alertas</h4>
                                        <div>
                                            <ul class="list-group">
                                                <li class="list-group-item">
                                                    <span class="badge badge-success">0</span>
                                                    <a href="{{ URL::to('') }}">Anotaciones a los libros de obras pendientes de respuesta</a>
                                                </li>
                                                <li class="list-group-item">
                                                    <span class="badge badge-success">0</span>
                                                    Anotaciones nuevas libro de obras
                                                </li>
                                                <li class="list-group-item">
                                                    <span class="badge badge-success">
                                                    0
                                                    
                                                    </span>
                                                    Incumplimiento de requisitos de documento
                                                </li>
                                                <li class="list-group-item">
                                                    
                                                     <?php
                                                $sql = 'SELECT COUNT(idDocumento) doc
                                                            FROM ( 
                                                            SELECT DISTINCT idDocumento FROM tbl_documentos d
                                                            INNER JOIN tbl_contratos_personas cp ON d.IdEntidad = cp.IdContratista 
                                                            INNER JOIN tbl_contrato c ON c.contrato_id = cp.contrato_id
                                                            INNER JOIN tbl_contratistas cn ON cn.IdContratista = c.IdContratista
                                                            WHERE entidad =1 AND 
                                                            DATEDIFF(fechaVencimiento, CURRENT_DATE) <30 AND
                                                            cn.entry_by_access =   '.Auth::user()->id.'
                                                            
                                                            UNION ALL
                                                            
                                                            SELECT DISTINCT idDocumento FROM tbl_documentos d                                                           
                                                            INNER JOIN tbl_contratos_personas cp ON d.IdEntidad = cp.contrato_id 
                                                            INNER JOIN tbl_contrato c ON c.contrato_id = cp.contrato_id
                                                            INNER JOIN tbl_contratistas cn ON cn.IdContratista = c.IdContratista
                                                            WHERE entidad =2 AND DATEDIFF(fechaVencimiento, CURRENT_DATE) <30 AND
                                                            cn.entry_by_access =   '.Auth::user()->id.'
                                                            
                                                            UNION ALL
                                                            
                                                            SELECT DISTINCT idDocumento FROM tbl_documentos d
                                                            INNER JOIN tbl_contratos_personas cp ON d.IdEntidad = cp.idpersona 
                                                            INNER JOIN tbl_contrato c ON c.contrato_id = cp.contrato_id
                                                            INNER JOIN tbl_contratistas cn ON cn.IdContratista = c.IdContratista
                                                            WHERE entidad =3 AND 
                                                            DATEDIFF(fechaVencimiento, CURRENT_DATE) <30 AND
                                                            cn.entry_by_access =   '.Auth::user()->id.') c';
                                                            
                                                            
                                                foreach ($conn2->query($sql) as $row) {
                                                    $num = $row['doc'];
                                                }
                                                if ($num>0){
                                                    echo "<span class=\"badge badge-danger\">";
                                                }
                                                else{
                                                    echo "<span class=\"badge badge-success\">";
                                                }
                                                print $num;
                                        ?>
                                                    </span>
                                                    Vencimientos de documentos
                                                </li>
                                                <li class="list-group-item">
                                                       
                                                     <?php
                                                $sql = 'SELECT COUNT(idDocumento) doc
                                                            FROM ( 
                                                            SELECT DISTINCT idDocumento FROM tbl_documentos d
                                                            INNER JOIN tbl_contratos_personas cp ON d.IdEntidad = cp.IdContratista 
                                                            INNER JOIN tbl_contrato c ON c.contrato_id = cp.contrato_id
                                                            INNER JOIN tbl_contratistas cn ON cn.IdContratista = c.IdContratista
                                                            WHERE entidad =1 AND 
                                                            d.IdEstatus = 3 AND
                                                            cn.entry_by_access = '.Auth::user()->id.'
                                                            
                                                            UNION ALL
                                                            
                                                            SELECT DISTINCT idDocumento FROM tbl_documentos d                                                           
                                                            INNER JOIN tbl_contratos_personas cp ON d.IdEntidad = cp.contrato_id 
                                                            INNER JOIN tbl_contrato c ON c.contrato_id = cp.contrato_id
                                                            INNER JOIN tbl_contratistas cn ON cn.IdContratista = c.IdContratista
                                                            WHERE entidad =2 AND d.IdEstatus = 3 AND
                                                            cn.entry_by_access = '.Auth::user()->id.'
                                                            
                                                            UNION ALL
                                                            
                                                            SELECT DISTINCT idDocumento FROM tbl_documentos d
                                                            INNER JOIN tbl_contratos_personas cp ON d.IdEntidad = cp.idpersona 
                                                            INNER JOIN tbl_contrato c ON c.contrato_id = cp.contrato_id
                                                            INNER JOIN tbl_contratistas cn ON cn.IdContratista = c.IdContratista
                                                            WHERE entidad =3 AND 
                                                            d.IdEstatus = 3 AND
                                                            cn.entry_by_access = '.Auth::user()->id.') c';
                                                            
                                                            
                                                foreach ($conn2->query($sql) as $row) {
                                                    $num = $row['doc'];
                                                }
                                                if ($num>0){
                                                    echo "<span class=\"badge badge-danger\">";
                                                }
                                                else{
                                                    echo "<span class=\"badge badge-success\">";
                                                }
                                                print $num;
                                        ?>
                                                    </span>
                                                    Rechazos de documentos
                                                </li>
                                                <li class="list-group-item">
                                                                           
                                                     <?php
                                                $sql = 'SELECT COUNT(idDocumento) doc
                                                            FROM ( 
                                                            SELECT DISTINCT idDocumento FROM tbl_documentos d
                                                            INNER JOIN tbl_contratos_personas cp ON d.IdEntidad = cp.IdContratista 
                                                            INNER JOIN tbl_contrato c ON c.contrato_id = cp.contrato_id
                                                            INNER JOIN tbl_contratistas cn ON cn.IdContratista = c.IdContratista
                                                            WHERE entidad =1 AND 
                                                            d.IdEstatus = 2 AND
                                                            cn.entry_by_access = '.Auth::user()->id.'
                                                            
                                                            UNION ALL
                                                            
                                                            SELECT DISTINCT idDocumento FROM tbl_documentos d                                                           
                                                            INNER JOIN tbl_contratos_personas cp ON d.IdEntidad = cp.contrato_id 
                                                            INNER JOIN tbl_contrato c ON c.contrato_id = cp.contrato_id
                                                            INNER JOIN tbl_contratistas cn ON cn.IdContratista = c.IdContratista
                                                            WHERE entidad =2 AND d.IdEstatus = 2 AND
                                                            cn.entry_by_access = '.Auth::user()->id.'
                                                            
                                                            UNION ALL
                                                            
                                                            SELECT DISTINCT idDocumento FROM tbl_documentos d
                                                            INNER JOIN tbl_contratos_personas cp ON d.IdEntidad = cp.idpersona 
                                                            INNER JOIN tbl_contrato c ON c.contrato_id = cp.contrato_id
                                                            INNER JOIN tbl_contratistas cn ON cn.IdContratista = c.IdContratista
                                                            WHERE entidad =3 AND 
                                                            d.IdEstatus = 2 AND
                                                            cn.entry_by_access = '.Auth::user()->id.') c';
                                                            
                                                            
                                                foreach ($conn2->query($sql) as $row) {
                                                    $num = $row['doc'];
                                                }
                                                if ($num>0){
                                                    echo "<span class=\"badge badge-danger\">";
                                                }
                                                else{
                                                    echo "<span class=\"badge badge-success\">";
                                                }
                                                print $num;
                                        ?>
                                                    </span>
                                                    Documentos por aprobar
                                                </li>
                                                <li class="list-group-item">
                                                    <span class="badge badge-success">0</span>
                                                    Documentos no entregados en plazo
                                                </li>
                                                <li class="list-group-item">
                                            
                                                     <?php
                                                $sql = 'SELECT COUNT(idDocumento) doc
                                                            FROM ( 
                                                            SELECT DISTINCT idDocumento FROM tbl_documentos d
                                                            INNER JOIN tbl_contratos_personas cp ON d.IdEntidad = cp.IdContratista 
                                                            INNER JOIN tbl_contrato c ON c.contrato_id = cp.contrato_id
                                                            INNER JOIN tbl_contratistas cn ON cn.IdContratista = c.IdContratista
                                                            WHERE entidad =1 AND 
                                                            d.IdEstatus = 1  AND
                                                            cn.entry_by_access = '.Auth::user()->id.'
                                                            
                                                            UNION ALL
                                                            
                                                            SELECT DISTINCT idDocumento FROM tbl_documentos d                                                           
                                                            INNER JOIN tbl_contratos_personas cp ON d.IdEntidad = cp.contrato_id 
                                                            INNER JOIN tbl_contrato c ON c.contrato_id = cp.contrato_id
                                                            INNER JOIN tbl_contratistas cn ON cn.IdContratista = c.IdContratista
                                                            WHERE entidad =2 AND d.IdEstatus = 1  AND
                                                            cn.entry_by_access = '.Auth::user()->id.'
                                                            
                                                            UNION ALL
                                                            
                                                            SELECT DISTINCT idDocumento FROM tbl_documentos d
                                                            INNER JOIN tbl_contratos_personas cp ON d.IdEntidad = cp.idpersona 
                                                            INNER JOIN tbl_contrato c ON c.contrato_id = cp.contrato_id
                                                            INNER JOIN tbl_contratistas cn ON cn.IdContratista = c.IdContratista
                                                            WHERE entidad =3 AND 
                                                            d.IdEstatus = 1  AND
                                                            cn.entry_by_access = '.Auth::user()->id.') c';
                                                            
                                                            
                                                foreach ($conn2->query($sql) as $row) {
                                                    $num = $row['doc'];
                                                }
                                                if ($num>0){
                                                    echo "<span class=\"badge badge-danger\">";
                                                }
                                                else{
                                                    echo "<span class=\"badge badge-success\">";
                                                }
                                                print $num;
                                        ?>
                                                    </span>
                                                    Documentos pendientes de entrega
                                                </li>
                                                
                                            </ul>
                                        </div>
                                    </div>

                            </div>
                            <!-- </div style="height: 1196px;"> -->

                        </div>
                        <!-- </div class="col-lg-3" style="background-color:#ebebed;"> -->

                    </div>

    
            </div>  
        </div>                  
    
                                        
<?php
                  $sqlgr = 'SELECT fecha_ambito, fecha, CAST(SUM(n) AS DECIMAL(5,2)) Negocio,CAST(SUM(f) AS DECIMAL(5,2))Financiero , CAST(SUM(l) AS DECIMAL(5,2)) Laboral, CAST(SUM(s) AS DECIMAL(5,2)) seguridad
from
(
SELECT fecha_mes fecha_ambito,1 ambito,CONCAT(CONCAT(NMes3L,\'-\'),Anio) fecha, AVG(prom_neg) n,0 f,0 l ,0 s
FROM
(
SELECT fecha_mes,NMes3L,anio,AVG(kpi) prom_neg
FROM
(SELECT  admin_id,b.contrato_id,kpiDet_fecha, NMes3L,Anio,fecha_mes, (SUM(CASE WHEN kpi_tipo_calc = 1 THEN CASE WHEN kpiDet_puntaje >= kpiDet_meta THEN 1 ELSE 0 END WHEN kpi_tipo_calc = 2 THEN CASE WHEN kpiDet_puntaje <= kpiDet_meta THEN 1 ELSE 0 END WHEN kpi_tipo_calc = 3 THEN CASE WHEN kpiDet_puntaje BETWEEN kpiDet_min AND kpiDet_max THEN 1 ELSE 0 END WHEN kpi_tipo_calc = 4 THEN 1 END )/
COUNT(DISTINCT a.id_kpi) )*100 kpi
FROM tbl_kpimensual a INNER JOIN tbl_kpigral b
ON a.id_kpi = b.id_kpi
INNER JOIN dim_tiempo ON kpiDet_fecha = fecha
inner join tbl_contrato c on c.contrato_id = b.contrato_id
WHERE kpiDet_puntaje IS NOT NULL
and c.entry_by_access = '.Auth::user()->id.'
AND anio = YEAR(CASE WHEN DAY(CURRENT_DATE) >= 20 THEN DATE_ADD(DATE_ADD(CURRENT_DATE, INTERVAL -1 MONTH), INTERVAL -(DAY(CURRENT_DATE)-1) DAY)  ELSE DATE_ADD(DATE_ADD(CURRENT_DATE, INTERVAL -2 MONTH), INTERVAL -(DAY(CURRENT_DATE)-1) DAY) END)
AND fecha_mes <= CASE WHEN DAY(CURRENT_DATE) >= 20 THEN DATE_ADD(DATE_ADD(CURRENT_DATE, INTERVAL -1 MONTH), INTERVAL -(DAY(CURRENT_DATE)-1) DAY)  ELSE DATE_ADD(DATE_ADD(CURRENT_DATE, INTERVAL -2 MONTH), INTERVAL -(DAY(CURRENT_DATE)-1) DAY) END 
GROUP BY 1,2,3,4,5) asd
group by 1,2,3

UNION ALL

SELECT
b.fecha_mes,NMes3L,b.anio,
CASE WHEN AVG(eval_puntaje) >70 THEN 75 + ((CASE WHEN AVG(eval_puntaje) >70 THEN 25/30 ELSE 75/70 END)*(AVG(eval_puntaje) - 70)) 
ELSE 75 - ((CASE WHEN AVG(eval_puntaje) >70 THEN 25/30 ELSE 75/70 END)*(AVG(eval_puntaje) - 70)) END f
FROM
(SELECT semestre,anio,fecha_mes, contrato_id, eval_puntaje
FROM tbl_evalcontratistas a INNER JOIN dim_tiempo 
ON eval_fecha = fecha 
) a
INNER JOIN
(SELECT semestre,anio,NMes3L, mes,fecha_mes, contrato_id,admin_id, entry_by_access FROM
 tbl_contrato b  CROSS JOIN (SELECT DISTINCT semestre, fecha_mes,anio,NMes3L,mes FROM dim_tiempo WHERE anio = YEAR(CASE WHEN DAY(CURRENT_DATE) >= 20 THEN DATE_ADD(DATE_ADD(CURRENT_DATE, INTERVAL -1 MONTH), INTERVAL -(DAY(CURRENT_DATE)-1) DAY)  ELSE DATE_ADD(DATE_ADD(CURRENT_DATE, INTERVAL -2 MONTH), INTERVAL -(DAY(CURRENT_DATE)-1) DAY) END)) c
 ) b
ON a.contrato_id = b.contrato_id AND b.semestre = a.semestre
WHERE  
eval_puntaje IS NOT NULL
and b.entry_by_access = '.Auth::user()->id.'
AND b.anio = YEAR(CASE WHEN DAY(CURRENT_DATE) >= 20 THEN DATE_ADD(DATE_ADD(CURRENT_DATE, INTERVAL -1 MONTH), INTERVAL -(DAY(CURRENT_DATE)-1) DAY)  ELSE DATE_ADD(DATE_ADD(CURRENT_DATE, INTERVAL -2 MONTH), INTERVAL -(DAY(CURRENT_DATE)-1) DAY) END)
AND b.fecha_mes <= CASE WHEN DAY(CURRENT_DATE) >= 20 THEN DATE_ADD(DATE_ADD(CURRENT_DATE, INTERVAL -1 MONTH), INTERVAL -(DAY(CURRENT_DATE)-1) DAY)  ELSE DATE_ADD(DATE_ADD(CURRENT_DATE, INTERVAL -2 MONTH), INTERVAL -(DAY(CURRENT_DATE)-1) DAY) END 
GROUP BY 1,2,3

UNION ALL

SELECT b.fecha_mes,NMes3L,b.anio, AVG(resultado) puntaje
FROM 
(SELECT trimestre,anio,fecha_mes, contrato_id, resultado
FROM tbl_calidad_adm_cont a INNER JOIN dim_tiempo 
ON fec_rev = fecha 
) a
INNER JOIN
(SELECT trimestre,anio,NMes3L, mes,fecha_mes, contrato_id,admin_id, entry_by_access FROM
 tbl_contrato b  CROSS JOIN (SELECT DISTINCT trimestre, fecha_mes,anio,NMes3L,mes FROM dim_tiempo WHERE anio = YEAR(CASE WHEN DAY(CURRENT_DATE) >= 20 THEN DATE_ADD(DATE_ADD(CURRENT_DATE, INTERVAL -1 MONTH), INTERVAL -(DAY(CURRENT_DATE)-1) DAY)  ELSE DATE_ADD(DATE_ADD(CURRENT_DATE, INTERVAL -2 MONTH), INTERVAL -(DAY(CURRENT_DATE)-1) DAY) END)) c
 ) b
ON a.contrato_id = b.contrato_id AND b.trimestre = a.trimestre
WHERE resultado IS NOT NULL
and b.entry_by_access = '.Auth::user()->id.'
AND b.anio = YEAR(CASE WHEN DAY(CURRENT_DATE) >= 20 THEN DATE_ADD(DATE_ADD(CURRENT_DATE, INTERVAL -1 MONTH), INTERVAL -(DAY(CURRENT_DATE)-1) DAY)  ELSE DATE_ADD(DATE_ADD(CURRENT_DATE, INTERVAL -2 MONTH), INTERVAL -(DAY(CURRENT_DATE)-1) DAY) END)
AND b.fecha_mes <= CASE WHEN DAY(CURRENT_DATE) >= 20 THEN DATE_ADD(DATE_ADD(CURRENT_DATE, INTERVAL -1 MONTH), INTERVAL -(DAY(CURRENT_DATE)-1) DAY)  ELSE DATE_ADD(DATE_ADD(CURRENT_DATE, INTERVAL -2 MONTH), INTERVAL -(DAY(CURRENT_DATE)-1) DAY) END
GROUP BY 1,2,3
) ambito
group by 1,2

UNION ALL 

-- financiero

SELECT fecha_mes fecha_ambito,2,CONCAT(CONCAT(NMes3L,\'-\'),Anio) fecha,0, COALESCE(AVG(prom_fin),0) f ,0,0
FROM
(SELECT 
NMes3L,Anio,fecha_mes,
CASE WHEN AVG(valor) > 50 THEN 75 - (AVG(valor) -50) * (CASE WHEN AVG(valor) > 50 THEN 75/50 ELSE 25/50 END) ELSE
75 + (AVG(valor) -50) * (CASE WHEN AVG(valor) > 50 THEN 75/50 ELSE 25/50 END) END prom_fin
FROM tbl_eval_financiera a
INNER JOIN tbl_contrato c ON c.contrato_id = a.contrato_id
INNER JOIN dim_tiempo d ON fecha_validez = d.fecha
WHERE
c.entry_by_access = '.Auth::user()->id.' and
fecha_mes <= CASE WHEN DAY(CURRENT_DATE) >= 20 THEN DATE_ADD(DATE_ADD(CURRENT_DATE, INTERVAL -1 MONTH), INTERVAL -(DAY(CURRENT_DATE)-1) DAY)  ELSE DATE_ADD(DATE_ADD(CURRENT_DATE, INTERVAL -2 MONTH), INTERVAL -(DAY(CURRENT_DATE)-1) DAY) END
AND anio = YEAR(CASE WHEN DAY(CURRENT_DATE) >= 20 THEN DATE_ADD(DATE_ADD(CURRENT_DATE, INTERVAL -1 MONTH), INTERVAL -(DAY(CURRENT_DATE)-1) DAY)  ELSE DATE_ADD(DATE_ADD(CURRENT_DATE, INTERVAL -2 MONTH), INTERVAL -(DAY(CURRENT_DATE)-1) DAY) END)
group by 1,2,3

UNION ALL

SELECT NMes3L,Anio,fecha_mes,
CASE WHEN AVG(per_moros*100) > 30 THEN 75 - ABS(AVG(per_moros*100) -30) * (CASE WHEN AVG(per_moros*100) > 30 THEN 75/67 ELSE 25/13 END) ELSE
75 + ABS(AVG(per_moros*100) -30) * (CASE WHEN AVG(per_moros*100) > 30 THEN 75/67 ELSE 25/13 END) END prom_trib

FROM tbl_morosidad a
INNER JOIN dim_tiempo d ON fecha_Eval = d.fecha
INNER JOIN tbl_contrato c ON c.contrato_id = a.contrato_id
WHERE 
c.entry_by_access = '.Auth::user()->id.' and
fecha_mes <= CASE WHEN DAY(CURRENT_DATE) >= 20 THEN DATE_ADD(DATE_ADD(CURRENT_DATE, INTERVAL -1 MONTH), INTERVAL -(DAY(CURRENT_DATE)-1) DAY)  ELSE DATE_ADD(DATE_ADD(CURRENT_DATE, INTERVAL -2 MONTH), INTERVAL -(DAY(CURRENT_DATE)-1) DAY) END
AND anio = YEAR(CASE WHEN DAY(CURRENT_DATE) >= 20 THEN DATE_ADD(DATE_ADD(CURRENT_DATE, INTERVAL -1 MONTH), INTERVAL -(DAY(CURRENT_DATE)-1) DAY)  ELSE DATE_ADD(DATE_ADD(CURRENT_DATE, INTERVAL -2 MONTH), INTERVAL -(DAY(CURRENT_DATE)-1) DAY) END)
GROUP BY 1,2,3

UNION ALL

SELECT NMes3L,Anio,fecha_mes,(1 -( COUNT(DISTINCT (CASE WHEN tCont_eval = \'MALO\' THEN a.contrato_id ELSE 0 END))/ 
COUNT(DISTINCT a.contrato_id)) )*100
FROM
tbl_contratista_tributario a INNER JOIN tbl_contrato b ON a.contrato_id = b.contrato_id

INNER JOIN dim_tiempo d ON a.tCont_fecha = d.fecha
WHERE 
b.entry_by_access = '.Auth::user()->id.' and
fecha_mes <= CASE WHEN DAY(CURRENT_DATE) >= 20 THEN DATE_ADD(DATE_ADD(CURRENT_DATE, INTERVAL -1 MONTH), INTERVAL -(DAY(CURRENT_DATE)-1) DAY)  ELSE DATE_ADD(DATE_ADD(CURRENT_DATE, INTERVAL -2 MONTH), INTERVAL -(DAY(CURRENT_DATE)-1) DAY) END
AND anio = YEAR(CASE WHEN DAY(CURRENT_DATE) >= 20 THEN DATE_ADD(DATE_ADD(CURRENT_DATE, INTERVAL -1 MONTH), INTERVAL -(DAY(CURRENT_DATE)-1) DAY)  ELSE DATE_ADD(DATE_ADD(CURRENT_DATE, INTERVAL -2 MONTH), INTERVAL -(DAY(CURRENT_DATE)-1) DAY) END)
GROUP BY 1,2,3

UNION ALL

SELECT  NMes3L,Anio,fecha_mes,AVG((total_fondos + prendas)/(mutuos + total_impacto))*100
FROM tbl_garantias a INNER JOIN dim_tiempo b
ON a.fecha = b.fecha
INNER JOIN tbl_contrato c ON c.contrato_id = a.contrato_id
WHERE  
c.entry_by_access = '.Auth::user()->id.' and
fecha_mes <= CASE WHEN DAY(CURRENT_DATE) >= 20 THEN DATE_ADD(DATE_ADD(CURRENT_DATE, INTERVAL -1 MONTH), INTERVAL -(DAY(CURRENT_DATE)-1) DAY)  ELSE DATE_ADD(DATE_ADD(CURRENT_DATE, INTERVAL -2 MONTH), INTERVAL -(DAY(CURRENT_DATE)-1) DAY) END 
AND anio = YEAR(CASE WHEN DAY(CURRENT_DATE) >= 20 THEN DATE_ADD(DATE_ADD(CURRENT_DATE, INTERVAL -1 MONTH), INTERVAL -(DAY(CURRENT_DATE)-1) DAY)  ELSE DATE_ADD(DATE_ADD(CURRENT_DATE, INTERVAL -2 MONTH), INTERVAL -(DAY(CURRENT_DATE)-1) DAY) END)
GROUP BY 1,2,3
) finan
group by 1,2

UNION ALL

-- laboral

SELECT fecha_mes fecha_ambito,3,CONCAT(CONCAT(NMes3L,\'-\'),Anio) fecha,0,0,CAST(COALESCE(AVG(prom_lab),0) AS UNSIGNED) l,0
FROM
(SELECT NMes3L,Anio,fecha_mes,
CASE WHEN AVG(valor)*100 > 16 THEN 75 - (ABS(AVG(valor)*100 -16)) * (CASE WHEN AVG(valor)*100 > 16 THEN 75/84 ELSE 25/16 END) ELSE
75 + (ABS(AVG(valor)*100 -16)) * (CASE WHEN AVG(valor)*100 > 16 THEN 75/84 ELSE 25/16 END) END prom_lab
FROM tbl_obliglab_repo a INNER JOIN dim_tiempo b
ON a.fecha = b.fecha
INNER JOIN tbl_contrato c ON c.contrato_id = a.contrato_id
WHERE 
c.entry_by_access = '.Auth::user()->id.' and
fecha_mes <= CASE WHEN DAY(CURRENT_DATE) >= 20 THEN DATE_ADD(DATE_ADD(CURRENT_DATE, INTERVAL -1 MONTH), INTERVAL -(DAY(CURRENT_DATE)-1) DAY)  ELSE DATE_ADD(DATE_ADD(CURRENT_DATE, INTERVAL -2 MONTH), INTERVAL -(DAY(CURRENT_DATE)-1) DAY) END 
AND anio = YEAR(CASE WHEN DAY(CURRENT_DATE) >= 20 THEN DATE_ADD(DATE_ADD(CURRENT_DATE, INTERVAL -1 MONTH), INTERVAL -(DAY(CURRENT_DATE)-1) DAY)  ELSE DATE_ADD(DATE_ADD(CURRENT_DATE, INTERVAL -2 MONTH), INTERVAL -(DAY(CURRENT_DATE)-1) DAY) END)
GROUP BY 1,2,3
UNION ALL

SELECT  NMes3L,Anio,fecha_mes,((SELECT COUNT(DISTINCT contrato_id) FROM tbl_contrato WHERE cont_fechaFin > CURRENT_DATE) - 
COUNT(DISTINCT a.contrato_id))/(SELECT COUNT(DISTINCT contrato_id) FROM tbl_contrato WHERE cont_fechaFin > CURRENT_DATE) * 100
FROM tbl_contratistacondicionfin a
INNER JOIN dim_tiempo b
ON a.finCont_fecha = b.fecha
INNER JOIN tbl_contrato c ON c.contrato_id = a.contrato_id
WHERE c.entry_by_access = '.Auth::user()->id.' and
fecha_mes <= CASE WHEN DAY(CURRENT_DATE) >= 20 THEN DATE_ADD(DATE_ADD(CURRENT_DATE, INTERVAL -1 MONTH), INTERVAL -(DAY(CURRENT_DATE)-1) DAY)  ELSE DATE_ADD(DATE_ADD(CURRENT_DATE, INTERVAL -2 MONTH), INTERVAL -(DAY(CURRENT_DATE)-1) DAY) END 
AND anio = YEAR(CASE WHEN DAY(CURRENT_DATE) >= 20 THEN DATE_ADD(DATE_ADD(CURRENT_DATE, INTERVAL -1 MONTH), INTERVAL -(DAY(CURRENT_DATE)-1) DAY)  ELSE DATE_ADD(DATE_ADD(CURRENT_DATE, INTERVAL -2 MONTH), INTERVAL -(DAY(CURRENT_DATE)-1) DAY) END)
GROUP BY 1,2,3
UNION ALL

SELECT  NMes3L,Anio,fecha_mes,CAST(AVG((PERC_LAB)) AS DECIMAL(4,1)) puntaje
FROM tbl_fiscalización a INNER JOIN dim_tiempo b
ON a.fecha = b.fecha
INNER JOIN tbl_contrato c ON c.contrato_id = a.contrato_id
WHERE c.entry_by_access = '.Auth::user()->id.' and
fecha_mes <= CASE WHEN DAY(CURRENT_DATE) >= 20 THEN DATE_ADD(DATE_ADD(CURRENT_DATE, INTERVAL -1 MONTH), INTERVAL -(DAY(CURRENT_DATE)-1) DAY)  ELSE DATE_ADD(DATE_ADD(CURRENT_DATE, INTERVAL -2 MONTH), INTERVAL -(DAY(CURRENT_DATE)-1) DAY) END 
AND anio = YEAR(CASE WHEN DAY(CURRENT_DATE) >= 20 THEN DATE_ADD(DATE_ADD(CURRENT_DATE, INTERVAL -1 MONTH), INTERVAL -(DAY(CURRENT_DATE)-1) DAY)  ELSE DATE_ADD(DATE_ADD(CURRENT_DATE, INTERVAL -2 MONTH), INTERVAL -(DAY(CURRENT_DATE)-1) DAY) END)
GROUP BY 1,2,3
) lab 
group by 1,2

UNION ALL

-- seguridad
SELECT fecha_mes fecha_ambito,4,CONCAT(CONCAT(NMes3L,\'-\'),Anio) fecha,0,0,0, CAST(COALESCE(AVG(prom_seg),0) AS DECIMAL(3,0)) s
FROM
(SELECT NMes3L,Anio,fecha_mes,
CASE WHEN (SUM(n_accid)/SUM(hh)*200000)> 5.2 THEN 0 ELSE
CASE WHEN (SUM(n_accid)/SUM(hh)*200000)> 2.6 THEN 75 - ABS((SUM(n_accid)/SUM(hh)*200000)-2.6) * (CASE WHEN (SUM(n_accid)/SUM(hh)*200000) > 2.6 THEN 75/2.6 ELSE 25/2.6 END) ELSE
75 + ABS((SUM(n_accid)/SUM(hh)*200000) -2.6) * (CASE WHEN (SUM(n_accid)/SUM(hh)*200000) > 2.6 THEN 75/2.6 ELSE 25/2.6 END) END
END prom_seg
FROM tbl_accidentes a
INNER JOIN dim_tiempo d ON a.fecha_informe = d.fecha
INNER JOIN tbl_contrato c ON a.contrato_id = c.contrato_id
WHERE 
c.entry_by_access = '.Auth::user()->id.' and
fecha_mes <= CASE WHEN DAY(CURRENT_DATE) >= 20 THEN DATE_ADD(DATE_ADD(CURRENT_DATE, INTERVAL -1 MONTH), INTERVAL -(DAY(CURRENT_DATE)-1) DAY)  ELSE DATE_ADD(DATE_ADD(CURRENT_DATE, INTERVAL -2 MONTH), INTERVAL -(DAY(CURRENT_DATE)-1) DAY) END 
AND anio = YEAR(CASE WHEN DAY(CURRENT_DATE) >= 20 THEN DATE_ADD(DATE_ADD(CURRENT_DATE, INTERVAL -1 MONTH), INTERVAL -(DAY(CURRENT_DATE)-1) DAY)  ELSE DATE_ADD(DATE_ADD(CURRENT_DATE, INTERVAL -2 MONTH), INTERVAL -(DAY(CURRENT_DATE)-1) DAY) END)
GROUP BY 1,2,3
UNION ALL

SELECT NMes3L,Anio,fecha_mes,
CASE WHEN (SUM(DIAS_PERD)/SUM(hh)*200000) > 60 THEN 0 ELSE 
CASE WHEN (SUM(DIAS_PERD)/SUM(hh)*200000)> 31.1 THEN 75 - ABS(( SUM(DIAS_PERD)/SUM(hh)*200000 )-31.1) * (CASE WHEN (SUM(DIAS_PERD)/SUM(hh)*200000) > 31.1 THEN 75/31.1 ELSE 25/31.1 END) ELSE
75 + ABS((SUM(DIAS_PERD)/SUM(hh)*200000) -31.1) * (CASE WHEN (SUM(DIAS_PERD)/SUM(hh)*200000) > 31.1 THEN 75/31.1 ELSE 25/31.1 END) END 
END f
FROM tbl_accidentes a
INNER JOIN dim_tiempo d ON a.fecha_informe = d.fecha
INNER JOIN tbl_contrato c ON a.contrato_id = c.contrato_id
WHERE 
c.entry_by_access = '.Auth::user()->id.' and
fecha_mes <= CASE WHEN DAY(CURRENT_DATE) >= 20 THEN DATE_ADD(DATE_ADD(CURRENT_DATE, INTERVAL -1 MONTH), INTERVAL -(DAY(CURRENT_DATE)-1) DAY)  ELSE DATE_ADD(DATE_ADD(CURRENT_DATE, INTERVAL -2 MONTH), INTERVAL -(DAY(CURRENT_DATE)-1) DAY) END 
AND anio = YEAR(CASE WHEN DAY(CURRENT_DATE) >= 20 THEN DATE_ADD(DATE_ADD(CURRENT_DATE, INTERVAL -1 MONTH), INTERVAL -(DAY(CURRENT_DATE)-1) DAY)  ELSE DATE_ADD(DATE_ADD(CURRENT_DATE, INTERVAL -2 MONTH), INTERVAL -(DAY(CURRENT_DATE)-1) DAY) END)
GROUP BY 1,2,3
UNION ALL

SELECT NMes3L,Anio,fecha_mes,AVG((PERC_COND + perc_equip)/2) 
FROM tbl_fiscalización a
INNER JOIN dim_tiempo d ON a.fecha = d.fecha
INNER JOIN tbl_contrato c ON a.contrato_id = c.contrato_id
WHERE 
c.entry_by_access = '.Auth::user()->id.' and
fecha_mes <= CASE WHEN DAY(CURRENT_DATE) >= 20 THEN DATE_ADD(DATE_ADD(CURRENT_DATE, INTERVAL -1 MONTH), INTERVAL -(DAY(CURRENT_DATE)-1) DAY)  ELSE DATE_ADD(DATE_ADD(CURRENT_DATE, INTERVAL -2 MONTH), INTERVAL -(DAY(CURRENT_DATE)-1) DAY) END 
AND anio = YEAR(CASE WHEN DAY(CURRENT_DATE) >= 20 THEN DATE_ADD(DATE_ADD(CURRENT_DATE, INTERVAL -1 MONTH), INTERVAL -(DAY(CURRENT_DATE)-1) DAY)  ELSE DATE_ADD(DATE_ADD(CURRENT_DATE, INTERVAL -2 MONTH), INTERVAL -(DAY(CURRENT_DATE)-1) DAY) END)
GROUP BY 1,2,3
) seg 
GROUP BY 1,2
) panel
group by 1,2
                            ';
                    $rawdata = array();
                    $i=0;
                    
                    foreach ($conn2->query($sqlgr) as $row1) {
                           
                          $rawdata[$i] = $row1;
                          $i++;
                    }
                    
                 ?>                 
                
  <script type="text/javascript">
                        $(function () {
                                $('#grafico').highcharts({
                                    title: {
                                        text: '',
                                        x: -20 //center
                                    },
                                    credits:{
                                        text:"Sourcing",
                                        href:"http://www.sourcing.cl"
                                    },
                                    xAxis: {
                                        categories: [
                                        <?php
                                                for($i = 0 ;$i<count($rawdata);$i++){
                                                    if($i==count($rawdata))
                                                        echo "'".$rawdata[$i]["fecha"]."'";
                                                    else
                                                        echo "'".$rawdata[$i]["fecha"]."',";
                                                }       
                                                ?>
                                                    
                                        ]
                                    },
                                    yAxis: {
                                        title: {
                                            text: 'Cumplimiento %'
                                        },
                                        plotLines: [{
                                            value: 0,
                                            width: 1,
                                            color: '#808080'
                                        }]
                                    },
                                    tooltip: {
                                        valueSuffix: '%'
                                    },
                                    legend: {
                                        layout: 'vertical',
                                        align: 'right',
                                        verticalAlign: 'middle',
                                        borderWidth: 0
                                    },
                                     exporting: {
                                        enabled: true
                                    },
                                    series: [{
                                        name: 'Negocio',
                                        data: (function() {
                                                var data = [];
                                                    <?php
                                                        for($i = 0 ;$i<count($rawdata);$i++){
                                                    ?>
                                                    data.push([<?php echo $rawdata[$i]["Negocio"];?>]);
                                                    <?php } ?>
                                                return data;
                                                })()
                                    }, {
                                        name: 'Financiero',
                                        data: (function() {
                                                var data = [];
                                                    <?php
                                                        for($i = 0 ;$i<count($rawdata);$i++){
                                                    ?>
                                                    data.push([<?php echo $rawdata[$i]["Financiero"];?>]);
                                                    <?php } ?>
                                                return data;
                                                })()
                                    }, {
                                        name: 'Entorno Laboral',
                                         data: (function() {
                                                var data = [];
                                                    <?php
                                                        for($i = 0 ;$i<count($rawdata);$i++){
                                                    ?>
                                                    data.push([<?php echo $rawdata[$i]["Laboral"];?>]);
                                                    <?php } ?>
                                                return data;
                                                })()
                                    }, {
                                        name: 'Seguridad',
                                         data: (function() {
                                                var data = [];
                                                    <?php
                                                        for($i = 0 ;$i<count($rawdata);$i++){
                                                    ?>
                                                    data.push([<?php echo $rawdata[$i]["seguridad"];?>]);
                                                    <?php } ?>
                                                return data;
                                                })()
                                    }]
                                });
                            });

            </script> 

            
                                        
<?php
                  $sqlgr = 'SELECT  CAST(SUM(n) AS UNSIGNED) Negocio,
(SELECT ambito_nombre FROM tbl_planes_y_programas_ambito WHERE ambito_id = 1) amb_neg,
CAST(SUM(f) AS UNSIGNED) Financiero ,
(SELECT ambito_nombre FROM tbl_planes_y_programas_ambito WHERE ambito_id = 2) amb_fin,
CAST(SUM(l) AS UNSIGNED) Laboral, 
(SELECT ambito_nombre FROM tbl_planes_y_programas_ambito WHERE ambito_id = 3) amb_lab,
CAST(SUM(s) AS UNSIGNED) seguridad,  
(SELECT ambito_nombre FROM tbl_planes_y_programas_ambito WHERE ambito_id = 4) amb_seg,
CAST(AVG(n+f+l+s) AS UNSIGNED) general
FROM
(
SELECT CAST(COALESCE(AVG(prom_neg),0) AS UNSIGNED) n,0 f,0 l ,0 s
FROM
(
SELECT CAST(AVG(kpi) AS DECIMAL(4,1)) prom_neg

FROM
(SELECT  b.contrato_id,kpiDet_fecha,  (SUM(CASE WHEN kpi_tipo_calc = 1 THEN CASE WHEN kpiDet_puntaje >= kpiDet_meta THEN 1 ELSE 0 END WHEN kpi_tipo_calc = 2 THEN CASE WHEN kpiDet_puntaje <= kpiDet_meta THEN 1 ELSE 0 END WHEN kpi_tipo_calc = 3 THEN CASE WHEN kpiDet_puntaje BETWEEN kpiDet_min AND kpiDet_max THEN 1 ELSE 0 END WHEN kpi_tipo_calc = 4 THEN 1 END )/
COUNT(DISTINCT a.id_kpi) )*100 kpi
FROM tbl_kpimensual a INNER JOIN tbl_kpigral b
ON a.id_kpi = b.id_kpi
INNER JOIN dim_tiempo ON kpiDet_fecha = fecha
INNER JOIN tbl_contrato c ON c.contrato_id = b.contrato_id
WHERE kpiDet_puntaje IS NOT NULL
and c.entry_by_access = '.Auth::user()->id.'
AND 
fecha_mes =  CASE WHEN DAY(CURRENT_DATE) >= 20 THEN DATE_ADD(DATE_ADD(CURRENT_DATE, INTERVAL -1 MONTH), INTERVAL -(DAY(CURRENT_DATE)-1) DAY)  ELSE DATE_ADD(DATE_ADD(CURRENT_DATE, INTERVAL -2 MONTH), INTERVAL -(DAY(CURRENT_DATE)-1) DAY) END
GROUP BY 1,2) asd

UNION ALL

SELECT
CASE WHEN AVG(eval_puntaje) >70 THEN 75 + ((CASE WHEN AVG(eval_puntaje) >70 THEN 25/30 ELSE 75/70 END)*(AVG(eval_puntaje) - 70)) 
ELSE 75 - ((CASE WHEN AVG(eval_puntaje) >70 THEN 25/30 ELSE 75/70 END)*(AVG(eval_puntaje) - 70)) END f
FROM tbl_evalcontratistas a INNER JOIN dim_tiempo 
ON eval_fecha = fecha 
INNER JOIN tbl_contrato b ON a.contrato_id = b.contrato_id
WHERE  
b.entry_by_access = '.Auth::user()->id.' and
eval_puntaje IS NOT NULL
AND 
fecha_mes IN (SELECT DISTINCT fecha_mes FROM dim_tiempo WHERE semestre  = (SELECT DISTINCT semestre FROM dim_tiempo WHERE fecha_mes =  CASE WHEN DAY(CURRENT_DATE) >= 20 THEN DATE_ADD(DATE_ADD(CURRENT_DATE, INTERVAL -1 MONTH), INTERVAL -(DAY(CURRENT_DATE)-1) DAY)  ELSE DATE_ADD(DATE_ADD(CURRENT_DATE, INTERVAL -2 MONTH), INTERVAL -(DAY(CURRENT_DATE)-1) DAY) END) AND Anio =YEAR(CASE WHEN DAY(CURRENT_DATE) >= 20 THEN DATE_ADD(DATE_ADD(CURRENT_DATE, INTERVAL -1 MONTH), INTERVAL -(DAY(CURRENT_DATE)-1) DAY)  ELSE DATE_ADD(DATE_ADD(CURRENT_DATE, INTERVAL -2 MONTH), INTERVAL -(DAY(CURRENT_DATE)-1) DAY) END))

UNION ALL

SELECT CAST(AVG(resultado) AS DECIMAL(4,1)) puntaje
FROM tbl_calidad_adm_cont a INNER JOIN dim_tiempo 
ON fec_rev = fecha
INNER JOIN tbl_contrato b ON a.contrato_id = b.contrato_id
WHERE resultado IS NOT NULL
AND b.entry_by_access = '.Auth::user()->id.' and
fecha_mes IN (SELECT DISTINCT fecha_mes FROM dim_tiempo WHERE trimestre  = (SELECT DISTINCT trimestre FROM dim_tiempo WHERE fecha_mes =  CASE WHEN DAY(CURRENT_DATE) >= 20 THEN DATE_ADD(DATE_ADD(CURRENT_DATE, INTERVAL -1 MONTH), INTERVAL -(DAY(CURRENT_DATE)-1) DAY)  ELSE DATE_ADD(DATE_ADD(CURRENT_DATE, INTERVAL -2 MONTH), INTERVAL -(DAY(CURRENT_DATE)-1) DAY) END) AND Anio =YEAR(CASE WHEN DAY(CURRENT_DATE) >= 20 THEN DATE_ADD(DATE_ADD(CURRENT_DATE, INTERVAL -1 MONTH), INTERVAL -(DAY(CURRENT_DATE)-1) DAY)  ELSE DATE_ADD(DATE_ADD(CURRENT_DATE, INTERVAL -2 MONTH), INTERVAL -(DAY(CURRENT_DATE)-1) DAY) END))
) ambito

UNION ALL 

-- financiero

SELECT 0, CAST(COALESCE(AVG(prom_fin),0) AS UNSIGNED) f ,0,0
FROM
(SELECT 
CASE WHEN AVG(valor) > 50 THEN 75 - (AVG(valor) -50) * (CASE WHEN AVG(valor) > 50 THEN 75/50 ELSE 25/50 END) ELSE
75 + (AVG(valor) -50) * (CASE WHEN AVG(valor) > 50 THEN 75/50 ELSE 25/50 END) END prom_fin
FROM tbl_eval_financiera a
INNER JOIN tbl_contrato c ON c.contrato_id = a.contrato_id
INNER JOIN dim_tiempo d ON fecha_validez = d.fecha
WHERE
c.entry_by_access = '.Auth::user()->id.' and
fecha_mes =  CASE WHEN DAY(CURRENT_DATE) >= 20 THEN DATE_ADD(DATE_ADD(CURRENT_DATE, INTERVAL -1 MONTH), INTERVAL -(DAY(CURRENT_DATE)-1) DAY)  ELSE DATE_ADD(DATE_ADD(CURRENT_DATE, INTERVAL -2 MONTH), INTERVAL -(DAY(CURRENT_DATE)-1) DAY) END

UNION ALL

SELECT 
CASE WHEN AVG(per_moros*100) > 30 THEN 75 - ABS(AVG(per_moros*100) -30) * (CASE WHEN AVG(per_moros*100) > 30 THEN 75/67 ELSE 25/13 END) ELSE
75 + ABS(AVG(per_moros*100) -30) * (CASE WHEN AVG(per_moros*100) > 30 THEN 75/67 ELSE 25/13 END) END prom_trib
FROM tbl_morosidad a
INNER JOIN dim_tiempo d ON fecha_Eval = d.fecha
INNER JOIN tbl_contrato c ON c.contrato_id = a.contrato_id
WHERE 
c.entry_by_access = '.Auth::user()->id.' and
fecha_mes =  CASE WHEN DAY(CURRENT_DATE) >= 20 THEN DATE_ADD(DATE_ADD(CURRENT_DATE, INTERVAL -1 MONTH), INTERVAL -(DAY(CURRENT_DATE)-1) DAY)  ELSE DATE_ADD(DATE_ADD(CURRENT_DATE, INTERVAL -2 MONTH), INTERVAL -(DAY(CURRENT_DATE)-1) DAY) END

UNION ALL

SELECT (1 -( COUNT(DISTINCT (CASE WHEN tCont_eval = \'MALO\' THEN a.contrato_id ELSE 0 END))/ 
COUNT(DISTINCT a.contrato_id)) )*100
FROM
tbl_contratista_tributario a INNER JOIN tbl_contrato b ON a.contrato_id = b.contrato_id

INNER JOIN dim_tiempo d ON a.tCont_fecha = d.fecha
WHERE 
b.entry_by_access = '.Auth::user()->id.' and
fecha_mes =  CASE WHEN DAY(CURRENT_DATE) >= 20 THEN DATE_ADD(DATE_ADD(CURRENT_DATE, INTERVAL -1 MONTH), INTERVAL -(DAY(CURRENT_DATE)-1) DAY)  ELSE DATE_ADD(DATE_ADD(CURRENT_DATE, INTERVAL -2 MONTH), INTERVAL -(DAY(CURRENT_DATE)-1) DAY) END

UNION ALL

SELECT  AVG((total_fondos + prendas)/(mutuos + total_impacto))*100
FROM tbl_garantias a INNER JOIN dim_tiempo b
ON a.fecha = b.fecha
INNER JOIN tbl_contrato c ON c.contrato_id = a.contrato_id
WHERE  
c.entry_by_access = '.Auth::user()->id.' and
fecha_mes =  CASE WHEN DAY(CURRENT_DATE) >= 20 THEN DATE_ADD(DATE_ADD(CURRENT_DATE, INTERVAL -1 MONTH), INTERVAL -(DAY(CURRENT_DATE)-1) DAY)  ELSE DATE_ADD(DATE_ADD(CURRENT_DATE, INTERVAL -2 MONTH), INTERVAL -(DAY(CURRENT_DATE)-1) DAY) END
) finan

UNION ALL

-- laboral

SELECT 0,0,CAST(COALESCE(AVG(prom_lab),0) AS UNSIGNED) l,0
FROM
(SELECT 
CASE WHEN AVG(valor)*100 > 16 THEN 75 - (ABS(AVG(valor)*100 -16)) * (CASE WHEN AVG(valor)*100 > 16 THEN 75/84 ELSE 25/16 END) ELSE
75 + (ABS(AVG(valor)*100 -16)) * (CASE WHEN AVG(valor)*100 > 16 THEN 75/84 ELSE 25/16 END) END prom_lab
FROM tbl_obliglab_repo a INNER JOIN dim_tiempo b
ON a.fecha = b.fecha
INNER JOIN tbl_contrato c ON c.contrato_id = a.contrato_id
WHERE 
c.entry_by_access = '.Auth::user()->id.' and
fecha_mes =  CASE WHEN DAY(CURRENT_DATE) >= 20 THEN DATE_ADD(DATE_ADD(CURRENT_DATE, INTERVAL -1 MONTH), INTERVAL -(DAY(CURRENT_DATE)-1) DAY)  ELSE DATE_ADD(DATE_ADD(CURRENT_DATE, INTERVAL -2 MONTH), INTERVAL -(DAY(CURRENT_DATE)-1) DAY) END

UNION ALL

SELECT  ((SELECT COUNT(DISTINCT contrato_id) FROM tbl_contrato WHERE cont_fechaFin > CURRENT_DATE) - 
COUNT(DISTINCT a.contrato_id))/(SELECT COUNT(DISTINCT contrato_id) FROM tbl_contrato WHERE cont_fechaFin > CURRENT_DATE) * 100
FROM tbl_contratistacondicionfin a
INNER JOIN dim_tiempo b
ON a.finCont_fecha = b.fecha
INNER JOIN tbl_contrato c ON c.contrato_id = a.contrato_id
WHERE c.entry_by_access = '.Auth::user()->id.' and 
fecha_mes =  CASE WHEN DAY(CURRENT_DATE) >= 20 THEN DATE_ADD(DATE_ADD(CURRENT_DATE, INTERVAL -1 MONTH), INTERVAL -(DAY(CURRENT_DATE)-1) DAY)  ELSE DATE_ADD(DATE_ADD(CURRENT_DATE, INTERVAL -2 MONTH), INTERVAL -(DAY(CURRENT_DATE)-1) DAY) END

UNION ALL

SELECT  CAST(AVG((PERC_LAB)) AS DECIMAL(4,1)) puntaje
FROM tbl_fiscalización a INNER JOIN dim_tiempo b
ON a.fecha = b.fecha
INNER JOIN tbl_contrato c ON c.contrato_id = a.contrato_id
WHERE c.entry_by_access = '.Auth::user()->id.' and 
fecha_mes =  CASE WHEN DAY(CURRENT_DATE) >= 20 THEN DATE_ADD(DATE_ADD(CURRENT_DATE, INTERVAL -1 MONTH), INTERVAL -(DAY(CURRENT_DATE)-1) DAY)  ELSE DATE_ADD(DATE_ADD(CURRENT_DATE, INTERVAL -2 MONTH), INTERVAL -(DAY(CURRENT_DATE)-1) DAY) END
) lab 

UNION ALL

-- seguridad

SELECT 0,0,0, CAST(COALESCE(AVG(prom_seg),0) AS DECIMAL(3,0)) s
FROM
(SELECT 
CASE WHEN (SUM(n_accid)/SUM(hh)*200000)> 5.2 THEN 0 ELSE
CASE WHEN (SUM(n_accid)/SUM(hh)*200000)> 2.6 THEN 75 - ABS((SUM(n_accid)/SUM(hh)*200000)-2.6) * (CASE WHEN (SUM(n_accid)/SUM(hh)*200000) > 2.6 THEN 75/2.6 ELSE 25/2.6 END) ELSE
75 + ABS((SUM(n_accid)/SUM(hh)*200000) -2.6) * (CASE WHEN (SUM(n_accid)/SUM(hh)*200000) > 2.6 THEN 75/2.6 ELSE 25/2.6 END) END
END prom_seg
FROM tbl_accidentes a
INNER JOIN dim_tiempo d ON a.fecha_informe = d.fecha
INNER JOIN tbl_contrato c ON a.contrato_id = c.contrato_id
WHERE 
c.entry_by_access = '.Auth::user()->id.' and
fecha_mes =  CASE WHEN DAY(CURRENT_DATE) >= 20 THEN DATE_ADD(DATE_ADD(CURRENT_DATE, INTERVAL -1 MONTH), INTERVAL -(DAY(CURRENT_DATE)-1) DAY)  ELSE DATE_ADD(DATE_ADD(CURRENT_DATE, INTERVAL -2 MONTH), INTERVAL -(DAY(CURRENT_DATE)-1) DAY) END

UNION ALL

SELECT 
CASE WHEN (SUM(DIAS_PERD)/SUM(hh)*200000) > 60 THEN 0 ELSE 
CASE WHEN (SUM(DIAS_PERD)/SUM(hh)*200000)> 31.1 THEN 75 - ABS(( SUM(DIAS_PERD)/SUM(hh)*200000 )-31.1) * (CASE WHEN (SUM(DIAS_PERD)/SUM(hh)*200000) > 31.1 THEN 75/31.1 ELSE 25/31.1 END) ELSE
75 + ABS((SUM(DIAS_PERD)/SUM(hh)*200000) -31.1) * (CASE WHEN (SUM(DIAS_PERD)/SUM(hh)*200000) > 31.1 THEN 75/31.1 ELSE 25/31.1 END) END 
END f
FROM tbl_accidentes a
INNER JOIN dim_tiempo d ON a.fecha_informe = d.fecha
INNER JOIN tbl_contrato c ON a.contrato_id = c.contrato_id
WHERE 
c.entry_by_access = '.Auth::user()->id.' and
fecha_mes =  CASE WHEN DAY(CURRENT_DATE) >= 20 THEN DATE_ADD(DATE_ADD(CURRENT_DATE, INTERVAL -1 MONTH), INTERVAL -(DAY(CURRENT_DATE)-1) DAY)  ELSE DATE_ADD(DATE_ADD(CURRENT_DATE, INTERVAL -2 MONTH), INTERVAL -(DAY(CURRENT_DATE)-1) DAY) END

UNION ALL

SELECT AVG((PERC_COND + perc_equip)/2) 
FROM tbl_fiscalización a
INNER JOIN dim_tiempo d ON a.fecha = d.fecha
INNER JOIN tbl_contrato c ON a.contrato_id = c.contrato_id
 WHERE 
 c.entry_by_access = '.Auth::user()->id.' and
fecha_mes =  CASE WHEN DAY(CURRENT_DATE) >= 20 THEN DATE_ADD(DATE_ADD(CURRENT_DATE, INTERVAL -1 MONTH), INTERVAL -(DAY(CURRENT_DATE)-1) DAY)  ELSE DATE_ADD(DATE_ADD(CURRENT_DATE, INTERVAL -2 MONTH), INTERVAL -(DAY(CURRENT_DATE)-1) DAY) END
) seg
) panel
                            ';
                    $rawgraf = array();
                    $i=0;
                    
                    foreach ($conn2->query($sqlgr) as $row1) {
                           
                          $rawgraf = $row1;
                          $i++;
                    }
                    
                 ?> 
                 
<script type="text/javascript">
    $(function () {
    
   var color_v = '#3d9b35';
   var color_r = '#e64427';
   var color_n = '#FFC000';
    $('#container').highcharts({
        chart: {
            backgroundColor: 'white',
            credits:{
                    text:"Sourcing",
                    href:"http://www.sourcing.cl",
            },
            events: {
                load: function () {
                    
                    // Dibujar el grafico
                    var ren = this.renderer;
                   
                    var c_barra1 = color_r,c_barra2 = color_r,c_barra3 = color_r,c_barra4 = color_r,c_circ = color_r;
                  // crea color de barras y circulo
                  <?php
                    if($rawgraf['Negocio']>=75){
                    
                        echo "c_barra1 = color_v;";
                    }
                    if($rawgraf['Financiero']>=75){
                    
                        echo "c_barra2 = color_v;";
                    }
                    if($rawgraf['Laboral']>=75){
                    
                        echo "c_barra3 = color_v;";
                    }
                    if($rawgraf['seguridad']>=75){
                    
                        echo "c_barra4 = color_v;";
                    }
                    if($rawgraf['general']>=75){
                    
                        echo "c_circ = color_v;";
                    }
                    
           ?>
                    //crea cuadros de ambito
                    ren.rect(10,10,320,45,10)
                    .css({
                        fill: c_barra1,
                        stroke: 'white',
                        strokeWidth: '2'
                        })
                    .add();
                    
                    ren.rect(10,55,320,45,10)
                    .css({
                        fill: c_barra2,
                        stroke: 'white',
                        strokeWidth: '2'
                        })
                    .add();
                    
                    ren.rect(10,100,320,45,10)
                    .css({
                        fill: c_barra3,
                        stroke: 'white',
                        strokeWidth: '2'
                        })
                    .add();
                       
                    ren.rect(10,145,320,45,10)
                    .css({
                        fill: c_barra4,
                        stroke: 'white',
                        strokeWidth: '2'
                        })
                    .add();

                    //crea circulo de porcentaje
                    ren.circle(330,100,90)
                    .css({
                        fill: c_circ,
                        stroke: 'white',
                        strokeWidth: '2'
                    })
                    .add();
                    
                    //footer grafico 
                    ren.rect(10,200,150,30,0)
                    .css({
                        fill: 'white',
                        stroke: 'black',
                        strokeWidth: '2'
                    })
                    .add();
                    
                    ren.circle(65,215,7)
                    .css({
                        fill: color_r,
                        stroke: 'white',
                        strokeWidth: '2'
                    })
                    .add();
                    
                    ren.circle(140,215,7)
                    .css({
                        fill: color_v,
                        stroke: 'white',
                        strokeWidth: '2'
                    })
                    .add();
                    
            
                    
                    ren.label('75%<',15,205).add();
                    ren.label('75%>=',85,205).add();
                    
                    ren.label('%Cumplimiento [0%-100%]',260,200)
                    .css({
                        fontSize:'12px',
                        fontWeight: 'bold',
                        fontStyle: 'italic'
                    }).add();
                    
                    //labels de datos
                    ren.label(<?php echo $rawgraf['general'] ?>+'%',280,70)
                    .css({
                        fontWeight: 'bold',
                        color: 'white',
                        fontSize: '40px',
                        width: '180px'
                    })
                    .add();
                    
                    ren.label('<?php echo $rawgraf['amb_neg'] ?> '+'<br><center>' + <?php echo $rawgraf['Negocio'] ?> +'%',70,10, null, null, null, true)
                    
                    .css({
                        fontWeight: 'bold',
                        color: 'white',
                        fontSize:'11px',
                        width: '180px'
                    })
                    .add();
                    
                    ren.label('<?php echo $rawgraf['amb_fin'] ?> '+'<br><center>' + <?php echo $rawgraf['Financiero'] ?> +'%',75,55, null, null, null, true)
                    .css({
                        fontWeight: 'bold',
                        color: 'white',
                        fontSize:'11px',
                        width: '180px'
                    })
                    .add();
                    
                    ren.label('AMBITO DEL ENTORNO LABORAL'+'<br><center>' + <?php echo $rawgraf['Laboral'] ?> +'%',40,100, null, null, null, true)
                    .css({
                        fontWeight: 'bold',
                        color: 'white',
                        fontSize:'11px',
                        width: '200px'
                    })
                    .add();
                    
                    ren.label('<?php echo $rawgraf['amb_seg'] ?> '+'<br><center>' + <?php echo $rawgraf['seguridad'] ?> +'%',20,145, null, null, null, true)
                    .css({
                        color: 'white',
                        fontWeight: 'bold',
                        textAlign: 'center',
                        fontSize: '11px',
                        width: '300px'
                    })
                    .add();
                    
            
                    
                    
                }
            }
        },
         title:{text:''},
         credits: {
            enabled: false
        },

  /*       exporting:{
             buttons:{
                 contextButton: {
                        align: 'left',
                        height:25,
                        symbol:'circle',
                        text: 'Descarga'
                 }
            }
         }*/
         
      
    });
} );
        
 </script> 
    
    @endif

 @stop
