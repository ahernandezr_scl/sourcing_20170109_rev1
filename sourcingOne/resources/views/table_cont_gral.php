<?php  
  if (isset($_GET["year"])) {
    $year = $_GET["year"];
   }
  else{  
    $year = 2016;
}
?>

<header>
  <style type="text/css">
.circle_green
    {       
       padding: 7px 4px;
  display: table-cell;
  text-align: center;
  vertical-align: middle;
  border-radius: 100%;
  background: #3d9b35;
  font-size: 10px;
  color: white;
        
    }
    
.circle_red
{       
padding: 7px 4px;
  display: table-cell;
  text-align: center;
  vertical-align: middle;
  border-radius: 100%;
  background: #e64427;
  font-size: 10px;
  color: white;
}

.circle_grey
{       
padding: 7px 7px;
  display: table-cell;
  text-align: center;
  vertical-align: middle;
  border-radius: 100%;
  background: #cec6c0;
  font-size: 10px;
  color: white;
}      

        #embed-container {
                position: relative;
                padding-bottom: 56.25%;
                height: 0;
                overflow: hidden;
        }
        #embed-container iframe {
                position: absolute;
                top: 0;
                left: 0;
                width: 100%;
                height: 100%;
        }

</style>
<script src="http://code.jquery.com/jquery-1.9.0.min.js"></script>
</header>

<body style="background-color:white;">

<div id="tbl_cont">


<?php

$sql_cont = 'SELECT  c.contrato_id,d.fecha,mes,anio,cont_numero,cont_proveedor,
CAST(AVG(n) AS UNSIGNED) general
FROM
(
SELECT contrato_id,fecha,CAST(COALESCE(AVG(prom_neg),0) AS UNSIGNED) n
FROM
(
SELECT contrato_id,fecha,CAST(AVG(kpi) AS DECIMAL(4,1)) prom_neg

FROM
(SELECT  b.contrato_id,kpiDet_fecha fecha,  (SUM(
    CASE WHEN kpi_tipo_calc = 1 THEN
      CASE WHEN kpiDet_puntaje >= kpiDet_meta THEN 1 ELSE 0 END
       WHEN kpi_tipo_calc = 2 THEN
    CASE WHEN kpiDet_puntaje <= kpiDet_meta THEN 1 ELSE 0 END
       WHEN kpi_tipo_calc = 3 THEN
    CASE WHEN kpiDet_puntaje BETWEEN kpiDet_min AND kpiDet_max THEN 1 ELSE 0 END
       WHEN kpi_tipo_calc = 4 THEN 1
      
  END )/
COUNT(DISTINCT a.id_kpi) )*100 kpi
FROM tbl_kpimensual a INNER JOIN tbl_kpigral b
ON a.id_kpi = b.id_kpi
INNER JOIN dim_tiempo ON kpiDet_fecha = fecha
INNER JOIN tbl_contrato c ON c.contrato_id = b.contrato_id
WHERE kpiDet_puntaje IS NOT NULL

GROUP BY 1,2) asd
GROUP BY 1,2
) ambito
GROUP BY 1,2
) panel
INNER JOIN tbl_contrato c ON panel.contrato_id = c.contrato_id
inner join dim_tiempo d on panel.fecha = d.fecha
where anio = '.$year.' and mes =8
GROUP BY 1,2,3,4,5,6;';

                    $rawdata = array();
                    $i=0;
                    
                    foreach ($conn2->query($sql_cont) as $row1) {
                           
                          $rawdata[$i] = $row1;
                          $i=$i+1;
                    }

                    $j=0;
                    $k=0;
                    echo "<table class=\"table table-striped\">";
                    for($j = 0 ;$j<count($rawdata);$j=($j+3)){
                      echo "<tr>";
                      for($k=0;$k<3;$k++){
                        if(($j+$k)<count($rawdata)){
                        if($rawdata[$j+$k]['general'] < 75)
                        {
                           echo "<td> <div class=\"circle_red\">".$rawdata[$j+$k]['general']."%</div></td>";
                        }
                        else if($rawdata[$j+$k]['general'] >= 75)
                        {
                           echo "<td> <div class=\"circle_green\">".$rawdata[$j+$k]['general']."%</div></td>";
                        }
                        else
                        {
                          echo "<td> <div class=\"circle_grey\">".$rawdata[$j+$k]['general']."%</div></td>";
                        }
                         echo "<td> <a href=\"http://andina.sourcing.cl/ctrldetcontrato?id=".$rawdata[$j+$k]['contrato_id']."&mes=".$rawdata[$j+$k]['mes']."&year=".$rawdata[$j+$k]['anio']."\">".$rawdata[$j+$k]['cont_numero']." - ".$rawdata[$j+$k]['cont_proveedor']."</a></td>";
                       }
                      }
                      echo "</tr>";
                    }
                    echo "</table>";





?>
</div>

</body>