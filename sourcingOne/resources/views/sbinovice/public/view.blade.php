<div class="m-t" style="padding-top:25px;">	
    <div class="row m-b-lg animated fadeInDown delayp1 text-center">
        <h3> {{ $pageTitle }} <small> {{ $pageNote }} </small></h3>
        <hr />       
    </div>
</div>
<div class="m-t">
	<div class="table-responsive" > 	

		<table class="table table-striped table-bordered" >
			<tbody>	
		
			
					<tr>
						<td width='30%' class='label-view text-right'>{{ SiteHelpers::activeLang('InvoiceID', (isset($fields['InvoiceID']['language'])? $fields['InvoiceID']['language'] : array())) }}</td>
						<td>{{ $row->InvoiceID}} </td>
						
					</tr>
				
					<tr>
						<td width='30%' class='label-view text-right'>{{ SiteHelpers::activeLang('Number', (isset($fields['Number']['language'])? $fields['Number']['language'] : array())) }}</td>
						<td>{{ $row->Number}} </td>
						
					</tr>
				
					<tr>
						<td width='30%' class='label-view text-right'>{{ SiteHelpers::activeLang('ClientID', (isset($fields['UserID']['language'])? $fields['UserID']['language'] : array())) }}</td>
						<td>{{ SiteHelpers::formatLookUp($row->UserID,'UserID','1:tbl_contratistas:IdContratista:NombreFantasia') }} </td>
						
					</tr>
				
					<tr>
						<td width='30%' class='label-view text-right'>{{ SiteHelpers::activeLang('DateIssued', (isset($fields['DateIssued']['language'])? $fields['DateIssued']['language'] : array())) }}</td>
						<td>{{ $row->DateIssued}} </td>
						
					</tr>
				
					<tr>
						<td width='30%' class='label-view text-right'>{{ SiteHelpers::activeLang('DueDate', (isset($fields['DueDate']['language'])? $fields['DueDate']['language'] : array())) }}</td>
						<td>{{ $row->DueDate}} </td>
						
					</tr>
				
					<tr>
						<td width='30%' class='label-view text-right'>{{ SiteHelpers::activeLang('Amount', (isset($fields['Amount']['language'])? $fields['Amount']['language'] : array())) }}</td>
						<td>{{ $row->Amount}} </td>
						
					</tr>
						
					<tr>
						<td width='30%' class='label-view text-right'></td>
						<td> <a href="javascript:history.go(-1)" class="btn btn-primary"> Back To Grid <a> </td>
						
					</tr>					
				
			</tbody>	
		</table>   

	 
	
	</div>
</div>	