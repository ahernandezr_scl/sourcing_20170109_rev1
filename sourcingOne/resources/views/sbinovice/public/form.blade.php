

		 {!! Form::open(array('url'=>'sbinovice/savepublic', 'class'=>'form-vertical','files' => true , 'parsley-validate'=>'','novalidate'=>' ')) !!}

	@if(Session::has('messagetext'))
	  
		   {!! Session::get('messagetext') !!}
	   
	@endif
	<ul class="parsley-error-list">
		@foreach($errors->all() as $error)
			<li>{{ $error }}</li>
		@endforeach
	</ul>		


<div class="col-md-6">
						<fieldset><legend> Clients Info</legend>
									
									  <div class="form-group  " >
										<label for="ipt" class=" control-label "> Clients  <span class="asterix"> * </span>  </label>									
										  <select name='UserID' rows='5' id='UserID' class='select2 ' required  ></select> 						
									  </div> 					
									  <div class="form-group  " >
										<label for="ipt" class=" control-label "> Amount    </label>									
										  <textarea name='Amount' rows='5' id='Amount' class='form-control '  
				           >{{ $row['Amount'] }}</textarea> 						
									  </div> </fieldset>
			</div>
			
			<div class="col-md-6">
						<fieldset><legend> Invoice Detail</legend>
				{!! Form::hidden('InvoiceID', $row['InvoiceID']) !!}					
									  <div class="form-group  " >
										<label for="ipt" class=" control-label "> Number  <span class="asterix"> * </span>  </label>									
										  {!! Form::text('Number', $row['Number'],array('class'=>'form-control', 'placeholder'=>'', 'required'=>'true'  )) !!} 						
									  </div> 					
									  <div class="form-group  " >
										<label for="ipt" class=" control-label "> Invoice Date  <span class="asterix"> * </span>  </label>									
										  
				<div class="input-group m-b" style="width:150px !important;">
					{!! Form::text('DateIssued', $row['DateIssued'],array('class'=>'form-control date')) !!}
					<span class="input-group-addon"><i class="fa fa-calendar"></i></span>
				</div> 						
									  </div> 					
									  <div class="form-group  " >
										<label for="ipt" class=" control-label "> Due Date  <span class="asterix"> * </span>  </label>									
										  
				<div class="input-group m-b" style="width:150px !important;">
					{!! Form::text('DueDate', $row['DueDate'],array('class'=>'form-control date')) !!}
					<span class="input-group-addon"><i class="fa fa-calendar"></i></span>
				</div> 						
									  </div> </fieldset>
			</div>
			
			

			<div style="clear:both"></div>	
				
					
				  <div class="form-group">
					<label class="col-sm-4 text-right">&nbsp;</label>
					<div class="col-sm-8">	
					<button type="submit" name="apply" class="btn btn-info btn-sm" ><i class="fa  fa-check-circle"></i> {{ Lang::get('core.sb_apply') }}</button>
					<button type="submit" name="submit" class="btn btn-primary btn-sm" ><i class="fa  fa-save "></i> {{ Lang::get('core.sb_save') }}</button>
				  </div>	  
			
		</div> 
		 
		 {!! Form::close() !!}
		 
   <script type="text/javascript">
	$(document).ready(function() { 
		$('.addC').relCopy({});
		
		$("#UserID").jCombo("{!! url('sbinovice/comboselect?filter=tbl_contratistas:IdContratista:NombreFantasia') !!}",
		{  selected_value : '{{ $row["UserID"] }}' });
		 

		$('.removeCurrentFiles').on('click',function(){
			var removeUrl = $(this).attr('href');
			$.get(removeUrl,function(response){});
			$(this).parent('div').empty();	
			return false;
		});		
		
	});
	</script>		 
