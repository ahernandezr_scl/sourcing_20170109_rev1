@if($setting['view-method'] =='native')
<div class="sbox">
	<div class="sbox-title">  
		<h4> <i class="fa fa-table"></i> <?php echo $pageTitle ;?> <small>{{ $pageNote }}</small>
			<a href="javascript:void(0)" class="collapse-close pull-right btn btn-xs btn-danger" onclick="ajaxViewClose('#{{ $pageModule }}')">
			<i class="fa fa fa-times"></i></a>
		</h4>
	 </div>

	<div class="sbox-content"> 
@endif	

		<table class="table table-striped table-bordered" >
			<tbody>	
				
					<tr>
						<td width='30%' class='label-view text-right'>{{ SiteHelpers::activeLang('Contract', (isset($fields['contrato_id']['language'])? $fields['contrato_id']['language'] : array())) }}</td>
						<td>{{ SiteHelpers::formatLookUp($row->contrato_id,'contrato_id','1:tbl_contrato:contrato_id:cont_proveedor') }} </td>
						
					</tr>
				
					<tr>
						<td width='30%' class='label-view text-right'>{{ SiteHelpers::activeLang('Date of evaluation', (isset($fields['evalcont_fecha']['language'])? $fields['evalcont_fecha']['language'] : array())) }}</td>
						<td>{{ date('d-m-Y',strtotime($row->evalcont_fecha)) }} </td>
						
					</tr>
				
					<tr>
						<td width='30%' class='label-view text-right'>{{ SiteHelpers::activeLang('Segment', (isset($fields['evalcont_segmento']['language'])? $fields['evalcont_segmento']['language'] : array())) }}</td>
						<td>{{ SiteHelpers::formatLookUp($row->evalcont_segmento,'evalcont_segmento','1:tbl_evalcontratista_seg:evalcontseg_id:evalcontseg_desc') }} </td>
						
					</tr>
				
					<tr>
						<td width='30%' class='label-view text-right'>{{ SiteHelpers::activeLang('Question', (isset($fields['evalcont_pregunta']['language'])? $fields['evalcont_pregunta']['language'] : array())) }}</td>
						<td>{{ $row->evalcont_pregunta}} </td>
						
					</tr>
				
					<tr>
						<td width='30%' class='label-view text-right'>{{ SiteHelpers::activeLang('Question Description', (isset($fields['evalcont_preguntaGlosa']['language'])? $fields['evalcont_preguntaGlosa']['language'] : array())) }}</td>
						<td>{{ $row->evalcont_preguntaGlosa}} </td>
						
					</tr>
				
					<tr>
						<td width='30%' class='label-view text-right'>{{ SiteHelpers::activeLang('Evaluation', (isset($fields['evalcont_nota']['language'])? $fields['evalcont_nota']['language'] : array())) }}</td>
						<td>{{ $row->evalcont_nota}} </td>
						
					</tr>
				
					<tr>
						<td width='30%' class='label-view text-right'>{{ SiteHelpers::activeLang('Comments', (isset($fields['evalcont_comentarios']['language'])? $fields['evalcont_comentarios']['language'] : array())) }}</td>
						<td>{{ $row->evalcont_comentarios}} </td>
						
					</tr>
				
			</tbody>	
		</table>  
			
		 	

@if($setting['form-method'] =='native')
	</div>	
</div>	
@endif	

<script>
$(document).ready(function(){

});
</script>	