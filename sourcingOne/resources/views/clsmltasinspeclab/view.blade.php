@extends('layouts.app')

@section('content')
<div class="page-content row">
	 
	 
 	<div class="page-content-wrapper m-t">   

<div class="sbox "> 
	<div class="sbox-title"> 

	 <h3> {{ $pageTitle }} <small>{{ $pageNote }}</small></h3>

	 	<div class="sbox-tools">
	   		<a href="{{ URL::to('clsmltasinspeclab?return='.$return) }}" class="tips btn btn-xs btn-white pull-right" title="{{ Lang::get('core.btn_back') }}"><i class="fa fa-arrow-circle-left"></i>&nbsp;{{ Lang::get('core.btn_back') }}</a>
			
			@if($access['is_add'] ==1)
	   		<a href="{{ URL::to('clsmltasinspeclab/update/'.$id.'?return='.$return) }}" class="tips btn btn-xs btn-white pull-right" title="{{ Lang::get('core.btn_edit') }}"><i class="fa fa-edit"></i>&nbsp;{{ Lang::get('core.btn_edit') }}</a>
			@endif 
		</div>
	</div>
	<div class="sbox-content" > 	

		<table class="table table-striped table-bordered" >
			<tbody>	
		
					<tr>
						<td width='30%' class='label-view text-right'>{{ SiteHelpers::activeLang('FinContratista Id', (isset($fields['finContratista_id']['language'])? $fields['finContratista_id']['language'] : array())) }}</td>
						<td>{{ $row->finContratista_id}} </td>

					</tr>
				
					<tr>
						<td width='30%' class='label-view text-right'>{{ SiteHelpers::activeLang('Contrato Id', (isset($fields['contrato_id']['language'])? $fields['contrato_id']['language'] : array())) }}</td>
						<td>{{ $row->contrato_id}} </td>

					</tr>
				
					<tr>
						<td width='30%' class='label-view text-right'>{{ SiteHelpers::activeLang('FinCont Fecha', (isset($fields['finCont_fecha']['language'])? $fields['finCont_fecha']['language'] : array())) }}</td>
						<td>{{ $row->finCont_fecha}} </td>

					</tr>
				
					<tr>
						<td width='30%' class='label-view text-right'>{{ SiteHelpers::activeLang('FinCont TipoProblema', (isset($fields['finCont_tipoProblema']['language'])? $fields['finCont_tipoProblema']['language'] : array())) }}</td>
						<td>{{ $row->finCont_tipoProblema}} </td>

					</tr>
				
					<tr>
						<td width='30%' class='label-view text-right'>{{ SiteHelpers::activeLang('FinCont Cantidad', (isset($fields['finCont_cantidad']['language'])? $fields['finCont_cantidad']['language'] : array())) }}</td>
						<td>{{ $row->finCont_cantidad}} </td>

					</tr>
				
					<tr>
						<td width='30%' class='label-view text-right'>{{ SiteHelpers::activeLang('FinCont Comentario', (isset($fields['finCont_comentario']['language'])? $fields['finCont_comentario']['language'] : array())) }}</td>
						<td>{{ $row->finCont_comentario}} </td>

					</tr>
				
					<tr>
						<td width='30%' class='label-view text-right'>{{ SiteHelpers::activeLang('FinCont Monto', (isset($fields['finCont_monto']['language'])? $fields['finCont_monto']['language'] : array())) }}</td>
						<td>{{ $row->finCont_monto}} </td>

					</tr>
				
			</tbody>	
		</table>   
	
	</div>
</div>	

	</div>
</div>
	  
@stop