

		 {!! Form::open(array('url'=>'clsmltasinspeclab/savepublic', 'class'=>'form-horizontal','files' => true , 'parsley-validate'=>'','novalidate'=>' ')) !!}

	@if(Session::has('messagetext'))
	  
		   {!! Session::get('messagetext') !!}
	   
	@endif
	<ul class="parsley-error-list">
		@foreach($errors->all() as $error)
			<li>{{ $error }}</li>
		@endforeach
	</ul>		


<div class="col-md-12">
						<fieldset><legend> Multas Inspección del Trabajo</legend>
				
									  <div class="form-group  " >
										<label for="FinContratista Id" class=" control-label col-md-4 text-left"> FinContratista Id </label>
										<div class="col-md-6">
										  {!! Form::text('finContratista_id', $row['finContratista_id'],array('class'=>'form-control', 'placeholder'=>'',   )) !!}
										 </div>
										 <div class="col-md-2">
										 	
										 </div>
									  </div> 
									  <div class="form-group  " >
										<label for="Contrato Id" class=" control-label col-md-4 text-left"> Contrato Id </label>
										<div class="col-md-6">
										  {!! Form::text('contrato_id', $row['contrato_id'],array('class'=>'form-control', 'placeholder'=>'',   )) !!}
										 </div>
										 <div class="col-md-2">
										 	
										 </div>
									  </div> 
									  <div class="form-group  " >
										<label for="FinCont Fecha" class=" control-label col-md-4 text-left"> FinCont Fecha </label>
										<div class="col-md-6">
										  {!! Form::text('finCont_fecha', $row['finCont_fecha'],array('class'=>'form-control', 'placeholder'=>'',   )) !!}
										 </div>
										 <div class="col-md-2">
										 	
										 </div>
									  </div> 
									  <div class="form-group  " >
										<label for="FinCont TipoProblema" class=" control-label col-md-4 text-left"> FinCont TipoProblema </label>
										<div class="col-md-6">
										  {!! Form::text('finCont_tipoProblema', $row['finCont_tipoProblema'],array('class'=>'form-control', 'placeholder'=>'',   )) !!}
										 </div>
										 <div class="col-md-2">
										 	
										 </div>
									  </div> 
									  <div class="form-group  " >
										<label for="FinCont Cantidad" class=" control-label col-md-4 text-left"> FinCont Cantidad </label>
										<div class="col-md-6">
										  {!! Form::text('finCont_cantidad', $row['finCont_cantidad'],array('class'=>'form-control', 'placeholder'=>'',   )) !!}
										 </div>
										 <div class="col-md-2">
										 	
										 </div>
									  </div> 
									  <div class="form-group  " >
										<label for="FinCont Comentario" class=" control-label col-md-4 text-left"> FinCont Comentario </label>
										<div class="col-md-6">
										  <textarea name='finCont_comentario' rows='5' id='finCont_comentario' class='form-control '
				           >{{ $row['finCont_comentario'] }}</textarea>
										 </div>
										 <div class="col-md-2">
										 	
										 </div>
									  </div> 
									  <div class="form-group  " >
										<label for="FinCont Monto" class=" control-label col-md-4 text-left"> FinCont Monto </label>
										<div class="col-md-6">
										  {!! Form::text('finCont_monto', $row['finCont_monto'],array('class'=>'form-control', 'placeholder'=>'',   )) !!}
										 </div>
										 <div class="col-md-2">
										 	
										 </div>
									  </div> </fieldset>
			</div>

			

			<div style="clear:both"></div>	
				
					
				  <div class="form-group">
					<label class="col-sm-4 text-right">&nbsp;</label>
					<div class="col-sm-8">	
					<button type="submit" name="apply" class="btn btn-info btn-sm" ><i class="fa  fa-check-circle"></i> {{ Lang::get('core.sb_apply') }}</button>
					<button type="submit" name="submit" class="btn btn-primary btn-sm" ><i class="fa  fa-save "></i> {{ Lang::get('core.sb_save') }}</button>
				  </div>	  
			
		</div> 
		 
		 {!! Form::close() !!}
		 
   <script type="text/javascript">
	$(document).ready(function() { 
		
		 

		$('.removeCurrentFiles').on('click',function(){
			var removeUrl = $(this).attr('href');
			$.get(removeUrl,function(response){});
			$(this).parent('div').empty();	
			return false;
		});		
		
	});
	</script>		 
