<div class="m-t" style="padding-top:25px;">	
    <div class="row m-b-lg animated fadeInDown delayp1 text-center">
        <h3> {{ $pageTitle }} <small> {{ $pageNote }} </small></h3>
        <hr />       
    </div>
</div>
<div class="m-t">
	<div class="table-responsive" > 	

		<table class="table table-striped table-bordered" >
			<tbody>	
		
			
					<tr>
						<td width='30%' class='label-view text-right'>{{ SiteHelpers::activeLang('FinContratista Id', (isset($fields['finContratista_id']['language'])? $fields['finContratista_id']['language'] : array())) }}</td>
						<td>{{ $row->finContratista_id}} </td>

					</tr>
				
					<tr>
						<td width='30%' class='label-view text-right'>{{ SiteHelpers::activeLang('Contrato Id', (isset($fields['contrato_id']['language'])? $fields['contrato_id']['language'] : array())) }}</td>
						<td>{{ $row->contrato_id}} </td>

					</tr>
				
					<tr>
						<td width='30%' class='label-view text-right'>{{ SiteHelpers::activeLang('FinCont Fecha', (isset($fields['finCont_fecha']['language'])? $fields['finCont_fecha']['language'] : array())) }}</td>
						<td>{{ $row->finCont_fecha}} </td>

					</tr>
				
					<tr>
						<td width='30%' class='label-view text-right'>{{ SiteHelpers::activeLang('FinCont TipoProblema', (isset($fields['finCont_tipoProblema']['language'])? $fields['finCont_tipoProblema']['language'] : array())) }}</td>
						<td>{{ $row->finCont_tipoProblema}} </td>

					</tr>
				
					<tr>
						<td width='30%' class='label-view text-right'>{{ SiteHelpers::activeLang('FinCont Cantidad', (isset($fields['finCont_cantidad']['language'])? $fields['finCont_cantidad']['language'] : array())) }}</td>
						<td>{{ $row->finCont_cantidad}} </td>

					</tr>
				
					<tr>
						<td width='30%' class='label-view text-right'>{{ SiteHelpers::activeLang('FinCont Comentario', (isset($fields['finCont_comentario']['language'])? $fields['finCont_comentario']['language'] : array())) }}</td>
						<td>{{ $row->finCont_comentario}} </td>

					</tr>
				
					<tr>
						<td width='30%' class='label-view text-right'>{{ SiteHelpers::activeLang('FinCont Monto', (isset($fields['finCont_monto']['language'])? $fields['finCont_monto']['language'] : array())) }}</td>
						<td>{{ $row->finCont_monto}} </td>

					</tr>
						
					<tr>
						<td width='30%' class='label-view text-right'></td>
						<td> <a href="javascript:history.go(-1)" class="btn btn-primary"> Back To Grid <a> </td>
						
					</tr>					
				
			</tbody>	
		</table>   

	 
	
	</div>
</div>	