<?php usort($tableGrid, "SiteHelpers::_sort"); ?>
<div class="sbox">
	<div class="sbox-title"> 
	<h3><i class="fa fa-table"></i> <?php echo $pageTitle ;?> <small>{{ $pageNote }}</small></h3>
		
		<div class="sbox-tools" >
			<a href="javascript:void(0)" class="btn btn-xs btn-white tips" title="Clear Search" onclick="reloadData('#{{ $pageModule }}','areasdetrabajocontrato/data?search=')"><i class="fa fa-trash-o"></i> Clear Search </a>
			<a href="javascript:void(0)" class="btn btn-xs btn-white tips" title="Reload Data" onclick="reloadData('#{{ $pageModule }}','areasdetrabajocontrato/data?return={{ $return }}')"><i class="fa fa-refresh"></i></a>
			@if(Session::get('gid') ==1)
			<a href="{{ url('sximo/module/config/'.$pageModule) }}" class="btn btn-xs btn-white tips" title=" {{ Lang::get('core.btn_config') }}" ><i class="fa fa-cog"></i></a>
			@endif 
		</div>
	</div>
	<div class="sbox-content"> 	

	<div class="row m-b">
		<div class="col-md-6">
		<div class="form-group  " >
										<label for="IdRequisito" class=" control-label col-md-4 text-left"> Centro </label>
										<div class="col-md-6">
										   <select id="IdCentro" name="IdCentro" class="select2" onchange="filterEstatus(this.value)"></select>
										 </div> 
										 <div class="col-md-2">
										 	
										 </div>
									  </div>
		</div>
		<div class="col-md-6">
		</div>
	</div>

	 {!! (isset($search_map) ? $search_map : '') !!}
	 <?php echo Form::open(array('url'=>'areasdetrabajocontrato/delete/', 'class'=>'form-horizontal' ,'id' =>'SximoTable'  ,'data-parsley-validate'=>'' )) ;?>
<div class="table-responsive">	
	@if(count($rowData)>=1)
    <table class="table table-striped  " id="{{ $pageModule }}Table">
        <thead>
			<tr>
				<th width="20"> No </th>
				<th width="30"> <input type="checkbox" class="checkall" /></th>	
				<th width="30"> RUT </th>		
				<th width="30"> Nombre </th>		
				<th width="30"> Rol </th>	
				<?php foreach ($rowData as $row) : 
           			  $id = $row->IdAreaTrabajo;
           		?>
					 <?php foreach ($tableGrid as $field) :
					 	if($field['view'] =='1' && $field['field']=='Descripcion') : 
							$value = SiteHelpers::formatRows($row->{$field['field']}, $field , $row);
						 	?>
						 	<?php $limited = isset($field['limited']) ? $field['limited'] :''; ?>
						 	@if(SiteHelpers::filterColumn($limited ))
								<th align="center" data-values="{{ $row->{$field['field']} }}" data-field="{{ $field['field'] }}" data-format="{{ htmlentities($value) }}">					 
									{!! $value !!}							 
								</th>
							@endif
						 <?php endif;					 
						endforeach; 
					  ?>
            <?php endforeach;?>
			  </tr>
        </thead>

        <tbody>
        	
        	<?php foreach ($rowDatatwo as $row) : 
           			  $id = $row->IdPersona;
           		?>
                <tr class="editable" id="form-{{ $row->IdPersona }}">
					<td class="number"> <?php echo ++$i;?>  </td>
					<td ><input type="checkbox" class="ids" name="ids[]" value="<?php echo $row->IdPersona ;?>" />  </td>					
					@if($setting['view-method']=='expand')
					<td><a href="javascript:void(0)" class="expandable" rel="#row-{{ $row->IdPersona }}" data-url="{{ url('personas/show/'.$id) }}"><i class="fa fa-plus " ></i></a></td>								
					@endif			
						 	
								 <td align="" data-values="{{ $row->{'RUT'} }}" data-field="{{ 'RUT' }}" data-format="{{ htmlentities($value) }}">					 
									{!! $row->{'RUT'} !!}					 
								 </td>
								 <td align="" data-values="{{ $row->{'Nombres'} }}" data-field="{{ 'Nombres' }}" data-format="{{ htmlentities($value) }}">					 
									{!! $row->{'Nombres'} !!}					 
								 </td>					 
								 <td align="" data-values="{{ $row->{'Roles'} }}" data-field="{{ 'Roles' }}" data-format="{{ htmlentities($value) }}">					 
									{!! $row->{'Roles'} !!}					 
								 </td>	
			
								<?php foreach ($rowData as $rows) : 
				           			  $id = $rows->IdAreaTrabajo;
				           		?>
									 <?php foreach ($tableGrid as $field) :
									 	if($field['view'] =='1' && $field['field']=='Descripcion') : 
											$value = SiteHelpers::formatRows($rows->{$field['field']}, $field , $rows);
										 	?>
										 	<?php $limited = isset($field['limited']) ? $field['limited'] :''; ?>
										 	@if(SiteHelpers::filterColumn($limited ))
												<td align="center" data-values="{{ $rows->{$field['field']} }}" data-field="{{ $field['field'] }}" data-format="{{ htmlentities($value) }}">					 
													<input type="checkbox" id="valchech_{!! $rows->{'IdAreaTrabajo'} !!}"/> 							 
												</td>
											@endif	
										 <?php endif;					 
										endforeach; 
									  ?>
				            <?php endforeach;?>

                </tr>
            <?php endforeach;?>      
			

        </tbody>
      
    </table>
	@else

	<div style="margin:100px 0; text-align:center;">
	
		<p> No Record Found </p>
	</div>
	
	@endif		
	
	</div>
	<?php echo Form::close() ;?>
	
	</div>
</div>	
	
	@if($setting['inline'] =='true') @include('sximo.module.utility.inlinegrid') @endif
<script>
$(document).ready(function() {

	$("#IdCentro").jCombo("{!! url('aprobaciones/comboselect?filter=tbl_centro:IdCentro:Descripcion') !!}",
		{  selected_value : '' });

	$('.tips').tooltip();	
	$('input[type="checkbox"],input[type="radio"]').iCheck({
		checkboxClass: 'icheckbox_square-red',
		radioClass: 'iradio_square-red',
	});	
	$('#{{ $pageModule }}Table .checkall').on('ifChecked',function(){
		$('#{{ $pageModule }}Table input[type="checkbox"]').iCheck('check');
	});
	$('#{{ $pageModule }}Table .checkall').on('ifUnchecked',function(){
		$('#{{ $pageModule }}Table input[type="checkbox"]').iCheck('uncheck');
	});	
	
	$('#{{ $pageModule }}Paginate .pagination li a').click(function() {
		var url = $(this).attr('href');
		reloadData('#{{ $pageModule }}',url);		
		return false ;
	});

	<?php if($setting['view-method'] =='expand') :
			echo AjaxHelpers::htmlExpandGrid();
		endif;
	 ?>	
});	
function filterEstatus(val){
     if (val !==''){
        reloadData('#{{ $pageModule }}','areasdetrabajocontrato/data?IdCentro='+val);
    }
}	
</script>	
<style>
.table th.right { text-align:right !important;}
.table th.center { text-align:center !important;}

</style>
	