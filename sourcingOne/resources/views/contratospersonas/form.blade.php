
@if($setting['form-method'] =='native')
	<div class="sbox">
		<div class="sbox-title">  
			<h4> <i class="fa fa-table"></i> <?php echo $pageTitle ;?> <small>{{ $pageNote }}</small>
				<a href="javascript:void(0)" class="collapse-close pull-right btn btn-xs btn-danger" onclick="ajaxViewClose('#{{ $pageModule }}')"><i class="fa fa fa-times"></i></a>
			</h4>
	</div>

	<div class="sbox-content"> 
@endif	
			{!! Form::open(array('url'=>'contratospersonas/save/'.SiteHelpers::encryptID($row['IdContratosPersonas']), 'class'=>'form-horizontal','files' => true , 'parsley-validate'=>'','novalidate'=>' ','id'=> 'contratospersonasFormAjax')) !!}
			<div class="col-md-12">
						<fieldset><legend> ContratosPersonas</legend>
				{!! Form::hidden('IdContratosPersonas', $row['IdContratosPersonas']) !!}					
									  <div class="form-group  " >
										<label for="Persona" class=" control-label col-md-4 text-left"> Persona </label>
										<div class="col-md-6">
										  <select name='IdPersona' rows='5' id='IdPersona' class='select2 '   ></select> 
										 </div> 
										 <div class="col-md-2">
										 	
										 </div>
									  </div> 					
									  <div class="form-group  " >
										<label for="Contratista" class=" control-label col-md-4 text-left"> Contratista </label>
										<div class="col-md-6">
										  <select name='IdContratista' rows='5' id='IdContratista' class='select2 '   ></select> 
										 </div> 
										 <div class="col-md-2">
										 	
										 </div>
									  </div> 					
									  <div class="form-group  " >
										<label for="Contrato" class=" control-label col-md-4 text-left"> Contrato </label>
										<div class="col-md-6">
										  <select name='contrato_id' rows='5' id='contrato_id' class='select2 '   ></select> 
										 </div> 
										 <div class="col-md-2">
										 	
										 </div>
									  </div> 					
									  <div class="form-group  " >
										<label for="Rol" class=" control-label col-md-4 text-left"> Rol </label>
										<div class="col-md-6">
										  <select name='IdRol' rows='5' id='IdRol' class='select2 '   ></select> 
										 </div> 
										 <div class="col-md-2">
										 	
										 </div>
									  </div> 					
									  <div class="form-group  " >
										<label for="Centro" class=" control-label col-md-4 text-left"> Centro </label>
										<div class="col-md-6">
										  <select name='IdCentro' rows='5' id='IdCentro' class='select2 '   ></select> 
										 </div> 
										 <div class="col-md-2">
										 	
										 </div>
									  </div> </fieldset>
			</div>
			
												
								
						
			<div style="clear:both"></div>	
							
			<div class="form-group">
				<label class="col-sm-4 text-right">&nbsp;</label>
				<div class="col-sm-8">	
					<button type="submit" class="btn btn-primary btn-sm "><i class="icon-checkmark-circle2"></i>  {{ Lang::get('core.sb_save') }} </button>
					<button type="button" onclick="ajaxViewClose('#{{ $pageModule }}')" class="btn btn-success btn-sm"><i class="icon-cancel-circle2 "></i>  {{ Lang::get('core.sb_cancel') }} </button>
				</div>			
			</div> 		 
			{!! Form::close() !!}


@if($setting['form-method'] =='native')
	</div>	
</div>	
@endif	

	
</div>	
			 
<script type="text/javascript">
$(document).ready(function() { 
	
		$("#IdPersona").jCombo("{!! url('contratospersonas/comboselect?filter=vw_personas_no_contratadas:IdPersona:RUT|Nombres|Apellidos') !!}&parent=entry_by_access:",
		{  parent: '#entry_by_access', selected_value : '{{ $row["IdPersona"] }}' });
		
		$("#IdContratista").jCombo("{!! url('contratospersonas/comboselect?filter=tbl_contratistas:IdContratista:RUT|RazonSocial') !!}",
		{  selected_value : '{{ $row["IdContratista"] }}' });
		
		$("#contrato_id").jCombo("{!! url('contratospersonas/comboselect?filter=tbl_contrato:contrato_id:cont_numero') !!}",
		{  selected_value : '{{ $row["contrato_id"] }}' });
		
		$("#IdRol").jCombo("{!! url('contratospersonas/comboselect?filter=tbl_roles:IdRol:Descripción') !!}",
		{  selected_value : '{{ $row["IdRol"] }}' });
		
		$("#IdCentro").jCombo("{!! url('contratospersonas/comboselect?filter=tbl_centro:IdCentro:Descripcion') !!}",
		{  selected_value : '{{ $row["IdCentro"] }}' });
		 
	
	$('.editor').summernote();
	$('.previewImage').fancybox();	
	$('.tips').tooltip();	
	$(".select2").select2({ width:"98%"});	
	$('.date').datepicker({format:'yyyy-mm-dd',autoClose:true})
	$('.datetime').datetimepicker({format: 'yyyy-mm-dd hh:ii:ss'}); 
	$('input[type="checkbox"],input[type="radio"]').iCheck({
		checkboxClass: 'icheckbox_square-red',
		radioClass: 'iradio_square-red',
	});			
		$('.removeMultiFiles').on('click',function(){
			var removeUrl = '{{ url("contratospersonas/removefiles?file=")}}'+$(this).attr('url');
			$(this).parent().remove();
			$.get(removeUrl,function(response){});
			$(this).parent('div').empty();	
			return false;
		});
				
	var form = $('#contratospersonasFormAjax'); 
	form.parsley();
	form.submit(function(){
		
		if(form.parsley('isValid') == true){			
			var options = { 
				dataType:      'json', 
				beforeSubmit :  showRequest,
				success:       showResponse  
			}  
			$(this).ajaxSubmit(options); 
			return false;
						
		} else {
			return false;
		}		
	
	});

});

function showRequest()
{
	$('.ajaxLoading').show();		
}  
function showResponse(data)  {		
	
	if(data.status == 'success')
	{
		ajaxViewClose('#{{ $pageModule }}');
		ajaxFilter('#{{ $pageModule }}','{{ $pageUrl }}/data');
		notyMessage(data.message);	
		$('#sximo-modal').modal('hide');	
	} else {
		notyMessageError(data.message);	
		$('.ajaxLoading').hide();
		return false;
	}	
}			 

</script>		 