

		 {!! Form::open(array('url'=>'contratospersonas/savepublic', 'class'=>'form-horizontal','files' => true , 'parsley-validate'=>'','novalidate'=>' ')) !!}

	@if(Session::has('messagetext'))
	  
		   {!! Session::get('messagetext') !!}
	   
	@endif
	<ul class="parsley-error-list">
		@foreach($errors->all() as $error)
			<li>{{ $error }}</li>
		@endforeach
	</ul>		


<div class="col-md-12">
						<fieldset><legend> ContratosPersonas</legend>
				{!! Form::hidden('IdContratosPersonas', $row['IdContratosPersonas']) !!}					
									  <div class="form-group  " >
										<label for="Persona" class=" control-label col-md-4 text-left"> Persona </label>
										<div class="col-md-6">
										  <select name='IdPersona' rows='5' id='IdPersona' class='select2 '   ></select> 
										 </div> 
										 <div class="col-md-2">
										 	
										 </div>
									  </div> 					
									  <div class="form-group  " >
										<label for="Contratista" class=" control-label col-md-4 text-left"> Contratista </label>
										<div class="col-md-6">
										  <select name='IdContratista' rows='5' id='IdContratista' class='select2 '   ></select> 
										 </div> 
										 <div class="col-md-2">
										 	
										 </div>
									  </div> 					
									  <div class="form-group  " >
										<label for="Contrato" class=" control-label col-md-4 text-left"> Contrato </label>
										<div class="col-md-6">
										  <select name='contrato_id' rows='5' id='contrato_id' class='select2 '   ></select> 
										 </div> 
										 <div class="col-md-2">
										 	
										 </div>
									  </div> 					
									  <div class="form-group  " >
										<label for="Rol" class=" control-label col-md-4 text-left"> Rol </label>
										<div class="col-md-6">
										  <select name='IdRol' rows='5' id='IdRol' class='select2 '   ></select> 
										 </div> 
										 <div class="col-md-2">
										 	
										 </div>
									  </div> 					
									  <div class="form-group  " >
										<label for="Centro" class=" control-label col-md-4 text-left"> Centro </label>
										<div class="col-md-6">
										  <select name='IdCentro' rows='5' id='IdCentro' class='select2 '   ></select> 
										 </div> 
										 <div class="col-md-2">
										 	
										 </div>
									  </div> </fieldset>
			</div>
			
			

			<div style="clear:both"></div>	
				
					
				  <div class="form-group">
					<label class="col-sm-4 text-right">&nbsp;</label>
					<div class="col-sm-8">	
					<button type="submit" name="apply" class="btn btn-info btn-sm" ><i class="fa  fa-check-circle"></i> {{ Lang::get('core.sb_apply') }}</button>
					<button type="submit" name="submit" class="btn btn-primary btn-sm" ><i class="fa  fa-save "></i> {{ Lang::get('core.sb_save') }}</button>
				  </div>	  
			
		</div> 
		 
		 {!! Form::close() !!}
		 
   <script type="text/javascript">
	$(document).ready(function() { 
		
		
		$("#IdPersona").jCombo("{!! url('contratospersonas/comboselect?filter=tbl_personas:IdPersona:RUT|Nombres|Apellidos') !!}",
		{  selected_value : '{{ $row["IdPersona"] }}' });
		
		$("#IdContratista").jCombo("{!! url('contratospersonas/comboselect?filter=tbl_contratistas:IdContratista:RUT|RazonSocial') !!}",
		{  selected_value : '{{ $row["IdContratista"] }}' });
		
		$("#contrato_id").jCombo("{!! url('contratospersonas/comboselect?filter=tbl_contrato:contrato_id:cont_numero') !!}",
		{  selected_value : '{{ $row["contrato_id"] }}' });
		
		$("#IdRol").jCombo("{!! url('contratospersonas/comboselect?filter=tbl_roles:IdRol:Descripción') !!}",
		{  selected_value : '{{ $row["IdRol"] }}' });
		
		$("#IdCentro").jCombo("{!! url('contratospersonas/comboselect?filter=tbl_centro:IdCentro:Descripcion') !!}",
		{  selected_value : '{{ $row["IdCentro"] }}' });
		 

		$('.removeCurrentFiles').on('click',function(){
			var removeUrl = $(this).attr('href');
			$.get(removeUrl,function(response){});
			$(this).parent('div').empty();	
			return false;
		});		
		
	});
	</script>		 
