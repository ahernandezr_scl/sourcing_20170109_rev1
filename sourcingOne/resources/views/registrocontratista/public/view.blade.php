<div class="m-t" style="padding-top:25px;">	
    <div class="row m-b-lg animated fadeInDown delayp1 text-center">
        <h3> {{ $pageTitle }} <small> {{ $pageNote }} </small></h3>
        <hr />       
    </div>
</div>
<div class="m-t">
	<div class="table-responsive" > 	

		<table class="table table-striped table-bordered" >
			<tbody>	
		
			
					<tr>
						<td width='30%' class='label-view text-right'>{{ SiteHelpers::activeLang('Tipo', (isset($fields['tipo']['language'])? $fields['tipo']['language'] : array())) }}</td>
						<td>{{ $row->tipo}} </td>
						
					</tr>
				
					<tr>
						<td width='30%' class='label-view text-right'>{{ SiteHelpers::activeLang('Rut', (isset($fields['rut']['language'])? $fields['rut']['language'] : array())) }}</td>
						<td>{{ $row->rut}} </td>
						
					</tr>
				
					<tr>
						<td width='30%' class='label-view text-right'>{{ SiteHelpers::activeLang('Razon Social', (isset($fields['nombre']['language'])? $fields['nombre']['language'] : array())) }}</td>
						<td>{{ $row->nombre}} </td>
						
					</tr>
				
					<tr>
						<td width='30%' class='label-view text-right'>{{ SiteHelpers::activeLang('Estado', (isset($fields['estado_id']['language'])? $fields['estado_id']['language'] : array())) }}</td>
						<td>{{ $row->estado_id}} </td>
						
					</tr>
				
					<tr>
						<td width='30%' class='label-view text-right'>{{ SiteHelpers::activeLang('Creacion', (isset($fields['creacion']['language'])? $fields['creacion']['language'] : array())) }}</td>
						<td>{{ $row->creacion}} </td>
						
					</tr>
				
					<tr>
						<td width='30%' class='label-view text-right'>{{ SiteHelpers::activeLang('Nombre Fantasia', (isset($fields['nombre_fantasia']['language'])? $fields['nombre_fantasia']['language'] : array())) }}</td>
						<td>{{ $row->nombre_fantasia}} </td>
						
					</tr>
				
					<tr>
						<td width='30%' class='label-view text-right'>{{ SiteHelpers::activeLang('Direccion', (isset($fields['direccion']['language'])? $fields['direccion']['language'] : array())) }}</td>
						<td>{{ $row->direccion}} </td>
						
					</tr>
				
					<tr>
						<td width='30%' class='label-view text-right'>{{ SiteHelpers::activeLang('Telefono', (isset($fields['telefono']['language'])? $fields['telefono']['language'] : array())) }}</td>
						<td>{{ $row->telefono}} </td>
						
					</tr>
				
					<tr>
						<td width='30%' class='label-view text-right'>{{ SiteHelpers::activeLang('Celular', (isset($fields['celular']['language'])? $fields['celular']['language'] : array())) }}</td>
						<td>{{ $row->celular}} </td>
						
					</tr>
				
					<tr>
						<td width='30%' class='label-view text-right'>{{ SiteHelpers::activeLang('Correo', (isset($fields['correo']['language'])? $fields['correo']['language'] : array())) }}</td>
						<td>{{ $row->correo}} </td>
						
					</tr>
				
					<tr>
						<td width='30%' class='label-view text-right'>{{ SiteHelpers::activeLang('Correo Alternativo', (isset($fields['correo_alternativo']['language'])? $fields['correo_alternativo']['language'] : array())) }}</td>
						<td>{{ $row->correo_alternativo}} </td>
						
					</tr>
				
					<tr>
						<td width='30%' class='label-view text-right'>{{ SiteHelpers::activeLang('Sitio', (isset($fields['sitio']['language'])? $fields['sitio']['language'] : array())) }}</td>
						<td>{{ $row->sitio}} </td>
						
					</tr>
				
					<tr>
						<td width='30%' class='label-view text-right'>{{ SiteHelpers::activeLang('Rep.Legal', (isset($fields['representante_legal1']['language'])? $fields['representante_legal1']['language'] : array())) }}</td>
						<td>{{ $row->representante_legal1}} </td>
						
					</tr>
				
					<tr>
						<td width='30%' class='label-view text-right'>{{ SiteHelpers::activeLang('Representante Legal', (isset($fields['representante_legal2']['language'])? $fields['representante_legal2']['language'] : array())) }}</td>
						<td>{{ $row->representante_legal2}} </td>
						
					</tr>
				
					<tr>
						<td width='30%' class='label-view text-right'>{{ SiteHelpers::activeLang('Representante Legal', (isset($fields['representante_legal3']['language'])? $fields['representante_legal3']['language'] : array())) }}</td>
						<td>{{ $row->representante_legal3}} </td>
						
					</tr>
				
					<tr>
						<td width='30%' class='label-view text-right'>{{ SiteHelpers::activeLang('Contacto', (isset($fields['contacto']['language'])? $fields['contacto']['language'] : array())) }}</td>
						<td>{{ $row->contacto}} </td>
						
					</tr>
				
					<tr>
						<td width='30%' class='label-view text-right'>{{ SiteHelpers::activeLang('Giro', (isset($fields['giro']['language'])? $fields['giro']['language'] : array())) }}</td>
						<td>{{ $row->giro}} </td>
						
					</tr>
						
					<tr>
						<td width='30%' class='label-view text-right'></td>
						<td> <a href="javascript:history.go(-1)" class="btn btn-primary"> Back To Grid <a> </td>
						
					</tr>					
				
			</tbody>	
		</table>   

	 
	
	</div>
</div>	