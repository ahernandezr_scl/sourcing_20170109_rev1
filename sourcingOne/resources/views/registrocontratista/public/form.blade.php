

		 {!! Form::open(array('url'=>'registrocontratista/savepublic', 'class'=>'form-horizontal','files' => true , 'parsley-validate'=>'','novalidate'=>' ')) !!}

	@if(Session::has('messagetext'))
	  
		   {!! Session::get('messagetext') !!}
	   
	@endif
	<ul class="parsley-error-list">
		@foreach($errors->all() as $error)
			<li>{{ $error }}</li>
		@endforeach
	</ul>		


<div class="col-md-6">
						<fieldset><legend> Datos Principales</legend>
				{!! Form::hidden('id_empresa', $row['id_empresa']) !!}					
									  <div class="form-group  " >
										<label for="Tipo" class=" control-label col-md-4 text-left"> Tipo <span class="asterix"> * </span></label>
										<div class="col-md-6">
										  
					<label class='radio radio-inline'>
					<input type='radio' name='tipo' value ='Contratista' required @if($row['tipo'] == 'Contratista') checked="checked" @endif > Contratista </label>
					<label class='radio radio-inline'>
					<input type='radio' name='tipo' value ='Proveedor' required @if($row['tipo'] == 'Proveedor') checked="checked" @endif > Proveedor </label> 
										 </div> 
										 <div class="col-md-2">
										 	
										 </div>
									  </div> 					
									  <div class="form-group  " >
										<label for="Rut" class=" control-label col-md-4 text-left"> Rut <span class="asterix"> * </span></label>
										<div class="col-md-6">
										  {!! Form::text('rut', $row['rut'],array('class'=>'form-control', 'placeholder'=>'', 'required'=>'true'  )) !!} 
										 </div> 
										 <div class="col-md-2">
										 	
										 </div>
									  </div> 					
									  <div class="form-group  " >
										<label for="Razon Social" class=" control-label col-md-4 text-left"> Razon Social </label>
										<div class="col-md-6">
										  {!! Form::text('nombre', $row['nombre'],array('class'=>'form-control', 'placeholder'=>'',   )) !!} 
										 </div> 
										 <div class="col-md-2">
										 	
										 </div>
									  </div> 					
									  <div class="form-group  " >
										<label for="Nombre Fantasia" class=" control-label col-md-4 text-left"> Nombre Fantasia </label>
										<div class="col-md-6">
										  <textarea name='nombre_fantasia' rows='5' id='nombre_fantasia' class='form-control '  
				           >{{ $row['nombre_fantasia'] }}</textarea> 
										 </div> 
										 <div class="col-md-2">
										 	
										 </div>
									  </div> 					
									  <div class="form-group  " >
										<label for="Giro" class=" control-label col-md-4 text-left"> Giro </label>
										<div class="col-md-6">
										  {!! Form::text('giro', $row['giro'],array('class'=>'form-control', 'placeholder'=>'',   )) !!} 
										 </div> 
										 <div class="col-md-2">
										 	
										 </div>
									  </div> 					
									  <div class="form-group  " >
										<label for="Sitio Web" class=" control-label col-md-4 text-left"> Sitio Web </label>
										<div class="col-md-6">
										  {!! Form::text('sitio', $row['sitio'],array('class'=>'form-control', 'placeholder'=>'',   )) !!} 
										 </div> 
										 <div class="col-md-2">
										 	
										 </div>
									  </div> </fieldset>
			</div>
			
			<div class="col-md-6">
						<fieldset><legend> Datos de contacto</legend>
									
									  <div class="form-group  " >
										<label for="Direccion" class=" control-label col-md-4 text-left"> Direccion </label>
										<div class="col-md-6">
										  {!! Form::text('direccion', $row['direccion'],array('class'=>'form-control', 'placeholder'=>'',   )) !!} 
										 </div> 
										 <div class="col-md-2">
										 	
										 </div>
									  </div> 					
									  <div class="form-group  " >
										<label for="Telefono" class=" control-label col-md-4 text-left"> Telefono </label>
										<div class="col-md-6">
										  {!! Form::text('telefono', $row['telefono'],array('class'=>'form-control', 'placeholder'=>'',   )) !!} 
										 </div> 
										 <div class="col-md-2">
										 	
										 </div>
									  </div> 					
									  <div class="form-group  " >
										<label for="Celular" class=" control-label col-md-4 text-left"> Celular </label>
										<div class="col-md-6">
										  {!! Form::text('celular', $row['celular'],array('class'=>'form-control', 'placeholder'=>'',   )) !!} 
										 </div> 
										 <div class="col-md-2">
										 	
										 </div>
									  </div> 					
									  <div class="form-group  " >
										<label for="Correo" class=" control-label col-md-4 text-left"> Correo </label>
										<div class="col-md-6">
										  {!! Form::text('correo', $row['correo'],array('class'=>'form-control', 'placeholder'=>'',   )) !!} 
										 </div> 
										 <div class="col-md-2">
										 	
										 </div>
									  </div> 					
									  <div class="form-group  " >
										<label for="Correo Alternativo" class=" control-label col-md-4 text-left"> Correo Alternativo </label>
										<div class="col-md-6">
										  {!! Form::text('correo_alternativo', $row['correo_alternativo'],array('class'=>'form-control', 'placeholder'=>'',   )) !!} 
										 </div> 
										 <div class="col-md-2">
										 	
										 </div>
									  </div> 					
									  <div class="form-group  " >
										<label for="Representante Legal" class=" control-label col-md-4 text-left"> Representante Legal </label>
										<div class="col-md-6">
										  {!! Form::text('representante_legal1', $row['representante_legal1'],array('class'=>'form-control', 'placeholder'=>'',   )) !!} 
										 </div> 
										 <div class="col-md-2">
										 	
										 </div>
									  </div> 					
									  <div class="form-group  " >
										<label for="Representante Legal" class=" control-label col-md-4 text-left"> Representante Legal </label>
										<div class="col-md-6">
										  {!! Form::text('representante_legal2', $row['representante_legal2'],array('class'=>'form-control', 'placeholder'=>'',   )) !!} 
										 </div> 
										 <div class="col-md-2">
										 	
										 </div>
									  </div> 					
									  <div class="form-group  " >
										<label for="Representante Legal" class=" control-label col-md-4 text-left"> Representante Legal </label>
										<div class="col-md-6">
										  {!! Form::text('representante_legal3', $row['representante_legal3'],array('class'=>'form-control', 'placeholder'=>'',   )) !!} 
										 </div> 
										 <div class="col-md-2">
										 	
										 </div>
									  </div> 					
									  <div class="form-group  " >
										<label for="Contacto" class=" control-label col-md-4 text-left"> Contacto </label>
										<div class="col-md-6">
										  {!! Form::text('contacto', $row['contacto'],array('class'=>'form-control', 'placeholder'=>'',   )) !!} 
										 </div> 
										 <div class="col-md-2">
										 	
										 </div>
									  </div> </fieldset>
			</div>
			
			

			<div style="clear:both"></div>	
				
					
				  <div class="form-group">
					<label class="col-sm-4 text-right">&nbsp;</label>
					<div class="col-sm-8">	
					<button type="submit" name="apply" class="btn btn-info btn-sm" ><i class="fa  fa-check-circle"></i> {{ Lang::get('core.sb_apply') }}</button>
					<button type="submit" name="submit" class="btn btn-primary btn-sm" ><i class="fa  fa-save "></i> {{ Lang::get('core.sb_save') }}</button>
				  </div>	  
			
		</div> 
		 
		 {!! Form::close() !!}
		 
   <script type="text/javascript">
	$(document).ready(function() { 
		
		 

		$('.removeCurrentFiles').on('click',function(){
			var removeUrl = $(this).attr('href');
			$.get(removeUrl,function(response){});
			$(this).parent('div').empty();	
			return false;
		});		
		
	});
	</script>		 
