@extends('layouts.app')

@section('content')

  <div class="page-content row">
    <!-- Page header -->

 
 	<div class="page-content-wrapper m-t">


<div class="sbox">
	<div class="sbox-title"> <h3> {{ $pageTitle }} <small>{{ $pageNote }}</small></h3>
		<div class="sbox-tools" >
			<a href="{{ url($pageModule.'?return='.$return) }}" class="btn btn-xs btn-white tips"  title="{{ Lang::get('core.btn_back') }}" ><i class="icon-backward"></i> {{ Lang::get('core.btn_back') }} </a> 
		</div>
	</div>
	<div class="sbox-content"> 	

		<ul class="parsley-error-list">
			@foreach($errors->all() as $error)
				<li>{{ $error }}</li>
			@endforeach
		</ul>	
  <!-- Nav tabs YOSUA-->
			  <ul class="nav nav-tabs" role="tablist">
			  	<li role="presentation" class="active"><a href="#form_entry" aria-controls="form_entry" role="tab" data-toggle="tab"><b>{{ $pageTitle }} : </b></a></li>
			  @if($row['id_order'] !== '')	<li role="presentation" ><a href="#masterdetailform" aria-controls="masterdetailform" role="tab" data-toggle="tab"><b> Detalle : </b></a></li>
			  	<li role="presentation" ><a href="#adjuntos" aria-controls="adjuntos" role="tab" data-toggle="tab"><b> Adjuntos : </b></a></li> @endif
			 </ul>
 			<!-- Tab panes -->
 			{!! Form::open(array('url'=>'orden/save?return='.$return, 'class'=>'form-horizontal','files' => true , 'parsley-validate'=>'','novalidate'=>' ')) !!}
  			<div class="tab-content m-t">
	  			<div role="tabpanel" class="tab-pane active" id="form_entry">
				<div class="col-md-12">
						<fieldset><legend> Orden</legend>
						{!! Form::hidden('id_order', $row['id_order']) !!}					
									  <div class="form-group  " >
										<label for="Contrato" class=" control-label col-md-4 text-left"> Contrato <span class="asterix"> * </span></label>
										<div class="col-md-6">
										  <select name='contrato_id' rows='5' id='contrato_id' class='select2 ' required onchange="changeContrato(this);" ></select> 
										 </div> 
										 <div class="col-md-2">
										 	
										 </div>
									  </div> 					
									  <div class="form-group" >
										<label for="Pedido" class=" control-label col-md-2 text-left"> Desde <span class="asterix"> * </span></label>
										<div class="col-md-2">
											<div class="input-group m-b" style="width:150px !important;">
												{!! Form::text('dateIssued', $row['dateIssued'],array('class'=>'form-control date')) !!}
												<span class="input-group-addon"><i class="fa fa-calendar"></i></span>
											</div> 
										 </div> 
										 <div class="col-md-2"></div>
											<label for="Finalizado" class=" control-label col-md-2 text-left"> Hasta </label>
											<div class="col-md-2">
												<div class="input-group m-b" style="width:150px !important;">
													{!! Form::text('dueDate', $row['dueDate'],array('class'=>'form-control date')) !!}
													<span class="input-group-addon"><i class="fa fa-calendar"></i></span>
												</div> 
											 </div> 
											 <div class="col-md-2"></div>
									  </div> 										
									  <div class="form-group  " >
										<label for="Monto" class=" control-label col-md-4 text-left"> Monto <span class="asterix"> * </span></label>
										<div class="col-md-6">
										  {!! Form::text('amount', $row['amount'],array('class'=>'form-control', 'placeholder'=>'', 'required'=>'true'  )) !!} 
										 </div> 
										 <div class="col-md-2">
										 	
										 </div>
									  </div> 					
									 <!-- YOSUA se quitar estado. COntrol de cambio <div class="form-group  " >
										<label for="Estado" class=" control-label col-md-4 text-left"> Estado </label>
										<div class="col-md-6">
										  
					<?php $estado_id = explode(',',$row['estado_id']);
					$estado_id_opt = array( 'Pendiente' => 'Pendiente' ); ?>
					<select name='estado_id' rows='5'   class='select2 '  > 
						<?php 
						foreach($estado_id_opt as $key=>$val)
						{
							echo "<option  value ='$key' ".($row['estado_id'] == $key ? " selected='selected' " : '' ).">$val</option>"; 						
						}						
						?></select> 
										 </div> 
										 <div class="col-md-2">
										 	
										 </div>
									  </div> -->
									   <div class="form-group  " >
										<label for="Tipo Pago" class=" control-label col-md-4 text-left"> Tipo Pago </label>
										<div class="col-md-6">
										  
					<label class='radio radio-inline'>
					<input type='radio' name='tipo_pago' value ='Total'  @if($row['tipo_pago'] == 'Total') checked="checked" @endif > Total </label>
					<label class='radio radio-inline'>
					<input type='radio' name='tipo_pago' value ='Parcial'  @if($row['tipo_pago'] == 'Parcial') checked="checked" @endif > Parcial </label> 
										 </div> 
										 <div class="col-md-2">
										 	
										 </div>
									  </div> 					
									  <div class="form-group  " >
										<label for="Reg Externo" class=" control-label col-md-4 text-left"> Reg Externo </label>
										<div class="col-md-6">
										  {!! Form::text('reg_externo', $row['reg_externo'],array('class'=>'form-control', 'placeholder'=>'',   )) !!} 
										 </div> 
										 <div class="col-md-2">
										 	
										 </div>
									  </div> 					
									  <div class="form-group  " >
										<label for="Monto Pagado" class=" control-label col-md-4 text-left"> Monto Autorizado </label>
										<div class="col-md-6">
										  {!! Form::text('monto_pagado', $row['monto_pagado'],array('class'=>'form-control', 'placeholder'=>'',   )) !!} 
										 </div> 
										 <div class="col-md-2">
										 	
										 </div>
									  </div> </fieldset>
			</div>
			
		</div>	
		@if($row['id_order'] !== '')
<div role="tabpanel" class="tab-pane" id="masterdetailform">
					
	<hr />
	<div class="clr clear"></div>
	
	<h5> Orden items </h5>
	
	<div class="table-responsive">
    <table class="table table-striped ">
        <thead>
			<tr>
				@foreach ($subform['tableGrid'] as $t)
					@if($t['view'] =='1' && $t['field'] !='id_order')
						<th>{{ $t['label'] }}</th>
					@endif
				@endforeach
				<th></th>	
			  </tr>

        </thead>

        <tbody>
        @if(count($subform['rowData'])>=1)
            @foreach ($subform['rowData'] as $rows)
            <tr class="clone clonedInput">
									
			 @foreach ($subform['tableGrid'] as $field)
				 @if($field['view'] =='1' && $field['field'] !='id_order')
				 <td>					 
				 	{!! SiteHelpers::bulkForm($field['field'] , $subform['tableForm'] , $rows->{$field['field']}) !!}							 
				 </td>
				 @endif					 
			 
			 @endforeach
			 <td>
			 	<a onclick=" $(this).parents('.clonedInput').remove(); return false" href="#" class="remove btn btn-xs btn-danger">-</a>
			 	<input type="hidden" name="counter[]">
			 </td>
			@endforeach
			</tr> 

		@else
            <tr class="clone clonedInput">
									
			 @foreach ($subform['tableGrid'] as $field)

				 @if($field['view'] =='1' && $field['field'] !='id_order')
				 <td>					 
				 	{!! SiteHelpers::bulkForm($field['field'] , $subform['tableForm'] ) !!}							 
				 </td>
				 @endif					 
			 
			 @endforeach
			 <td>
			 	<a onclick=" $(this).parents('.clonedInput').remove(); return false" href="#" class="remove btn btn-xs btn-danger">-</a>
			 	<input type="hidden" name="counter[]">
			 </td>
			
			</tr> 

		
		@endif	


        </tbody>	

     </table>  
     <input type="hidden" name="enable-masterdetail" value="true">
     </div>
	<br /><br />
     
     <a href="javascript:void(0);" class="addC btn btn-xs btn-info" rel=".clone"><i class="fa fa-plus"></i> New Item</a>

     		</div>
				<div role="tabpanel" class="tab-pane" id="adjuntos">
						<div id="adjuntos-content"></div>
							
						<a href="../../adjuntosorden/update?orderid={{$row['id_order']}}" class="btn btn-xs btn-info" target="_blank"><i class="fa fa-plus"></i>Nuevo Adjunto</a>	
									
				</div>
				@endif
</div>
			<div style="clear:both"></div>	
				
					
				  <div class="form-group">
					<label class="col-sm-4 text-right">&nbsp;</label>
					<div class="col-sm-8">	
					<button type="submit" name="apply" class="btn btn-info btn-sm" ><i class="icon-checkmark-circle2"></i> {{ Lang::get('core.sb_apply') }}</button>
					<button type="submit" name="submit" class="btn btn-primary btn-sm" ><i class="icon-bubble-check"></i> {{ Lang::get('core.sb_save') }}</button>
					<button type="button" onclick="location.href='{{ URL::to('orden?return='.$return) }}' " class="btn btn-warning btn-sm "><i class="icon-cancel-circle2 "></i>  {{ Lang::get('core.sb_cancel') }} </button>
					</div>	  
			
				  </div> 
		 
		 {!! Form::close() !!}
	</div>
</div>		 
</div>	
</div>

   <script type="text/javascript">
	$(document).ready(function() { 
		$('#adjuntos-content').load('../lookup/Adjuntos-orden-id_order-adjuntosorden-sb_order_adjuntos-id_order-{{$row['id_order']}}');
		$('.addC').relCopy({});
		
		$("#contrato_id").jCombo("{!! url('orden/comboselect?filter=sb_contratos:id_contrato:nro_contrato|nombre_contrato') !!}",
		{  selected_value : '{{ $row["contrato_id"] }}' });
		 

		$('.removeMultiFiles').on('click',function(){
			var removeUrl = '{{ url("orden/removefiles?file=")}}'+$(this).attr('url');
			$(this).parent().remove();
			$.get(removeUrl,function(response){});
			$(this).parent('div').empty();	
			return false;
		});		

		@if($row['id_order'] !== '')
		$.get('{{ url("contratoitem/itemsbycontrato")}}/{{$row['contrato_id']}}',function(output){
			console.log('out::  ' + output.data.services);
			localStorage.setItem("service", JSON.stringify(output.data.services));

			$('input[name="bulk_servicio_id[]"]').each(function() {
				var val = $(this).val();  
				var select = '<select name=bulk_servicio_id[] class="form-control">';
				var array = $.parseJSON( localStorage.getItem("service"));
				$.each(array,function(index, value){ 
				  if(val==value.id){
				    select = select + '<option value="'+value.id+'" selected="selected">'+ value.name +'</option>';
				  }else{
				    select = select + '<option value="'+value.id+'">'+ value.name +'</option>';
				  }
				});
				select = select + '</select>';
				var combo = $(select);
			  	$(this).replaceWith( combo);  
			});
		});
		@endif
	});
	function changeContrato(data){

		
	}
	</script>		 
@stop