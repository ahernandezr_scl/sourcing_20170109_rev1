
@if($setting['form-method'] =='native')
	<div class="sbox">
		<div class="sbox-title">
			<h4> <i class="fa fa-table"></i> <?php echo $pageTitle ;?> <small>{{ $pageNote }}</small>
				<a href="javascript:void(0)" class="collapse-close pull-right btn btn-xs btn-danger" onclick="ajaxViewClose('#{{ $pageModule }}')"><i class="fa fa fa-times"></i></a>
			</h4>
	</div>

	<div class="sbox-content">
@endif
			{!! Form::open(array('url'=>'personas/save/'.SiteHelpers::encryptID($row['IdPersona']), 'class'=>'form-horizontal','files' => true , 'parsley-validate'=>'','novalidate'=>' ','id'=> 'personasFormAjax')) !!}
			<ul class="nav nav-tabs"><li class="active"><a href="#Personas" data-toggle="tab">Personas</a></li>
				<li class="" ><a href="#Anotaciones" id="DataAnota"data-toggle="tab">Anotaciones</a></li>
				</ul><div class="tab-content"><div class="tab-pane m-t active" id="Personas">
				{!! Form::hidden('IdPersona', $row['IdPersona']) !!}
									  <div class="form-group  " >
										<label for="RUT" class=" control-label col-md-4 text-left"> RUT <span class="asterix"> * </span></label>
										<div class="col-md-6">
										  {!! Form::text('RUT', $row['RUT'],array('class'=>'form-control', 'id'=>'rut', 'placeholder'=>'', 'required'=>'true'  )) !!}
										 </div>
										 <div class="col-md-2">

										 </div>
									  </div>
									  <div class="form-group  " >
										<label for="Nombres" class=" control-label col-md-4 text-left"> Nombres <span class="asterix"> * </span></label>
										<div class="col-md-6">
										  {!! Form::text('Nombres', $row['Nombres'],array('class'=>'form-control', 'placeholder'=>'', 'required'=>'true'  )) !!}
										 </div>
										 <div class="col-md-2">

										 </div>
									  </div>
									  <div class="form-group  " >
										<label for="Apellidos" class=" control-label col-md-4 text-left"> Apellidos <span class="asterix"> * </span></label>
										<div class="col-md-6">
										  {!! Form::text('Apellidos', $row['Apellidos'],array('class'=>'form-control', 'placeholder'=>'', 'required'=>'true'  )) !!}
										 </div>
										 <div class="col-md-2">

										 </div>
									  </div> {!! Form::hidden('Foto', $row['Foto']) !!}
									  <div class="form-group  " >
										<label for="Archivo Foto" class=" control-label col-md-4 text-left"> Archivo Foto </label>
										<div class="col-md-6">
										  <input  type='file' name='ArchivoFoto' id='ArchivoFoto' @if($row['ArchivoFoto'] =='') class='required' @endif style='width:150px !important;'  />
					 	<div >
						{!! SiteHelpers::showUploadedFile($row['ArchivoFoto'],'/uploads/employee/') !!}

						</div>

										 </div>
										 <div class="col-md-2">

										 </div>
									  </div>
									  <div class="form-group  " >
										<label for="Direccion" class=" control-label col-md-4 text-left"> Direccion </label>
										<div class="col-md-6">
										  <textarea name='Direccion' rows='5' id='Direccion' class='form-control '
				           >{{ $row['Direccion'] }}</textarea>
										 </div>
										 <div class="col-md-2">

										 </div>
									  </div>
									  <div class="form-group  " >
										<label for="Fecha Nacimiento" class=" control-label col-md-4 text-left"> Fecha Nacimiento </label>
										<div class="col-md-6">

				<div class="input-group m-b" style="width:150px !important;">
					{!! Form::text('FechaNacimiento', $row['FechaNacimiento'],array('class'=>'form-control date')) !!}
					<span class="input-group-addon"><i class="fa fa-calendar"></i></span>
				</div>
										 </div>
										 <div class="col-md-2">

										 </div>
									  </div>
									  <div class="form-group  " >
										<label for="Sexo" class=" control-label col-md-4 text-left"> Sexo </label>
										<div class="col-md-6">

					<?php $Sexo = explode(',',$row['Sexo']);
					$Sexo_opt = array( '' => 'Seleccione Valor' ,  '1' => 'Hombre' ,  '2' => 'Mujer' , ); ?>
					<select name='Sexo' id='Sexo' rows='5'   class='select2 '  >
						<?php
						foreach($Sexo_opt as $key=>$val)
						{
							echo "<option  value ='$key' ".($row['Sexo'] == $key ? " selected='selected' " : '' ).">$val</option>";
						}
						?></select>
										 </div>
										 <div class="col-md-2">

										 </div>
									  </div>
									  <div class="form-group  " >
										<label for="Estado Civil" class=" control-label col-md-4 text-left"> Estado Civil </label>
										<div class="col-md-6">

					<?php $EstadoCivil = explode(',',$row['EstadoCivil']);
					$EstadoCivil_opt = array( '' => 'Seleccione Valor' ,  '1' => 'Soltero' ,  '2' => 'Casado' ,  '3' => 'Divorciado' ,  '4' => 'Viudo' , ); ?>
					<select name='EstadoCivil' id='EstadoCivil' rows='5'   class='select2 '  >
						<?php
						foreach($EstadoCivil_opt as $key=>$val)
						{
							echo "<option  value ='$key' ".($row['EstadoCivil'] == $key ? " selected='selected' " : '' ).">$val</option>";
						}
						?></select>
										 </div>
										 <div class="col-md-2">

										 </div>
									  </div>
									  <div class="form-group  " >
										<label for="Asignado a" class=" control-label col-md-4 text-left"> Asignado a </label>
										<div class="col-md-6">
										  <select name='entry_by_access' rows='5' id='entry_by_access' class='select2 '   ></select>
										 </div>
										 <div class="col-md-2">

										 </div>
									  </div>
									  <?php if (strlen($row['IdEstatus'])>0): ?>
									  <div class="form-group  " >
										<label for="Estatus" class=" control-label col-md-4 text-left"> Estatus </label>
										<div class="col-md-6">

										<?php $IdEstatus = explode(',',$row['IdEstatus']);
										$IdEstatus_opt = array( '' => 'Seleccione Valor' ,  '1' => 'Activo' ,  '2' => 'Suspendido' , ); ?>
										<select name='IdEstatus' rows='5'   class='select2 '  >
											<?php
											foreach($IdEstatus_opt as $key=>$val)
											{
												echo "<option  value ='$key' ".($row['IdEstatus'] == $key ? " selected='selected' " : '' ).">$val</option>";
											}
											?></select>
										 </div>
										 <div class="col-md-2">

										 </div>
									  </div>
 									<?php elseif (strlen($row['IdEstatus'])==0): ?>
                                      <input type="hidden" name="IdEstatus" value="1">
                                    <?php endif; ?>
                                    {!! Form::hidden('createdOn', $row['createdOn']) !!}
                                    				{!! Form::hidden('entry_by', $row['entry_by']) !!}
									   {!! Form::hidden('updatedOn', $row['updatedOn']) !!}
			</div>

			<div class="tab-pane m-t " id="Anotaciones">
				<hr />
	<div class="clr clear"></div>

	<h5> Anotaciones </h5>

	<div class="table-responsive">
    <table class="table table-striped " id="tanotacion">
        <thead>
			<tr>
				@foreach ($subform['tableGrid'] as $t)
					@if($t['view'] =='1' && $t['field'] !='IdPersona')
						<th>{{ $t['label'] }}</th>
					@endif
				@endforeach
				<th></th>
			  </tr>

        </thead>

        <tbody>
        @if(count($subform['rowData'])>=1)
            @foreach ($subform['rowData'] as $rows)
            <tr class="clone clonedInput">
			 @foreach ($subform['tableGrid'] as $field)
				 @if($field['view'] =='1' && $field['field'] !='IdPersona')
				 <td>
				 	{!! SiteHelpers::bulkForm($field['field'] , $subform['tableForm'] , $rows->{$field['field']}) !!}
				 </td>
				 @endif

			 @endforeach
			 <td>
			 	<a onclick=" $(this).parents('.clonedInput').remove(); return false" href="#" class="remove btn btn-xs btn-danger">-</a>
			 	<input type="hidden" name="counter[]">
			 </td>
			@endforeach
			</tr>
		@endif
			<tr class="clone clonedInput">
			 @foreach ($subform['tableGrid'] as $field)

				 @if($field['view'] =='1' && $field['field'] !='IdPersona')
				 <td>
				 	{!! SiteHelpers::bulkForm($field['field'] , $subform['tableForm'] ) !!}
				 </td>
				 @endif

			 @endforeach
			 <td>
			 	<a onclick=" $(this).parents('.clonedInput').remove(); return false" href="#" class="remove btn btn-xs btn-danger">-</a>
			 	<input type="hidden" name="counter[]">
			 </td>

			</tr>
        </tbody>

     </table>
     <input type="hidden" name="enable-masterdetail" value="true">
     </div>
	<br /><br />

     <a href="javascript:void(0);" class="addC btn btn-xs btn-info" rel=".clone"><i class="fa fa-plus"></i>Nuevo registro</a>
     <hr />
			</div>


			<div style="clear:both"></div>

			<div class="form-group">
				<label class="col-sm-4 text-right">&nbsp;</label>
				<div class="col-sm-8">
					<button type="submit" class="btn btn-primary btn-sm "><i class="icon-checkmark-circle2"></i>  {{ Lang::get('core.sb_save') }} </button>
					<button type="button" onclick="ajaxViewClose('#{{ $pageModule }}')" class="btn btn-success btn-sm"><i class="icon-cancel-circle2 "></i>  {{ Lang::get('core.sb_cancel') }} </button>
				</div>
			</div>
			{!! Form::close() !!}


@if($setting['form-method'] =='native')
	</div>
</div>
@endif


</div>
<script type="text/javascript">
$(document).ready(function() {

$(".bulk_IdConceptoAnotacion").focus(function(){
	var lobjIdCentro = $(this);
	$(".bulk_IdConceptoAnotacion").each(function () {
		if ($(this).val()!=lobjIdCentro.val()) {
	  	    	lobjIdCentro.find("option[value='"+$(this).val()+"']").remove(); //elimino el valor
	  	    }
	  	});
});
$(".bulk_IdConceptoAnotacion").change(function(){
	var lobjIdCentro = $(this);
	lobjIdCentro.attr("eval-val-combo","true");
	$(".bulk_IdConceptoAnotacion").each(function () { //recorro todos los datos que se llamen igual que el
		if ($(this).attr("eval-val-combo")!="true"){
			if ($(this).val()==lobjIdCentro.val()) {
				lobjIdCentro.val("");
				notyMessageError("Este registro ya fue seleccionado.");
			}
		}
	});
	lobjIdCentro.attr("eval-val-combo","false");
});

<?php if(count($subform['rowData'])>=1): ?>
	$('.bulk_IdConceptoAnotacion').each(function(index) {
	 $( this ).attr('id', 'bulk_IdConceptoAnotacion'+ (index+1));
	});

$("#DataAnota").css('color', 'red');
<?php endif; ?>

		$("#entry_by_access").jCombo("{!! url('personas/comboselect?filter=vw_usuarios_contratistas:id:first_name|last_name') !!}",
		{  selected_value : '{{ $row["entry_by_access"] }}' });
	 /*Codigo para validar RUT*/
       var Fn = {
		    // Valida el rut con su cadena completa "XXXXXXXX-X"
		    validaRut : function (rutCompleto) {
		        if (!/^[0-9]+-[0-9kK]{1}$/.test( rutCompleto ))
		            return false;
		        var tmp     = rutCompleto.split('-');
		        var digv    = tmp[1];
		        var rut     = tmp[0];
		        if ( digv == 'K' ) digv = 'k' ;
		        return (Fn.dv(rut) == digv );
		    },
		    dv : function(T){
		        var M=0,S=1;
		        for(;T;T=Math.floor(T/10))
		            S=(S+T%10*(9-M++%6))%11;
		        return S?S-1:'k';
		    }
		}

		$("#rut").focusout(function(){
		    if (Fn.validaRut( $("#rut").val() )){
		        $("#rut").css("border-color", "#1ab394");
		        $("#guardar").prop("disabled", false);
		    } else {
		        alert("El Rut no es válido");
		        $("#rut").css("border-color", "#ed5565");
		        $("#guardar").prop("disabled", true);
		    }
		});
		/*Fin cogigo RUT*/

    $("#rut").blur(function(){
	$.post('personas/compruebarut',{ rut:$('#rut').val(),},
				function(data) {
				if (data.valores==""){
					if($('#guardar').is(':enabled'))
					$("#guardar").prop("disabled", false);
				}
				else{

					    <?php if(count($subform['rowData'])>=1): ?>
					     $('#tanotacion tbody tr:not(:first)').remove();
					     $(".bulk_IdConceptoAnotacion").val('')
					     <?php endif;?>
					     $(".clone.clonedInput").css("display", "block");
					 var filaStored = $(".clone.clonedInput").clone();
					var rowCount = $('#tanotacion tbody tr').length;
					if (rowCount>1){
					 	$("#tanotacion .clone.clonedInput").remove();
					}
					$('input[name=IdPersona]').val(data.valores[0].IdPersona);
					$('input[name=Nombres]').val(data.valores[0].Nombres);
					$('input[name=Apellidos]').val(data.valores[0].Apellidos);
					$("#Direccion").val(data.valores[0].Direccion);
					$('input[name=FechaNacimiento]').val(data.valores[0].FechaNacimiento);
					$("#Sexo").select2();
					$("#Sexo").val(data.valores[0].Sexo).trigger("change");
					$("#EstadoCivil").select2();
					$('#EstadoCivil').val(data.valores[0].EstadoCivil).trigger("change");
					$("#entry_by_access").select2();
					$('#entry_by_access').val(data.valores[0].entry_by_access).trigger("change");
					if (data.valores[0].IdAnotacion!=null){
						$("#DataAnota").css('color', 'red');
						$("#tanotacion .clone.clonedInput").remove();
						for( i = 0; i < data.valores.length;i++){
							filaStored.clone().appendTo("#tanotacion tbody" );
								$('.bulk_IdConceptoAnotacion').each(function(index) {
								 $( this ).attr('id', 'bulk_IdConceptoAnotacion'+ (index+1));
								});
							var fieldId = "#bulk_IdConceptoAnotacion" + (i+1);
  							 $(fieldId).val(data.valores[i].IdConceptoAnotacion);
						}

					}

				}
			 });

	});
	$('.addC').relCopy({});
	$('.editor').summernote();
	$('.previewImage').fancybox();
	$('.tips').tooltip();
	$(".select2").select2({ width:"98%"});
	$('.date').datepicker({format:'yyyy-mm-dd',autoClose:true})
	$('.datetime').datetimepicker({format: 'yyyy-mm-dd hh:ii:ss'});
	$('input[type="checkbox"],input[type="radio"]').iCheck({
		checkboxClass: 'icheckbox_square-red',
		radioClass: 'iradio_square-red',
	});
		$('.removeMultiFiles').on('click',function(){
			var removeUrl = '{{ url("personas/removefiles?file=")}}'+$(this).attr('url');
			$(this).parent().remove();
			$.get(removeUrl,function(response){});
			$(this).parent('div').empty();
			return false;
		});

	var form = $('#personasFormAjax');
	form.parsley();
	form.submit(function(){

		if(form.parsley('isValid') == true){
			var options = {
				dataType:      'json',
				beforeSubmit :  showRequest,
				success:       showResponse
			}
			$(this).ajaxSubmit(options);
			return false;

		} else {
			return false;
		}

	});

});

function showRequest()
{
	$('.ajaxLoading').show();
}
function showResponse(data)  {

	if(data.status == 'success')
	{
		ajaxViewClose('#{{ $pageModule }}');
		ajaxFilter('#{{ $pageModule }}','{{ $pageUrl }}/data');
		notyMessage(data.message);
		$('#sximo-modal').modal('hide');
	} else {
		notyMessageError(data.message);
		$('.ajaxLoading').hide();
		return false;
	}
}

</script>
<style type="text/css">
	#tanotacion tbody tr:last-child {
		display:none;
	}
</style>