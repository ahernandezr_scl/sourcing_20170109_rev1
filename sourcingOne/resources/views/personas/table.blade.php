<?php $lintLevelUser = \MySourcing::LevelUser(Session::get('uid'));?>

<!-- Modal -->
<div id="eliminarModal" class="modal fade" role="dialog">
  <div class="modal-dialog">
    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Desvincular persona</h4>
      </div>
      <div class="modal-body">
        <div class="form-group  ">
          <div class="col-md-6">
                <select name="anotacion" id="anotacion" class="form-control" >
                    <option value="">Seleccione una Anotacion</option>
                    <?php foreach ($anotaciones as $rows): ?>
                        <option value="<?php echo $rows->IdConceptoAnotacion ?>"><?php echo $rows->Descripcion ?></option>
                    <?php endforeach ?>
                </select>
                <input type="hidden" id="user" value="<?php echo Session::get('uid') ?>">
            </div>
            <div class="col-md-2">
            </div>
        </div>
      </div>
      <div class="modal-footer">
         <input type="hidden" name="" id="idEsta" />
         <button type="button" id="GuardarI" class="btn btn-default" onclick="ajaxDeleteInLine($('#idEsta').val())">Guardar</button>
         <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
      </div>
    </div>

  </div>
</div>
<?php usort($tableGrid, "SiteHelpers::_sort"); ?>
     <!-- Incluimos la libería para que carguemos el archivo por ajax -->
     <link href="{{ asset('sximo/css/plugins/fileinput/fileinput.css')}}" media="all" rel="stylesheet" type="text/css" />
     <script src="{{ asset('sximo/js/plugins/fileinput/fileinput.min.js')}}" type="text/javascript"></script>
     <script src="{{ asset('sximo/js/plugins/fileinput/fileinput_locale_es.js')}}" type="text/javascript"></script>
     <!-- Incluimos la libería para que carguemos el archivo por ajax -->


<div class="sbox float-e-margins">
  <div class="sbox-title">
    <h3 style="display: block!important;">Carga masiva</h3>
      <div class="sbox-tools" >
        <a class="collapse-link">
            <i class="fa fa-chevron-down"></i>
        </a>
      </div>
  </div>
  <div class="sbox-content" style="display:none">
    <div id="inputfileup" class="row">
        <div class="col-xs-12">
            <div class="form-group  " >
              <label for="Archivo" class=" control-label col-md-2 text-left">Archivo excel </label>
              <div class="col-md-8">
                <input id="FileDataEmployee" name="FileDataEmployee" type="file" class="file" data-show-preview="false"  data-allowed-file-extensions='["xls", "xlsx"]'>
               </div>
               <div class="col-md-2"></div>
            </div>
            </div>
<br><br>
            <div class="col-xs-12">
            <div class="form-group  " >
              <label for="Archivo" class=" control-label col-md-2 text-left">Archivo Ejemplo</label>
              <div class="col-md-8">
                  <a download href="uploads/FormatoPersonas.xls"><img src="sximo/images/excel.png" alt="Imagen Excel"></a>
               </div>
               <div class="col-md-2"></div>
            </div>
            </div>
        </div>
      </div>
  </div>
</div>

<div class="sbox">
    <div class="sbox-title">
    <h3><i class="fa fa-table"></i> <?php echo $pageTitle ;?> <small>{{ $pageNote }}</small></h3>

        <div class="sbox-tools" >
            <a href="javascript:void(0)" class="btn btn-xs btn-white tips" title="Clear Search" onclick="reloadData('#{{ $pageModule }}','personas/data?search=')"><i class="fa fa-trash-o"></i> Clear Search </a>
            <a href="javascript:void(0)" class="btn btn-xs btn-white tips" title="Reload Data" onclick="reloadData('#{{ $pageModule }}','personas/data?return={{ $return }}')"><i class="fa fa-refresh"></i></a>
            @if(Session::get('gid') ==1)
            <a href="{{ url('sximo/module/config/'.$pageModule) }}" class="btn btn-xs btn-white tips" title=" {{ Lang::get('core.btn_config') }}" ><i class="fa fa-cog"></i></a>
            @endif
        </div>
    </div>
    <div class="sbox-content">
     {!! (isset($search_map) ? $search_map : '') !!}

    <?php echo Form::open(array('url'=>'personas/delete/', 'class'=>'form-horizontal' ,'id' =>'SximoTable'  ,'data-parsley-validate'=>'' )) ;?>
      <div class="table-responsive" id="{{ $pageModule }}TableContainer">
      <input type="hidden" name="nivel" id="nivel" value="<?php echo $lintLevelUser ?>">
        <table class="table   table-striped table-bordered table-hover dataTable  " id="{{ $pageModule }}Table">
          <thead>
            <tr>
              <th width="20"> No </th>
              <th width="30"> <input type="checkbox" class="checkall" /></th>
              @if($setting['view-method']=='expand')<th width="30" style="width: 30px;">  </th> @endif
              <?php
                foreach ($tableGrid as $t) :
                  if($t['view'] =='1'):
                    $limited = isset($t['limited']) ? $t['limited'] :'';
                    if(SiteHelpers::filterColumn($limited ))
                    {
                        echo '<th align="'.$t['align'].'" width="'.$t['width'].'">'.\SiteHelpers::activeLang($t['label'],(isset($t['language'])? $t['language'] : array())).'</th>';
                    }
                  endif;
                endforeach;
              ?>
              <th width="100" class="text-right"><?php echo Lang::get('core.btn_action') ;?></th>
            </tr>
          </thead>
          <tbody>

          </tbody>
        </table>
      </div>
    <?php echo Form::close() ;?>

  </div>
</div>

    @if($setting['inline'] =='true') @include('sximo.module.utility.inlinegrid') @endif
<script>
$(document).ready(function() {
  $("#anotacion").change(function() {
         if (this.value!='')
               $('#GuardarI').prop("disabled", false );
});



$("#FileDataEmployee").fileinput('refresh',
                {
                  browseLabel: ' Seleccionar',
                  removeLabel: ' Eliminar',
                  uploadLabel: ' Cargar',
                  uploadUrl: '{{ asset("personas/masivo")}}',
                  extra: {idtipodocumento: 1}
                }
            );
            $('#FileDataEmployee').on('fileunlock', function(event, filestack, extraData) {
               reloadData('#{{ $pageModule }}','personas/data?return={{ $return }}');
            });


  $('.collapse-link').click(function () {
    console.log("hola");
        var ibox = $(this).closest('div.sbox');
        var button = $(this).find('i');
        var content = ibox.find('div.sbox-content');
        content.slideToggle(200);
        button.toggleClass('fa-chevron-up').toggleClass('fa-chevron-down');
        ibox.toggleClass('').toggleClass('border-bottom');
        setTimeout(function () {
            ibox.resize();
            ibox.find('[id^=map-]').resize();
        }, 50);
    });

$("#FileDataEmployee").fileinput('refresh',
                {
                  browseLabel: ' Seleccionar',
                  removeLabel: ' Eliminar',
                  uploadLabel: ' Cargar',
                  uploadUrl: '{{ asset("personas/masivo")}}',
                  extra: {idtipodocumento: 1}
                }
            );
            $('#FileDataEmployee').on('fileunlock', function(event, filestack, extraData) {
               reloadData('#{{ $pageModule }}','personas/data?return={{ $return }}');
            });


    $('#{{ $pageModule }}Table').DataTable({
        "dom": 'Tfigtlp',
        "tableTools": {
            "sSwfPath": "sximo/js/plugins/dataTables/swf/copy_csv_xls_pdf.swf"
        },
        "language": {
            "url": "sximo/js/plugins/dataTables/language/Spanish.json"
        },
        "info": false,
        "ajax": 'personas/lista?Nivel='+$('#nivel').val(),
        "deferRender": true,
          "columns": [
            { "data": "id" },
            { "data": "checkbox" },
            <?php
              foreach ($tableGrid as $field) {
                if($field['view'] =='1') {
                  echo '{"data": "'.$field['field'].'"}, ';
                }
              }
            ?>
            { "data": "action" },
            ],
        "columnDefs": [
                        { "orderable": false, "targets": 5 }
                      ],
        "initComplete": function(settings, json) {
            <?php if($access['is_add'] ==1 || $method['form-method'] =='modal') { ?>
              $('.DTTT_container').before('<a style="float:left;" href="<?php echo URL::to($pageModule.'/update'); ?>" class="tips btn btn-sm btn-white" onclick="SximoModal(this.href,\'Create Detail\'); return false; \"><i class="icon-plus-circle2 "></i> <?php echo Lang::get('core.btn_create'); ?> </a>');
            <?php }elseif ($access['is_add'] ==1) { ?>
                $('.DTTT_container').before('<a style="float:left;" href="javascript://ajax" class="tips btn btn-sm btn-white" onclick="ajaxViewDetail(\'#{{ $pageModule }}\',this.href); return false; "><i class="icon-plus-circle2 "></i> <?php echo Lang::get('core.btn_create'); ?> </a>');
            <?php } ?>

            <?php if($access['is_remove'] ==1) { ?>
              $('.DTTT_container').before('<a style="float:left;" href="javascript://ajax" class="btn btn-sm btn-white" onclick="ajaxRemove(\'#{{ $pageModule }}\',\'{{ $pageUrl }}\');"><i class="icon-remove4 "></i> <?php echo Lang::get('core.btn_remove'); ?> </a>');
            <?php } ?>
                        }
    });

    $('.tips').tooltip();
    $('input[type="checkbox"],input[type="radio"]').iCheck({
        checkboxClass: 'icheckbox_square-red',
        radioClass: 'iradio_square-red',
    });
    $('#{{ $pageModule }}Table .checkall').on('ifChecked',function(){
        $('#{{ $pageModule }}Table input[type="checkbox"]').iCheck('check');
    });
    $('#{{ $pageModule }}Table .checkall').on('ifUnchecked',function(){
        $('#{{ $pageModule }}Table input[type="checkbox"]').iCheck('uncheck');
    });

    $('#{{ $pageModule }}Paginate .pagination li a').click(function() {
        var url = $(this).attr('href');
        reloadData('#{{ $pageModule }}',url);
        return false ;
    });

    <?php if($setting['view-method'] =='expand') :
            echo AjaxHelpers::htmlExpandGrid();
        endif;
     ?>
});
function valores(id){
        $('#eliminarModal').modal('show');
        $('#GuardarI').prop("disabled", true );
        $('#idEsta').val(id)
    }

    function ajaxDeleteInLine(id){
      var usuario = $("#user").val();
         var motivo = $("#anotacion").val()
        var datas = {IdPersona:id,"result":motivo,"usuario":usuario};
        $('.ajaxLoading').show();
        $.post( '{{$pageModule}}/borrar' ,datas, function( data ) {
            if(data.status == 'success')
            {
                ajaxFilter('#{{ $pageModule }}','{{ $pageUrl }}/data');
                notyMessage(data.message);
                $('#eliminarModal').modal('hide');
            } else {
                $('.ajaxLoading').hide();
                notyMessageError(data.message);
                return false;
            }
        },"json");


    }
</script>
<style>
.table th.right { text-align:right !important;}
.table th.center { text-align:center !important;}
/* DATATABLES */
#{{ $pageModule }}TableContainer {
    border:none;
}
table.dataTable thead .sorting,
table.dataTable thead .sorting_asc:after,
table.dataTable thead .sorting_desc,
table.dataTable thead .sorting_asc_disabled,
table.dataTable thead .sorting_desc_disabled {
  background: transparent;
}
.dataTables_length {
  float: left;
}
body.DTTT_Print {
  background: #fff;
}
.DTTT_Print #page-wrapper {
  margin: 0;
  background: #fff;
}
button.DTTT_button,
div.DTTT_button,
a.DTTT_button {
  border: 1px solid #e7eaec;
  background: #fff;
  color: #676a6c;
  box-shadow: none;
  padding: 6px 8px;
}
button.DTTT_button:hover,
div.DTTT_button:hover,
a.DTTT_button:hover {
  border: 1px solid #d2d2d2;
  background: #fff;
  color: #676a6c;
  box-shadow: none;
  padding: 6px 8px;
}
button.DTTT_button:hover:not(.DTTT_disabled),
div.DTTT_button:hover:not(.DTTT_disabled),
a.DTTT_button:hover:not(.DTTT_disabled) {
  border: 1px solid #d2d2d2;
  background: #fff;
  box-shadow: none;
}
.dataTables_filter label {
  margin-right: 5px;
}
#inputfileup {
    display: block;
    padding:20px 0px;
}
.btn-file, .fileinput-remove-button, .fileinput-cancel-button, .fileinput-upload-button {
    padding-top: 5px!important;
    padding-bottom: 4px!important;
}
.kv-upload-progress {
    display:none!important;
}

</style>

