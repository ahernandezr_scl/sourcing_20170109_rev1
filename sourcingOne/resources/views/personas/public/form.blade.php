

		 {!! Form::open(array('url'=>'personas/savepublic', 'class'=>'form-horizontal','files' => true , 'parsley-validate'=>'','novalidate'=>' ')) !!}

	@if(Session::has('messagetext'))
	  
		   {!! Session::get('messagetext') !!}
	   
	@endif
	<ul class="parsley-error-list">
		@foreach($errors->all() as $error)
			<li>{{ $error }}</li>
		@endforeach
	</ul>		


<ul class="nav nav-tabs"><li class="active"><a href="#Personas" data-toggle="tab">Personas</a></li>
				<li class=""><a href="#Anotaciones" data-toggle="tab">Anotaciones</a></li>
				</ul><div class="tab-content"><div class="tab-pane m-t active" id="Personas"> 
				{!! Form::hidden('IdPersona', $row['IdPersona']) !!}					
									  <div class="form-group  " >
										<label for="RUT" class=" control-label col-md-4 text-left"> RUT <span class="asterix"> * </span></label>
										<div class="col-md-6">
										  {!! Form::text('RUT', $row['RUT'],array('class'=>'form-control', 'placeholder'=>'', 'required'=>'true'  )) !!} 
										 </div> 
										 <div class="col-md-2">
										 	
										 </div>
									  </div> 					
									  <div class="form-group  " >
										<label for="Nombres" class=" control-label col-md-4 text-left"> Nombres <span class="asterix"> * </span></label>
										<div class="col-md-6">
										  {!! Form::text('Nombres', $row['Nombres'],array('class'=>'form-control', 'placeholder'=>'', 'required'=>'true'  )) !!} 
										 </div> 
										 <div class="col-md-2">
										 	
										 </div>
									  </div> 					
									  <div class="form-group  " >
										<label for="Apellidos" class=" control-label col-md-4 text-left"> Apellidos <span class="asterix"> * </span></label>
										<div class="col-md-6">
										  {!! Form::text('Apellidos', $row['Apellidos'],array('class'=>'form-control', 'placeholder'=>'', 'required'=>'true'  )) !!} 
										 </div> 
										 <div class="col-md-2">
										 	
										 </div>
									  </div> {!! Form::hidden('Foto', $row['Foto']) !!}					
									  <div class="form-group  " >
										<label for="Archivo Foto" class=" control-label col-md-4 text-left"> Archivo Foto </label>
										<div class="col-md-6">
										  <input  type='file' name='ArchivoFoto' id='ArchivoFoto' @if($row['ArchivoFoto'] =='') class='required' @endif style='width:150px !important;'  />
					 	<div >
						{!! SiteHelpers::showUploadedFile($row['ArchivoFoto'],'/uploads/employee/') !!}
						
						</div>					
					 
										 </div> 
										 <div class="col-md-2">
										 	
										 </div>
									  </div> 					
									  <div class="form-group  " >
										<label for="Direccion" class=" control-label col-md-4 text-left"> Direccion </label>
										<div class="col-md-6">
										  <textarea name='Direccion' rows='5' id='Direccion' class='form-control '  
				           >{{ $row['Direccion'] }}</textarea> 
										 </div> 
										 <div class="col-md-2">
										 	
										 </div>
									  </div> 					
									  <div class="form-group  " >
										<label for="Fecha Nacimiento" class=" control-label col-md-4 text-left"> Fecha Nacimiento </label>
										<div class="col-md-6">
										  
				<div class="input-group m-b" style="width:150px !important;">
					{!! Form::text('FechaNacimiento', $row['FechaNacimiento'],array('class'=>'form-control date')) !!}
					<span class="input-group-addon"><i class="fa fa-calendar"></i></span>
				</div> 
										 </div> 
										 <div class="col-md-2">
										 	
										 </div>
									  </div> 					
									  <div class="form-group  " >
										<label for="Sexo" class=" control-label col-md-4 text-left"> Sexo </label>
										<div class="col-md-6">
										  
					<?php $Sexo = explode(',',$row['Sexo']);
					$Sexo_opt = array( '' => 'Seleccione Valor' ,  '1' => 'Hombre' ,  '2' => 'Mujer' , ); ?>
					<select name='Sexo' rows='5'   class='select2 '  > 
						<?php 
						foreach($Sexo_opt as $key=>$val)
						{
							echo "<option  value ='$key' ".($row['Sexo'] == $key ? " selected='selected' " : '' ).">$val</option>"; 						
						}						
						?></select> 
										 </div> 
										 <div class="col-md-2">
										 	
										 </div>
									  </div> 					
									  <div class="form-group  " >
										<label for="Estado Civil" class=" control-label col-md-4 text-left"> Estado Civil </label>
										<div class="col-md-6">
										  
					<?php $EstadoCivil = explode(',',$row['EstadoCivil']);
					$EstadoCivil_opt = array( '' => 'Seleccione Valor' ,  '1' => 'Soltero' ,  '2' => 'Casado' ,  '3' => 'Divorciado' ,  '4' => 'Viudo' , ); ?>
					<select name='EstadoCivil' rows='5'   class='select2 '  > 
						<?php 
						foreach($EstadoCivil_opt as $key=>$val)
						{
							echo "<option  value ='$key' ".($row['EstadoCivil'] == $key ? " selected='selected' " : '' ).">$val</option>"; 						
						}						
						?></select> 
										 </div> 
										 <div class="col-md-2">
										 	
										 </div>
									  </div> 					
									  <div class="form-group  " >
										<label for="Estatus" class=" control-label col-md-4 text-left"> Estatus </label>
										<div class="col-md-6">
										  
					<?php $IdEstatus = explode(',',$row['IdEstatus']);
					$IdEstatus_opt = array( '' => 'Seleccione Valor' ,  '1' => 'Activo' ,  '2' => 'Inactivo' , ); ?>
					<select name='IdEstatus' rows='5'   class='select2 '  > 
						<?php 
						foreach($IdEstatus_opt as $key=>$val)
						{
							echo "<option  value ='$key' ".($row['IdEstatus'] == $key ? " selected='selected' " : '' ).">$val</option>"; 						
						}						
						?></select> 
										 </div> 
										 <div class="col-md-2">
										 	
										 </div>
									  </div> {!! Form::hidden('createdOn', $row['createdOn']) !!}					
									  <div class="form-group  " >
										<label for="Asignado a" class=" control-label col-md-4 text-left"> Asignado a </label>
										<div class="col-md-6">
										  <select name='entry_by_access' rows='5' id='entry_by_access' class='select2 '   ></select> 
										 </div> 
										 <div class="col-md-2">
										 	
										 </div>
									  </div> {!! Form::hidden('updatedOn', $row['updatedOn']) !!}
			</div>
			
			<div class="tab-pane m-t " id="Anotaciones"> 
				
			</div>
			
			

			<div style="clear:both"></div>	
				
					
				  <div class="form-group">
					<label class="col-sm-4 text-right">&nbsp;</label>
					<div class="col-sm-8">	
					<button type="submit" name="apply" class="btn btn-info btn-sm" ><i class="fa  fa-check-circle"></i> {{ Lang::get('core.sb_apply') }}</button>
					<button type="submit" name="submit" class="btn btn-primary btn-sm" ><i class="fa  fa-save "></i> {{ Lang::get('core.sb_save') }}</button>
				  </div>	  
			
		</div> 
		 
		 {!! Form::close() !!}
		 
   <script type="text/javascript">
	$(document).ready(function() { 
		$('.addC').relCopy({});
		
		$("#entry_by_access").jCombo("{!! url('personas/comboselect?filter=tb_users:id:first_name|last_name') !!}",
		{  selected_value : '{{ $row["entry_by_access"] }}' });
		 

		$('.removeCurrentFiles').on('click',function(){
			var removeUrl = $(this).attr('href');
			$.get(removeUrl,function(response){});
			$(this).parent('div').empty();	
			return false;
		});		
		
	});
	</script>		 
