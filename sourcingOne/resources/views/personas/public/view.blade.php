<div class="m-t" style="padding-top:25px;">	
    <div class="row m-b-lg animated fadeInDown delayp1 text-center">
        <h3> {{ $pageTitle }} <small> {{ $pageNote }} </small></h3>
        <hr />       
    </div>
</div>
<div class="m-t">
	<div class="table-responsive" > 	

		<table class="table table-striped table-bordered" >
			<tbody>	
		
			
					<tr>
						<td width='30%' class='label-view text-right'>{{ SiteHelpers::activeLang('RUT', (isset($fields['RUT']['language'])? $fields['RUT']['language'] : array())) }}</td>
						<td>{{ $row->RUT}} </td>
						
					</tr>
				
					<tr>
						<td width='30%' class='label-view text-right'>{{ SiteHelpers::activeLang('Name', (isset($fields['Nombres']['language'])? $fields['Nombres']['language'] : array())) }}</td>
						<td>{{ $row->Nombres}} </td>
						
					</tr>
				
					<tr>
						<td width='30%' class='label-view text-right'>{{ SiteHelpers::activeLang('Last Name', (isset($fields['Apellidos']['language'])? $fields['Apellidos']['language'] : array())) }}</td>
						<td>{{ $row->Apellidos}} </td>
						
					</tr>
				
					<tr>
						<td width='30%' class='label-view text-right'>{{ SiteHelpers::activeLang('Picture', (isset($fields['Foto']['language'])? $fields['Foto']['language'] : array())) }}</td>
						<td>{{ $row->Foto}} </td>
						
					</tr>
				
					<tr>
						<td width='30%' class='label-view text-right'>{{ SiteHelpers::activeLang('Foto', (isset($fields['ArchivoFoto']['language'])? $fields['ArchivoFoto']['language'] : array())) }}</td>
						<td>{!! SiteHelpers::formatRows($row->ArchivoFoto,$fields['ArchivoFoto'],$row ) !!} </td>
						
					</tr>
				
					<tr>
						<td width='30%' class='label-view text-right'>{{ SiteHelpers::activeLang('Direccion', (isset($fields['Direccion']['language'])? $fields['Direccion']['language'] : array())) }}</td>
						<td>{{ $row->Direccion}} </td>
						
					</tr>
				
					<tr>
						<td width='30%' class='label-view text-right'>{{ SiteHelpers::activeLang('Born', (isset($fields['FechaNacimiento']['language'])? $fields['FechaNacimiento']['language'] : array())) }}</td>
						<td>{{ date('d/m/Y',strtotime($row->FechaNacimiento)) }} </td>
						
					</tr>
				
					<tr>
						<td width='30%' class='label-view text-right'>{{ SiteHelpers::activeLang('Sexo', (isset($fields['Sexo']['language'])? $fields['Sexo']['language'] : array())) }}</td>
						<td>{{ MySourcing::ValorUno($row->{{$row->Sexo}}) }} </td>
						
					</tr>
				
					<tr>
						<td width='30%' class='label-view text-right'>{{ SiteHelpers::activeLang('Civl', (isset($fields['EstadoCivil']['language'])? $fields['EstadoCivil']['language'] : array())) }}</td>
						<td>{{ MySourcing::ValorDos($row->{{$row->EstadoCivil}}) }} </td>
						
					</tr>
				
					<tr>
						<td width='30%' class='label-view text-right'>{{ SiteHelpers::activeLang('Status', (isset($fields['IdEstatus']['language'])? $fields['IdEstatus']['language'] : array())) }}</td>
						<td>{{ MySourcing::Status($row->{{$row->IdEstatus}}) }} </td>
						
					</tr>
				
					<tr>
						<td width='30%' class='label-view text-right'>{{ SiteHelpers::activeLang('Created On', (isset($fields['createdOn']['language'])? $fields['createdOn']['language'] : array())) }}</td>
						<td>{{ date('d/m/Y',strtotime($row->createdOn)) }} </td>
						
					</tr>
				
					<tr>
						<td width='30%' class='label-view text-right'>{{ SiteHelpers::activeLang('Entry By', (isset($fields['entry_by']['language'])? $fields['entry_by']['language'] : array())) }}</td>
						<td>{{ SiteHelpers::formatLookUp($row->entry_by,'entry_by','1:tb_users:id:first_name|last_name') }} </td>
						
					</tr>
				
					<tr>
						<td width='30%' class='label-view text-right'>{{ SiteHelpers::activeLang('Updated On', (isset($fields['updatedOn']['language'])? $fields['updatedOn']['language'] : array())) }}</td>
						<td>{{ date('d/m/Y',strtotime($row->updatedOn)) }} </td>
						
					</tr>
				
					<tr>
						<td width='30%' class='label-view text-right'>{{ SiteHelpers::activeLang('Entry By Access', (isset($fields['entry_by_access']['language'])? $fields['entry_by_access']['language'] : array())) }}</td>
						<td>{{ SiteHelpers::formatLookUp($row->entry_by_access,'entry_by_access','1:tb_users:id:first_name|last_name') }} </td>
						
					</tr>
						
					<tr>
						<td width='30%' class='label-view text-right'></td>
						<td> <a href="javascript:history.go(-1)" class="btn btn-primary"> Back To Grid <a> </td>
						
					</tr>					
				
			</tbody>	
		</table>   

	 
	
	</div>
</div>	