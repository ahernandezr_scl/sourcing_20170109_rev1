@if($setting['view-method'] =='native')
<div class="sbox">
	<div class="sbox-title">  
		<h4> <i class="fa fa-table"></i> <?php echo $pageTitle ;?> <small>{{ $pageNote }}</small>
			<a href="javascript:void(0)" class="collapse-close pull-right btn btn-xs btn-danger" onclick="ajaxViewClose('#{{ $pageModule }}')">
			<i class="fa fa fa-times"></i></a>
		</h4>
	 </div>

	<div class="sbox-content"> 
@endif	

		<table class="table table-striped table-bordered" >
			<tbody>	
				
					<tr>
						<td width='30%' class='label-view text-right'>{{ SiteHelpers::activeLang('RUT', (isset($fields['RUT']['language'])? $fields['RUT']['language'] : array())) }}</td>
						<td>{{ $row->RUT}} </td>

					</tr>
				
					<tr>
						<td width='30%' class='label-view text-right'>{{ SiteHelpers::activeLang('Nombres', (isset($fields['Nombres']['language'])? $fields['Nombres']['language'] : array())) }}</td>
						<td>{{ $row->Nombres}} </td>

					</tr>
				
					<tr>
						<td width='30%' class='label-view text-right'>{{ SiteHelpers::activeLang('Apellidos', (isset($fields['Apellidos']['language'])? $fields['Apellidos']['language'] : array())) }}</td>
						<td>{{ $row->Apellidos}} </td>

					</tr>
				
					<tr>
						<td width='30%' class='label-view text-right'>{{ SiteHelpers::activeLang('Foto', (isset($fields['Foto']['language'])? $fields['Foto']['language'] : array())) }}</td>
						<td>{!! SiteHelpers::formatRows($row->Foto,$fields['Foto'],$row ) !!} </td>

					</tr>
				
					<tr>
						<td width='30%' class='label-view text-right'>{{ SiteHelpers::activeLang('ArchivoFoto', (isset($fields['ArchivoFoto']['language'])? $fields['ArchivoFoto']['language'] : array())) }}</td>
						<td>{{ $row->ArchivoFoto}} </td>

					</tr>
				
					<tr>
						<td width='30%' class='label-view text-right'>{{ SiteHelpers::activeLang('Direccion', (isset($fields['Direccion']['language'])? $fields['Direccion']['language'] : array())) }}</td>
						<td>{{ $row->Direccion}} </td>

					</tr>
				
					<tr>
						<td width='30%' class='label-view text-right'>{{ SiteHelpers::activeLang('Fecha Nacimiento', (isset($fields['FechaNacimiento']['language'])? $fields['FechaNacimiento']['language'] : array())) }}</td>
						<td>{{ $row->FechaNacimiento}} </td>

					</tr>
				
					<tr>
						<td width='30%' class='label-view text-right'>{{ SiteHelpers::activeLang('Sexo', (isset($fields['Sexo']['language'])? $fields['Sexo']['language'] : array())) }}</td>
						<td>{{ $row->Sexo}} </td>

					</tr>
				
					<tr>
						<td width='30%' class='label-view text-right'>{{ SiteHelpers::activeLang('Estado Civil', (isset($fields['EstadoCivil']['language'])? $fields['EstadoCivil']['language'] : array())) }}</td>
						<td>{{ $row->EstadoCivil}} </td>

					</tr>
				
					<tr>
						<td width='30%' class='label-view text-right'>{{ SiteHelpers::activeLang('IdEstatus', (isset($fields['IdEstatus']['language'])? $fields['IdEstatus']['language'] : array())) }}</td>
						<td>{{ $row->IdEstatus}} </td>

					</tr>
				
					<tr>
						<td width='30%' class='label-view text-right'>{{ SiteHelpers::activeLang('CreatedOn', (isset($fields['createdOn']['language'])? $fields['createdOn']['language'] : array())) }}</td>
						<td>{{ $row->createdOn}} </td>

					</tr>
				
					<tr>
						<td width='30%' class='label-view text-right'>{{ SiteHelpers::activeLang('Entry By', (isset($fields['entry_by']['language'])? $fields['entry_by']['language'] : array())) }}</td>
						<td>{{ $row->entry_by}} </td>

					</tr>
				
					<tr>
						<td width='30%' class='label-view text-right'>{{ SiteHelpers::activeLang('UpdatedOn', (isset($fields['updatedOn']['language'])? $fields['updatedOn']['language'] : array())) }}</td>
						<td>{{ $row->updatedOn}} </td>

					</tr>
				
			</tbody>	
		</table>  
			
		 	

@if($setting['form-method'] =='native')
	</div>	
</div>	
@endif	

<script>
$(document).ready(function(){

});
</script>	