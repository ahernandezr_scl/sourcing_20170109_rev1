
@if($setting['form-method'] =='native')
<div class="sbox">
	<div class="sbox-title">
		<h4> <i class="fa fa-table"></i> <?php echo $pageTitle ;?> <small>{{ $pageNote }}</small>
			<a href="javascript:void(0)" class="collapse-close pull-right btn btn-xs btn-danger" onclick="ajaxViewClose('#{{ $pageModule }}')"><i class="fa fa fa-times"></i></a>
		</h4>
	</div>

	<div class="sbox-content">
		@endif
		{!! Form::open(array('url'=>'accesos/save/'.SiteHelpers::encryptID($row['IdAcceso']), 'class'=>'form-horizontal','files' => true , 'parsley-validate'=>'','novalidate'=>' ','id'=> 'accesosFormAjax')) !!}
		<div class="col-md-12">
			<fieldset><legend> accesos</legend>
				{!! Form::hidden('IdAcceso', $row['IdAcceso']) !!}
				<div class="form-group  " >
					<label for="Tipo Acceso" class=" control-label col-md-4 text-left"> Tipo Acceso <span class="asterix"> * </span></label>
					<div class="col-md-6">

						<?php $IdTipoAcceso = explode(',',$row['IdTipoAcceso']);
						$IdTipoAcceso_opt = array( '1' => 'Trabajador' ,  '2' => 'Visitante' , ); ?>
						<?php if (strlen($row['IdTipoAcceso'])==0): ?>
							<select name='IdTipoAcceso' id='IdTipoAcceso' rows='5' required  class='select2 '  >
								<?php
								foreach($IdTipoAcceso_opt as $key=>$val)
								{
									echo "<option  value ='$key' ".($row['IdTipoAcceso'] == $key ? " selected='selected' " : '' ).">$val</option>";
								}
								?></select>
							<?php elseif (strlen($row['IdTipoAcceso'])>0): ?>
								<?php foreach($IdTipoAcceso_opt as $key=>$val): ?>
									<label for="Tipo Acceso" ><?php echo $row['IdTipoAcceso'] == $key ? $val : '' ?></label>
								<?php endforeach; ?>
								{!! Form::hidden('IdTipoAcceso', $row['IdTipoAcceso']) !!}
							<?php endif; ?>
						</div>
						<div class="col-md-2">

						</div>
					</div>
					<div class="form-group  " >
						<label for="Fecha Inicio" class=" control-label col-md-4 text-left"> Fecha Inicio </label>
						<div class="col-md-6">

							<div class="input-group m-b" style="width:150px !important;">
								{!! Form::text('FechaInicio', $row['FechaInicio'],array('class'=>'form-control date','id'=>'FechaI')) !!}
								<span class="input-group-addon"><i class="fa fa-calendar"></i></span>
							</div>
						</div>
						<div class="col-md-2">

						</div>
					</div>
					<div class="form-group  " >
						<label for="Fecha Final" class=" control-label col-md-4 text-left"> Fecha Final </label>
						<div class="col-md-6">

							<div class="input-group m-b" style="width:150px !important;">
								{!! Form::text('FechaFinal', $row['FechaFinal'],array('class'=>'form-control date','id'=>'FechaF')) !!}
								<span class="input-group-addon"><i class="fa fa-calendar"></i></span>
							</div>
						</div>
						<div class="col-md-2">

						</div>
					</div>
					<div id="contContrato">
						<div class="form-group  " >
							<label for="Contrato" class=" control-label col-md-4 text-left"> Contrato </label>
							<div class="col-md-6">
								<?php if (strlen($row['contrato_id'])==0): ?>
									<select name='contrato_id' rows='5' id='contrato_id' class='select2 '   ></select>
								<?php elseif (strlen($row['contrato_id'])>0): ?>
									<?php $IdCont = $row['contrato_id'] ?>
									<label for="ContV" ></label>
									{!! Form::hidden('contrato_id', $row['contrato_id']) !!}
								<?php endif; ?>
								<?php if (is_null($row['contrato_id'])): ?>
									<input type="hidden" id="contrato_id"  name="contrato_id"  value="NULL">
								<?php endif; ?>
							</div>
							<div class="col-md-2">
							</div>
						</div>
						<div class="form-group"  id="ConTodos">
							<label for="Todos" class=" control-label col-md-4 text-left">  </label>
							<div class="col-md-6">
								<input type="checkbox" id="Todos"  name="Todos"> Seleccionar Todos
							</div>
							<div class="col-md-2">
							</div>
						</div>
					</div>
					<div class="form-group  " >
						<label for="Persona" class=" control-label col-md-4 text-left"> Persona <span class="asterix"> * </span></label>
						<div class="col-md-6">
							<?php if (strlen($row['IdPersona'])==0): ?>
								<select name='IdPersona[]' multiple rows='5' id='IdPersona' class='select2 ' required  ></select>
							<?php elseif (strlen($row['IdPersona'])>0): ?>
								<?php $IdPer = $row['IdPersona'] ?>
								<label for="ContP"></label>
								{!! Form::hidden('IdPersona', $row['IdPersona']) !!}
							<?php endif; ?>
						</div>
						<div class="col-md-2">

						</div>
					</div>

					<div class="form-group  " >
						<label for="Estatus" class=" control-label col-md-4 text-left"> Estatus </label>
						<div class="col-md-6">
							<?php   if (strlen($row['IdEstatus'])>0): ?>
								<?php $IdEstatus = explode(',',$row['IdEstatus']);
								$IdEstatus_opt = array( '' => 'Seleccione Valor' ,  '1' => 'Con Acceso' ,  '2' => 'Sin Acceso' , '3' => 'Acceso Temporal' , ); ?>
								<select name='IdEstatus' rows='5'   class='select2 '  >
									<?php
									foreach($IdEstatus_opt as $key=>$val)
									{
										echo "<option  value ='$key' ".($row['IdEstatus'] == $key ? " selected='selected' " : '' ).">$val</option>";
									}
									?></select>
								</div>
								<div class="col-md-2">

								</div>
							</div>
						<?php elseif (strlen($row['IdEstatus'])==0): ?>
							<input type="checkbox" id="IdEstatusC"  name="IdEstatusC"  value="3"> Acceso Temporal
							<input type="hidden" id="IdEstatus"  name="IdEstatus" >
						<?php endif; ?>
						{!! Form::hidden('createdOn', $row['createdOn']) !!}{!! Form::hidden('entry_by', $row['entry_by']) !!}{!! Form::hidden('updatedOn', $row['updatedOn']) !!}
						{!! Form::hidden('IdSolicitudAcceso', $row['IdSolicitudAcceso']) !!}</fieldset>
					</div>



					<hr />
					<div class="clr clear"></div>

					<h5> Areas </h5>

					<div class="table-responsive">
						<table class="table table-striped " id="Centros">
							<thead>
								<tr>
									@foreach ($subform['tableGrid'] as $t)
									@if($t['view'] =='1' && $t['field'] !='IdAcceso' && $t['field'] !='IdEstatus')
									<th>{{ $t['label'] }}</th>
									@endif
									@endforeach
									<th></th>
								</tr>

							</thead>

							<tbody>
								@if(count($subform['rowData'])>=1)

								@foreach ($subform['rowData'] as $rows)
								<tr class="clone clonedInput">

									@foreach ($subform['tableGrid'] as $field)
									@if($field['view'] =='1' && $field['field'] !='IdAcceso' && $field['field'] !='IdEstatus')
									<td>
										{!! SiteHelpers::bulkForm($field['field'] , $subform['tableForm'] , $rows->{$field['field']}) !!}
									</td>
									@endif

									@endforeach
									<td>
										<a onclick=" $(this).parents('.clonedInput').remove(); return false" href="#" class="remove btn btn-xs btn-danger">-</a>
										<input type="hidden" name="counter[]">
									</td>
									@endforeach
								</tr>

								@else

								<tr class="clone clonedInput">

									@foreach ($subform['tableGrid'] as $field)

									@if($field['view'] =='1' && $field['field'] !='IdAcceso' && $field['field'] !='IdEstatus')
									<td>
										{!! SiteHelpers::bulkForm($field['field'] , $subform['tableForm'] ) !!}
									</td>
									@endif

									@endforeach
									<td>
										<a onclick=" $(this).parents('.clonedInput').remove(); return false" href="#" class="remove btn btn-xs btn-danger">-</a>
										<input type="hidden" name="counter[]">
										<input type="hidden" name="bulk_IdEstatus" id="bulk_IdEstatus" value="1" >
									</td>

								</tr>


								@endif


							</tbody>

						</table>
						<input type="hidden" name="enable-masterdetail" value="true">
					</div>
					<br /><br />

					<a href="javascript:void(0);" id="pruebaB" class="addC btn btn-xs btn-info" rel=".clone"><i class="fa fa-plus"></i> New Item</a>
					<hr />

					<div style="clear:both"></div>

					<div class="form-group">
						<label class="col-sm-4 text-right">&nbsp;</label>
						<div class="col-sm-8">
							<button type="submit" class="btn btn-primary btn-sm "><i class="icon-checkmark-circle2"></i>  {{ Lang::get('core.sb_save') }} </button>
							<button type="button" onclick="ajaxViewClose('#{{ $pageModule }}')" class="btn btn-success btn-sm"><i class="icon-cancel-circle2 "></i>  {{ Lang::get('core.sb_cancel') }} </button>
						</div>
					</div>
					{!! Form::close() !!}


					@if($setting['form-method'] =='native')
				</div>
			</div>
			@endif
		</div>

		<script type="text/javascript">
			$(document).ready(function() {
				<?php if ( strlen($row['IdAcceso'])==0): ?>
				$('#IdEstatus').val(2);

				$("#IdEstatusC").on("ifChanged", function (event) {
					if($("#IdEstatusC").is(':checked') ){
						$('#IdEstatus').val(3);
					}else{
						$('#IdEstatus').val(2);
					}

				});
			<?php endif; ?>

			<?php if(count($subform['rowData'])>=1): ?>
			$('.bulk_IdAreaTrabajo').each(function(index) {
				$( this ).attr('id', 'bulk_IdAreaTrabajo'+ (index));
			});

			$(".bulk_IdAreaTrabajo").prop("disabled",false);


	var centros = $("#Centros > tbody > tr").find(".bulk_IdCentro");
	$('.bulk_IdAreaTrabajo').each(function(index,value) {
	valor = centros[index].value;
	area = value.value;
	   $.post('accesos/datasarea',{ id:valor,area:area,},function(response,state){
	   	var fieldId = "#bulk_IdAreaTrabajo" + (index);
	   	$(fieldId).empty()
	   	for (var i =0; i<response.valores.length; i++) {
	   		if (response.valores[i].IdAreaTrabajo==response.resultado)
	   			$(fieldId).append("<option selected value='"+response.valores[i].IdAreaTrabajo+"'>"+response.valores[i].Descripcion+"</option>");
	   		else
	   			$(fieldId).append("<option  value='"+response.valores[i].IdAreaTrabajo+"'>"+response.valores[i].Descripcion+"</option>");

	   	}
	   });
	});

<?php else: ?>
		$(".bulk_IdAreaTrabajo").prop("disabled",true);
<?php endif; ?>


$(".bulk_IdCentro").change(function(event){
	var lobjIdAreaDeTrabajo = $(this).parent("td").parent("tr").find(".bulk_IdAreaTrabajo");
	$.post('accesos/datasarea',{ id:event.target.value,},function(response,state){
		lobjIdAreaDeTrabajo.empty();
		for (var i =0; i<response.valores.length; i++) {
			lobjIdAreaDeTrabajo.append("<option value='"+response.valores[i].IdAreaTrabajo+"'>"+response.valores[i].Descripcion+"</option>");
		}
	});

	var lintIdAreaDeTrabajo;
	var lintExists = 0;
	lobjIdAreaDeTrabajo.find("option").each(function () {
		lintIdAreaDeTrabajo = $(this).val();
		lintExists = 0;
		$(".bulk_IdCentro").each(function () {
			if ($(this).val()==lintIdAreaDeTrabajo) {
				lintExists = 1;
				return false;
			}
		});
		if (!lintExists){
			lobjIdAreaDeTrabajo.find("option[value='"+lintIdAreaDeTrabajo+"']").remove();
		}
	});
	lobjIdAreaDeTrabajo.prop( "disabled", false );
});

<?php if ((strlen($row['contrato_id'])>0) && (strlen($row['IdPersona'])>0)): ?>
	$("#ConTodos").hide();
	var cont = "<?php echo $IdCont?>"
	var per = "<?php echo $IdPer?>"
	$.post('accesos/datacontrato',{ contrato:cont,idpersona:per,},
		function(data) {
			if (data.valores==""){
				//alert("No se encontraron resultados")
			}
			else{
				$("label[for='ContV']").html(data.valores[0].cont_numero);
				$("label[for='ContP']").html(data.valores[0].RUT + " "+ data.valores[0].Nombres + " " + data.valores[0].Apellidos);
			}
		});

<?php elseif ((strlen($row['contrato_id'])==0) && (strlen($row['IdPersona'])==0)): ?>

	$("#IdTipoAcceso").on('change', function() {
		if( this.value==2){
			$("#contContrato").hide();
			$("#IdPersona").jCombo("{!! url('accesos/comboselect?filter=personasncontratadas:IdPersona:RUT|Nombres|Apellidos') !!}",
				{   parent: '', selected_value : '{{ $row["IdPersona"] }}' });
			$("#IdPersona").prop("disabled", false);
		}
		else{
			$("#contContrato").show();
			$("#IdPersona").jCombo("{!! url('accesos/comboselect?filter=personascontratadas:IdPersona:RUT|Nombres|Apellidos') !!}&parent=contrato_id:",
				{  parent: '#contrato_id', selected_value : '{{ $row["IdPersona"] }}' });
		}
	});

	$("#contrato_id").jCombo("{!! url('accesos/comboselect?filter=tbl_contrato:contrato_id:cont_numero') !!}",
		{  selected_value : '{{ $row["contrato_id"] }}' });

	$("#IdPersona").jCombo("{!! url('accesos/comboselect?filter=personascontratadas:IdPersona:RUT|Nombres|Apellidos') !!}&parent=contrato_id:",
		{  parent: '#contrato_id', selected_value : '{{ $row["IdPersona"] }}' });
<?php endif; ?>
<?php if ( strlen($row['IdPersona'])>0): ?>
	$("#contContrato").hide();
	var per = "<?php echo $IdPer?>"
	$.post('accesos/datapersona',{ idpersona:per,},
		function(data) {
			if (data.valores==""){
				//alert("No se encontraron resultados")
			}
			else{
				$("label[for='ContP']").html(data.valores[0].RUT + " "+ data.valores[0].Nombres + " " + data.valores[0].Apellidos);
			}
		});
<?php endif; ?>
$('.addC').relCopy({});
$('.editor').summernote();
$('.previewImage').fancybox();
$('.tips').tooltip();
$(".select2").select2({ width:"98%"});
//$('.date').datepicker({format:'yyyy-mm-dd',autoClose:true})
 $('.date').datepicker({format: "yyyy-mm-dd"}).on('change', function(){$('.datepicker').hide();});
$('.datetime').datetimepicker({format: 'yyyy-mm-dd hh:ii:ss',autoClose:true});
$('input[type="checkbox"],input[type="radio"]').iCheck({
	checkboxClass: 'icheckbox_square-red',
	radioClass: 'iradio_square-red',
});
$('.removeMultiFiles').on('click',function(){
	var removeUrl = '{{ url("accesos/removefiles?file=")}}'+$(this).attr('url');
	$(this).parent().remove();
	$.get(removeUrl,function(response){});
	$(this).parent('div').empty();
	return false;
});

var form = $('#accesosFormAjax');
form.parsley();
form.submit(function(){

	if(form.parsley('isValid') == true){
		var options = {
			dataType:      'json',
			beforeSubmit :  showRequest,
			success:       showResponse
		}
		$(this).ajaxSubmit(options);
		return false;

	} else {
		return false;
	}

});

$("#Todos").on("ifChanged", function (event) {

	$('.i-checks').iCheck('update');
	if($("#Todos").is(':checked') ){
		$("#IdPersona > option").not(':first').prop("selected","selected");
		$("#IdPersona").trigger("change");
	}else{
		$("#IdPersona > option").removeAttr("selected");
		$("#IdPersona").trigger("change");
	}

});



});

function showRequest()
{
	$('.ajaxLoading').show();
}
function showResponse(data)  {

	if(data.status == 'success')
	{
		ajaxViewClose('#{{ $pageModule }}');
		ajaxFilter('#{{ $pageModule }}','{{ $pageUrl }}/data');
		notyMessage(data.message);
		$('#sximo-modal').modal('hide');
	} else {
		notyMessageError(data.message);
		$('.ajaxLoading').hide();
		return false;
	}
}

</script>
<style type="text/css">
	@if(count($subform['rowData'])==0)
	#Centros tbody tr:first-child {
		display:none;
	}
	@endif
</style>
