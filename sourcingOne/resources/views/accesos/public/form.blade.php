

		 {!! Form::open(array('url'=>'accesos/savepublic', 'class'=>'form-horizontal','files' => true , 'parsley-validate'=>'','novalidate'=>' ')) !!}

	@if(Session::has('messagetext'))
	  
		   {!! Session::get('messagetext') !!}
	   
	@endif
	<ul class="parsley-error-list">
		@foreach($errors->all() as $error)
			<li>{{ $error }}</li>
		@endforeach
	</ul>		


<div class="col-md-12">
						<fieldset><legend> accesos</legend>
				{!! Form::hidden('IdAcceso', $row['IdAcceso']) !!}					
									  <div class="form-group  " >
										<label for="Tipo Acceso" class=" control-label col-md-4 text-left"> Tipo Acceso <span class="asterix"> * </span></label>
										<div class="col-md-6">
										  
					<?php $IdTipoAcceso = explode(',',$row['IdTipoAcceso']);
					$IdTipoAcceso_opt = array( '' => 'Seleccione Valor' ,  '1' => 'Trabajador' ,  '2' => 'Visitante' , ); ?>
					<select name='IdTipoAcceso' rows='5' required  class='select2 '  > 
						<?php 
						foreach($IdTipoAcceso_opt as $key=>$val)
						{
							echo "<option  value ='$key' ".($row['IdTipoAcceso'] == $key ? " selected='selected' " : '' ).">$val</option>"; 						
						}						
						?></select> 
										 </div> 
										 <div class="col-md-2">
										 	
										 </div>
									  </div> 					
									  <div class="form-group  " >
										<label for="Fecha Inicio" class=" control-label col-md-4 text-left"> Fecha Inicio </label>
										<div class="col-md-6">
										  
				<div class="input-group m-b" style="width:150px !important;">
					{!! Form::text('FechaInicio', $row['FechaInicio'],array('class'=>'form-control date')) !!}
					<span class="input-group-addon"><i class="fa fa-calendar"></i></span>
				</div> 
										 </div> 
										 <div class="col-md-2">
										 	
										 </div>
									  </div> 					
									  <div class="form-group  " >
										<label for="Fecha Final" class=" control-label col-md-4 text-left"> Fecha Final </label>
										<div class="col-md-6">
										  
				<div class="input-group m-b" style="width:150px !important;">
					{!! Form::text('FechaFinal', $row['FechaFinal'],array('class'=>'form-control date')) !!}
					<span class="input-group-addon"><i class="fa fa-calendar"></i></span>
				</div> 
										 </div> 
										 <div class="col-md-2">
										 	
										 </div>
									  </div> 					
									  <div class="form-group  " >
										<label for="Contrato" class=" control-label col-md-4 text-left"> Contrato </label>
										<div class="col-md-6">
										  <select name='contrato_id' rows='5' id='contrato_id' class='select2 '   ></select> 
										 </div> 
										 <div class="col-md-2">
										 	
										 </div>
									  </div> 					
									  <div class="form-group  " >
										<label for="Persona" class=" control-label col-md-4 text-left"> Persona <span class="asterix"> * </span></label>
										<div class="col-md-6">
										  <select name='IdPersona[]' multiple rows='5' id='IdPersona' class='select2 ' required  ></select> 
										 </div> 
										 <div class="col-md-2">
										 	
										 </div>
									  </div> 					
									  <div class="form-group  " >
										<label for="Estatus" class=" control-label col-md-4 text-left"> Estatus </label>
										<div class="col-md-6">
										  
					<?php $IdEstatus = explode(',',$row['IdEstatus']);
					$IdEstatus_opt = array( '' => 'Seleccione Valor' ,  '1' => 'Activo' ,  '2' => 'Suspendido' , ); ?>
					<select name='IdEstatus' rows='5'   class='select2 '  > 
						<?php 
						foreach($IdEstatus_opt as $key=>$val)
						{
							echo "<option  value ='$key' ".($row['IdEstatus'] == $key ? " selected='selected' " : '' ).">$val</option>"; 						
						}						
						?></select> 
										 </div> 
										 <div class="col-md-2">
										 	
										 </div>
									  </div> {!! Form::hidden('IdSolicitudAcceso', $row['IdSolicitudAcceso']) !!}</fieldset>
			</div>
			
			

			<div style="clear:both"></div>	
				
					
				  <div class="form-group">
					<label class="col-sm-4 text-right">&nbsp;</label>
					<div class="col-sm-8">	
					<button type="submit" name="apply" class="btn btn-info btn-sm" ><i class="fa  fa-check-circle"></i> {{ Lang::get('core.sb_apply') }}</button>
					<button type="submit" name="submit" class="btn btn-primary btn-sm" ><i class="fa  fa-save "></i> {{ Lang::get('core.sb_save') }}</button>
				  </div>	  
			
		</div> 
		 
		 {!! Form::close() !!}
		 
   <script type="text/javascript">
	$(document).ready(function() { 
		$('.addC').relCopy({});
		
		$("#contrato_id").jCombo("{!! url('accesos/comboselect?filter=tbl_contrato:contrato_id:cont_numero') !!}",
		{  selected_value : '{{ $row["contrato_id"] }}' });
		
		$("#IdPersona").jCombo("{!! url('accesos/comboselect?filter=personascontratadas:IdPersona:RUT|Nombres|Apellidos') !!}&parent=contrato_id:",
		{  parent: '#contrato_id', selected_value : '{{ $row["IdPersona"] }}' });
		 

		$('.removeCurrentFiles').on('click',function(){
			var removeUrl = $(this).attr('href');
			$.get(removeUrl,function(response){});
			$(this).parent('div').empty();	
			return false;
		});		
		
	});
	</script>		 
