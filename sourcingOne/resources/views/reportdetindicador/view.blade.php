<?php
$servername = "localhost";
$username = "root";
$password = "8044302";
$year;
$mes;$reg;

#try {
    $conn2 = new PDO("mysql:host=$servername;dbname=andina_koadmin", $username, $password);
    // set the PDO error mode to exception
#    $conn2->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
#   echo "Connected successfully";
#    }
#catch(PDOException $e)
#    {
#    echo "Connection failed: " . $e->getMessage();
#    }


?>

<?php  
  
  if (isset($_GET["year"])) {
    $year = $_GET["year"];
   }
  else{ 
	if(date("m") == 1)
	{
		$year = date("Y") -1;
		
	}
	else if(date("m") == 2 )
	{
		if(date("d")< 20)
		{
			$year = date("Y") -1;
		}
		else
		{
			$year = date("Y");
		}
	}
	else
	{
		$year = date("Y");
	}
    
}

  if (isset($_GET["mes"])) {
    $mes = $_GET["mes"];
   }
  else{  
	if(date("m") == 1)
	{
		if(date("d")< 20)
		{
			$mes = 11;
		}
		else
		{
			$mes = 12;
		} 
	}
	else if(date("m") == 2 )
	{
		if(date("d")< 20)
		{
			$mes = 12;
		}
		else
		{
			$mes = date("m") -1;
		}
	}
	else
	{
		if(date("d")< 20)
		{
			$mes = date("m") -2;
		}
		else
		{
			$mes = date("m") -1;
		}
	}
}

 if (isset($_GET["area"])) {
    $area = $_GET["area"];
   }
  else{  
    $area = 0;
}

 if (isset($_GET["reg"])) {
    $reg = $_GET["reg"];
   }
  else{  
    $reg = 0;
}

 if (isset($_GET["seg"])) {
    $seg = $_GET["seg"];
   }
  else{  
    $seg = 0;
}


 if (isset($_GET["id"])) {
    $id = $_GET["id"];
   }
  else{  
    $id = 0;
}

 if (isset($_GET["ind"])) {
    $ind = $_GET["ind"];
   }
  else{  
    $ind = 0;
}

if (isset($_GET["rep"])) {
    $rep = $_GET["rep"];
   }
  else{  
    $rep = 0;
}

if (isset($_GET["org"])) {
    $org = $_GET["org"];
   }
  else{  
    $org = 0;
}

 if (isset($_GET["proc"])) {
    $proc = $_GET["proc"];
   }
  else{  
    $proc = 0;
}

 if (isset($_GET["tip"])) {
    $tip = $_GET["tip"];
   }
  else{  
    $tip = 0;
}
?>


  
<?php 
if($proc == 0){
     
    if($ind =="adm"){
	 $sql_gral = 'SELECT CAST(AVG(resultado) AS DECIMAL(4,1)) puntaje, (select ambito_nombre from tbl_planes_y_programas_ambitosub where subambito_id = 3) nombre, cont_proveedor

        FROM tbl_calidad_adm_cont a INNER JOIN dim_tiempo 
        ON fec_rev = fecha
        inner join tbl_contrato c on a.contrato_id = c.contrato_id
        WHERE resultado IS NOT NULL
        AND 
        case when '.$mes.'=0 then Anio = '.$year.' else
        mes IN (SELECT DISTINCT mes FROM dim_tiempo WHERE trimestre  = (SELECT DISTINCT trimestre FROM dim_tiempo WHERE mes= '.$mes.' AND Anio = '.$year.'))
        AND Anio = '.$year.' end

        and a.contrato_id ='.$id;
	  
    $sql_det='SELECT \'Administración de Contrato\' nombre,\'\' descr,
              Ntrimestre fecha,resultado dato,  fecha fecv, 0 meta,
              (SELECT ambito_nombre FROM tbl_planes_y_programas_ambitosub WHERE subambito_id = 3) indic


              FROM 

              tbl_calidad_adm_cont a inner join dim_tiempo b on a.fec_rev = b.fecha

              where 
              case when  '.$mes.'=0 then Anio = '.$year.' else
              trimestre  = (SELECT DISTINCT trimestre FROM dim_tiempo
              WHERE mes= '.$mes.' AND Anio = '.$year.') AND Anio = '.$year.'
              end 
              and contrato_id = '.$id.' 
              order by fecv';

     $sql_com='SELECT concat(concat(NMes3L,\'-\'),anio) fecha,b.ambito_nombre, c.ambito_nombre indicador,programas_idDescripcion
           FROM tbl_planes_y_programas a INNER JOIN tbl_planes_y_programas_ambito b ON a.ambito_id = b.ambito_id
           INNER JOIN tbl_planes_y_programas_ambitosub c ON c.subambito_id = a.subambito_id
           INNER JOIN dim_tiempo d ON a.programas_idFechaInicio = d.fecha
           
           where
           case when '.$mes.' = 0 then 1=1 else mes = '.$mes.' end
           and anio = '.$year.'
           and a.ambito_id = 1
           and a.subambito_id = 3
           and a.contrato_id = '.$id.'
            and a.programas_idTipo = 1';

    $sql_acc='SELECT concat(concat(NMes3L,\'-\'),anio) fecha,b.ambito_nombre, c.ambito_nombre indicador,programas_idDescripcion
             FROM tbl_planes_y_programas a INNER JOIN tbl_planes_y_programas_ambito b ON a.ambito_id = b.ambito_id
             INNER JOIN tbl_planes_y_programas_ambitosub c ON c.subambito_id = a.subambito_id
             INNER JOIN dim_tiempo d ON a.programas_idFechaInicio = d.fecha
             
             where
             case when '.$mes.' = 0 then 1=1 else mes = '.$mes.' end
             and anio = '.$year.'
             and a.ambito_id = 1
             and a.subambito_id = 3
             and a.contrato_id = '.$id.'
             and a.programas_idTipo = 2';

    $sql_lin='SELECT \'Real\' serie,NTrimestre fecha, CAST(AVG(resultado) AS DECIMAL(4,1)) valor, 

             \'Auto Evaluación Calidad Administración de Contrato\' titulo, \'Promedio resultado respuestas encuesta Trimestral\' sub, 0 med
            , (SELECT ambito_nombre FROM tbl_planes_y_programas_ambitosub WHERE subambito_id = 3) indic
            FROM tbl_calidad_adm_cont a INNER JOIN dim_tiempo 
            ON fec_rev = fecha
            inner join tbl_contrato c on c.contrato_id = a.contrato_id
            WHERE resultado IS NOT NULL

            and Anio = '.$year.'
            and a.contrato_id = '.$id.'
            GROUP BY 1,2

            UNION ALL

            SELECT DISTINCT \'Meta\' Serie, NTrimestre, CASE WHEN Trimestre IN (1,2) THEN 60 ELSE 70 END meta , 
             \'Auto Evaluación Calidad Administración de Contrato\' titulo, \'Promedio resultado respuestas encuesta Trimestral\' sub  , 0 med
            , (SELECT ambito_nombre FROM tbl_planes_y_programas_ambitosub WHERE subambito_id = 3) indic
            FROM dim_tiempo 

            WHERE anio = '.$year.' AND dia = 1 AND mes >= (SELECT MIN(MONTH(fec_rev)) FROM tbl_calidad_adm_cont WHERE YEAR(fec_rev) = '.$year.')';
  } 
    
  else if($ind =="kpi"){
	  $sql_gral = 'SELECT CAST(AVG(kpi) AS DECIMAL(4,1)) dato, (select ambito_nombre from tbl_planes_y_programas_ambitosub where subambito_id = 1) nombre, cont_proveedor

        FROM
        (SELECT  b.contrato_id,kpiDet_fecha, cont_proveedor,  

        (SUM(
            CASE WHEN kpi_tipo_calc = 1 THEN
                CASE WHEN kpiDet_puntaje >= kpiDet_meta THEN 1 ELSE 0 END
               WHEN kpi_tipo_calc = 2 THEN
            CASE WHEN kpiDet_puntaje <= kpiDet_meta THEN 1 ELSE 0 END
               WHEN kpi_tipo_calc = 3 THEN
            CASE WHEN kpiDet_puntaje BETWEEN kpiDet_min AND kpiDet_max THEN 1 ELSE 0 END
               WHEN kpi_tipo_calc = 4 THEN 1
              
          END )/
        COUNT(DISTINCT a.id_kpi) )*100 kpi
        FROM tbl_kpimensual a INNER JOIN tbl_kpigral b
        ON a.id_kpi = b.id_kpi
        INNER JOIN dim_tiempo ON kpiDet_fecha = fecha
        inner join tbl_contrato c on b.contrato_id = c.contrato_id
        WHERE kpiDet_puntaje IS NOT NULL
        AND 
        case when '.$mes.'=0 then Anio = '.$year.' else
        mes = '.$mes.' AND Anio = '.$year.'
        end

        and c.contrato_id ='.$id.'
        GROUP BY 1,2) asd';
		
    $sql_det = 'SELECT kpi_nombreIndicador nombre,kpi_descripcionFormula descr,
CONCAT(CONCAT(NMes3L,\'-\'),anio) fecha, kpiDet_puntaje  dato , 


CASE WHEN kpiDet_meta is null AND kpiDet_min <  kpidet_max THEN CONCAT(CONCAT(CAST(kpiDet_min AS DECIMAL(4,1)),\' - \'),CAST(kpidet_max AS DECIMAL(4,1))) 
WHEN kpiDet_meta is null AND kpiDet_min >  kpidet_max THEN CONCAT(CONCAT(CAST(kpidet_max AS DECIMAL(4,1)),\' - \'),CAST(kpiDet_min AS DECIMAL(4,1)))
WHEN kpiDet_meta is null AND kpiDet_min =  kpidet_max THEN CAST(kpidet_max AS DECIMAL(4,1)) 
ELSE CAST(kpiDet_meta AS DECIMAL(12,1)) END meta,
 fecha fecv, (SELECT ambito_nombre FROM tbl_planes_y_programas_ambitosub WHERE subambito_id = 1) indic



FROM tbl_kpigral a INNER JOIN tbl_kpimensual b ON a.id_kpi = b.id_kpi
INNER JOIN dim_tiempo c ON kpiDet_fecha = c.fecha

WHERE 
CASE WHEN  '.$mes.'=0 THEN Anio = '.$year.' ELSE
mes = '.$mes.' AND Anio = '.$year.'
END  
AND contrato_id = '.$id.' 
order by fecv';


$sql_lin = 'SELECT \'Real\' serie, fec fecha, cast(AVG(kpi) as DECIMAL(4,1)) valor, 
 \'Promedio Cumplimiento KPI empresas\' titulo, \'(Cumplimiento KPI: KPI Positivos respecto KPIs Totales)\' sub, 0 med
, (SELECT ambito_nombre FROM tbl_planes_y_programas_ambitosub WHERE subambito_id = 1) indic
FROM
(SELECT  b.contrato_id,Fecha_mes  fec,  

(SUM(
  CASE WHEN kpi_tipo_calc = 1 THEN
    CASE WHEN kpiDet_puntaje >= kpiDet_meta THEN 1 ELSE 0 END
       WHEN kpi_tipo_calc = 2 THEN
    CASE WHEN kpiDet_puntaje <= kpiDet_meta THEN 1 ELSE 0 END
       WHEN kpi_tipo_calc = 3 THEN
    CASE WHEN kpiDet_puntaje BETWEEN kpiDet_min AND kpiDet_max THEN 1 ELSE 0 END
       WHEN kpi_tipo_calc = 4 THEN 1
      
  END )/
COUNT(DISTINCT a.id_kpi) )*100 kpi
FROM tbl_kpimensual a INNER JOIN tbl_kpigral b
ON a.id_kpi = b.id_kpi
INNER JOIN dim_tiempo ON kpiDet_fecha = fecha
inner join tbl_contrato c on c.contrato_id = b.contrato_id
WHERE kpiDet_puntaje IS NOT NULL
AND CASE WHEN '.$year.'!=\'\' THEN Anio = '.$year.' ELSE 1=1 END

and b.contrato_id = '.$id.'
GROUP BY 1,2) asd
GROUP BY 1,2

UNION ALL

SELECT DISTINCT \'Meta\' Serie,concat(concat(NMes3L,\'-\'),anio), 
CASE WHEN mes BETWEEN 1 AND 3 THEN 50
WHEN mes BETWEEN 4 AND 6 THEN 60
WHEN mes BETWEEN 7 AND 9 THEN 65
WHEN mes BETWEEN 10 AND 12 THEN 75
  END meta,
 \'Promedio Cumplimiento KPI empresas\' titulo, \'(Cumplimiento KPI: KPI Positivos respecto KPIs Totales)\' sub, 0 med
, (SELECT ambito_nombre FROM tbl_planes_y_programas_ambitosub WHERE subambito_id = 1) indic

FROM dim_tiempo WHERE anio = '.$year.' AND dia = 1 AND mes >= (SELECT MIN(MONTH(kpiDet_fecha)) FROM tbl_kpimensual WHERE YEAR(kpiDet_fecha) = '.$year.')';

 $sql_com='SELECT concat(concat(NMes3L,\'-\'),anio) fecha,b.ambito_nombre, c.ambito_nombre indicador,programas_idDescripcion
           FROM tbl_planes_y_programas a INNER JOIN tbl_planes_y_programas_ambito b ON a.ambito_id = b.ambito_id
           INNER JOIN tbl_planes_y_programas_ambitosub c ON c.subambito_id = a.subambito_id
           INNER JOIN dim_tiempo d ON a.programas_idFechaInicio = d.fecha
           
           where
           case when '.$mes.' = 0 then 1=1 else mes = '.$mes.' end
           and anio = '.$year.'
           and a.ambito_id = 1
           and a.subambito_id = 1
           and a.contrato_id = '.$id.'
            and a.programas_idTipo = 1';

    $sql_acc='SELECT concat(concat(NMes3L,\'-\'),anio) fecha,b.ambito_nombre, c.ambito_nombre indicador,programas_idDescripcion
             FROM tbl_planes_y_programas a INNER JOIN tbl_planes_y_programas_ambito b ON a.ambito_id = b.ambito_id
             INNER JOIN tbl_planes_y_programas_ambitosub c ON c.subambito_id = a.subambito_id
             INNER JOIN dim_tiempo d ON a.programas_idFechaInicio = d.fecha
             
             where
             case when '.$mes.' = 0 then 1=1 else mes = '.$mes.' end
             and anio = '.$year.'
             and a.ambito_id = 1
             and a.subambito_id = 1
             and a.contrato_id = '.$id.'
             and a.programas_idTipo = 2';
   
  } 
   
  else if($ind =="dym") 
   {
	  $sql_gral = 'SELECT cast(AVG(per_moros*100) as DECIMAL(4,1)) valor, (select ambito_nombre from tbl_planes_y_programas_ambitosub where subambito_id = 6) nombre, cont_proveedor

          FROM tbl_morosidad a
          INNER JOIN dim_tiempo d ON fecha_Eval = d.fecha
          inner join tbl_contrato b on a.contrato_id = b.contrato_id
          WHERE 
          case when  '.$mes.'=0 then Anio = '.$year.' else
          mes= '.$mes.' AND Anio = '.$year.'
          end 

          and b.contrato_id ='.$id;
		  
    $sql_det = 'SELECT \'Deudas\' nombre,\'\' descr,CONCAT(CONCAT(NMes3L,\'-\'),anio) fecha,per_moros *100 dato, \'< 30\' meta, fecha fecv
, (SELECT ambito_nombre FROM tbl_planes_y_programas_ambitosub WHERE subambito_id = 6) indic

FROM tbl_morosidad  a INNER JOIN dim_tiempo b ON a.fecha_eval = b.fecha

 
WHERE 
case when  '.$mes.'=0 then Anio = '.$year.' else
mes= '.$mes.' AND Anio = '.$year.'

end
AND contrato_id = '.$id.' 
order by fecv';


$sql_lin = 'SELECT \'Real\' serie,Fecha_mes  fecha,CAST(AVG(per_moros)*100 AS DECIMAL(4,1)) valor,

 \'Predictor Empresarial Promedio Contratista\' titulo, \'(promedio de resultados por empresa según predictor Equifax)\' sub, 0 med
, (SELECT ambito_nombre FROM tbl_planes_y_programas_ambitosub WHERE subambito_id = 6) indic

FROM tbl_morosidad a
INNER JOIN dim_tiempo b ON fecha_Eval = fecha
inner join tbl_contrato c on c.contrato_id = a.contrato_id
where 
case when '.$year.'!=\'\' then Anio = '.$year.' else 1=1 end

and a.contrato_id = '.$id.'
GROUP BY 1,2

UNION ALL

SELECT DISTINCT \'Meta\' Serie, concat(concat(NMes3L,\'-\'),anio), 30  
, \'Predictor Empresarial Promedio Contratista\' titulo, \'(promedio de resultados por empresa según predictor Equifax)\' sub, 0 med
, (SELECT ambito_nombre FROM tbl_planes_y_programas_ambitosub WHERE subambito_id = 6) indic
FROM dim_tiempo
WHERE anio = '.$year.' AND dia = 1 AND mes >= (SELECT MIN(MONTH(fecha_Eval)) FROM tbl_morosidad WHERE YEAR(fecha_Eval) = '.$year.')';

 $sql_com='SELECT concat(concat(NMes3L,\'-\'),anio) fecha,b.ambito_nombre, c.ambito_nombre indicador,programas_idDescripcion
           FROM tbl_planes_y_programas a INNER JOIN tbl_planes_y_programas_ambito b ON a.ambito_id = b.ambito_id
           INNER JOIN tbl_planes_y_programas_ambitosub c ON c.subambito_id = a.subambito_id
           INNER JOIN dim_tiempo d ON a.programas_idFechaInicio = d.fecha
           
           where
           case when '.$mes.' = 0 then 1=1 else mes = '.$mes.' end
           and anio = '.$year.'
           and a.ambito_id = 2
           and a.subambito_id = 6
           and a.contrato_id = '.$id.'
            and a.programas_idTipo = 1';

    $sql_acc='SELECT concat(concat(NMes3L,\'-\'),anio) fecha,b.ambito_nombre, c.ambito_nombre indicador,programas_idDescripcion
             FROM tbl_planes_y_programas a INNER JOIN tbl_planes_y_programas_ambito b ON a.ambito_id = b.ambito_id
             INNER JOIN tbl_planes_y_programas_ambitosub c ON c.subambito_id = a.subambito_id
             INNER JOIN dim_tiempo d ON a.programas_idFechaInicio = d.fecha
             
             where
             case when '.$mes.' = 0 then 1=1 else mes = '.$mes.' end
             and anio = '.$year.'
             and a.ambito_id = 2
             and a.subambito_id = 6
             and a.contrato_id = '.$id.'
             and a.programas_idTipo = 2';
   
  } 

  else if($ind =="esf") 
   {
	 $sql_gral = 'SELECT CAST(AVG(valor) AS DECIMAL(4,1)) dato, (select ambito_nombre from tbl_planes_y_programas_ambitosub where subambito_id = 5) nombre, cont_proveedor
          FROM tbl_eval_financiera a
          INNER JOIN tbl_contrato c ON c.contrato_id = a.contrato_id
          INNER JOIN dim_tiempo d ON fecha_validez = d.fecha
          where a.contrato_id ='.$id.'
          and case when  '.$mes.'=0 then Anio = '.$year.' else
          mes = '.$mes.' AND Anio = '.$year.'
          end ';
		  
     $sql_det = 'SELECT \'Estado Financiero Anual\' nombre,concat(\'Estado Financiero del año \',a.fecha)  descr,CONCAT(CONCAT(NMes3L,\'-\'),anio) fecha, valor dato, \'< 22.5\' meta, 
(SELECT ambito_nombre FROM tbl_planes_y_programas_ambitosub WHERE subambito_id = 5) indic
 FROM tbl_eval_financiera a inner join dim_tiempo b on a.fecha_validez = b.fecha
WHERE Anio ='.$year.'
and
 a.contrato_id = '.$id;


$sql_lin = 'SELECT \'Real\' serie,Anio fecha,cast(AVG(valor) as DECIMAL(4,1)) valor
, \'Promedio probabilidad de riesgo financiero\' titulo, \'(Promedio resultados por empresa)\' sub, 0 med
, (SELECT ambito_nombre FROM tbl_planes_y_programas_ambitosub WHERE subambito_id = 5) indic

FROM tbl_eval_financiera a
inner join tbl_contrato b on a.contrato_id = b.contrato_id
inner join dim_tiempo c on a.fecha_validez = c.fecha
where 
Anio BETWEEN YEAR(CURRENT_DATE)-3 AND YEAR(CURRENT_DATE)
and
 a.contrato_id = '.$id.'

group by 1,2

UNION ALL

SELECT DISTINCT \'Meta\' Serie, anio, 50
, \'Promedio probabilidad de riesgo financiero\' titulo, \'(Promedio resultados por empresa)\' sub, 0 med
, (SELECT ambito_nombre FROM tbl_planes_y_programas_ambitosub WHERE subambito_id = 5) indic
  FROM 
dim_tiempo 
WHERE Anio between YEAR(CURRENT_DATE)-3 AND YEAR(CURRENT_DATE)
and Anio >= (select min(year(fecha_validez)) from tbl_eval_financiera)';

 $sql_com='SELECT concat(concat(NMes3L,\'-\'),anio) fecha,b.ambito_nombre, c.ambito_nombre indicador,programas_idDescripcion
           FROM tbl_planes_y_programas a INNER JOIN tbl_planes_y_programas_ambito b ON a.ambito_id = b.ambito_id
           INNER JOIN tbl_planes_y_programas_ambitosub c ON c.subambito_id = a.subambito_id
           INNER JOIN dim_tiempo d ON a.programas_idFechaInicio = d.fecha
           
           where
           case when '.$mes.' = 0 then 1=1 else mes = '.$mes.' end
           and anio = '.$year.'
           and a.ambito_id = 2
           and a.subambito_id = 5
           and a.contrato_id = '.$id.'
            and a.programas_idTipo = 1';

    $sql_acc='SELECT concat(concat(NMes3L,\'-\'),anio) fecha,b.ambito_nombre, c.ambito_nombre indicador,programas_idDescripcion
             FROM tbl_planes_y_programas a INNER JOIN tbl_planes_y_programas_ambito b ON a.ambito_id = b.ambito_id
             INNER JOIN tbl_planes_y_programas_ambitosub c ON c.subambito_id = a.subambito_id
             INNER JOIN dim_tiempo d ON a.programas_idFechaInicio = d.fecha
             
             where
             case when '.$mes.' = 0 then 1=1 else mes = '.$mes.' end
             and anio = '.$year.'
             and a.ambito_id = 2
             and a.subambito_id = 5
             and a.contrato_id = '.$id.'
             and a.programas_idTipo = 2';
    
  }
  else if($ind =="evp") 
  {
	  $sql_gral = 'Select cast( AVG(eval_puntaje) as decimal(4,1)) puntaje, (select ambito_nombre from tbl_planes_y_programas_ambitosub where subambito_id = 2) nombre, cont_proveedor

        FROM tbl_evalcontratistas a INNER JOIN dim_tiempo 
        ON eval_fecha = fecha 
        INNER JOIN tbl_contrato b ON a.contrato_id = b.contrato_id
        where  
        eval_puntaje IS NOT NULL
        AND 
        case when '.$mes.'=0 then Anio = '.$year.' else
        mes IN (SELECT DISTINCT mes FROM dim_tiempo WHERE semestre  = (SELECT DISTINCT semestre FROM dim_tiempo WHERE mes= '.$mes.' AND Anio = '.$year.'))
         AND Anio = '.$year.' end

        and a.contrato_id ='.$id;
		
    $sql_det = 'SELECT \'Evaluación Contratista\' nombre,COALESCE(eval_comentario,\' \') descr,NSemestre  fecha,COALESCE(eval_puntaje,0) dato , 
\'> 75\' meta, fecha fecv, (SELECT ambito_nombre FROM tbl_planes_y_programas_ambitosub WHERE subambito_id = 2) indic


FROM tbl_evalcontratistas a INNER JOIN dim_tiempo b ON a.eval_fecha = b.fecha

WHERE 
CASE WHEN  '.$mes.'=0 THEN Anio = '.$year.' ELSE
 semestre  = (SELECT DISTINCT Semestre FROM dim_tiempo
WHERE mes= '.$mes.' AND Anio = '.$year.') AND Anio = '.$year.'
END 
AND contrato_id = '.$id.' 
order by fecv';


$sql_lin = 'SELECT \'Real\' serie,NSemestre fecha, CAST(AVG(eval_puntaje) AS DECIMAL(4,1)) valor
, \'Evaluación Proveedor\' titulo, \'Promedio evaluación de empresas\' sub, 0 med
, (SELECT ambito_nombre FROM tbl_planes_y_programas_ambitosub WHERE subambito_id = 2) indic
FROM tbl_evalcontratistas a INNER JOIN dim_tiempo 
ON eval_fecha = fecha
INNER JOIN tbl_contrato b ON a.contrato_id = b.contrato_id
WHERE eval_puntaje IS NOT NULL
AND  Anio = '.$year.' 

and a.contrato_id = '.$id.'
GROUP BY 1,2

UNION ALL

SELECT DISTINCT \'Meta\' Serie, NSemestre, 75
, \'Evaluación Proveedor\' titulo, \'Promedio evaluación de empresas\' sub, 0 med
, (SELECT ambito_nombre FROM tbl_planes_y_programas_ambitosub WHERE subambito_id = 2) indic
 FROM dim_tiempo 


WHERE anio = '.$year.' AND dia = 1 AND mes >= (SELECT MIN(MONTH(eval_fecha)) FROM tbl_evalcontratistas WHERE YEAR(eval_fecha) = '.$year.')';

 $sql_com='SELECT concat(concat(NMes3L,\'-\'),anio) fecha,b.ambito_nombre, c.ambito_nombre indicador,programas_idDescripcion
           FROM tbl_planes_y_programas a INNER JOIN tbl_planes_y_programas_ambito b ON a.ambito_id = b.ambito_id
           INNER JOIN tbl_planes_y_programas_ambitosub c ON c.subambito_id = a.subambito_id
           INNER JOIN dim_tiempo d ON a.programas_idFechaInicio = d.fecha
           
           where
           case when '.$mes.' = 0 then 1=1 else mes = '.$mes.' end
           and anio = '.$year.'
           and a.ambito_id = 1
           and a.subambito_id = 2
           and a.contrato_id = '.$id.'
            and a.programas_idTipo = 1';

    $sql_acc='SELECT concat(concat(NMes3L,\'-\'),anio) fecha,b.ambito_nombre, c.ambito_nombre indicador,programas_idDescripcion
             FROM tbl_planes_y_programas a INNER JOIN tbl_planes_y_programas_ambito b ON a.ambito_id = b.ambito_id
             INNER JOIN tbl_planes_y_programas_ambitosub c ON c.subambito_id = a.subambito_id
             INNER JOIN dim_tiempo d ON a.programas_idFechaInicio = d.fecha
             
             where
             case when '.$mes.' = 0 then 1=1 else mes = '.$mes.' end
             and anio = '.$year.'
             and a.ambito_id = 1
             and a.subambito_id = 2
             and a.contrato_id = '.$id.'
             and a.programas_idTipo = 2';
  } 
  else if($ind =="fct") 
  {
	  $sql_gral = 'SELECT CAST(AVG((PERC_COND + perc_equip)/2) AS DECIMAL(4,1)) valor, (select ambito_nombre from tbl_planes_y_programas_ambitosub where subambito_id = 14) nombre, cont_proveedor
        FROM tbl_fiscalización a
        INNER JOIN dim_tiempo d ON a.fecha = d.fecha
        INNER JOIN tbl_contrato c ON a.contrato_id = c.contrato_id

         WHERE 
        CASE WHEN  '.$mes.'=0 THEN Anio = '.$year.' ELSE
        mes= '.$mes.' AND Anio = '.$year.'
        END

        AND c.contrato_id ='.$id;
	  
     $sql_det = '

SELECT \'% Condiciones\' nombre, \'\' descr, concat(concat(NMes3L,\'-\'),anio) fecha, perc_cond dato ,  \'>75 \' meta, a.fecha fecv
, (SELECT ambito_nombre FROM tbl_planes_y_programas_ambitosub WHERE subambito_id = 14) indic
FROM tbl_fiscalización a INNER JOIN dim_tiempo b ON a.fecha = b.fecha

WHERE 
case when  '.$mes.'=0 then Anio = '.$year.' else
mes= '.$mes.' AND Anio = '.$year.'

end
AND contrato_id = '.$id.' 

UNION ALL

SELECT \'% Equipamiento\' nom, \'\' descr, concat(concat(NMes3L,\'-\'),anio) fecha, perc_equip  ,  \'>75 \' meta, a.fecha fecv
, (SELECT ambito_nombre FROM tbl_planes_y_programas_ambitosub WHERE subambito_id = 14) indic 

FROM tbl_fiscalización a INNER JOIN dim_tiempo b ON a.fecha = b.fecha

 
WHERE 
case when  '.$mes.'=0 then Anio = '.$year.' else
mes= '.$mes.' AND Anio = '.$year.'

end
AND contrato_id = '.$id.' 

order by fecv';


$sql_lin = 'SELECT \'Real\' serie, Fecha_mes fecha ,CAST(AVG((PERC_COND + perc_equip)/2) AS DECIMAL(4,1)) valor, 
\'Fiscalización Condiciones de Trabajo\' titulo,  \'(Promedio % porcentaje cumplimiento)\' sub, 0 med
, (SELECT ambito_nombre FROM tbl_planes_y_programas_ambitosub WHERE subambito_id = 14) indic

FROM tbl_fiscalización a
INNER JOIN dim_tiempo b ON a.fecha = b.fecha
inner join tbl_contrato c on c.contrato_id = a.contrato_id
WHERE 
CASE WHEN '.$year.'!=\'\' THEN Anio = '.$year.' ELSE 1=1 END

and a.contrato_id = '.$id.'
GROUP BY 1,2

UNION ALL

SELECT DISTINCT \'Meta\' Serie, concat(concat(NMes3L,\'-\'),anio), case when mes between 1 and 6 then 80 else 90 end , 
\'Fiscalización Condiciones de Trabajo\' titulo,  \'(Promedio % porcentaje cumplimiento)\' sub, 0 med
, (SELECT ambito_nombre FROM tbl_planes_y_programas_ambitosub WHERE subambito_id = 14) indic
FROM dim_tiempo
WHERE anio = '.$year.' AND dia = 1 AND mes >= (SELECT MIN(MONTH(fecha)) FROM tbl_fiscalización WHERE YEAR(fecha) = '.$year.')
';

 $sql_com='SELECT concat(concat(NMes3L,\'-\'),anio) fecha,b.ambito_nombre, c.ambito_nombre indicador,programas_idDescripcion
           FROM tbl_planes_y_programas a INNER JOIN tbl_planes_y_programas_ambito b ON a.ambito_id = b.ambito_id
           INNER JOIN tbl_planes_y_programas_ambitosub c ON c.subambito_id = a.subambito_id
           INNER JOIN dim_tiempo d ON a.programas_idFechaInicio = d.fecha
           
           where
           case when '.$mes.' = 0 then 1=1 else mes = '.$mes.' end
           and anio = '.$year.'
           and a.ambito_id = 4
           and a.subambito_id = 14
           and a.contrato_id = '.$id.'
            and a.programas_idTipo = 1';

    $sql_acc='SELECT concat(concat(NMes3L,\'-\'),anio) fecha,b.ambito_nombre, c.ambito_nombre indicador,programas_idDescripcion
             FROM tbl_planes_y_programas a INNER JOIN tbl_planes_y_programas_ambito b ON a.ambito_id = b.ambito_id
             INNER JOIN tbl_planes_y_programas_ambitosub c ON c.subambito_id = a.subambito_id
             INNER JOIN dim_tiempo d ON a.programas_idFechaInicio = d.fecha
             
             where
             case when '.$mes.' = 0 then 1=1 else mes = '.$mes.' end
             and anio = '.$year.'
             and a.ambito_id = 4
             and a.subambito_id = 14
             and a.contrato_id = '.$id.'
             and a.programas_idTipo = 2';
  } 
  else if($ind =="fla") 
{
	$sql_gral = 'SELECT  CAST(AVG((PERC_LAB)) AS DECIMAL(4,1)) puntaje, (select ambito_nombre from tbl_planes_y_programas_ambitosub where subambito_id = 11) nombre, cont_proveedor
          FROM tbl_fiscalización a INNER JOIN dim_tiempo b
          ON a.fecha = b.fecha
          INNER JOIN tbl_contrato c ON a.contrato_id = c.contrato_id
          WHERE CASE WHEN  '.$mes.'=0 THEN Anio = '.$year.' ELSE
          mes = '.$mes.' AND Anio = '.$year.' END
          
          and c.contrato_id ='.$id;
    $sql_det = 'SELECT \'Fiscalización laboral\' nombre, \'\' descr, concat(concat(NMes3L,\'-\'),anio) fecha, perc_lab  dato , \'> 75\' meta, a.fecha fecv
, (SELECT ambito_nombre FROM tbl_planes_y_programas_ambitosub WHERE subambito_id = 11) indic
FROM tbl_fiscalización a INNER JOIN dim_tiempo b ON a.fecha = b.fecha

WHERE 
case when  '.$mes.'=0 then Anio = '.$year.' else
mes= '.$mes.' AND Anio = '.$year.'

end
AND contrato_id = '.$id.' 
order by fecv';


$sql_lin = 'SELECT \'Real\' serie ,Fecha_mes  fecha ,cast(AVG(perc_lab) as decimal(4,1)) valor,
\'Fiscalización Laboral\' titulo,  \'(Promedio % porcentaje cumplimiento)\' sub, 0 med
, (SELECT ambito_nombre FROM tbl_planes_y_programas_ambitosub WHERE subambito_id = 11) indic

FROM tbl_fiscalización a
INNER JOIN dim_tiempo b
ON a.fecha = b.fecha
inner join tbl_contrato c on c.contrato_id = a.contrato_id
WHERE   Anio = '.$year.'

and a.contrato_id = '.$id.'
GROUP BY 1,2

UNION ALL

SELECT DISTINCT \'Meta\' Serie, concat(concat(NMes,\'-\'),anio), CASE WHEN mes BETWEEN 1 AND 6 THEN 70 ELSE 80 END, 
\'Fiscalización Laboral\' titulo,  \'(Promedio % porcentaje cumplimiento)\' sub, 0 med
, (SELECT ambito_nombre FROM tbl_planes_y_programas_ambitosub WHERE subambito_id = 11) indic
 FROM dim_tiempo 

WHERE anio = '.$year.' AND dia = 1 AND mes >= (SELECT MIN(MONTH(fecha)) FROM tbl_fiscalización WHERE YEAR(fecha) = '.$year.')';

 $sql_com='SELECT concat(concat(NMes3L,\'-\'),anio) fecha,b.ambito_nombre, c.ambito_nombre indicador,programas_idDescripcion
           FROM tbl_planes_y_programas a INNER JOIN tbl_planes_y_programas_ambito b ON a.ambito_id = b.ambito_id
           INNER JOIN tbl_planes_y_programas_ambitosub c ON c.subambito_id = a.subambito_id
           INNER JOIN dim_tiempo d ON a.programas_idFechaInicio = d.fecha
           
           where
           case when '.$mes.' = 0 then 1=1 else mes = '.$mes.' end
           and anio = '.$year.'
           and a.ambito_id = 3
           and a.subambito_id = 11
           and a.contrato_id = '.$id.'
            and a.programas_idTipo = 1';

    $sql_acc='SELECT concat(concat(NMes3L,\'-\'),anio) fecha,b.ambito_nombre, c.ambito_nombre indicador,programas_idDescripcion
             FROM tbl_planes_y_programas a INNER JOIN tbl_planes_y_programas_ambito b ON a.ambito_id = b.ambito_id
             INNER JOIN tbl_planes_y_programas_ambitosub c ON c.subambito_id = a.subambito_id
             INNER JOIN dim_tiempo d ON a.programas_idFechaInicio = d.fecha
             
             where
             case when '.$mes.' = 0 then 1=1 else mes = '.$mes.' end
             and anio = '.$year.'
             and a.ambito_id = 3
             and a.subambito_id = 11
             and a.contrato_id = '.$id.'
             and a.programas_idTipo = 2';
  } 
  else if($ind =="gar") 
{
	$sql_gral = 'SELECT  CAST(AVG((total_fondos + prendas)/(mutuos + total_impacto))*100 AS DECIMAL(4,1)) puntaje, (select ambito_nombre from tbl_planes_y_programas_ambitosub where subambito_id = 8) nombre, cont_proveedor

          FROM tbl_garantias a INNER JOIN dim_tiempo b
          ON a.fecha = b.fecha
          inner join tbl_contrato c on a.contrato_id = c.contrato_id
          WHERE CASE WHEN  '.$mes.'=0 THEN Anio = '.$year.' ELSE
          mes = '.$mes.' AND Anio = '.$year.' END

          and c.contrato_id ='.$id;
		  
    $sql_det = 'SELECT \'Total Impacto\' nombre, \'\' descr,concat(concat(NMes3L,\'-\'),anio) fecha,total_impacto dato, \'--\' meta, a.fecha fecv
, (SELECT ambito_nombre FROM tbl_planes_y_programas_ambitosub WHERE subambito_id = 8) indic
FROM tbl_garantias a INNER JOIN dim_tiempo b ON a.fecha = b.fecha

WHERE 
CASE WHEN  '.$mes.'=0 THEN Anio = '.$year.' ELSE
mes = '.$mes.' AND Anio = '.$year.'
END 
AND contrato_id = '.$id.' 

UNION ALL
SELECT \'Total Fondos\' nombre, \'\' descr,concat(concat(NMes3L,\'-\'),anio) fecha,total_fondos , \'--\' meta, a.fecha fecv
, (SELECT ambito_nombre FROM tbl_planes_y_programas_ambitosub WHERE subambito_id = 8) indic
FROM tbl_garantias a INNER JOIN dim_tiempo b ON a.fecha = b.fecha

WHERE 
CASE WHEN  '.$mes.'=0 THEN Anio = '.$year.' ELSE
mes = '.$mes.' AND Anio = '.$year.'
END 
AND contrato_id = '.$id.' 

UNION ALL
SELECT \'Mutuos\' nombre, \'\' descr,concat(concat(NMes3L,\'-\'),anio) fecha,mutuos ,  \'--\' meta, a.fecha fecv
, (SELECT ambito_nombre FROM tbl_planes_y_programas_ambitosub WHERE subambito_id = 8) indic
FROM tbl_garantias a INNER JOIN dim_tiempo b ON a.fecha = b.fecha

WHERE 
CASE WHEN  '.$mes.'=0 THEN Anio = '.$year.' ELSE
mes = '.$mes.' AND Anio = '.$year.'
END 
AND contrato_id = '.$id.' 

UNION ALL
SELECT \'Prendas\' nombre, \'\' descr,concat(concat(NMes3L,\'-\'),anio) fecha,prendas , \'--\' meta, a.fecha fecv
, (SELECT ambito_nombre FROM tbl_planes_y_programas_ambitosub WHERE subambito_id = 8) indic
FROM tbl_garantias a INNER JOIN dim_tiempo b ON a.fecha = b.fecha

WHERE 
CASE WHEN  '.$mes.'=0 THEN Anio = '.$year.' ELSE
mes = '.$mes.' AND Anio = '.$year.'
END 
AND contrato_id = '.$id.' 

order by fecv
';


$sql_lin = 'SELECT \'Real\' serie,Fecha_mes fecha , CAST(AVG((total_fondos + prendas)/(mutuos + total_impacto))*100 AS DECIMAL(4,1)) valor ,
\'Garantías y Respaldos\' titulo,  \'(Promedio de cobertura de cada empresa)\' sub, 0 med
, (SELECT ambito_nombre FROM tbl_planes_y_programas_ambitosub WHERE subambito_id = 8) indic

FROM tbl_garantias a INNER JOIN dim_tiempo b
ON a.fecha = b.fecha
inner join tbl_contrato c on c.contrato_id = a.contrato_id
WHERE  Anio = '.$year.' 

and a.contrato_id = '.$id.'
GROUP BY 1,2

UNION ALL

SELECT DISTINCT \'Meta\' Serie, concat(concat(NMes3L,\'-\'),anio), 
case when semestre = 1 then 50 else 75 end meta,\'Garantías y Respaldos\' titulo,
  \'(Promedio de cobertura de cada empresa)\' sub, 0 med
, (SELECT ambito_nombre FROM tbl_planes_y_programas_ambitosub WHERE subambito_id = 8) indic
 FROM dim_tiempo 
WHERE anio = '.$year.' AND dia = 1 AND mes >= (SELECT MIN(MONTH(fecha)) FROM tbl_garantias WHERE YEAR(fecha) = '.$year.')';

 $sql_com='SELECT concat(concat(NMes3L,\'-\'),anio) fecha,b.ambito_nombre, c.ambito_nombre indicador,programas_idDescripcion
           FROM tbl_planes_y_programas a INNER JOIN tbl_planes_y_programas_ambito b ON a.ambito_id = b.ambito_id
           INNER JOIN tbl_planes_y_programas_ambitosub c ON c.subambito_id = a.subambito_id
           INNER JOIN dim_tiempo d ON a.programas_idFechaInicio = d.fecha
           
           where
           case when '.$mes.' = 0 then 1=1 else mes = '.$mes.' end
           and anio = '.$year.'
           and a.ambito_id = 2
           and a.subambito_id = 8
           and a.contrato_id = '.$id.'
            and a.programas_idTipo = 1';

    $sql_acc='SELECT concat(concat(NMes3L,\'-\'),anio) fecha,b.ambito_nombre, c.ambito_nombre indicador,programas_idDescripcion
             FROM tbl_planes_y_programas a INNER JOIN tbl_planes_y_programas_ambito b ON a.ambito_id = b.ambito_id
             INNER JOIN tbl_planes_y_programas_ambitosub c ON c.subambito_id = a.subambito_id
             INNER JOIN dim_tiempo d ON a.programas_idFechaInicio = d.fecha
             
             where
             case when '.$mes.' = 0 then 1=1 else mes = '.$mes.' end
             and anio = '.$year.'
             and a.ambito_id = 2
             and a.subambito_id = 8
             and a.contrato_id = '.$id.'
             and a.programas_idTipo = 2';
     } 
  else if($ind =="acc") 
 {
	 $sql_gral = 'SELECT CAST(SUM(n_accid)/SUM(hh)*200000 AS DECIMAL(4,2)) dato , (select ambito_nombre from tbl_planes_y_programas_ambitosub where subambito_id = 12) nombre, cont_proveedor

        FROM tbl_accidentes a
        INNER JOIN dim_tiempo d ON a.fecha_informe = d.fecha
        INNER JOIN tbl_contrato c ON a.contrato_id = c.contrato_id

         WHERE 
        CASE WHEN  '.$mes.'=0 THEN Anio = '.$year.' ELSE
        mes= '.$mes.' AND Anio = '.$year.'
        END

        AND c.contrato_id ='.$id;
		
    $sql_det = 'SELECT \'N° de Accidentes\' nombre, \'\' descr,concat(concat(NMes3L,\'-\'),anio)  fecha, n_accid dato,  \'--\' meta, fecha fecv
, (SELECT ambito_nombre FROM tbl_planes_y_programas_ambitosub WHERE subambito_id = 12) indic
FROM tbl_accidentes a INNER JOIN dim_tiempo b ON a.fecha_informe = b.fecha

WHERE 
CASE WHEN  '.$mes.'=0 THEN Anio = '.$year.' ELSE
mes = '.$mes.' AND Anio = '.$year.'
END 
AND contrato_id = '.$id.' 

UNION ALL

SELECT \'Horas Hombres\' nombre, \'\' descr, concat(concat(NMes3L,\'-\'),anio) , hh dato, \'--\' meta, fecha fecv
, (SELECT ambito_nombre FROM tbl_planes_y_programas_ambitosub WHERE subambito_id = 12) indic
FROM tbl_accidentes a INNER JOIN dim_tiempo b ON a.fecha_informe = b.fecha

WHERE 
CASE WHEN  '.$mes.'=0 THEN Anio = '.$year.' ELSE
mes = '.$mes.' AND Anio = '.$year.'
END 
AND contrato_id = '.$id.' 
order by fecv';


$sql_lin = 'SELECT \'Real\' serie,Fecha_mes  fecha,CAST(SUM(n_accid)/SUM(hh)*200000 AS DECIMAL(4,1)) valor, 
\'Índice LTIR\' titulo,  \'(N° Accidentes/HH *200000)\' sub , 1 med
, (SELECT ambito_nombre FROM tbl_planes_y_programas_ambitosub WHERE subambito_id = 12) indic
FROM tbl_accidentes a
INNER JOIN dim_tiempo ON fecha_informe = fecha
inner join tbl_contrato c on c.contrato_id = a.contrato_id
WHERE 
Anio = '.$year.' 

and a.contrato_id = '.$id.'

GROUP BY 1,2

UNION ALL

SELECT DISTINCT \'Meta\' Serie, concat(concat(NMes3L,\'-\'),anio), 1.5, \'Índice LTIR\' titulo,  \'(N° Accidentes/HH *200000)\' sub, 1 med
, (SELECT ambito_nombre FROM tbl_planes_y_programas_ambitosub WHERE subambito_id = 12) indic
  FROM dim_tiempo
WHERE anio = '.$year.' AND dia = 1 AND mes >= (SELECT MIN(MONTH(fecha_informe)) FROM tbl_accidentes WHERE YEAR(fecha_informe) = '.$year.')';

 $sql_com='SELECT concat(concat(NMes3L,\'-\'),anio) fecha,b.ambito_nombre, c.ambito_nombre indicador,programas_idDescripcion
           FROM tbl_planes_y_programas a INNER JOIN tbl_planes_y_programas_ambito b ON a.ambito_id = b.ambito_id
           INNER JOIN tbl_planes_y_programas_ambitosub c ON c.subambito_id = a.subambito_id
           INNER JOIN dim_tiempo d ON a.programas_idFechaInicio = d.fecha
           
           where
           case when '.$mes.' = 0 then 1=1 else mes = '.$mes.' end
           and anio = '.$year.'
           and a.ambito_id = 4
           and a.subambito_id = 12
           and a.contrato_id = '.$id.'
            and a.programas_idTipo = 1';

    $sql_acc='SELECT concat(concat(NMes3L,\'-\'),anio) fecha,b.ambito_nombre, c.ambito_nombre indicador,programas_idDescripcion
             FROM tbl_planes_y_programas a INNER JOIN tbl_planes_y_programas_ambito b ON a.ambito_id = b.ambito_id
             INNER JOIN tbl_planes_y_programas_ambitosub c ON c.subambito_id = a.subambito_id
             INNER JOIN dim_tiempo d ON a.programas_idFechaInicio = d.fecha
             
             where
             case when '.$mes.' = 0 then 1=1 else mes = '.$mes.' end
             and anio = '.$year.'
             and a.ambito_id = 4
             and a.subambito_id = 12
             and a.contrato_id = '.$id.'
             and a.programas_idTipo = 2';
  } 
  else if($ind =="gra") 
  {
	  $sql_gral = 'SELECT CAST(SUM(DIAS_PERD)/SUM(hh)*200000 AS DECIMAL(4,2)) dato, (select ambito_nombre from tbl_planes_y_programas_ambitosub where subambito_id = 13) nombre, cont_proveedor
        FROM tbl_accidentes a
        INNER JOIN dim_tiempo d ON a.fecha_informe = d.fecha
        INNER JOIN tbl_contrato c ON a.contrato_id = c.contrato_id

         WHERE 
        CASE WHEN  '.$mes.'=0 THEN Anio = '.$year.' ELSE
        mes= '.$mes.' AND Anio = '.$year.'
        END

        AND c.contrato_id ='.$id;
		
    $sql_det = 'SELECT \'Días Perdidos\' nombre, \'\' descr, concat(concat(NMes3L,\'-\'),anio)  fecha, DIAS_PERD dato,  \'--\' meta, fecha fecv
, (SELECT ambito_nombre FROM tbl_planes_y_programas_ambitosub WHERE subambito_id = 13) indic
FROM tbl_accidentes a INNER JOIN dim_tiempo b ON a.fecha_informe = b.fecha

WHERE 
CASE WHEN  '.$mes.'=0 THEN Anio = '.$year.' ELSE
mes = '.$mes.' AND Anio = '.$year.'
END 
AND contrato_id = '.$id.' 

UNION ALL
SELECT \'Horas Hombres\' nombre, \'\' descr, concat(concat(NMes3L,\'-\'),anio) , hh,  \'--\', fecha
, (SELECT ambito_nombre FROM tbl_planes_y_programas_ambitosub WHERE subambito_id = 13) indic
FROM tbl_accidentes a INNER JOIN dim_tiempo b ON a.fecha_informe = b.fecha

WHERE 
CASE WHEN  '.$mes.'=0 THEN Anio = '.$year.' ELSE
mes = '.$mes.' AND Anio = '.$year.'
END 
AND contrato_id = '.$id.' 

order by fecv';


$sql_lin = '
SELECT \'Real\' serie,Fecha_mes fecha ,CAST(SUM(DIAS_PERD)/SUM(hh)*200000 AS DECIMAL(4,1)) valor ,
\'Índice LTISR\' titulo,  \'(N° Días Perdidos/HH *200000)\' sub, 1 med
, (SELECT ambito_nombre FROM tbl_planes_y_programas_ambitosub WHERE subambito_id = 13) indic
FROM tbl_accidentes a
INNER JOIN dim_tiempo ON fecha_informe = fecha
inner join tbl_contrato c on c.contrato_id = a.contrato_id
WHERE 
CASE WHEN '.$year.'!=\'\' THEN Anio = '.$year.' ELSE 1=1 END

and a.contrato_id = '.$id.'
GROUP BY 1,2

UNION ALL

SELECT DISTINCT \'Meta\' Serie, concat(concat(NMes3L,\'-\'),anio), 20,
\'Índice LTISR\' titulo,  \'(N° Días Perdidos/HH *200000)\' sub, 1 med
, (SELECT ambito_nombre FROM tbl_planes_y_programas_ambitosub WHERE subambito_id = 13) indic
 FROM dim_tiempo
WHERE anio = '.$year.' AND dia = 1 AND mes >= (SELECT MIN(MONTH(fecha_Eval)) FROM tbl_morosidad WHERE YEAR(fecha_Eval) = '.$year.')';
 $sql_com='SELECT concat(concat(NMes3L,\'-\'),anio) fecha,b.ambito_nombre, c.ambito_nombre indicador,programas_idDescripcion
           FROM tbl_planes_y_programas a INNER JOIN tbl_planes_y_programas_ambito b ON a.ambito_id = b.ambito_id
           INNER JOIN tbl_planes_y_programas_ambitosub c ON c.subambito_id = a.subambito_id
           INNER JOIN dim_tiempo d ON a.programas_idFechaInicio = d.fecha
           
           where
           case when '.$mes.' = 0 then 1=1 else mes = '.$mes.' end
           and anio = '.$year.'
           and a.ambito_id = 4
           and a.subambito_id = 13
           and a.contrato_id = '.$id.'
            and a.programas_idTipo = 1';

    $sql_acc='SELECT concat(concat(NMes3L,\'-\'),anio) fecha,b.ambito_nombre, c.ambito_nombre indicador,programas_idDescripcion
             FROM tbl_planes_y_programas a INNER JOIN tbl_planes_y_programas_ambito b ON a.ambito_id = b.ambito_id
             INNER JOIN tbl_planes_y_programas_ambitosub c ON c.subambito_id = a.subambito_id
             INNER JOIN dim_tiempo d ON a.programas_idFechaInicio = d.fecha
             
             where
             case when '.$mes.' = 0 then 1=1 else mes = '.$mes.' end
             and anio = '.$year.'
             and a.ambito_id = 4
             and a.subambito_id = 13
             and a.contrato_id = '.$id.'
             and a.programas_idTipo = 2';

  }  
  else if($ind =="mit") {
	  
	  $sql_gral = 'SELECT  COUNT(DISTINCT a.contrato_id) dato, (select ambito_nombre from tbl_planes_y_programas_ambitosub where subambito_id = 10) nombre, cont_proveedor
          FROM tbl_contratistacondicionfin a
          INNER JOIN dim_tiempo b
          ON a.finCont_fecha = b.fecha
          INNER JOIN tbl_contrato c ON a.contrato_id = c.contrato_id
          WHERE CASE WHEN  '.$mes.'=0 THEN Anio = '.$year.' ELSE
          mes = '.$mes.' AND Anio = '.$year.' END
          
          and c.contrato_id ='.$id;
		  
     $sql_det = 'SELECT \'Multas\' nombre, finCont_comentario descr,concat(concat(NMes3L,\'-\'),anio)  fecha, SUM(finCont_cantidad) dato , finCont_monto meta
, fecha fecv, (SELECT ambito_nombre FROM tbl_planes_y_programas_ambitosub WHERE subambito_id = 10) indic
 FROM tbl_contratistacondicionfin  a INNER JOIN dim_tiempo b ON a.finCont_fecha = b.fecha

WHERE 
CASE WHEN  '.$mes.'=0 THEN Anio = '.$year.' ELSE
mes = '.$mes.' AND Anio = '.$year.'
END 
AND contrato_id = '.$id.' 

GROUP BY 1,2,3
order by fecv';


$sql_lin = 'SELECT \'Real\' serie,Fecha_mes fecha, SUM(coalesce(a.finCont_cantidad,0)) valor,
\'Multas Inspección de Trabajo\' titulo,  \'N° empresas diferentes con multas\' sub, 1 med
, (SELECT ambito_nombre FROM tbl_planes_y_programas_ambitosub WHERE subambito_id = 10) indic
FROM 

(select distinct fecha_mes, Anio, mes from dim_tiempo where fecha >= (SELECT MIN(finCont_fecha) FROM tbl_contratistacondicionfin WHERE YEAR(finCont_fecha) = '.$year.') and Anio = '.$year.')
b
left join 
( Select finCont_cantidad, a.contrato_id, finCont_fecha

from 
tbl_contratistacondicionfin a

where a.contrato_id = '.$id.' 
) a
ON b.fecha_mes = a.finCont_fecha 
WHERE Anio = '.$year.'
GROUP BY 1,2

UNION ALL

SELECT DISTINCT \'Meta\' Serie, concat(concat(NMes3L,\'-\'),anio), 1 , \'Multas Inspección de Trabajo\' titulo,  \'N° empresas diferentes con multas\' sub
, 0 med, (SELECT ambito_nombre FROM tbl_planes_y_programas_ambitosub WHERE subambito_id = 10) indic
 FROM dim_tiempo
WHERE anio = '.$year.' AND dia = 1 AND mes >= (SELECT MIN(MONTH(finCont_fecha)) FROM tbl_contratistacondicionfin WHERE YEAR(finCont_fecha) = '.$year.')';

 $sql_com='SELECT concat(concat(NMes3L,\'-\'),anio) fecha,b.ambito_nombre, c.ambito_nombre indicador,programas_idDescripcion
           FROM tbl_planes_y_programas a INNER JOIN tbl_planes_y_programas_ambito b ON a.ambito_id = b.ambito_id
           INNER JOIN tbl_planes_y_programas_ambitosub c ON c.subambito_id = a.subambito_id
           INNER JOIN dim_tiempo d ON a.programas_idFechaInicio = d.fecha
           
           where
           case when '.$mes.' = 0 then 1=1 else mes = '.$mes.' end
           and anio = '.$year.'
           and a.ambito_id = 3
           and a.subambito_id = 10
           and a.contrato_id = '.$id.'
            and a.programas_idTipo = 1';

    $sql_acc='SELECT concat(concat(NMes3L,\'-\'),anio) fecha,b.ambito_nombre, c.ambito_nombre indicador,programas_idDescripcion
             FROM tbl_planes_y_programas a INNER JOIN tbl_planes_y_programas_ambito b ON a.ambito_id = b.ambito_id
             INNER JOIN tbl_planes_y_programas_ambitosub c ON c.subambito_id = a.subambito_id
             INNER JOIN dim_tiempo d ON a.programas_idFechaInicio = d.fecha
             
             where
             case when '.$mes.' = 0 then 1=1 else mes = '.$mes.' end
             and anio = '.$year.'
             and a.ambito_id = 3
             and a.subambito_id = 10
             and a.contrato_id = '.$id.'
             and a.programas_idTipo = 2';
  } 
    
  else if($ind =="obl"){
	  $sql_gral = 'SELECT  CAST(AVG(valor)*100 AS DECIMAL(4,1)) dato, (select ambito_nombre from tbl_planes_y_programas_ambitosub where subambito_id = 9) nombre, cont_proveedor

          FROM tbl_obliglab_repo a INNER JOIN dim_tiempo b
          ON a.fecha = b.fecha
          INNER JOIN tbl_contrato c ON a.contrato_id = c.contrato_id
          WHERE CASE WHEN  '.$mes.'=0 THEN Anio = '.$year.' ELSE
          mes = '.$mes.' AND Anio = '.$year.' END
          and c.contrato_id ='.$id;
		  
     $sql_det = 'SELECT \'Obligaciones Laborales\' nombre, \'\' descr, concat(concat(NMes3L,\'-\'),anio) fecha, CAST(valor*100 AS DECIMAL(4,1)) dato ,  \'< 16\' meta
, a.fecha fecv, (SELECT ambito_nombre FROM tbl_planes_y_programas_ambitosub WHERE subambito_id = 9) indic
FROM tbl_obliglab_repo a INNER JOIN dim_tiempo b ON a.fecha = b.fecha

WHERE 
CASE WHEN  '.$mes.'=0 THEN Anio = '.$year.' ELSE
mes = '.$mes.' AND Anio = '.$year.'
END 
AND contrato_id = '.$id.' 

GROUP BY 1,2,3
order by fecv
';


$sql_lin = 'SELECT \'Real\' serie,Fecha_mes fecha, CAST(AVG(valor)*100 AS DECIMAL(4,1)) valor,
\'Potencial Incumplimiento Obligaciones Laborales\' titulo,  \'Promedio de porcentaje de trabajadores no informados o no pagados sobre total trabajadores ultimos 12 meses\' sub
, 0 med, (SELECT ambito_nombre FROM tbl_planes_y_programas_ambitosub WHERE subambito_id = 9) indic
FROM tbl_obliglab_repo a INNER JOIN dim_tiempo b
ON a.fecha = b.fecha
inner join tbl_contrato c on c.contrato_id = a.contrato_id
WHERE  Anio = '.$year.'

and a.contrato_id = '.$id.'

GROUP BY 1,2

UNION ALL

SELECT DISTINCT \'Meta\' Serie, concat(concat(NMes3L,\'-\'),anio), 8
,
\'Potencial Incumplimiento Obligaciones Laborales\' titulo,  
\'Promedio de porcentaje de trabajadores no informados o no pagados sobre total trabajadores ultimos 12 meses\' sub, 0 med
, (SELECT ambito_nombre FROM tbl_planes_y_programas_ambitosub WHERE subambito_id = 9) indic
  FROM dim_tiempo 
WHERE anio = '.$year.' AND dia = 1 AND mes >= (SELECT MIN(MONTH(fecha)) FROM tbl_obliglab_repo  WHERE YEAR(fecha) = '.$year.')
';

 $sql_com='SELECT concat(concat(NMes3L,\'-\'),anio) fecha,b.ambito_nombre, c.ambito_nombre indicador,programas_idDescripcion
           FROM tbl_planes_y_programas a INNER JOIN tbl_planes_y_programas_ambito b ON a.ambito_id = b.ambito_id
           INNER JOIN tbl_planes_y_programas_ambitosub c ON c.subambito_id = a.subambito_id
           INNER JOIN dim_tiempo d ON a.programas_idFechaInicio = d.fecha
           
           where
           case when '.$mes.' = 0 then 1=1 else mes = '.$mes.' end
           and anio = '.$year.'
           and a.ambito_id = 3
           and a.subambito_id = 9
           and a.contrato_id = '.$id.'
            and a.programas_idTipo = 1';

    $sql_acc='SELECT concat(concat(NMes3L,\'-\'),anio) fecha,b.ambito_nombre, c.ambito_nombre indicador,programas_idDescripcion
             FROM tbl_planes_y_programas a INNER JOIN tbl_planes_y_programas_ambito b ON a.ambito_id = b.ambito_id
             INNER JOIN tbl_planes_y_programas_ambitosub c ON c.subambito_id = a.subambito_id
             INNER JOIN dim_tiempo d ON a.programas_idFechaInicio = d.fecha
             
             where
             case when '.$mes.' = 0 then 1=1 else mes = '.$mes.' end
             and anio = '.$year.'
             and a.ambito_id = 3
             and a.subambito_id = 9
             and a.contrato_id = '.$id.'
             and a.programas_idTipo = 2';
  }  
    
  else if($ind =="tri"){
	  $sql_gral = 'SELECT cast((SUM(CASE WHEN tCont_eval = \'MALO\' THEN 1 ELSE 0 END)/ 
          COUNT(distinct a.contrato_id))*100 as DECIMAL(4,1)) dato, (select ambito_nombre from tbl_planes_y_programas_ambitosub where subambito_id = 7) nombre, cont_proveedor
          FROM
          tbl_contratista_tributario a INNER JOIN tbl_contrato b ON a.contrato_id = b.contrato_id
          INNER JOIN dim_tiempo d ON a.tCont_fecha = d.fecha
          WHERE 
          case when  '.$mes.'=0 then Anio = '.$year.' else
          mes= '.$mes.' AND Anio = '.$year.' end 
          and b.contrato_id = '.$id.' 
		  GROUP BY 2,3';
		  
    $sql_det = 'SELECT \'Situación tributaria\' nombre, 
CASE WHEN tcont_eval IS NULL THEN \'Sin Evaluación\'
  WHEN tCont_Obs_SII IS NULL THEN \'Sin Observación\' 
  ELSE tCont_Obs_SII END descr,concat(concat(NMes3L,\'-\'),anio)  fecha, COALESCE(tcont_eval,\'--\') dato , \'--\' meta
, fecha fecv, (SELECT ambito_nombre FROM tbl_planes_y_programas_ambitosub WHERE subambito_id = 7) indic
  
FROM tbl_contratista_tributario a INNER JOIN dim_tiempo b ON a.tCont_fecha = b.fecha

WHERE 
CASE WHEN  '.$mes.'=0 THEN Anio = '.$year.' ELSE
mes = '.$mes.' AND Anio = '.$year.'
END 
AND contrato_id = '.$id.' 

GROUP BY 1,2,3
order by fecv';


$sql_lin = 'SELECT \'Real\' serie,Fecha_mes  fecha, 
cast((SUM(CASE WHEN tCont_eval = \'BUENO\' THEN 0 ELSE 1 END)  
/(SELECT COUNT(DISTINCT contrato_id) 
FROM tbl_contratista_tributario WHERE tCont_fecha = a.tCont_fecha)) *100 as DECIMAL(4,1)) valor,
\'Situación Tributaria\' titulo,  \'% Empresas con conflicto respecto de total de Empresas\' sub, 0 med
, (SELECT ambito_nombre FROM tbl_planes_y_programas_ambitosub WHERE subambito_id = 7) indic


FROM tbl_contratista_tributario a
inner join tbl_contrato b on a.contrato_id = b.contrato_id
inner join dim_tiempo c on a.tCont_fecha = c.fecha
where 
case when '.$year.'!=\'\' then Anio = '.$year.' else 1=1 end

and a.contrato_id = '.$id.'
GROUP BY tCont_fecha

UNION ALL

SELECT DISTINCT \'Meta\' Serie, concat(concat(NMes3L,\'-\'),anio), 10,
\'Situación Tributaria\' titulo,  \'% Empresas con conflicto respecto de total de Empresas\' sub, 0 med
, (SELECT ambito_nombre FROM tbl_planes_y_programas_ambitosub WHERE subambito_id = 7) indic
  FROM dim_tiempo
WHERE anio = '.$year.' AND dia = 1 AND mes >= (SELECT MIN(MONTH(tCont_fecha)) FROM tbl_contratista_tributario WHERE YEAR(tCont_fecha) = '.$year.')';

 $sql_com='SELECT concat(concat(NMes3L,\'-\'),anio) fecha,b.ambito_nombre, c.ambito_nombre indicador,programas_idDescripcion
           FROM tbl_planes_y_programas a INNER JOIN tbl_planes_y_programas_ambito b ON a.ambito_id = b.ambito_id
           INNER JOIN tbl_planes_y_programas_ambitosub c ON c.subambito_id = a.subambito_id
           INNER JOIN dim_tiempo d ON a.programas_idFechaInicio = d.fecha
           
           where
           case when '.$mes.' = 0 then 1=1 else mes = '.$mes.' end
           and anio = '.$year.'
           and a.ambito_id = 2
           and a.subambito_id = 7
           and a.contrato_id = '.$id.'
            and a.programas_idTipo = 1';

    $sql_acc='SELECT concat(concat(NMes3L,\'-\'),anio) fecha,b.ambito_nombre, c.ambito_nombre indicador,programas_idDescripcion
             FROM tbl_planes_y_programas a INNER JOIN tbl_planes_y_programas_ambito b ON a.ambito_id = b.ambito_id
             INNER JOIN tbl_planes_y_programas_ambitosub c ON c.subambito_id = a.subambito_id
             INNER JOIN dim_tiempo d ON a.programas_idFechaInicio = d.fecha
             
             where
             case when '.$mes.' = 0 then 1=1 else mes = '.$mes.' end
             and anio = '.$year.'
             and a.ambito_id = 2
             and a.subambito_id = 7
             and a.contrato_id = '.$id.'
             and a.programas_idTipo = 2';
  }  

  else if($ind =="avf"){
	  $sql_gral = 'select 1';
	  
    $sql_det = 'SELECT \'Avance Financiero\' nombre, 
\'\' descr, concat(concat(NMes3L,\'-\'),anio)  fecha, CAST(SUM(financieroand_mtoreal) AS DECIMAL(20,0)) dato ,  CAST(SUM(financieroand_mtoplan) AS DECIMAL(20,0)) meta
, fecha fecv, (SELECT ambito_nombre FROM tbl_planes_y_programas_ambitosub WHERE subambito_id = 4) indic
  
FROM tbl_financiero_and a INNER JOIN dim_tiempo b ON a.financieroand_fecha = b.fecha

WHERE 
CASE WHEN  '.$mes.'=0 THEN Anio = '.$year.' ELSE
mes = '.$mes.' AND Anio = '.$year.'
END 
AND contrato_id = '.$id.' 

GROUP BY 1,2,3
order by fecv';


$sql_tabgen = 'SELECT cont_proveedor Contrato, 
coalesce(CAST(SUM(financieroand_mtoreal)/1000000 AS DECIMAL(10,3)),0) val_real_fn,
coalesce(CAST(SUM(financieroand_mtoplan)/1000000 AS DECIMAL(10,3)),0) val_plan_fn,
coalesce(CAST( 1 -  SUM(financieroand_mtoreal) /SUM(financieroand_mtoplan) AS DECIMAL(4,2))*100,0) desv_fn

FROM tbl_financiero_and a
INNER JOIN dim_tiempo c
ON financieroand_fecha = c.fecha
INNER JOIN tbl_contrato b ON a.contrato_id = b.contrato_id 


WHERE 

CASE WHEN '.$mes.' = 0 THEN 1=1 ELSE mes = '.$mes.' END AND Anio = '.$year.'

and b.contrato_id = '.$id.'

GROUP BY 1
';


$sql_tabfn = 'SELECT \'PROM ACUMULADO\' descp, 
coalesce(cast(SUM(financieroand_mtoreal)/(SELECT SUM(financieroand_mtoplan) FROM tbl_financiero_and
INNER JOIN dim_tiempo ON financieroand_fecha = fecha WHERE anio = '.$year.')*100
 AS DECIMAL(4,1)),0) val_real,
coalesce(cast(SUM(financieroand_mtoplan)/(SELECT SUM(financieroand_mtoplan) FROM tbl_financiero_and
INNER JOIN dim_tiempo ON financieroand_fecha = fecha WHERE anio = '.$year.')*100
 AS DECIMAL(4,1)),0) val_plan,
cast( 1 -  SUM(financieroand_mtoreal) /SUM(financieroand_mtoplan) as decimal(4,1))*100 desv
FROM tbl_financiero_and a
INNER JOIN dim_tiempo c
ON financieroand_fecha = c.fecha
INNER JOIN tbl_contrato b ON a.contrato_id = b.contrato_id 

WHERE 
case when '.$mes.' = 0 then 1=1 else mes <= '.$mes.' end and Anio = '.$year.'

and b.contrato_id = '.$id.'

UNION ALL

SELECT \'ULTIMO MES\' descp,
coalesce(cast(SUM(financieroand_mtoreal)/(SELECT SUM(financieroand_mtoplan) FROM tbl_financiero_and
INNER JOIN dim_tiempo ON financieroand_fecha = fecha WHERE anio = '.$year.')*100  AS DECIMAL(4,1)),0) val_real,
coalesce(cast(SUM(financieroand_mtoplan)/(SELECT SUM(financieroand_mtoplan) FROM tbl_financiero_and
INNER JOIN dim_tiempo ON financieroand_fecha = fecha WHERE anio = '.$year.')*100
 AS DECIMAL(4,1)),0) val_plan,
cast( 1 -  SUM(financieroand_mtoreal) /SUM(financieroand_mtoplan) as decimal(4,1))*100 \'DESV(%)\'
FROM tbl_financiero_and a
INNER JOIN dim_tiempo c
ON financieroand_fecha = c.fecha
INNER JOIN tbl_contrato b ON a.contrato_id = b.contrato_id 

WHERE case when '.$mes.' = 0 then 1=1 else mes = '.$mes.' end  and Anio = '.$year.'

and b.contrato_id = '.$id;


$sql_lin = '
SELECT \'Real\' serie, Fecha_mes  fecha ,  CAST(SUM(financieroand_mtoreal)/1000000 AS DECIMAL(20,2)) valor , financieroand_fecha
,
\'Avance Financiero\' titulo,  \'\' sub, 2 med, (SELECT ambito_nombre FROM tbl_planes_y_programas_ambitosub WHERE subambito_id = 4) indic

 FROM tbl_financiero_and a INNER JOIN dim_tiempo c
ON financieroand_fecha = c.fecha
INNER JOIN tbl_contrato b ON a.contrato_id = b.contrato_id 
WHERE 
CASE WHEN '.$year.'!=\'\' THEN Anio = '.$year.' ELSE 1=1 END

and a.contrato_id = '.$id.'
GROUP BY 1,2

UNION ALL

SELECT \'Meta\' serie, CONCAT(CONCAT(NMes3L,\'-\'),anio) fecha , CAST(SUM(financieroand_mtoplan)/1000000 AS DECIMAL(20,2)) valor , financieroand_fecha
,
\'Avance Financiero\' titulo,  \'\' sub, 0 med, (SELECT ambito_nombre FROM tbl_planes_y_programas_ambitosub WHERE subambito_id = 4) indic

FROM  (SELECT financieroand_fecha,financieroand_mtoplan, contrato_id  FROM tbl_financiero_and GROUP BY 1,2,3 ) a INNER JOIN dim_tiempo c
ON financieroand_fecha = c.fecha
INNER JOIN tbl_contrato b ON a.contrato_id = b.contrato_id 

WHERE anio = '.$year.' AND dia = 1 AND mes >= (SELECT MIN(MONTH(financieroand_fecha)) FROM tbl_financiero_and WHERE YEAR(financieroand_fecha) = '.$year.')
and a.contrato_id = '.$id.'
GROUP BY 4,2';

 $sql_com = 'SELECT concat(concat(NMes3L,\'-\'),anio) fecha,b.ambito_nombre, c.ambito_nombre indicador,programas_idDescripcion
           FROM tbl_planes_y_programas a INNER JOIN tbl_planes_y_programas_ambito b ON a.ambito_id = b.ambito_id
           INNER JOIN tbl_planes_y_programas_ambitosub c ON c.subambito_id = a.subambito_id
           INNER JOIN dim_tiempo d ON a.programas_idFechaInicio = d.fecha
           
           where
           case when '.$mes.' = 0 then 1=1 else mes = '.$mes.' end
           and anio = '.$year.'
           and a.ambito_id = 1
           and a.subambito_id = 4
           and a.contrato_id = '.$id.'
            and a.programas_idTipo = 1';

    $sql_acc='SELECT concat(concat(NMes3L,\'-\'),anio) fecha,b.ambito_nombre, c.ambito_nombre indicador,programas_idDescripcion
             FROM tbl_planes_y_programas a INNER JOIN tbl_planes_y_programas_ambito b ON a.ambito_id = b.ambito_id
             INNER JOIN tbl_planes_y_programas_ambitosub c ON c.subambito_id = a.subambito_id
             INNER JOIN dim_tiempo d ON a.programas_idFechaInicio = d.fecha
             
             where
             case when '.$mes.' = 0 then 1=1 else mes = '.$mes.' end
             and anio = '.$year.'
             and a.ambito_id = 1
             and a.subambito_id = 4
             and a.contrato_id = '.$id.'
             and a.programas_idTipo = 2';
			 
			 
  }  
    
  
   

    if($tip==1)
    {
      $sqlgr = $sql_lin;
    }
    else if($tip == 2)
    {
      $sqlgr = $sql_det;

    }
    else if($tip == 3){
      $sqlgr = $sql_com;

    }
     else if($tip == 4){
      $sqlgr = $sql_acc;

    }
     else if($tip == 5){
		 if($ind == "avf")
			$sqlgr = $sql_tabgen;
		else
			$sqlgr = $sql_gral;

    }
	
	 else if($tip == 6){
		 
		  if($ind == "avf")
			$sqlgr = $sql_tabfn;
		else
			$sqlgr = 'Select 1';
     

    }

                    $rawgraf = array();
                    $i=0;
                    
                    $row1 = $conn2->query($sqlgr);
                    $results = $row1->fetchAll(PDO::FETCH_ASSOC);       
                       
                    echo json_encode($results);
  }                 
else
{ ?>
 <form method="post">
   <div style="display: inline-block;width:70px; height:30px;">
      <b>Año</b>
                     <select name="sl_year" id="sl_year" onchange="contratos(this.value,1)">

<?php
                   $sqlyear = ' select distinct Anio from dim_tiempo 
                              where anio between year(current_Date) -5
                              and YEAR(CASE WHEN DAY(CURRENT_DATE) >= 20 THEN DATE_ADD(DATE_ADD(CURRENT_DATE, INTERVAL -1 MONTH), INTERVAL -(DAY(CURRENT_DATE)-1) DAY)  ELSE DATE_ADD(DATE_ADD(CURRENT_DATE, INTERVAL -2 MONTH), INTERVAL -(DAY(CURRENT_DATE)-1) DAY) END)
                              order by anio desc
                            ';
                     $i=0;
                    
                    foreach ($conn2->query($sqlyear) as $row1) {
                           if($row1["Anio"]==$year){
                              echo "<option  value='".$row1["Anio"]."' selected>".$row1["Anio"]."</option>"; 
                           }
                           else
                              echo "<option  value='".$row1["Anio"]."'>".$row1["Anio"]."</option>"; 
                          $i++;
                    }
                    
            ?>

                    </select>

       
       </div>  
       <div style="display: inline-block; width:80px; height:30px;">
            
           <b>Mes</b>
                     <select name="sl_mes" id="sl_mes" onchange="contratos(this.value,2)">

<?php


                   $sqlmes = ' SELECT 0 mes, \'Todos\' NMes3L
                   UNION ALL
                   SELECT DISTINCT mes , NMes3L  FROM dim_tiempo 
                   WHERE fecha_mes <= CASE WHEN DAY(CURRENT_DATE) >= 20 THEN DATE_ADD(DATE_ADD(CURRENT_DATE, INTERVAL -1 MONTH), INTERVAL -(DAY(CURRENT_DATE)-1) DAY)  ELSE DATE_ADD(DATE_ADD(CURRENT_DATE, INTERVAL -2 MONTH), INTERVAL -(DAY(CURRENT_DATE)-1) DAY) END
                   AND  anio = '.$year.' 
                   ORDER BY mes';
                     $i=0;
                    
                    foreach ($conn2->query($sqlmes) as $row1) {
                           if($row1["mes"]==$mes){
                              echo "<option  value='".$row1["mes"]."' selected>".$row1["NMes3L"]."</option>"; 
                           }
                           else
                              echo "<option  value='".$row1["mes"]."'>".$row1["NMes3L"]."</option>"; 
                          $i++;
                    }
                    
            ?>

                   
                    </select>
</div>
 <div style="display: inline-block; width:300px; height:30px;">
            
           <b>Contrato</b>
                     <select name="sl_cont" id="sl_cont" onchange="contratos(this.value,3)">

<?php


                   $sqlmes = ' 
                   SELECT DISTINCT contrato_id , cont_proveedor cont  FROM tbl_contrato 
                   WHERE cont_fechaFin >= current_date
                   oRDER BY contrato_id';
                     $i=0;
                    
                    foreach ($conn2->query($sqlmes) as $row1) {
                           if($row1["contrato_id"]==$id){
                              echo "<option  value='".$row1["contrato_id"]."' selected>".$row1["cont"]."</option>"; 
                           }
                           else
                              echo "<option  value='".$row1["contrato_id"]."'>".$row1["cont"]."</option>"; 
                          $i++;
                    }
                    
            ?>

                   
                    </select>
</div>
 <div style="display: inline-block;width:100px; height:30px;">
                <button type="reset" class="btn-xs btn-default " onclick="contratos('0',6)">Limpiar</button>
          </div>
 
        </form>

<?php
}
                 ?>               
