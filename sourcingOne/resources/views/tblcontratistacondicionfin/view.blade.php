@if($setting['view-method'] =='native')
<div class="sbox">
	<div class="sbox-title">  
		<h4> <i class="fa fa-table"></i> <?php echo $pageTitle ;?> <small>{{ $pageNote }}</small>
			<a href="javascript:void(0)" class="collapse-close pull-right btn btn-xs btn-danger" onclick="ajaxViewClose('#{{ $pageModule }}')">
			<i class="fa fa fa-times"></i></a>
		</h4>
	 </div>

	<div class="sbox-content"> 
@endif	

		<table class="table table-striped table-bordered" >
			<tbody>	
				
					<tr>
						<td width='30%' class='label-view text-right'>{{ SiteHelpers::activeLang('Contract', (isset($fields['contrato_id']['language'])? $fields['contrato_id']['language'] : array())) }}</td>
						<td>{{ SiteHelpers::formatLookUp($row->contrato_id,'contrato_id','1:tbl_contrato:contrato_id:cont_proveedor') }} </td>
						
					</tr>
				
					<tr>
						<td width='30%' class='label-view text-right'>{{ SiteHelpers::activeLang('Date of data', (isset($fields['finCont_fecha']['language'])? $fields['finCont_fecha']['language'] : array())) }}</td>
						<td>{{ date('d-m-Y',strtotime($row->finCont_fecha)) }} </td>
						
					</tr>
				
					<tr>
						<td width='30%' class='label-view text-right'>{{ SiteHelpers::activeLang('Type of problem', (isset($fields['finCont_tipoProblema']['language'])? $fields['finCont_tipoProblema']['language'] : array())) }}</td>
						<td>{{ SiteHelpers::formatLookUp($row->finCont_tipoProblema,'finCont_tipoProblema','1:tbl_contratistacondicionfintipo:contCondTipo_id:contCond_nombre') }} </td>
						
					</tr>
				
					<tr>
						<td width='30%' class='label-view text-right'>{{ SiteHelpers::activeLang('Quantity', (isset($fields['finCont_cantidad']['language'])? $fields['finCont_cantidad']['language'] : array())) }}</td>
						<td>{{ $row->finCont_cantidad}} </td>
						
					</tr>
				
					<tr>
						<td width='30%' class='label-view text-right'>{{ SiteHelpers::activeLang('Comments', (isset($fields['finCont_comentario']['language'])? $fields['finCont_comentario']['language'] : array())) }}</td>
						<td>{{ $row->finCont_comentario}} </td>
						
					</tr>
				
					<tr>
						<td width='30%' class='label-view text-right'>{{ SiteHelpers::activeLang('Value', (isset($fields['finCont_monto']['language'])? $fields['finCont_monto']['language'] : array())) }}</td>
						<td>{{ $row->finCont_monto}} </td>
						
					</tr>
				
			</tbody>	
		</table>  
			
		 	

@if($setting['form-method'] =='native')
	</div>	
</div>	
@endif	

<script>
$(document).ready(function(){

});
</script>	