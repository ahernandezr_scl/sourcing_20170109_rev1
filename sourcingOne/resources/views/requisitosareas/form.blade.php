
@if($setting['form-method'] =='native')
	<div class="sbox">
		<div class="sbox-title">  
			<h4> <i class="fa fa-table"></i> <?php echo $pageTitle ;?> <small>{{ $pageNote }}</small>
				<a href="javascript:void(0)" class="collapse-close pull-right btn btn-xs btn-danger" onclick="ajaxViewClose('#{{ $pageModule }}')"><i class="fa fa fa-times"></i></a>
			</h4>
	</div>

	<div class="sbox-content"> 
@endif	
			{!! Form::open(array('url'=>'requisitosareas/save/'.SiteHelpers::encryptID($row['IdRequisitoDetalle']), 'class'=>'form-horizontal','files' => true , 'parsley-validate'=>'','novalidate'=>' ','id'=> 'requisitosareasFormAjax')) !!}
			<div class="col-md-12">
						<fieldset><legend> RequisitoDetalleAreas</legend>
				{!! Form::hidden('IdRequisitoDetalle', $row['IdRequisitoDetalle']) !!}					
									  <div class="form-group  " >
										<label for="IdRequisito" class=" control-label col-md-4 text-left"> IdRequisito <span class="asterix"> * </span></label>
										<div class="col-md-6">
										  <select name='IdRequisito' rows='5' id='IdRequisito' class='select2 ' required  ></select> 
										 </div> 
										 <div class="col-md-2">
										 	
										 </div>
									  </div> 					
									  <div class="form-group  " >
										<label for="Centro" class=" control-label col-md-4 text-left"> Centro <span class="asterix"> * </span></label>
										<div class="col-md-6">
										  <select name='Entidad' rows='5' id='Entidad' class='select2 ' required  ></select> 
										 </div> 
										 <div class="col-md-2">
										 	
										 </div>
									  </div> 					
									  <div class="form-group  " >
										<label for="Area de trabajo" class=" control-label col-md-4 text-left"> Area de trabajo <span class="asterix"> * </span></label>
										<div class="col-md-6">
										  <select name='IdEntidad' rows='5' id='IdEntidad' class='select2 ' required  ></select> 
										 </div> 
										 <div class="col-md-2">
										 	
										 </div>
									  </div> </fieldset>
			</div>
			
												
								
						
			<div style="clear:both"></div>	
							
			<div class="form-group">
				<label class="col-sm-4 text-right">&nbsp;</label>
				<div class="col-sm-8">	
					<button type="submit" class="btn btn-primary btn-sm "><i class="icon-checkmark-circle2"></i>  {{ Lang::get('core.sb_save') }} </button>
					<button type="button" onclick="ajaxViewClose('#{{ $pageModule }}')" class="btn btn-success btn-sm"><i class="icon-cancel-circle2 "></i>  {{ Lang::get('core.sb_cancel') }} </button>
				</div>			
			</div> 		 
			{!! Form::close() !!}


@if($setting['form-method'] =='native')
	</div>	
</div>	
@endif	

	
</div>	
			 
<script type="text/javascript">
$(document).ready(function() { 
	
		$("#IdRequisito").jCombo("{!! url('requisitosareas/comboselect?filter=tbl_requisitos:IdRequisito:IdRequisito') !!}",
		{  selected_value : '{{ $row["IdRequisito"] }}' });
		
		$("#Entidad").jCombo("{!! url('requisitosareas/comboselect?filter=tbl_centro:IdCentro:IdCentro|Descripcion') !!}",
		{  selected_value : '{{ $row["Entidad"] }}' });
		
		$("#IdEntidad").jCombo("{!! url('requisitosareas/comboselect?filter=tbl_area_de_trabajo:IdAreaTrabajo:Descripcion') !!}&parent=idCentro:",
		{  parent: '#idCentro', selected_value : '{{ $row["IdEntidad"] }}' });
		 
	
	$('.editor').summernote();
	$('.previewImage').fancybox();	
	$('.tips').tooltip();	
	$(".select2").select2({ width:"98%"});	
	$('.date').datepicker({format:'yyyy-mm-dd',autoClose:true})
	$('.datetime').datetimepicker({format: 'yyyy-mm-dd hh:ii:ss'}); 
	$('input[type="checkbox"],input[type="radio"]').iCheck({
		checkboxClass: 'icheckbox_square-red',
		radioClass: 'iradio_square-red',
	});			
		$('.removeMultiFiles').on('click',function(){
			var removeUrl = '{{ url("requisitosareas/removefiles?file=")}}'+$(this).attr('url');
			$(this).parent().remove();
			$.get(removeUrl,function(response){});
			$(this).parent('div').empty();	
			return false;
		});
				
	var form = $('#requisitosareasFormAjax'); 
	form.parsley();
	form.submit(function(){
		
		if(form.parsley('isValid') == true){			
			var options = { 
				dataType:      'json', 
				beforeSubmit :  showRequest,
				success:       showResponse  
			}  
			$(this).ajaxSubmit(options); 
			return false;
						
		} else {
			return false;
		}		
	
	});

});

function showRequest()
{
	$('.ajaxLoading').show();		
}  
function showResponse(data)  {		
	
	if(data.status == 'success')
	{
		ajaxViewClose('#{{ $pageModule }}');
		ajaxFilter('#{{ $pageModule }}','{{ $pageUrl }}/data');
		notyMessage(data.message);	
		$('#sximo-modal').modal('hide');	
	} else {
		notyMessageError(data.message);	
		$('.ajaxLoading').hide();
		return false;
	}	
}			 

</script>		 