

		 {!! Form::open(array('url'=>'requisitosareas/savepublic', 'class'=>'form-horizontal','files' => true , 'parsley-validate'=>'','novalidate'=>' ')) !!}

	@if(Session::has('messagetext'))
	  
		   {!! Session::get('messagetext') !!}
	   
	@endif
	<ul class="parsley-error-list">
		@foreach($errors->all() as $error)
			<li>{{ $error }}</li>
		@endforeach
	</ul>		


<div class="col-md-12">
						<fieldset><legend> RequisitoDetalleAreas</legend>
				{!! Form::hidden('IdRequisitoDetalle', $row['IdRequisitoDetalle']) !!}					
									  <div class="form-group  " >
										<label for="IdRequisito" class=" control-label col-md-4 text-left"> IdRequisito <span class="asterix"> * </span></label>
										<div class="col-md-6">
										  <select name='IdRequisito' rows='5' id='IdRequisito' class='select2 ' required  ></select> 
										 </div> 
										 <div class="col-md-2">
										 	
										 </div>
									  </div> 					
									  <div class="form-group  " >
										<label for="Centro" class=" control-label col-md-4 text-left"> Centro <span class="asterix"> * </span></label>
										<div class="col-md-6">
										  <select name='Entidad' rows='5' id='Entidad' class='select2 ' required  ></select> 
										 </div> 
										 <div class="col-md-2">
										 	
										 </div>
									  </div> 					
									  <div class="form-group  " >
										<label for="Area de trabajo" class=" control-label col-md-4 text-left"> Area de trabajo <span class="asterix"> * </span></label>
										<div class="col-md-6">
										  <select name='IdEntidad' rows='5' id='IdEntidad' class='select2 ' required  ></select> 
										 </div> 
										 <div class="col-md-2">
										 	
										 </div>
									  </div> </fieldset>
			</div>
			
			

			<div style="clear:both"></div>	
				
					
				  <div class="form-group">
					<label class="col-sm-4 text-right">&nbsp;</label>
					<div class="col-sm-8">	
					<button type="submit" name="apply" class="btn btn-info btn-sm" ><i class="fa  fa-check-circle"></i> {{ Lang::get('core.sb_apply') }}</button>
					<button type="submit" name="submit" class="btn btn-primary btn-sm" ><i class="fa  fa-save "></i> {{ Lang::get('core.sb_save') }}</button>
				  </div>	  
			
		</div> 
		 
		 {!! Form::close() !!}
		 
   <script type="text/javascript">
	$(document).ready(function() { 
		
		
		$("#IdRequisito").jCombo("{!! url('requisitosareas/comboselect?filter=tbl_requisitos:IdRequisito:IdRequisito') !!}",
		{  selected_value : '{{ $row["IdRequisito"] }}' });
		
		$("#Entidad").jCombo("{!! url('requisitosareas/comboselect?filter=tbl_centro:IdCentro:IdCentro|Descripcion') !!}",
		{  selected_value : '{{ $row["Entidad"] }}' });
		
		$("#IdEntidad").jCombo("{!! url('requisitosareas/comboselect?filter=tbl_area_de_trabajo:IdAreaTrabajo:Descripcion') !!}&parent=idCentro:",
		{  parent: '#idCentro', selected_value : '{{ $row["IdEntidad"] }}' });
		 

		$('.removeCurrentFiles').on('click',function(){
			var removeUrl = $(this).attr('href');
			$.get(removeUrl,function(response){});
			$(this).parent('div').empty();	
			return false;
		});		
		
	});
	</script>		 
