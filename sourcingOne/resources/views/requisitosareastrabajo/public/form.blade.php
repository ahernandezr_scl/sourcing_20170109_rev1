

		 {!! Form::open(array('url'=>'requisitosareastrabajo/savepublic', 'class'=>'form-horizontal','files' => true , 'parsley-validate'=>'','novalidate'=>' ')) !!}

	@if(Session::has('messagetext'))
	  
		   {!! Session::get('messagetext') !!}
	   
	@endif
	<ul class="parsley-error-list">
		@foreach($errors->all() as $error)
			<li>{{ $error }}</li>
		@endforeach
	</ul>		


<div class="col-md-12">
						<fieldset><legend> Requisitos para el area de trabajo</legend>
				{!! Form::hidden('IdRequisito', $row['IdRequisito']) !!}{!! Form::hidden('Entidad', $row['Entidad']) !!}					
									  <div class="form-group  " >
										<label for="Tipo documento" class=" control-label col-md-4 text-left"> Tipo documento </label>
										<div class="col-md-6">
										  <select name='IdTipoDocumento' rows='5' id='IdTipoDocumento' class='select2 '   ></select> 
										 </div> 
										 <div class="col-md-2">
										 	
										 </div>
									  </div> 					
									  <div class="form-group  " >
										<label for="Vigencia" class=" control-label col-md-4 text-left"> Vigencia </label>
										<div class="col-md-6">
										  
					<?php $Vigencia = explode(',',$row['Vigencia']);
					$Vigencia_opt = array( 'Origen' => 'Para todos' ,  'Ahora' => 'Desde Ahora' , ); ?>
					<select name='Vigencia' rows='5'   class='select2 '  > 
						<?php 
						foreach($Vigencia_opt as $key=>$val)
						{
							echo "<option  value ='$key' ".($row['Vigencia'] == $key ? " selected='selected' " : '' ).">$val</option>"; 						
						}						
						?></select> 
										 </div> 
										 <div class="col-md-2">
										 	
										 </div>
									  </div> {!! Form::hidden('FechaCumplimiento', $row['FechaCumplimiento']) !!}{!! Form::hidden('createdOn', $row['createdOn']) !!}{!! Form::hidden('entry_by', $row['entry_by']) !!}{!! Form::hidden('updatedOn', $row['updatedOn']) !!}</fieldset>
			</div>
			
			

			<div style="clear:both"></div>	
				
					
				  <div class="form-group">
					<label class="col-sm-4 text-right">&nbsp;</label>
					<div class="col-sm-8">	
					<button type="submit" name="apply" class="btn btn-info btn-sm" ><i class="fa  fa-check-circle"></i> {{ Lang::get('core.sb_apply') }}</button>
					<button type="submit" name="submit" class="btn btn-primary btn-sm" ><i class="fa  fa-save "></i> {{ Lang::get('core.sb_save') }}</button>
				  </div>	  
			
		</div> 
		 
		 {!! Form::close() !!}
		 
   <script type="text/javascript">
	$(document).ready(function() { 
		$('.addC').relCopy({});
		
		$("#IdTipoDocumento").jCombo("{!! url('requisitosareastrabajo/comboselect?filter=tbl_tipos_documentos:IdTipoDocumento:Descripcion') !!}",
		{  selected_value : '{{ $row["IdTipoDocumento"] }}' });
		 

		$('.removeCurrentFiles').on('click',function(){
			var removeUrl = $(this).attr('href');
			$.get(removeUrl,function(response){});
			$(this).parent('div').empty();	
			return false;
		});		
		
	});
	</script>		 
