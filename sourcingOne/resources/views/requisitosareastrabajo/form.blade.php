@extends('layouts.app')

@section('content')

  <div class="page-content row">

 	<div class="page-content-wrapper m-t">


<div class="sbox">
	<div class="sbox-title"> <h3> {{ $pageTitle }} <small>{{ $pageNote }}</small></h3> </div>
	<div class="sbox-content">

		<ul class="parsley-error-list">
			@foreach($errors->all() as $error)
				<li>{{ $error }}</li>
			@endforeach
		</ul>

		 {!! Form::open(array('url'=>'requisitosareastrabajo/save?return='.$return, 'class'=>'form-horizontal','files' => true , 'parsley-validate'=>'','novalidate'=>' ')) !!}
<div class="col-md-12">
						<fieldset><legend> Requisitos para el area de trabajo</legend>
				{!! Form::hidden('IdRequisito', $row['IdRequisito']) !!}
				{!! Form::hidden('Entidad', '5') !!}
									  <div class="form-group  " >
										<label for="Tipo documento" class=" control-label col-md-4 text-left"> Tipo documento </label>
										<div class="col-md-6">
										  <select name='IdTipoDocumento' rows='5' id='IdTipoDocumento' class='select2 '   ></select>
										 </div>
										 <div class="col-md-2">

										 </div>
									  </div>
									  <div class="form-group  " >
										<label for="Vigencia" class=" control-label col-md-4 text-left"> Vigencia </label>
										<div class="col-md-6">

					<?php $Vigencia = explode(',',$row['Vigencia']);
					$Vigencia_opt = array( 'Origen' => 'Para todos' ,  'Ahora' => 'Desde Ahora' , ); ?>
					<select name='Vigencia' rows='5'   class='select2 '  >
						<?php
						foreach($Vigencia_opt as $key=>$val)
						{
							echo "<option  value ='$key' ".($row['Vigencia'] == $key ? " selected='selected' " : '' ).">$val</option>";
						}
						?></select>
										 </div>
										 <div class="col-md-2">

										 </div>
									  </div> {!! Form::hidden('FechaCumplimiento', $row['FechaCumplimiento']) !!}{!! Form::hidden('createdOn', $row['createdOn']) !!}{!! Form::hidden('entry_by', $row['entry_by']) !!}{!! Form::hidden('updatedOn', $row['updatedOn']) !!}</fieldset>
			</div>




	<hr />
	<div class="clr clear"></div>

	<h5> Areas de trabajo </h5>

	<div class="table-responsive">
    <table class="table table-striped ">
        <thead>
			<tr>
				@foreach ($subform['tableGrid'] as $t)
					@if($t['view'] =='1' && $t['field'] !='IdRequisito')
						<th>{{ $t['label'] }}</th>
					@endif
				@endforeach
				<th></th>
			  </tr>

        </thead>

        <tbody>
        @if(count($subform['rowData'])>=1)
            @foreach ($subform['rowData'] as $rows)
            <tr class="clone clonedInput">

			 @foreach ($subform['tableGrid'] as $field)
				 @if($field['view'] =='1' && $field['field'] !='IdRequisito')
				 <td>
				 	{!! SiteHelpers::bulkForm($field['field'] , $subform['tableForm'] , $rows->{$field['field']}) !!}
				 </td>
				 @endif

			 @endforeach
			 <td>
			 	<a onclick=" $(this).parents('.clonedInput').remove(); return false" href="#" class="remove btn btn-xs btn-danger">-</a>
			 	<input type="hidden" name="counter[]">
			 </td>
			@endforeach
			</tr>

		@else
            <tr class="clone clonedInput">

			 @foreach ($subform['tableGrid'] as $field)

				 @if($field['view'] =='1' && $field['field'] !='IdRequisito')
				 <td>
				 	{!! SiteHelpers::bulkForm($field['field'] , $subform['tableForm'] ) !!}
				 </td>
				 @endif

			 @endforeach
			 <td>
			 	<a onclick=" $(this).parents('.clonedInput').remove(); return false" href="#" class="remove btn btn-xs btn-danger">-</a>
			 	<input type="hidden" name="counter[]">
			 </td>

			</tr>


		@endif


        </tbody>

     </table>
     <input type="hidden" name="enable-masterdetail" value="true">
     </div>
	<br /><br />

     <a href="javascript:void(0);" class="addC btn btn-xs btn-info" rel=".clone"><i class="fa fa-plus"></i> New Item</a>
     <hr />
			<div style="clear:both"></div>


				  <div class="form-group">
					<label class="col-sm-4 text-right">&nbsp;</label>
					<div class="col-sm-8">
					<button type="submit" name="apply" class="btn btn-info btn-sm" ><i class="icon-checkmark-circle2"></i> {{ Lang::get('core.sb_apply') }}</button>
					<button type="submit" name="submit" class="btn btn-primary btn-sm" ><i class="icon-bubble-check"></i> {{ Lang::get('core.sb_save') }}</button>
					<button type="button" onclick="location.href='{{ URL::to('requisitosareastrabajo?return='.$return) }}' " class="btn btn-warning btn-sm "><i class="icon-cancel-circle2 "></i>  {{ Lang::get('core.sb_cancel') }} </button>
					</div>

				  </div>

		 {!! Form::close() !!}
	</div>
</div>
</div>
</div>
   <script type="text/javascript">
	$(document).ready(function() {
		$('.addC').relCopy({});
  location.origin = location.protocol + "//" + location.host;
  var base_url = location.origin + '/requisitosareastrabajo';

		$("#IdTipoDocumento").jCombo("{!! url('requisitosareastrabajo/comboselect?filter=tbl_tipos_documentos:IdTipoDocumento:Descripcion') !!}",
		{  selected_value : '{{ $row["IdTipoDocumento"] }}' });

	$(".bulk_IdEntidad").prop("disabled",true);

	$(".bulk_Entidad").change(function(event){
		var lobjIdAreaDeTrabajo = $(this).parent("td").parent("tr").find(".bulk_IdEntidad");
		$.post(base_url+'/areatrabajo',{ id:event.target.value,},function(response,state){
			lobjIdAreaDeTrabajo.empty();
			for (var i =0; i<response.valores.length; i++) {
				lobjIdAreaDeTrabajo.append("<option value='"+response.valores[i].IdAreaTrabajo+"'>"+response.valores[i].Descripcion+"</option>");
			}
		});

	        var lintIdAreaDeTrabajo;
	        var lintExists = 0;
	        lobjIdAreaDeTrabajo.find("option").each(function () {
	            lintIdAreaDeTrabajo = $(this).val();
	            lintExists = 0;
	            $(".bulk_Entidad").each(function () {
	              if ($(this).val()==lintIdAreaDeTrabajo) {
	                lintExists = 1;
	                 return false;
	              }
	            });
	            if (!lintExists){
	               lobjIdAreaDeTrabajo.find("option[value='"+lintIdAreaDeTrabajo+"']").remove();
	            }
	        });
	        lobjIdAreaDeTrabajo.prop( "disabled", false );
	});


		$('.removeMultiFiles').on('click',function(){
			var removeUrl = '{{ url("requisitosareastrabajo/removefiles?file=")}}'+$(this).attr('url');
			$(this).parent().remove();
			$.get(removeUrl,function(response){});
			$(this).parent('div').empty();
			return false;
		});

	});
	</script>
@stop
