
@if($setting['form-method'] =='native')
	<div class="sbox">
		<div class="sbox-title">
			<h4> <i class="fa fa-table"></i> <?php echo $pageTitle ;?> <small>{{ $pageNote }}</small>
				<a href="javascript:void(0)" class="collapse-close pull-right btn btn-xs btn-danger" onclick="ajaxViewClose('#{{ $pageModule }}')"><i class="fa fa fa-times"></i></a>
			</h4>
	</div>

	<div class="sbox-content">
@endif
			{!! Form::open(array('url'=>'centros/save/'.SiteHelpers::encryptID($row['IdCentro']), 'class'=>'form-horizontal','files' => true , 'parsley-validate'=>'','novalidate'=>' ','id'=> 'centrosFormAjax')) !!}
			<div class="col-md-12">
						<fieldset><legend> centros</legend>
				{!! Form::hidden('IdCentro', $row['IdCentro']) !!}
									  <div class="form-group  " >
										<label for="Description" class=" control-label col-md-4 text-left"> Description <span class="asterix"> * </span></label>
										<div class="col-md-6">
										  {!! Form::text('Descripcion', $row['Descripcion'],array('class'=>'form-control', 'placeholder'=>'', 'required'=>'true'  )) !!}
										 </div>
										 <div class="col-md-2">

										 </div>
									  </div>
									  <div class="form-group  " >
										<label for="Group" class=" control-label col-md-4 text-left"> Group <span class="asterix"> * </span></label>
										<div class="col-md-6">
										  <select name='IdAgrupacion' rows='5' id='IdAgrupacion' class='select2 ' required  ></select>
										 </div>
										 <div class="col-md-2">

										 </div>
									  </div>
									  <div class="form-group  " >
										<label for="Region" class=" control-label col-md-4 text-left"> Region <span class="asterix"> * </span></label>
										<div class="col-md-6">
										  <select name='IdRegion' rows='5' id='IdRegion' class='select2 ' required  ></select>
										 </div>
										 <div class="col-md-2">

										 </div>
									  </div>
									  <div class="form-group  " >
										<label for="Province" class=" control-label col-md-4 text-left"> Province <span class="asterix"> * </span></label>
										<div class="col-md-6">
										  <select name='IdProvincia' rows='5' id='IdProvincia' class='select2 ' required  ></select>
										 </div>
										 <div class="col-md-2">

										 </div>
									  </div>
									  <div class="form-group  " >
										<label for="Commune" class=" control-label col-md-4 text-left"> Commune <span class="asterix"> * </span></label>
										<div class="col-md-6">
										  <select name='IdComuna' rows='5' id='IdComuna' class='select2 ' required  ></select>
										 </div>
										 <div class="col-md-2">

										 </div>
									  </div>
									  <div class="form-group  " >
										<label for="Address" class=" control-label col-md-4 text-left"> Address </label>
										<div class="col-md-6">
										  <textarea name='Direccion' rows='5' id='Direccion' class='form-control '
				           >{{ $row['Direccion'] }}</textarea>
										 </div>
										 <div class="col-md-2">

										 </div>
									  </div> {!! Form::hidden('createdOn', $row['createdOn']) !!}{!! Form::hidden('entry_by', $row['entry_by']) !!}{!! Form::hidden('updatedOn', $row['updatedOn']) !!}</fieldset>
			</div>



	<hr />
	<div class="clr clear"></div>

	<h5> Lista de areas de trabajo </h5>

	<div class="table-responsive">
    <table class="table table-striped ">
        <thead>
			<tr>
				@foreach ($subform['tableGrid'] as $t)
					@if($t['view'] =='1' && $t['field'] !='IdCentro')
						<th>{{ $t['label'] }}</th>
					@endif
				@endforeach
				<th></th>
			  </tr>

        </thead>

        <tbody>
        @if(count($subform['rowData'])>=1)
            @foreach ($areat as $rows)
            <tr class="clone clonedInput">
				 <td>
				<input type="text" id="bulk_Descripcion" name="bulk_Descripcion[]" class="form-control input-sm bulk_Descripcion" data-parsley-required="true" value="<?php echo $rows->Descripcion?>">
				 </td>
				 <td><input type="hidden"  name="IdArea[]" id="IdArea" value="<?php echo $rows->IdAreaTrabajo?>"></td>

			 <td>
			 	<a onclick=" $(this).parents('.clonedInput').remove(); return false" href="#" class="remove btn btn-xs btn-danger">-</a>
			 	<input type="hidden" name="counter[]">
			 </td>
			@endforeach
			</tr>

		@else
            <tr class="clone clonedInput">

			 @foreach ($subform['tableGrid'] as $field)

				 @if($field['view'] =='1' && $field['field'] !='IdCentro')
				 <td>
				 	{!! SiteHelpers::bulkForm($field['field'] , $subform['tableForm'] ) !!}
				 </td>
				 @endif

			 @endforeach
			 <td>
			 	<a onclick=" $(this).parents('.clonedInput').remove(); return false" href="#" class="remove btn btn-xs btn-danger">-</a>
			 	<input type="hidden" name="counter[]">
			 </td>

			</tr>


		@endif


        </tbody>

     </table>
     <input type="hidden" name="enable-masterdetail" value="true">
     </div>
	<br /><br />

     <a href="javascript:void(0);" class="addC btn btn-xs btn-info" rel=".clone"><i class="fa fa-plus"></i> New Item</a>
     <hr />

			<div style="clear:both"></div>

			<div class="form-group">
				<label class="col-sm-4 text-right">&nbsp;</label>
				<div class="col-sm-8">
					<button type="submit" class="btn btn-primary btn-sm "><i class="icon-checkmark-circle2"></i>  {{ Lang::get('core.sb_save') }} </button>
					<button type="button" onclick="ajaxViewClose('#{{ $pageModule }}')" class="btn btn-success btn-sm"><i class="icon-cancel-circle2 "></i>  {{ Lang::get('core.sb_cancel') }} </button>
				</div>
			</div>
			{!! Form::close() !!}


@if($setting['form-method'] =='native')
	</div>
</div>
@endif


</div>

<script type="text/javascript">
$(document).ready(function() {

		$("#IdAgrupacion").jCombo("{!! url('centros/comboselect?filter=tbl_agrupacion_geografica:IdAgrupacion:Descripcion') !!}",
		{  selected_value : '{{ $row["IdAgrupacion"] }}' });

		$("#IdRegion").jCombo("{!! url('centros/comboselect?filter=dim_region:id:id|nombre') !!}",
		{  selected_value : '{{ $row["IdRegion"] }}' });

		$("#IdProvincia").jCombo("{!! url('centros/comboselect?filter=dim_provincia:id:nombre') !!}&parent=IdRegion:",
		{  parent: '#IdRegion', selected_value : '{{ $row["IdProvincia"] }}' });

		$("#IdComuna").jCombo("{!! url('centros/comboselect?filter=dim_comuna:id:nombre') !!}&parent=IdProvincia:",
		{  parent: '#IdProvincia', selected_value : '{{ $row["IdComuna"] }}' });

	$('.addC').relCopy({});
	$('.editor').summernote();
	$('.previewImage').fancybox();
	$('.tips').tooltip();
	$(".select2").select2({ width:"98%"});
	$('.date').datepicker({format:'yyyy-mm-dd',autoClose:true})
	$('.datetime').datetimepicker({format: 'yyyy-mm-dd hh:ii:ss'});
	$('input[type="checkbox"],input[type="radio"]').iCheck({
		checkboxClass: 'icheckbox_square-red',
		radioClass: 'iradio_square-red',
	});
		$('.removeMultiFiles').on('click',function(){
			var removeUrl = '{{ url("centros/removefiles?file=")}}'+$(this).attr('url');
			$(this).parent().remove();
			$.get(removeUrl,function(response){});
			$(this).parent('div').empty();
			return false;
		});

	var form = $('#centrosFormAjax');
	form.parsley();
	form.submit(function(){

		if(form.parsley('isValid') == true){
			var options = {
				dataType:      'json',
				beforeSubmit :  showRequest,
				success:       showResponse
			}
			$(this).ajaxSubmit(options);
			return false;

		} else {
			return false;
		}

	});

});

function showRequest()
{
	$('.ajaxLoading').show();
}
function showResponse(data)  {

	if(data.status == 'success')
	{
		ajaxViewClose('#{{ $pageModule }}');
		ajaxFilter('#{{ $pageModule }}','{{ $pageUrl }}/data');
		notyMessage(data.message);
		$('#sximo-modal').modal('hide');
	} else {
		notyMessageError(data.message);
		$('.ajaxLoading').hide();
		return false;
	}
}

</script>
