@if($setting['view-method'] =='native')
<div class="sbox">
	<div class="sbox-title">  
		<h4> <i class="fa fa-table"></i> <?php echo $pageTitle ;?> <small>{{ $pageNote }}</small>
			<a href="javascript:void(0)" class="collapse-close pull-right btn btn-xs btn-danger" onclick="ajaxViewClose('#{{ $pageModule }}')">
			<i class="fa fa fa-times"></i></a>
		</h4>
	 </div>

	<div class="sbox-content"> 
@endif	

		<table class="table table-striped table-bordered" >
			<tbody>	
				
					<tr>
						<td width='30%' class='label-view text-right'>{{ SiteHelpers::activeLang('IdCentro', (isset($fields['IdCentro']['language'])? $fields['IdCentro']['language'] : array())) }}</td>
						<td>{{ $row->IdCentro}} </td>
						
					</tr>
				
					<tr>
						<td width='30%' class='label-view text-right'>{{ SiteHelpers::activeLang('Region', (isset($fields['IdRegion']['language'])? $fields['IdRegion']['language'] : array())) }}</td>
						<td>{{ SiteHelpers::formatLookUp($row->IdRegion,'IdRegion','1:dim_region:id:nombre') }} </td>
						
					</tr>
				
					<tr>
						<td width='30%' class='label-view text-right'>{{ SiteHelpers::activeLang('Province', (isset($fields['IdProvincia']['language'])? $fields['IdProvincia']['language'] : array())) }}</td>
						<td>{{ SiteHelpers::formatLookUp($row->IdProvincia,'IdProvincia','1:dim_provincia:id:nombre') }} </td>
						
					</tr>
				
					<tr>
						<td width='30%' class='label-view text-right'>{{ SiteHelpers::activeLang('Commune', (isset($fields['IdComuna']['language'])? $fields['IdComuna']['language'] : array())) }}</td>
						<td>{{ SiteHelpers::formatLookUp($row->IdComuna,'IdComuna','1:dim_comuna:id:nombre') }} </td>
						
					</tr>
				
					<tr>
						<td width='30%' class='label-view text-right'>{{ SiteHelpers::activeLang('Group', (isset($fields['IdAgrupacion']['language'])? $fields['IdAgrupacion']['language'] : array())) }}</td>
						<td>{{ SiteHelpers::formatLookUp($row->IdAgrupacion,'IdAgrupacion','1:tbl_agrupacion_geografica:IdAgrupacion:Descripcion') }} </td>
						
					</tr>
				
					<tr>
						<td width='30%' class='label-view text-right'>{{ SiteHelpers::activeLang('Description', (isset($fields['Descripcion']['language'])? $fields['Descripcion']['language'] : array())) }}</td>
						<td>{{ $row->Descripcion}} </td>
						
					</tr>
				
					<tr>
						<td width='30%' class='label-view text-right'>{{ SiteHelpers::activeLang('CreatedOn', (isset($fields['createdOn']['language'])? $fields['createdOn']['language'] : array())) }}</td>
						<td>{{ date('d/m/Y',strtotime($row->createdOn)) }} </td>
						
					</tr>
				
					<tr>
						<td width='30%' class='label-view text-right'>{{ SiteHelpers::activeLang('Address', (isset($fields['Direccion']['language'])? $fields['Direccion']['language'] : array())) }}</td>
						<td>{{ $row->Direccion}} </td>
						
					</tr>
				
					<tr>
						<td width='30%' class='label-view text-right'>{{ SiteHelpers::activeLang('Entry By', (isset($fields['entry_by']['language'])? $fields['entry_by']['language'] : array())) }}</td>
						<td>{{ SiteHelpers::formatLookUp($row->entry_by,'entry_by','1:tb_users:id:first_name|last_name') }} </td>
						
					</tr>
				
					<tr>
						<td width='30%' class='label-view text-right'>{{ SiteHelpers::activeLang('UpdatedOn', (isset($fields['updatedOn']['language'])? $fields['updatedOn']['language'] : array())) }}</td>
						<td>{{ date('d/m/Y',strtotime($row->updatedOn)) }} </td>
						
					</tr>
				
			</tbody>	
		</table>  
			
		 	

@if($setting['form-method'] =='native')
	</div>	
</div>	
@endif	

<script>
$(document).ready(function(){

});
</script>	