

		 {!! Form::open(array('url'=>'centros/savepublic', 'class'=>'form-horizontal','files' => true , 'parsley-validate'=>'','novalidate'=>' ')) !!}

	@if(Session::has('messagetext'))
	  
		   {!! Session::get('messagetext') !!}
	   
	@endif
	<ul class="parsley-error-list">
		@foreach($errors->all() as $error)
			<li>{{ $error }}</li>
		@endforeach
	</ul>		


<div class="col-md-12">
						<fieldset><legend> centros</legend>
				{!! Form::hidden('IdCentro', $row['IdCentro']) !!}					
									  <div class="form-group  " >
										<label for="Description" class=" control-label col-md-4 text-left"> Description <span class="asterix"> * </span></label>
										<div class="col-md-6">
										  {!! Form::text('Descripcion', $row['Descripcion'],array('class'=>'form-control', 'placeholder'=>'', 'required'=>'true'  )) !!} 
										 </div> 
										 <div class="col-md-2">
										 	
										 </div>
									  </div> 					
									  <div class="form-group  " >
										<label for="Group" class=" control-label col-md-4 text-left"> Group <span class="asterix"> * </span></label>
										<div class="col-md-6">
										  <select name='IdAgrupacion' rows='5' id='IdAgrupacion' class='select2 ' required  ></select> 
										 </div> 
										 <div class="col-md-2">
										 	
										 </div>
									  </div> 					
									  <div class="form-group  " >
										<label for="Region" class=" control-label col-md-4 text-left"> Region <span class="asterix"> * </span></label>
										<div class="col-md-6">
										  <select name='IdRegion' rows='5' id='IdRegion' class='select2 ' required  ></select> 
										 </div> 
										 <div class="col-md-2">
										 	
										 </div>
									  </div> 					
									  <div class="form-group  " >
										<label for="Province" class=" control-label col-md-4 text-left"> Province <span class="asterix"> * </span></label>
										<div class="col-md-6">
										  <select name='IdProvincia' rows='5' id='IdProvincia' class='select2 ' required  ></select> 
										 </div> 
										 <div class="col-md-2">
										 	
										 </div>
									  </div> 					
									  <div class="form-group  " >
										<label for="Commune" class=" control-label col-md-4 text-left"> Commune <span class="asterix"> * </span></label>
										<div class="col-md-6">
										  <select name='IdComuna' rows='5' id='IdComuna' class='select2 ' required  ></select> 
										 </div> 
										 <div class="col-md-2">
										 	
										 </div>
									  </div> 					
									  <div class="form-group  " >
										<label for="Address" class=" control-label col-md-4 text-left"> Address </label>
										<div class="col-md-6">
										  <textarea name='Direccion' rows='5' id='Direccion' class='form-control '  
				           >{{ $row['Direccion'] }}</textarea> 
										 </div> 
										 <div class="col-md-2">
										 	
										 </div>
									  </div> {!! Form::hidden('createdOn', $row['createdOn']) !!}{!! Form::hidden('entry_by', $row['entry_by']) !!}{!! Form::hidden('updatedOn', $row['updatedOn']) !!}</fieldset>
			</div>
			
			

			<div style="clear:both"></div>	
				
					
				  <div class="form-group">
					<label class="col-sm-4 text-right">&nbsp;</label>
					<div class="col-sm-8">	
					<button type="submit" name="apply" class="btn btn-info btn-sm" ><i class="fa  fa-check-circle"></i> {{ Lang::get('core.sb_apply') }}</button>
					<button type="submit" name="submit" class="btn btn-primary btn-sm" ><i class="fa  fa-save "></i> {{ Lang::get('core.sb_save') }}</button>
				  </div>	  
			
		</div> 
		 
		 {!! Form::close() !!}
		 
   <script type="text/javascript">
	$(document).ready(function() { 
		$('.addC').relCopy({});
		
		$("#IdAgrupacion").jCombo("{!! url('centros/comboselect?filter=tbl_agrupacion_geografica:IdAgrupacion:Descripcion') !!}",
		{  selected_value : '{{ $row["IdAgrupacion"] }}' });
		
		$("#IdRegion").jCombo("{!! url('centros/comboselect?filter=dim_region:id:id|nombre') !!}",
		{  selected_value : '{{ $row["IdRegion"] }}' });
		
		$("#IdProvincia").jCombo("{!! url('centros/comboselect?filter=dim_provincia:id:nombre') !!}&parent=IdRegion:",
		{  parent: '#IdRegion', selected_value : '{{ $row["IdProvincia"] }}' });
		
		$("#IdComuna").jCombo("{!! url('centros/comboselect?filter=dim_comuna:id:nombre') !!}&parent=IdProvincia:",
		{  parent: '#IdProvincia', selected_value : '{{ $row["IdComuna"] }}' });
		 

		$('.removeCurrentFiles').on('click',function(){
			var removeUrl = $(this).attr('href');
			$.get(removeUrl,function(response){});
			$(this).parent('div').empty();	
			return false;
		});		
		
	});
	</script>		 
