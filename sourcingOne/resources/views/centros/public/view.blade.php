<div class="m-t" style="padding-top:25px;">	
    <div class="row m-b-lg animated fadeInDown delayp1 text-center">
        <h3> {{ $pageTitle }} <small> {{ $pageNote }} </small></h3>
        <hr />       
    </div>
</div>
<div class="m-t">
	<div class="table-responsive" > 	

		<table class="table table-striped table-bordered" >
			<tbody>	
		
			
					<tr>
						<td width='30%' class='label-view text-right'>{{ SiteHelpers::activeLang('IdCentro', (isset($fields['IdCentro']['language'])? $fields['IdCentro']['language'] : array())) }}</td>
						<td>{{ $row->IdCentro}} </td>
						
					</tr>
				
					<tr>
						<td width='30%' class='label-view text-right'>{{ SiteHelpers::activeLang('Region', (isset($fields['IdRegion']['language'])? $fields['IdRegion']['language'] : array())) }}</td>
						<td>{{ SiteHelpers::formatLookUp($row->IdRegion,'IdRegion','1:dim_region:id:nombre') }} </td>
						
					</tr>
				
					<tr>
						<td width='30%' class='label-view text-right'>{{ SiteHelpers::activeLang('Province', (isset($fields['IdProvincia']['language'])? $fields['IdProvincia']['language'] : array())) }}</td>
						<td>{{ SiteHelpers::formatLookUp($row->IdProvincia,'IdProvincia','1:dim_provincia:id:nombre') }} </td>
						
					</tr>
				
					<tr>
						<td width='30%' class='label-view text-right'>{{ SiteHelpers::activeLang('Commune', (isset($fields['IdComuna']['language'])? $fields['IdComuna']['language'] : array())) }}</td>
						<td>{{ SiteHelpers::formatLookUp($row->IdComuna,'IdComuna','1:dim_comuna:id:nombre') }} </td>
						
					</tr>
				
					<tr>
						<td width='30%' class='label-view text-right'>{{ SiteHelpers::activeLang('Group', (isset($fields['IdAgrupacion']['language'])? $fields['IdAgrupacion']['language'] : array())) }}</td>
						<td>{{ SiteHelpers::formatLookUp($row->IdAgrupacion,'IdAgrupacion','1:tbl_agrupacion_geografica:IdAgrupacion:Descripcion') }} </td>
						
					</tr>
				
					<tr>
						<td width='30%' class='label-view text-right'>{{ SiteHelpers::activeLang('Description', (isset($fields['Descripcion']['language'])? $fields['Descripcion']['language'] : array())) }}</td>
						<td>{{ $row->Descripcion}} </td>
						
					</tr>
				
					<tr>
						<td width='30%' class='label-view text-right'>{{ SiteHelpers::activeLang('CreatedOn', (isset($fields['createdOn']['language'])? $fields['createdOn']['language'] : array())) }}</td>
						<td>{{ date('d/m/Y',strtotime($row->createdOn)) }} </td>
						
					</tr>
				
					<tr>
						<td width='30%' class='label-view text-right'>{{ SiteHelpers::activeLang('Address', (isset($fields['Direccion']['language'])? $fields['Direccion']['language'] : array())) }}</td>
						<td>{{ $row->Direccion}} </td>
						
					</tr>
				
					<tr>
						<td width='30%' class='label-view text-right'>{{ SiteHelpers::activeLang('Entry By', (isset($fields['entry_by']['language'])? $fields['entry_by']['language'] : array())) }}</td>
						<td>{{ SiteHelpers::formatLookUp($row->entry_by,'entry_by','1:tb_users:id:first_name|last_name') }} </td>
						
					</tr>
				
					<tr>
						<td width='30%' class='label-view text-right'>{{ SiteHelpers::activeLang('UpdatedOn', (isset($fields['updatedOn']['language'])? $fields['updatedOn']['language'] : array())) }}</td>
						<td>{{ date('d/m/Y',strtotime($row->updatedOn)) }} </td>
						
					</tr>
						
					<tr>
						<td width='30%' class='label-view text-right'></td>
						<td> <a href="javascript:history.go(-1)" class="btn btn-primary"> Back To Grid <a> </td>
						
					</tr>					
				
			</tbody>	
		</table>   

	 
	
	</div>
</div>	