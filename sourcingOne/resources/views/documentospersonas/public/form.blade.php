

		 {!! Form::open(array('url'=>'documentospersonas/savepublic', 'class'=>'form-horizontal','files' => true , 'parsley-validate'=>'','novalidate'=>' ')) !!}

	@if(Session::has('messagetext'))
	  
		   {!! Session::get('messagetext') !!}
	   
	@endif
	<ul class="parsley-error-list">
		@foreach($errors->all() as $error)
			<li>{{ $error }}</li>
		@endforeach
	</ul>		


<div class="col-md-12">
						<fieldset><legend> DocumentosPersonas</legend>
									
									  <div class="form-group  " >
										<label for="IdDocumento" class=" control-label col-md-4 text-left"> IdDocumento </label>
										<div class="col-md-6">
										  {!! Form::text('IdDocumento', $row['IdDocumento'],array('class'=>'form-control', 'placeholder'=>'',   )) !!} 
										 </div> 
										 <div class="col-md-2">
										 	
										 </div>
									  </div> 					
									  <div class="form-group  " >
										<label for="IdRequisito" class=" control-label col-md-4 text-left"> IdRequisito </label>
										<div class="col-md-6">
										  <select name='IdRequisito' rows='5' id='IdRequisito' class='select2 '   ></select> 
										 </div> 
										 <div class="col-md-2">
										 	
										 </div>
									  </div> 					
									  <div class="form-group  " >
										<label for="IdTipoDocumento" class=" control-label col-md-4 text-left"> IdTipoDocumento </label>
										<div class="col-md-6">
										  <select name='IdTipoDocumento' rows='5' id='IdTipoDocumento' class='select2 '   ></select> 
										 </div> 
										 <div class="col-md-2">
										 	
										 </div>
									  </div> 					
									  <div class="form-group  " >
										<label for="Entidad" class=" control-label col-md-4 text-left"> Entidad </label>
										<div class="col-md-6">
										  <select name='Entidad' rows='5' id='Entidad' class='select2 '   ></select> 
										 </div> 
										 <div class="col-md-2">
										 	
										 </div>
									  </div> 					
									  <div class="form-group  " >
										<label for="IdEntidad" class=" control-label col-md-4 text-left"> IdEntidad </label>
										<div class="col-md-6">
										  {!! Form::text('IdEntidad', $row['IdEntidad'],array('class'=>'form-control', 'placeholder'=>'',   )) !!} 
										 </div> 
										 <div class="col-md-2">
										 	
										 </div>
									  </div> 					
									  <div class="form-group  " >
										<label for="Documento" class=" control-label col-md-4 text-left"> Documento </label>
										<div class="col-md-6">
										  <textarea name='Documento' rows='5' id='Documento' class='form-control '  
				           >{{ $row['Documento'] }}</textarea> 
										 </div> 
										 <div class="col-md-2">
										 	
										 </div>
									  </div> 					
									  <div class="form-group  " >
										<label for="DocumentoURL" class=" control-label col-md-4 text-left"> DocumentoURL </label>
										<div class="col-md-6">
										  {!! Form::text('DocumentoURL', $row['DocumentoURL'],array('class'=>'form-control', 'placeholder'=>'',   )) !!} 
										 </div> 
										 <div class="col-md-2">
										 	
										 </div>
									  </div> 					
									  <div class="form-group  " >
										<label for="FechaVencimiento" class=" control-label col-md-4 text-left"> FechaVencimiento </label>
										<div class="col-md-6">
										  
				<div class="input-group m-b" style="width:150px !important;">
					{!! Form::text('FechaVencimiento', $row['FechaVencimiento'],array('class'=>'form-control datetime', 'style'=>'width:150px !important;')) !!}
					<span class="input-group-addon"><i class="fa fa-calendar"></i></span>
				</div>
				 
										 </div> 
										 <div class="col-md-2">
										 	
										 </div>
									  </div> 					
									  <div class="form-group  " >
										<label for="IdEstatus" class=" control-label col-md-4 text-left"> IdEstatus </label>
										<div class="col-md-6">
										  {!! Form::text('IdEstatus', $row['IdEstatus'],array('class'=>'form-control', 'placeholder'=>'',   )) !!} 
										 </div> 
										 <div class="col-md-2">
										 	
										 </div>
									  </div> 					
									  <div class="form-group  " >
										<label for="CreatedOn" class=" control-label col-md-4 text-left"> CreatedOn </label>
										<div class="col-md-6">
										  
				<div class="input-group m-b" style="width:150px !important;">
					{!! Form::text('createdOn', $row['createdOn'],array('class'=>'form-control datetime', 'style'=>'width:150px !important;')) !!}
					<span class="input-group-addon"><i class="fa fa-calendar"></i></span>
				</div>
				 
										 </div> 
										 <div class="col-md-2">
										 	
										 </div>
									  </div> 					
									  <div class="form-group  " >
										<label for="Entry By" class=" control-label col-md-4 text-left"> Entry By </label>
										<div class="col-md-6">
										  {!! Form::text('entry_by', $row['entry_by'],array('class'=>'form-control', 'placeholder'=>'',   )) !!} 
										 </div> 
										 <div class="col-md-2">
										 	
										 </div>
									  </div> 					
									  <div class="form-group  " >
										<label for="UpdatedOn" class=" control-label col-md-4 text-left"> UpdatedOn </label>
										<div class="col-md-6">
										  
				<div class="input-group m-b" style="width:150px !important;">
					{!! Form::text('updatedOn', $row['updatedOn'],array('class'=>'form-control datetime', 'style'=>'width:150px !important;')) !!}
					<span class="input-group-addon"><i class="fa fa-calendar"></i></span>
				</div>
				 
										 </div> 
										 <div class="col-md-2">
										 	
										 </div>
									  </div> </fieldset>
			</div>
			
			

			<div style="clear:both"></div>	
				
					
				  <div class="form-group">
					<label class="col-sm-4 text-right">&nbsp;</label>
					<div class="col-sm-8">	
					<button type="submit" name="apply" class="btn btn-info btn-sm" ><i class="fa  fa-check-circle"></i> {{ Lang::get('core.sb_apply') }}</button>
					<button type="submit" name="submit" class="btn btn-primary btn-sm" ><i class="fa  fa-save "></i> {{ Lang::get('core.sb_save') }}</button>
				  </div>	  
			
		</div> 
		 
		 {!! Form::close() !!}
		 
   <script type="text/javascript">
	$(document).ready(function() { 
		
		
		$("#IdRequisito").jCombo("{!! url('documentospersonas/comboselect?filter=tbl_requisitos:IdRequisito:IdRequisito') !!}",
		{  selected_value : '{{ $row["IdRequisito"] }}' });
		
		$("#IdTipoDocumento").jCombo("{!! url('documentospersonas/comboselect?filter=tbl_tipos_documentos:IdTipoDocumento:IdTipoDocumento') !!}",
		{  selected_value : '{{ $row["IdTipoDocumento"] }}' });
		
		$("#Entidad").jCombo("{!! url('documentospersonas/comboselect?filter=tbl_entidades:IdEntidad:IdEntidad') !!}",
		{  selected_value : '{{ $row["Entidad"] }}' });
		 

		$('.removeCurrentFiles').on('click',function(){
			var removeUrl = $(this).attr('href');
			$.get(removeUrl,function(response){});
			$(this).parent('div').empty();	
			return false;
		});		
		
	});
	</script>		 
