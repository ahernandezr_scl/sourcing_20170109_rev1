@if($setting['view-method'] =='native')
<div class="sbox">
	<div class="sbox-title">  
		<h4> <i class="fa fa-table"></i> <?php echo $pageTitle ;?> <small>{{ $pageNote }}</small>
			<a href="javascript:void(0)" class="collapse-close pull-right btn btn-xs btn-danger" onclick="ajaxViewClose('#{{ $pageModule }}')">
			<i class="fa fa fa-times"></i></a>
		</h4>
	 </div>

	<div class="sbox-content"> 
@endif	

		<table class="table table-striped table-bordered" >
			<tbody>	
				
					<tr>
						<td width='30%' class='label-view text-right'>{{ SiteHelpers::activeLang('Id Kpi', (isset($fields['id_kpi']['language'])? $fields['id_kpi']['language'] : array())) }}</td>
						<td>{{ SiteHelpers::formatLookUp($row->id_kpi,'id_kpi','1:vw_kpi:id_kpi:kpi_nombreIndicador|contrato') }} </td>

					</tr>
				
					<tr>
						<td width='30%' class='label-view text-right'>{{ SiteHelpers::activeLang('Date of data', (isset($fields['kpiDet_fecha']['language'])? $fields['kpiDet_fecha']['language'] : array())) }}</td>
						<td>{{ date('d-m-Y',strtotime($row->kpiDet_fecha)) }} </td>

					</tr>
				
					<tr>
						<td width='30%' class='label-view text-right'>{{ SiteHelpers::activeLang('Min value KPI', (isset($fields['kpiDet_min']['language'])? $fields['kpiDet_min']['language'] : array())) }}</td>
						<td>{{ $row->kpiDet_min}} </td>

					</tr>
				
					<tr>
						<td width='30%' class='label-view text-right'>{{ SiteHelpers::activeLang('Max value KPI', (isset($fields['kpiDet_max']['language'])? $fields['kpiDet_max']['language'] : array())) }}</td>
						<td>{{ $row->kpiDet_max}} </td>

					</tr>
				
					<tr>
						<td width='30%' class='label-view text-right'>{{ SiteHelpers::activeLang('Goal KPI', (isset($fields['kpiDet_meta']['language'])? $fields['kpiDet_meta']['language'] : array())) }}</td>
						<td>{{ $row->kpiDet_meta}} </td>

					</tr>
				
					<tr>
						<td width='30%' class='label-view text-right'>{{ SiteHelpers::activeLang('Score KPI', (isset($fields['kpiDet_puntaje']['language'])? $fields['kpiDet_puntaje']['language'] : array())) }}</td>
						<td>{{ $row->kpiDet_puntaje}} </td>

					</tr>
				
			</tbody>	
		</table>  
			
		 	

@if($setting['form-method'] =='native')
	</div>	
</div>	
@endif	

<script>
$(document).ready(function(){

});
</script>	