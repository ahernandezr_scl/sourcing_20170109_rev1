<div class="m-t" style="padding-top:25px;">	
    <div class="row m-b-lg animated fadeInDown delayp1 text-center">
        <h3> {{ $pageTitle }} <small> {{ $pageNote }} </small></h3>
        <hr />       
    </div>
</div>
<div class="m-t">
	<div class="table-responsive" > 	

		<table class="table table-striped table-bordered" >
			<tbody>	
		
			
					<tr>
						<td width='30%' class='label-view text-right'>{{ SiteHelpers::activeLang('Id Kpi', (isset($fields['id_kpi']['language'])? $fields['id_kpi']['language'] : array())) }}</td>
						<td>{{ SiteHelpers::formatLookUp($row->id_kpi,'id_kpi','1:vw_kpi:id_kpi:kpi_nombreIndicador|contrato') }} </td>

					</tr>
				
					<tr>
						<td width='30%' class='label-view text-right'>{{ SiteHelpers::activeLang('Date of data', (isset($fields['kpiDet_fecha']['language'])? $fields['kpiDet_fecha']['language'] : array())) }}</td>
						<td>{{ date('d-m-Y',strtotime($row->kpiDet_fecha)) }} </td>

					</tr>
				
					<tr>
						<td width='30%' class='label-view text-right'>{{ SiteHelpers::activeLang('Min value KPI', (isset($fields['kpiDet_min']['language'])? $fields['kpiDet_min']['language'] : array())) }}</td>
						<td>{{ $row->kpiDet_min}} </td>

					</tr>
				
					<tr>
						<td width='30%' class='label-view text-right'>{{ SiteHelpers::activeLang('Max value KPI', (isset($fields['kpiDet_max']['language'])? $fields['kpiDet_max']['language'] : array())) }}</td>
						<td>{{ $row->kpiDet_max}} </td>

					</tr>
				
					<tr>
						<td width='30%' class='label-view text-right'>{{ SiteHelpers::activeLang('Goal KPI', (isset($fields['kpiDet_meta']['language'])? $fields['kpiDet_meta']['language'] : array())) }}</td>
						<td>{{ $row->kpiDet_meta}} </td>

					</tr>
				
					<tr>
						<td width='30%' class='label-view text-right'>{{ SiteHelpers::activeLang('Score KPI', (isset($fields['kpiDet_puntaje']['language'])? $fields['kpiDet_puntaje']['language'] : array())) }}</td>
						<td>{{ $row->kpiDet_puntaje}} </td>

					</tr>
						
					<tr>
						<td width='30%' class='label-view text-right'></td>
						<td> <a href="javascript:history.go(-1)" class="btn btn-primary"> Back To Grid <a> </td>
						
					</tr>					
				
			</tbody>	
		</table>   

	 
	
	</div>
</div>	