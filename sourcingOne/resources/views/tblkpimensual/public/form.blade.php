

		 {!! Form::open(array('url'=>'tblkpimensual/savepublic', 'class'=>'form-horizontal','files' => true , 'parsley-validate'=>'','novalidate'=>' ')) !!}

	@if(Session::has('messagetext'))
	  
		   {!! Session::get('messagetext') !!}
	   
	@endif
	<ul class="parsley-error-list">
		@foreach($errors->all() as $error)
			<li>{{ $error }}</li>
		@endforeach
	</ul>		


<div class="col-md-12">
						<fieldset><legend> KPI Details</legend>
				
									  <div class="form-group  " >
										<label for="Id Kpi" class=" control-label col-md-4 text-left"> Id Kpi </label>
										<div class="col-md-6">
										  <select name='id_kpi' rows='5' id='id_kpi' class='select2 '   ></select>
										 </div>
										 <div class="col-md-2">
										 	
										 </div>
									  </div> 
									  <div class="form-group  " >
										<label for="Date of data" class=" control-label col-md-4 text-left"> Date of data </label>
										<div class="col-md-6">
										  
				<div class="input-group m-b" style="width:150px !important;">
					{!! Form::text('kpiDet_fecha', $row['kpiDet_fecha'],array('class'=>'form-control date')) !!}
					<span class="input-group-addon"><i class="fa fa-calendar"></i></span>
				</div>
										 </div>
										 <div class="col-md-2">
										 	
										 </div>
									  </div> 
									  <div class="form-group  " >
										<label for="Min value KPI" class=" control-label col-md-4 text-left"> Min value KPI </label>
										<div class="col-md-6">
										  {!! Form::text('kpiDet_min', $row['kpiDet_min'],array('class'=>'form-control', 'placeholder'=>'',   )) !!}
										 </div>
										 <div class="col-md-2">
										 	
										 </div>
									  </div> 
									  <div class="form-group  " >
										<label for="Max value KPI" class=" control-label col-md-4 text-left"> Max value KPI </label>
										<div class="col-md-6">
										  {!! Form::text('kpiDet_max', $row['kpiDet_max'],array('class'=>'form-control', 'placeholder'=>'',   )) !!}
										 </div>
										 <div class="col-md-2">
										 	
										 </div>
									  </div> 
									  <div class="form-group  " >
										<label for="Goal KPI" class=" control-label col-md-4 text-left"> Goal KPI </label>
										<div class="col-md-6">
										  {!! Form::text('kpiDet_meta', $row['kpiDet_meta'],array('class'=>'form-control', 'placeholder'=>'',   )) !!}
										 </div>
										 <div class="col-md-2">
										 	
										 </div>
									  </div> 
									  <div class="form-group  " >
										<label for="Score KPI" class=" control-label col-md-4 text-left"> Score KPI </label>
										<div class="col-md-6">
										  {!! Form::text('kpiDet_puntaje', $row['kpiDet_puntaje'],array('class'=>'form-control', 'placeholder'=>'',   )) !!}
										 </div>
										 <div class="col-md-2">
										 	
										 </div>
									  </div> </fieldset>
			</div>

			

			<div style="clear:both"></div>	
				
					
				  <div class="form-group">
					<label class="col-sm-4 text-right">&nbsp;</label>
					<div class="col-sm-8">	
					<button type="submit" name="apply" class="btn btn-info btn-sm" ><i class="fa  fa-check-circle"></i> {{ Lang::get('core.sb_apply') }}</button>
					<button type="submit" name="submit" class="btn btn-primary btn-sm" ><i class="fa  fa-save "></i> {{ Lang::get('core.sb_save') }}</button>
				  </div>	  
			
		</div> 
		 
		 {!! Form::close() !!}
		 
   <script type="text/javascript">
	$(document).ready(function() { 
		
		
		$("#id_kpi").jCombo("{!! url('tblkpimensual/comboselect?filter=vw_kpi:id_kpi:kpi_nombreIndicador|contrato') !!}",
		{  selected_value : '{{ $row["id_kpi"] }}' });
		 

		$('.removeCurrentFiles').on('click',function(){
			var removeUrl = $(this).attr('href');
			$.get(removeUrl,function(response){});
			$(this).parent('div').empty();	
			return false;
		});		
		
	});
	</script>		 
