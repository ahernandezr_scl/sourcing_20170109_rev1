<?php
$servername = "localhost";
$username = "root";
$password = "8044302";
$year;
$mes;$reg;

#try {
    $conn2 = new PDO("mysql:host=$servername;dbname=andina_koadmin", $username, $password);
    // set the PDO error mode to exception
#    $conn2->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
#   echo "Connected successfully";
#    }
#catch(PDOException $e)
#    {
#    echo "Connection failed: " . $e->getMessage();
#    }


?>

<?php  
  if (isset($_GET["year"])) {
    $year = $_GET["year"];
   }
  else{  
    $year = date("Y");
}

  if (isset($_GET["mes"])) {
    $mes = $_GET["mes"];
   }
  else{  
    $mes = 0;
}


 if (isset($_GET["proc"])) {
    $proc = $_GET["proc"];
   }
  else{  
    $proc = 0;
}

 if (isset($_GET["id"])) {
    $id = $_GET["id"];
   }
  else{  
    $id = 0;
}

 if (isset($_GET["tip"])) {
    $tip = $_GET["tip"];
   }
  else{  
    $tip = 0;
}
?>


  
<?php 
if($proc == 0){
     
   $sql_panel='SELECT  CAST(SUM(n) AS UNSIGNED) Negocio,
      (SELECT ambito_nombre FROM tbl_planes_y_programas_ambito WHERE ambito_id = 1) amb_neg,
      CAST(SUM(f) AS UNSIGNED) Financiero ,
      (SELECT ambito_nombre FROM tbl_planes_y_programas_ambito WHERE ambito_id = 2) amb_fin,
      CAST(SUM(l) AS UNSIGNED) Laboral, 
      (SELECT ambito_nombre FROM tbl_planes_y_programas_ambito WHERE ambito_id = 3) amb_lab,
      CAST(SUM(s) AS UNSIGNED) seguridad,  
      (SELECT ambito_nombre FROM tbl_planes_y_programas_ambito WHERE ambito_id = 4) amb_seg,
      CAST(AVG(n+f+l+s) AS UNSIGNED) general, cont_proveedor
      FROM
      (
      SELECT CAST(COALESCE(AVG(prom_neg),0) AS UNSIGNED) n,0 f,0 l ,0 s,contrato_id
      FROM
      (
      SELECT contrato_id,CAST(AVG(kpi) AS DECIMAL(4,1)) prom_neg

      FROM
      (SELECT  b.contrato_id,kpiDet_fecha,  (SUM(CASE WHEN kpi_tipo_calc = 1 THEN CASE WHEN kpiDet_puntaje >= kpiDet_meta THEN 1 ELSE 0 END WHEN kpi_tipo_calc = 2 THEN CASE WHEN kpiDet_puntaje <= kpiDet_meta THEN 1 ELSE 0 END WHEN kpi_tipo_calc = 3 THEN CASE WHEN kpiDet_puntaje BETWEEN kpiDet_min AND kpiDet_max THEN 1 ELSE 0 END WHEN kpi_tipo_calc = 4 THEN 1 END )/
      COUNT(DISTINCT a.id_kpi) )*100 kpi
      FROM tbl_kpimensual a INNER JOIN tbl_kpigral b
      ON a.id_kpi = b.id_kpi
      INNER JOIN dim_tiempo ON kpiDet_fecha = fecha
      INNER JOIN tbl_contrato c ON c.contrato_id = b.contrato_id
      WHERE kpiDet_puntaje IS NOT NULL
      AND 

      anio = '.$year.' AND 
      CASE WHEN '.$mes.'= 0 THEN 1=1 ELSE  mes = '.$mes.' END

      and c.contrato_id='.$id.'
      GROUP BY 1,2) asd

      UNION ALL

      SELECT b.contrato_id,
      CASE WHEN AVG(eval_puntaje) >70 THEN 75 + ((CASE WHEN AVG(eval_puntaje) >70 THEN 25/30 ELSE 75/70 END)*(AVG(eval_puntaje) - 70)) 
      ELSE 75 - ((CASE WHEN AVG(eval_puntaje) >70 THEN 25/30 ELSE 75/70 END)*(AVG(eval_puntaje) - 70)) END f
      FROM tbl_evalcontratistas a INNER JOIN dim_tiempo 
      ON eval_fecha = fecha 
      INNER JOIN tbl_contrato b ON a.contrato_id = b.contrato_id
      WHERE  
      eval_puntaje IS NOT NULL
      AND 
      anio = '.$year.' AND 
      CASE WHEN '.$mes.'= 0 THEN 1=1 ELSE  mes = '.$mes.' END

      and b.contrato_id='.$id.'

      UNION ALL

      SELECT b.contrato_id,CAST(AVG(resultado) AS DECIMAL(4,1)) puntaje
      FROM tbl_calidad_adm_cont a INNER JOIN dim_tiempo 
      ON fec_rev = fecha
      INNER JOIN tbl_contrato b ON a.contrato_id = b.contrato_id
      WHERE resultado IS NOT NULL
      AND 
      anio = '.$year.' AND 
      CASE WHEN '.$mes.'= 0 THEN 1=1 ELSE  mes = '.$mes.' END

      and b.contrato_id='.$id.'
      ) ambito

      UNION ALL 

      -- financiero

      SELECT 0, CAST(COALESCE(AVG(prom_fin),0) AS UNSIGNED) f ,0,0,contrato_id
      FROM
      (SELECT c.contrato_id,
      CASE WHEN AVG(valor) > 50 THEN 75 - (AVG(valor) -50) * (CASE WHEN AVG(valor) > 50 THEN 75/50 ELSE 25/50 END) ELSE
      75 + (AVG(valor) -50) * (CASE WHEN AVG(valor) > 50 THEN 75/50 ELSE 25/50 END) END prom_fin
      FROM tbl_eval_financiera a
      INNER JOIN tbl_contrato c ON c.contrato_id = a.contrato_id
      INNER JOIN dim_tiempo d ON fecha_validez = d.fecha
      WHERE
      anio = '.$year.' AND 
      CASE WHEN '.$mes.'= 0 THEN 1=1 ELSE  mes = '.$mes.' END
      and c.contrato_id='.$id.'

      UNION ALL

      SELECT c.contrato_id,
      CASE WHEN AVG(per_moros*100) > 30 THEN 75 - ABS(AVG(per_moros*100) -30) * (CASE WHEN AVG(per_moros*100) > 30 THEN 75/67 ELSE 25/13 END) ELSE
      75 + ABS(AVG(per_moros*100) -30) * (CASE WHEN AVG(per_moros*100) > 30 THEN 75/67 ELSE 25/13 END) END prom_trib
      FROM tbl_morosidad a
      INNER JOIN dim_tiempo d ON fecha_Eval = d.fecha
      INNER JOIN tbl_contrato c ON c.contrato_id = a.contrato_id
      WHERE 
      anio = '.$year.' AND 
      CASE WHEN '.$mes.'= 0 THEN 1=1 ELSE  mes = '.$mes.' END
      and c.contrato_id='.$id.'

      UNION ALL

      SELECT b.contrato_id,(1 -( COUNT(DISTINCT (CASE WHEN tCont_eval = \'MALO\' THEN a.contrato_id ELSE 0 END))/ 
      COUNT(DISTINCT a.contrato_id)) )*100
      FROM
      tbl_contratista_tributario a INNER JOIN tbl_contrato b ON a.contrato_id = b.contrato_id
      INNER JOIN dim_tiempo d ON a.tCont_fecha = d.fecha
      WHERE 
      anio = '.$year.' AND 
      CASE WHEN '.$mes.'= 0 THEN 1=1 ELSE  mes = '.$mes.' END
      and b.contrato_id='.$id.'

      UNION ALL

      SELECT c.contrato_id, AVG((total_fondos + prendas)/(mutuos + total_impacto))*100
      FROM tbl_garantias a INNER JOIN dim_tiempo b
      ON a.fecha = b.fecha
      INNER JOIN tbl_contrato c ON c.contrato_id = a.contrato_id
      WHERE  
      anio = '.$year.' AND 
      CASE WHEN '.$mes.'= 0 THEN 1=1 ELSE  mes = '.$mes.' END
      and c.contrato_id='.$id.'
      ) finan

      UNION ALL

      -- laboral

      SELECT 0,0,CAST(COALESCE(AVG(prom_lab),0) AS UNSIGNED) l,0,contrato_id
      FROM
      (SELECT c.contrato_id,
      CASE WHEN AVG(valor)*100 > 16 THEN 75 - (ABS(AVG(valor)*100 -16)) * (CASE WHEN AVG(valor)*100 > 16 THEN 75/84 ELSE 25/16 END) ELSE
      75 + (ABS(AVG(valor)*100 -16)) * (CASE WHEN AVG(valor)*100 > 16 THEN 75/84 ELSE 25/16 END) END prom_lab
      FROM tbl_obliglab_repo a INNER JOIN dim_tiempo b
      ON a.fecha = b.fecha
      INNER JOIN tbl_contrato c ON c.contrato_id = a.contrato_id
      WHERE anio = '.$year.' AND 
      CASE WHEN '.$mes.'= 0 THEN 1=1 ELSE  mes = '.$mes.' END
      and c.contrato_id='.$id.'

      UNION ALL

      SELECT  c.contrato_id,((SELECT COUNT(DISTINCT contrato_id) FROM tbl_contrato WHERE cont_fechaFin > CURRENT_DATE) - 
      COUNT(DISTINCT a.contrato_id))/(SELECT COUNT(DISTINCT contrato_id) FROM tbl_contrato WHERE cont_fechaFin > CURRENT_DATE) * 100
      FROM tbl_contratistacondicionfin a
      INNER JOIN dim_tiempo b
      ON a.finCont_fecha = b.fecha
      INNER JOIN tbl_contrato c ON c.contrato_id = a.contrato_id
      WHERE anio = '.$year.' AND 
      CASE WHEN '.$mes.'= 0 THEN 1=1 ELSE  mes = '.$mes.' END
      and c.contrato_id='.$id.'

      UNION ALL

      SELECT  c.contrato_id,CAST(AVG((PERC_LAB)) AS DECIMAL(4,1)) puntaje
      FROM tbl_fiscalización a INNER JOIN dim_tiempo b
      ON a.fecha = b.fecha
      INNER JOIN tbl_contrato c ON c.contrato_id = a.contrato_id
      WHERE anio = '.$year.' AND 
      CASE WHEN '.$mes.'= 0 THEN 1=1 ELSE  mes = '.$mes.' END
      and c.contrato_id='.$id.'
      ) lab 

      UNION ALL

      -- seguridad

      SELECT 0,0,0, CAST(COALESCE(AVG(prom_seg),0) AS DECIMAL(3,0)) s,contrato_id
      FROM
      (SELECT c.contrato_id,
      CASE WHEN (SUM(n_accid)/SUM(hh)*200000)> 5.2 THEN 0 ELSE
      CASE WHEN (SUM(n_accid)/SUM(hh)*200000)> 2.6 THEN 75 - ABS((SUM(n_accid)/SUM(hh)*200000)-2.6) * (CASE WHEN (SUM(n_accid)/SUM(hh)*200000) > 2.6 THEN 75/2.6 ELSE 25/2.6 END) ELSE
      75 + ABS((SUM(n_accid)/SUM(hh)*200000) -2.6) * (CASE WHEN (SUM(n_accid)/SUM(hh)*200000) > 2.6 THEN 75/2.6 ELSE 25/2.6 END) END
      END prom_seg
      FROM tbl_accidentes a
      INNER JOIN dim_tiempo d ON a.fecha_informe = d.fecha
      INNER JOIN tbl_contrato c ON a.contrato_id = c.contrato_id
      WHERE 
      anio = '.$year.' AND 
      CASE WHEN '.$mes.'= 0 THEN 1=1 ELSE  mes = '.$mes.' END
      and c.contrato_id='.$id.'

      UNION ALL

      SELECT c.contrato_id,
      CASE WHEN (SUM(DIAS_PERD)/SUM(hh)*200000) > 60 THEN 0 ELSE 
      CASE WHEN (SUM(DIAS_PERD)/SUM(hh)*200000)> 31.1 THEN 75 - ABS(( SUM(DIAS_PERD)/SUM(hh)*200000 )-31.1) * (CASE WHEN (SUM(DIAS_PERD)/SUM(hh)*200000) > 31.1 THEN 75/31.1 ELSE 25/31.1 END) ELSE
      75 + ABS((SUM(DIAS_PERD)/SUM(hh)*200000) -31.1) * (CASE WHEN (SUM(DIAS_PERD)/SUM(hh)*200000) > 31.1 THEN 75/31.1 ELSE 25/31.1 END) END 
      END f
      FROM tbl_accidentes a
      INNER JOIN dim_tiempo d ON a.fecha_informe = d.fecha
      INNER JOIN tbl_contrato c ON a.contrato_id = c.contrato_id
      WHERE 
      anio = '.$year.' AND 
      CASE WHEN '.$mes.'= 0 THEN 1=1 ELSE  mes = '.$mes.' END
      and c.contrato_id='.$id.'

      UNION ALL

      SELECT c.contrato_id,AVG((PERC_COND + perc_equip)/2) 
      FROM tbl_fiscalización a
      INNER JOIN dim_tiempo d ON a.fecha = d.fecha
      INNER JOIN tbl_contrato c ON a.contrato_id = c.contrato_id
       WHERE 
      anio = '.$year.' AND 
      CASE WHEN '.$mes.'= 0 THEN 1=1 ELSE  mes = '.$mes.' END
      and c.contrato_id='.$id.'
      ) seg
      ) panel inner join tbl_contrato co on panel.contrato_id = co.contrato_id';


  $sql_neg='SELECT CAST(AVG(kpi) AS DECIMAL(4,1)) dato, (select ambito_nombre from tbl_planes_y_programas_ambitosub where subambito_id = 1) nombre

        FROM
        (SELECT  b.contrato_id,kpiDet_fecha,  

        (SUM(
            CASE WHEN kpi_tipo_calc = 1 THEN
                CASE WHEN kpiDet_puntaje >= kpiDet_meta THEN 1 ELSE 0 END
               WHEN kpi_tipo_calc = 2 THEN
            CASE WHEN kpiDet_puntaje <= kpiDet_meta THEN 1 ELSE 0 END
               WHEN kpi_tipo_calc = 3 THEN
            CASE WHEN kpiDet_puntaje BETWEEN kpiDet_min AND kpiDet_max THEN 1 ELSE 0 END
               WHEN kpi_tipo_calc = 4 THEN 1
              
          END )/
        COUNT(DISTINCT a.id_kpi) )*100 kpi
        FROM tbl_kpimensual a INNER JOIN tbl_kpigral b
        ON a.id_kpi = b.id_kpi
        INNER JOIN dim_tiempo ON kpiDet_fecha = fecha
        inner join tbl_contrato c on b.contrato_id = c.contrato_id
        WHERE kpiDet_puntaje IS NOT NULL
        AND 
        case when '.$mes.'=0 then Anio = '.$year.' else
        mes = '.$mes.' AND Anio = '.$year.'
        end

        and c.contrato_id ='.$id.'
        GROUP BY 1,2) asd

        UNION ALL

        Select cast( AVG(eval_puntaje) as decimal(4,1)) puntaje, (select ambito_nombre from tbl_planes_y_programas_ambitosub where subambito_id = 2) nombre

        FROM tbl_evalcontratistas a INNER JOIN dim_tiempo 
        ON eval_fecha = fecha 
        INNER JOIN tbl_contrato b ON a.contrato_id = b.contrato_id
        where  
        eval_puntaje IS NOT NULL
        AND 
        case when '.$mes.'=0 then Anio = '.$year.' else
        mes IN (SELECT DISTINCT mes FROM dim_tiempo WHERE semestre  = (SELECT DISTINCT semestre FROM dim_tiempo WHERE mes= '.$mes.' AND Anio = '.$year.'))
         AND Anio = '.$year.' end

        and a.contrato_id ='.$id.'
        UNION ALL

        SELECT CAST(AVG(resultado) AS DECIMAL(4,1)) puntaje, (select ambito_nombre from tbl_planes_y_programas_ambitosub where subambito_id = 3) nombre

        FROM tbl_calidad_adm_cont a INNER JOIN dim_tiempo 
        ON fec_rev = fecha
        inner join tbl_contrato c on a.contrato_id = c.contrato_id
        WHERE resultado IS NOT NULL
        AND 
        case when '.$mes.'=0 then Anio = '.$year.' else
        mes IN (SELECT DISTINCT mes FROM dim_tiempo WHERE trimestre  = (SELECT DISTINCT trimestre FROM dim_tiempo WHERE mes= '.$mes.' AND Anio = '.$year.'))
        AND Anio = '.$year.' end

        and a.contrato_id ='.$id.'

        union all

        SELECT CAST(SUM(financieroand_mtoreal)/1000000 AS DECIMAL(10,1)) puntaje, (select ambito_nombre from tbl_planes_y_programas_ambitosub where subambito_id = 4) nombre

         FROM tbl_financiero_and a INNER JOIN dim_tiempo c
        ON financieroand_fecha = c.fecha
        INNER JOIN tbl_contrato b ON a.contrato_id = b.contrato_id 
        WHERE 
        CASE WHEN '.$mes.'=0 THEN Anio = '.$year.' ELSE
        mes = '.$mes.' AND Anio = '.$year.'
        END

        AND a.contrato_id ='.$id;

  $sql_seg='SELECT CAST(SUM(n_accid)/SUM(hh)*200000 AS DECIMAL(4,2)) dato , (select ambito_nombre from tbl_planes_y_programas_ambitosub where subambito_id = 12) nombre

        FROM tbl_accidentes a
        INNER JOIN dim_tiempo d ON a.fecha_informe = d.fecha
        INNER JOIN tbl_contrato c ON a.contrato_id = c.contrato_id

         WHERE 
        CASE WHEN  '.$mes.'=0 THEN Anio = '.$year.' ELSE
        mes= '.$mes.' AND Anio = '.$year.'
        END

        AND c.contrato_id ='.$id.'

        UNION ALL

        SELECT CAST(SUM(DIAS_PERD)/SUM(hh)*200000 AS DECIMAL(4,2)), (select ambito_nombre from tbl_planes_y_programas_ambitosub where subambito_id = 13) nombre
        FROM tbl_accidentes a
        INNER JOIN dim_tiempo d ON a.fecha_informe = d.fecha
        INNER JOIN tbl_contrato c ON a.contrato_id = c.contrato_id

         WHERE 
        CASE WHEN  '.$mes.'=0 THEN Anio = '.$year.' ELSE
        mes= '.$mes.' AND Anio = '.$year.'
        END

        AND c.contrato_id ='.$id.'

        UNION ALL

        SELECT CAST(AVG((PERC_COND + perc_equip)/2) AS DECIMAL(4,1)), (select ambito_nombre from tbl_planes_y_programas_ambitosub where subambito_id = 14) nombre
        FROM tbl_fiscalización a
        INNER JOIN dim_tiempo d ON a.fecha = d.fecha
        INNER JOIN tbl_contrato c ON a.contrato_id = c.contrato_id

         WHERE 
        CASE WHEN  '.$mes.'=0 THEN Anio = '.$year.' ELSE
        mes= '.$mes.' AND Anio = '.$year.'
        END

        AND c.contrato_id ='.$id;
  $sql_fin='SELECT CAST(AVG(valor) AS DECIMAL(4,1)) dato, (select ambito_nombre from tbl_planes_y_programas_ambitosub where subambito_id = 5) nombre
          FROM tbl_eval_financiera a
          INNER JOIN tbl_contrato c ON c.contrato_id = a.contrato_id
          INNER JOIN dim_tiempo d ON fecha_validez = d.fecha
          where a.contrato_id ='.$id.'
          and case when  '.$mes.'=0 then Anio = '.$year.' else
          mes= '.$mes.' AND Anio = '.$year.'
          end 


          UNION ALL

          SELECT cast(AVG(per_moros*100) as DECIMAL(4,1)), (select ambito_nombre from tbl_planes_y_programas_ambitosub where subambito_id = 6) nombre

          FROM tbl_morosidad a
          INNER JOIN dim_tiempo d ON fecha_Eval = d.fecha
          inner join tbl_contrato b on a.contrato_id = b.contrato_id
          WHERE 
          case when  '.$mes.'=0 then Anio = '.$year.' else
          mes= '.$mes.' AND Anio = '.$year.'
          end 

          and b.contrato_id ='.$id.'

          UNION ALL

          SELECT cast((SUM(CASE WHEN tCont_eval = \'MALO\' THEN 1 ELSE 0 END)/ 
          COUNT(distinct a.contrato_id))*100 as DECIMAL(4,1)), (select ambito_nombre from tbl_planes_y_programas_ambitosub where subambito_id = 7) nombre
          FROM
          tbl_contratista_tributario a INNER JOIN tbl_contrato b ON a.contrato_id = b.contrato_id
          INNER JOIN dim_tiempo d ON a.tCont_fecha = d.fecha
          WHERE 
          case when  '.$mes.'=0 then Anio = '.$year.' else
          mes= '.$mes.' AND Anio = '.$year.'
          end 

          and b.contrato_id ='.$id.'


          UNION ALL

          SELECT  CAST(AVG((total_fondos + prendas)/(mutuos + total_impacto))*100 AS DECIMAL(4,1)) puntaje, (select ambito_nombre from tbl_planes_y_programas_ambitosub where subambito_id = 8) nombre

          FROM tbl_garantias a INNER JOIN dim_tiempo b
          ON a.fecha = b.fecha
          inner join tbl_contrato c on a.contrato_id = c.contrato_id
          WHERE CASE WHEN  '.$mes.'=0 THEN Anio = '.$year.' ELSE
          mes = '.$mes.' AND Anio = '.$year.' END

          and c.contrato_id ='.$id;

  $sql_ent='SELECT  CAST(AVG(valor)*100 AS DECIMAL(4,1)) dato, (select ambito_nombre from tbl_planes_y_programas_ambitosub where subambito_id = 9) nombre

          FROM tbl_obliglab_repo a INNER JOIN dim_tiempo b
          ON a.fecha = b.fecha
          INNER JOIN tbl_contrato c ON a.contrato_id = c.contrato_id
          WHERE CASE WHEN  '.$mes.'=0 THEN Anio = '.$year.' ELSE
          mes = '.$mes.' AND Anio = '.$year.' END
          and c.contrato_id ='.$id.'

          UNION ALL

          SELECT  COUNT(DISTINCT a.contrato_id), (select ambito_nombre from tbl_planes_y_programas_ambitosub where subambito_id = 10) nombre
          FROM tbl_contratistacondicionfin a
          INNER JOIN dim_tiempo b
          ON a.finCont_fecha = b.fecha
          INNER JOIN tbl_contrato c ON a.contrato_id = c.contrato_id
          WHERE CASE WHEN  '.$mes.'=0 THEN Anio = '.$year.' ELSE
          mes = '.$mes.' AND Anio = '.$year.' END
          
          and c.contrato_id ='.$id.'

          UNION ALL

          SELECT  CAST(AVG((PERC_LAB)) AS DECIMAL(4,1)) puntaje, (select ambito_nombre from tbl_planes_y_programas_ambitosub where subambito_id = 11) nombre
          FROM tbl_fiscalización a INNER JOIN dim_tiempo b
          ON a.fecha = b.fecha
          INNER JOIN tbl_contrato c ON a.contrato_id = c.contrato_id
          WHERE CASE WHEN  '.$mes.'=0 THEN Anio = '.$year.' ELSE
          mes = '.$mes.' AND Anio = '.$year.' END
          
          and c.contrato_id ='.$id;
  
   

    if($tip==1)
    {
      $sqlgr = $sql_panel;
    }
    else if($tip == 2)
    {
      $sqlgr = $sql_neg;

    }
    else if($tip == 3){
      $sqlgr = $sql_fin;

    }
     else if($tip == 4){
      $sqlgr = $sql_seg;

    }
     else if($tip == 5){
      $sqlgr = $sql_ent;

    }

                    $rawgraf = array();
                    $i=0;
                    
                    $row1 = $conn2->query($sqlgr);
                    $results = $row1->fetchAll(PDO::FETCH_ASSOC);       
                       
                    echo json_encode($results);
  }                 
else
{ ?>
 <form method="post">
   <div style="display: inline-block;width:70px; height:30px;">
      <b>Año</b>
                     <select name="sl_year" id="sl_year" onchange="contratos(this.value,1)">

<?php
                   $sqlyear = ' select distinct Anio from dim_tiempo 
                              where anio between year(current_Date) -5
                              and YEAR(CASE WHEN DAY(CURRENT_DATE) >= 20 THEN DATE_ADD(DATE_ADD(CURRENT_DATE, INTERVAL -1 MONTH), INTERVAL -(DAY(CURRENT_DATE)-1) DAY)  ELSE DATE_ADD(DATE_ADD(CURRENT_DATE, INTERVAL -2 MONTH), INTERVAL -(DAY(CURRENT_DATE)-1) DAY) END)
                              order by anio desc
                            ';
                     $i=0;
                    
                    foreach ($conn2->query($sqlyear) as $row1) {
                           if($row1["Anio"]==$year){
                              echo "<option  value='".$row1["Anio"]."' selected>".$row1["Anio"]."</option>"; 
                           }
                           else
                              echo "<option  value='".$row1["Anio"]."'>".$row1["Anio"]."</option>"; 
                          $i++;
                    }
                    
            ?>

                    </select>

       
       </div>  
       <div style="display: inline-block; width:80px; height:30px;">
            
           <b>Mes</b>
                     <select name="sl_mes" id="sl_mes" onchange="contratos(this.value,2)">

<?php


                   $sqlmes = ' SELECT 0 mes, \'Todos\' NMes3L
                   UNION ALL
                   SELECT DISTINCT mes , NMes3L  FROM dim_tiempo 
                   WHERE fecha_mes <= CASE WHEN DAY(CURRENT_DATE) >= 20 THEN DATE_ADD(DATE_ADD(CURRENT_DATE, INTERVAL -1 MONTH), INTERVAL -(DAY(CURRENT_DATE)-1) DAY)  ELSE DATE_ADD(DATE_ADD(CURRENT_DATE, INTERVAL -2 MONTH), INTERVAL -(DAY(CURRENT_DATE)-1) DAY) END
                   AND  anio = '.$year.' 
                   ORDER BY mes';
                     $i=0;
                    
                    foreach ($conn2->query($sqlmes) as $row1) {
                           if($row1["mes"]==$mes){
                              echo "<option  value='".$row1["mes"]."' selected>".$row1["NMes3L"]."</option>"; 
                           }
                           else
                              echo "<option  value='".$row1["mes"]."'>".$row1["NMes3L"]."</option>"; 
                          $i++;
                    }
                    
            ?>

                   
                    </select>
</div>
 <div style="display: inline-block; width:300px; height:30px;">
            
           <b>Contrato</b>
                     <select name="sl_cont" id="sl_cont" onchange="contratos(this.value,3)">

<?php


                   $sqlmes = ' 
                   SELECT DISTINCT contrato_id , cont_proveedor cont  FROM tbl_contrato 
                   WHERE cont_fechaFin >= current_date
                   oRDER BY contrato_id';
                     $i=0;
                    
                    foreach ($conn2->query($sqlmes) as $row1) {
                           if($row1["contrato_id"]==$id){
                              echo "<option  value='".$row1["contrato_id"]."' selected>".$row1["cont"]."</option>"; 
                           }
                           else
                              echo "<option  value='".$row1["contrato_id"]."'>".$row1["cont"]."</option>"; 
                          $i++;
                    }
                    
            ?>

                   
                    </select>
</div>
 <div style="display: inline-block;width:100px; height:30px;">
                <button type="reset" class="btn-xs btn-default " onclick="contratos('0',6)">Limpiar</button>
          </div>
 
        </form>

<?php
}
                 ?>               
