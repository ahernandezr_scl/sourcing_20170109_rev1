@extends('layouts.app')
@section('content')

<?php
$servername = "localhost";
$username = "root";
$password = "8044302";

#try {
    $conn2 = new PDO("mysql:host=$servername;dbname=andina_koadmin", $username, $password);
    // set the PDO error mode to exception
#    $conn2->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
#   echo "Connected successfully";
#    }
#catch(PDOException $e)
#    {
#    echo "Connection failed: " . $e->getMessage();
#    }
?>


@if(Auth::check() && Auth::user()->group_id == 1)

    <script src="https://code.highcharts.com/highcharts.js"></script>
    <script src="https://code.highcharts.com/modules/exporting.js"></script>
    <script src="http://code.highcharts.com/highcharts-more.js"></script>
                                <tr>
                                    <td> <!--{{ Auth::user()->id }}--></td>
                                    	<!--{{ Auth::user()->id }}-->
                              </tr>
    <div class="embed-container">
	    <!-- -->
        <div class="row m-t">
          <div class="col-lg-12">
            <div class="row">
                <!-- Scatter Plot -->
                <div class="col-lg-12">
                  <div class="sbox-content">
  									<button class='gen_btn'>Generar Archivo CSV</button>
                  </div>
                </div>
                <!-- /Scatter Plot -->
              </div>
            </div>
          </div>
          <div class="row m-t">
            <div class="col-lg-12">
              <div class="row">

                <div class="col-xs-6 col-sm-3">
                  <div class="sbox">
                    <div class="pull-center">
                        <!-- <h4 class="panel-title">Cantidad de Contratos</h4> -->
                        <div class="sbox-title" align="center">
                            <!-- <span class="label label-success pull-right">Mes anterior</span> -->
                            <h4>Total Personas</h4>
                        </div>
                        <div class="sbox-content" align="center">
                          <h1>{!! $personas[0]->total !!}</h1>
                        </div>
                        <!-- <h5 class="text-success">2.00% increased</h5>-->
                    </div>
                  </div>
                </div>
                <div class="col-xs-6 col-sm-3">
                  <div class="sbox">
                    <div class="pull-center">
                      <!-- <h4 class="panel-title">Cantidad de Contratos</h4> -->
                      <div class="sbox-title" align="center">
                          <!-- <span class="label label-success pull-right">Mes anterior</span> -->
                          <h4>Personas con Contratos</h4>
                      </div>
                      <div class="sbox-content" align="center">
                        <h1>{!! $personas[0]->personasContrato !!}</h1>
                      </div>
                      <!-- <h5 class="text-success">2.00% increased</h5>-->
                    </div>
                  </div>
                </div>
                <div class="col-xs-6 col-sm-3">
                  <div class="sbox">
                    <div class="pull-center">
                      <!-- <h4 class="panel-title">Cantidad de Contratos</h4> -->
                      <div class="sbox-title" align="center">
                          <!-- <span class="label label-success pull-right">Mes anterior</span> -->
                          <h4>Personas con Acceso</h4>
                      </div>
                      <div class="sbox-content" align="center">
                        <h1>{!! $personas[0]->personasAcceso !!}</h1>
                      </div>
                      <!-- <h5 class="text-success">2.00% increased</h5>-->
                    </div>
                  </div>
                </div>
                <div class="col-xs-6 col-sm-3">
                  <div class="sbox">
                    <div class="pull-center">
                    <!-- <h4 class="panel-title">Cantidad de Contratos</h4> -->
                      <div class="sbox-title" align="center">
                          <!-- <span class="label label-success pull-right">Mes anterior</span> -->
                          <h4>Personas con Anotaciones</h4>
                      </div>
                      <div class="sbox-content" align="center">
                        <h1>{!! $personas[0]->personasAnotacion !!}</h1>
                      </div>
                      <!-- <h5 class="text-success">2.00% increased</h5>-->
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>

        <div class="row m-t">
          <div class="col-lg-12">
            <div class="row">

              <!-- Tabla Contratos -->
              <div class="col-lg-4">
                <div class="sbox">
                  <div class="sbox-title">
                    <span class="label label-success pull-right">Mes anterior</span>
                    <h4>Distribución Etaria</h4>
                  </div>
                  <div class="sbox-content">
                    <div style="top: 10px;  left: 0;   width: 100%;" height="300px;background: white;">
                        <div id="columnContainer1" style="height: 185px; min-width: 310px; max-width: 600px; margin: 0 auto"></div>
                    </div>
                    <div class="stat-percent font-bold text-success">
                        <?php
                                $sql = 'SELECT Anio, NMes3L,
                                            YEAR(b.financieroand_fecha) AS AGNO
                                        ,   MONTH(b.financieroand_fecha) AS MES
                                        ,   REPLACE(REPLACE(REPLACE(FORMAT(SUM(b.financieroand_mtoreal), 0), ".", "@"), ",", "."), "@", ",") AS MtoPago FROM tbl_contrato AS a
                                        INNER JOIN tbl_financiero_and b ON a.contrato_id = b.contrato_id
                                        INNER JOIN dim_tiempo t ON b.financieroand_fecha = t.fecha
                                        WHERE
                                            b.financieroand_fecha = DATE_ADD(DATE_ADD(CURRENT_DATE, INTERVAL -2 MONTH), INTERVAL -(DAY(CURRENT_DATE)-1) DAY)
                                         ';
                            foreach ($conn2->query($sql) as $row) {
                                print $row['NMes3L']."-".$row['Anio']."\n";
                                }
                        ?><!--<i class="fa fa-level-up"></i>-->
                      </div>
                      <small>PERIODO</small>
                    </div>
                  </div><!-- /sbox -->
                </div><!-- </div class="col-lg-4"> -->
                <!-- Scatter Plot -->
                <div class="col-lg-4">
                  <div class="sbox">
                    <div class="sbox-title">
                      <span class="label label-success pull-right">Mes anterior</span>
                      <h4><center>Distribución de Géneros</h4>
                    </div>
                    <div class="sbox-content">
                      <div style="top: 10px;  left: 0;   width: 100%;" height="300px;background: white;">
                          <div id="pieContainer1" style="height: 185px; min-width: 310px; max-width: 600px; margin: 0 auto"></div>
                      </div>
                      <div class="stat-percent font-bold text-success">
                      </div>
                      <small>PERIODO</small>
                    </div>
                  </div>
                </div>

                <div class="col-lg-4">
                  <div class="sbox">
                    <div class="sbox-title">
                      <span class="label label-success pull-right">Mes anterior</span>
                      <h4><center>Distribución de Nacionalidades</h4>
                    </div>
                    <div class="sbox-content">
                      <div style="top: 10px;  left: 0;   width: 100%;" height="300px;background: white;">
                          <div id="columnContainer2" style="height: 185px; min-width: 310px; max-width: 600px; margin: 0 auto"></div>
                      </div>
                      <div class="stat-percent font-bold text-success">
                      </div>
                      <small>PERIODO</small>
                    </div>
                  </div>
                </div>
                <!-- /Scatter Plot -->

              </div>
              <!-- /Tabla Contratos -->
              <div class="row m-t">
                <div class="col-lg-4">
                  <div class="sbox">
                    <div class="sbox-title">
                      <span class="label label-success pull-right">Mes anterior</span>
                      <h4><center>Personas por Segmento</h4>
                    </div>
                    <div class="sbox-content">
                      <div style="top: 10px;  left: 0;   width: 100%;" height="300px;background: white;">
                          <div id="pieContainer2" style="height: 185px; min-width: 310px; max-width: 600px; margin: 0 auto"></div>
                      </div>
                      <div class="stat-percent font-bold text-success">
                      </div>
                      <small>PERIODO</small>
                    </div>
                  </div>
                </div>

                <div class="col-lg-4">
                  <div class="sbox">
                    <div class="sbox-title">
                      <span class="label label-success pull-right">Mes anterior</span>
                      <h4><center>Personas por Área (Gerencia)</h4>
                    </div>
                    <div class="sbox-content">
                      <div style="top: 10px;  left: 0;   width: 100%;" height="300px;background: white;">
                          <div id="pieContainer3" style="height: 185px; min-width: 310px; max-width: 600px; margin: 0 auto"></div>
                      </div>
                      <div class="stat-percent font-bold text-success">
                        <?php
                                $sql = 'SELECT Anio, NMes3L,
                                            YEAR(b.financieroand_fecha) AS AGNO
                                        ,   MONTH(b.financieroand_fecha) AS MES
                                        ,   REPLACE(REPLACE(REPLACE(FORMAT(SUM(b.financieroand_mtoreal), 0), ".", "@"), ",", "."), "@", ",") AS MtoPago FROM tbl_contrato AS a
                                        INNER JOIN tbl_financiero_and b ON a.contrato_id = b.contrato_id
                                        INNER JOIN dim_tiempo t ON b.financieroand_fecha = t.fecha
                                        WHERE
                                            b.financieroand_fecha = DATE_ADD(DATE_ADD(CURRENT_DATE, INTERVAL -2 MONTH), INTERVAL -(DAY(CURRENT_DATE)-1) DAY)
                                         ';
                            foreach ($conn2->query($sql) as $row) {
                                print $row['NMes3L']."-".$row['Anio']."\n";
                                }
                        ?><!--<i class="fa fa-level-up"></i>-->
                      </div>
                      <small>PERIODO</small>
                    </div>
                  </div>
                </div> <!--</div class="col-lg-12"> -->

                <div class="col-lg-4">
                  <div class="sbox">
                    <div class="sbox-title">
                      <span class="label label-success pull-right">Mes anterior</span>
                      <h4><center>Personas por Centro</h4>
                    </div>
                    <div class="sbox-content">
                      <div style="top: 10px;  left: 0;   width: 100%;" height="300px;background: white;">
                          <div id="columnContainer3" style="height: 185px; min-width: 310px; max-width: 600px; margin: 0 auto"></div>
                      </div>
                      <div class="stat-percent font-bold text-success">
                        <?php
                                $sql = 'SELECT Anio, NMes3L,
                                            YEAR(b.financieroand_fecha) AS AGNO
                                        ,   MONTH(b.financieroand_fecha) AS MES
                                        ,   REPLACE(REPLACE(REPLACE(FORMAT(SUM(b.financieroand_mtoreal), 0), ".", "@"), ",", "."), "@", ",") AS MtoPago FROM tbl_contrato AS a
                                        INNER JOIN tbl_financiero_and b ON a.contrato_id = b.contrato_id
                                        INNER JOIN dim_tiempo t ON b.financieroand_fecha = t.fecha
                                        WHERE
                                            b.financieroand_fecha = DATE_ADD(DATE_ADD(CURRENT_DATE, INTERVAL -2 MONTH), INTERVAL -(DAY(CURRENT_DATE)-1) DAY)
                                         ';
                            foreach ($conn2->query($sql) as $row) {
                                print $row['NMes3L']."-".$row['Anio']."\n";
                                }
                        ?><!--<i class="fa fa-level-up"></i>-->
                      </div>
                      <small>PERIODO</small>
                    </div>
                  </div>
                </div> <!--</div class="col-lg-12"> -->
              </div><!-- </div class="row-mt"> -->
            </div>
          </div>



<script type="text/javascript">
$(document).ready(function(){
    $('button').click(function(){
        var data = {!! $data !!};
        if(data == '')
            return;

        JSONToCSVConvertor(data, "", true);
    });
});

function JSONToCSVConvertor(JSONData, ReportTitle, ShowLabel) {
    //If JSONData is not an object then JSON.parse will parse the JSON string in an Object
    var arrData = typeof JSONData != 'object' ? JSON.parse(JSONData) : JSONData;

    var CSV = '';
    //Set Report title in first row or line

    CSV += ReportTitle + '\r\n\n';

    //This condition will generate the Label/Header
    if (ShowLabel) {
        var row = "";

        //This loop will extract the label from 1st index of on array
        for (var index in arrData[0]) {

            //Now convert each value to string and comma-seprated
            row += index + ',';
        }

        row = row.slice(0, -1);

        //append Label row with line break
        CSV += row + '\r\n';
    }

    //1st loop is to extract each row
    for (var i = 0; i < arrData.length; i++) {
        var row = "";

        //2nd loop will extract each column and convert it in string comma-seprated
        for (var index in arrData[i]) {
            row += '"' + arrData[i][index] + '",';
        }

        row.slice(0, row.length - 1);

        //add a line break after each row
        CSV += row + '\r\n';
    }

    if (CSV == '') {
        alert("Invalid data");
        return;
    }

    //Generate a file name
    var fileName = "MyReport_";
    //this will remove the blank-spaces from the title and replace it with an underscore
    fileName += ReportTitle.replace(/ /g,"_");

    //Initialize file format you want csv or xls
    var uri = 'data:text/csv;charset=utf-8,' + escape(CSV);

    // Now the little tricky part.
    // you can use either>> window.open(uri);
    // but this will not work in some browsers
    // or you will not get the correct file extension

    //this trick will generate a temp <a /> tag
    var link = document.createElement("a");
    link.href = uri;

    //set the visibility hidden so it will not effect on your web-layout
    link.style = "visibility:hidden";
    link.download = fileName + ".csv";

    //this part will append the anchor tag and remove it after automatic click
    document.body.appendChild(link);
    link.click();
    document.body.removeChild(link);
}

$(function () {
    Highcharts.chart('columnContainer1', {
        chart: {
            type: 'column'
        },
        title: {
            text: false
        },
        credits: {
           enabled: false
        },
        legend: {
            enabled: false
        },
        xAxis: {
            categories: [
                '0-18',
                '19-30',
                '31-40',
                '41-50',
                '51-60',,
                '60+'
            ],
            crosshair: true
        },
        yAxis: {
            min: 0,
            title: {
                text: false
            }
        },
        tooltip: {
            headerFormat: '<span style="font-size:10px">{point.key}</span><table>',
            pointFormat: '<tr><td style="color:{series.color};padding:0">{series.name}: </td>' +
                '<td style="padding:0"><b>{point.y}</b></td></tr>',
            footerFormat: '</table>',
            shared: true,
            useHTML: true
        },
        plotOptions: {
            column: {
                pointPadding: 0.2,
                borderWidth: 0
            },
            series:{
              dataLabels: {
                  enabled: true
              },
            }
        },
        series: [{
            name: 'Cantidad Empresas',
            data: [{!! $personas[0]->edad0 !!},
                    {!! $personas[0]->edad1 !!},
                    {!! $personas[0]->edad2 !!},
                    {!! $personas[0]->edad3 !!},
                    {!! $personas[0]->edad4 !!},
                    {!! $personas[0]->edad5 !!}]
        }]
    });
});

$(function () {
    Highcharts.chart('columnContainer2', {
        chart: {
            type: 'column'
        },
        title: {
            text: false
        },
        credits: {
           enabled: false
        },
        legend: {
            enabled: false
        },
        xAxis: {
            categories: [
                'I',
                'II',
                'III',
                'IV',
                'V',,
                'VI',
                'VII',
                'VIII',
                'IX',
                'X',
                'XI',
                'XII',
                'RM',
                'XIV',
                'XV'
            ],
            crosshair: true
        },
        yAxis: {
            min: 0,
            title: {
                text: false
            }
        },
        tooltip: {
            headerFormat: '<span style="font-size:10px">{point.key}</span><table>',
            pointFormat: '<tr><td style="color:{series.color};padding:0">{series.name}: </td>' +
                '<td style="padding:0"><b>{point.y}</b></td></tr>',
            footerFormat: '</table>',
            shared: true,
            useHTML: true
        },
        plotOptions: {
            column: {
                pointPadding: 0.2,
                borderWidth: 0
            },
            series:{
              dataLabels: {
                  enabled: true
              },
            }
        },
        series: [{
            name: 'Cantidad Empresas',
            data: [1,2,3,4,5,6,7,8,9,0,1,2,3,4,5]
        }]
    });
});

$(function () {
    Highcharts.chart('columnContainer3', {
        chart: {
            type: 'column'
        },
        title: {
            text: false
        },
        credits: {
           enabled: false
        },
        legend: {
            enabled: false
        },
        xAxis: {
            categories: [
                'I',
                'II',
                'III',
                'IV',
                'V',,
                'VI',
                'VII',
                'VIII',
                'IX',
                'X',
                'XI',
                'XII',
                'RM',
                'XIV',
                'XV'
            ],
            crosshair: true
        },
        yAxis: {
            min: 0,
            title: {
                text: false
            }
        },
        tooltip: {
            headerFormat: '<span style="font-size:10px">{point.key}</span><table>',
            pointFormat: '<tr><td style="color:{series.color};padding:0">{series.name}: </td>' +
                '<td style="padding:0"><b>{point.y}</b></td></tr>',
            footerFormat: '</table>',
            shared: true,
            useHTML: true
        },
        plotOptions: {
            column: {
                pointPadding: 0.2,
                borderWidth: 0
            },
            series:{
              dataLabels: {
                  enabled: true
              },
            }
        },
        series: [{
            name: 'Cantidad Empresas',
            data: [1,2,3,4,5,6,7,8,9,0,1,2,3,4,5]
        }]
    });
});

$(function () {
    Highcharts.chart('pieContainer1', {
        chart: {
            plotBackgroundColor: null,
            plotBorderWidth: null,
            plotShadow: false,
            type: 'pie'
        },
        title: false,
        tooltip: {
            pointFormat: '{series.name}: <b>{point.y:.1f}</b>'
        },
        legend: {
          align: 'left',
          layout: 'vertical',
          verticalAlign: 'top',
          x: 0,
          y: 0
        },
        credits: false,
        plotOptions: {
            pie: {
                allowPointSelect: true,
                cursor: 'pointer',
                dataLabels: {
                    enabled: true,
                    format: '<b>{point.name}</b>: {point.percentage:.1f} %',
                    style: {
                        color: (Highcharts.theme && Highcharts.theme.contrastTextColor) || 'black'
                    }
                }
            }
        },
        series: [{
            name: 'Género',
            colorByPoint: true,
            showInLegend: true,
            data: [{
                name: 'Hombre',
                y: {!! $personas[0]->personasM !!}
            }, {
                name: 'Mujer',
                y: {!! $personas[0]->personasF !!}
            }],

        }]
    });
});

$(function () {
    Highcharts.chart('pieContainer2', {
        chart: {
            plotBackgroundColor: null,
            plotBorderWidth: null,
            plotShadow: false,
            type: 'pie'
        },
        title: false,
        tooltip: {
            pointFormat: '{series.name}: <b>{point.y:.1f}</b>'
        },
        legend: {
          align: 'left',
          layout: 'vertical',
          verticalAlign: 'top',
          x: 0,
          y: 0
        },
        credits: false,
        plotOptions: {
            pie: {
                allowPointSelect: true,
                cursor: 'pointer',
                dataLabels: {
                    enabled: true,
                    format: '<b>{point.name}</b>: {point.percentage:.1f} %',
                    style: {
                        color: (Highcharts.theme && Highcharts.theme.contrastTextColor) || 'black'
                    }
                }
            }
        },
        series: [{
            name: 'Solicitudes',
            colorByPoint: true,
            showInLegend: true,
            data: [{
                name: 'Por Cargar',
                y: 1
            }, {
                name: 'Por Aprobar',
                y: 2
            }],

        }]
    });
});

$(function () {
    Highcharts.chart('pieContainer3', {
        chart: {
            plotBackgroundColor: null,
            plotBorderWidth: null,
            plotShadow: false,
            type: 'pie'
        },
        title: false,
        tooltip: {
            pointFormat: '{series.name}: <b>{point.y:.1f}</b>'
        },
        legend: {
          align: 'left',
          layout: 'vertical',
          verticalAlign: 'top',
          x: 0,
          y: 0
        },
        credits: false,
        plotOptions: {
            pie: {
                allowPointSelect: true,
                cursor: 'pointer',
                dataLabels: {
                    enabled: true,
                    format: '<b>{point.name}</b>: {point.percentage:.1f} %',
                    style: {
                        color: (Highcharts.theme && Highcharts.theme.contrastTextColor) || 'black'
                    }
                }
            }
        },
        series: [{
            name: 'Género',
            colorByPoint: true,
            showInLegend: true,
            data: [{
                name: 'Hombre',
                y: {!! $personas[0]->personasM !!}
            }, {
                name: 'Mujer',
                y: {!! $personas[0]->personasF !!}
            }],

        }]
    });
});
 </script>

    @endif

 @stop
