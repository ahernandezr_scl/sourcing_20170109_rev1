

		 {!! Form::open(array('url'=>'accesoareas/savepublic', 'class'=>'form-horizontal','files' => true , 'parsley-validate'=>'','novalidate'=>' ')) !!}

	@if(Session::has('messagetext'))
	  
		   {!! Session::get('messagetext') !!}
	   
	@endif
	<ul class="parsley-error-list">
		@foreach($errors->all() as $error)
			<li>{{ $error }}</li>
		@endforeach
	</ul>		


<div class="col-md-12">
						<fieldset><legend> Acceso Areas</legend>
				{!! Form::hidden('IdAccesoArea', $row['IdAccesoArea']) !!}					
									  <div class="form-group  " >
										<label for="Area Trabajo" class=" control-label col-md-4 text-left"> Area Trabajo </label>
										<div class="col-md-6">
										  <select name='IdAreaTrabajo' rows='5' id='IdAreaTrabajo' class='select2 '   ></select> 
										 </div> 
										 <div class="col-md-2">
										 	
										 </div>
									  </div> 					
									  <div class="form-group  " >
										<label for="Acceso" class=" control-label col-md-4 text-left"> Acceso </label>
										<div class="col-md-6">
										  <select name='IdAcceso' rows='5' id='IdAcceso' class='select2 '   ></select> 
										 </div> 
										 <div class="col-md-2">
										 	
										 </div>
									  </div> 					
									  <div class="form-group  " >
										<label for="Estatus" class=" control-label col-md-4 text-left"> Estatus </label>
										<div class="col-md-6">
										  
					<?php $IdEstatus = explode(',',$row['IdEstatus']);
					$IdEstatus_opt = array( '' => 'Seleccione Valor' ,  '1' => 'Activo' ,  '2' => 'Suspendido' , ); ?>
					<select name='IdEstatus' rows='5'   class='select2 '  > 
						<?php 
						foreach($IdEstatus_opt as $key=>$val)
						{
							echo "<option  value ='$key' ".($row['IdEstatus'] == $key ? " selected='selected' " : '' ).">$val</option>"; 						
						}						
						?></select> 
										 </div> 
										 <div class="col-md-2">
										 	
										 </div>
									  </div> 					
									  <div class="form-group  " >
										<label for="IdCentro" class=" control-label col-md-4 text-left"> IdCentro </label>
										<div class="col-md-6">
										  <select name='IdCentro' rows='5' id='IdCentro' class='select2 '   ></select> 
										 </div> 
										 <div class="col-md-2">
										 	
										 </div>
									  </div> </fieldset>
			</div>
			
			

			<div style="clear:both"></div>	
				
					
				  <div class="form-group">
					<label class="col-sm-4 text-right">&nbsp;</label>
					<div class="col-sm-8">	
					<button type="submit" name="apply" class="btn btn-info btn-sm" ><i class="fa  fa-check-circle"></i> {{ Lang::get('core.sb_apply') }}</button>
					<button type="submit" name="submit" class="btn btn-primary btn-sm" ><i class="fa  fa-save "></i> {{ Lang::get('core.sb_save') }}</button>
				  </div>	  
			
		</div> 
		 
		 {!! Form::close() !!}
		 
   <script type="text/javascript">
	$(document).ready(function() { 
		
		
		$("#IdAreaTrabajo").jCombo("{!! url('accesoareas/comboselect?filter=tbl_area_de_trabajo:IdAreaTrabajo:Descripcion') !!}&parent=IdCentro:",
		{  parent: '#IdCentro', selected_value : '{{ $row["IdAreaTrabajo"] }}' });
		
		$("#IdAcceso").jCombo("{!! url('accesoareas/comboselect?filter=tbl_accesos:IdAcceso:IdAcceso') !!}",
		{  selected_value : '{{ $row["IdAcceso"] }}' });
		
		$("#IdCentro").jCombo("{!! url('accesoareas/comboselect?filter=tbl_centro:IdCentro:Descripcion') !!}",
		{  selected_value : '{{ $row["IdCentro"] }}' });
		 

		$('.removeCurrentFiles').on('click',function(){
			var removeUrl = $(this).attr('href');
			$.get(removeUrl,function(response){});
			$(this).parent('div').empty();	
			return false;
		});		
		
	});
	</script>		 
