
@if($setting['form-method'] =='native')
	<div class="sbox">
		<div class="sbox-title">  
			<h4> <i class="fa fa-table"></i> <?php echo $pageTitle ;?> <small>{{ $pageNote }}</small>
				<a href="javascript:void(0)" class="collapse-close pull-right btn btn-xs btn-danger" onclick="ajaxViewClose('#{{ $pageModule }}')"><i class="fa fa fa-times"></i></a>
			</h4>
	</div>

	<div class="sbox-content"> 
@endif	
			{!! Form::open(array('url'=>'accesoareas/save/'.SiteHelpers::encryptID($row['IdAccesoArea']), 'class'=>'form-horizontal','files' => true , 'parsley-validate'=>'','novalidate'=>' ','id'=> 'accesoareasFormAjax')) !!}
			<div class="col-md-12">
						<fieldset><legend> Acceso Areas</legend>
				{!! Form::hidden('IdAccesoArea', $row['IdAccesoArea']) !!}					
									  <div class="form-group  " >
										<label for="Area Trabajo" class=" control-label col-md-4 text-left"> Area Trabajo </label>
										<div class="col-md-6">
										  <select name='IdAreaTrabajo' rows='5' id='IdAreaTrabajo' class='select2 '   ></select> 
										 </div> 
										 <div class="col-md-2">
										 	
										 </div>
									  </div> 					
									  <div class="form-group  " >
										<label for="Acceso" class=" control-label col-md-4 text-left"> Acceso </label>
										<div class="col-md-6">
										  <select name='IdAcceso' rows='5' id='IdAcceso' class='select2 '   ></select> 
										 </div> 
										 <div class="col-md-2">
										 	
										 </div>
									  </div> 					
									  <div class="form-group  " >
										<label for="Estatus" class=" control-label col-md-4 text-left"> Estatus </label>
										<div class="col-md-6">
										  
					<?php $IdEstatus = explode(',',$row['IdEstatus']);
					$IdEstatus_opt = array( '' => 'Seleccione Valor' ,  '1' => 'Activo' ,  '2' => 'Suspendido' , ); ?>
					<select name='IdEstatus' rows='5'   class='select2 '  > 
						<?php 
						foreach($IdEstatus_opt as $key=>$val)
						{
							echo "<option  value ='$key' ".($row['IdEstatus'] == $key ? " selected='selected' " : '' ).">$val</option>"; 						
						}						
						?></select> 
										 </div> 
										 <div class="col-md-2">
										 	
										 </div>
									  </div> 					
									  <div class="form-group  " >
										<label for="IdCentro" class=" control-label col-md-4 text-left"> IdCentro </label>
										<div class="col-md-6">
										  <select name='IdCentro' rows='5' id='IdCentro' class='select2 '   ></select> 
										 </div> 
										 <div class="col-md-2">
										 	
										 </div>
									  </div> </fieldset>
			</div>
			
												
								
						
			<div style="clear:both"></div>	
							
			<div class="form-group">
				<label class="col-sm-4 text-right">&nbsp;</label>
				<div class="col-sm-8">	
					<button type="submit" class="btn btn-primary btn-sm "><i class="icon-checkmark-circle2"></i>  {{ Lang::get('core.sb_save') }} </button>
					<button type="button" onclick="ajaxViewClose('#{{ $pageModule }}')" class="btn btn-success btn-sm"><i class="icon-cancel-circle2 "></i>  {{ Lang::get('core.sb_cancel') }} </button>
				</div>			
			</div> 		 
			{!! Form::close() !!}


@if($setting['form-method'] =='native')
	</div>	
</div>	
@endif	

	
</div>	
			 
<script type="text/javascript">
$(document).ready(function() { 
	
		$("#IdAreaTrabajo").jCombo("{!! url('accesoareas/comboselect?filter=tbl_area_de_trabajo:IdAreaTrabajo:Descripcion') !!}&parent=IdCentro:",
		{  parent: '#IdCentro', selected_value : '{{ $row["IdAreaTrabajo"] }}' });
		
		$("#IdAcceso").jCombo("{!! url('accesoareas/comboselect?filter=tbl_accesos:IdAcceso:IdAcceso') !!}",
		{  selected_value : '{{ $row["IdAcceso"] }}' });
		
		$("#IdCentro").jCombo("{!! url('accesoareas/comboselect?filter=tbl_centro:IdCentro:Descripcion') !!}",
		{  selected_value : '{{ $row["IdCentro"] }}' });
		 
	
	$('.editor').summernote();
	$('.previewImage').fancybox();	
	$('.tips').tooltip();	
	$(".select2").select2({ width:"98%"});	
	$('.date').datepicker({format:'yyyy-mm-dd',autoClose:true})
	$('.datetime').datetimepicker({format: 'yyyy-mm-dd hh:ii:ss'}); 
	$('input[type="checkbox"],input[type="radio"]').iCheck({
		checkboxClass: 'icheckbox_square-red',
		radioClass: 'iradio_square-red',
	});			
		$('.removeMultiFiles').on('click',function(){
			var removeUrl = '{{ url("accesoareas/removefiles?file=")}}'+$(this).attr('url');
			$(this).parent().remove();
			$.get(removeUrl,function(response){});
			$(this).parent('div').empty();	
			return false;
		});
				
	var form = $('#accesoareasFormAjax'); 
	form.parsley();
	form.submit(function(){
		
		if(form.parsley('isValid') == true){			
			var options = { 
				dataType:      'json', 
				beforeSubmit :  showRequest,
				success:       showResponse  
			}  
			$(this).ajaxSubmit(options); 
			return false;
						
		} else {
			return false;
		}		
	
	});

});

function showRequest()
{
	$('.ajaxLoading').show();		
}  
function showResponse(data)  {		
	
	if(data.status == 'success')
	{
		ajaxViewClose('#{{ $pageModule }}');
		ajaxFilter('#{{ $pageModule }}','{{ $pageUrl }}/data');
		notyMessage(data.message);	
		$('#sximo-modal').modal('hide');	
	} else {
		notyMessageError(data.message);	
		$('.ajaxLoading').hide();
		return false;
	}	
}			 

</script>		 