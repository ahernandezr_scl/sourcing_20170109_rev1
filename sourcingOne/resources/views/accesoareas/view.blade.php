@if($setting['view-method'] =='native')
<div class="sbox">
	<div class="sbox-title">  
		<h4> <i class="fa fa-table"></i> <?php echo $pageTitle ;?> <small>{{ $pageNote }}</small>
			<a href="javascript:void(0)" class="collapse-close pull-right btn btn-xs btn-danger" onclick="ajaxViewClose('#{{ $pageModule }}')">
			<i class="fa fa fa-times"></i></a>
		</h4>
	 </div>

	<div class="sbox-content"> 
@endif	

		<table class="table table-striped table-bordered" >
			<tbody>	
				
					<tr>
						<td width='30%' class='label-view text-right'>{{ SiteHelpers::activeLang('Center', (isset($fields['IdCentro']['language'])? $fields['IdCentro']['language'] : array())) }}</td>
						<td>{{ $row->IdCentro}} </td>
						
					</tr>
				
					<tr>
						<td width='30%' class='label-view text-right'>{{ SiteHelpers::activeLang('Work Area', (isset($fields['IdAreaTrabajo']['language'])? $fields['IdAreaTrabajo']['language'] : array())) }}</td>
						<td>{{ $row->IdAreaTrabajo}} </td>
						
					</tr>
				
					<tr>
						<td width='30%' class='label-view text-right'>{{ SiteHelpers::activeLang('IdAcceso', (isset($fields['IdAcceso']['language'])? $fields['IdAcceso']['language'] : array())) }}</td>
						<td>{{ $row->IdAcceso}} </td>
						
					</tr>
				
					<tr>
						<td width='30%' class='label-view text-right'>{{ SiteHelpers::activeLang('Estatus', (isset($fields['IdEstatus']['language'])? $fields['IdEstatus']['language'] : array())) }}</td>
						<td>{{ $row->IdEstatus}} </td>
						
					</tr>
				
			</tbody>	
		</table>  
			
		 	

@if($setting['form-method'] =='native')
	</div>	
</div>	
@endif	

<script>
$(document).ready(function(){

});
</script>	