<div class="row m-b">
	<div class="col-md-6">
			@if($access['is_edit'] ==1)
			<a href="javascript://ajax" class="btn btn-sm btn-white" onclick="ajaxApprove('#{{ $pageModule }}','{{ $pageUrl }}',5)"><i class="fa fa-check"></i> Aprobar </a>
			<a href="javascript://ajax" class="btn btn-sm btn-white" onclick="ajaxApprove('#{{ $pageModule }}','{{ $pageUrl }}',4)"><i class="fa fa-clock-o"></i> Temporal </a>
			<a href="javascript://ajax"  onclick="$('#motivorechazoModal').modal('show'); "   class="btn btn-sm btn-white" ><i class="fa fa-ban"></i> Rechazar </a>
			<select name="Estatus" id="Estatus"   class="select-liquid" onchange="filterEstatus(this.value)">
				<option <?=($param['Estatus']==7?'selected':'')?> value="7"> Por asociar</option>
				<option <?=($param['Estatus']==1?'selected':'')?> value="1"> Por cargar</option>
				<option <?=($param['Estatus']==2?'selected':'')?> value="2" > Por aprobar</option>
				<option <?=($param['Estatus']==3?'selected':'')?> value="3"> No aprobado</option>
				<option <?=($param['Estatus']==4?'selected':'')?> value="4"> Temporal</option>
				<option <?=($param['Estatus']==5?'selected':'')?> value="5"> Aprobado</option>
				<option <?=($param['Estatus']==8?'selected':'')?> value="8"> Vencidos</option>
				<option <?=($param['Estatus']==6?'selected':'')?> value="6"> Todos</option>
			</select>
			@endif
	</div>
	<div class="col-md-6 ">
		@if($access['is_excel'] ==1)
		<div class="pull-right">
			<a href="{{ URL::to( $pageModule .'/export/excel?return='.$return) }}" class="btn btn-sm btn-white"><i class="icon-file-download"></i> Excel</a>
			<a href="{{ URL::to( $pageModule .'/export/word?return='.$return) }}" class="btn btn-sm btn-white"><i class="icon-file-download"></i> Word </a>
			<a href="{{ URL::to( $pageModule .'/export/csv?return='.$return) }}" class="btn btn-sm btn-white"><i class="icon-file-download"></i> CSV </a>
			<a href="{{ URL::to( $pageModule .'/export/print?return='.$return) }}" class="btn btn-sm btn-white" onclick="ajaxPopupStatic(this.href); return false;" ><i class="icon-print2"></i> Print</a>
		</div>
		@endif
	</div>
</div>
