<!-- Modal -->
<div id="myModal" class="modal fade" role="dialog">
  <div class="modal-dialog modal-lg">
    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Detalle</h4>
      </div>
      <div class="modal-body">
        <div id="ModalViewPDF" ></div>
        <div id="AdditionalData" ></div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
      </div>
    </div>

  </div>
</div>
<!-- Modal -->
<div id="motivorechazoModal" class="modal fade" role="dialog">
  <div class="modal-dialog">
    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Motivo rechazo</h4>
      </div>
      <div class="modal-body">
        <div class="form-group  ">
			<label for="MotivoRechazo" class=" control-label col-md-4 text-left"> Motivo del rechazo </label>
			<div class="col-md-6">
			    {!! Form::text('MotivoRechazo', ' ',array('class'=>'form-control','id'=>'MotivoRechazo' ,'placeholder'=>'',   )) !!}
			</div>
			<div class="col-md-2">

			</div>
		</div>
      </div>
      <div class="modal-footer">
      <button type="button" id="GuardarV" class="btn btn-default" onclick="ajaxApprove('#{{ $pageModule }}','{{ $pageUrl }}',3)">Guardar</button>
      <input type="hidden" name="" id="idEsta">
         <button type="button" id="GuardarI" class="btn btn-default" onclick="ajaxApproveInLine($('#idEsta').val(),3);$('#GuardarI').hide()">Guardar</button>

        <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
      </div>
    </div>

  </div>
</div>
<?php usort($tableGrid, "SiteHelpers::_sort"); ?>
<div class="sbox">
	<div class="sbox-title">
	<h3><i class="fa fa-table"></i> <?php echo $pageTitle ;?> <small>{{ $pageNote }}</small></h3>

		<div class="sbox-tools" >
			<a href="javascript:void(0)" class="btn btn-xs btn-white tips" title="Clear Search" onclick="reloadData('#{{ $pageModule }}','aprobaciones/data?search=')"><i class="fa fa-trash-o"></i> Clear Search </a>
			<a href="javascript:void(0)" class="btn btn-xs btn-white tips" title="Reload Data" onclick="reloadData('#{{ $pageModule }}','aprobaciones/data?return={{ $return }}')"><i class="fa fa-refresh"></i></a>
			@if(Session::get('gid') ==1)
			<a href="{{ url('sximo/module/config/'.$pageModule) }}" class="btn btn-xs btn-white tips" title=" {{ Lang::get('core.btn_config') }}" ><i class="fa fa-cog"></i></a>
			@endif
		</div>
	</div>
	<div class="sbox-content">

	@include( $pageModule.'/toolbar')

	 {!! (isset($search_map) ? $search_map : '') !!}

	 <?php echo Form::open(array('url'=>'aprobaciones/delete/', 'class'=>'form-horizontal' ,'id' =>'SximoTable'  ,'data-parsley-validate'=>'' )) ;?>
<div class="table-responsive">
	@if(count($rowData)>=1)
    <table class="table table-striped  " id="{{ $pageModule }}Table">
        <thead>
			<tr>
				<th width="20"> No </th>
				<th width="30"> <input type="checkbox" class="checkall" /></th>
				@if($setting['view-method']=='expand')<th width="30" style="width: 30px;">  </th> @endif
				<?php foreach ($tableGrid as $t) :
					if($t['view'] =='1'):
						$limited = isset($t['limited']) ? $t['limited'] :'';
						if(SiteHelpers::filterColumn($limited ))
						{
							echo '<th align="'.$t['align'].'" width="'.$t['width'].'">'.\SiteHelpers::activeLang($t['label'],(isset($t['language'])? $t['language'] : array())).'</th>';
						}
					endif;
				endforeach; ?>
				<th width="100" class="text-right"><?php echo Lang::get('core.btn_action') ;?></th>
			  </tr>
        </thead>

        <tbody>
        	@if($access['is_add'] =='1' && $setting['inline']=='true')
			<tr id="form-0" >
				<td> # </td>
				<td> </td>
				@if($setting['view-method']=='expand') <td> </td> @endif
				@foreach ($tableGrid as $t)
					@if($t['view'] =='1')
					<?php $limited = isset($t['limited']) ? $t['limited'] :''; ?>
						@if(SiteHelpers::filterColumn($limited ))
						<td data-form="{{ $t['field'] }}" data-form-type="{{ AjaxHelpers::inlineFormType($t['field'],$tableForm)}}">
							{!! SiteHelpers::transForm($t['field'] , $tableForm) !!}
						</td>
						@endif
					@endif
				@endforeach
				<td width="100" class="text-right">
					<button onclick="saved('form-0')" class="btn btn-primary btn-xs" type="button"><i class="icon-checkmark-circle2"></i></button>
				</td>
			  </tr>
			  @endif

           		<?php foreach ($rowData as $row) :
           			  $id = $row->IdDocumento;
           		?>
                <tr class="editable" id="form-{{ $row->IdDocumento }}">
					<td class="number"> <?php echo ++$i;?>  </td>
					<td ><input type="checkbox" class="ids" name="ids[]" value="<?php echo $row->IdDocumento ;?>" />  </td>
					@if($setting['view-method']=='expand')
					<td><a href="javascript:void(0)" class="expandable" rel="#row-{{ $row->IdDocumento }}" data-url="{{ url('aprobaciones/show/'.$id) }}"><i class="fa fa-plus " ></i></a></td>
					@endif
					 <?php foreach ($tableGrid as $field) :
					 	if($field['view'] =='1') :
							$value = SiteHelpers::formatRows($row->{$field['field']}, $field , $row);
						 	?>
						 	<?php $limited = isset($field['limited']) ? $field['limited'] :''; ?>
						 	@if(SiteHelpers::filterColumn($limited ))
								 <td align="<?php echo $field['align'];?>" data-values="{{ $row->{$field['field']} }}" data-field="{{ $field['field'] }}" data-format="{{ htmlentities($value) }}">
									{!! $value !!}
								 </td>
							@endif
						 <?php endif;
						endforeach;
					  ?>
				 <td data-values="action" data-key="<?php echo $row->IdDocumento ;?>" width="100" class="text-right">
				    @if($access['is_detail']==1)
					<div class=" action dropup" >
					  <a href="#" onclick="ajaxApproveInLine('<?php echo $id; ?>',5); return false;"  class="btn btn-xs btn-white tips" title=" Aprobar "><i class="fa  fa-check"></i></a>
					  <a href="#" onclick="ajaxApproveInLine('<?php echo $id; ?>',4); return false;"  class="btn btn-xs btn-white tips" title=" Temporal "><i class="fa  fa-clock-o"></i></a>
                                      <a href="javascript://ajax"   onclick="valores('<?php echo $id; ?>',3); return false;"   class="btn btn-xs btn-white tips" title=" Rechazar"><i class="fa fa-ban"></i></a>

		            </div>
		            @endif
				</td>
                </tr>
                @if($setting['view-method']=='expand')
                <tr style="display:none" class="expanded" id="row-{{ $row->IdDocumento }}">
                	<td class="number"></td>
                	<td></td>
                	<td></td>
                	<td colspan="{{ $colspan}}" class="data"></td>
                	<td></td>
                </tr>
                @endif
            <?php endforeach;?>

        </tbody>

    </table>
	@else

	<div style="margin:100px 0; text-align:center;">

		<p> No Record Found </p>
	</div>

	@endif

	</div>
	<?php echo Form::close() ;?>
	@include('ajaxfooter')

	</div>
</div>

	@if($setting['inline'] =='true') @include('sximo.module.utility.inlinegrid') @endif
<script>
$(document).ready(function() {
    $('#GuardarI').hide()
	$('.tips').tooltip();
	$('input[type="checkbox"],input[type="radio"]').iCheck({
		checkboxClass: 'icheckbox_square-red',
		radioClass: 'iradio_square-red',
	});
	$('#{{ $pageModule }}Table .checkall').on('ifChecked',function(){
		$('#{{ $pageModule }}Table input[type="checkbox"]').iCheck('check');
	});
	$('#{{ $pageModule }}Table .checkall').on('ifUnchecked',function(){
		$('#{{ $pageModule }}Table input[type="checkbox"]').iCheck('uncheck');
	});

	$('#{{ $pageModule }}Paginate .pagination li a').click(function() {
		var url = $(this).attr('href');
		reloadData('#{{ $pageModule }}',url);
		return false ;
	});

	<?php if($setting['view-method'] =='expand') :
			echo AjaxHelpers::htmlExpandGrid();
		endif;
	 ?>

});

function valores(id,estatus){
$('#motivorechazoModal').modal('show');
$('#GuardarV').hide();
$('#GuardarI').show();
$('#idEsta').val(id)
}

function ajaxApprove( id, url, idestatus)
	{
        var motivo = $("#MotivoRechazo").val()
                        if (idestatus==5)
                            mensaje="Estas seguro de querer aprobar los documentos seleccionados?"
                        if (idestatus==3)
                             mensaje="Estas seguro de querer rechazar los documentos seleccionados?"
		var serialdatas = $( id +'Table :input').serialize();
		var datas = {"ids":serialdatas,"status":idestatus, "result":motivo};
		if(confirm(mensaje)) {
			$.post( url+'/approve' ,datas,function( data ) {
				if(data.status =='success')
				{
					notyMessage(data.message);
					ajaxFilter( id ,'aprobaciones/data' );
                                                                 $('#motivorechazoModal').modal('hide');
				} else {
					notyMessageError(data.message);
				}
			});

		}
	}
	function ajaxApproveInLine(id, idestatus){
             var motivo = $("#MotivoRechazo").val()
		var datas = {ids:id,oneline:true, status:idestatus, "result":motivo};
		$('.ajaxLoading').show();
		$.post( '{{$pageModule}}/approve' ,datas, function( data ) {
			if(data.status == 'success')
			{
				ajaxFilter('#{{ $pageModule }}','{{ $pageUrl }}/data?Estatus='+6);
				notyMessage(data.message);
                                                    $('#motivorechazoModal').modal('hide');
			} else {
				$('.ajaxLoading').hide();
				notyMessageError(data.message);
				return false;
			}
		},"html");
	}
	function ViewPDF(url,id){
		PDFObject.embed("uploads/documents/"+url, "#ModalViewPDF");
		var lstrAdditionalData = "";
		var datas = {"iddocumento":id};
		$('.ajaxLoading').show();
        $.post( '{{$pageModule}}/infoadicional' ,datas, function( data ) {		
			$("#AdditionalData").html(data);
			$('.ajaxLoading').hide();
		},"html");
		$("#myModal").modal();
	}
</script>
<style>
.table th.right { text-align:right !important;}
.table th.center { text-align:center !important;}
#ModalViewPDF {
	height: 400px;
}
</style>

