<!-- Modal -->
<div id="myModal" class="modal fade" role="dialog">
  <div class="modal-dialog modal-lg">
    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Detalle</h4>
      </div>
      <div class="modal-body">
        <div id="ModalViewPDF" ></div>
        <div id="AdditionalData" ></div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
      </div>
    </div>

  </div>
</div>
<!-- Modal -->
<div id="motivorechazoModal" class="modal fade" role="dialog">
  <div class="modal-dialog">
    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Motivo rechazo</h4>
      </div>
      <div class="modal-body">
        <div class="form-group  ">
			<label for="MotivoRechazo" class=" control-label col-md-4 text-left"> Motivo del rechazo </label>
			<div class="col-md-6">
			    {!! Form::text('MotivoRechazo', ' ',array('class'=>'form-control','id'=>'MotivoRechazo' ,'placeholder'=>'',   )) !!}
			</div>
			<div class="col-md-2">

			</div>
		</div>
      </div>
      <div class="modal-footer">
      <button type="button" id="GuardarV" class="btn btn-default" onclick="ajaxApprove('#{{ $pageModule }}','{{ $pageUrl }}',3)">Guardar</button>
      <input type="hidden" name="" id="idEsta">
         <button type="button" id="GuardarI" class="btn btn-default" onclick="ajaxApproveInLine($('#idEsta').val(),3);$('#GuardarI').hide()">Guardar</button>

        <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
      </div>
    </div>

  </div>
</div>
<div class="sbox">
	<div class="sbox-title">
	<h3><i class="fa fa-table"></i> <?php echo $pageTitle ;?> <small>{{ $pageNote }}</small></h3>

		<div class="sbox-tools" >
			<a href="javascript:void(0)" class="btn btn-xs btn-white tips" title="Clear Search" onclick="reloadData('#{{ $pageModule }}','aprobaciones/data?search=')"><i class="fa fa-trash-o"></i> Clear Search </a>
			<a href="javascript:void(0)" class="btn btn-xs btn-white tips" title="Reload Data" onclick="reloadData('#{{ $pageModule }}','aprobaciones/data?return={{ $return }}')"><i class="fa fa-refresh"></i></a>
			@if(Session::get('gid') ==1)
			<a href="{{ url('sximo/module/config/'.$pageModule) }}" class="btn btn-xs btn-white tips" title=" {{ Lang::get('core.btn_config') }}" ><i class="fa fa-cog"></i></a>
			@endif
		</div>
	</div>
	<div class="sbox-content">

	 {!! (isset($search_map) ? $search_map : '') !!}

	 <?php echo Form::open(array('url'=>'aprobaciones/delete/', 'class'=>'form-horizontal' ,'id' =>'SximoTable'  ,'data-parsley-validate'=>'' )) ;?>
<div class="table-responsive" id="{{ $pageModule }}TableContainer">
    <table class="table  table-striped table-bordered table-hover dataTable  " id="{{ $pageModule }}Table">
        <thead>
			<tr>
				<th width="20"> No </th>
				<th width="30"> <input type="checkbox" class="checkall" /></th>
				@if($setting['view-method']=='expand')<th width="30" style="width: 30px;">  </th> @endif
				<?php foreach ($tableGrid as $t) :
					if($t['view'] =='1'):
						$limited = isset($t['limited']) ? $t['limited'] :'';
						if(SiteHelpers::filterColumn($limited ))
						{
							echo '<th align="'.$t['align'].'" width="'.$t['width'].'">'.\SiteHelpers::activeLang($t['label'],(isset($t['language'])? $t['language'] : array())).'</th>';
						}
					endif;
				endforeach; ?>
				<th width="100" class="text-right"><?php echo Lang::get('core.btn_action') ;?></th>
			  </tr>
        </thead>

        <tbody>
            <!--
        	@if($access['is_add'] =='1' && $setting['inline']=='true')
			<tr id="form-0" >
				<td> # </td>
				<td> </td>
				@if($setting['view-method']=='expand') <td> </td> @endif
				@foreach ($tableGrid as $t)
					@if($t['view'] =='1')
					<?php $limited = isset($t['limited']) ? $t['limited'] :''; ?>
						@if(SiteHelpers::filterColumn($limited ))
						<td data-form="{{ $t['field'] }}" data-form-type="{{ AjaxHelpers::inlineFormType($t['field'],$tableForm)}}">
							{!! SiteHelpers::transForm($t['field'] , $tableForm) !!}
						</td>
						@endif
					@endif
				@endforeach
				<td width="100" class="text-right">
					<button onclick="saved('form-0')" class="btn btn-primary btn-xs" type="button"><i class="icon-checkmark-circle2"></i></button>
				</td>
			  </tr>
			  @endif

           		<?php foreach ($rowData as $row) :
           			  $id = $row->IdDocumento;
           		?>
                <tr class="editable" id="form-{{ $row->IdDocumento }}">
					<td class="number"> <?php echo ++$i;?>  </td>
					<td ><input type="checkbox" class="ids" name="ids[]" value="<?php echo $row->IdDocumento ;?>" />  </td>
					@if($setting['view-method']=='expand')
					<td><a href="javascript:void(0)" class="expandable" rel="#row-{{ $row->IdDocumento }}" data-url="{{ url('aprobaciones/show/'.$id) }}"><i class="fa fa-plus " ></i></a></td>
					@endif
					 <?php foreach ($tableGrid as $field) :
					 	if($field['view'] =='1') :
							$value = SiteHelpers::formatRows($row->{$field['field']}, $field , $row);
						 	?>
						 	<?php $limited = isset($field['limited']) ? $field['limited'] :''; ?>
						 	@if(SiteHelpers::filterColumn($limited ))
								 <td align="<?php echo $field['align'];?>" data-values="{{ $row->{$field['field']} }}" data-field="{{ $field['field'] }}" data-format="{{ htmlentities($value) }}">
									{!! $value !!}
								 </td>
							@endif
						 <?php endif;
						endforeach;
					  ?>
				 <td data-values="action" data-key="<?php echo $row->IdDocumento ;?>" width="100" class="text-right">
				    @if($access['is_detail']==1)
					<div class=" action dropup" >
					  <a href="#" onclick="ajaxApproveInLine('<?php echo $id; ?>',5); return false;"  class="btn btn-xs btn-white tips" title=" Aprobar "><i class="fa  fa-check"></i></a>
					  <a href="#" onclick="ajaxApproveInLine('<?php echo $id; ?>',4); return false;"  class="btn btn-xs btn-white tips" title=" Temporal "><i class="fa  fa-clock-o"></i></a>
                                      <a href="javascript://ajax"   onclick="valores('<?php echo $id; ?>',3); return false;"   class="btn btn-xs btn-white tips" title=" Rechazar"><i class="fa fa-ban"></i></a>

		            </div>
		            @endif
				</td>
                </tr>
                @if($setting['view-method']=='expand')
                <tr style="display:none" class="expanded" id="row-{{ $row->IdDocumento }}">
                	<td class="number"></td>
                	<td></td>
                	<td></td>
                	<td colspan="{{ $colspan}}" class="data"></td>
                	<td></td>
                </tr>
                @endif
            <?php endforeach;?>-->

        </tbody>

    </table>

	</div>
	<?php echo Form::close() ;?>

	</div>
</div>

	@if($setting['inline'] =='true') @include('sximo.module.utility.inlinegrid') @endif
<script>
$(document).ready(function() {
    $('#GuardarI').hide()
	$('.tips').tooltip();
	$('input[type="checkbox"],input[type="radio"]').iCheck({
		checkboxClass: 'icheckbox_square-red',
		radioClass: 'iradio_square-red',
	});
	$('#{{ $pageModule }}Table .checkall').on('ifChecked',function(){
		$('#{{ $pageModule }}Table input[type="checkbox"]').iCheck('check');
	});
	$('#{{ $pageModule }}Table .checkall').on('ifUnchecked',function(){
		$('#{{ $pageModule }}Table input[type="checkbox"]').iCheck('uncheck');
	});

	$('#{{ $pageModule }}Paginate .pagination li a').click(function() {
		var url = $(this).attr('href');
		reloadData('#{{ $pageModule }}',url);
		return false ;
	});

	<?php if($setting['view-method'] =='expand') :
			echo AjaxHelpers::htmlExpandGrid();
		endif;
	 ?>

});

function valores(id,estatus){
$('#motivorechazoModal').modal('show');
$('#GuardarV').hide();
$('#GuardarI').show();
$('#idEsta').val(id)
}

var lintIdEstatus = <?php if (isset($param["Estatus"])) { echo $param["Estatus"]; }else{ echo 0; } ?>;

var lobjDataTable =  $('#{{ $pageModule }}Table').DataTable({
        "dom": 'Tfigtlp',
        "tableTools": {
            "sSwfPath": "sximo/js/plugins/dataTables/swf/copy_csv_xls_pdf.swf"
        },
        "language": {
            "url": "sximo/js/plugins/dataTables/language/Spanish.json"
        },
        "deferRender": true,
        "ajax": { url:'aprobaciones/showlist?IdEstatus='+lintIdEstatus },
        "columns": [
            { "data": "id" },
            { "data": "checkbox" },
            <?php
              foreach ($tableGrid as $field) {
                if($field['view'] =='1') {
                  echo '            {"data": "'.$field['field'].'"}, ';
                }
              }
            ?>
            { "data": "action" }
        ],
        "info": false,
        "columnDefs": [
                        { "orderable": false, "targets": 1 },
                        { "orderable": false, "targets": 14 }
                      ],
        "initComplete": function(settings, json) {

        	var lstrResult;
            <?php if($access['is_add'] ==1 ) { ?>
			lstrResult = '<label style="float:left">';
			lstrResult += '<a href="javascript://ajax" class="btn btn-sm btn-white" onclick="ajaxApprove(\'#{{ $pageModule }}\',\'{{ $pageUrl }}\',5)"><i class="fa fa-check"></i> Aprobar </a>';
			lstrResult += '<a href="javascript://ajax" class="btn btn-sm btn-white" onclick="ajaxApprove(\'#{{ $pageModule }}\',\'{{ $pageUrl }}\',4)"><i class="fa fa-clock-o"></i> Temporal </a>';
			lstrResult += '<a href="javascript://ajax"  onclick="$(\'#motivorechazoModal\').modal(\'show\'); "   class="btn btn-sm btn-white" ><i class="fa fa-ban"></i> Rechazar </a>';
			lstrResult += '<select name="Estatus" id="Estatus"   class="form-control select-liquid" onchange="filterEstatus(this.value)">';
			lstrResult += '<option <?=($param["Estatus"]==7?"selected":"")?> value="7"> Por asociar</option>';
			lstrResult += '<option <?=($param["Estatus"]==1?"selected":"")?> value="1"> Por cargar</option>';
			lstrResult += '<option <?=($param["Estatus"]==2?"selected":"")?> value="2" > Por aprobar</option>';
			lstrResult += '<option <?=($param["Estatus"]==3?"selected":"")?> value="3"> No aprobado</option>';
			lstrResult += '<option <?=($param["Estatus"]==4?"selected":"")?> value="4"> Temporal</option>';
			lstrResult += '<option <?=($param["Estatus"]==5?"selected":"")?> value="5"> Aprobado</option>';
			lstrResult += '<option <?=($param["Estatus"]==8?"selected":"")?> value="8"> Vencido</option>';
			lstrResult += '<option <?=($param["Estatus"]==6?"selected":"")?> value="6"> Todos</option>';
			lstrResult += '</select>';
			lstrResult += '</label>';
			$('.DTTT_container').before(lstrResult);
			<?php } ?>
        }
    });
function filterEstatus(val){
     if (val !==''){
        lintIdEstatus = val;
        reloaddatatable();
    }
}
function reloaddatatable(){
	$('.ajaxLoading').show();
    lobjDataTable.ajax.url('aprobaciones/showlist?IdEstatus='+lintIdEstatus).load(function(){
		$('.ajaxLoading').hide();
    });
}
function ajaxApprove( id, url, idestatus)
	{
        var motivo = $("#MotivoRechazo").val()
                        if (idestatus==5)
                            mensaje="Estas seguro de querer aprobar los documentos seleccionados?"
                        if (idestatus==3)
                             mensaje="Estas seguro de querer rechazar los documentos seleccionados?"
		var serialdatas = $( id +'Table :input').serialize();
		var datas = {"ids":serialdatas,"status":idestatus, "result":motivo};
		if(confirm(mensaje)) {
			$.post( url+'/approve' ,datas,function( data ) {
				console.log("data");
				if(data.status =='success')
				{
					notyMessage(data.message);
					reloaddatatable();
					//reloadData('#{{ $pageModule }}','{{ $pageUrl }}/data?IdEstatus='+lintIdEstatus);
                    $('#motivorechazoModal').modal('hide');
				} else {
					notyMessageError(data.message);
				}
			},"json");

		}
	}
	function ajaxApproveInLine(id, idestatus){
        var motivo = $("#MotivoRechazo").val()
		var datas = {ids:id,oneline:true, status:idestatus, "result":motivo};
		lintIdEstatus = $("#Estatus").val();
		$('.ajaxLoading').show();
		$.post( '{{$pageModule}}/approve' ,datas, function( data ) {
			if(data.status == 'success'){
				//reloadData('#{{ $pageModule }}','{{ $pageUrl }}/data?IdEstatus='+lintIdEstatus);
				reloaddatatable();
				notyMessage(data.message);
                $('#motivorechazoModal').modal('hide');
			} else {
				$('.ajaxLoading').hide();
				notyMessageError(data.message);
				return false;
			}
		},"json");
	}
	function ViewPDF(url,id){
		var lstrAdditionalData = "";
		var lintIdTipo = "1";
		var datas = {"iddocumento":id};
		$('.ajaxLoading').show();
		//$.post( '{{$pageModule}}/infotipo' ,datas, function( data ) {
			//lintIdTipo = data.tipo;
			$.post( '{{$pageModule}}/infoadicional' ,datas, function( data ) {
				$("#AdditionalData").html(data);
				if (lintIdTipo=="1"){
					PDFObject.embed("uploads/documents/"+url, "#ModalViewPDF");
				}
                                        if (url=="")
                                            $("#ModalViewPDF").hide()
				$("#myModal").modal();
			},"html");

		//},"json");
		$('.ajaxLoading').hide();

	}
</script>
<style>
.table th.right { text-align:right !important;}
.table th.center { text-align:center !important;}
#ModalViewPDF {
	height: 400px;
  }
.img-circle {display:none;}
/* DATATABLES */
#{{ $pageModule }}TableContainer {
	   border:none;
}
table.dataTable thead .sorting,
table.dataTable thead .sorting_asc:after,
table.dataTable thead .sorting_desc,
table.dataTable thead .sorting_asc_disabled,
table.dataTable thead .sorting_desc_disabled {
  background: transparent;
}
.dataTables_length {
  float: left;
}
body.DTTT_Print {
  background: #fff;
}
.DTTT_Print #page-wrapper {
  margin: 0;
  background: #fff;
}
button.DTTT_button,
div.DTTT_button,
a.DTTT_button {
  border: 1px solid #e7eaec;
  background: #fff;
  color: #676a6c;
  box-shadow: none;
  padding: 6px 8px;
}
button.DTTT_button:hover,
div.DTTT_button:hover,
a.DTTT_button:hover {
  border: 1px solid #d2d2d2;
  background: #fff;
  color: #676a6c;
  box-shadow: none;
  padding: 6px 8px;
}
button.DTTT_button:hover:not(.DTTT_disabled),
div.DTTT_button:hover:not(.DTTT_disabled),
a.DTTT_button:hover:not(.DTTT_disabled) {
  border: 1px solid #d2d2d2;
  background: #fff;
  box-shadow: none;
}
.dataTables_filter label {
  margin-right: 5px;
}
</style>

