<?php
$servername = "localhost";
$username = "root";
$password = "8044302";
$year;
$mes;$reg;

#try {
    $conn2 = new PDO("mysql:host=$servername;dbname=andina_koadmin", $username, $password);
    // set the PDO error mode to exception
#    $conn2->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
#   echo "Connected successfully";
#    }
#catch(PDOException $e)
#    {
#    echo "Connection failed: " . $e->getMessage();
#    }


?>

<?php  
  if (isset($_GET["year"])) {
    $year = $_GET["year"];
   }
  else{  
    $year = date("Y");
}

  if (isset($_GET["mes"])) {
    $mes = $_GET["mes"];
   }
  else{  
    $mes = 0;
}

 if (isset($_GET["reg"])) {
    $reg = $_GET["reg"];
   }
  else{  
    $reg = 0;
}

 if (isset($_GET["seg"])) {
    $seg = $_GET["seg"];
   }
  else{  
    $seg = 0;
}

 if (isset($_GET["area"])) {
    $area = $_GET["area"];
   }
  else{  
    $area = 0;
}


 if (isset($_GET["proc"])) {
    $proc = $_GET["proc"];
   }
  else{  
    $proc = 0;
}
?>
<?php
$nom_mes;
    if ($mes==1){
        $nom_mes = 'Ene';
    }
    else  if ($mes==2){
        $nom_mes = 'Feb';
    }
    else  if ($mes==3){
        $nom_mes = 'Mar';
    }
    else  if ($mes==4){
        $nom_mes = 'Abr';
    }
    else  if ($mes==5){
        $nom_mes = 'May';
    }
    else  if ($mes==6){
        $nom_mes = 'Jun';
    }
    else  if ($mes==7){
        $nom_mes = 'Jul';
    }
    else  if ($mes==8){
        $nom_mes = 'Ago';
    }
    else  if ($mes==9){
        $nom_mes = 'Sep';
    }
    else  if ($mes==10){
        $nom_mes = 'Oct';
    }
    else  if ($mes==11){
        $nom_mes = 'Nov';
    }
    else  if ($mes==12){
        $nom_mes = 'Dic';
    }
   $color_v = '#3d9b35';
   $color_r = '#e64427';
   $color_n = '#FFC000';

?>

  
<?php 
if($proc == 0){
     
                  $sqlgr = 'SELECT  CAST(SUM(n) AS UNSIGNED) Negocio,
(SELECT ambito_nombre FROM tbl_planes_y_programas_ambito WHERE ambito_id = 1) amb_neg,
CAST(SUM(f) AS UNSIGNED) Financiero ,
(SELECT ambito_nombre FROM tbl_planes_y_programas_ambito WHERE ambito_id = 2) amb_fin,
CAST(SUM(l) AS UNSIGNED) Laboral, 
(SELECT ambito_nombre FROM tbl_planes_y_programas_ambito WHERE ambito_id = 3) amb_lab,
CAST(SUM(s) AS UNSIGNED) seguridad,  
(SELECT ambito_nombre FROM tbl_planes_y_programas_ambito WHERE ambito_id = 4) amb_seg,
CAST(AVG(n+f+l+s) AS UNSIGNED) general
FROM
(
SELECT CAST(COALESCE(AVG(prom_neg),0) AS UNSIGNED) n,0 f,0 l ,0 s
FROM
(
SELECT CAST(AVG(kpi) AS DECIMAL(4,1)) prom_neg

FROM
(SELECT  b.contrato_id,kpiDet_fecha,  (SUM(CASE WHEN kpi_tipo_calc = 1 THEN CASE WHEN kpiDet_puntaje >= kpiDet_meta THEN 1 ELSE 0 END WHEN kpi_tipo_calc = 2 THEN CASE WHEN kpiDet_puntaje <= kpiDet_meta THEN 1 ELSE 0 END WHEN kpi_tipo_calc = 3 THEN CASE WHEN kpiDet_puntaje BETWEEN kpiDet_min AND kpiDet_max THEN 1 ELSE 0 END WHEN kpi_tipo_calc = 4 THEN 1 END )/
COUNT(DISTINCT a.id_kpi) )*100 kpi
FROM tbl_kpimensual a INNER JOIN tbl_kpigral b
ON a.id_kpi = b.id_kpi
INNER JOIN dim_tiempo ON kpiDet_fecha = fecha
INNER JOIN tbl_contrato c ON c.contrato_id = b.contrato_id
WHERE kpiDet_puntaje IS NOT NULL
AND 

anio = '.$year.' AND 
CASE WHEN '.$mes.'= 0 THEN 1=1 ELSE  mes = '.$mes.' END
AND CASE WHEN '.$reg.'= 0 THEN 1=1 ELSE geo_id = '.$reg.' END
AND CASE WHEN '.$seg.'= 0 THEN 1=1 ELSE segmento_id = '.$reg.' END
AND CASE WHEN '.$area.'= 0 THEN 1=1 ELSE afuncional_id = '.$reg.' END

GROUP BY 1,2) asd

UNION ALL


SELECT
CASE WHEN AVG(eval_puntaje) >70 THEN 75 + ((CASE WHEN AVG(eval_puntaje) >70 THEN 25/30 ELSE 75/70 END)*(AVG(eval_puntaje) - 70)) 
ELSE 75 - ((CASE WHEN AVG(eval_puntaje) >70 THEN 25/30 ELSE 75/70 END)*(AVG(eval_puntaje) - 70)) END f
FROM tbl_evalcontratistas a INNER JOIN dim_tiempo 
ON eval_fecha = fecha 
INNER JOIN tbl_contrato b ON a.contrato_id = b.contrato_id
WHERE  
eval_puntaje IS NOT NULL
AND 
CASE WHEN '.$mes.'=0 THEN Anio = '.$year.' ELSE
semestre  = (SELECT DISTINCT semestre FROM dim_tiempo WHERE mes= '.$mes.' AND Anio = '.$year.') AND Anio = '.$year.'
END
and case when '.$area.' = 0 then 1=1 else afuncional_id = '.$area.' end
and case when '.$reg.' = 0 then 1=1 else geo_id = '.$reg.' end
and case when '.$seg.' = 0 then 1=1 else segmento_id = '.$seg.' end

UNION ALL

SELECT CAST(AVG(resultado) AS DECIMAL(4,1)) puntaje
FROM tbl_calidad_adm_cont a INNER JOIN dim_tiempo 
ON fec_rev = fecha
INNER JOIN tbl_contrato b ON a.contrato_id = b.contrato_id
WHERE resultado IS NOT NULL
AND 
CASE WHEN '.$mes.'=0 THEN Anio = '.$year.' ELSE
 trimestre  = (SELECT DISTINCT trimestre FROM dim_tiempo WHERE mes= '.$mes.' AND Anio = '.$year.') AND Anio = '.$year.'
END
and case when '.$area.' = 0 then 1=1 else afuncional_id = '.$area.' end
and case when '.$reg.' = 0 then 1=1 else geo_id = '.$reg.' end
and case when '.$seg.' = 0 then 1=1 else segmento_id = '.$seg.' end
) ambito


UNION ALL 

-- financiero

SELECT 0, CAST(COALESCE(AVG(prom_fin),0) AS UNSIGNED) f ,0,0
FROM
(SELECT 
CASE WHEN AVG(valor) > 50 THEN 75 - (AVG(valor) -50) * (CASE WHEN AVG(valor) > 50 THEN 75/50 ELSE 25/50 END) ELSE
75 + (AVG(valor) -50) * (CASE WHEN AVG(valor) > 50 THEN 75/50 ELSE 25/50 END) END prom_fin
FROM tbl_eval_financiera a
INNER JOIN tbl_contrato c ON c.contrato_id = a.contrato_id
INNER JOIN dim_tiempo d ON fecha_validez = d.fecha
WHERE
anio = '.$year.' AND 
CASE WHEN '.$mes.'= 0 THEN 1=1 ELSE  mes = '.$mes.' END
AND CASE WHEN '.$reg.'= 0 THEN 1=1 ELSE geo_id = '.$reg.' END
AND CASE WHEN '.$seg.'= 0 THEN 1=1 ELSE segmento_id = '.$reg.' END
AND CASE WHEN '.$area.'= 0 THEN 1=1 ELSE afuncional_id = '.$reg.' END

UNION ALL

SELECT 
CASE WHEN AVG(per_moros*100) > 30 THEN 75 - ABS(AVG(per_moros*100) -30) * (CASE WHEN AVG(per_moros*100) > 30 THEN 75/67 ELSE 25/13 END) ELSE
75 + ABS(AVG(per_moros*100) -30) * (CASE WHEN AVG(per_moros*100) > 30 THEN 75/67 ELSE 25/13 END) END prom_trib
FROM tbl_morosidad a
INNER JOIN dim_tiempo d ON fecha_Eval = d.fecha
INNER JOIN tbl_contrato c ON c.contrato_id = a.contrato_id
WHERE 
anio = '.$year.' AND 
CASE WHEN '.$mes.'= 0 THEN 1=1 ELSE  mes = '.$mes.' END
AND CASE WHEN '.$reg.'= 0 THEN 1=1 ELSE geo_id = '.$reg.' END
AND CASE WHEN '.$seg.'= 0 THEN 1=1 ELSE segmento_id = '.$reg.' END
AND CASE WHEN '.$area.'= 0 THEN 1=1 ELSE afuncional_id = '.$reg.' END

UNION ALL

SELECT (1 -( COUNT(DISTINCT (CASE WHEN tCont_eval = \'MALO\' THEN a.contrato_id ELSE 0 END))/ 
COUNT(DISTINCT a.contrato_id)) )*100
FROM
tbl_contratista_tributario a INNER JOIN tbl_contrato b ON a.contrato_id = b.contrato_id

INNER JOIN dim_tiempo d ON a.tCont_fecha = d.fecha
WHERE 
anio = '.$year.' AND 
CASE WHEN '.$mes.'= 0 THEN 1=1 ELSE  mes = '.$mes.' END
AND CASE WHEN '.$reg.'= 0 THEN 1=1 ELSE geo_id = '.$reg.' END
AND CASE WHEN '.$seg.'= 0 THEN 1=1 ELSE segmento_id = '.$reg.' END
AND CASE WHEN '.$area.'= 0 THEN 1=1 ELSE afuncional_id = '.$reg.' END

UNION ALL

SELECT  AVG((total_fondos + prendas)/(mutuos + total_impacto))*100
FROM tbl_garantias a INNER JOIN dim_tiempo b
ON a.fecha = b.fecha
INNER JOIN tbl_contrato c ON c.contrato_id = a.contrato_id
WHERE  
anio = '.$year.' AND 
CASE WHEN '.$mes.'= 0 THEN 1=1 ELSE  mes = '.$mes.' END
AND CASE WHEN '.$reg.'= 0 THEN 1=1 ELSE geo_id = '.$reg.' END
AND CASE WHEN '.$seg.'= 0 THEN 1=1 ELSE segmento_id = '.$reg.' END
AND CASE WHEN '.$area.'= 0 THEN 1=1 ELSE afuncional_id = '.$reg.' END
) finan

UNION ALL

-- laboral

SELECT 0,0,CAST(COALESCE(AVG(prom_lab),0) AS UNSIGNED) l,0
FROM
(SELECT 
CASE WHEN AVG(valor)*100 > 16 THEN 75 - (ABS(AVG(valor)*100 -16)) * (CASE WHEN AVG(valor)*100 > 16 THEN 75/84 ELSE 25/16 END) ELSE
75 + (ABS(AVG(valor)*100 -16)) * (CASE WHEN AVG(valor)*100 > 16 THEN 75/84 ELSE 25/16 END) END prom_lab
FROM tbl_obliglab_repo a INNER JOIN dim_tiempo b
ON a.fecha = b.fecha
INNER JOIN tbl_contrato c ON c.contrato_id = a.contrato_id
WHERE anio = '.$year.' AND 
CASE WHEN '.$mes.'= 0 THEN 1=1 ELSE  mes = '.$mes.' END
AND CASE WHEN '.$reg.'= 0 THEN 1=1 ELSE geo_id = '.$reg.' END
AND CASE WHEN '.$seg.'= 0 THEN 1=1 ELSE segmento_id = '.$reg.' END
AND CASE WHEN '.$area.'= 0 THEN 1=1 ELSE afuncional_id = '.$reg.' END

UNION ALL

SELECT  ((SELECT COUNT(DISTINCT contrato_id) FROM tbl_contrato WHERE cont_fechaFin > CURRENT_DATE) - 
COUNT(DISTINCT a.contrato_id))/(SELECT COUNT(DISTINCT contrato_id) FROM tbl_contrato WHERE cont_fechaFin > CURRENT_DATE) * 100
FROM tbl_contratistacondicionfin a
INNER JOIN dim_tiempo b
ON a.finCont_fecha = b.fecha
INNER JOIN tbl_contrato c ON c.contrato_id = a.contrato_id
WHERE anio = '.$year.' AND 
CASE WHEN '.$mes.'= 0 THEN 1=1 ELSE  mes = '.$mes.' END
AND CASE WHEN '.$reg.'= 0 THEN 1=1 ELSE geo_id = '.$reg.' END
AND CASE WHEN '.$seg.'= 0 THEN 1=1 ELSE segmento_id = '.$reg.' END
AND CASE WHEN '.$area.'= 0 THEN 1=1 ELSE afuncional_id = '.$reg.' END

UNION ALL

SELECT  CAST(AVG((PERC_LAB)) AS DECIMAL(4,1)) puntaje
FROM tbl_fiscalización a INNER JOIN dim_tiempo b
ON a.fecha = b.fecha
INNER JOIN tbl_contrato c ON c.contrato_id = a.contrato_id
WHERE anio = '.$year.' AND 
CASE WHEN '.$mes.'= 0 THEN 1=1 ELSE  mes = '.$mes.' END
AND CASE WHEN '.$reg.'= 0 THEN 1=1 ELSE geo_id = '.$reg.' END
AND CASE WHEN '.$seg.'= 0 THEN 1=1 ELSE segmento_id = '.$reg.' END
AND CASE WHEN '.$area.'= 0 THEN 1=1 ELSE afuncional_id = '.$reg.' END
) lab 

UNION ALL

-- seguridad

SELECT 0,0,0, CAST(COALESCE(AVG(prom_seg),0) AS DECIMAL(3,0)) s
FROM
(SELECT 
CASE WHEN (SUM(n_accid)/SUM(hh)*200000)> 5.2 THEN 0 ELSE
CASE WHEN (SUM(n_accid)/SUM(hh)*200000)> 2.6 THEN 75 - ABS((SUM(n_accid)/SUM(hh)*200000)-2.6) * (CASE WHEN (SUM(n_accid)/SUM(hh)*200000) > 2.6 THEN 75/2.6 ELSE 25/2.6 END) ELSE
75 + ABS((SUM(n_accid)/SUM(hh)*200000) -2.6) * (CASE WHEN (SUM(n_accid)/SUM(hh)*200000) > 2.6 THEN 75/2.6 ELSE 25/2.6 END) END
END prom_seg
FROM tbl_accidentes a
INNER JOIN dim_tiempo d ON a.fecha_informe = d.fecha
INNER JOIN tbl_contrato c ON a.contrato_id = c.contrato_id
WHERE 
anio = '.$year.' AND 
CASE WHEN '.$mes.'= 0 THEN 1=1 ELSE  mes = '.$mes.' END
AND CASE WHEN '.$reg.'= 0 THEN 1=1 ELSE geo_id = '.$reg.' END
AND CASE WHEN '.$seg.'= 0 THEN 1=1 ELSE segmento_id = '.$reg.' END
AND CASE WHEN '.$area.'= 0 THEN 1=1 ELSE afuncional_id = '.$reg.' END

UNION ALL

SELECT 
CASE WHEN (SUM(DIAS_PERD)/SUM(hh)*200000) > 60 THEN 0 ELSE 
CASE WHEN (SUM(DIAS_PERD)/SUM(hh)*200000)> 31.1 THEN 75 - ABS(( SUM(DIAS_PERD)/SUM(hh)*200000 )-31.1) * (CASE WHEN (SUM(DIAS_PERD)/SUM(hh)*200000) > 31.1 THEN 75/31.1 ELSE 25/31.1 END) ELSE
75 + ABS((SUM(DIAS_PERD)/SUM(hh)*200000) -31.1) * (CASE WHEN (SUM(DIAS_PERD)/SUM(hh)*200000) > 31.1 THEN 75/31.1 ELSE 25/31.1 END) END 
END f
FROM tbl_accidentes a
INNER JOIN dim_tiempo d ON a.fecha_informe = d.fecha
INNER JOIN tbl_contrato c ON a.contrato_id = c.contrato_id
WHERE 
anio = '.$year.' AND 
CASE WHEN '.$mes.'= 0 THEN 1=1 ELSE  mes = '.$mes.' END
AND CASE WHEN '.$reg.'= 0 THEN 1=1 ELSE geo_id = '.$reg.' END
AND CASE WHEN '.$seg.'= 0 THEN 1=1 ELSE segmento_id = '.$reg.' END
AND CASE WHEN '.$area.'= 0 THEN 1=1 ELSE afuncional_id = '.$reg.' END

UNION ALL

SELECT AVG((PERC_COND + perc_equip)/2) 
FROM tbl_fiscalización a
INNER JOIN dim_tiempo d ON a.fecha = d.fecha
INNER JOIN tbl_contrato c ON a.contrato_id = c.contrato_id
 WHERE 
anio = '.$year.' AND 
CASE WHEN '.$mes.'= 0 THEN 1=1 ELSE  mes = '.$mes.' END
AND CASE WHEN '.$reg.'= 0 THEN 1=1 ELSE geo_id = '.$reg.' END
AND CASE WHEN '.$seg.'= 0 THEN 1=1 ELSE segmento_id = '.$reg.' END
AND CASE WHEN '.$area.'= 0 THEN 1=1 ELSE afuncional_id = '.$reg.' END
) seg
) panel
                            ';
                    $rawgraf = array();
                    $i=0;

                    $row1 = $conn2->query($sqlgr);
                    $results = $row1->fetchAll(PDO::FETCH_ASSOC);       
                       
                    echo json_encode($results);
                    
                  
}
else if($proc == 1)
{ ?> 
 <form method="post" >
   <div style="display: inline-block;width:70px; height:30px;">
      <b>Año</b>
                     <select name="sl_year" id="sl_year" onchange="contratos(this.value,1)">

<?php
                   $sqlyear = ' select distinct Anio from dim_tiempo 
                              where anio between year(current_Date) -5
                              and YEAR(CASE WHEN DAY(CURRENT_DATE) >= 20 THEN DATE_ADD(DATE_ADD(CURRENT_DATE, INTERVAL -1 MONTH), INTERVAL -(DAY(CURRENT_DATE)-1) DAY)  ELSE DATE_ADD(DATE_ADD(CURRENT_DATE, INTERVAL -2 MONTH), INTERVAL -(DAY(CURRENT_DATE)-1) DAY) END)
                              order by anio desc
                            ';
                     $i=0;
                    
                    foreach ($conn2->query($sqlyear) as $row1) {
                           if($row1["Anio"]==$year){
                              echo "<option  value='".$row1["Anio"]."' selected>".$row1["Anio"]."</option>"; 
                           }
                           else
                              echo "<option  value='".$row1["Anio"]."'>".$row1["Anio"]."</option>"; 
                          $i++;
                    }
                    
            ?>

                    </select>

       
       </div>  
       <div style="display: inline-block; width:80px; height:30px;">
            
           <b>Mes</b>
                     <select name="sl_mes" id="sl_mes" onchange="contratos(this.value,2)">

<?php


                   $sqlmes = ' SELECT 0 mes, \'Todos\' NMes3L
                   UNION ALL
                   SELECT DISTINCT mes , NMes3L  FROM dim_tiempo 
                   WHERE fecha_mes <= CASE WHEN DAY(CURRENT_DATE) >= 20 THEN DATE_ADD(DATE_ADD(CURRENT_DATE, INTERVAL -1 MONTH), INTERVAL -(DAY(CURRENT_DATE)-1) DAY)  ELSE DATE_ADD(DATE_ADD(CURRENT_DATE, INTERVAL -2 MONTH), INTERVAL -(DAY(CURRENT_DATE)-1) DAY) END
                   AND  anio = '.$year.' 
                   ORDER BY mes';
                     $i=0;
                    
                    foreach ($conn2->query($sqlmes) as $row1) {
                           if($row1["mes"]==$mes){
                              echo "<option  value='".$row1["mes"]."' selected>".$row1["NMes3L"]."</option>"; 
                           }
                           else
                              echo "<option  value='".$row1["mes"]."'>".$row1["NMes3L"]."</option>"; 
                          $i++;
                    }
                    
            ?>

                   
                    </select>
</div>
  <div style="display: inline-block;width:120px; height:30px;">
                <b>Tipo Faena</b>
                     <select name="sl_reg" id="sl_reg" onchange="contratos(this.value,3)">

<?php
                   $sqlreg = ' SELECT 0 id, \'Todos\' reg
                   UNION ALL
                   SELECT geo_id, geo_nombre FROM tbl_contgeografico';
                     $i=0;
                    
                    foreach ($conn2->query($sqlreg) as $row1) {
                           if($row1["id"]==$reg){
                              echo "<option  value='".$row1["id"]."' selected>".$row1["reg"]."</option>"; 
                           }
                           else
                              echo "<option  value='".$row1["id"]."'>".$row1["reg"]."</option>"; 
                          $i++;
                    }
                    
            ?>

                   
                    </select>
</div>
 <div style="display: inline-block;width:280px; height:30px;">
                <b>Área</b>
                     <select name="sl_area" id="sl_area" onchange="contratos(this.value,4)">

<?php
                   $sqlarea = ' SELECT 0 id, \'Todos\' area 
                   union all
                   SELECT afuncional_id ,afuncional_nombre area FROM tbl_contareafuncional';
                     $i=0;
                    
                    foreach ($conn2->query($sqlarea) as $row1) {
                           if($row1["id"]==$area){
                              echo "<option  value='".$row1["id"]."' selected>".$row1["area"]."</option>"; 
                           }
                           else
                              echo "<option  value='".$row1["id"]."'>".$row1["area"]."</option>"; 
                          $i++;
                    }
                    
            ?>

                   
                    </select>
        </div>
        <div style="display: inline-block;width:280px; height:30px;">

                    <b>Segmento</b>
                     <select name="sl_seg" id="sl_seg" onchange="contratos(this.value,5)">

<?php
                   $sqlseg = ' SELECT 0 id, \'Todos\' nombre
                   union all
                   SELECT segmento_id, seg_nombre FROM tbl_contsegmento';
                     $i=0;
                    
                    foreach ($conn2->query($sqlseg) as $row1) {
                           if($row1["id"]==$seg){
                              echo "<option  value='".$row1["id"]."' selected>".$row1["nombre"]."</option>"; 
                           }
                           else
                              echo "<option  value='".$row1["id"]."'>".$row1["nombre"]."</option>"; 
                          $i++;
                    }
                    
            ?>

                   
                    </select>
          </div>
             <div style="display: inline-block;width:100px; height:30px;">
                <button type="reset" class="btn-xs btn-default " onclick="contratos('0',6)">Limpiar</button>
          </div>
</form>

<?php
}

?>

