<div class="m-t" style="padding-top:25px;">	
    <div class="row m-b-lg animated fadeInDown delayp1 text-center">
        <h3> {{ $pageTitle }} <small> {{ $pageNote }} </small></h3>
        <hr />       
    </div>
</div>
<div class="m-t">
	<div class="table-responsive" > 	

		<table class="table table-striped table-bordered" >
			<tbody>	
		
			
					<tr>
						<td width='30%' class='label-view text-right'>IdPersona</td>
						<td>{{ $row->IdPersona}} </td>
						
					</tr>
				
					<tr>
						<td width='30%' class='label-view text-right'>RUT</td>
						<td>{{ $row->RUT}} </td>
						
					</tr>
				
					<tr>
						<td width='30%' class='label-view text-right'>Nombres</td>
						<td>{{ $row->Nombres}} </td>
						
					</tr>
				
					<tr>
						<td width='30%' class='label-view text-right'>Apellidos</td>
						<td>{{ $row->Apellidos}} </td>
						
					</tr>
				
					<tr>
						<td width='30%' class='label-view text-right'>Direccion</td>
						<td>{{ $row->Direccion}} </td>
						
					</tr>
				
					<tr>
						<td width='30%' class='label-view text-right'>FechaNacimiento</td>
						<td>{{ $row->FechaNacimiento}} </td>
						
					</tr>
				
					<tr>
						<td width='30%' class='label-view text-right'>Sexo</td>
						<td>{{ $row->Sexo}} </td>
						
					</tr>
				
					<tr>
						<td width='30%' class='label-view text-right'>EstadoCivil</td>
						<td>{{ $row->EstadoCivil}} </td>
						
					</tr>
				
					<tr>
						<td width='30%' class='label-view text-right'>IdEstatus</td>
						<td>{{ $row->IdEstatus}} </td>
						
					</tr>
				
					<tr>
						<td width='30%' class='label-view text-right'>CreadoPor</td>
						<td>{{ $row->CreadoPor}} </td>
						
					</tr>
				
					<tr>
						<td width='30%' class='label-view text-right'>FechaCreacion</td>
						<td>{{ $row->FechaCreacion}} </td>
						
					</tr>
						
					<tr>
						<td width='30%' class='label-view text-right'></td>
						<td> <a href="javascript:history.go(-1)" class="btn btn-primary"> Back To Grid <a> </td>
						
					</tr>					
				
			</tbody>	
		</table>   

	 
	
	</div>
</div>	