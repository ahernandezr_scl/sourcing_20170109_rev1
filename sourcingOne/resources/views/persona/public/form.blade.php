

		 {!! Form::open(array('url'=>'persona/savepublic', 'class'=>'form-horizontal','files' => true , 'parsley-validate'=>'','novalidate'=>' ')) !!}

	@if(Session::has('messagetext'))
	  
		   {!! Session::get('messagetext') !!}
	   
	@endif
	<ul class="parsley-error-list">
		@foreach($errors->all() as $error)
			<li>{{ $error }}</li>
		@endforeach
	</ul>		


<div class="col-md-12">
						<fieldset><legend> Persona</legend>
									
									  <div class="form-group  " >
										<label for="IdPersona" class=" control-label col-md-4 text-left"> IdPersona </label>
										<div class="col-md-6">
										  {!! Form::text('IdPersona', $row['IdPersona'],array('class'=>'form-control', 'placeholder'=>'',   )) !!} 
										 </div> 
										 <div class="col-md-2">
										 	
										 </div>
									  </div> 					
									  <div class="form-group  " >
										<label for="RUT" class=" control-label col-md-4 text-left"> RUT </label>
										<div class="col-md-6">
										  {!! Form::text('RUT', $row['RUT'],array('class'=>'form-control', 'placeholder'=>'',   )) !!} 
										 </div> 
										 <div class="col-md-2">
										 	
										 </div>
									  </div> 					
									  <div class="form-group  " >
										<label for="Nombres" class=" control-label col-md-4 text-left"> Nombres </label>
										<div class="col-md-6">
										  {!! Form::text('Nombres', $row['Nombres'],array('class'=>'form-control', 'placeholder'=>'',   )) !!} 
										 </div> 
										 <div class="col-md-2">
										 	
										 </div>
									  </div> 					
									  <div class="form-group  " >
										<label for="Apellidos" class=" control-label col-md-4 text-left"> Apellidos </label>
										<div class="col-md-6">
										  {!! Form::text('Apellidos', $row['Apellidos'],array('class'=>'form-control', 'placeholder'=>'',   )) !!} 
										 </div> 
										 <div class="col-md-2">
										 	
										 </div>
									  </div> 					
									  <div class="form-group  " >
										<label for="Direccion" class=" control-label col-md-4 text-left"> Direccion </label>
										<div class="col-md-6">
										  {!! Form::text('Direccion', $row['Direccion'],array('class'=>'form-control', 'placeholder'=>'',   )) !!} 
										 </div> 
										 <div class="col-md-2">
										 	
										 </div>
									  </div> 					
									  <div class="form-group  " >
										<label for="FechaNacimiento" class=" control-label col-md-4 text-left"> FechaNacimiento </label>
										<div class="col-md-6">
										  {!! Form::text('FechaNacimiento', $row['FechaNacimiento'],array('class'=>'form-control', 'placeholder'=>'',   )) !!} 
										 </div> 
										 <div class="col-md-2">
										 	
										 </div>
									  </div> 					
									  <div class="form-group  " >
										<label for="Sexo" class=" control-label col-md-4 text-left"> Sexo </label>
										<div class="col-md-6">
										  {!! Form::text('Sexo', $row['Sexo'],array('class'=>'form-control', 'placeholder'=>'',   )) !!} 
										 </div> 
										 <div class="col-md-2">
										 	
										 </div>
									  </div> 					
									  <div class="form-group  " >
										<label for="EstadoCivil" class=" control-label col-md-4 text-left"> EstadoCivil </label>
										<div class="col-md-6">
										  {!! Form::text('EstadoCivil', $row['EstadoCivil'],array('class'=>'form-control', 'placeholder'=>'',   )) !!} 
										 </div> 
										 <div class="col-md-2">
										 	
										 </div>
									  </div> 					
									  <div class="form-group  " >
										<label for="IdEstatus" class=" control-label col-md-4 text-left"> IdEstatus </label>
										<div class="col-md-6">
										  {!! Form::text('IdEstatus', $row['IdEstatus'],array('class'=>'form-control', 'placeholder'=>'',   )) !!} 
										 </div> 
										 <div class="col-md-2">
										 	
										 </div>
									  </div> 					
									  <div class="form-group  " >
										<label for="CreadoPor" class=" control-label col-md-4 text-left"> CreadoPor </label>
										<div class="col-md-6">
										  {!! Form::text('CreadoPor', $row['CreadoPor'],array('class'=>'form-control', 'placeholder'=>'',   )) !!} 
										 </div> 
										 <div class="col-md-2">
										 	
										 </div>
									  </div> 					
									  <div class="form-group  " >
										<label for="FechaCreacion" class=" control-label col-md-4 text-left"> FechaCreacion </label>
										<div class="col-md-6">
										  {!! Form::text('FechaCreacion', $row['FechaCreacion'],array('class'=>'form-control', 'placeholder'=>'',   )) !!} 
										 </div> 
										 <div class="col-md-2">
										 	
										 </div>
									  </div> </fieldset>
			</div>
			
			

			<div style="clear:both"></div>	
				
					
				  <div class="form-group">
					<label class="col-sm-4 text-right">&nbsp;</label>
					<div class="col-sm-8">	
					<button type="submit" name="apply" class="btn btn-info btn-sm" ><i class="fa  fa-check-circle"></i> {{ Lang::get('core.sb_apply') }}</button>
					<button type="submit" name="submit" class="btn btn-primary btn-sm" ><i class="fa  fa-save "></i> {{ Lang::get('core.sb_save') }}</button>
				  </div>	  
			
		</div> 
		 
		 {!! Form::close() !!}
		 
   <script type="text/javascript">
	$(document).ready(function() { 
		
		 

		$('.removeCurrentFiles').on('click',function(){
			var removeUrl = $(this).attr('href');
			$.get(removeUrl,function(response){});
			$(this).parent('div').empty();	
			return false;
		});		
		
	});
	</script>		 
