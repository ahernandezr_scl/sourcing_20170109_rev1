@extends('layouts.app')

@section('content')
<div class="page-content row">
    <!-- Page header -->

	 
	 
 	<div class="page-content-wrapper m-t">   

<div class="sbox ">
	<div class="sbox-title"><h3> {{ $pageTitle }} <small>{{ $pageNote }}</small></h3> 
	<div class="sbox-tools" >
   		<a href="{{ URL::to('persona?return='.$return) }}" class="tips btn btn-xs btn-white pull-right" title="{{ Lang::get('core.btn_back') }}"><i class="icon-backward"></i>&nbsp;{{ Lang::get('core.btn_back') }}</a>
		@if($access['is_add'] ==1)
   		<a href="{{ URL::to('persona/update/'.$id.'?return='.$return) }}" class="tips btn btn-xs btn-white pull-right" title="{{ Lang::get('core.btn_edit') }}"><i class="fa fa-edit"></i>&nbsp;{{ Lang::get('core.btn_edit') }}</a>
		@endif 
	</div>	
	</div>
	<div class="sbox-content" style="background:#fff;"> 	

		<table class="table table-striped table-bordered" >
			<tbody>	
		
					<tr>
						<td width='30%' class='label-view text-right'>IdPersona</td>
						<td>{{ $row->IdPersona}} </td>
						
					</tr>
				
					<tr>
						<td width='30%' class='label-view text-right'>RUT</td>
						<td>{{ $row->RUT}} </td>
						
					</tr>
				
					<tr>
						<td width='30%' class='label-view text-right'>Nombres</td>
						<td>{{ $row->Nombres}} </td>
						
					</tr>
				
					<tr>
						<td width='30%' class='label-view text-right'>Apellidos</td>
						<td>{{ $row->Apellidos}} </td>
						
					</tr>
				
					<tr>
						<td width='30%' class='label-view text-right'>Direccion</td>
						<td>{{ $row->Direccion}} </td>
						
					</tr>
				
					<tr>
						<td width='30%' class='label-view text-right'>FechaNacimiento</td>
						<td>{{ $row->FechaNacimiento}} </td>
						
					</tr>
				
					<tr>
						<td width='30%' class='label-view text-right'>Sexo</td>
						<td>{{ $row->Sexo}} </td>
						
					</tr>
				
					<tr>
						<td width='30%' class='label-view text-right'>EstadoCivil</td>
						<td>{{ $row->EstadoCivil}} </td>
						
					</tr>
				
					<tr>
						<td width='30%' class='label-view text-right'>IdEstatus</td>
						<td>{{ $row->IdEstatus}} </td>
						
					</tr>
				
					<tr>
						<td width='30%' class='label-view text-right'>CreadoPor</td>
						<td>{{ $row->CreadoPor}} </td>
						
					</tr>
				
					<tr>
						<td width='30%' class='label-view text-right'>FechaCreacion</td>
						<td>{{ $row->FechaCreacion}} </td>
						
					</tr>
				
			</tbody>	
		</table>   

	 
	
	</div>
</div>	

	</div>
</div>
	  
@stop