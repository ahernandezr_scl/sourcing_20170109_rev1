<?php 
    $lintLevelUser = \MySourcing::LevelUser(\Session::get('uid'));
    $lintIdUser = \Session::get('uid');
?>
<!-- Modal -->
<div id="myModal" class="modal fade" role="dialog">
  <div class="modal-dialog modal-lg">
    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Detalle</h4>
      </div>
      <div class="modal-body">
        <div id="ModalViewPDF" ></div>
        <div id="AdditionalData" ></div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
      </div>
    </div>
  </div>
</div>
<?php usort($tableGrid, "SiteHelpers::_sort"); ?>

<?php if ($lintLevelUser!=6) { ?>
<div class="sbox float-e-margins">
  <div class="sbox-title">
    <h3 style="display: block!important;">Solicitudes especiales</h3>
      <div class="sbox-tools" >
        <a class="collapse-link">
            <i class="fa fa-chevron-down"></i>
        </a>
      </div>
  </div>
  <div class="sbox-content" style="display:none">
    <?php echo Form::open(array('url'=>'documentos/delete/', 'class'=>'form-horizontal' ,'id' =>'SximoTable'  ,'data-parsley-validate'=>'' )) ;?>
	<div class="table-responsive-doc">
    <table class="table table-striped table-bordered table-hover dataTable"  role="grid" id="{{ $pageModule }}TableTwo">
        <thead>
			<tr>
				<th width="20"> No </th>
				<th width="30"> <input type="checkbox" class="checkall" /></th>
				@if($setting['view-method']=='expand')<th width="30" style="width: 30px;">  </th> @endif
				<?php foreach ($tableGrid as $t) :
					if($t['view'] =='1'):
						$limited = isset($t['limited']) ? $t['limited'] :'';
						if(SiteHelpers::filterColumn($limited ))
						{
							if ($t['field']!="contrato_id"  && $t['field']!="IdEntidad"  && $t['field']!="Detalle" && $t['field']!="FechaEmision" &&  $t['field']!="FechaVencimiento" &&  $t['field']!="IdEstatus"   ) {
							  echo '<th align="'.$t['align'].'" width="'.$t['width'].'">'.\SiteHelpers::activeLang($t['label'],(isset($t['language'])? $t['language'] : array())).'</th>';
							}
						}
					endif;
				endforeach; ?>
				<th width="100" class="text-right"><?php echo Lang::get('core.btn_action') ;?></th>
			  </tr>
        </thead>

        <tbody>
           		<?php 
           			$i = 0;
           		    foreach ($rowDataDos as $row) :
           			  $id = $row->IdDocumento;
           		?>
                <tr class="editable" id="form-{{ $row->IdDocumento }}">
					<td class="number"> <?php echo ++$i;?>  </td>
					<td ><input type="checkbox" class="ids" name="ids[]" value="<?php echo $row->IdDocumento ;?>" />  </td>
					@if($setting['view-method']=='expand')
					<td><a href="javascript:void(0)" class="expandable" rel="#row-{{ $row->IdDocumento }}" data-url="{{ url('documentos/show/'.$id) }}"><i class="fa fa-plus " ></i></a></td>
					@endif
					 <?php foreach ($tableGrid as $field) :
					 	if($field['view'] =='1') :
							$value = SiteHelpers::formatRows($row->{$field['field']}, $field , $row);
						 	?>
						 	<?php $limited = isset($field['limited']) ? $field['limited'] :''; ?>
						 	@if(SiteHelpers::filterColumn($limited ))
								<?php if ($field['field']!="contrato_id"  && $field['field']!="IdEntidad" && $field['field']!="Detalle"  &&  $field['field']!="FechaEmision" &&  $field['field']!="FechaVencimiento" &&  $field['field']!="IdEstatus"   ) { ?>

								 <td align="<?php echo $field['align'];?>" data-values="{{ $row->{$field['field']} }}" data-field="{{ $field['field'] }}" data-format="{{ htmlentities($value) }}">
									{!! $value !!}
								 </td>
								<?php } ?>
							@endif
						 <?php endif;
						endforeach;
					  ?>
				 <td data-values="action" data-key="<?php echo $row->IdDocumento ;?>" width="100" class="text-right">
					<div class=" action dropup" >

						<a href="{{ URL::to('documentos/update/'.$id) }}" onclick="SximoModal(this.href,'Edit Form'); return false; "  class="btn btn-xs btn-white tips" title="{{ Lang::get('core.btn_edit') }}"><i class="fa  fa-upload"></i></a>

					</div>
				</td>
                </tr>
                @if($setting['view-method']=='expand')
                <tr style="display:none" class="expanded" id="row-{{ $row->IdDocumento }}">
                	<td class="number"></td>
                	<td></td>
                	<td></td>
                	<td colspan="{{ $colspan}}" class="data"></td>
                	<td></td>
                </tr>
                @endif
            <?php endforeach;?> 

        </tbody>

    </table>

	</div>
	<?php echo Form::close() ;?>


  </div>
</div>

<?php } ?>

<div class="sbox">
	<div class="sbox-title">
	<h3><i class="fa fa-table"></i> <?php echo $pageTitle ;?> <small>{{ $pageNote }}</small></h3>

		<div class="sbox-tools" >
			<a href="javascript:void(0)" class="btn btn-xs btn-white tips" title="Clear Search" onclick="reloadData('#{{ $pageModule }}','documentos/data?search=')"><i class="fa fa-trash-o"></i> Clear Search </a>
			<a href="javascript:void(0)" class="btn btn-xs btn-white tips" title="Reload Data" onclick="reloadData('#{{ $pageModule }}','documentos/data?return={{ $return }}')"><i class="fa fa-refresh"></i></a>
			@if(Session::get('gid') ==1)
			<a href="{{ url('sximo/module/config/'.$pageModule) }}" class="btn btn-xs btn-white tips" title=" {{ Lang::get('core.btn_config') }}" ><i class="fa fa-cog"></i></a>
			@endif
		</div>
	</div>
	<div class="sbox-content">

	 {!! (isset($search_map) ? $search_map : '') !!}

	 <?php echo Form::open(array('url'=>'documentos/delete/', 'class'=>'form-horizontal' ,'id' =>'SximoTable'  ,'data-parsley-validate'=>'' )) ;?>
<div class="table-responsive">
    <table class="table table-striped table-bordered table-hover dataTable"  role="grid" id="{{ $pageModule }}Table">
        <thead>
			<tr>
				<th width="20"> No </th>
				<th width="30"> <input type="checkbox" class="checkall" /></th>
				@if($setting['view-method']=='expand')<th width="30" style="width: 30px;">  </th> @endif
				<?php foreach ($tableGrid as $t) :
					if($t['view'] =='1'):
						$limited = isset($t['limited']) ? $t['limited'] :'';
						if(SiteHelpers::filterColumn($limited ))
						{
							echo '<th align="'.$t['align'].'" width="'.$t['width'].'">'.\SiteHelpers::activeLang($t['label'],(isset($t['language'])? $t['language'] : array())).'</th>';
						}
					endif;
				endforeach; ?>
				<th width="100" class="text-right"><?php echo Lang::get('core.btn_action') ;?></th>
			  </tr>
        </thead>

        <tbody>
            <!--
        	@if($access['is_add'] =='1' && $setting['inline']=='true')
			<tr id="form-0" >
				<td> # </td>
				<td> </td>
				@if($setting['view-method']=='expand') <td> </td> @endif
				@foreach ($tableGrid as $t)
					@if($t['view'] =='1')
					<?php $limited = isset($t['limited']) ? $t['limited'] :''; ?>
						@if(SiteHelpers::filterColumn($limited ))
						<td data-form="{{ $t['field'] }}" data-form-type="{{ AjaxHelpers::inlineFormType($t['field'],$tableForm)}}">
							{!! SiteHelpers::transForm($t['field'] , $tableForm) !!}
						</td>
						@endif
					@endif
				@endforeach
				<td width="100" class="text-right">
					<button onclick="saved('form-0')" class="btn btn-primary btn-xs" type="button"><i class="icon-checkmark-circle2"></i></button>
				</td>
			  </tr>
			  @endif

           		<?php 
           			$i = 0;
           			foreach ($rowData as $row) :
         			$id = $row->IdDocumento;
           		?>
                <tr class="editable" id="form-{{ $row->IdDocumento }}">
					<td class="number"> <?php echo ++$i;?>  </td>
					<td ><input type="checkbox" class="ids" name="ids[]" value="<?php echo $row->IdDocumento ;?>" />  </td>
					@if($setting['view-method']=='expand')
					<td><a href="javascript:void(0)" class="expandable" rel="#row-{{ $row->IdDocumento }}" data-url="{{ url('documentos/show/'.$id) }}"><i class="fa fa-plus " ></i></a></td>
					@endif
					 <?php foreach ($tableGrid as $field) :
					 	if($field['view'] =='1') :
							$value = SiteHelpers::formatRows($row->{$field['field']}, $field , $row);
						 	?>
						 	<?php $limited = isset($field['limited']) ? $field['limited'] :''; ?>
						 	@if(SiteHelpers::filterColumn($limited ))
								 <td align="<?php echo $field['align'];?>" data-values="{{ $row->{$field['field']} }}" data-field="{{ $field['field'] }}" data-format="{{ htmlentities($value) }}">
									{!! $value !!}
								 </td>
							@endif
						 <?php endif;
						endforeach;
					  ?>
				 <td data-values="action" data-key="<?php echo $row->IdDocumento ;?>" width="100" class="text-right">
					<div class=" action dropup" >

						<a href="{{ URL::to('documentos/update/'.$id) }}" onclick="SximoModal(this.href,'Edit Form'); return false; "  class="btn btn-xs btn-white tips" title="{{ Lang::get('core.btn_edit') }}"><i class="fa  fa-upload"></i></a>

					</div>
				</td>
                </tr>
                @if($setting['view-method']=='expand')
                <tr style="display:none" class="expanded" id="row-{{ $row->IdDocumento }}">
                	<td class="number"></td>
                	<td></td>
                	<td></td>
                	<td colspan="{{ $colspan}}" class="data"></td>
                	<td></td>
                </tr>
                @endif
            <?php endforeach;?> -->

        </tbody>

    </table>

	</div>
	<?php echo Form::close() ;?>


	</div>
</div>

	@if($setting['inline'] =='true') @include('sximo.module.utility.inlinegrid') @endif
<script>
	var lintIdEstatus = <?=isset($param["IdEstatus"])?$param["IdEstatus"]:0;?>;
	function reloaddatatable(){
		$('.ajaxLoading').show();
	    lobjDataTable.ajax.url('documentos/showlist?IdEstatus='+lintIdEstatus).load(function(){
			$('.ajaxLoading').hide();
	    });
    }
    function filterEstatus(val){
        if (val !==''){
            lintIdEstatus = val;
            reloaddatatable();
        }
	}
$(document).ready(function() {
	$('.collapse-link').click(function () {
        var ibox = $(this).closest('div.sbox');
        var button = $(this).find('i');
        var content = ibox.find('div.sbox-content');
        content.slideToggle(200);
        button.toggleClass('fa-chevron-up').toggleClass('fa-chevron-down');
        ibox.toggleClass('').toggleClass('border-bottom');
        setTimeout(function () {
            ibox.resize();
            ibox.find('[id^=map-]').resize();
        }, 50);
    });

	$('.tips').tooltip();
	$('input[type="checkbox"],input[type="radio"]').iCheck({
		checkboxClass: 'icheckbox_square-red',
		radioClass: 'iradio_square-red',
	});
	$('#{{ $pageModule }}Table .checkall').on('ifChecked',function(){
		$('#{{ $pageModule }}Table input[type="checkbox"]').iCheck('check');
	});
	$('#{{ $pageModule }}Table .checkall').on('ifUnchecked',function(){
		$('#{{ $pageModule }}Table input[type="checkbox"]').iCheck('uncheck');
	});

	lobjDataTable = $('#{{ $pageModule }}Table').DataTable({
        "dom": 'Tfigtlp',
        "tableTools": {
            "sSwfPath": "sximo/js/plugins/dataTables/swf/copy_csv_xls_pdf.swf"
        },
        "language": {
            "url": "sximo/js/plugins/dataTables/language/Spanish.json"
        },
        "deferRender": true,
        "ajax": { url:'documentos/showlist?IdEstatus='+lintIdEstatus },
        "columns": [
            { "data": "id" },
            { "data": "checkbox" },
            <?php
              foreach ($tableGrid as $field) {
                if($field['view'] =='1') {
                  echo '            {"data": "'.$field['field'].'"}, ';
                }
              }
            ?>
            { "data": "action" }
        ],
        "info": false,
        "columnDefs": [
                        { "orderable": false, "targets": 1 },
                        { "orderable": false, "targets": 13 },
                        { "orderable": false, "targets": 5 }
                      ],
        "initComplete": function(settings, json) {
            var lstrResult;
            <?php if($access['is_add'] ==1 ) { ?>
            lstrResult = '<label style="float:left">';
            lstrResult += '<select name="IdEstatus" id="IdEstatus"   class="form-control select-liquid" onchange="filterEstatus(this.value)">';
            lstrResult += '<option <?=($param["IdEstatus"]==7?"selected":"")?> value="7"> Por asociar</option>';
            lstrResult += '<option <?=($param["IdEstatus"]==1?"selected":"")?> value="1"> Por cargar</option>';
            lstrResult += '<option <?=($param["IdEstatus"]==2?"selected":"")?> value="2" > Por aprobar</option>';
            lstrResult += '<option <?=($param["IdEstatus"]==3?"selected":"")?> value="3"> No aprobado</option>';
            lstrResult += '<option <?=($param["IdEstatus"]==4?"selected":"")?> value="4"> Temporal</option>';
            lstrResult += '<option <?=($param["IdEstatus"]==5?"selected":"")?> value="5"> Aprobado</option>';
            lstrResult += '<option <?=($param["IdEstatus"]==8?"selected":"")?> value="8"> Vencido</option>';
            lstrResult += '<option <?=($param["IdEstatus"]==6?"selected":"")?> value="6"> Todos</option>';
            lstrResult += '</select>';
            lstrResult += '</label>';
            $('.DTTT_container').before(lstrResult);
            <?php } ?>

                        }
    });

	$('#{{ $pageModule }}TableTwo').DataTable({
        "dom": 'gtp',
        "tableTools": {
            "sSwfPath": "sximo/js/plugins/dataTables/swf/copy_csv_xls_pdf.swf"
        },
        "language": {
            "url": "sximo/js/plugins/dataTables/language/Spanish.json"
        },
        "info": false,
        "columnDefs": [
                        { "orderable": false, "targets": 1 }
                      ],
        "initComplete": function(settings, json) {

                        }
    });

});
function ViewPDF(url,id){
		PDFObject.embed("uploads/documents/"+url, "#ModalViewPDF");
		var lstrAdditionalData = "";
		var datas = {"iddocumento":id};
		$('.ajaxLoading').show();
        $.post( 'aprobaciones/infoadicional' ,datas, function( data ) {
			$("#AdditionalData").html(data);
			$('.ajaxLoading').hide();
		},"html");
		$("#myModal").modal();
	}
</script>
<style>
.table th.right { text-align:right !important;}
.table th.center { text-align:center !important;}
.img-circle {display:none;}
/* DATATABLES */
.table-responsive {
	   border:none;
}
table.dataTable thead .sorting,
table.dataTable thead .sorting_asc:after,
table.dataTable thead .sorting_desc,
table.dataTable thead .sorting_asc_disabled,
table.dataTable thead .sorting_desc_disabled {
  background: transparent;
}
.dataTables_wrapper {
  padding-bottom: 30px;
}
.dataTables_length {
  float: left;
}
body.DTTT_Print {
  background: #fff;
}
.DTTT_Print #page-wrapper {
  margin: 0;
  background: #fff;
}
button.DTTT_button,
div.DTTT_button,
a.DTTT_button {
  border: 1px solid #e7eaec;
  background: #fff;
  color: #676a6c;
  box-shadow: none;
  padding: 6px 8px;
}
button.DTTT_button:hover,
div.DTTT_button:hover,
a.DTTT_button:hover {
  border: 1px solid #d2d2d2;
  background: #fff;
  color: #676a6c;
  box-shadow: none;
  padding: 6px 8px;
}
button.DTTT_button:hover:not(.DTTT_disabled),
div.DTTT_button:hover:not(.DTTT_disabled),
a.DTTT_button:hover:not(.DTTT_disabled) {
  border: 1px solid #d2d2d2;
  background: #fff;
  box-shadow: none;
}
.dataTables_filter label {
  margin-right: 5px;
}
#ModalViewPDF {
	height: 400px;
}
</style>

