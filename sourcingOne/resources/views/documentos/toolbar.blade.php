<div class="row m-b">
	<div class="col-md-6">
			<a href="{{ URL::to( $pageModule .'/search/ajax') }}" class="btn btn-sm btn-white" onclick="SximoModal(this.href,'Advance Search'); return false;" ><i class="icon-search3"></i> Search</a>
				<select name="Estatus" id="Estatus"   class="select-liquid" onchange="filterEstatus(this.value)">
				<option <?=($param['Estatus']==7?'selected':'')?> value="7"> Por asociar</option>
				<option <?=($param['Estatus']==1?'selected':'')?> value="1"> Por cargar</option>
				<option <?=($param['Estatus']==2?'selected':'')?> value="2" > Por aprobar</option>
				<option <?=($param['Estatus']==3?'selected':'')?> value="3"> No probado</option>
				<option <?=($param['Estatus']==4?'selected':'')?> value="4"> Temporal</option>
				<option <?=($param['Estatus']==5?'selected':'')?> value="5"> Aprobado</option>
				<option <?=($param['Estatus']==8?'selected':'')?> value="8"> Vencidos</option>
				<option <?=($param['Estatus']==6?'selected':'')?> value="6"> Todos</option>
			</select>

	</div>
	<div class="col-md-6 ">
		@if($access['is_excel'] ==1)
		<div class="pull-right">
			<a href="{{ URL::to( $pageModule .'/export/excel?return='.$return) }}" class="btn btn-sm btn-white"><i class="icon-file-download"></i> Excel</a>
			<a href="{{ URL::to( $pageModule .'/export/word?return='.$return) }}" class="btn btn-sm btn-white"><i class="icon-file-download"></i> Word </a>
			<a href="{{ URL::to( $pageModule .'/export/csv?return='.$return) }}" class="btn btn-sm btn-white"><i class="icon-file-download"></i> CSV </a>
			<a href="{{ URL::to( $pageModule .'/export/print?return='.$return) }}" class="btn btn-sm btn-white" onclick="ajaxPopupStatic(this.href); return false;" ><i class="icon-print2"></i> Print</a>
		</div>
		@endif
	</div>
</div>
