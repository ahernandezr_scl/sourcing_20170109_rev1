@if($setting['view-method'] =='native')
<div class="sbox">
	<div class="sbox-title">
		<h4> <i class="fa fa-table"></i> <?php echo $pageTitle ;?> <small>{{ $pageNote }}</small>
			<a href="javascript:void(0)" class="collapse-close pull-right btn btn-xs btn-danger" onclick="ajaxViewClose('#{{ $pageModule }}')">
			<i class="fa fa fa-times"></i></a>
		</h4>
	 </div>

	<div class="sbox-content">
@endif

		<table class="table table-striped table-bordered" >
			<tbody>

					<tr>
						<td width='30%' class='label-view text-right'>{{ SiteHelpers::activeLang('IdDocumento', (isset($fields['IdDocumento']['language'])? $fields['IdDocumento']['language'] : array())) }}</td>
						<td>{{ $row->IdDocumento}} </td>

					</tr>

					<tr>
						<td width='30%' class='label-view text-right'>{{ SiteHelpers::activeLang('IdRequisito', (isset($fields['IdRequisito']['language'])? $fields['IdRequisito']['language'] : array())) }}</td>
						<td>{{ $row->IdRequisito}} </td>

					</tr>

					<tr>
						<td width='30%' class='label-view text-right'>{{ SiteHelpers::activeLang('Type', (isset($fields['IdTipoDocumento']['language'])? $fields['IdTipoDocumento']['language'] : array())) }}</td>
						<td>{{ SiteHelpers::formatLookUp($row->IdTipoDocumento,'IdTipoDocumento','1:tbl_tipos_documentos:IdTipoDocumento:Descripcion') }} </td>

					</tr>

					<tr>
						<td width='30%' class='label-view text-right'>{{ SiteHelpers::activeLang('Entidad', (isset($fields['Entidad']['language'])? $fields['Entidad']['language'] : array())) }}</td>
						<td>{{ SiteHelpers::formatLookUp($row->Entidad,'Entidad','1:tbl_entidades:IdEntidad:Entidad') }} </td>

					</tr>

					<tr>
						<td width='30%' class='label-view text-right'>{{ SiteHelpers::activeLang('', (isset($fields['IdEntidad']['language'])? $fields['IdEntidad']['language'] : array())) }}</td>
						<td>{{ MySourcing::DocumentsDetailTwo($row->{{$row->IdEntidad}},Entidad) }} </td>

					</tr>

					<tr>
						<td width='30%' class='label-view text-right'>{{ SiteHelpers::activeLang('Documento', (isset($fields['Documento']['language'])? $fields['Documento']['language'] : array())) }}</td>
						<td>{{ $row->Documento}} </td>

					</tr>

					<tr>
						<td width='30%' class='label-view text-right'>{{ SiteHelpers::activeLang('Documento', (isset($fields['DocumentoURL']['language'])? $fields['DocumentoURL']['language'] : array())) }}</td>
						<td>{!! SiteHelpers::formatRows($row->DocumentoURL,$fields['DocumentoURL'],$row ) !!} </td>

					</tr>

					<tr>
						<td width='30%' class='label-view text-right'>{{ SiteHelpers::activeLang('FechaVencimiento', (isset($fields['FechaVencimiento']['language'])? $fields['FechaVencimiento']['language'] : array())) }}</td>
						<td>{{ date('d/m/Y',strtotime($row->FechaVencimiento)) }} </td>

					</tr>

					<tr>
						<td width='30%' class='label-view text-right'>{{ SiteHelpers::activeLang('IdEstatus', (isset($fields['IdEstatus']['language'])? $fields['IdEstatus']['language'] : array())) }}</td>
						<td>{{ MySourcing::DocumentsStatus($row->{{$row->IdEstatus}}) }} </td>

					</tr>

					<tr>
						<td width='30%' class='label-view text-right'>{{ SiteHelpers::activeLang('CreatedOn', (isset($fields['createdOn']['language'])? $fields['createdOn']['language'] : array())) }}</td>
						<td>{{ date('d/m/Y',strtotime($row->createdOn)) }} </td>

					</tr>

					<tr>
						<td width='30%' class='label-view text-right'>{{ SiteHelpers::activeLang('Entry By', (isset($fields['entry_by']['language'])? $fields['entry_by']['language'] : array())) }}</td>
						<td>{{ SiteHelpers::formatLookUp($row->entry_by,'entry_by','1:tb_users:id:first_name|last_name') }} </td>

					</tr>

					<tr>
						<td width='30%' class='label-view text-right'>{{ SiteHelpers::activeLang('UpdatedOn', (isset($fields['updatedOn']['language'])? $fields['updatedOn']['language'] : array())) }}</td>
						<td>{{ date('d/m/Y',strtotime($row->updatedOn)) }} </td>

					</tr>

					<tr>
						<td width='30%' class='label-view text-right'>{{ SiteHelpers::activeLang('Detalle', (isset($fields['Detalle']['language'])? $fields['Detalle']['language'] : array())) }}</td>
						<td>{{ $row->Detalle}} </td>

					</tr>

			</tbody>
		</table>



@if($setting['form-method'] =='native')
	</div>
</div>
@endif

<script>
$(document).ready(function(){

});
</script>
