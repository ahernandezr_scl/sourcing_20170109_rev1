

		 {!! Form::open(array('url'=>'documentos/savepublic', 'class'=>'form-horizontal','files' => true , 'parsley-validate'=>'','novalidate'=>' ')) !!}

	@if(Session::has('messagetext'))
	  
		   {!! Session::get('messagetext') !!}
	   
	@endif
	<ul class="parsley-error-list">
		@foreach($errors->all() as $error)
			<li>{{ $error }}</li>
		@endforeach
	</ul>		


<div class="col-md-12">
						<fieldset><legend> Solicitudes</legend>
				{!! Form::hidden('IdDocumento', $row['IdDocumento']) !!}{!! Form::hidden('IdRequisito', $row['IdRequisito']) !!}{!! Form::hidden('IdTipoDocumento', $row['IdTipoDocumento']) !!}{!! Form::hidden('Entidad', $row['Entidad']) !!}{!! Form::hidden('IdEntidad', $row['IdEntidad']) !!}{!! Form::hidden('Documento', $row['Documento']) !!}
									  <div class="form-group  " >
										<label for="Documento" class=" control-label col-md-4 text-left"> Documento <span class="asterix"> * </span></label>
										<div class="col-md-6">
										  <input  type='file' name='DocumentoURL' id='DocumentoURL' @if($row['DocumentoURL'] =='') class='required' @endif style='width:150px !important;'  />
					 	<div >
						{!! SiteHelpers::showUploadedFile($row['DocumentoURL'],'/uploads/documents/') !!}

						</div>
					
										 </div>
										 <div class="col-md-2">
										 	
										 </div>
									  </div> 
									  <div class="form-group  " >
										<label for="FechaEmision" class=" control-label col-md-4 text-left"> FechaEmision <span class="asterix"> * </span></label>
										<div class="col-md-6">
										  
				<div class="input-group m-b" style="width:150px !important;">
					{!! Form::text('FechaEmision', $row['FechaEmision'],array('class'=>'form-control date')) !!}
					<span class="input-group-addon"><i class="fa fa-calendar"></i></span>
				</div>
										 </div>
										 <div class="col-md-2">
										 	
										 </div>
									  </div> 
									  <div class="form-group  " >
										<label for="Fecha Vencimiento" class=" control-label col-md-4 text-left"> Fecha Vencimiento </label>
										<div class="col-md-6">
										  
				<div class="input-group m-b" style="width:150px !important;">
					{!! Form::text('FechaVencimiento', $row['FechaVencimiento'],array('class'=>'form-control date')) !!}
					<span class="input-group-addon"><i class="fa fa-calendar"></i></span>
				</div>
										 </div>
										 <div class="col-md-2">
										 	
										 </div>
									  </div> 
									  <div class="form-group  " >
										<label for="Resultado" class=" control-label col-md-4 text-left"> Resultado </label>
										<div class="col-md-6">
										  <textarea name='Resultado' rows='5' id='Resultado' class='form-control '
				           >{{ $row['Resultado'] }}</textarea>
										 </div>
										 <div class="col-md-2">
										 	
										 </div>
									  </div> {!! Form::hidden('IdEstatus', $row['IdEstatus']) !!}{!! Form::hidden('createdOn', $row['createdOn']) !!}{!! Form::hidden('updatedOn', $row['updatedOn']) !!}
									  <div class="form-group  " >
										<label for="Contrato Id" class=" control-label col-md-4 text-left"> Contrato Id </label>
										<div class="col-md-6">
										  <textarea name='contrato_id' rows='5' id='contrato_id' class='form-control '
				           >{{ $row['contrato_id'] }}</textarea>
										 </div>
										 <div class="col-md-2">
										 	
										 </div>
									  </div> </fieldset>
			</div>

			

			<div style="clear:both"></div>	
				
					
				  <div class="form-group">
					<label class="col-sm-4 text-right">&nbsp;</label>
					<div class="col-sm-8">	
					<button type="submit" name="apply" class="btn btn-info btn-sm" ><i class="fa  fa-check-circle"></i> {{ Lang::get('core.sb_apply') }}</button>
					<button type="submit" name="submit" class="btn btn-primary btn-sm" ><i class="fa  fa-save "></i> {{ Lang::get('core.sb_save') }}</button>
				  </div>	  
			
		</div> 
		 
		 {!! Form::close() !!}
		 
   <script type="text/javascript">
	$(document).ready(function() { 
		$('.addC').relCopy({});
		 

		$('.removeCurrentFiles').on('click',function(){
			var removeUrl = $(this).attr('href');
			$.get(removeUrl,function(response){});
			$(this).parent('div').empty();	
			return false;
		});		
		
	});
	</script>		 
