
@if($setting['form-method'] =='native')
	<div class="sbox">
		<div class="sbox-title">
			<h4> <i class="fa fa-table"></i> <?php echo $pageTitle ;?> <small>{{ $pageNote }}</small>
				<a href="javascript:void(0)" class="collapse-close pull-right btn btn-xs btn-danger" onclick="ajaxViewClose('#{{ $pageModule }}')"><i class="fa fa fa-times"></i></a>
			</h4>
	</div>

	<div class="sbox-content">
@endif
<?php
  $lintVigencia = 0;
  foreach ($lrowTipoDocumento as $t) :
    $lintVigencia = $t->Vigencia;
    $lintTipo = $t->Tipo;
    $Descripcion = $t->Descripcion;
  endforeach;
?>
			{!! Form::open(array('url'=>'documentos/save/'.SiteHelpers::encryptID($row['IdDocumento']), 'class'=>'form-horizontal','files' => true , 'parsley-validate'=>'','novalidate'=>' ','id'=> 'documentosFormAjax')) !!}
			<div class="col-md-12">
						<fieldset><legend> Solicitudes </legend>
				{!! Form::hidden('IdDocumento', $row['IdDocumento']) !!}
				{!! Form::hidden('IdTipoDocumento', $row['IdTipoDocumento']) !!}
				{!! Form::hidden('Entidad', $row['Entidad']) !!}
				{!! Form::hidden('IdEntidad', $row['IdEntidad']) !!}
				{!! Form::hidden('Documento', $row['Documento']) !!}

					<div class="form-group  " >
						<label for="Documento" class=" control-label col-md-4 text-left"> Tipo Documento: </label>
						<div class="col-md-6">
							{!! $Descripcion !!}
						</div>
						<div class="col-md-2">
						</div>
					</div>

					<?php if ($lintTipo!=2):?>
					  <div class="form-group  " >
						<label for="Documento" class=" control-label col-md-4 text-left"> Documento <span class="asterix"> * </span></label>
						<div class="col-md-6">
						  <input  type='file' name='DocumentoURL' id='DocumentoURL' @if($row['DocumentoURL'] =='') class='required' @endif style='width:150px !important;'  />
					 	  <div >
							{!! SiteHelpers::showUploadedFile($row['DocumentoURL'],'/uploads/documents/') !!}
						  </div>
						</div>
						<div class="col-md-2"></div>
					  </div>
					<?php else: ?>
					  <input  type='file' style="display:none;" name='DocumentoURL' id='DocumentoURL' style='width:150px !important;'  />
					<?php endif ?>

					<?php if ($row['IdTipoDocumento']==5):?>
						<div class="form-group  " >
							<label for="Documento" class=" control-label col-md-4 text-left"> PDF con firmas <span class="asterix"> * </span></label>
							<div class="col-md-6">
							 <input  type='file' name='DocumentoURLFirma' id='DocumentoURLFirma' style='width:150px !important;'  />
						 	 <div >
								{!! SiteHelpers::showUploadedFile($row['DocumentoURL'],'/uploads/documents/') !!}
							 </div>
							 </div>
							 <div class="col-md-2">

							 </div>
						</div>
					<?php elseif ($row['IdTipoDocumento']==7):?>
						<div class="form-group  " >
							<label for="Documento" class=" control-label col-md-4 text-left"> PDF <span class="asterix"> * </span></label>
							<div class="col-md-6">
							 <input  type='file' name='DocumentoURLFirma' id='DocumentoURLFirma' style='width:150px !important;'  />
						 	 <div >
								{!! SiteHelpers::showUploadedFile($row['DocumentoURL'],'/uploads/documents/') !!}
							 </div>
							 </div>
							 <div class="col-md-2">
								
							 </div>
						</div>
					<?php elseif ($row['IdTipoDocumento']!=1):?>
						<?php if ($lintVigencia=="1"):?>
						  <div class="form-group  " >
							<label for="Fecha Vencimiento" class=" control-label col-md-4 text-left"> Fecha Vencimiento </label>
							<div class="col-md-6">

								<div class="input-group m-b" style="width:150px !important;">
									{!! Form::text('FechaVencimiento', $row['FechaVencimiento'],array('class'=>'form-control date')) !!}
									<span class="input-group-addon"><i class="fa fa-calendar"></i></span>
								</div>
							 </div>
							 <div class="col-md-2"></div>
						  </div>
						<?php elseif ($lintVigencia=="2"):?>
						  
						<?php elseif ($lintVigencia=="3"):?>
						  <div class="form-group  " >
							<label class=" control-label col-md-4 text-left"> Recurrencia </label>
							<div class="col-md-6">
							  <span id="Vigencia" name="Vigencia">
							  	Una vez
							  	<span style="display:none;" id="Vigencia">{{ $row['Vigencia'] }}</span>
							  </span>
							 </div>
							 <div class="col-md-2"></div>
						  </div>
                          {!! Form::hidden('FechaVencimiento', '') !!}
						<?php endif ?>
					<?php else: ?>
					   {!! Form::hidden('Vigencia', $row['Vigencia']) !!}
					   {!! Form::hidden('FechaVencimiento', $row['FechaVencimiento']) !!}
					   {!! Form::hidden('FechaEmision', $row['FechaEmision']) !!}
					<?php endif ?>
			   {!! Form::hidden('IdEstatus', 2) !!}
			   {!! Form::hidden('createdOn', $row['createdOn']) !!}
			   {!! Form::hidden('entry_by', $row['entry_by']) !!}
			   {!! Form::hidden('updatedOn', $row['updatedOn']) !!}
			   {!! Form::hidden('entry_by_access', $row['entry_by_access']) !!}
			   {!! Form::hidden('contrato_id', $row['contrato_id']) !!}
			   </fieldset>
			</div>



	<hr />
	<div class="clr clear"></div>

	<h5> Datos Requeridos  </h5>
 			<?php foreach ($lrowTipoDocumentoValor as $p): ?>
  				<div class="form-group  " >

					<label for="<?=$p->Etiqueta?>" class=" control-label col-md-4 text-left"> <?=$p->Etiqueta?> <span class="asterix"><?=$p->Requerido=='SI'?'*':''?></span></label>
					<div class="col-md-6">
						<div class="input-group m-b" style="width:150px !important;">
						<?php  if($p->TipoValor=="Fecha"){?>
							<input type="text" name="bulk_Valor[]" class="form-control date" <?=$p->Requerido=='SI'?'required':''?>>
							<span class="input-group-addon"><i class="fa fa-calendar"></i></span>
						  <?php } else if ($p->TipoValor=="Numérico"){?>
						    <input type="number" name="bulk_Valor[]" class="form-control" <?=$p->Requerido=='SI'?'required':''?>>
						    <?php } else if ($p->TipoValor=="Texto"){ ?>
						    <input type="text" name="bulk_Valor[]" class="form-control" <?=$p->Requerido=='SI'?'required':''?> >
						     <?php }else if ($p->TipoValor=="Select Option" || $p->TipoValor=="Radio" || $p->TipoValor=="CheckBox")
							$listaValores = \DB::table('tbl_tipo_documento_data')->where('IdTipoDocumentoValor',$p->IdTipoDocumentoValor)->get();
								if ($p->TipoValor=="Select Option"){
							?>
								   <select name="bulk_Valor[]" class="form-control" <?=$p->Requerido=='SI'?'required':''?>>
								   <option value="">Seleccione Valor</option>
								<?php foreach ($listaValores as $val) :?>
								<option value="<?=$val->Valor?>"><?=$val->Display?></option>
								<?php endforeach; ?>
								   </select>
							<?php } if ($p->TipoValor=="Radio"){ ?>
								<?php foreach ($listaValores as $val) :?>
								<input type="radio" name="bulk_Valor[]" value="<?=$val->Valor?>" > <?=$val->Display?><br>
								<?php endforeach; ?>
							<?php } if ($p->TipoValor=="CheckBox"){ ?>
								<?php foreach ($listaValores as $val) :?>
								<input type="checkbox" name="bulk_Valor[]" value="<?=$val->Valor?>" > <?=$val->Display?><br>
								<?php endforeach; ?>
								<?php } ?>
							<input type="hidden" name="bulk_IdTipoDocumentoValor[]" id="IdTipoDocumentoValor" value="<?=$p->IdTipoDocumentoValor?>">
							<input type="hidden" name="counter[]">
						</div>
					</div>
					<div class="col-md-2"></div>
				</div>
  			<?php endforeach; ?>
			  <input type="hidden" name="enable-masterdetail" value="true">
     <hr />

			<div style="clear:both"></div>

			<div class="form-group">
				<label class="col-sm-4 text-right">&nbsp;</label>
				<div class="col-sm-8">
					<button type="submit" class="btn btn-primary btn-sm "><i class="icon-checkmark-circle2"></i>  {{ Lang::get('core.sb_save') }} </button>
					<button type="button" onclick="ajaxViewClose('#{{ $pageModule }}')" class="btn btn-success btn-sm"><i class="icon-cancel-circle2 "></i>  {{ Lang::get('core.sb_cancel') }} </button>
				</div>
			</div>
			{!! Form::close() !!}


@if($setting['form-method'] =='native')
	</div>
</div>
@endif


</div>

<script type="text/javascript">
$(document).ready(function() {

	$('.addC').relCopy({});
	$('.editor').summernote();
	$('.previewImage').fancybox();
	$('.tips').tooltip();
	$(".select2").select2({ width:"98%"});
	$('.date').datepicker({format:'yyyy-mm-dd',autoClose:true})
	$('.datetime').datetimepicker({format: 'yyyy-mm-dd hh:ii:ss'});
	$('input[type="checkbox"],input[type="radio"]').iCheck({
		checkboxClass: 'icheckbox_square-red',
		radioClass: 'iradio_square-red',
	});
	    $(".FechaEmision").change(function(){

        var fecha = new Date($(this).val()+"T10:20:30Z");
    	fecha.setSeconds($("#Vigencia").html()*86400);
		var day = fecha.getDate();
		var month = fecha.getMonth()+1;
		var year = fecha.getFullYear();
		var newfecha = year+'-'+month+'-'+day;
		$('.FechaVencimiento').datepicker('update', newfecha);

	});
		$('.removeMultiFiles').on('click',function(){
			var removeUrl = '{{ url("documentos/removefiles?file=")}}'+$(this).attr('url');
			$(this).parent().remove();
			$.get(removeUrl,function(response){});
			$(this).parent('div').empty();
			return false;
		});

	var form = $('#documentosFormAjax');
	form.parsley();
	form.submit(function(){

		if(form.parsley('isValid') == true){
			var options = {
				dataType:      'json',
				beforeSubmit :  showRequest,
				success:       showResponse
			}
			$(this).ajaxSubmit(options);
			return false;

		} else {
			return false;
		}

	});

});

function showRequest()
{
	$('.ajaxLoading').show();
}
function showResponse(data)  {

	if(data.status == 'success')
	{
		ajaxViewClose('#{{ $pageModule }}');
		//ajaxFilter('#{{ $pageModule }}','{{ $pageUrl }}/data');
		reloaddatatable();
		notyMessage(data.message);
		$('#sximo-modal').modal('hide');
	} else {
		notyMessageError(data.message);
		$('.ajaxLoading').hide();
		return false;
	}
}

</script>
