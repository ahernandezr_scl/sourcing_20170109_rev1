

		 {!! Form::open(array('url'=>'personascontratospersonas/savepublic', 'class'=>'form-horizontal','files' => true , 'parsley-validate'=>'','novalidate'=>' ')) !!}

	@if(Session::has('messagetext'))
	  
		   {!! Session::get('messagetext') !!}
	   
	@endif
	<ul class="parsley-error-list">
		@foreach($errors->all() as $error)
			<li>{{ $error }}</li>
		@endforeach
	</ul>		


<div class="col-md-12">
						<fieldset><legend> Personas Contratos</legend>
									
									  <div class="form-group  " >
										<label for="IdContratosPersonas" class=" control-label col-md-4 text-left"> IdContratosPersonas </label>
										<div class="col-md-6">
										  {!! Form::text('IdContratosPersonas', $row['IdContratosPersonas'],array('class'=>'form-control', 'placeholder'=>'',   )) !!} 
										 </div> 
										 <div class="col-md-2">
										 	
										 </div>
									  </div> 					
									  <div class="form-group  " >
										<label for="IdPersona" class=" control-label col-md-4 text-left"> IdPersona </label>
										<div class="col-md-6">
										  <select name='IdPersona' rows='5' id='IdPersona' class='select2 '   ></select> 
										 </div> 
										 <div class="col-md-2">
										 	
										 </div>
									  </div> 					
									  <div class="form-group  " >
										<label for="IdContratista" class=" control-label col-md-4 text-left"> IdContratista </label>
										<div class="col-md-6">
										  {!! Form::text('IdContratista', $row['IdContratista'],array('class'=>'form-control', 'placeholder'=>'',   )) !!} 
										 </div> 
										 <div class="col-md-2">
										 	
										 </div>
									  </div> 					
									  <div class="form-group  " >
										<label for="Contrato Id" class=" control-label col-md-4 text-left"> Contrato Id </label>
										<div class="col-md-6">
										  <select name='contrato_id' rows='5' id='contrato_id' class='select2 '   ></select> 
										 </div> 
										 <div class="col-md-2">
										 	
										 </div>
									  </div> 					
									  <div class="form-group  " >
										<label for="IdRol" class=" control-label col-md-4 text-left"> IdRol </label>
										<div class="col-md-6">
										  <select name='IdRol' rows='5' id='IdRol' class='select2 '   ></select> 
										 </div> 
										 <div class="col-md-2">
										 	
										 </div>
									  </div> 					
									  <div class="form-group  " >
										<label for="IdCentro" class=" control-label col-md-4 text-left"> IdCentro </label>
										<div class="col-md-6">
										  {!! Form::text('IdCentro', $row['IdCentro'],array('class'=>'form-control', 'placeholder'=>'',   )) !!} 
										 </div> 
										 <div class="col-md-2">
										 	
										 </div>
									  </div> 					
									  <div class="form-group  " >
										<label for="IdEstatus" class=" control-label col-md-4 text-left"> IdEstatus </label>
										<div class="col-md-6">
										  {!! Form::text('IdEstatus', $row['IdEstatus'],array('class'=>'form-control', 'placeholder'=>'',   )) !!} 
										 </div> 
										 <div class="col-md-2">
										 	
										 </div>
									  </div> 					
									  <div class="form-group  " >
										<label for="Renta" class=" control-label col-md-4 text-left"> Renta </label>
										<div class="col-md-6">
										  {!! Form::text('Renta', $row['Renta'],array('class'=>'form-control', 'placeholder'=>'',   )) !!} 
										 </div> 
										 <div class="col-md-2">
										 	
										 </div>
									  </div> 					
									  <div class="form-group  " >
										<label for="Entry By Access" class=" control-label col-md-4 text-left"> Entry By Access </label>
										<div class="col-md-6">
										  {!! Form::text('entry_by_access', $row['entry_by_access'],array('class'=>'form-control', 'placeholder'=>'',   )) !!} 
										 </div> 
										 <div class="col-md-2">
										 	
										 </div>
									  </div> </fieldset>
			</div>
			
			

			<div style="clear:both"></div>	
				
					
				  <div class="form-group">
					<label class="col-sm-4 text-right">&nbsp;</label>
					<div class="col-sm-8">	
					<button type="submit" name="apply" class="btn btn-info btn-sm" ><i class="fa  fa-check-circle"></i> {{ Lang::get('core.sb_apply') }}</button>
					<button type="submit" name="submit" class="btn btn-primary btn-sm" ><i class="fa  fa-save "></i> {{ Lang::get('core.sb_save') }}</button>
				  </div>	  
			
		</div> 
		 
		 {!! Form::close() !!}
		 
   <script type="text/javascript">
	$(document).ready(function() { 
		
		
		$("#IdPersona").jCombo("{!! url('personascontratospersonas/comboselect?filter=tbl_personas:IdPersona:IdPersona') !!}",
		{  selected_value : '{{ $row["IdPersona"] }}' });
		
		$("#contrato_id").jCombo("{!! url('personascontratospersonas/comboselect?filter=tbl_contrato:contrato_id:contrato_id') !!}",
		{  selected_value : '{{ $row["contrato_id"] }}' });
		
		$("#IdRol").jCombo("{!! url('personascontratospersonas/comboselect?filter=tbl_roles:IdRol:IdRol') !!}",
		{  selected_value : '{{ $row["IdRol"] }}' });
		 

		$('.removeCurrentFiles').on('click',function(){
			var removeUrl = $(this).attr('href');
			$.get(removeUrl,function(response){});
			$(this).parent('div').empty();	
			return false;
		});		
		
	});
	</script>		 
