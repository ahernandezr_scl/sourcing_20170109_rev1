<div class="m-t" style="padding-top:25px;">	
    <div class="row m-b-lg animated fadeInDown delayp1 text-center">
        <h3> {{ $pageTitle }} <small> {{ $pageNote }} </small></h3>
        <hr />       
    </div>
</div>
<div class="m-t">
	<div class="table-responsive" > 	

		<table class="table table-striped table-bordered" >
			<tbody>	
		
			
					<tr>
						<td width='30%' class='label-view text-right'>{{ SiteHelpers::activeLang('Concepto Anotacion', (isset($fields['IdConceptoAnotacion']['language'])? $fields['IdConceptoAnotacion']['language'] : array())) }}</td>
						<td>{{ $row->IdConceptoAnotacion}} </td>
						
					</tr>
				
					<tr>
						<td width='30%' class='label-view text-right'>{{ SiteHelpers::activeLang('Persona', (isset($fields['IdPersona']['language'])? $fields['IdPersona']['language'] : array())) }}</td>
						<td>{{ $row->IdPersona}} </td>
						
					</tr>
				
					<tr>
						<td width='30%' class='label-view text-right'>{{ SiteHelpers::activeLang('Observacion', (isset($fields['Observacion']['language'])? $fields['Observacion']['language'] : array())) }}</td>
						<td>{{ $row->Observacion}} </td>
						
					</tr>
						
					<tr>
						<td width='30%' class='label-view text-right'></td>
						<td> <a href="javascript:history.go(-1)" class="btn btn-primary"> Back To Grid <a> </td>
						
					</tr>					
				
			</tbody>	
		</table>   

	 
	
	</div>
</div>	