

		 {!! Form::open(array('url'=>'anotaciones/savepublic', 'class'=>'form-horizontal','files' => true , 'parsley-validate'=>'','novalidate'=>' ')) !!}

	@if(Session::has('messagetext'))
	  
		   {!! Session::get('messagetext') !!}
	   
	@endif
	<ul class="parsley-error-list">
		@foreach($errors->all() as $error)
			<li>{{ $error }}</li>
		@endforeach
	</ul>		


<div class="col-md-12">
						<fieldset><legend> Anotaciones</legend>
				{!! Form::hidden('IdAnotacion', $row['IdAnotacion']) !!}					
									  <div class="form-group  " >
										<label for="Annotation" class=" control-label col-md-4 text-left"> Annotation <span class="asterix"> * </span></label>
										<div class="col-md-6">
										  <select name='IdConceptoAnotacion' rows='5' id='IdConceptoAnotacion' class='select2 ' required  ></select> 
										 </div> 
										 <div class="col-md-2">
										 	
										 </div>
									  </div> 					
									  <div class="form-group  " >
										<label for="IdPersona" class=" control-label col-md-4 text-left"> IdPersona </label>
										<div class="col-md-6">
										  <select name='IdPersona' rows='5' id='IdPersona' class='select2 '   ></select> 
										 </div> 
										 <div class="col-md-2">
										 	
										 </div>
									  </div> 					
									  <div class="form-group  " >
										<label for="Observations" class=" control-label col-md-4 text-left"> Observations </label>
										<div class="col-md-6">
										  {!! Form::text('Observacion', $row['Observacion'],array('class'=>'form-control', 'placeholder'=>'',   )) !!} 
										 </div> 
										 <div class="col-md-2">
										 	
										 </div>
									  </div> {!! Form::hidden('createdOn', $row['createdOn']) !!}{!! Form::hidden('entry_by', $row['entry_by']) !!}{!! Form::hidden('updatedOn', $row['updatedOn']) !!}</fieldset>
			</div>
			
			

			<div style="clear:both"></div>	
				
					
				  <div class="form-group">
					<label class="col-sm-4 text-right">&nbsp;</label>
					<div class="col-sm-8">	
					<button type="submit" name="apply" class="btn btn-info btn-sm" ><i class="fa  fa-check-circle"></i> {{ Lang::get('core.sb_apply') }}</button>
					<button type="submit" name="submit" class="btn btn-primary btn-sm" ><i class="fa  fa-save "></i> {{ Lang::get('core.sb_save') }}</button>
				  </div>	  
			
		</div> 
		 
		 {!! Form::close() !!}
		 
   <script type="text/javascript">
	$(document).ready(function() { 
		
		
		$("#IdConceptoAnotacion").jCombo("{!! url('anotaciones/comboselect?filter=tbl_concepto_anotacion:IdConceptoAnotacion:Descripcion') !!}",
		{  selected_value : '{{ $row["IdConceptoAnotacion"] }}' });
		
		$("#IdPersona").jCombo("{!! url('anotaciones/comboselect?filter=tbl_personas:IdPersona:IdPersona') !!}",
		{  selected_value : '{{ $row["IdPersona"] }}' });
		 

		$('.removeCurrentFiles').on('click',function(){
			var removeUrl = $(this).attr('href');
			$.get(removeUrl,function(response){});
			$(this).parent('div').empty();	
			return false;
		});		
		
	});
	</script>		 
