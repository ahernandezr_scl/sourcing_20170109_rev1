<?php

return array(

	/*
	|--------------------------------------------------------------------------
	| Password Reminder Language Lines
	|--------------------------------------------------------------------------
	|
	| The following language lines are the default lines which match reasons
	| that are given by the password broker for a password update attempt
	| has failed, such as for an invalid token or invalid new password.
	|
	*/

	"password" => "Las contraseñas tienen que ser de almenos 6 caracteres y ser conseguentes con la complejidad.",

	"user" => "No se puede encontrar al usuario con esta cuenta de correo!",

	"token" => "El token de restauración de contraseña no es valido!.",

	"sent" => "Recordatorio de password enviado!",

);
