<?php

return array(
// General 
	"norecord" => "No se encuentran registros",
	"create" => "Crear nuevo",

	// General , Login Info & Signup
	"home" 					=> "Inicio",
	"group" 					=> "Grupo",
	"username" 				=> "Usuario",
	"email" 					=> "Dirección Email",
	"password" 				=> "Password",
	"repassword" 			=> "Confirmar Password",
	"forgotpassword" 		=> "Olvide mi Password",
	"newpassword" 			=> "Nueva Password",
	"conewpassword" 		=> "Confirmar Password",
	"notepassword" 			=> "Deje en blanco si no quiere cambiar la contraseña", // updated apidevlab	
	"submit" 				=> "Insertar",
	"signin" 				=> "Ingresar",
	"signup" 				=> "Registrarse",
	"subscribe" 				=> "Subscribirse",
	"language" 				=> "Lenguaje",
	"subscription" 			=> "Subscripción",
	"firstname" 				=> "Nombres",
	"lastname" 				=> "Apellidos ",
	"lastlogin"				=> "Último ingreso",
	"personalinfo"			=> "Información personal",
	"changepassword"		=> "Cambiar Password",
	"registernew" 			=> "Registrar nueva cuenta ",
	"backtosite" 			=> " Volver al sitio ",
	"planexpiration" 		=> "Plan de expiración",
	'invitationmail' 		=> 'Mail de invitación',
	
/* grid , pagination */
	"grid_displaying" 		=> "Mostrando",
	"grid_to" 				=> "Desde",
	"grid_of" 				=> "Hasta",
	"grid_show" 				=> "Muestra",
	"grid_sort" 				=> "Ordenar por",
	"grid_order" 			=> "Ordenar",	
	"grid_page" 				=> "Páguina",	
		

/* Menu navigation here */
	"m_controlpanel"		=> "Panel de control",
	"m_dashboard" 			=> "Página inicial",
	"m_setting" 				=> "Configuración", // updated apidevlab
	"m_usersgroups" 		=> "Usuarios y Grupos",
	"m_users" 				=> "Usuarios",
	"m_groups" 				=> "Grupos",
	"m_pagecms" 				=> "Paguinas CMS",
	"m_menu" 				=> "Administrador Menú",
	"m_logs" 				=> "Log de actividad",
	"m_codebuilder" 		=> "Modulos y codificación",
	"m_blastemail" 			=> "Email masívo",
	"m_myaccount" 			=> "Mi cuenta",
	"m_logout" 				=> "Salir",
	"m_profile" 				=> "Perfil",
	"m_manual" 				=> "Guia y manual ",
	//modificación por Alvaro Hernandez
	"m_mainsite"			=> "Página principal",
	"m_filemanager"			=> "Archivos Usuario",
	"m_sourcecodeeditor"	=> "Editor de programación",
	"m_databasetables"		=> "Tablas BD",
	"m_changelog"			=> "Log de cambios",
	"m_templateguide"		=> "Guía de plantilla",
	"m_onlinedocto"			=> "",
	"m_sourcecode"			=> "Editor de código",
	"m_databasetables"		=> "Tablas Base de datos",
	"m_filemanager" 		=> "Archivos Usuario",
	"m_mainsite"			=> "Portal",

/* Setting page translation */	

	"t_generalsetting"			=> "Configuración General" , //updated apidevlab
	"t_generalsettingsmall"		=> "Administrar configuración de valores", // updated apidevlab
	"t_blastemail"				=> "Correo masivo", // updated apidevlab
	"t_blastemailsmall"			=> "Mail Masivo",
	"t_emailtemplate"			=> "Plantillas de email",
	"t_emailtemplatesmall"		=> "Administrar plantillas de email", // updated apidevlab
	"t_loginsecurity"			=> "Login y Seguridad", // updated apidevlab
	"t_loginsecuritysmall"		=> "Administrar Logins & Seguridad",	// updated apidevlab	
	"t_socialmedia"				=> "Login vía redes sociales", // updated apidevlab
	"t_lfb"						=> "Login vía Facebook", // updated apidevlab
	"t_lgoogle"					=> "Login vía Google", // updated apidevlab
	"t_ltwit"					=> "Login vía Twitter", // updated apidevlab
	"tab_siteinfo"				=> "Configuración General", // Site Info updated apidevlab
	"tab_loginsecurity"			=> "Login y Seguridad",
	"tab_email"					=> "Plantillas email", // updated apidevlab
	"tab_translation"			=> "Traducciones", 
	"fr_appname"				=> "Nombre aplicación ",
	"fr_appdesc"				=> "Descripción aplicación",
	"fr_comname"				=> "Nombre compañía ",
	"fr_emailsys"				=> "Email del sistema ",
	"fr_emailmessage"			=> "Mensaje de email ",
	"fr_enable"					=> "Habilitado",
	"fr_multilanguage"			=> "Multi lenguaje",
	"fr_onlylayoutinterface"	=> "Solo la interfaz principal",
	"fr_mainlanguage"			=> "Lenguaje principal",
	"fr_fronttemplate"			=> "Plantilla Frontend",
	"fr_appmode"				=> "Modo Aplicación",
	"fr_appid"					=> "APP ID",
	"fr_secret"					=> "NUMERO SECRETO",
	"fr_registrationdefault"	=> "Grupo de registro por defecto ",
	"fr_registrationsetting"	=> "Configuración de registro",
	"fr_registration"			=> "Registro",
	"fr_allowregistration"		=> "Aceptar Registros públicos",
	"fr_allowfrontend"			=> "Habilitar Frontend",
	"fr_registrationauto"		=> "Activación Automatica ",
	"fr_registrationmanual"		=> "Activación Manual ",
	"fr_registrationemail"		=> "Email con código de activación ",	
	"fr_emailsubject"			=> "Asunto",
	"fr_emailsendto"			=> "Enviar a",
	"fr_emailmessage"			=> "Mensaje en Email",
	"fr_emailtag"				=> "Usted puede usar",
	//actualizacion por parte de Alvaro Hernandez
	"fr_dateformat"				=> "Formato de fecha",
	"fr_metadescription"		=> "Meta descripción",
	"fr_backendlogo" 			=> "Logo backend",
	"fr_mailsystem"				=> "Sistema de Mail",
	"fr_requireconfig"			=> "Requiere configuración",
	"fr_languagemanager"		=> "Administrador de idiomas",
	"fr_addnewtranslate"		=> "Agregar nueva traducción",
	"fr_tbltranslate_name"		=> "Nombre",
	"fr_tbltranslate_folder"	=> "Carpeta",
	"fr_tbltranslate_autor"		=> "Autor",
	"fr_tbltranslate_action"	=> "Acción",
	"fr_tbltranslate_admin" 	=> "Administrar",
	"fr_tbltranslate_delete"	=> "Borrar",
	//fin primera actualización
	
/* submit */
	"sb_savechanges"			=> "Guardar cambios",
	"sb_send"					=> "Enviar",
	"sb_save"					=> "Guardar",
	"sb_apply"					=> "Aplicar cambio(s)",
	"sb_submit"					=> "Enviar",
	"sb_cancel"					=> "Cancelar",	
	
/* button */
	"btn_back"						=> "Atras",	
	"btn_action"					=> "Acción",	
	"btn_search"					=> "Buscar",	
	"btn_clearsearch"				=> "Limpiar Búsqueda",	
	"btn_download"					=> "Download",	
	"btn_config"					=> "Configuración",	
	"btn_copy"						=> "Copiar", // Ajax 
	"btn_print"						=> "Imprimir",	
	"btn_create"					=> "Crear",	
	"btn_install"					=> "Instalar",
	"btn_backup"					=> "Respaldo",
	"btn_remove"					=> "Eliminar",
	"btn_edit"						=> "Editar",	
	"btn_view"						=> "Ver",
	"btn_typesearch"				=> "Escriba y Ingrese",	// updated apidevlab	
	
/* Core Module */
	"t_menu"						=> "Administrador de menú",
	"t_menusmall"					=> "Lista de todos los menús",
	"t_tipsdrag"					=> "Arrastre y suelte para reordenar el menú ", // updated apidevlab
	"t_tipsnote"					=> "Nota!, Los menús soportan solo 3 niveles ", // updated apidevlab
	"tab_topmenu"					=> "Menú superior",
	"tab_sidemenu"					=> "Menú lateral ",
	"sb_reorder"					=> "Reordenar menú",	
	"fr_mtitle"						=> "Nombre / Título ",	
	"fr_mtype"						=> "Tipo de menú",	
	"fr_mposition"					=> "Posición",	
	"fr_mactive"					=> "Activo",	
	"fr_minactive"					=> "Inactivo",
	"fr_maccess"					=> "Acceso", // updated apidevlab	
	"fr_miconclass"					=> "Clase de icono",
	"fr_mpublic"					=> "Público",
	"fr_mexample"					=> "Ejemplo",
	"fr_musage"						=> "Uso", // updated apidevlab	
	
/* Code BuilderModule */
	"t_module"						=> "Módulo",
	"t_modulesmall"					=> "Lista de todos los módulos",// updated apidevlab
	"tab_installed"					=> "Módulos instalados", // updated apidevlab
	"tab_core"						=> "Módulos del nucleo de sistema", // updated apidevlab
	"fr_modtitle"					=> "Módulo Nombre / Titulo",
	"fr_modnote"					=> "Notas del módulo",
	"fr_modtable"					=> "Tabla del módulo",
	"fr_modautosql"					=> "Sentencia MySQL Automática",
	"fr_modmanualsql"				=> "Sentencia MySQL manual ",
	"fr_createmodule"				=> "Crear nuevo módulo",
	"fr_installmodule"				=> "Instalar módulo",
	"fr_backupmodule"				=> "Respaldar y crear instalador de los módulos ",

/* dashboard Interface */

	"dash_i_module"					=> "Módulo",
	"dash_i_setting"				=> "Ajustes",
	"dash_i_sitemenu"				=> "Menú del sitio",
	"dash_i_usergroup"				=> "Usuarios y grupos",
	"dash_module"					=> "Administrar módulos existentes o Crear uno nuevo",
	"dash_setting"					=> "Configure las opciones de ingreso de su sistema ,nombre , correo etc. ",
	"dash_sitemenu"					=> "Administre el menu de su aplicación frontend o backend",
	"dash_usergroup"				=> "Administre usuarios y grupos y otorgue permisos a módulos y los menus a los que puede ingresar",

/*updates	on may ,5 2014 */
	
	"loginsocial"				=> "Ingreso via redes sociales", // updated apidevlab
	"enteremailforgot"			=> "Ingrese su correo electrónico",
	"detail" 					=> "Ver detalles",
	"addedit" 					=> "Agregar - Editar",
	
/* Notification */
	"note_noexists"				=> "Disculpe, la pagina no existe!", // updated apidevlab
	"note_restric"				=> "Disculpe, usted no tiene acceso a esta pagina!", // updated apidevlab
	"note_success"				=> "Guardado satisfactoriamente!", // updated apidevlab
	"note_error"				=> "El siguiente error a ocurrido !",
	"note_success_delete"		=> "Eliminado satisfactoriamente!",	// updated apidevlab
	"connect_stripe_note"		=> "You need to connect to stripe first.",
	"accept_payment"			=>"Miembros no es capaz de aceptar el pago en este momento.",
);
