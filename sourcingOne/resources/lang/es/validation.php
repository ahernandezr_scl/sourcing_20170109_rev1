<?php

return array(

	"accepted"         => "El :attribute deberá ser aceptado.",
	"active_url"       => "El :attribute no es una URL valida.",
	"after"            => "El :attribute tiene que ser antes de :date.",
	"alpha"            => "El :attribute solo debe contener letras.",
	"alpha_dash"       => "El :attribute solo debe contener letras, numeros, y guiones.",
	"alpha_num"        => "El :attribute solo debe contener letras y numeros.",
	"array"            => "El :attribute tiene que ser un arreglo.",
	"before"           => "El :attribute tiene que ser una fecha despues de :date.",
	"between"          => array(
		"numeric" => "El :attribute tiene que estar entre :min y :max.",
		"file"    => "El :attribute tiene que estar entre :min y :max kilobytes.",
		"string"  => "El :attribute tiene que estar entre :min y :max caracteres.",
		"array"   => "El :attribute tiene que estar entre :min y :max items.",
	),
	"confirmed"        => "El :attribute confirmación no coincide.",
	"date"             => "El :attribute no es una fecha válida.",
	"date_format"      => "El :attribute no cuadra con el formato :format.",
	"different"        => "El :attribute y :oElr deben ser diferentes.",
	"digits"           => "El :attribute debe ser :digits digitos.",
	"digits_between"   => "El :attribute deben ser entre :min y :max digitos.",
	"email"            => "El :attribute formato es inválido.",
	"exists"           => "El selected :attribute es inválido.",
	"image"            => "El :attribute deber ser una imagen.",
	"in"               => "El selected :attribute es inválido.",
	"integer"          => "El :attribute debe ser un entero.",
	"ip"               => "El :attribute debe ser una dirección IP válida.",
	"max"              => array(
		"numeric" => "El :attribute no debe ser mas grande que :max.",
		"file"    => "El :attribute no debe ser mas grande que :max kilobytes.",
		"string"  => "El :attribute no debe ser mas grande que :max caracteres.",
		"array"   => "El :attribute no puede tener mas de :max items.",
	),
	"mimes"            => "El :attribute debe ser un archivo del tipo: :values.",
	"min"              => array(
		"numeric" => "El :attribute al menos debe ser :min.",
		"file"    => "El :attribute al menos debe ser :min kilobytes.",
		"string"  => "El :attribute al menos debe ser :min caracteres.",
		"array"   => "El :attribute debe tener al menos :min items.",
	),
	"not_in"           => "El :attribute seleccionado es invalido.",
	"numeric"          => "El :attribute tiene que ser un numero.",
	"regex"            => "El :attribute formato es invalido.",
	"required"         => "El :attribute es requerido.",
	"required_if"      => "El :attribute es requerido cuando :oElr es :value.",
	"required_with"    => "El :attribute es requerido cuando :values es present.",
	"required_without" => "El :attribute es requerido cuando :values es not present.",
	"same"             => "El :attribute y :oElr deben coincidir.",
	"size"             => array(
		"numeric" => "El :attribute debe ser :size.",
		"file"    => "El :attribute debe ser :size kilobytes.",
		"string"  => "El :attribute debe ser :size characters.",
		"array"   => "El :attribute 	debe contener :size items.",
	),
	"unique"           => "El :attribute ya se ha tomado.",
	"url"              => "El :attribute formato no es válido.",
	"recaptcha" => 'El :attribute campo no es correcto.',
	/*
	|--------------------------------------------------------------------------
	| Custom Validation Language Lines
	|--------------------------------------------------------------------------
	|
	| Here you may specify custom validation messages for attributes using El
	| convention "attribute.rule" to name El lines. This makes it quick to
	| specify a specific custom language line for a given attribute rule.
	|
	*/

	'custom' => array(),

	/*
	|--------------------------------------------------------------------------
	| Custom Validation Attributes
	|--------------------------------------------------------------------------
	|
	| El following language lines are used to swap attribute place-holders
	| with something more reader friendly such as E-Mail Address instead
	| of "email". This simply helps us make messages a little cleaner.
	|
	*/

	'attributes' => array(),

);
